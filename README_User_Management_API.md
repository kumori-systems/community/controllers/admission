## Brief API documentation

### Create user - `POST /users`

Expected POST data (**all properties are mandatory** for now):

```
{
  "username": "johnny",
  "email": "johnny@kumori.systems",
  "firstName": "johnny",
  "lastName": "meinmuto",
  "password": "cool",
  "enabled": true,
  "groups": [
    "developers", "administrators"
  ]
}
```

Admission response data: **true** on success

### Update user - `PUT /users`

Identity of the user to be updated is determined by the username property in the provided data. This means **`username` property is mandatory**.
All other user properties are optional; omitted properties will keep their previous values.

Expected PUT data:

```
{
  "username": "johnny",
  "email": "johnny@kumori.systems",
  "firstName": "johnny",
  "lastName": "meinmuto",
  "password": "cool",
  "enabled": true,
  "groups": [
    "developers", "administrators"
  ]
}
```

Admission response data: **true** on success


### Delete user - `DELETE /users/<username>`

Admission response data: **true** on success


### Get user - `GET /users/<username>`

Admission response data: **a User object**.

Example:
```
{
  "username": "johnny",
  "email": "johnny@kumori.systems",
  "firstName": "johnny",
  "lastName": "meinmuto",
  "enabled": true,
  "groups": [
    "developers", "administrators"
  ]
}
```


### Get users - `GET /users`

Admission response data: **an array of User objects**.

Example:
```
[
  {
    "username": "johnny",
    "email": "johnny@kumori.systems",
    "firstName": "johnny",
    "lastName": "meinmuto",
    "enabled": true,
    "groups": [
      "developers", "administrators"
    ]
  },
  {
    "username": "mike",
    [...]
  }
]
```


### Get groups - `GET /groups`

Admission response data: **an array of Group names**.

Example:
```
[ "developers", "administrators" ]
```

