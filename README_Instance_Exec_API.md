# API for command execution in instances (ticket 152)

Admission API now supports requesting commands execution in deployed instances.
This document describes how the 'exec' mechanism is implemented and how a client
must use it.

This has been implemented as part of ticket [ticket 152](https://gitlab.com/kumori/kv3/project-management/-/issues/152).

## Mechanism description

Since we will support execution of interactive commands (for example `/bin/sh`),
the mechanism must be able to handle bidirectional communication, so the
**implementation is based on Websockets.**

To maximize compatibility, standard websockets are used.

Proper solutions for securing websockets when using detached Express routers and
an authentication/authorization middleware (like Keycloak in our case) are hard
to implement in a clean way, since websocket *upgrade* mechanism must be handled
by the underlying HTTP server. This may result in the need to export some logic
knowledge outside of the routers, which is not desirable. To guarantee security
as well as the client getting as much information as possible when
authentication fails, we have implemented a custom high-level "protocol" for
using the `exec` endpoint of Admission API.


### API endpoint

The `exec` API endpoint is:

`<admission-endpoint>/instances/:instance/exec?<query>`

where:

- **`instance` (mandatory)**: the ID of the instance where the command should be
  executed, as returned from API methods that retrieve deployment/instances
  information. Currently, this ID is the name of the Pod backing the instance
  (although that might change in the future).
- **query**: the following parameters can be set in the request query:
  - **`access_token` (mandatory if `authorization` header not set)**: the user
    access token, for authentication in case no `authorization` header has been
    set.
  - **`container` (mandatory if instance has several containers)**: if the
    instance has several containers, the name of the container where the command
    should be executed, as defined in the Component manifest. If the instance
    has only one container, this can be omitted.
  - **`command` (mandatory)**: the command to execute. The command can be a
    string or an array of strings (keep in mind spaces are not allowed and
    array notation should be used instead). Some valid examples:
    - `"ls"`
    - `[ "ls", "-l" ]`
    - `[ "/bin/sh", "-c", "echo HELLO && ls -l && cat /etc/resolv.conf && pwd" ]`
  - **`tty`**: if set to **`yes`** it indicates the command should be executed
    in a TTY enabled session. Implications of setting `tty` are explained later.
  - **`rows`**: number of columns the output should be prepared for. If both
    `rows` and `columns` are set, resizing functionality will be enabled.
  - **`columns`**: number of columns the output should be prepared for. If both
    `rows` and `columns` are set, resizing functionality will be enabled.

Using the Websocket endpoint is a two-step process.

### First step: validation and get a session token

To initiate an `exec` websocket session, the client must perform **a regular
HTTP request** to `GET <admission-endpoint>/instances/:instance/exec?<query>`,
with the parameters described above.

If all the necessary validations are passed, Admission will return the following
response:
```
{
  "success": true,
  "message": "Websocket access is allowed to instance <instanceId>.",
  "data": {
    "wsSessionToken": "xxxxxxxxxxx"
  }
}
```

The obtained **`wsSessionToken`** must included in the query when establishing
the actual websocket connection. It is an ephemeral token that is **only valid
during 30 seconds**.

### Second step: websocket connection

Once the client has obtained the `wsSessionToken`, it can establish the
websocket connection with the same parameters described above **plus a new
`session_token` query parameter with the obtained token**.

For example, in Coffeescript:
```
WebSocket   = require 'ws'

query =
  session_token: "xxxxxxxxxx"
  access_token: "eyJh...ouOA"
  container: null
  command: "/bin/sh"
  tty: 'yes'
queryString = querystring.stringify query

url = ".../instances/<instanceId>/exec?#{queryString}"

ws = new WebSocket url
```

The `access_token` query parameter can be omitted if the HTTP request includes
the access token in a standard `Authorization` header.

### Execution of non interactive commands

The result of the executed command will be returned to the client as one or more
messages. Since there is a single communication channel, `stdout` and `stderr`
messages will be prefixed with `OUT:` and `ERR:` respectively.

When the command execution finishes the server will close the Websocket, with a
status `code` and a `reason` message.

In case the execution could be completed successfully (independently of the
result of the command), the closing `code` will always be `1000` and the
`reason` will be set to "Command finished." or something similar.

Errors handled or detected by Admission server will set the `code` to `4000` and
always include an error message in `reason`.

In some scenarios, the client might end up getting other error codes. For
example if authentication would fail, the websocket would be closed with a
`1005` code and no explanation in `reason`. This is precisely the reason to have
implemented the initial HTTP `GET` request: to detect as many error cases as
possible and be able to clearly inform the client.


### Execution of interactive commands

When an interactive command is executed, the websocket session remains open
until the command ends (typically by a client subcommand or action).

The output and errors are handled exactly as for non-interactive commands.

Every message received by the server-side websocket will be redirected to the
execution process as if it was typed in the remote terminal.

### Output resizing

If a terminal size was set, the client can send a special type of message to
notify a resize. Such messages must **always start with "RESIZE:", followed by
the new rows and columns values separated by a comma**:

`RESIZE:80,140`


### Implications of setting `tty`

When a command is executed requesting to use a tty (`tty='yes'`), some important
behavioural details should be considered.

First of all, `stderr` is automatically redirected to `stdout`, meaning it
becomes impossible to determine whether a messages is normal output or an error
message.

Also, in "`tty` mode", everything typed is echoed back to the client before the
command result.

Those two are the most relevant differences from the point of view to
implementing a client, but there are some other differences that we recommend
investigating before deciding whether to use it (execution environment,
environment variables, output format of some commands, etc.)

