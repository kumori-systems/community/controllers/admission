#!/bin/bash

printf "# Copyright © 2020 Kumori Systems S.L. All rights reserved.\n\n" > /tmp/tmp-admission-license

shopt -s globstar nullglob extglob
GLOBIGNORE='node_modules/**'

for f in **/*.@(coffee) ;do
  [[ ! -f $f ]] && continue
  if (grep -q "Copyright © 2020 Kumori Systems S.L" $f);then
    echo "File: $f  -  No need to add License Header"
  else
    echo "File: $f  -  Adding License Header"
    cat "/tmp/tmp-admission-license" "$f" | sponge "$f"
  fi
done

for f in **/Dockerfile ;do
  [[ ! -f $f ]] && continue
  if (grep -q "Copyright © 2020 Kumori Systems S.L" $f);then
    echo "File: $f  -  No need to add License Header"
  else
    echo "File: $f  -  Adding License Header"
    cat "/tmp/tmp-admission-license" "$f" | sponge "$f"
  fi
done
