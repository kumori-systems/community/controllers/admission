'use strict';

(function () {
  /*
  * Copyright 2020 Kumori Systems S.L.
   * Licensed under the EUPL, Version 1.2 or – as soon they
    will be approved by the European Commission - subsequent
    versions of the EUPL (the "Licence");
   * You may not use this work except in compliance with the
    Licence.
   * You may obtain a copy of the Licence at:
     https://joinup.ec.europa.eu/software/page/eupl
   * Unless required by applicable law or agreed to in
    writing, software distributed under the Licence is
    distributed on an "AS IS" basis,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
    express or implied.
   * See the Licence for the specific language governing
    permissions and limitations under the Licence.
   */
  var ADMISSION_PORT_ENV_VAR,
      ADMISSION_VALID_ROLES,
      AdmissionRestAPI,
      AdmissionServer,
      BEGIN_CERT_SEPARATOR,
      CLIENT_CERT_HEADER_NAME,
      DEFAULT_ADM_CONFIG,
      DEFAULT_ADM_CONFIG_FILE,
      DEFAULT_ADM_CONFIG_SAMPLE,
      DEFAULT_PORT,
      END_CERT_SEPARATOR,
      EventEmitter,
      Keycloak,
      KeycloakRequest,
      KeycloakUserManager,
      MockKeycloak,
      MockKeycloakClientCert,
      MockKeycloakClientCertForLogin,
      MockKeycloakClientCertUserManager,
      MockKeycloakUserManager,
      WebsocketPublisher,
      _,
      admConfig,
      admServer,
      args,
      bodyParser,
      configFile,
      cors,
      crypto,
      err,
      express,
      expressWs,
      http,
      multer,
      parseArgs,
      parseXFCCHeader,
      path,
      session,
      utils,
      indexOf = [].indexOf;

  _ = require('lodash');

  http = require('http');

  cors = require('cors');

  path = require('path');

  crypto = require('crypto');

  multer = require('multer');

  express = require('express');

  expressWs = require('express-ws');

  parseArgs = require('minimist');

  bodyParser = require('body-parser');

  session = require('express-session');

  Keycloak = require('keycloak-connect');

  EventEmitter = require('events').EventEmitter;

  require('./index');

  AdmissionRestAPI = require('./admission-rest-api');

  WebsocketPublisher = require('./websocket-publisher');

  KeycloakRequest = require('./keycloak-api/keycloak-request');

  KeycloakUserManager = require('./keycloak-api/keycloak-user-manager');

  utils = require('./utils');

  // Port where Admission server will listen
  // DEFAULT_PORT = 8085
  DEFAULT_PORT = 3000;

  // Name of the ENV variable with Admission port
  ADMISSION_PORT_ENV_VAR = 'ADMISSION_PORT';

  DEFAULT_ADM_CONFIG = {
    restTimeout: -1,
    maxBundleSize: 52428800,
    manifestRepository: {
      type: "k8s",
      config: {}
    },
    domains: {
      refDomain: "test.kumori.cloud",
      random: {
        baseDomain: "test.deployedin.cloud",
        prefix: "",
        join: "-",
        words: 2,
        sufixNumbers: 4
      }
    }
  };

  DEFAULT_ADM_CONFIG_SAMPLE = {
    restTimeout: -1,
    maxBundleSize: 52428800,
    manifestRepository: {
      type: "k8s",
      config: {
        k8sApi: {
          kubeConfig: {
            clusters: [{
              name: 'minikube-cluster',
              server: 'https://192.168.99.100:8443',
              caFile: '/home/jferrer/.minikube/ca.crt',
              skipTLSVerify: false
            }],
            users: [{
              name: 'minikube',
              certFile: '/home/jferrer/.minikube/client.crt',
              keyFile: '/home/jferrer/.minikube/client.key'
            }, {
              name: 'my-user',
              password: 'my-password'
            }],
            contexts: [{
              name: 'minikube',
              user: 'my-user',
              cluster: 'minikube-cluster'
            }],
            currentContext: 'minikube'
          }
        }
      }
    },
    domains: {
      refDomain: "test.kumori.cloud",
      random: {
        baseDomain: "test.deployedin.cloud",
        prefix: "",
        join: "-",
        words: 2,
        sufixNumbers: 4
      }
    },
    authentication: {
      keycloakConfig: {
        "realm": "quickstart",
        "auth-server-url": "http://jferrer-keycloak.test.kumori.cloud:8080/auth",
        "ssl-required": "external",
        "resource": "test-webserver",
        "verify-token-audience": true,
        "credentials": {
          "secret": "951addd0-d563-439e-aa74-dfcb0e55ba64"
        },
        "use-resource-role-mappings": true,
        "confidential-port": 0,
        bearerOnly: true,
        operator: {
          namespace: "keycloak",
          realmName: "KumoriCluster",
          realmLabel: "keycloak-sso",
          clientName: "admission",
          finalizerRemovalPeriodSeconds: 30
        }
      }
    },
    tcpports: [],
    tcpInternalPorts: []
  };

  // "realm": "quickstart",
  // "bearer-only": true,
  // "auth-server-url": "http://jferrer-keycloak.test.kumori.cloud:8080/auth",
  // "ssl-required": "external",
  // "resource": "test-webserver",
  // "verify-token-audience": true,
  // "use-resource-role-mappings": true,
  // "confidential-port": 0

  // 'realm': 'quickstart'
  // 'auth-server-url': 'http://jferrer-keycloak.test.kumori.cloud:8080/auth'
  // 'ssl-required': 'external'
  // 'resource': 'test-webserver'
  // 'public-client': true
  // 'confidential-port': 0

  // realm: 'quickstart'
  // clientId: 'myclient'
  // bearerOnly: true
  // serverUrl: 'http://localhost:8080/auth'
  // realmPublicKey: 'MIIBIjANB...'

  // KEYCLOAK CONFIG WITH "PUBLIC":

  //       'realm': 'quickstart'
  //       'auth-server-url': 'http://jferrer-keycloak.test.kumori.cloud:8080/auth'
  //       'ssl-required': 'external'
  //       'resource': 'test-webserver'
  //       'public-client': true
  //       'confidential-port': 0
  // KEYCLOAK CONFIG WITH BEARER-ONLY:

  //       "realm": "quickstart",
  //       "bearer-only": true,
  //       "auth-server-url": "http://jferrer-keycloak.test.kumori.cloud:8080/auth",
  //       "ssl-required": "external",
  //       "resource": "test-webserver",
  //       "verify-token-audience": true,
  //       "use-resource-role-mappings": true,
  //       "confidential-port": 0

  // KEYCLOAK CONFIG WITH "CONFIDENTIAL":

  //       "realm": "quickstart",
  //       "auth-server-url": "http://jferrer-keycloak.test.kumori.cloud:8080/auth",
  //       "ssl-required": "external",
  //       "resource": "test-webserver",
  //       "verify-token-audience": true,
  //       "credentials": {
  //         "secret": "<secret>"
  //       },
  //       "use-resource-role-mappings": true,
  //       "confidential-port": 0

  //###############################################################################
  //###############################################################################
  // Mock classes for replacing Keycloak middleware when no authentication is     #
  // configured.                                                                  #
  //###############################################################################
  MockKeycloak = class MockKeycloak {
    constructor() {}

    init() {}

    protect(protectedScope) {
      if (protectedScope === 'admission:administrator') {
        // For Admin user API methods, return a 404
        return function (req, res, next) {
          return res.status(404).send('Not Found (disabled)');
        };
      } else {
        // For all other methods, do nothing (pass to next middleware)
        return function (req, res, next) {
          return next();
        };
      }
    }

    middleware() {
      return function (req, res, next) {
        req.session['keycloak-token'] = 'anonymous';
        return next();
      };
    }

  };

  MockKeycloakUserManager = class MockKeycloakUserManager {
    constructor() {}

    init() {}

    terminate() {}

    removeFinalizers() {
      return q();
    }

    getUsers() {
      return q([]);
    }

    getUser() {
      return q({});
    }

    getGroups() {
      return q([]);
    }

    deleteUser() {
      return q();
    }

    createUser() {
      return q();
    }

    updateUser() {
      return q();
    }

    updateUserPassword() {
      return q();
    }

  };

  //###############################################################################
  //###############################################################################
  // Mock classes for replacing Keycloak middleware when Client Certificate       #
  // authentication is used.                                                      #
  //###############################################################################
  // Client Certificate header name
  CLIENT_CERT_HEADER_NAME = "x-forwarded-client-cert";

  BEGIN_CERT_SEPARATOR = "-----BEGIN CERTIFICATE-----";

  END_CERT_SEPARATOR = "-----END CERTIFICATE-----";

  ADMISSION_VALID_ROLES = ['devel', 'admin' // TODO: move somewhere else
  ];

  // Parses the contents of a X-Forwarded-Client-Cert (XFCC) header and returns an
  // object with the header information.
  // - if the string is in PEM format, the object will have the following keys:
  //   - 'cert': contains the received PEM format
  //   - 'chain': contains the received PEM format
  // - if the string is in XFCC format, the object will have the same keys as the
  //   received XFCC header (typically, 'cert, 'chain' and 'hash').

  // XFCC headers containing more than one XFCC element are not accepted.

  // X-Forwarded-Client-Cert specification:
  // https://www.envoyproxy.io/docs/envoy/latest/configuration/http/http_conn_man/headers#x-forwarded-client-cert

  // Sample 'X-Forwarded-Client-Cert' header in PEM format.
  // It contains a simple string with the URL-encoded PEM certificate.
  // SAMPLE_CERTIFICATE_HEADER="-----BEGIN%20CERTIFICATE-----%0AMIIDCTCC[...]BTRefw%3D%3D%0A-----END%20CERTIFICATE-----%0A"

  // Sample 'X-Forwarded-Client-Cert' header in XFCC-compliant format.
  // It contains an object with multiple key (Hash, Cert, Chain, etc.).
  // New lines added for better reading
  // SAMPLE_CERTIFICATE_HEADER_LATEST="
  //   Hash=85884bfc867f7049061cf6d44ca5a814e628846dc496891c53751e0ff2d054d8;\
  //   Cert="-----BEGIN%20CERTIFICATE-----%0AMIIDdzCCAl[...]dWnek3pa%0A-----END%20CERTIFICATE-----%0A";\
  //   Chain="-----BEGIN%20CERTIFICATE-----%0AMIIDdzCCAl[...]dWnek3pa%0A-----END%20CERTIFICATE-----%0A-----BEGIN%20CERTIFICATE-----%0AMIIDMT[...]ADeJC4B4%3D%0A-----END%20CERTIFICATE-----%0A"
  // "
  parseXFCCHeader = function (xfccHeader) {
    var equalSignPosition, errMsg, headerParts, key, meth, newPart, parsedHeader, value;
    meth = "MKCC.parseXFCCHeader";
    //console.log "#{meth} - XFCC Header: \nxfccHeader"

    // The XFCC header specification allows multiple proxies to append an XFCC
    // element, separated by commas. Admission will not allow headers with more
    // than one XFCC element.
    if (xfccHeader.indexOf(',') !== -1) {
      errMsg = 'Certificate header has more than one XFCC elements';
      console.log(`${meth} - ERROR: ${errMsg}.`);
      throw new Error(errMsg);
    }
    // If no semicolon (;) is found in the header, the it is a "simple" raw
    // certificate (with no 'Cert', 'Chain', 'Hash' properties)
    if (xfccHeader.indexOf(';') === -1) {
      console.log(`${meth} - Certificate header has no semicolons, treating it as a raw PEM certificate.`);
      parsedHeader = {
        cert: xfccHeader,
        chain: xfccHeader
      };
    } else {
      console.log(`${meth} - Certificate header with semicolons, treating it as an XFCC certificate.`);
      parsedHeader = {};
      headerParts = xfccHeader.split(';');
      // console.log "Parts        : #{headerParts}"
      console.log(`${meth} - Certificate has ${headerParts.length} fields.`);
      while (headerParts.length > 0) {
        newPart = headerParts.shift();
        // console.log "Processing part : #{newPart}"

        // Each certificate field is composed as 'key=value'
        equalSignPosition = newPart.indexOf('=');
        key = newPart.substring(0, equalSignPosition);
        value = newPart.substring(equalSignPosition + 1);
        // console.log " Key   : #{key}"
        // console.log " Value : #{value}"

        // If values come double-quoted, remove the quotes
        if (value.startsWith("\"") && value.endsWith("\"")) {
          value = value.substring(1, value.length - 1);
        }
        // console.log " Value: #{value}"

        // Add the new key to the parsed object (lowercase the key)
        parsedHeader[key.toLowerCase()] = value;
      }
      console.log(`${meth} - Successfully parsed XFCC header.`);
    }
    return parsedHeader;
  };

  MockKeycloakClientCert = class MockKeycloakClientCert {
    constructor(trustedCAStr = '', clusterRefDomain1, validateWithTrustedCA1 = false) {
      this.trustedCAStr = trustedCAStr;
      this.clusterRefDomain = clusterRefDomain1;
      this.validateWithTrustedCA = validateWithTrustedCA1;
      this.trustedCA = null;
    }

    init() {
      var err;
      // If certificate validation is enabled, trusted CA is required
      if (this.validateWithTrustedCA) {
        if (this.trustedCAStr != null && this.trustedCAStr !== '') {
          try {
            this.trustedCA = new crypto.X509Certificate(this.trustedCAStr);
            console.log('MKCC - Converted trusted CA to X509.');
            return true;
          } catch (error) {
            err = error;
            console.log(`MKCC - ERROR: converting trusted CA to X509. Error: ${err.message || err}`);
            throw new Error('Invalid trusted CA certificate information.');
          }
        } else {
          console.log('MKCC - ERROR: missing mandatory configuration parameter: trusted CA (CA validation is enabled).');
          throw new Error('Missing mandatory configuration parameter: trusted CA (CA validation is enabled).');
        }
      } else {
        return console.log('MKCC - CA validation is disabled.');
      }
    }

    protect(protectedScope) {
      return (req, res, next) => {
        var acceptedRoles, accessGranted, allChainOK, auxCert, auxCert2, auxCertStr, auxClientCert, auxStr, auxStrArr, certPosition, chainX509ClientCerts, clientCert, clientCertCN, clientCertCNDomain, clientCertCNUsername, clientCertHeader, clientCertOU, clientCertRole, clientCertStr, cnSegments, decodedClientCertHeader, err, i, j, k, len, parsedXFCCHeader, ref, ref1, refDomainSegments, subRefDomain, token;
        // Check that the request contains a client cert header and get it.
        // Otherwise, reject the request.
        if (!(CLIENT_CERT_HEADER_NAME in req.headers) || req.headers[CLIENT_CERT_HEADER_NAME].trim() === '') {
          console.log("MKCC - ERROR: Client Certificate information not available.");
          console.log(`MKCC - Request headers: ${Object.keys(req.headers)}`);
          console.log(`MKCC - Expected header: ${CLIENT_CERT_HEADER_NAME}`);
          res.status(496).send('Client Certificate information not available.');
          return;
        }
        clientCertHeader = req.headers[CLIENT_CERT_HEADER_NAME];
        console.log(`MKCC - Got certificate header: ${clientCertHeader}`);
        decodedClientCertHeader = decodeURIComponent(clientCertHeader);
        console.log(`MKCC - Decoded certificate header: ${decodedClientCertHeader}`);
        try {
          parsedXFCCHeader = parseXFCCHeader(decodedClientCertHeader);
        } catch (error) {
          // console.log "MKCC - Parsed XFCC header: #{JSON.stringify parsedXFCCHeader}"
          err = error;
          console.log(`MKCC - ERROR: parsing Certificate header. Error: ${err.message || err}`);
          res.status(495).send('Unable to parse Certificate header.');
          return;
        }
        // If the received certificate must be validated against the configured
        // trusted CA, the certificate chain must be validated.
        if (this.validateWithTrustedCA) {
          console.log("MKCC - Validating certificate against the trusted CA...");
          // Create X509 certificates for all certificates in the parsed XFCC object
          // 'chain' field to validate

          // The process splits the string with the X509 knwown certificate separators,
          // then rebuilds them individually for creating an X509 cert for each.
          auxStr = parsedXFCCHeader.chain;
          auxStrArr = auxStr.split(BEGIN_CERT_SEPARATOR);
          // console.log "MKCC - Cert Array: #{auxStrArr.length}"
          // console.log "MKCC - Cert Array: #{auxStrArr[0]}"
          // console.log "MKCC - Cert Array: #{auxStrArr[1]}"
          // console.log "MKCC - Cert Array: #{auxStrArr[2]}"
          auxStr = auxStrArr.join(';');
          // console.log "MKCC - Cert Array REJOINED: #{auxStr}"
          auxStrArr = auxStr.split(END_CERT_SEPARATOR);
          // console.log "MKCC - Cert Array: #{auxStrArr.length}"
          // console.log "MKCC - Cert Array: #{auxStrArr[0]}"
          // console.log "MKCC - Cert Array: #{auxStrArr[1]}"
          // console.log "MKCC - Cert Array: #{auxStrArr[2]}"
          auxStr = auxStrArr.join(';');
          console.log(`MKCC - Cert Array REJOINED: ${auxStr}`);
          // Process the final token resulting of the splits
          chainX509ClientCerts = [];
          auxStrArr = auxStr.split(';');
          certPosition = -1;
          for (j = 0, len = auxStrArr.length; j < len; j++) {
            token = auxStrArr[j];
            if (token.trim() === "") {
              // Ignore empty tokens (spaces, newlines, etc)
              continue;
            } else {
              certPosition++;
              // For each non-empty token, build an X509 certificate object
              // console.log "MKCC - TOKEN (#{certPosition}): #{token}"
              auxCertStr = `${BEGIN_CERT_SEPARATOR}\n${token.trim()}\n${END_CERT_SEPARATOR}\n`;
              console.log(`MKCC - auxCertStr (${certPosition}): ${auxCertStr}`);
              try {
                auxClientCert = new crypto.X509Certificate(auxCertStr);
                console.log(`MKCC - Created X509 certificate from chain element number ${certPosition}.`);
                chainX509ClientCerts.push(auxClientCert);
              } catch (error) {
                err = error;
                console.log(`MKCC - ERROR: creating X509 certificate from chain element number ${certPosition}. Error: ${err.message || err}`);
                res.status(495).send(`Invalid Client Certificate information received (position ${certPosition}).`);
                return;
              }
            }
          }
          console.log(`MKCC - Built list of X509 certificates. Total: ${chainX509ClientCerts.length}`);
          // console.log "MKCC - X509 Certificates: #{chainX509ClientCerts}"
          if (chainX509ClientCerts.length === 0) {
            console.log("MKCC - ERROR: no X509 certificates have been created.");
            res.status(495).send("No Client Certificates found.");
            return;
          }
          // Check that each certificate of the chain, except for the last, is
          // issued by the subsequent one
          console.log("MKCC - Checking the chain signatures (except the last certificate)");
          console.log(`MKCC - X509 Cert Array length: ${chainX509ClientCerts.length}`);
          // If there is more than one certificate in the chain, validate that each
          // is signed by the following.
          if (chainX509ClientCerts.length >= 2) {
            // Loop from the first element to the one before the last
            console.log(`MKCC - Looping from element 0 to element ${chainX509ClientCerts.length - 2}`);
            allChainOK = true;
            for (i = k = 0, ref = chainX509ClientCerts.length - 2; 0 <= ref ? k <= ref : k >= ref; i = 0 <= ref ? ++k : --k) {
              // console.log "** ITERATION #{i} ***"
              auxCert = chainX509ClientCerts[i];
              auxCert2 = chainX509ClientCerts[i + 1];
              // console.log "MKCC - chainCert #{i} subject: #{chainX509ClientCerts[i].subject}"
              // console.log "MKCC - chainCert #{i} issuer: #{chainX509ClientCerts[i].issuer}"
              // console.log "MKCC - chainCert #{i+1} subject: #{chainX509ClientCerts[i+1].subject}"
              // console.log "MKCC - chainCert #{i+1} issuer: #{chainX509ClientCerts[i+1].issuer}"
              if (chainX509ClientCerts[i].checkIssued(chainX509ClientCerts[i + 1])) {
                console.log(`MKCC - Chain validation succeeded: certificate ${i} is issued by certificate ${i + 1}`);
              } else {
                console.log(`MKCC - ERROR: Chain validation failed: certificate ${i} is not issued by certificate ${i + 1}`);
                allChainOK = false;
              }
            }
            if (allChainOK) {
              console.log("MKCC - All chain (except last element) has been successfully validated.");
            } else {
              console.log("MKCC - ERROR: some chain validations failed.");
              res.status(495).send("Client Certificate intermediate chain validation failed.");
              return;
            }
          }
          // Ensure that the last certificate in the chain is signed by the
          // expected Root CA.
          console.log("");
          console.log("MKCC - Checking the last certificate of the chain against the trusted CA");
          console.log("");
          if (chainX509ClientCerts[chainX509ClientCerts.length - 1].checkIssued(this.trustedCA)) {
            console.log("MKCC - Chain validation succeeded: last certificate is issued by the trusted CA");
          } else {
            console.log("MKCC - ERROR: Chain validation failed: last certificate is not issued by a trusted CA");
            res.status(495).send("Client Certificate chain validation failed: last certificate not issued by a trusted CA");
            return;
          }
          // All issuer validations passed, start validation the main certificate data
          console.log("MKCC - All issuer validations passed, starting validation the main certificate data...");
        } else {
          console.log("MKCC - Skipping issuer CA validation (disabled).");
        }
        // Get the main (leaf) client certificatefor extracting user information
        clientCertStr = parsedXFCCHeader.cert;
        try {
          clientCert = new crypto.X509Certificate(clientCertStr);
          console.log("MKCC - Created X509 certificate from the client certificate.");
        } catch (error) {
          err = error;
          console.log(`MKCC - ERROR: creating X509 certificate from the client certificate. Error: ${err.message || err}`);
          res.status(495).send("Invalid Client Certificate information received (main cert).");
          return;
        }
        console.log("*********************************************");
        console.log("Certificate data:");
        console.log(`- SUBJECT CN : ${clientCert.toLegacyObject().subject.CN}`);
        console.log(`- SUBJECT OU : ${clientCert.toLegacyObject().subject.OU}`);
        console.log(`- ISSUER     : ${clientCert.issuer}`);
        // console.log "- SUBJECT : #{clientCert.subject}"
        console.log("*********************************************");
        clientCertCN = clientCert.toLegacyObject().subject.CN;
        clientCertOU = clientCert.toLegacyObject().subject.OU;
        clientCertRole = clientCertOU;
        // Check that the OU section of the subject contains a valid role
        if (indexOf.call(ADMISSION_VALID_ROLES, clientCertRole) < 0) {
          console.log(`MKCC - ERROR: client certificate has an unknow role (${clientCertRole}).`);
          res.status(495).send('Client Certificate has an unknown role.');
          return;
        }
        // Access grant logic is the following:
        // - user is granted access if and only if:
        //   - CN is "<username>.<clusterReferenceDomain>"
        //   - CN is "<username>.<subdomain_of_clusterReferenceDomain>"

        // Extract username and granted domain from CN section of the subject
        cnSegments = clientCertCN.split('.');
        if (cnSegments.length < 2) {
          console.log("MKCC - ERROR: client certificate CN format is wrong.");
          res.status(495).send('Client Certificate has wrong subject format (CN).');
          return;
        }
        // First element is the username. Keep it and remove it from the segments array.
        // The remaining will be the granted access domain.
        // clientCertCNUsername = cnSegments[0]
        clientCertCNUsername = cnSegments.shift();
        // The remaining elements are the CN domain
        clientCertCNDomain = cnSegments.join('.');
        console.log("*********************************************");
        console.log(`Cert CN Username : ${clientCertCNUsername}`);
        console.log(`Cert CN Domain   : ${clientCertCNDomain}`);
        console.log(`Cert CN Role     : ${clientCertRole}`);
        console.log("*********************************************");
        accessGranted = false;
        if (clientCertCNDomain === this.clusterRefDomain) {
          console.log("MKCC - Cert CN Domain matches cluster RefDomain. Access is granted to this cluster.");
          accessGranted = true;
        } else {
          // Tokenize the Reference Domain to easier comparing
          refDomainSegments = this.clusterRefDomain.split('.');
          while (refDomainSegments.length > 1) {
            // Remove the first level and rebuild a domain string
            refDomainSegments.shift();
            subRefDomain = refDomainSegments.join('.');
            // console.log "--- loop ---"
            // console.log "Cert CN Username : #{clientCertCNUsername}"
            // console.log "Cert CN Domain   : #{clientCertCNDomain}"
            // console.log "Reference Domain : #{subRefDomain}"
            if (clientCertCNDomain === subRefDomain) {
              console.log(`MKCC - Cert CN Domain matches a cluster RefDomain subdomain (${subRefDomain}). Access is granted to this cluster.`);
              accessGranted = true;
            }
          }
        }
        if (!accessGranted) {
          console.log("MKCC - ERROR: access NOT granted for this cluster.");
          res.status(495).send('Client Certificate is not granted for this cluster.');
          return;
        } else {
          console.log("MKCC - Access granted for this cluster.");
        }
        if (protectedScope === 'admission:administrator') {
          // User must have role 'admin' to access this section since it is
          // protected for 'admission:administrator' only
          acceptedRoles = ['admin'];
        } else {
          acceptedRoles = ['admin', 'devel'];
        }
        console.log(`MKCC - Accepted roles are: ${acceptedRoles}`);
        if (ref1 = clientCertRole.toLowerCase(), indexOf.call(acceptedRoles, ref1) < 0) {
          console.log("MKCC - ERROR: user has not the required roles.");
          res.status(495).send('Client Certificate has not the required roles.');
          return;
        } else {
          console.log(`MKCC - Access granted for user ${clientCertCNUsername} with role ${clientCertRole}.`);
        }
        // If we have gotten this far, everything is OK!

        // Add header to the request with the user information in the same format
        // Keycloak middleware would
        req.clientcertauth = {
          user: {
            id: clientCertCNUsername,
            username: clientCertCNUsername,
            name: clientCertCNUsername,
            email: '',
            groups: [],
            kumoriGroups: [clientCertRole],
            roles: [clientCertRole],
            limits: null
          }
        };
        return next();
      };
    }

    middleware() {
      return function (req, res, next) {
        req.session['keycloak-token'] = 'anonymous';
        return next();
      };
    }

  };

  MockKeycloakClientCertForLogin = class MockKeycloakClientCertForLogin {
    constructor() {}

    init() {}

    protect(protectedScope) {
      return function (req, res, next) {
        return res.status(404).send('Not Found (disabled)');
      };
    }

    middleware() {
      return function (req, res, next) {
        req.session['keycloak-token'] = 'anonymous';
        return next();
      };
    }

  };

  MockKeycloakClientCertUserManager = class MockKeycloakClientCertUserManager {
    constructor() {}

    init() {}

    terminate() {}

    removeFinalizers() {
      return q();
    }

    getUsers() {
      return q([]);
    }

    getUser() {
      return q({});
    }

    getGroups() {
      return q([]);
    }

    deleteUser() {
      return q();
    }

    createUser() {
      return q();
    }

    updateUser() {
      return q();
    }

    updateUserPassword() {
      return q();
    }

  };

  //###############################################################################
  //###############################################################################
  AdmissionServer = class AdmissionServer {
    constructor(config) {
      var base, base1, meth;
      this.gracefulShutdown = this.gracefulShutdown.bind(this);
      meth = 'AdmissionServer.constructor';
      // if config?.admission?.authentication?
      //   console.log "#{meth} - Disabling Keycloak (remove Keycloak configuration)"
      //   delete config.admission.authentication

      // console.log "#{meth} - CONFIG: #{JSON.stringify config, null, 2}"
      this.config = config || {};
      if (this.config.port != null) {
        console.log(`${meth} - Using port ${this.config.port} (from configuration)`);
      } else if (ADMISSION_PORT_ENV_VAR in process.env) {
        this.config.port = process.env[ADMISSION_PORT_ENV_VAR];
        console.log(`${meth} - Using port ${this.config.port} (from ENV variable)`);
      } else {
        if ((base = this.config).port == null) {
          base.port = DEFAULT_PORT;
        }
        console.log(`${meth} - Using port ${this.config.port} (default)`);
      }
      if ((base1 = this.config).admission == null) {
        base1.admission = DEFAULT_ADM_CONFIG;
      }
      this.app = null;
      this.server = null;
      this.admissionRestApi = null;
      this.wsp = null;
    }

    init() {
      var admCfg, clusterRefDomain, keycloak, keycloakConfig, keycloakConfigLogin, keycloakLogin, keycloakRequester, keycloakUserManager, memoryStore, meth, mockPlanner, multerStorage, ref, ref1, trustedCA, upload, validateWithTrustedCA;
      meth = 'AdmissionServer.init';
      console.log(meth);
      console.log(`${meth} - Setting up default error handlers...`);
      // Configure default unexpected error handlers
      process.on('unhandledRejection', (reason, p) => {
        console.log('************************************************************');
        console.log(`UnhandledRejection: reason=${reason} at Promise`, p);
        return console.log('************************************************************');
      }).on('uncaughtException', err => {
        console.log('************************************************************');
        console.log("UNCAUGHT EXCEPTION: ", err);
        console.log('************************************************************');
        return process.exit(1);
      });
      console.log(`${meth} - Default error handlers set!`);
      // Configure Multer upload
      multerStorage = multer.diskStorage({
        destination: '/tmp',
        filename: function (req, file, cb) {
          var name;
          name = file.fieldname + '-' + utils.generateId() + path.extname(file.originalname);
          return cb(null, name);
        }
      });
      upload = multer({
        storage: multerStorage
      }).any();
      console.log(`${meth} - Creating Express server...`);
      this.app = express();
      this.expressWs = expressWs(this.app);
      this.app.use(bodyParser.json());
      this.app.use(bodyParser.urlencoded({
        extended: true
      }));
      this.app.use(cors());
      this.app.use(upload);
      // Custom middleware for setting in Websocket connections authentication
      // headers as expècted by Keycloak middleware. Some client libraries or
      // languages have problems setting headers for websocket connections.
      this.app.use(this.websocketAuthHelper);
      // Create a session store to be used by both the express-session middleware
      // and the keycloak middleware.
      memoryStore = new session.MemoryStore();
      this.app.use(session({
        secret: 'some secret',
        resave: false,
        saveUninitialized: true,
        store: memoryStore
      }));
      keycloak = null;
      keycloakLogin = null;
      keycloakRequester = null;
      if (((ref = this.config.admission.authentication) != null ? ref.keycloakConfig : void 0) != null) {
        console.log(`${meth} - Authentication type: Keycloak.`);
        keycloakConfig = this.config.admission.authentication.keycloakConfig;
        // console.log "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
        // console.log "KEYCLOAK CONFIG:"
        // console.log "#{JSON.stringify keycloakConfig, null, 2}"
        // console.log "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"

        // Provide the session store to the Keycloak so that sessions
        // can be invalidated from the Keycloak console callback.

        // If no configuration is passed, it is assumed that a kycloak.json file
        // exists in root dir.
        keycloak = new Keycloak({
          store: memoryStore
        }, keycloakConfig);
        keycloakConfigLogin = _.cloneDeep(keycloakConfig);
        keycloakConfigLogin.bearerOnly = false;
        // keycloakConfigLogin['enable-cors'] = true
        // keycloakConfigLogin['public-client'] = true
        // keycloakConfigLogin['cors'] = true
        // console.log "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
        // console.log "KEYCLOAK LOGIN CONFIG:"
        // console.log "#{JSON.stringify keycloakConfigLogin, null, 2}"
        // console.log "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
        keycloakLogin = new Keycloak({
          store: memoryStore
        }, keycloakConfigLogin);
        keycloakRequester = new KeycloakRequest(keycloakConfig);
        keycloakUserManager = new KeycloakUserManager(keycloakConfig.operator);
        keycloakUserManager.init(); // synchronous call
      } else if (((ref1 = this.config.admission.authentication) != null ? ref1.clientCertificateConfig : void 0) != null) {
        console.log(`${meth} - Authentication type: Client Certificate validation. Config: ${JSON.stringify(this.config.admission.authentication.clientCertificateConfig)}`);
        // Retrieve the necessary configuration for Client Certificate Authentication
        // NOTE: currently retrieved from Admission configuration file
        trustedCA = this.config.admission.authentication.clientCertificateConfig.trustedCA;
        clusterRefDomain = this.config.admission.authentication.clientCertificateConfig.clusterReferenceDomain;
        validateWithTrustedCA = this.config.admission.authentication.clientCertificateConfig.validateWithTrustedCA;
        keycloak = new MockKeycloakClientCert(trustedCA, clusterRefDomain, validateWithTrustedCA);
        keycloak.init(); // synchronous call
        keycloakLogin = new MockKeycloakClientCertForLogin();
        keycloakLogin.init(); // synchronous call
        keycloakUserManager = new MockKeycloakClientCertUserManager();
        keycloakUserManager.init(); // synchronous call
      } else {
        console.log(`${meth} - IMPORTANT: Authentication will be disabled (no authentication was provided).`);
        keycloak = new MockKeycloak();
        keycloakLogin = new MockKeycloak();
        keycloakUserManager = new MockKeycloakUserManager();
      }
      // keycloakRequester = new MockKeycloakRequester()

      // keycloak.init { flow: "hybrid" }

      // Specifies that the user-accessible application URL to logout should be
      // mounted at /logout
      // Specifies that Keycloak console callbacks should target the root URL.
      this.app.use(keycloak.middleware({
        logout: '/logout',
        admin: '/'
      }));
      this.app.use(keycloakLogin.middleware({
        logout: '/logout',
        admin: '/'
      }));
      // Create Admission Rest API
      admCfg = this.config.admission || DEFAULT_ADM_CONFIG;
      mockPlanner = {};
      this.admissionRestApi = new AdmissionRestAPI(admCfg, mockPlanner, keycloak, keycloakLogin, keycloakUserManager);
      this.receivedPlannerEventList = [];
      this.receivedAdmEventList = [];
      this.receivedResEventList = [];
      // AdmissionRestApi Event handlers
      this.admissionRestApi.on('planner', evt => {
        return this.receivedPlannerEventList.push(evt);
      });
      this.admissionRestApi.on('admission', evt => {
        return this.receivedAdmEventList.push(evt);
      });
      this.admissionRestApi.on('resource', evt => {
        return this.receivedResEventList.push(evt);
      });
      // Initialize Admission Rest API
      return this.admissionRestApi.init().then(() => {
        this.app.use('/admission', this.admissionRestApi.getRouter());
        this.app.use('/management', this.admissionRestApi.getManagementRouter());
        this.app.use('/auth', this.admissionRestApi.getLoginRouter());
        // Basic error handler
        this.app.use(function (req, res, next) {
          return res.status(404).send('Not Found');
        });
        this.app.use((err, req, res, next) => {
          console.error(`EXPRESS ERROR: ${err.stack}`);
          return next(err);
        });
        // @server = http.createServer(@app)
        // Create Websocket publisher connected to the HTTP server
        // @wsp = new WebsocketPublisher @server, keycloakRequester
        console.log(`${meth} - AdmissionServer listening at port ${this.config.port}`);
        // @server.listen @config.port
        this.server = this.app.listen(this.config.port);
        // Set OS signal handler (SIGINT and SIGTERM)
        process.on('SIGTERM', this.gracefulShutdown);
        process.on('SIGINT', this.gracefulShutdown);
        return true;
      }).fail(err => {
        console.log(`${meth} Error: ${err.message || err}`);
        console.log(`${err.stack}`);
        return true;
      }).done();
    }

    // For Websocket connections, get AccessToken from request query parameters
    // and set it as 'Authorization' header.
    websocketAuthHelper(req, res, next) {
      var accessToken, meth, ref;
      meth = 'AdmissionServer.websocketAuthHelper';
      // If request has a WebSocket object, 'upgrade' property exists and is true,
      // has no 'authorization' header set and request query contains access token
      // info, get accessToken from query parameters and set it in request headers.
      if (req.upgrade && req.ws != null && !('authorization' in req.headers) && ((ref = req.query) != null ? ref.access_token : void 0) != null) {
        console.log(`${meth} - Websocket request detected with no auth header and access token data in query. Setting authorization header.`);
        accessToken = req.query.access_token || "";
        req.headers['authorization'] = `bearer ${accessToken}`;
        req.rawHeaders.push('Authorization');
        req.rawHeaders.push(`bearer ${accessToken}`);
      }
      return next();
    }

    gracefulShutdown() {
      console.log('Starting graceful shutdown process...');
      console.log('- Stopping Express...');
      return this.server.close(() => {
        console.log('  Express server is shut down.');
        console.log('- Terminating internal objects...');
        return this.admissionRestApi.terminate().then(function () {
          console.log("\nGraceful shutdown completed.");
          console.log("Exiting...");
          return process.exit(1);
        }).catch(function (err) {
          return console.log(`  ERROR: ${err.message} ${err.stack}`);
        });
      });
    }

  };

  //###############################################################################
  //#  TEST CODE ##
  //###############################################################################
  DEFAULT_ADM_CONFIG_FILE = __dirname + '/../config/admission-server-config.json';

  admConfig = null;

  configFile = null;

  args = parseArgs(process.argv.slice(2));

  if (!('f' in args)) {
    console.log("\nWARNING: No configuration file provided, using default. \n         You can specify a configuration file with the following flag: \n            -f <path_to_config_file>\n");
    configFile = DEFAULT_ADM_CONFIG_FILE;
  } else {
    console.log(`\nINFO: using configuration file in ${args.f}\n`);
    configFile = args.f;
  }

  console.log(`Trying to read default configuration file: ${configFile}`);

  try {
    admConfig = require(configFile);
  } catch (error) {
    err = error;
    console.log("Default configuration file could not be loaded.");
    admConfig = null;
  }

  admServer = new AdmissionServer(admConfig);

  admServer.init();
}).call(undefined);