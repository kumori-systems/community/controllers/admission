'use strict';

(function () {
  /*
  * Copyright 2021 Kumori Systems S.L.
   * Licensed under the EUPL, Version 1.2 or – as soon they
    will be approved by the European Commission - subsequent
    versions of the EUPL (the "Licence");
   * You may not use this work except in compliance with the
    Licence.
   * You may obtain a copy of the Licence at:
     https://joinup.ec.europa.eu/software/page/eupl
   * Unless required by applicable law or agreed to in
    writing, software distributed under the Licence is
    distributed on an "AS IS" basis,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
    express or implied.
   * See the Licence for the specific language governing
    permissions and limitations under the Licence.
   */
  var ReschedulerDescheduler, ReschedulerFactory;

  ReschedulerDescheduler = require('./rescheduler-descheduler');

  ReschedulerFactory = class ReschedulerFactory {
    // Static method to create new Manifest Stores
    static create(type, options) {
      switch (type) {
        case 'descheduler':
          return new ReschedulerDescheduler(options);
        default:
          throw new Error(`Invalid Rescheduler type: '${type}'`);
      }
    }

  };

  module.exports = ReschedulerFactory;
}).call(undefined);