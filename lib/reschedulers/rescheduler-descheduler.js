'use strict';

(function () {
  /*
  * Copyright 2021 Kumori Systems S.L.
   * Licensed under the EUPL, Version 1.2 or – as soon they
    will be approved by the European Commission - subsequent
    versions of the EUPL (the "Licence");
   * You may not use this work except in compliance with the
    Licence.
   * You may obtain a copy of the Licence at:
     https://joinup.ec.europa.eu/software/page/eupl
   * Unless required by applicable law or agreed to in
    writing, software distributed under the Licence is
    distributed on an "AS IS" basis,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
    express or implied.
   * See the Licence for the specific language governing
    permissions and limitations under the Licence.
   */
  var CONFIGMAP_TEMPLATE, DESCHEDULER_CONFIGMAP, DESCHEDULER_NAMESPACE, JOB_DESCRIPTION_LABEL, JOB_TEMPLATE, Rescheduler, ReschedulerDescheduler, TIMESTAMP_FORMAT, _, moment, q, utils;

  _ = require('lodash');

  q = require('q');

  moment = require('moment');

  Rescheduler = require('./rescheduler');

  utils = require('../utils');

  DESCHEDULER_NAMESPACE = "kube-system";

  DESCHEDULER_CONFIGMAP = "descheduler-policy-configmap";

  JOB_DESCRIPTION_LABEL = 'kumori-rescheduler-job';

  CONFIGMAP_TEMPLATE = {
    apiVersion: 'v1',
    kind: 'ConfigMap',
    metadata: {
      name: 'descheduler-job-policy-configmap-__SUFFIX__',
      namespace: 'kube-system',
      ownerReferences: [{
        apiVersion: 'batch/v1',
        kind: 'Job',
        name: 'descheduler-job-__SUFFIX__',
        uid: '',
        blockOwnerDeletion: true
      }]
    },
    data: {
      "policy.yaml": ''
    }
  };

  JOB_TEMPLATE = {
    apiVersion: 'batch/v1',
    kind: 'Job',
    metadata: {
      name: 'descheduler-job-__SUFFIX__',
      namespace: 'kube-system',
      labels: {
        description: JOB_DESCRIPTION_LABEL,
        reschedulerType: 'descheduler'
      }
    },
    spec: {
      ttlSecondsAfterFinished: 60 * 60, // One hour
      parallelism: 1,
      completions: 1,
      template: {
        metadata: {
          name: 'descheduler-job-pod-__SUFFIX__'
        },
        spec: {
          priorityClassName: 'system-cluster-critical',
          containers: [{
            name: 'descheduler',
            image: 'k8s.gcr.io/descheduler/descheduler:v0.22.0',
            volumeMounts: [{
              mountPath: '/policy-dir',
              name: 'policy-volume'
            }],
            command: ['/bin/descheduler'],
            args: ['--policy-config-file', '/policy-dir/policy.yaml', '--v', '4'],
            resources: {
              limits: {
                cpu: '500m',
                memory: '256Mi'
              },
              requests: {
                cpu: '500m',
                memory: '256Mi'
              }
            },
            securityContext: {
              allowPrivilegeEscalation: false,
              capabilities: {
                drop: -'ALL'
              },
              privileged: false,
              readOnlyRootFilesystem: true,
              runAsNonRoot: true
            }
          }],
          restartPolicy: 'Never',
          serviceAccountName: 'descheduler-sa',
          terminationGracePeriodSeconds: 30,
          tolerations: [{
            effect: 'NoSchedule',
            key: 'node-role.kubernetes.io/master',
            operator: 'Exists'
          }, {
            effect: 'NoExecute',
            key: 'node.kubernetes.io/not-ready',
            operator: 'Exists',
            tolerationSeconds: 300
          }, {
            effect: 'NoExecute',
            key: 'node.kubernetes.io/unreachable',
            operator: 'Exists',
            tolerationSeconds: 300
          }],
          volumes: [{
            name: 'policy-volume',
            configMap: {
              name: 'descheduler-job-policy-configmap-__SUFFIX__'
            }
          }]
        }
      }
    }
  };

  TIMESTAMP_FORMAT = 'YYYYMMDDHHmmss';

  ReschedulerDescheduler = class ReschedulerDescheduler extends Rescheduler {
    // Expected configuration options:
    //   options:
    //     k8sApiStub: <API stub object>
    constructor(options) {
      super('descheduler');
      this.k8sApiStub = options.k8sApiStub;
    }

    //#############################################################################
    //#                            PUBLIC METHODS                                ##
    //#############################################################################

    // Returns a promise
    init() {
      return q(true);
    }

    // Returns a promise
    terminate() {
      return q(true);
    }

    // Creates a rescheduling job and returns a promise
    createJob(jobConfig = {}) {
      var meth, newPolicy, timestamp;
      meth = 'ReschedulerDescheduler.createJob';
      this.logger.info(`${meth} - New job config: ${JSON.stringify(jobConfig)}`);
      this.validateJobConfig(jobConfig);
      this.logger.info(`${meth} - New job config validated.`);
      // Create a timestamp to use as suffix
      timestamp = utils.getTimestamp(TIMESTAMP_FORMAT);
      newPolicy = null;
      // Retrieve current Descheduler configuration from the cluster
      return this.getDeschedulerPolicy().then(deschedulerPolicy => {
        var errMsg, jobManifest;
        if (deschedulerPolicy == null) {
          errMsg = "Unable to retrieve current Descheduler configuration.";
          this.logger.warn(`${meth} - ${errMsg}`);
          throw new Error(errMsg);
        }
        this.logger.debug(`${meth} - Found descheduler policy: ${utils.yamlStringify(deschedulerPolicy)}`);
        // Create a new policy by combining the cluster policy and the provided
        // job configuration
        this.logger.info(`${meth} - Creating new policy...`);
        newPolicy = this.mergePolicies(deschedulerPolicy, jobConfig);
        this.logger.info(`${meth} - New policy: ${utils.yamlStringify(newPolicy)}`);
        jobManifest = this.prepareJobManifest(timestamp);
        this.logger.info(`${meth} - Creating Job...`);
        return this.createK8sJob(jobManifest);
      }).then(jobObject => {
        var configMapManifest, jobUID;
        this.logger.info(`${meth} - Created Job.`);
        jobUID = jobObject.metadata.uid;
        this.logger.info(`${meth} - Job UID: ${jobUID}`);
        configMapManifest = this.prepareConfigMapManifest(newPolicy, timestamp, jobUID);
        this.logger.info(`${meth} - Creating ConfigMap...`);
        return this.createK8sConfigMap(configMapManifest);
      }).then(configMapObject => {
        this.logger.info(`${meth} - Created ConfigMap.`);
        return true;
      });
    }

    // List existing rescheduling jobs (independently of their state)
    listJobs() {
      var meth;
      meth = 'ReschedulerDescheduler.listJob';
      this.logger.info(`${meth} - Getting Job list`);
      return this.getK8sJobs().then(jobList => {
        this.logger.info(`${meth} - Retrieved list of descheduler Jobs.`);
        return jobList;
      });
    }

    //#############################################################################
    //#                           PRIVATE METHODS                                ##
    //#############################################################################
    prepareJobManifest(suffix) {
      var jobManifest, meth;
      meth = 'ReschedulerDescheduler.prepareJobManifest';
      this.logger.info(`${meth}`);
      jobManifest = _.cloneDeep(JOB_TEMPLATE);
      jobManifest.metadata.name = jobManifest.metadata.name.replace('__SUFFIX__', suffix);
      jobManifest.spec.template.metadata.name = jobManifest.spec.template.metadata.name.replace('__SUFFIX__', suffix);
      jobManifest.spec.template.spec.volumes[0].configMap.name = jobManifest.spec.template.spec.volumes[0].configMap.name.replace('__SUFFIX__', suffix);
      this.logger.debug(`${meth} - Job manifest: \n${utils.yamlStringify(jobManifest)}`);
      return jobManifest;
    }

    prepareConfigMapManifest(policy, suffix, jobUID) {
      var configMapManifest, meth;
      meth = 'ReschedulerDescheduler.prepareConfigMapManifest';
      this.logger.info(`${meth}`);
      // Prepare ConfigMap manifest
      configMapManifest = _.cloneDeep(CONFIGMAP_TEMPLATE);
      configMapManifest.metadata.name = configMapManifest.metadata.name.replace('__SUFFIX__', suffix);
      configMapManifest.metadata.ownerReferences[0].name = configMapManifest.metadata.ownerReferences[0].name.replace('__SUFFIX__', suffix);
      configMapManifest.metadata.ownerReferences[0].uid = jobUID;
      configMapManifest.data['policy.yaml'] = utils.yamlStringify(policy);
      this.logger.debug(`${meth} - ConfigMap manifest: \n${utils.yamlStringify(configMapManifest)}`);
      return configMapManifest;
    }

    // SAMPLE CLUSTER CONFIGURATION

    // kind: DeschedulerPolicy
    // evictLocalStoragePods: false
    // evictSystemCriticalPods: false
    // ignorePvcPods: false
    // nodeSelector: kumori/role=worker
    // strategies:
    //   RemoveDuplicates:
    //     enabled: false
    //   LowNodeUtilization:
    //     enabled: false
    //     params:
    //       thresholdPriorityClassName: kumori-platform
    //       nodeResourceUtilizationThresholds:
    //         thresholds:
    //           cpu: 30
    //           memory: 30
    //           pods: 30
    //         targetThresholds:
    //           cpu: 60
    //           memory: 60
    //           pods: 60
    //   HighNodeUtilization:
    //     enabled: false
    //   RemovePodsViolatingInterPodAntiAffinity:
    //     enabled: false
    //   RemovePodsViolatingNodeAffinity:
    //     enabled: false
    //   RemovePodsViolatingNodeTaints:
    //     enabled: false
    //   RemovePodsViolatingTopologySpreadConstraint:
    //     enabled: true
    //     params:
    //       includeSoftConstraints: true
    //       nodeFit: true
    //       thresholdPriorityClassName: kumori-platform
    //       namespaces:
    //         include:
    //           - kumori
    //   RemovePodsHavingTooManyRestarts:
    //     enabled: false
    //   PodLifeTime:
    //     enabled: false

    // SAMPLE JOB CONFIG

    // {
    //   "lowNodeUtilization": {
    //     "enabled": false,
    //     "threshold": {
    //       "cpu": 30,
    //       "memory": 30,
    //       "pods": 30
    //     },
    //     "target": {
    //       "cpu": 60,
    //       "memory": 60,
    //       "pods": 60
    //     }
    //   },
    //   "spreadConstraintsViolation": {
    //     "enabled": true
    //   }
    // }
    mergePolicies(deschedulerPolicy, jobConfig = {}) {
      var ref, ref1, ref2, ref3;
      // If custom job config is empty return the original one untouched
      if (Object.keys(jobConfig).length === 0) {
        return deschedulerPolicy;
      }
      // Combine both objects. BE CAREFUL WITH CASE DIFFERENCES !
      if (((ref = jobConfig.lowNodeUtilization) != null ? ref.enabled : void 0) != null) {
        deschedulerPolicy.strategies.LowNodeUtilization.enabled = jobConfig.lowNodeUtilization.enabled;
      }
      if (((ref1 = jobConfig.lowNodeUtilization) != null ? ref1.threshold : void 0) != null) {
        deschedulerPolicy.strategies.LowNodeUtilization.params.nodeResourceUtilizationThresholds.thresholds = jobConfig.lowNodeUtilization.threshold;
      }
      if (((ref2 = jobConfig.lowNodeUtilization) != null ? ref2.target : void 0) != null) {
        deschedulerPolicy.strategies.LowNodeUtilization.params.nodeResourceUtilizationThresholds.targetThresholds = jobConfig.lowNodeUtilization.target;
      }
      if (((ref3 = jobConfig.spreadConstraintsViolation) != null ? ref3.enabled : void 0) != null) {
        deschedulerPolicy.strategies.RemovePodsViolatingTopologySpreadConstraint.enabled = jobConfig.spreadConstraintsViolation.enabled;
      }
      return deschedulerPolicy;
    }

    getDeschedulerPolicy() {
      var meth;
      meth = 'ReschedulerDescheduler.getDeschedulerPolicy';
      this.logger.info(`${meth} - Looking for Descheduler configuration...`);
      return this.k8sApiStub.getConfigMap(DESCHEDULER_NAMESPACE, DESCHEDULER_CONFIGMAP).then(configMap => {
        var parsedPolicy, policyStr, ref;
        this.logger.info(`${meth} - Found Descheduler configuration.`);
        // Extract policy from 'data."policy.yaml"' property'
        if ((configMap != null ? (ref = configMap.data) != null ? ref['policy.yaml'] : void 0 : void 0) != null) {
          policyStr = configMap.data['policy.yaml'];
          this.logger.debug(`${meth} - Descheduler policy (string): ${policyStr}`);
          parsedPolicy = this.parseYaml(policyStr);
          return parsedPolicy;
        } else {
          this.logger.warn(`${meth} - Descheduler policy not found.`);
          return null;
        }
      }).catch(err => {
        this.logger.warn(`${meth} - Descheduler configuration not found in cluster.`);
        return null;
      });
    }

    createK8sConfigMap(configMapManifest) {
      return this.k8sApiStub.createConfigMap(DESCHEDULER_NAMESPACE, configMapManifest);
    }

    createK8sJob(jobManifest) {
      return this.k8sApiStub.createJob(DESCHEDULER_NAMESPACE, jobManifest);
    }

    getK8sJobs() {
      var selector;
      selector = {
        description: JOB_DESCRIPTION_LABEL,
        reschedulerType: this.type
      };
      return this.k8sApiStub.getJobs(DESCHEDULER_NAMESPACE, selector).then(list => {
        var i, item, len;
        for (i = 0, len = list.length; i < len; i++) {
          item = list[i];
          delete item.metadata.managedFields;
        }
        return list;
      });
    }

    validateJobConfig(jobConfig) {
      // Not implemented
      return jobConfig;
    }

    parseYaml(yamlStr) {
      var e, meth, yamlObj;
      meth = 'ReschedulerDescheduler.parseYaml';
      this.logger.info(`${meth}`);
      try {
        yamlObj = utils.yamlParse(yamlStr);
        return yamlObj;
      } catch (error) {
        e = error;
        this.logger.warn(`${meth} - Couldn't parse YAML: \"${yamlStr}\". Reason: ${e.message}`);
        return null;
      }
    }

  };

  module.exports = ReschedulerDescheduler;
}).call(undefined);