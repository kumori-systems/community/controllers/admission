'use strict';

(function () {
  /*
  * Copyright 2022 Kumori Systems S.L.
   * Licensed under the EUPL, Version 1.2 or – as soon they
    will be approved by the European Commission - subsequent
    versions of the EUPL (the "Licence");
   * You may not use this work except in compliance with the
    Licence.
   * You may obtain a copy of the Licence at:
     https://joinup.ec.europa.eu/software/page/eupl
   * Unless required by applicable law or agreed to in
    writing, software distributed under the Licence is
    distributed on an "AS IS" basis,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
    express or implied.
   * See the Licence for the specific language governing
    permissions and limitations under the Licence.
   */
  var EventEmitter,
      EventStoreFactory,
      KUMORI_NAMESPACE,
      ManifestHelper,
      MockRequester,
      PlannerStub,
      q,
      utils,
      indexOf = [].indexOf;

  q = require('q');

  EventEmitter = require('events').EventEmitter;

  ManifestHelper = require('./manifest-helper');

  EventStoreFactory = require('./event-stores/event-store-factory');

  utils = require('./utils');

  MockRequester = class MockRequester {
    constructor() {}

    do() {
      return q();
    }

  };

  KUMORI_NAMESPACE = 'kumori';

  PlannerStub = function () {
    var quantityToScalar;

    class PlannerStub extends EventEmitter {
      constructor(k8sApiStub, manifestRepository, eventStoreConfig = null) {
        var esConfig, meth;
        super();
        this.k8sApiStub = k8sApiStub;
        this.manifestRepository = manifestRepository;
        meth = 'PlannerStub.constructor';
        this.logger.info(`${meth}`);
        this.requester = new MockRequester();
        this.manifestHelper = new ManifestHelper();
        this.eventStore = null;
        if ((eventStoreConfig != null ? eventStoreConfig.type : void 0) != null && eventStoreConfig.type !== '') {
          // Initialize an Events Store from configuration
          this.logger.info(`${meth} Initializing EventStore of type: ${eventStoreConfig.type}`);
          // If store is of type 'k8s' pass it the K8sApiStub object
          if (eventStoreConfig.type === 'k8s') {
            eventStoreConfig.config.k8sApiStub = this.k8sApiStub;
          }
          this.eventStore = EventStoreFactory.create(eventStoreConfig.type, eventStoreConfig.config);
        } else {
          // If no configuration is supplied, EventStore is Kubernetes itself
          this.logger.info(`${meth} Initializing default EventStore (K8s).`);
          esConfig = {
            k8sApiStub: this.k8sApiStub
          };
          this.eventStore = EventStoreFactory.create('k8s', esConfig);
        }
      }

      init() {
        return this.eventStore.init().then(() => {
          return true;
        });
      }

      terminate() {
        return this.eventStore.terminate().then(() => {
          return true;
        });
      }

      //#############################################################################
      //#                   PUBLIC METHODS TO BE USED FROM OUTSIDE                 ##
      //#############################################################################
      execSolution(solutionManifest) {
        this.logger.info('PlannerStub.execSolution()');
        return this.manifestRepository.storeManifest(solutionManifest).then(function (solutionURN) {
          return {
            solutionURN: solutionURN
          };
        });
      }

      deleteSolution(name, kind) {
        this.logger.info(`PlannerStub.deleteSolution() Name: ${name} Kind: ${kind}`);
        return this.manifestRepository.deleteManifestByNameAndKind(name, kind);
      }

      execDeployment(fullDeployment) {
        this.logger.info('PlannerStub.execDeployment()');
        return this.manifestRepository.storeManifest(fullDeployment).then(function (deploymentURN) {
          return {
            deploymentURN: deploymentURN
          };
        });
      }

      execUndeployment(name, kind) {
        this.logger.info(`PlannerStub.execUndeployment() Name: ${name} Kind: ${kind}`);
        return this.manifestRepository.deleteManifestByNameAndKind(name, kind);
      }

      linkServices(linkManifest) {
        this.logger.info('PlannerStub.linkServices()');
        // @requester.do
        //   url: "#{@url}/linkServices"
        //   message: 'linkManifest':linkManifest
        return this.manifestRepository.storeManifest(linkManifest);
      }

      unlinkServices(name) {
        this.logger.info(`PlannerStub.unlinkServices() Name: ${name}`);
        return this.manifestRepository.deleteManifestByNameAndKind(name, 'link');
      }

      isElementInUse(urn) {
        var elementK8sId, elementRef, filter, meth, used;
        meth = `PlannerStub.isElementInUse() - urn: ${urn}`;
        this.logger.info(meth);
        elementRef = this.manifestHelper.urnToRef(urn);
        elementK8sId = elementRef.name;
        // Filter on the existence of a label named as the element
        filter = {
          [`${elementK8sId}`]: '*'
        };
        used = false;
        return this.manifestRepository.listElementType('v3deployments', null, filter).then(elements => {
          if (Object.keys(elements).length > 0) {
            this.logger.info(`${meth} - Found v3deployments using the element.`);
            used = true;
          }
          return used;
        }).then(used => {
          if (!used) {
            return this.manifestRepository.listElementType('solutions', null, filter).then(elements => {
              var usedByURN;
              if (Object.keys(elements).length > 0) {
                this.logger.info(`${meth} - Found Solutions using the element.`);
                used = true;
                usedByURN = Object.values(elements)[0].urn || 'unknown';
              }
              return used;
            });
          } else {
            return used;
          }
        }).then(used => {
          return {
            inUse: used
          };
        }).catch(err => {
          var errMsg;
          errMsg = `Unable to determine if element is in use: ${err.message} ${err.stack}`;
          this.logger.error(`${meth} - ERROR: ${errMsg}`);
          return q.reject(new Error(errMsg));
        });
      }

      isResourceInUse(resourceName) {
        var filter, meth, used, usedByURN;
        meth = `PlannerStub.isResourceInUse() - Resource name: ${resourceName}`;
        this.logger.info(meth);
        // Filter on the existence of a label named as the element
        filter = {
          [`${resourceName}`]: '*'
        };
        used = false;
        usedByURN = null;
        return this.manifestRepository.listElementType('v3deployments', null, filter).then(elements => {
          if (Object.keys(elements).length > 0) {
            this.logger.info(`${meth} - Found v3deployments using the element.`);
            used = true;
            usedByURN = Object.values(elements)[0].urn || 'unknown';
          }
          return used;
        }).then(used => {
          if (!used) {
            return this.manifestRepository.listElementType('solutions', null, filter).then(elements => {
              if (Object.keys(elements).length > 0) {
                this.logger.info(`${meth} - Found Solutions using the element.`);
                used = true;
                usedByURN = Object.values(elements)[0].urn || 'unknown';
              }
              return used;
            });
          } else {
            return used;
          }
        }).then(used => {
          return {
            inUse: used,
            usedByURN: usedByURN
          };
        }).catch(err => {
          var errMsg;
          errMsg = `Unable to determine if resource is in use: ${err.message} ${err.stack}`;
          this.logger.error(`${meth} - ERROR: ${errMsg}`);
          return q.reject(new Error(errMsg));
        });
      }

      isResourceInUseBySolution(resourceID) {
        var filter, meth, used, usedByURN;
        meth = `PlannerStub.isResourceInUseBySolution() - Name: ${resourceID}`;
        this.logger.info(meth);
        // Filter on the existence of a label named as the element
        filter = {
          [`${resourceID}`]: '*'
        };
        used = false;
        usedByURN = null;
        return this.manifestRepository.listElementType('solutions', null, filter).then(elements => {
          if (Object.keys(elements).length > 0) {
            this.logger.info(`${meth} - Found solutions using the element.`);
            used = true;
            usedByURN = Object.values(elements)[0].urn || 'unknown';
          }
          return used;
        }).then(used => {
          return {
            inUse: used,
            usedByURN: usedByURN
          };
        }).catch(err => {
          var errMsg;
          errMsg = `Unable to determine if resource is in use: ${err.message} ${err.stack}`;
          this.logger.error(`${meth} - ERROR: ${errMsg}`);
          return q.reject(new Error(errMsg));
        });
      }

      listResources(filter, includePublic = false) {
        var allResources, meth, owner, validResourceKinds;
        meth = 'PlannerStub.listResources';
        if (includePublic) {
          this.logger.info(`${meth} FILTER: ${JSON.stringify(filter)} (include public)`);
        } else {
          this.logger.info(`${meth} FILTER: ${JSON.stringify(filter)}`);
        }
        owner = (filter != null ? filter.owner : void 0) || null;
        validResourceKinds = this.manifestHelper.getValidResourceKinds();
        allResources = {};
        return this.manifestRepository.listElementTypes(validResourceKinds, owner, null).then(resourcesDict => {
          var publicFilter;
          allResources = resourcesDict;
          includePublic = true; // TODO: for testing
          if (includePublic) {
            publicFilter = {
              'kumori/public': "true"
            };
            return this.manifestRepository.listElementTypes(validResourceKinds, null, publicFilter).then(publicResourcesDict => {
              var k, v;
              for (k in publicResourcesDict) {
                v = publicResourcesDict[k];
                // We don't care if there are duplicates since they would be the same
                allResources[k] = v;
              }
              return true;
            });
          }
        }).then(() => {
          return allResources;
        }).catch(err => {
          var errMsg;
          errMsg = `Unable to list resources: ${err.message}`;
          this.logger.error(`${meth} - ERROR: ${errMsg} - ${err.stack}`);
          return q.reject(new Error(errMsg));
        });
      }

      listResourcesInUse(filter, context) {
        var counter, meth, resInUse;
        meth = 'PlannerStub.listResourcesInUse';
        this.logger.info(`${meth} FILTER: ${JSON.stringify(filter)}`);
        // If the URN provided is a Solution, modify the filter
        if (filter.urn != null && this.manifestHelper.isSolutionURN(filter.urn)) {
          filter.solutionURN = filter.urn;
          delete filter.urn;
        }
        counter = 0;
        resInUse = {};
        filter.show = 'manifest';
        return this.deploymentQuery(filter).then(deployments => {
          var deplData, deplResources, deplURN, i, len, ref1, resDetails, resRef, resURN, resourceList;
          this.logger.info(`${meth} - Got ${Object.keys(deployments).length} deployments.`);
          if (Object.keys(deployments).length > 0) {
            for (deplURN in deployments) {
              deplData = deployments[deplURN];
              deplResources = ((ref1 = deplData.config) != null ? ref1.resource : void 0) || {};
              resourceList = this.extractResources(deplResources, context);
              for (i = 0, len = resourceList.length; i < len; i++) {
                resURN = resourceList[i];
                if (!(resURN in resInUse)) {
                  resRef = this.manifestHelper.urnToRef(resURN);
                  resDetails = {
                    type: resRef.kind,
                    deployment: deplURN,
                    name: resURN
                  };
                  resInUse[resURN] = resDetails;
                  counter++;
                }
              }
            }
          }
          this.logger.info(`${meth} - Found ${counter} resources in use.`);
          return resInUse;
        }).catch(err => {
          var errMsg;
          errMsg = `Unable to perform DeploymentQuery: ${err.message} ${err.stack}`;
          this.logger.error(`${meth} - ERROR: ${errMsg}`);
          return q.reject(new Error(errMsg));
        });
      }

      describeResource(resourceURN) {
        var meth, resourceManifest, result;
        meth = `PlannerStub.describeResource(${resourceURN})`;
        this.logger.info(`${meth}`);
        resourceManifest = null;
        result = null;
        // Get the resource manifest to determine the resource type
        return this.manifestRepository.getManifest(resourceURN).then(manifest => {
          var elementType, errMsg, filter;
          resourceManifest = manifest;
          this.logger.info(`${meth} - Got manifest for ${resourceURN}`);
          elementType = this.manifestHelper.getManifestType(resourceManifest);
          // Check it's a resource
          if (elementType !== ManifestHelper.RESOURCE) {
            errMsg = `Wrong element type (${elementType}).`;
            this.logger.error(`${meth} - ${errMsg}`);
            throw new Error(errMsg);
          }
          this.logger.info(`${meth} - Element type checked OK - ${resourceURN}`);
          // complete the generic part for all resource types
          result = {
            name: resourceManifest.name,
            urn: resourceManifest.urn,
            description: {},
            inUseBy: [],
            items: {},
            public: false,
            creationTimestamp: resourceManifest.creationTimestamp,
            lastModification: resourceManifest.lastModification
          };
          // Determine if the element is public or not
          if (resourceManifest.public != null) {
            result.public = resourceManifest.public;
          } else {
            result.public = false;
          }
          // Look for deployments that used this resource
          this.logger.info(`${meth} - Looking for v3deployments using the resource.`);
          // Filter on the existence of a label named as the element
          filter = {
            [`${resourceManifest.name}`]: '*'
          };
          return this.manifestRepository.listElementType('solutions', null, filter).then(elements => {
            var urn;
            if (Object.keys(elements).length > 0) {
              this.logger.info(`${meth} - Found solutions using the element.`);
              for (urn in elements) {
                manifest = elements[urn];
                result.inUseBy.push(manifest.urn);
              }
            } else {
              this.logger.info(`${meth} - No solutions using the element.`);
            }
            return true;
          }).catch(err => {
            errMsg = `Unable to determine if element is in use: ${err.message} ${err.stack}`;
            this.logger.error(`${meth} - ERROR: ${errMsg}`);
            return true;
          });
        }).then(() => {
          var errMsg;
          // Complete information for each specific resource type
          if (this.manifestHelper.isCA(resourceManifest)) {
            return this.describeResourceCA(resourceManifest, result);
          } else if (this.manifestHelper.isCertificate(resourceManifest)) {
            return this.describeResourceCertificate(resourceManifest, result);
          } else if (this.manifestHelper.isDomain(resourceManifest)) {
            return this.describeResourceDomain(resourceManifest, result);
          } else if (this.manifestHelper.isPort(resourceManifest)) {
            return this.describeResourcePort(resourceManifest, result);
          } else if (this.manifestHelper.isSecret(resourceManifest)) {
            return this.describeResourceSecret(resourceManifest, result);
          } else if (this.manifestHelper.isVolume(resourceManifest)) {
            return this.describeResourceVolume(resourceManifest, result);
          } else {
            errMsg = `Describe not supported for type '${resourceManifest.ref.kind}'.`;
            this.logger.warn(`${meth} - ${errMsg}`);
            throw new Error(errMsg);
          }
        }).then(() => {
          return result;
        });
      }

      describeResourceCA(resourceManifest, result) {
        var meth;
        meth = `PlannerStub.describeResourceCA(${resourceManifest.urn})`;
        this.logger.info(`${meth}`);
        return result.description = resourceManifest.description;
      }

      describeResourceCertificate(resourceManifest, result) {
        var meth;
        meth = `PlannerStub.describeResourceCertificate(${resourceManifest.urn})`;
        this.logger.info(`${meth}`);
        return result.description = resourceManifest.description;
      }

      describeResourceDomain(resourceManifest, result) {
        var meth;
        meth = `PlannerStub.describeResourceDomain(${resourceManifest.urn})`;
        this.logger.info(`${meth}`);
        return result.description = resourceManifest.description;
      }

      describeResourcePort(resourceManifest, result) {
        var meth;
        meth = `PlannerStub.describeResourcePort(${resourceManifest.urn})`;
        this.logger.info(`${meth}`);
        return result.description = {
          port: resourceManifest.description.externalPort
        };
      }

      describeResourceSecret(resourceManifest, result) {
        var meth;
        meth = `PlannerStub.describeResourceSecret(${resourceManifest.urn})`;
        this.logger.info(`${meth}`);
        // For secrets, there is currently no specific info to include
        return result.description = {};
      }

      describeResourceVolume(resourceManifest, result) {
        var KVI_PLURAL, KV_PLURAL, kukuVolumeItemList, kvID, kviMetrics, meth;
        meth = `PlannerStub.describeResourceVolume(${resourceManifest.urn})`;
        this.logger.info(`${meth}`);
        KV_PLURAL = 'kukuvolumes';
        KVI_PLURAL = 'kukuvolumeitems';
        // Get KukuVolume Kumori internal ID
        kvID = resourceManifest.name;
        this.logger.info(`${meth} Resource ID: ${kvID}`);
        kukuVolumeItemList = [];
        kviMetrics = {};
        result.description = {
          maxItems: resourceManifest.description.items,
          size: resourceManifest.description.size,
          type: resourceManifest.description.type
        };
        // Get the KukuVolume Kubernetes object (in case we need something from its
        // status. (currently not necessary)
        this.logger.info(`${meth} - Getting KukuVolume object...`);
        return this.k8sApiStub.getKumoriElement(KV_PLURAL, kvID).then(kvObject => {
          var errMsg, labelSelector;
          if (kvObject == null) {
            errMsg = `Unable to get KukuVolume (${kvID}).`;
            this.logger.error(`${meth} - ${errMsg}`);
            return null;
          }
          this.logger.info(`${meth} - Getting related KukuVolumeItem objects...`);
          // Prepare label selector for finding Items
          labelSelector = `kumori/kukuvolume=${kvID}`;
          return this.k8sApiStub.listKumoriElements(KVI_PLURAL, labelSelector).then(items => {
            this.logger.info(`${meth} - Got list of related KukuVolumeItems (${items.length} elements).`);
            kukuVolumeItemList = items;
            return true;
          }).catch(err => {
            this.logger.error(`${meth} - Unable to get KukuVolumeItems: ${err.msg}`);
            return true;
          });
        }).then(() => {
          var i, kvi, len, promiseChain;
          promiseChain = q();
          for (i = 0, len = kukuVolumeItemList.length; i < len; i++) {
            kvi = kukuVolumeItemList[i];
            (kvi => {
              var kviName, kviPvc;
              kviName = kvi.metadata.name;
              kviPvc = kvi.metadata.labels['kumori/pvc'] || "";
              if (kviPvc != null && kvi.status.phase === 'Bound') {
                return promiseChain = promiseChain.then(() => {
                  this.logger.info(`${meth} - Getting metrics for KVI ${kviName} (PVC: ${kviPvc})...`);
                  return this.k8sApiStub.getPvcMetrics(KUMORI_NAMESPACE, kviPvc).then(pvcMetrics => {
                    this.logger.info(`${meth} - Got metrics for KVI ${kviName}.`);
                    return kviMetrics[kviName] = pvcMetrics;
                  }).catch(err => {
                    this.logger.error(`${meth} - Unable to get KVI metrics: ${err.msg}`);
                    return true;
                  });
                });
              }
            })(kvi);
          }
          return promiseChain.then(() => {
            this.logger.info(`${meth} - Got metrics for all relevant KVIs.`);
            return true;
          });
        }).then(() => {
          var i, inUseBy, kvi, kviItem, kviName, kviPvc, len, start;
          // All info gathered. Complete the result object
          for (i = 0, len = kukuVolumeItemList.length; i < len; i++) {
            kvi = kukuVolumeItemList[i];
            kviName = kvi.metadata.name;
            // If KVI is bound deduce the Pod name from the PVC name (deterministic).
            //   PVC name = <internal_volume_name>-<pod_name>
            //   (where Pod name starts with kd-)
            inUseBy = null;
            kviPvc = kvi.metadata.labels['kumori/pvc'] || "";
            if (kviPvc != null && kvi.status.phase === 'Bound') {
              if (kviPvc.includes('-kd-')) {
                start = 1 + kviPvc.indexOf('-kd-');
                inUseBy = kviPvc.substring(start);
              } else {
                inUseBy = 'unknown';
              }
            }
            kviItem = {
              phase: kvi.status.phase,
              creationTimestamp: kvi.metadata.creationTimestamp,
              inUseBy: inUseBy,
              metrics: kviMetrics[kviName] || null
            };
            result.items[kviName] = kviItem;
          }
          // This method modifies the original 'result' parameter provided
          return true;
        });
      }

      solutionQuery(params) {
        var VALID_SHOW_VALUES, e, errMsg, filter, meth, ref, ref1, type;
        VALID_SHOW_VALUES = ['urn', 'topology', 'extended', 'manifest'];
        meth = 'PlannerStub.solutionQuery()';
        this.logger.info(`${meth} - Params: ${JSON.stringify(params)}`);
        filter = {};
        type = "";
        if (params.urn != null) {
          try {
            ref = this.manifestHelper.urnToRef(params.urn);
            this.logger.info(`${meth} - URN Data = ${JSON.stringify(ref)}`);
            filter['kumori/name'] = this.manifestHelper.hashLabel(ref.name);
            filter['kumori/domain'] = this.manifestHelper.hashLabel(ref.domain);
            type = ref.kind;
          } catch (error) {
            e = error;
            errMsg = `Unable to perform SolutionQuery: Invalid URN ${params.urn} (${e.message}).`;
            this.logger.error(`${meth} - ERROR: ${errMsg}`);
            return q.reject(new Error(errMsg));
          }
        }
        // Filter that determines how much information on each deployment will be
        // returned. Possible values are:
        // - urn: return no data
        // - topology: return basic data + instances information (DEFAULT)
        // - extended: return all data available
        if (params.show == null) {
          this.logger.info(`${meth} - Empty 'show' property. Defaulting to 'topology'.`);
          params.show = 'topology';
        } else if (ref1 = params.show, indexOf.call(VALID_SHOW_VALUES, ref1) < 0) {
          this.logger.info(`${meth} - Invalid 'show' value: ${params.show}. Defaulting to 'topology'.`);
          params.show = 'topology';
        }
        // Look for solutions
        return this.manifestRepository.listElementType('solutions', params.owner, filter).then(elements => {
          if (params.show === 'manifest') {
            return q(elements);
          } else if (params.show === 'urn') {
            elements = this.removeSolutionData(elements);
            return q(elements);
          } else if (params.show === 'extended') {
            return this.completeSolutions(elements, true);
          } else {
            // Default is 'topology'
            this.completeSolutions(elements, false);
            return q(elements);
          }
        }).then(elements => {
          this.logger.info(`${meth} - Found ${Object.keys(elements).length} solutions`);
          return elements;
        }).catch(err => {
          errMsg = `Unable to perform SolutionQuery: ${err.message} ${err.stack}`;
          this.logger.error(`${meth} - ERROR: ${errMsg}`);
          return q.reject(new Error(errMsg));
        });
      }

      getDeployment(params) {
        var filterStr, meth;
        meth = `PlannerStub.getDeployment(${JSON.stringify(params)})`;
        this.logger.info(meth);
        // Just for log traces
        filterStr = JSON.stringify(params);
        return this.deploymentQuery(params).then(deploymentList => {
          var deploymentURN, errMsg;
          if (Object.keys(deploymentList).length === 0) {
            this.logger.debug(`${meth} - No deployment found for ${filterStr}`);
            return null;
          } else if (Object.keys(deploymentList).length > 1) {
            errMsg = `Several matches found for deployment ${filterStr}`;
            this.logger.warn(`${meth} - ${errMsg}: ${JSON.stringify(deploymentList)}`);
            return q.reject(new Error(errMsg));
          } else {
            deploymentURN = Object.keys(deploymentList)[0];
            this.logger.info(`${meth} - Found deployment for ${filterStr} : ${deploymentURN}`);
            return deploymentList[deploymentURN];
          }
        }).catch(err => {
          this.logger.error(`${meth} - ERROR: ${err.message}`);
          return null;
        });
      }

      deploymentQuery(params) {
        var VALID_SHOW_VALUES, deplList, e, errMsg, filter, meth, ref, ref1, type;
        VALID_SHOW_VALUES = ['urn', 'topology', 'extended', 'manifest'];
        meth = 'PlannerStub.deploymentQuery()';
        this.logger.info(`${meth} - Params: ${JSON.stringify(params)}`);
        filter = {};
        type = "";
        if (params.solutionURN != null) {
          try {
            ref = this.manifestHelper.urnToRef(params.solutionURN);
            this.logger.info(`${meth} - URN Data = ${JSON.stringify(ref)}`);
            filter['kumori/solution.domain'] = this.manifestHelper.hashLabel(ref.domain);
            filter['kumori/solution.name'] = this.manifestHelper.hashLabel(ref.name);
            // For solutions, look for any deployment type
            type = '*';
          } catch (error) {
            e = error;
            errMsg = `Unable to perform DeploymentQuery: Invalid URN ${params.urn} (${e.message}).`;
            this.logger.error(`${meth} - ERROR: ${errMsg}`);
            return q.reject(new Error(errMsg));
          }
        } else if (params.urn != null) {
          try {
            ref = this.manifestHelper.urnToRef(params.urn);
            this.logger.info(`${meth} - URN Data = ${JSON.stringify(ref)}`);
            filter['kumori/domain'] = this.manifestHelper.hashLabel(ref.domain);
            filter['kumori/name'] = this.manifestHelper.hashLabel(ref.name);
            type = ref.kind;
          } catch (error) {
            e = error;
            errMsg = `Unable to perform DeploymentQuery: Invalid URN ${params.urn} (${e.message}).`;
            this.logger.error(`${meth} - ERROR: ${errMsg}`);
            return q.reject(new Error(errMsg));
          }
        }
        if (params.ref != null) {
          filter['kumori/domain'] = this.manifestHelper.hashLabel(params.ref.domain);
          filter['kumori/name'] = this.manifestHelper.hashLabel(params.ref.name);
          type = params.ref.kind;
        }
        // Filter that determines how much information on each deployment will be
        // returned. Possible values are:
        // - urn: return no data
        // - topology: return basic data + instances information (DEFAULT)
        // - extended: return all data available
        if (params.show == null) {
          this.logger.info(`${meth} - Empty 'show' property. Defaulting to 'topology'.`);
          params.show = 'topology';
        } else if (ref1 = params.show, indexOf.call(VALID_SHOW_VALUES, ref1) < 0) {
          this.logger.info(`${meth} - Invalid 'show' value: ${params.show}. Defaulting to 'topology'.`);
          params.show = 'topology';
        }
        deplList = {};
        // Add v3deployments
        return q().then(() => {
          if (type === 'deployment' || type === '*' || type === '') {
            return this.manifestRepository.listElementType('v3deployments', params.owner, filter);
          } else {
            return q({});
          }
        }).then(elements => {
          if (params.show === 'manifest') {
            return q(elements);
          } else if (params.show === 'urn') {
            elements = this.removeDeploymentData(elements);
            return q(elements);
          } else if (params.show === 'extended') {
            return this.completeV3Deployments(elements, true);
          } else {
            // Default is 'topology'
            return this.completeV3Deployments(elements, false);
          }
        }).then(elements => {
          deplList = utils.extendObject(deplList, elements);
          this.logger.info(`${meth} - Found ${Object.keys(deplList).length} deployments`);
          // Make some adjustments in the returned information, since AdmissionClient
          // and/or Dashboard rely on specific fields not presents in the current
          // version
          return deplList;
        }).catch(err => {
          errMsg = `Unable to perform DeploymentQuery: ${err.message} ${err.stack}`;
          this.logger.error(`${meth} - ERROR: ${errMsg}`);
          return q.reject(new Error(errMsg));
        });
      }

      getDeploymentOwner(deploymentURN) {
        var meth, params;
        meth = `PlannerStub.getDeploymentOwner() - URN: ${deploymentURN}`;
        this.logger.info(meth);
        params = {
          urn: deploymentURN,
          show: 'manifest'
        };
        return this.deploymentQuery(params).then(deploymentList => {
          var errMsg;
          if (Object.keys(deploymentList).length === 0) {
            errMsg = `No deployment found for ${deploymentURN}`;
            this.logger.info(`${meth} - ${errMsg}`);
            return q.reject(new Error(errMsg));
          } else if (Object.keys(deploymentList).length > 1) {
            errMsg = `Several matches found for deployment ${deploymentURN}`;
            this.logger.warn(`${meth} - ${errMsg}: ${JSON.stringify(deploymentList)}`);
            return q.reject(new Error(errMsg));
          } else {
            deploymentURN = Object.keys(deploymentList)[0];
            this.logger.info(`${meth} - Found deployment for ${deploymentURN} : ${deploymentURN}`);
            this.logger.info(`${meth} - Owner: ${deploymentList[deploymentURN].owner}`);
            return deploymentList[deploymentURN].owner;
          }
        }).catch(err => {
          var errMsg;
          errMsg = `Unable to get owner of deployment ${deploymentURN}: ${err.message}`;
          this.logger.info(`${meth} - ERROR: ${errMsg}`);
          return q.reject(new Error(errMsg));
        });
      }

      getInstanceOwner(instanceId) {
        var deplList, deploymentK8sId, filter, meth;
        meth = `PlannerStub.getInstanceOwner() - instance: ${instanceId}`;
        this.logger.info(meth);
        deploymentK8sId = null;
        deplList = {};
        filter = {};
        return this.k8sApiStub.getPod(KUMORI_NAMESPACE, instanceId).then(podInfo => {
          var deploymentName, errMsg, ref1, ref2;
          if ((podInfo != null ? (ref1 = podInfo.metadata) != null ? (ref2 = ref1.labels) != null ? ref2['kumori/deployment.id'] : void 0 : void 0 : void 0) == null) {
            errMsg = 'Invalid Instance (no kumori/deployment.id label found)';
            throw new Error(errMsg);
          }
          deploymentName = podInfo.metadata.labels['kumori/deployment.id'];
          this.logger.info(`${meth} - Parent KukuDeployment: ${deploymentName}`);
          return this.manifestRepository.getManifestByNameAndKind(deploymentName, 'v3deployments');
        }).then(manifest => {
          this.logger.debug(`${meth} - Parent KukuDeployment manifest: ${JSON.stringify(manifest)}`);
          this.logger.debug(`${meth} - Parent KukuDeployment owner: ${manifest.owner}`);
          return manifest.owner;
        }).catch(err => {
          var errMsg;
          errMsg = `Unable to get owner of instance ${instanceId}: ${err.message}`;
          this.logger.error(`${meth} - ERROR: ${errMsg}`);
          return q.reject(new Error(errMsg));
        });
      }

      getDockerCredentialsFromSecret(secretReference) {
        var meth, secretURN;
        meth = `PlannerStub.getDockerCredentialsFromSecret() - Secret reference: ${secretReference}`;
        this.logger.info(meth);
        // Alternative of searching by labels (requires passing 'owner' to method)

        // refParts = secretReference.split '/'
        // filter = {}
        // filter.domain = refParts[0]
        // filter.name = refParts[1]
        // @manifestRepository.listElementType 'secrets', owner, filter

        // Convert secret reference to a URN
        // Secret reference is in format: <domain>/<name>
        secretURN = 'eslap://' + secretReference.replace('/', '/secret/');
        this.logger.info(`${meth} - Getting manifest for URN ${secretURN}`);
        return this.manifestRepository.getManifest(secretURN).then(manifest => {
          var authsKeys, credentials, decodedAuthData, decodedDockerConfigJson, dockerConfig, errMsg, parts, rawAuthData, rawDockerConfigJson, ref1;
          this.logger.info(`${meth} - Got KukuSecret!`);
          // 'data' property should contain a base64 string representation of a
          // stringified docker config in JSON format
          if ((manifest != null ? (ref1 = manifest.description) != null ? ref1.data : void 0 : void 0) != null) {
            rawDockerConfigJson = manifest.description.data;
            this.logger.info(`${meth} - RAW    : ${rawDockerConfigJson}`);
            decodedDockerConfigJson = Buffer.from(rawDockerConfigJson, 'base64').toString();
            // @logger.info "#{meth} - DECODED: #{decodedDockerConfigJson}"
            dockerConfig = JSON.parse(decodedDockerConfigJson);
            // @logger.info "#{meth} - PARSED : #{dockerConfig}"
            authsKeys = Object.keys(dockerConfig.auths);
            rawAuthData = dockerConfig.auths[authsKeys[0]].auth;
            // @logger.info "#{meth} - AUTH DATA RAW    : #{rawAuthData}"
            decodedAuthData = Buffer.from(rawAuthData, 'base64').toString();
            // @logger.info "#{meth} - AUTH DATA DECODED: #{decodedAuthData}"
            parts = decodedAuthData.split(':');
            if (parts.length < 2) {
              errMsg = "Secret decoded credentials are malformed.";
              throw new Error(errMsg);
            } else {
              credentials = {
                username: parts.shift(),
                password: parts.join(':')
              };
              // @logger.info "#{meth} - CREDENTIALS: #{JSON.stringify credentials}"
              this.logger.info(`${meth} - CREDENTIALS USERNAME: ${credentials.username}`);
              return credentials;
            }
          } else {
            errMsg = "Secret does not contain expected 'description/data' property.";
            throw new Error(errMsg);
          }
        }).catch(err => {
          var errMsg;
          errMsg = `Unable to get Secret ${secretReference}: ${err.message}`;
          this.logger.error(`${meth} - ERROR: ${errMsg}`);
          return q.reject(new Error(errMsg));
        });
      }

      restartInstance(podName) {
        var meth;
        meth = `PlannerStub.restartInstance(${podName})`;
        this.logger.info(meth);
        this.logger.info(`${meth} - Evicting Pod ${podName}...`);
        return this.k8sApiStub.evictPod(KUMORI_NAMESPACE, podName);
      }

      restartRole(deploymentName, role) {
        var deploymentList, labelSelector, meth, statefulSetList, updated;
        meth = `PlannerStub.restartRole Deployment: ${deploymentName}, Role: ${role}`;
        this.logger.info(meth);
        // Find deployments and statefulSets with the appropriate labels
        labelSelector = '';
        labelSelector += `kumori/deployment.id=${deploymentName}`;
        labelSelector += ",kumori/role=" + this.manifestHelper.hashLabel(role);
        updated = false;
        deploymentList = null;
        statefulSetList = null;
        return this.k8sApiStub.getDeployments(KUMORI_NAMESPACE, labelSelector).then(deployments => {
          deploymentList = deployments;
          return this.k8sApiStub.getStatefulSets(KUMORI_NAMESPACE, labelSelector);
        }).then(statefulSets => {
          return statefulSetList = statefulSets;
        }).then(() => {
          var errMsg, name, patch;
          this.logger.info(`${meth} - Found ${deploymentList.length} Deployments and ${statefulSetList.length} StatefulSets.`);
          if (deploymentList.length + statefulSetList.length === 0) {
            errMsg = `No Deployment or StatefulSet found for ID ${deploymentName} and role ${role}`;
            this.logger.error(`${meth} ERROR: ${errMsg}`);
            throw new Error(errMsg);
          } else if (deploymentList.length + statefulSetList.length > 1) {
            errMsg = `More than one Deployment or StatefulSet found for ID ${deploymentName} and role ${role}`;
            this.logger.error(`${meth} ERROR: ${errMsg}`);
            throw new Error(errMsg);
          } else {
            // There is only one, so let's patch it
            patch = [{
              op: 'add',
              // The slash in the annotation name (kumori~1restartedAt) is escaped in
              // JSONPatch by using '~1'.
              // Source: http://jsonpatch.com/#json-pointer
              path: '/spec/template/metadata/annotations/kumori~1restartedAt',
              value: utils.getTimestamp()
            }];
            if (deploymentList.length === 1) {
              // Patch the deployment
              name = deploymentList[0].metadata.name;
              this.logger.info(`${meth} - Patching deployment ${name}...`);
              return this.k8sApiStub.patchDeployment(KUMORI_NAMESPACE, name, patch);
            } else if (statefulSetList.length === 1) {
              // Patch the statefulSet
              name = statefulSetList[0].metadata.name;
              this.logger.info(`${meth} - Patching StatefulSet ${name}...`);
              return this.k8sApiStub.patchStatefulSet(KUMORI_NAMESPACE, name, patch);
            }
          }
        });
      }

      //#############################################################################
      //#                           HELPER FUNCTIONS                               ##
      //#############################################################################
      removeSolutionData(solutionManifests) {
        var cleanItem, cleanManifests, deplData, deplName, ref1, solData, solURN;
        cleanManifests = {};
        for (solURN in solutionManifests) {
          solData = solutionManifests[solURN];
          cleanItem = {
            deployments: []
          };
          // If there is a top, place it in the first position
          if (solData.top != null) {
            cleanItem.deployments.push(solData.top);
          }
          ref1 = solData.deployments;
          // Add the remaining deployments
          for (deplName in ref1) {
            deplData = ref1[deplName];
            if (indexOf.call(cleanItem.deployments, deplName) < 0) {
              cleanItem.deployments.push(deplName);
            }
          }
          // If object is being deleted add a deletionTimestamp property
          if (solData.deletionTimestamp != null) {
            cleanItem.deletionTimestamp = solData.deletionTimestamp;
          }
          cleanManifests[solURN] = cleanItem;
        }
        return cleanManifests;
      }

      removeDeploymentData(deploymentManifests) {
        var deplData, deplURN;
        for (deplURN in deploymentManifests) {
          deplData = deploymentManifests[deplURN];
          if (deplData.urn != null) {
            deploymentManifests[deplData.urn] = {};
          } else {
            this.logger.error(`PlannerStub.removeDeploymentData - Manifest ${deplURN} has no 'urn' property.`);
            deploymentManifests[deplURN] = {};
          }
        }
        return deploymentManifests;
      }

      completeSolutions(solutionManifests, extended = false) {
        var promiseList, solData, solURN;
        promiseList = [];
        for (solURN in solutionManifests) {
          solData = solutionManifests[solURN];
          promiseList.push(this.completeSolution(solData, extended));
        }
        return q.allSettled(promiseList).then(function () {
          return solutionManifests;
        });
      }

      completeV3Deployments(deploymentManifests, extended = false) {
        var deplData, deplURN, promiseList;
        promiseList = [];
        for (deplURN in deploymentManifests) {
          deplData = deploymentManifests[deplURN];
          promiseList.push(this.completeV3Deployment(deplData, extended));
        }
        return q.allSettled(promiseList).then(function () {
          return deploymentManifests;
        });
      }

      completeSolution(solutionManifest, extended = false) {
        var meth, params, promiseList, topDeploymentManifest, topDeploymentRef;
        meth = `PlannerStub.completeSolution ${solutionManifest.name} (${extended})`;
        this.logger.info(meth);
        promiseList = [];
        // We will need the top deployment ID to differentiate between solution and
        // top deployment links (they have the same domain and name)
        topDeploymentManifest = null;
        topDeploymentRef = {
          kind: 'deployment',
          domain: solutionManifest.ref.domain,
          name: solutionManifest.top
        };
        params = {
          ref: topDeploymentRef,
          show: 'manifest'
        };
        return this.getDeployment(params).then(deploymentManifest => {
          var errMsg;
          if (deploymentManifest == null) {
            errMsg = `Top deployment ${this.manifestHelper.refToUrn(topDeploymentRef)} not found.`;
            this.logger.error(`${meth} - ${errMsg}`);
            throw new Error(errMsg);
          }
          topDeploymentManifest = deploymentManifest;
          this.logger.debug(`${meth} - Found top deployment ${this.manifestHelper.refToUrn(topDeploymentRef)}`);
          this.logger.debug(`${meth} - Getting solution events...`);
          // Get solution events
          return this.eventStore.getSolutionEvents(KUMORI_NAMESPACE, solutionManifest.name);
        }).then(nsEvents => {
          this.logger.debug(`${meth} - Got ${nsEvents.length} events.`);
          // Add events info to the original manifest
          solutionManifest.events = this.filterSolutionEvents(solutionManifest.name, nsEvents);
          this.logger.debug(`${meth} - Getting active links of the solution...`);
          // Get solution links (only external links)
          return this.getSolutionLinks(solutionManifest.name, solutionManifest.urn, topDeploymentManifest.name, topDeploymentManifest.urn, true);
        }).then(links => {
          var solutionDeploymentsParams;
          this.logger.debug(`${meth} - Got links.`);
          // Add links info to the original manifest
          solutionManifest['externalLinks'] = links;
          this.logger.debug(`${meth} - Getting solution deployments info...`);
          // Get detailed info of all deployments of the solution (including  their
          // events and links)
          solutionDeploymentsParams = {
            solutionURN: solutionManifest.urn,
            show: 'extended'
          };
          return this.deploymentQuery(solutionDeploymentsParams);
        }).then(deploymentList => {
          var deplData, deplRef, deplURN, roleData, roleName, roles;
          this.logger.debug(`${meth} - Got ${Object.keys(deploymentList).length} deployments of the solution.`);
          // For each deployment, add its events, links and instances to the solution
          // manifest
          for (deplURN in deploymentList) {
            deplData = deploymentList[deplURN];
            deplRef = this.manifestHelper.urnToRef(deplURN);
            if (deplRef.name in solutionManifest.deployments) {
              // Add the internalID as 'id' (since name is already used)
              solutionManifest.deployments[deplRef.name].id = deplData.name;
              // Add links and events
              solutionManifest.deployments[deplRef.name].events = deplData.events;
              solutionManifest.deployments[deplRef.name].links = deplData.links;
              // Add deployment events to the solution-level events
              solutionManifest.events = utils.arrayMerge(solutionManifest.events, deplData.events, true);
              // For each role, add its instances
              roles = solutionManifest.deployments[deplRef.name].artifact.description.role;
              for (roleName in roles) {
                roleData = roles[roleName];
                roleData.instances = deplData.artifact.description.role[roleName].instances;
              }
            } else {
              this.logger.error = `${meth} - Deployment not part of the solution: ${deplURN}`;
            }
          }
          return solutionManifest;
        }).catch(err => {
          this.logger.error(`${meth} - ERROR: ${err.message} ${err.stack}`);
          return solutionManifest;
        });
      }

      completeV3Deployment(v3Manifest, extended = false) {
        var meth, promiseList;
        meth = `PlannerStub.completeV3Deployment ${v3Manifest.name} (${extended})`;
        this.logger.info(meth);
        promiseList = [];
        return this.eventStore.getDeploymentEvents(KUMORI_NAMESPACE, v3Manifest.name).then(nsEvents => {
          var ref1, ref2, ref3, ref4, roleData, roleName, serviceRoles;
          if (!('events' in v3Manifest)) {
            v3Manifest.events = this.filterDeploymentEvents(v3Manifest.name, nsEvents);
          }
          if (!('links' in v3Manifest)) {
            promiseList.push(this.getDeploymentLinks(v3Manifest, true).then(links => {
              return v3Manifest['links'] = links;
            }));
          }
          // Differentiate between normal deployment and builtins.

          // Currently, the only builtin is Inbound Service, which doesn't have
          // instances, and no extra info to include.
          if ((ref1 = v3Manifest.artifact) != null ? (ref2 = ref1.description) != null ? ref2.builtin : void 0 : void 0) {
            // Deployment is a builtin
            if (isInboundBuiltin(v3Manifest)) {
              // Deployment is an Inbound builtin
              // Currently, we dont add any extra information to an Inbound builtin
              logger.debug(`${meth} - Detected an Inbound builtin.`);
            } else {
              logger.warn(`${meth} - Unknown builtin: ${JSON.stringify(v3Manifest.ref)}`);
            }
          } else {
            // Deployment is not a builtin
            serviceRoles = ((ref3 = v3Manifest.artifact) != null ? (ref4 = ref3.description) != null ? ref4.role : void 0 : void 0) || {};
            for (roleName in serviceRoles) {
              roleData = serviceRoles[roleName];
              ((roleName, roleData) => {
                var contNames;
                if (extended && !('instances' in roleData)) {
                  contNames = Object.keys(roleData.artifact.description.code);
                  return promiseList.push(this.getRoleInstances(v3Manifest.name, roleName, contNames, nsEvents).then(roleInstances => {
                    return roleData['instances'] = roleInstances;
                  }));
                }
              })(roleName, roleData);
            }
          }
          return q.allSettled(promiseList);
        }).then(function () {
          return v3Manifest;
        }).catch(function (err) {
          logger.error(`${meth} - ERROR: ${err.message} ${err.stack}`);
          return v3Manifest;
        });
      }

      getRoleInstances(deplName, roleName, contNames, nsEvents) {
        var contHashToName, contName, hash, i, labelSelector, len, meth, promiseList, roleInstances;
        meth = `PlannerStub.getRoleInstances ${deplName} - ${roleName}`;
        this.logger.info(meth);
        // Prepare a reverse dictionnary of container names and their hashes
        contHashToName = {};
        for (i = 0, len = contNames.length; i < len; i++) {
          contName = contNames[i];
          hash = 'k-' + this.manifestHelper.hashLabel(contName);
          contHashToName[hash] = contName;
        }
        // Prepare the label selector for the search
        labelSelector = '';
        labelSelector += `kumori/deployment.id=${deplName}`;
        labelSelector += ",kumori/role=" + this.manifestHelper.hashLabel(roleName);
        roleInstances = {};
        promiseList = [];
        return this.k8sApiStub.getPods(KUMORI_NAMESPACE, labelSelector).then(podList => {
          var j, len1, pod;
          this.logger.info(`${meth} - Found ${podList.length} pods.`);
          for (j = 0, len1 = podList.length; j < len1; j++) {
            pod = podList[j];
            (pod => {
              return promiseList.push(this.k8sApiStub.getPodMetrics(KUMORI_NAMESPACE, pod.metadata.name).then(podMetrics => {
                var cond, contData, contRealName, contStatus, instanceData, l, len2, len3, len4, m, n, ref1, ref2, ref3, ref4;
                instanceData = {
                  name: pod.metadata.name,
                  creationDate: pod.metadata.creationTimestamp,
                  node: pod.spec.nodeName || null,
                  status: pod.status.phase,
                  statusReason: pod.status.reason || null,
                  statusMessage: pod.status.message || null,
                  ready: false,
                  containers: {},
                  metrics: {}
                };
                // Determine if Pod is marked for deletion
                if (pod.metadata.deletionTimestamp != null) {
                  instanceData.deletionTimestamp = pod.metadata.deletionTimestamp;
                  if (pod.metadata.deletionGracePeriodSeconds != null) {
                    instanceData.deletionGracePeriod = pod.metadata.deletionGracePeriodSeconds;
                  }
                }
                ref1 = pod.status.conditions || [];
                // Extract Pod ready value from Pod conditions
                for (l = 0, len2 = ref1.length; l < len2; l++) {
                  cond = ref1[l];
                  if (cond.type === 'Ready') {
                    if (((ref2 = cond.status) != null ? ref2.toLowerCase() : void 0) === 'true') {
                      instanceData.ready = true;
                    }
                  }
                }
                ref3 = pod.status.containerStatuses || [];
                // Process containers info
                for (m = 0, len3 = ref3.length; m < len3; m++) {
                  contStatus = ref3[m];
                  // if 'running' of contStatus.state
                  //   instanceData.
                  contData = {
                    restarts: contStatus.restartCount,
                    state: contStatus.state,
                    lastState: contStatus.lastState,
                    ready: contStatus.ready
                  };
                  contRealName = contHashToName[contStatus.name];
                  instanceData.containers[contRealName] = contData;
                }
                ref4 = pod.status.initContainerStatuses || [];
                // Process init containers info
                for (n = 0, len4 = ref4.length; n < len4; n++) {
                  contStatus = ref4[n];
                  // if 'running' of contStatus.state
                  //   instanceData.
                  contData = {
                    restarts: contStatus.restartCount,
                    state: contStatus.state,
                    lastState: contStatus.lastState,
                    ready: contStatus.ready
                  };
                  contRealName = contHashToName[contStatus.name];
                  instanceData.containers[contRealName] = contData;
                }
                this.addMetricData(instanceData, pod.spec.containers, podMetrics, contHashToName);
                this.addPodEvents(instanceData, nsEvents);
                return roleInstances[instanceData.name] = instanceData;
              }));
            })(pod);
          }
          return q.allSettled(promiseList);
        }).then(() => {
          // console.log "RETURNING INSTANCE LIST:"
          // console.log JSON.stringify roleInstances
          // @logger.debug "#{meth} RoleInstances: #{JSON.stringify roleInstances}"
          return roleInstances;
        });
      }

      // podMetrics is an already processed object:
      //  {
      //    "usage": {
      //      "memory": 2650112,
      //      "cpu": 0
      //    },
      //    "containers": {
      //      "frontend": {
      //        "usage": {
      //          "memory": 2650112,
      //          "cpu": 0
      //        }
      //      }
      //    }
      //  }
      addMetricData(instanceData, containersInfo, podMetrics, contHashToName) {
        var contData, contRealName, contRes, err, i, len, meth, podLimits, podRequests, podUsage, ref1, ref10, ref2, ref3, ref4, ref5, ref6, ref7, ref8, ref9, v;
        meth = `PlannerStub.addMetricData ${instanceData.name}`;
        this.logger.info(meth);
        try {
          podRequests = {
            cpu: 0,
            memory: 0
          };
          podLimits = {
            cpu: 0,
            memory: 0
          };
          podUsage = (podMetrics != null ? podMetrics.usage : void 0) || {
            cpu: 0,
            memory: 0
          };
          for (i = 0, len = containersInfo.length; i < len; i++) {
            contData = containersInfo[i];
            // Convert hashes name to real container name
            contRealName = contHashToName[contData.name];
            // If container is not in the instance object container list, skip it
            if (!(contRealName in (instanceData.containers || {}))) {
              continue;
            }
            contRes = {
              requests: {},
              limits: {},
              usage: (podMetrics != null ? (ref1 = podMetrics.containers) != null ? (ref2 = ref1[contData.name]) != null ? ref2.usage : void 0 : void 0 : void 0) || {
                cpu: 0,
                memory: 0
              }
            };
            if (((ref3 = contData.resources) != null ? (ref4 = ref3.requests) != null ? ref4.cpu : void 0 : void 0) != null) {
              v = quantityToScalar(contData.resources.requests.cpu);
              contRes.requests.cpu = v;
              podRequests.cpu += v;
            }
            if (((ref5 = contData.resources) != null ? (ref6 = ref5.requests) != null ? ref6.memory : void 0 : void 0) != null) {
              v = quantityToScalar(contData.resources.requests.memory);
              contRes.requests.memory = v;
              podRequests.memory += v;
            }
            if (((ref7 = contData.resources) != null ? (ref8 = ref7.limits) != null ? ref8.cpu : void 0 : void 0) != null) {
              v = quantityToScalar(contData.resources.limits.cpu);
              contRes.limits.cpu = v;
              podLimits.cpu += v;
            }
            if (((ref9 = contData.resources) != null ? (ref10 = ref9.limits) != null ? ref10.memory : void 0 : void 0) != null) {
              v = quantityToScalar(contData.resources.limits.memory);
              contRes.limits.memory = v;
              podLimits.memory += v;
            }
            instanceData.containers[contRealName].metrics = contRes;
          }
          instanceData.metrics = {
            requests: podRequests,
            limits: podLimits,
            usage: podUsage
          };
          return true;
        } catch (error) {
          err = error;
          this.logger.error(`${meth} Unexpected error: ${err.message} ${err.stack}`);
          return true;
        }
      }

      addPodEvents(instanceData, nsEvents) {
        var evt, evts, i, len, meth, nsEvt, ref1, ref2;
        meth = `PlannerStub.addPodEvents ${instanceData.name}`;
        this.logger.info(meth);
        evts = [];
        for (i = 0, len = nsEvents.length; i < len; i++) {
          nsEvt = nsEvents[i];
          // console.log "EVENT:"
          // console.log " - kind: #{nsEvt.involvedObject.kind}"
          // console.log " - name: #{nsEvt.involvedObject.name}"
          // console.log " - Pod:  #{instanceData.name}"
          if (((ref1 = nsEvt.involvedObject) != null ? ref1.kind : void 0) === 'Pod' && ((ref2 = nsEvt.involvedObject) != null ? ref2.name : void 0) === instanceData.name) {
            evt = {
              time: nsEvt.eventTime,
              firstTimestamp: nsEvt.firstTimestamp,
              lastTimestamp: nsEvt.lastTimestamp,
              counter: nsEvt.count,
              message: nsEvt.message,
              type: nsEvt.type,
              reason: nsEvt.reason
            };
            evts.push(evt);
          }
        }
        // console.log "POD EVENT LIST: #{evts}"
        return instanceData.events = evts;
      }

      filterSolutionEvents(solName, nsEvents) {
        var evt, evts, i, len, nsEvt, ref1;
        // RELEVANT_TYPES = [
        //   'Pod'
        //   'ReplicaSet'
        //   'StatefulSet'
        // ]
        evts = [];
        for (i = 0, len = nsEvents.length; i < len; i++) {
          nsEvt = nsEvents[i];
          // console.log "EVENT:"
          // console.log JSON.stringify nsEvt, null, 2
          // console.log ""
          // console.log "EVENT:"
          // console.log " - Event involvedObject kind: #{nsEvt.involvedObject.kind}"
          // console.log " - Event involvedObject name: #{nsEvt.involvedObject.name}"
          // console.log " - Deployment name          : #{deplName}"

          // if nsEvt.involvedObject?.kind in RELEVANT_TYPES
          // console.log "Involved object name: #{nsEvt.involvedObject.name}"
          // console.log "Involved object labels: #{JSON.stringify nsEvt.involvedObject}"
          // console.log "Involved object labels: #{JSON.stringify nsEvt.involvedObject.labels}"
          // console.log "Involved object annotations: #{JSON.stringify nsEvt.involvedObject.annotations}"
          if (((ref1 = nsEvt.involvedObject) != null ? ref1.name : void 0) != null && nsEvt.involvedObject.name.includes(solName)) {
            evt = {
              time: nsEvt.eventTime,
              firstTimestamp: nsEvt.firstTimestamp,
              lastTimestamp: nsEvt.lastTimestamp,
              counter: nsEvt.count,
              objectKind: nsEvt.involvedObject.kind,
              objectName: nsEvt.involvedObject.name,
              message: nsEvt.message,
              type: nsEvt.type,
              reason: nsEvt.reason
            };
            evts.push(evt);
          }
        }
        return evts;
      }

      filterDeploymentEvents(deplName, nsEvents) {
        var RELEVANT_TYPES, evt, evts, i, len, nsEvt, ref1;
        RELEVANT_TYPES = ['Pod', 'ReplicaSet', 'StatefulSet'];
        evts = [];
        for (i = 0, len = nsEvents.length; i < len; i++) {
          nsEvt = nsEvents[i];
          // console.log "EVENT:"
          // console.log JSON.stringify nsEvt, null, 2
          // console.log ""
          // console.log "EVENT:"
          // console.log " - Event involvedObject kind: #{nsEvt.involvedObject.kind}"
          // console.log " - Event involvedObject name: #{nsEvt.involvedObject.name}"
          // console.log " - Deployment name          : #{deplName}"

          // if nsEvt.involvedObject?.kind in RELEVANT_TYPES
          // console.log "Involved object name: #{nsEvt.involvedObject.name}"
          // console.log "Involved object labels: #{JSON.stringify nsEvt.involvedObject}"
          // console.log "Involved object labels: #{JSON.stringify nsEvt.involvedObject.labels}"
          // console.log "Involved object annotations: #{JSON.stringify nsEvt.involvedObject.annotations}"
          if (((ref1 = nsEvt.involvedObject) != null ? ref1.name : void 0) != null && nsEvt.involvedObject.name.includes(deplName)) {
            evt = {
              time: nsEvt.eventTime,
              firstTimestamp: nsEvt.firstTimestamp,
              lastTimestamp: nsEvt.lastTimestamp,
              counter: nsEvt.count,
              objectKind: nsEvt.involvedObject.kind,
              objectName: nsEvt.involvedObject.name,
              message: nsEvt.message,
              type: nsEvt.type,
              reason: nsEvt.reason
            };
            evts.push(evt);
          }
        }
        return evts;
      }

      getSolutionLinks(solutionName, solutionURN, topDeploymentName, topDeploymentURN, convertLinks = false) {
        var filter, meth;
        meth = `PlannerStub.getSolutionLinks() Solution Name: ${solutionName} - Top Deployment Name: ${topDeploymentName} - Top Deployment URN: ${topDeploymentURN}`;
        this.logger.info(meth);
        // Filter on the existence of a label named as the deployment
        filter = {
          [`${topDeploymentName}`]: '*',
          'kumori/solution.id': `!!${solutionName}`
        };
        return this.manifestRepository.listElementType('links', null, filter).then(elements => {
          var links;
          if (Object.keys(elements).length > 0) {
            this.logger.info(`${meth} - Found links for deployment ${topDeploymentName}.`);
          }
          if (convertLinks) {
            // console.log "\n\n\n******************************************"
            // console.log "LINKS:"
            // console.log JSON.stringify elements, null, 2

            // Solution level links (external links) are old style
            links = this.convertLinks(elements, solutionURN);
            // console.log "CONVERTED LINKS:"
            // console.log JSON.stringify links, null, 2
            // console.log "******************************************\n\n\n"
            return links;
          } else {
            return elements;
          }
        }).catch(err => {
          var errMsg;
          errMsg = `Unable to get links for deployment ${topDeploymentName}: ${err.message} ${err.stack}`;
          this.logger.error(`${meth} - ERROR: ${errMsg}`);
          return q.reject(new Error(errMsg));
        });
      }

      getDeploymentLinks(deplManifest, convertLinks = false) {
        var deplName, deplURN, filter, meth;
        deplName = deplManifest.name;
        deplURN = deplManifest.urn;
        meth = `PlannerStub.getDeploymentLinks() Name: ${deplName} URN: ${deplURN}`;
        this.logger.info(meth);
        // Filter on the existence of a label named as the deployment
        filter = {
          [`${deplName}`]: '*'
        };
        return this.manifestRepository.listElementType('links', null, filter).then(elements => {
          var links;
          if (Object.keys(elements).length > 0) {
            this.logger.info(`${meth} - Found links for deployment ${deplName}.`);
          }
          if (convertLinks) {
            // console.log "\n\n\n******************************************"
            // console.log "LINKS:"
            // console.log JSON.stringify elements, null, 2
            links = this.convertLinks(elements, deplURN);
            // console.log "CONVERTED LINKS:"
            // console.log JSON.stringify links, null, 2
            // console.log "******************************************\n\n\n"
            return links;
          } else {
            return elements;
          }
        }).catch(err => {
          var errMsg;
          errMsg = `Unable to get links for deployment ${deplName}: ${err.message} ${err.stack}`;
          this.logger.error(`${meth} - ERROR: ${errMsg}`);
          return q.reject(new Error(errMsg));
        });
      }

      convertLinks(links, referenceDeploymentURN) {
        var linkData, linkName, meth, newLinks;
        meth = 'PlannerStub.convertLinks';
        this.logger.debug(`${meth} ReferenceURN: ${referenceDeploymentURN} - Links: ${JSON.stringify(links)}`);
        // We currently support two link formats, for backwards compatibility.
        newLinks = {};
        for (linkName in links) {
          linkData = links[linkName];
          if ('endpoints' in linkData) {
            this.convertLinkOld(linkData, referenceDeploymentURN, newLinks);
          } else if ('s_d' in linkData) {
            this.convertLinkNew(linkData, referenceDeploymentURN, newLinks);
          } else {
            this.logger.error(`${meth} - ERROR: invalid link format.`);
          }
        }
        this.logger.debug(`${meth} Converted links: ${JSON.stringify(newLinks)}`);
        return newLinks;
      }

      convertLinkNew(linkData, referenceDeploymentURN, newLinks) {
        var deploymentName, deploymentRef, err, meth, sourceDeploymentURN, targetDeploymentURN;
        meth = 'PlannerStub.convertLinkNew';
        this.logger.debug(`${meth} ReferenceURN: ${referenceDeploymentURN} - Links: ${JSON.stringify(linkData)}`);
        // SOURCE FORMAT (ECLOUD):

        //  {
        //      "kl-c0962e640495cf45d78d073f03ae84ea6d50d169": {
        //          "s_d": "calchazel_deployment",
        //          "s_c": "hazel.management",
        //          "t_d": "calchazel_deployment.hazel",
        //          "t_c": "management",
        //          "meta": {},
        //          "name": "kl-c0962e640495cf45d78d073f03ae84ea6d50d169",
        //          "owner": "devel__arroba__kumori.cloud"
        //      },
        //      "kl-de0683b867a4df1851219838a649fc17b0325aa7": {
        //          "s_d": "calchazel_deployment",
        //          "s_c": "hazel.data",
        //          "t_d": "calchazel_deployment.hazel",
        //          "t_c": "data",
        //          "meta": {},
        //          "name": "kl-de0683b867a4df1851219838a649fc17b0325aa7",
        //          "owner": "devel__arroba__kumori.cloud"
        //      }
        //  }

        // EXPECTED FORMAT:

        // "http-acs": {
        //   "slap://eslap.cloud/deployments/20190911_133549/9b914f9c": {
        //     "service-acs": {}
        //   },
        //   "slap://eslap.cloud/deployments/20190911_133552/ae241834": {
        //     "frontend": {}
        //   }
        // }

        // Extract short deployment name from the URN
        deploymentRef = this.manifestHelper.urnToRef(referenceDeploymentURN);
        deploymentName = deploymentRef.name;
        try {
          sourceDeploymentURN = this.manifestHelper.refToUrn({
            kind: deploymentRef.kind,
            domain: deploymentRef.domain,
            name: linkData['s_d']
          });
          targetDeploymentURN = this.manifestHelper.refToUrn({
            kind: deploymentRef.kind,
            domain: deploymentRef.domain,
            name: linkData['t_d']
          });
          if (sourceDeploymentURN === referenceDeploymentURN) {
            if (!(linkData['s_c'] in newLinks)) {
              newLinks[linkData['s_c']] = {};
            }
            if (!(targetDeploymentURN in newLinks[linkData['s_c']])) {
              newLinks[linkData['s_c']][targetDeploymentURN] = {};
            }
            newLinks[linkData['s_c']][targetDeploymentURN][linkData['t_c']] = {};
          }
          if (targetDeploymentURN === referenceDeploymentURN) {
            if (!(linkData['t_c'] in newLinks)) {
              newLinks[linkData['t_c']] = {};
            }
            if (!(sourceDeploymentURN in newLinks[linkData['t_c']])) {
              newLinks[linkData['t_c']][sourceDeploymentURN] = {};
            }
            newLinks[linkData['t_c']][sourceDeploymentURN][linkData['s_c']] = {};
          }
        } catch (error) {
          err = error;
          this.logger.error(`${meth} ERROR: ${err.message} - ${err.stack}`);
        }
        // This method modifies the provided object newLinks
        return true;
      }

      convertLinkOld(linkData, referenceDeploymentURN, newLinks) {
        var depl0URN, depl1URN, eps, err, meth;
        meth = 'PlannerStub.convertLinkOld';
        this.logger.debug(`${meth} ReferenceURN: ${referenceDeploymentURN} - Links: ${JSON.stringify(linkData)}`);
        try {
          // for code legibility
          // SOURCE FORMAT (ECLOUD):

          // {
          //   "name": "eslap://eslap.cloud/links/kl-e913a2a4e9b5268cda2e3cbd870fa144e5cce769",
          //   "owner": "alice@keycloak.org",
          //   "endpoints": [
          //     {
          //       "channel": "",
          //       "deployment": "eslap://eslap.cloud/httpinbounds/20191114_162738-cb1bf5f9-kh"
          //     },
          //     {
          //       "channel": "http-admission",
          //       "deployment": "eslap://jferrer.dev.testing/deployments/20191114_162738-d5f9c1b8-kd"
          //     }
          //   ],
          //   "spec": "http://eslap.cloud/manifest/link/1_0_0"
          // }

          // EXPECTED FORMAT:

          // "http-acs": {
          //   "slap://eslap.cloud/deployments/20190911_133549/9b914f9c": {
          //     "service-acs": {}
          //   },
          //   "slap://eslap.cloud/deployments/20190911_133552/ae241834": {
          //     "frontend": {}
          //   }
          // }
          eps = linkData.endpoints;
          this.logger.debug(`${meth} Link endpoints: ${JSON.stringify(eps)}`);
          depl0URN = this.manifestHelper.refToUrn({
            kind: eps[0].kind,
            domain: eps[0].domain,
            name: eps[0].name
          });
          depl1URN = this.manifestHelper.refToUrn({
            kind: eps[1].kind,
            domain: eps[1].domain,
            name: eps[1].name
          });
          this.logger.debug(`${meth} Deployment1 URN: ${depl0URN}`);
          this.logger.debug(`${meth} Deployment2 URN: ${depl1URN}`);
          if (depl0URN === referenceDeploymentURN) {
            if (!(eps[0].channel in newLinks)) {
              newLinks[eps[0].channel] = {};
            }
            if (!(depl1URN in newLinks[eps[0].channel])) {
              newLinks[eps[0].channel][depl1URN] = {};
            }
            newLinks[eps[0].channel][depl1URN][eps[1].channel] = {
              meta: linkData.meta || {}
            };
          }
          if (depl1URN === referenceDeploymentURN) {
            if (!(eps[1].channel in newLinks)) {
              newLinks[eps[1].channel] = {};
            }
            if (!(depl0URN in newLinks[eps[1].channel])) {
              newLinks[eps[1].channel][depl0URN] = {};
            }
            newLinks[eps[1].channel][depl0URN][eps[0].channel] = {
              meta: linkData.meta || {}
            };
          }
        } catch (error) {
          err = error;
          this.logger.error(`${meth} ERROR: ${err.message} - ${err.stack}`);
        }
        // This method modifies the provided object newLinks
        return true;
      }

      // Look for Resources objects in an object at any depth
      extractResources(obj, context, resourceList = []) {
        var k, keys, normResRef, ref1, resURN, v, validKinds;
        validKinds = this.manifestHelper.getValidResourceKinds();
        if (obj == null) {
          return resourceList;
        }
        if ('object' !== typeof obj) {
          return resourceList;
        }
        keys = Object.keys(obj);
        if (keys.length === 1 && (ref1 = keys[0], indexOf.call(validKinds, ref1) >= 0)) {
          // It's a resource object, only keep resource references (strings) not
          // inline resrouce definitions (objects)
          if ('string' === typeof obj[keys[0]] && obj[keys[0]] !== '') {
            // First and only key (0) is the resource type
            // The object of that key is the resource reference

            // Normalize reference (add a domain if it doesn't include one)
            normResRef = this.normalizeResourceReference(obj[keys[0]], context);
            resURN = 'eslap://' + normResRef.replace('/', `/${keys[0]}/`);
            if (indexOf.call(resourceList, resURN) < 0) {
              resourceList.push(resURN);
            }
          }
        } else {
          for (k in obj) {
            v = obj[k];
            this.extractResources(v, context, resourceList);
          }
        }
        return resourceList;
      }

      // Add a domain to a reference if it doesn't have one. The added domain is the
      // current user ID.
      normalizeResourceReference(resourceReference, context) {
        var newResourceReference;
        if (resourceReference.indexOf('/') > 0) {
          newResourceReference = resourceReference;
        } else {
          newResourceReference = `${context.user.id}/${resourceReference}`;
        }
        return newResourceReference;
      }

      isInboundBuiltin(deploymentManifest) {
        var ref1, ref2, ref3;
        if (((ref1 = deploymentManifest.artifact) != null ? (ref2 = ref1.description) != null ? ref2.builtin : void 0 : void 0) && ((ref3 = deploymentManifest.artifact) != null ? ref3.ref : void 0) != null && deploymentManifest.artifact.ref.kind === 'service' && deploymentManifest.artifact.ref.domain === 'kumori.systems' && deploymentManifest.artifact.ref.name === 'inbound') {
          return true;
        }
        return false;
      }

    };

    quantityToScalar = function (quantity) {
      var cleanNumberStr, num, number;
      if (quantity == null) {
        return 0;
      }
      if (quantity.endsWith('m')) {
        // Rounded to two decimals
        cleanNumberStr = quantity.substr(0, quantity.length - 1);
        number = parseInt(cleanNumberStr, 10) / 1000.0;
        return Math.round(number * 100) / 100;
      }
      if (quantity.endsWith('Ki')) {
        cleanNumberStr = quantity.substr(0, quantity.length - 2);
        return parseInt(cleanNumberStr, 10) * 1024;
      }
      if (quantity.endsWith('K')) {
        cleanNumberStr = quantity.substr(0, quantity.length - 1);
        return parseInt(cleanNumberStr, 10) * 1000;
      }
      if (quantity.endsWith('Mi')) {
        cleanNumberStr = quantity.substr(0, quantity.length - 2);
        return parseInt(cleanNumberStr, 10) * 1024 * 1024;
      }
      if (quantity.endsWith('M')) {
        cleanNumberStr = quantity.substr(0, quantity.length - 1);
        return parseInt(cleanNumberStr, 10) * 1000 * 1000;
      }
      if (quantity.endsWith('Gi')) {
        cleanNumberStr = quantity.substr(0, quantity.length - 2);
        return parseInt(cleanNumberStr, 10) * 1024 * 1024 * 1024;
      }
      if (quantity.endsWith('G')) {
        cleanNumberStr = quantity.substr(0, quantity.length - 1);
        return parseInt(cleanNumberStr, 10) * 1024 * 1000 * 1000;
      }
      if (quantity.endsWith('Ti')) {
        cleanNumberStr = quantity.substr(0, quantity.length - 2);
        return parseInt(cleanNumberStr, 10) * 1024 * 1024 * 1024 * 1024;
      }
      if (quantity.endsWith('T')) {
        cleanNumberStr = quantity.substr(0, quantity.length - 1);
        return parseInt(cleanNumberStr, 10) * 1024 * 1000 * 1000 * 1000;
      }
      if (quantity.endsWith('Pi')) {
        cleanNumberStr = quantity.substr(0, quantity.length - 2);
        return parseInt(cleanNumberStr, 10) * 1024 * 1024 * 1024 * 1024 * 1024;
      }
      if (quantity.endsWith('P')) {
        cleanNumberStr = quantity.substr(0, quantity.length - 1);
        return parseInt(cleanNumberStr, 10) * 1024 * 1000 * 1000 * 1000 * 1000;
      }
      num = parseInt(quantity, 10);
      if (isNaN(num)) {
        throw new Error(`Unknown quantity ${quantity}`);
      } else {
        return num;
      }
    };

    return PlannerStub;
  }.call(this);

  module.exports = PlannerStub;
}).call(undefined);