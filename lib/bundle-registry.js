'use strict';

(function () {
  /*
  * Copyright 2022 Kumori Systems S.L.
   * Licensed under the EUPL, Version 1.2 or – as soon they
    will be approved by the European Commission - subsequent
    versions of the EUPL (the "Licence");
   * You may not use this work except in compliance with the
    Licence.
   * You may obtain a copy of the Licence at:
     https://joinup.ec.europa.eu/software/page/eupl
   * Unless required by applicable law or agreed to in
    writing, software distributed under the Licence is
    distributed on an "AS IS" basis,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
    express or implied.
   * See the Licence for the specific language governing
    permissions and limitations under the Licence.
   */
  var BLOB_PREFIX,
      BUNDLES_FILENAME,
      BUNDLE_PREFIX,
      BundleRegistry,
      CODE_NOT_REQUIRES_BLOB,
      DEFAULT_CODE_BLOB_NAME,
      DEPLOYMENT_PREFIX,
      DOT,
      EventEmitter,
      Git,
      KMV3_DEPL_PREFIX,
      LINK_PREFIX,
      MANIFEST_FILENAME,
      METADATA_FILENAME,
      ManifestHelper,
      ManifestStoreFactory,
      ManifestValidator,
      MockStorage,
      NEEDLE_HTTP_OPTIONS,
      REGISTRATION_PRECEDENCE,
      RESOURCE_PREFIX,
      SOLUTION_PREFIX,
      TEMPORARY_CONTEXT_PREFIX,
      TEST_PREFIX,
      UNKNOWN_PREFIX,
      ZIP_EXT,
      archiver,
      fs,
      metadataFromContext,
      mkdirp,
      moment,
      needle,
      path,
      q,
      rimraf,
      url,
      utils,
      indexOf = [].indexOf;

  q = require('q');

  fs = require('fs');

  Git = require('simple-git');

  url = require('url');

  path = require('path');

  rimraf = require('rimraf');

  mkdirp = require('mkdirp');

  needle = require('needle');

  archiver = require('archiver');

  moment = require('moment');

  EventEmitter = require('events').EventEmitter;

  ManifestHelper = require('./manifest-helper');

  ManifestValidator = require('./manifest-validator-mock');

  ManifestStoreFactory = require('./manifest-store-factory');

  utils = require('./utils');

  MANIFEST_FILENAME = 'Manifest.json';

  METADATA_FILENAME = 'metadata.json';

  BUNDLES_FILENAME = 'Bundles.json';

  // DEFAULT_CODE_BLOB_NAME = 'code.zip'
  DEFAULT_CODE_BLOB_NAME = 'image.tgz';

  DOT = '.';

  ZIP_EXT = '.zip';

  TEMPORARY_CONTEXT_PREFIX = 'temporary';

  NEEDLE_HTTP_OPTIONS = {
    decode_response: false
  };

  // MUST BE OF LENGTH 4
  RESOURCE_PREFIX = 'reso';

  DEPLOYMENT_PREFIX = 'depl';

  BUNDLE_PREFIX = 'bund';

  BLOB_PREFIX = 'blob';

  TEST_PREFIX = 'test';

  LINK_PREFIX = 'link';

  UNKNOWN_PREFIX = 'unkn';

  KMV3_DEPL_PREFIX = "kmv3";

  SOLUTION_PREFIX = "solu";

  // Prefixes in `code` key in manifests which not require a blob registration.
  CODE_NOT_REQUIRES_BLOB = ['docker:'];

  REGISTRATION_PRECEDENCE = [RESOURCE_PREFIX, SOLUTION_PREFIX, DEPLOYMENT_PREFIX, KMV3_DEPL_PREFIX, LINK_PREFIX, TEST_PREFIX, BUNDLE_PREFIX, BLOB_PREFIX, UNKNOWN_PREFIX];

  // FOR KUMORI V2, ONLY MANIFESTS WILL BE TAKEN INTO ACCOUNT, SINCE THERE WON'T BE
  // CODE COMPONENT OR RUNTIME BLOBS ANYMORE.
  // SERVICE BLOBS (CONTAINING SPREAD FUNCTIONS) WON'T BE SUPPORTED FOR V2.0, AND
  // LATER SUPPORT WILL BE EVALUATED.

  // - ManifestStore: now it's K8S API Server and etcd
  // - The bundle zip will be recursively extracted to come up with a list of JSON
  //   manifests.
  MockStorage = class MockStorage {
    constructor() {}

    put() {
      console.log("MockStorage.put - THIS SHOULD NEVER BE CALLED");
      return q();
    }

    deleteDir() {
      console.log("MockStorage.deleteDir - THIS SHOULD NEVER BE CALLED");
      return q();
    }

  };

  BundleRegistry = class BundleRegistry extends EventEmitter {
    constructor(storage, manifestRepository, maxBundleSize, tmpPath) {
      var meth;
      super();
      this.storage = storage;
      this.manifestRepository = manifestRepository;
      this.maxBundleSize = maxBundleSize;
      this.tmpPath = tmpPath;
      meth = 'BundleRegistry.constructor';
      this.validator = new ManifestValidator();
      this.manifestHelper = new ManifestHelper();
      if (this.storage == null) {
        this.storage = new MockStorage();
      }
      this.pstat = q.denodeify(fs.stat);
      this.prename = q.denodeify(fs.rename);
      this.preaddir = q.denodeify(fs.readdir);
      this.primraf = q.denodeify(rimraf);
      this.pmkdirp = q.denodeify(mkdirp);
      this.pchmod = q.denodeify(fs.chmod);
      this.pwritefile = q.denodeify(fs.writeFile);
      this.folderCounter = 0;
      this.regTokenCounter = 0;
      this.regLog = {};
      this.globalContexts = {};
    }

    init() {
      return this.validator.init();
    }

    close() {
      var ref;
      if ((ref = this.storage) != null ? ref.close : void 0) {
        return this.storage.close();
      }
    }

    registerBundleJson(context, jsonPath) {
      var regToken, workDirs;
      this.logger.info(`Registering Bundle.json: ${jsonPath}...`);
      regToken = this._initRegistration(context);
      workDirs = null;
      return this._initTemporaryDirs().then(dirs => {
        workDirs = dirs;
        this._addWorkDirs(regToken, workDirs);
        return this._processBundlesFile(jsonPath, workDirs.raw, regToken);
      }).then(() => {
        return this.processDirectory(workDirs, regToken);
      }).then(() => {
        this.logger.debug(`Bundle.json registration for ${jsonPath} finished.`);
        this._removeWorkDirs(regToken);
        return this.regLog[regToken];
      }).fail(err => {
        this.logger.warn(`Registering Bundle.json: ${err.message}`);
        this._addErrorMessage(regToken, `Registering Bundles.json: ${err.message}`);
        this._removeWorkDirs(regToken);
        return q(this.regLog[regToken]);
      }).finally(() => {
        this.logger.debug('Registration process finished, cleaning up temp files.');
        this._endRegistration(regToken);
        if ((workDirs != null ? workDirs.top : void 0) != null) {
          return this._cleanup(workDirs.top);
        }
      });
    }

    registerBundleZip(context, zipPath) {
      var regToken, workDirs;
      this.logger.info(`Registering Bundle ZIP file: ${zipPath}...`);
      this.logger.info(`Registering Bundle ZIP: CONTEXT: ${JSON.stringify(context)}...`);
      regToken = this._initRegistration(context);
      workDirs = null;
      return this._initTemporaryDirs().then(dirs => {
        workDirs = dirs;
        this._addWorkDirs(regToken, workDirs);
        this.logger.info(`Extracting zip file : ${zipPath} to ${workDirs.raw}...`);
        return utils.unzip(zipPath, workDirs.raw, this.maxBundleSize);
      }).then(() => {
        return this.processDirectory(workDirs, regToken);
      }).then(() => {
        this.logger.debug(`ZIP Bundle registration for ${zipPath} finished.`);
        this._removeWorkDirs(regToken);
        return this.regLog[regToken];
      }).fail(err => {
        this.logger.warn(`Registering zip bundle: ${err.message} ${err.stack}`);
        this._addErrorMessage(regToken, `Registering zip bundle: ${err.message}`);
        this._removeWorkDirs(regToken);
        return q(this.regLog[regToken]);
      }).finally(() => {
        this.logger.debug('Registration process finished, cleaning up temp files.');
        this._endRegistration(regToken);
        if ((workDirs != null ? workDirs.top : void 0) != null) {
          return this._cleanup(workDirs.top);
        }
      });
    }

    processDirectory(workDirs, regToken) {
      return this.preprocessDirectory(workDirs, regToken).then(() => {
        return this.processCleanDirectory(workDirs, regToken);
      });
    }

    //#############################################################################
    //#       PRIVATE METHODS - SHOULD NOT BE CALLED FROM OUTSIDE THE CLASS      ##
    //#############################################################################
    preprocessDirectory(workDirs, regToken) {
      var bundlesFile, currentDir, topWorkDirs;
      this.logger.debug(`Pre-processing raw directory ${workDirs.raw} ...`);
      currentDir = workDirs.raw;
      topWorkDirs = this._getWorkDirs(regToken);
      // STEP 1: If Bundles.json, bring bundles to current dir
      bundlesFile = path.join(currentDir, BUNDLES_FILENAME);
      return utils.checkFileExists(bundlesFile).then(exists => {
        if (exists) {
          this.logger.debug("Bundles.json file found, let's process it");
          return this._processBundlesFile(bundlesFile, currentDir).then(function () {
            return utils.deleteFile(bundlesFile);
          }).then(() => {
            return this._checkDirSize(topWorkDirs.raw, this.maxBundleSize); // Now, is too big?
          });
        } else {
          return q();
        }
      }).then(() => {
        // STEP 2: If any ZIP files, extract them into temporary directories
        return this._getDirContents(currentDir);
      }).then(dirContents => {
        var i, len, promiseChain, ref, zipFile;
        promiseChain = q();
        ref = dirContents.files;
        for (i = 0, len = ref.length; i < len; i++) {
          zipFile = ref[i];
          if (utils.endsWith(zipFile, ZIP_EXT)) {
            (zipFile => {
              return promiseChain = promiseChain.finally(() => {
                var tmpDir, zipFilePath;
                zipFilePath = path.join(currentDir, zipFile);
                tmpDir = null;
                return this._createTemporarySubdir(currentDir, this._getFilenameNoExtension(zipFile)).then(function (_tmpDir) {
                  tmpDir = _tmpDir;
                  return utils.getDirSize(topWorkDirs.raw);
                }).then(topWorkDirRawSize => {
                  var unzipMaxSize;
                  this.logger.info(`Extracting zip file: ${zipFilePath} to ${tmpDir}...`);
                  unzipMaxSize = this.maxBundleSize - topWorkDirRawSize;
                  return utils.unzip(zipFilePath, tmpDir, unzipMaxSize);
                }).fail(err => {
                  var message;
                  message = `Extracting zip file: ${zipFilePath}: ${err.message}`;
                  this.logger.warn(message);
                  // Error is stored, and continues processing files
                  this._addErrorMessage(regToken, message);
                  if (tmpDir != null) {
                    return this.primraf(tmpDir);
                  } else {
                    return q();
                  }
                }).finally(function () {
                  return utils.deleteFile(zipFilePath);
                });
              });
            })(zipFile);
          }
        }
        return promiseChain;
      }).then(() => {
        var manifestPath;
        // STEP 3: If there is a bundle, move it to the "clean" directory
        manifestPath = path.join(currentDir, MANIFEST_FILENAME);
        return utils.checkFileExists(manifestPath).then(exists => {
          var filesToMove, manifest;
          if (exists) {
            filesToMove = [MANIFEST_FILENAME];
            manifest = null;
            return utils.jsonFromFile(manifestPath).then(pmanifest => {
              manifest = pmanifest;
              if (this._isSolution(manifest)) {
                this.logger.debug("Detected solution manifest. Dont touch it!");
                this.logger.debug(`MANIFEST: ${JSON.stringify(manifest)}`);
                return q(null);
              } else if (this._isKmv3Deployment(manifest)) {
                this.logger.debug("Detected kmv3 deployment manifest. Dont touch it!");
                this.logger.debug(`MANIFEST: ${JSON.stringify(manifest)}`);
                return q(null);
              } else if (manifest.spec == null) {
                return q.reject(new Error('Manifest file has no required spec attribute.'));
              } else if (this._requiresBlob(manifest)) {
                return this._findCode(manifest.code, currentDir);
              } else {
                return q(null);
              }
            }).then(codeBundleDir => {
              if (codeBundleDir != null) {
                filesToMove.push(codeBundleDir);
              }
              return this._createTemporarySubdir(workDirs.clean, this._getPrefixFromType(manifest) + '_' + this.folderCounter++);
            }).then(cleanBundleDir => {
              return this._moveFiles(filesToMove, currentDir, cleanBundleDir);
            }).fail(err => {
              var errMsg;
              errMsg = (manifest != null ? manifest.name : void 0) != null ? `Moving bundle ${manifest.name}: ${err.message}` : `Moving bundle from ${manifestPath}: ${err.message}`;
              this.logger.warn(errMsg, err.stack);
              this._addErrorMessage(regToken, errMsg);
              return q();
            });
          } else {
            return q();
          }
        });
      }).then(() => {
        // STEP 4: Walk all sub-directories and pre-pocess them recursively
        return this._getDirContents(currentDir).then(dirContents => {
          var dir, i, len, promiseChain, ref;
          promiseChain = q();
          ref = dirContents.dirs;
          for (i = 0, len = ref.length; i < len; i++) {
            dir = ref[i];
            if (!utils.startsWith(dir, DOT)) {
              (dir => {
                return promiseChain = promiseChain.finally(() => {
                  var dirPath, subdirWorkDirs;
                  dirPath = path.join(currentDir, dir);
                  subdirWorkDirs = {
                    raw: dirPath,
                    clean: workDirs.clean
                  };
                  return this.preprocessDirectory(subdirWorkDirs, regToken);
                });
              })(dir);
            }
          }
          return promiseChain.then(() => {
            this.logger.info(`All subdirs explored. Deleting dir ${currentDir}.`);
            return this._cleanup(currentDir);
          });
        });
      });
    }

    processCleanDirectory(workDirs, regToken) {
      var cleanDir, context;
      this.logger.info(`Registering detected bundles in ${workDirs.clean}...`);
      context = {
        id: null,
        regToken: regToken
      };
      cleanDir = workDirs.clean;
      return this._getDirContents(cleanDir).then(dirContents => {
        var dir, i, j, len, len1, promiseChain, ref, ref1;
        if (dirContents.dirs.length === 0) {
          this.logger.info('Processing clean directories: nothing to analyze.');
          return q();
        } else {
          // Sort the bundles according to a predefined order, to avoid
          // inconsistencies in complex bundles.
          dirContents.dirs.sort(this._getPrecedenceSorter());
          ref = dirContents.dirs;
          for (i = 0, len = ref.length; i < len; i++) {
            dir = ref[i];
            if (TEST_PREFIX === this._getCleanDirPrefix(dir)) {
              if (context.id == null) {
                context = this._generateContext(regToken);
                this.logger.debug(`Created new temporary context: ${context.id}`);
              }
            }
          }
          promiseChain = q();
          ref1 = dirContents.dirs;
          for (j = 0, len1 = ref1.length; j < len1; j++) {
            dir = ref1[j];
            (dir => {
              this.logger.debug(`Registering bundle in: ${dir}`);
              return promiseChain = promiseChain.finally(() => {
                var elementType, fullDir;
                elementType = this._getCleanDirPrefix(dir);
                this.logger.debug(`ElementType: ${elementType}`);
                fullDir = path.join(cleanDir, dir);
                return this.processElementFromDir(elementType, fullDir, context).then(() => {
                  return this._cleanup(fullDir);
                }).fail(err => {
                  this.logger.warn(`Processing bundle in ${dir}: ${err.message}`);
                  this._addErrorMessage(regToken, `Processing bundle in ${dir}: ${err.message}`);
                  return q();
                });
              });
            })(dir);
          }
          return promiseChain.then(() => {
            this._setTemporaryContext(regToken, context);
            this.logger.debug('Bundle registration finished.');
            return q();
          });
        }
      });
    }

    processElementFromDir(type, dir, context) {
      switch (type) {
        case RESOURCE_PREFIX:
          return this.registerElement(dir, context);
        case DEPLOYMENT_PREFIX:
          return this.deferElement(dir, context, type);
        case KMV3_DEPL_PREFIX:
          return this.deferElement(dir, context, type);
        case SOLUTION_PREFIX:
          return this.deferElement(dir, context, type);
        case LINK_PREFIX:
          return this.deferElement(dir, context, type);
        case TEST_PREFIX:
          return this.deferElement(dir, context, type);
        default:
          this.logger.error(`Tried to register element of type: ${type}.`);
          return q.reject(new Error(`Tried to register element of type: ${type}.`));
      }
    }

    _validateManifest(manifest) {
      var data, ref;
      data = (ref = manifest.name) != null ? ref : manifest.spec;
      this.logger.info(`Validating manifest: ${data}`);
      return q.Promise((resolve, reject) => {
        var e, errMsg;
        try {
          if (this._isSolution(manifest)) {
            this.logger.info('Skipping validation of solution manifest');
          } else if (this._isKmv3Deployment(manifest)) {
            this.logger.info('Skipping validation of kmv3 deployment manifest');
          } else {
            this.validator.validateManifest(manifest);
          }
          return resolve(manifest);
        } catch (error1) {
          e = error1;
          errMsg = 'Manifest failed validation: ';
          if (e.validationErrors != null) {
            errMsg += e.validationErrors.toString();
          } else {
            errMsg += e.message;
          }
          this.logger.warn(`Manifest ${data} failed validation: ${errMsg}`);
          return reject(new Error(errMsg));
        }
      });
    }

    deferElement(dir, context, type) {
      var elementManifest, elementManifestPath;
      this.logger.debug(`DeferElement: ${dir}`);
      elementManifest = null;
      elementManifestPath = path.join(dir, MANIFEST_FILENAME);
      return utils.jsonFromFile(elementManifestPath).then(manifest => {
        elementManifest = manifest;
        return this._validateManifest(elementManifest);
      }).then(() => {
        var manifest;
        manifest = this.contextualizeManifest(context, elementManifest);
        if (type === DEPLOYMENT_PREFIX) {
          this._addPendingDeployment(context.regToken, manifest);
          this.logger.debug('Added deployment to pending actions list');
        } else if (type === LINK_PREFIX) {
          this._addPendingLink(context.regToken, manifest);
          this.logger.debug('Added link services to pending actions list');
        } else if (type === TEST_PREFIX) {
          this._addPendingTest(context.regToken, manifest);
          this.logger.debug('Added test to pending actions list');
        } else if (type === SOLUTION_PREFIX) {
          this._addPendingSolution(context.regToken, manifest);
          this.logger.debug('Added solution to pending actions list');
        } else if (type === KMV3_DEPL_PREFIX) {
          this._addPendingDeployment(context.regToken, manifest);
          this.logger.debug('Added kmv3 deployment to pending actions list');
        } else {
          this.logger.warn(`Could not add pending action of type: ${type}`);
        }
        return q();
      }).fail(err => {
        this.logger.warn(`Defering element in ${dir}: ${err.message}`);
        this._addErrorMessage(context.regToken, `Defering element in ${dir}: ${err.message}`);
        return q();
      });
    }

    registerElement(dir, context, requiresCode = false) {
      var elementManifest, elementManifestPath;
      this.logger.debug(`RegisterElement: ${dir}`);
      elementManifest = null;
      elementManifestPath = path.join(dir, MANIFEST_FILENAME);
      return utils.jsonFromFile(elementManifestPath).then(manifest => {
        elementManifest = manifest;
        return this._validateManifest(elementManifest);
      }).then(() => {
        this.logger.debug(`Registering element: ${elementManifest.name}`);
        return this.elementAlreadyInStorage(context, elementManifest);
      }).then(alreadyRegistered => {
        var ref;
        if (alreadyRegistered) {
          return q.reject(new Error(`Element ${elementManifest.name} already in storage.`));
        } else {
          if (!((ref = elementManifest.code) != null ? ref.length : void 0)) {
            if (requiresCode) {
              return q.reject(new Error(`Element ${elementManifest.name} is missing required code value.`));
            } else {
              return q(null);
            }
          } else {
            // @getBlobZipFile elementManifest, dir
            return this.getBlobTgzFile(elementManifest, dir, context).then(blobZipFile => {
              if (blobZipFile) {
                // Add a code locator to element manifest to facilitate
                // storing/retrieving the blob code from a storage.
                elementManifest.codelocator = elementManifest.name + '/' + blobZipFile.name;
                this.logger.debug(`Added codelocator to manifest: ${elementManifest.codelocator}`);
                return blobZipFile.path;
              } else {
                return q(null);
              }
            });
          }
        }
      }).then(blobZipFilePath => {
        return this.storeElement(context, elementManifest, blobZipFilePath);
      }).then(elementName => {
        this.logger.info(`Registered element: ${elementName}`);
        this._addRegisteredMessage(context.regToken, `Registered element: ${elementName}`);
        return q();
      }).fail(err => {
        this.logger.warn(`Registering bundle in ${dir}: ${err.stack}`);
        this._addErrorMessage(context.regToken, `Registering bundle in ${dir}: ${err.message}`);
        return q();
      });
    }

    prefixURN(prefix, urn) {
      if (prefix === null || prefix === '') {
        return urn;
      } else {
        return urn.replace('://', '://' + prefix + '/');
      }
    }

    replaceAll(str, find, replacement) {
      return str.split(find).join(replacement);
    }

    contextualizeManifest(context, manifest) {
      var manifestString, prefix, ref, subKey, subValue;
      if (context.id == null) {
        return manifest;
      }
      this.logger.info(`Contextualizing to prefix: ${context.storagePrefix}`, context);
      prefix = context.storagePrefix;
      // For deployments, do not change their names, since they might be referenced
      // by test manifests, and it's a user generated name.
      if (!this._isDeploymentManifest(manifest) && !this._isSolution(manifest) && !this._isKmv3Deployment(manifest)) {
        if (manifest.name != null) {
          context.substitutions[manifest.name] = this.prefixURN(prefix, manifest.name);
        }
        if (manifest.codelocator != null) {
          context.substitutions[manifest.codelocator] = this.prefixURN(prefix, manifest.codelocator);
        }
      }
      // Update reference to previously contextualized elements
      manifestString = JSON.stringify(manifest);
      ref = context.substitutions;
      for (subKey in ref) {
        subValue = ref[subKey];
        manifestString = this.replaceAll(manifestString, subKey, subValue);
      }
      manifest = JSON.parse(manifestString);
      return manifest;
    }

    storeElement(context, manifest, elementBlobPath = null) {
      var metadata, ref, storageKey;
      manifest = this.contextualizeManifest(context, manifest);
      metadata = metadataFromContext(this.globalContexts[context.regToken]);
      // Set element owner
      manifest.owner = metadata.owner;
      if (elementBlobPath != null) {
        storageKey = (ref = manifest.codelocator) != null ? ref : manifest.name;
        this.logger.debug(`Storing ${storageKey} with blob ${elementBlobPath}`);
        return utils.calculateChecksum(elementBlobPath).then(hash => {
          manifest.code = hash;
          return this.manifestRepository.storeManifest(manifest);
        }).then(() => {
          return this.storage.put(storageKey, elementBlobPath, manifest.code, false);
        }).then(function (info) {
          return q(manifest.name);
        });
      } else {
        this.logger.debug(`Storing ${manifest.name}`);
        return this.manifestRepository.storeManifest(manifest).then(() => {
          if (this._isResource(manifest)) {
            this.emit('resource', {
              timestamp: utils.getTimestamp(),
              owner: manifest.owner,
              source: 'admission',
              entity: {
                resource: manifest.name
              },
              type: 'resource',
              name: 'resourcecreated',
              data: manifest
            });
          }
          return q(manifest.name);
        });
      }
    }

    elementAlreadyInStorage(context, manifest) {
      manifest = this.contextualizeManifest(context, manifest);
      this.logger.debug(`Checking if element ${manifest.name} is already in storage`);
      // @manifestRepository.getManifest manifest.name
      return this.manifestRepository.checkElement(manifest.name).then(function () {
        return q(true);
      }).fail(function (err) {
        return q(false);
      });
    }

    getBlobTgzFile(elementManifest, dir, context) {
      var blobBundleDir, blobZipFileName, blobZipFilePath;
      blobZipFileName = null;
      blobZipFilePath = null;
      blobBundleDir = null;
      if (!this._requiresBlob(elementManifest)) {
        return q(null);
      }
      return this._getDirContents(dir).then(dirContents => {
        if (dirContents.dirs.length !== 1) {
          return q.reject(new Error(`More than one blob bundle directory for ${elementManifest.name}`));
        } else {
          blobBundleDir = path.join(dir, dirContents.dirs[0]);
          return this._getDirContents(blobBundleDir);
        }
      }).then(dirContents => {
        var blobManifest, blobManifestPath, zipFiles;
        zipFiles = dirContents.files.filter(function (x) {
          return utils.endsWith(x, ZIP_EXT);
        });
        blobManifest = null;
        blobManifestPath = path.join(blobBundleDir, MANIFEST_FILENAME);
        return utils.jsonFromFile(blobManifestPath).then(manifest => {
          var blobFolderPath, ref;
          blobManifest = manifest;
          if ((ref = blobManifest.hash) != null ? ref.trim().length : void 0) {
            // A hash attribute means that the code comes in a zip file
            if (zipFiles.length < 1) {
              return q.reject(new Error(`Expected blob zip file not found for ${elementManifest.name}`));
            } else if (zipFiles.length > 1) {
              return q.reject(new Error(`More than one blob zip file found for ${elementManifest.name}`));
            } else {
              blobZipFileName = zipFiles[0];
              blobZipFilePath = path.join(blobBundleDir, zipFiles[0]);
              return this._validateHash(blobZipFilePath, blobManifest.hash).then(matches => {
                var blobTgzFilePath;
                if (!matches) {
                  return q.reject(new Error(`Zip failed hash validation for ${elementManifest.name}`));
                } else {
                  this.logger.debug('CONVERTING ZIP BLOB TO TGZ BLOB');
                  this.logger.debug(`ZIP : ${blobZipFilePath}`);
                  this.logger.debug(`NAME: ${blobZipFileName}`);
                  blobTgzFilePath = blobZipFilePath.replace(blobZipFileName, DEFAULT_CODE_BLOB_NAME);
                  this.logger.debug(`TGZ : ${blobTgzFilePath}`);
                  return this._zipToTgz(blobZipFilePath, blobTgzFilePath, context).then(function () {
                    return {
                      name: DEFAULT_CODE_BLOB_NAME,
                      path: blobTgzFilePath
                    };
                  });
                }
              });
            }
          } else {
            // No hash attribute means that the code comes in a folder
            if (dirContents.dirs.length < 1) {
              return q.reject(new Error(`Expected blob folder not found for ${elementManifest.name}`));
            } else if (dirContents.dirs.length > 1) {
              return q.reject(new Error(`More than one blob folder found for ${elementManifest.name}`));
            } else {
              blobFolderPath = path.join(blobBundleDir, dirContents.dirs[0]);
              blobZipFileName = DEFAULT_CODE_BLOB_NAME;
              blobZipFilePath = path.join(blobBundleDir, DEFAULT_CODE_BLOB_NAME);
              return this._tgzDirectoryContents(blobFolderPath, blobZipFilePath).then(function () {
                return {
                  name: blobZipFileName,
                  path: blobZipFilePath
                };
              });
            }
          }
        });
      });
    }

    getBlobZipFile(elementManifest, dir) {
      var blobBundleDir, blobZipFileName, blobZipFilePath;
      blobZipFileName = null;
      blobZipFilePath = null;
      blobBundleDir = null;
      return this._getDirContents(dir).then(dirContents => {
        if (dirContents.dirs.length !== 1) {
          return q.reject(new Error(`More than one blob bundle directory for ${elementManifest.name}`));
        } else {
          blobBundleDir = path.join(dir, dirContents.dirs[0]);
          return this._getDirContents(blobBundleDir);
        }
      }).then(dirContents => {
        var blobManifest, blobManifestPath, zipFiles;
        zipFiles = dirContents.files.filter(function (x) {
          return utils.endsWith(x, ZIP_EXT);
        });
        blobManifest = null;
        blobManifestPath = path.join(blobBundleDir, MANIFEST_FILENAME);
        return utils.jsonFromFile(blobManifestPath).then(manifest => {
          var blobFolderPath, ref;
          blobManifest = manifest;
          if ((ref = blobManifest.hash) != null ? ref.trim().length : void 0) {
            // A hash attribute means that the code comes in a zip file
            if (zipFiles.length < 1) {
              return q.reject(new Error(`Expected blob zip file not found for ${elementManifest.name}`));
            } else if (zipFiles.length > 1) {
              return q.reject(new Error(`More than one blob zip file found for ${elementManifest.name}`));
            } else {
              blobZipFileName = zipFiles[0];
              blobZipFilePath = path.join(blobBundleDir, zipFiles[0]);
              return this._validateHash(blobZipFilePath, blobManifest.hash).then(function (matches) {
                if (!matches) {
                  return q.reject(new Error(`Zip failed hash validation for ${elementManifest.name}`));
                } else {
                  return {
                    name: blobZipFileName,
                    path: blobZipFilePath
                  };
                }
              });
            }
          } else {
            // No hash attribute means that the code comes in a folder
            if (dirContents.dirs.length < 1) {
              return q.reject(new Error(`Expected blob folder not found for ${elementManifest.name}`));
            } else if (dirContents.dirs.length > 1) {
              return q.reject(new Error(`More than one blob folder found for ${elementManifest.name}`));
            } else {
              blobFolderPath = path.join(blobBundleDir, dirContents.dirs[0]);
              blobZipFileName = DEFAULT_CODE_BLOB_NAME;
              blobZipFilePath = path.join(blobBundleDir, DEFAULT_CODE_BLOB_NAME);
              return this._zipDirectoryContents(blobFolderPath, blobZipFilePath).then(function () {
                return {
                  name: blobZipFileName,
                  path: blobZipFilePath
                };
              });
            }
          }
        });
      });
    }

    listTestContexts(context, isAdmin) {
      var tContexts;
      tContexts = {};
      return this.manifestRepository.list().then(manifests => {
        var c, i, k, len, m, pathT, promise, promises, v;
        for (i = 0, len = manifests.length; i < len; i++) {
          m = manifests[i];
          if (!m.startsWith('temporary')) {
            continue;
          }
          c = m.split('/')[1];
          if (tContexts[c] == null) {
            tContexts[c] = false;
          }
          if (m.endsWith('metadata.json')) {
            tContexts[c] = m;
          }
        }
        promise = q(true);
        if (!isAdmin) {
          promises = [];
          for (k in tContexts) {
            v = tContexts[k];
            if (!v) {
              continue;
            }
            pathT = `eslap://${path.dirname(v)}`;
            (k => {
              return promises.push(this.manifestRepository.getManifest(pathT, 'metadata.json').then(function (metadata) {
                if (metadata == null) {
                  return;
                }
                if (!(metadata.owner === context.user.id)) {
                  return delete tContexts[k];
                }
              }));
            })(k);
          }
          promise = q.all(promises);
        }
        return promise;
      }).then(function () {
        var k, results;
        results = [];
        for (k in tContexts) {
          results.push(k);
        }
        return results;
      });
    }

    releaseTestContext(context, contextURN, isAdmin) {
      var pathT, promise;
      this.logger.info(`Releasing test context ${contextURN}`);
      promise = q(true);
      if (!isAdmin) {
        pathT = contextURN.split('//')[1];
        promise = this.manifestRepository.list().then(manifests => {
          var i, len, m;
          for (i = 0, len = manifests.length; i < len; i++) {
            m = manifests[i];
            if (!m.startsWith(pathT)) {
              continue;
            }
            if (!m.endsWith('metadata.json')) {
              continue;
            }
            pathT = `eslap://${path.dirname(m)}`;
            return this.manifestRepository.getManifest(pathT, 'metadata.json');
          }
          return false;
        }).then(function (metadata) {
          if (metadata == null) {
            return;
          }
          if (!(metadata.owner === context.user.id)) {
            return q.reject(new Error(`Unauthorized access to ${contextURN}`));
          }
        });
      }
      return promise.then(() => {
        return this.storage.deleteDir(contextURN);
      }).then(() => {
        return this.manifestRepository.fetcher.deleteDir(contextURN);
      });
    }

    //#############################################################################
    //#  Class helpers methods                                                   ##
    //#############################################################################

    // This private method is used to determine if a code blob must be stored in
    // the internal storage or not. This is supposed to change
    _requiresBlob(elementManifest) {
      var i, len, prefix;
      // The element will not require a code blob if there isn't a code key in its
      // manifest
      if ((elementManifest != null ? elementManifest.code : void 0) == null) {
        return false;
      }
      // The element will not require a code blob if the code is stored in a docker
      // image
      for (i = 0, len = CODE_NOT_REQUIRES_BLOB.length; i < len; i++) {
        prefix = CODE_NOT_REQUIRES_BLOB[i];
        if ('string' !== typeof elementManifest.code) {
          return false;
        }
        if (elementManifest.code.startsWith(prefix)) {
          return false;
        }
      }
      // Otherwise, the element require a code blob
      return true;
    }

    // Create a new log for a registration operation, associated with a token
    _initRegistration(globalContext) {
      var newToken;
      newToken = this.regTokenCounter++;
      this.regLog[newToken] = {
        registered: [],
        errors: [],
        pendingActions: {
          solutions: [],
          deployments: [],
          links: [],
          tests: []
        },
        temporaryContext: null
      };
      // console.log "ADDING TO GLOBALCONTEXTS[#{newToken}]"
      this.globalContexts[newToken] = globalContext;
      return newToken;
    }

    // Dispose the registration log associated with a token
    _endRegistration(regToken) {
      delete this.regLog[regToken];
      return delete this.globalContexts[regToken];
    }

    // Add an error message to a registration log identified by the token
    _addErrorMessage(regToken, errorMessage) {
      return this.regLog[regToken].errors.push(errorMessage);
    }

    // Add a "registered" message to a registration log identified by the token
    _addRegisteredMessage(regToken, message) {
      return this.regLog[regToken].registered.push(message);
    }

    // Add a "pending" test manifest to a registration log identified by the token
    _addPendingTest(regToken, manifest) {
      return this.regLog[regToken].pendingActions.tests.push(manifest);
    }

    // Add a "pending" solution manifest to a registration log
    _addPendingSolution(regToken, manifest) {
      return this.regLog[regToken].pendingActions.solutions.push(manifest);
    }

    // Add a "pending" deployment manifest to a registration log
    _addPendingDeployment(regToken, manifest) {
      return this.regLog[regToken].pendingActions.deployments.push(manifest);
    }

    // Add a "pending" link manifest to a registration log
    _addPendingLink(regToken, manifest) {
      return this.regLog[regToken].pendingActions.links.push(manifest);
    }

    // Set a temporary context for all registry operations
    _setTemporaryContext(regToken, context) {
      if (context.id != null) {
        return this.regLog[regToken].temporaryContext = context;
      } else {
        return this.regLog[regToken].temporaryContext = null;
      }
    }

    _addWorkDirs(regToken, workDirs) {
      return this.regLog[regToken]._workDirs = workDirs;
    }

    _removeWorkDirs(regToken) {
      var ref;
      if (((ref = this.regLog[regToken]) != null ? ref._workDirs : void 0) != null) {
        return delete this.regLog[regToken]._workDirs;
      }
    }

    _getWorkDirs(regToken) {
      var ref;
      return (ref = this.regLog[regToken]) != null ? ref._workDirs : void 0;
    }

    // Cleans up a temporary directory
    _cleanup(tmpWorkDir) {
      this.logger.info(`Deleting temporary directory ${tmpWorkDir}`);
      return this.primraf(tmpWorkDir);
    }

    // Creates a set of temporary working directories
    _initTemporaryDirs() {
      var tmpDir, tmpDirClean, tmpDirRaw;
      tmpDir = path.join(this.tmpPath, utils.generateId());
      this.logger.info(`Creating temporary working directory: ${tmpDir}...`);
      tmpDirRaw = path.join(tmpDir, 'raw');
      tmpDirClean = path.join(tmpDir, 'clean');
      return this.pmkdirp(tmpDir).then(() => {
        return this.pmkdirp(tmpDirRaw);
      }).then(() => {
        return this.pmkdirp(tmpDirClean);
      }).then(function () {
        return {
          top: tmpDir,
          raw: tmpDirRaw,
          clean: tmpDirClean
        };
      });
    }

    // Create a temporary sub-directory of the given parent. The name is a
    // random string, which can be prefixed by a supplied string.
    _createTemporarySubdir(parentDir, prefix = null) {
      var tmpDir;
      if (prefix != null) {
        tmpDir = path.join(parentDir, prefix + '_' + utils.generateId());
      } else {
        tmpDir = path.join(parentDir, utils.generateId());
      }
      return this.pmkdirp(tmpDir).then(() => {
        this.logger.debug(`Created temporary subdir: ${tmpDir}...`);
        return tmpDir;
      });
    }

    // Generates a UUID without dashes (for elasticsearch search compatibility)
    _generateContext(regToken) {
      var context, uuid;
      uuid = utils.generateId();
      uuid = this.replaceAll(uuid, '-', '');
      context = {
        id: uuid,
        storagePrefix: path.join(TEMPORARY_CONTEXT_PREFIX, uuid),
        substitutions: {},
        regToken: regToken
      };
      return context;
    }

    // Given a path, it returns the filename without the extension.
    _getFilenameNoExtension(filename) {
      return path.parse(filename).name;
    }

    // Returns a prefix corresponding to an element type according to its manifest.
    _getPrefixFromType(manifest) {
      if (this._isSolution(manifest)) {
        return SOLUTION_PREFIX;
      }
      if (this._isKmv3Deployment(manifest)) {
        return KMV3_DEPL_PREFIX;
      }
      if (this._isResource(manifest)) {
        return RESOURCE_PREFIX;
      }
      if (this._isLinkManifest(manifest)) {
        return LINK_PREFIX;
      }
      if ((manifest != null ? manifest.spec : void 0) == null) {
        return '';
      }
      if (this._isDeploymentManifest(manifest)) {
        return DEPLOYMENT_PREFIX;
      }
      if (this._isBundleManifest(manifest)) {
        return BUNDLE_PREFIX;
      }
      if (this._isBlobManifest(manifest)) {
        return BLOB_PREFIX;
      }
      if (this._isTestManifest(manifest)) {
        return TEST_PREFIX;
      }
      return UNKNOWN_PREFIX;
    }

    // Returns a sorting function for ordering elements according to their
    // precedence in the registration process.
    _getPrecedenceSorter() {
      return (x, y) => {
        var orderX, orderY, xPrefix, yPrefix;
        xPrefix = this._getCleanDirPrefix(x);
        yPrefix = this._getCleanDirPrefix(y);
        orderX = REGISTRATION_PRECEDENCE.indexOf(xPrefix);
        orderY = REGISTRATION_PRECEDENCE.indexOf(yPrefix);
        if (orderX < orderY) {
          return -1;
        } else if (orderX > orderY) {
          return 1;
        } else if (x < y) {
          return -1;
        } else if (x > y) {
          return 1;
        } else {
          return 0;
        }
      };
    }

    _getCleanDirPrefix(dirname) {
      var err, prefix;
      try {
        prefix = dirname.slice(0, 4);
        if (indexOf.call(REGISTRATION_PRECEDENCE, prefix) >= 0) {
          return prefix;
        } else {
          return UNKNOWN_PREFIX;
        }
      } catch (error1) {
        err = error1;
        return UNKNOWN_PREFIX;
      }
    }

    // TODO: These validations should be a little more sophisticated in the future,
    // like spec version checks or even JSON validation with corresponding schemas.
    _isDeploymentManifest(manifest) {
      var manifestType;
      manifestType = this.manifestHelper.getManifestType(manifest);
      return manifestType === ManifestHelper.DEPLOYMENT;
    }

    _isSolution(manifest) {
      return this.manifestHelper.isSolution(manifest);
    }

    _isKmv3Deployment(manifest) {
      return this.manifestHelper.isKmv3Deployment(manifest);
    }

    _isLinkManifest(manifest) {
      var manifestType;
      manifestType = this.manifestHelper.getManifestType(manifest);
      return manifestType === ManifestHelper.LINK;
    }

    _isResource(manifest) {
      var manifestType;
      manifestType = this.manifestHelper.getManifestType(manifest);
      return manifestType === ManifestHelper.RESOURCE;
    }

    _isBundleManifest(manifest) {
      var manifestType;
      manifestType = this.manifestHelper.getManifestType(manifest);
      return manifestType === ManifestHelper.BUNDLE;
    }

    _isBlobManifest(manifest) {
      var manifestType;
      manifestType = this.manifestHelper.getManifestType(manifest);
      return manifestType === ManifestHelper.BLOB;
    }

    _isTestManifest(manifest) {
      var manifestType;
      manifestType = this.manifestHelper.getManifestType(manifest);
      return manifestType === ManifestHelper.TEST;
    }

    // Clones the git repository of the URL to a specific directory.
    _gitCloneRepository(repositoryURL, dstPath) {
      return q.Promise(function (resolve, reject) {
        var git;
        git = new Git(dstPath);
        return git.silent(true).clone(repositoryURL, dstPath, function (error) {
          if (error) {
            return reject(error);
          } else {
            return resolve();
          }
        });
      });
    }

    // Switch to specified branch in a local git repository
    _gitCheckout(repository, branch) {
      return q.Promise(function (resolve, reject) {
        return repository.checkout(branch, function (error) {
          if (error) {
            return reject(error);
          } else {
            return resolve(branch);
          }
        });
      });
    }

    // Returns two lists (files and sub-directories) based on the contents of the
    // given directory.
    _getDirContents(dir) {
      var directoryList, fileList;
      fileList = [];
      directoryList = [];
      return this.preaddir(dir).then(elements => {
        var el, i, len, promiseList;
        promiseList = [];
        for (i = 0, len = elements.length; i < len; i++) {
          el = elements[i];
          (el => {
            var elementPath;
            elementPath = path.join(dir, el);
            return promiseList.push(this.pstat(elementPath).then(function (fileStats) {
              if (fileStats.isFile()) {
                return fileList.push(el);
              } else if (fileStats.isDirectory()) {
                return directoryList.push(el);
              }
            }));
          })(el);
        }
        return q.all(promiseList);
      }).then(function () {
        return {
          files: fileList,
          dirs: directoryList
        };
      });
    }

    // Move the contents of a directory to another directory
    _moveDirContents(srcDir, dstDir) {
      return this.pmkdirp(dstDir).then(() => {
        return this.preaddir(srcDir);
      }).then(elements => {
        var el, i, len, promiseList;
        promiseList = [];
        for (i = 0, len = elements.length; i < len; i++) {
          el = elements[i];
          (el => {
            var dstElement, srcElement;
            srcElement = path.join(srcDir, el);
            dstElement = path.join(dstDir, el);
            return promiseList.push(this.prename(srcElement, dstElement));
          })(el);
        }
        return q.all(promiseList);
      });
    }

    // Move a list of files or sub-directories from a directory to another
    _moveFiles(fileList, srcDir, dstDir) {
      return this.pmkdirp(dstDir).then(() => {
        var file, i, len, promiseList;
        promiseList = [];
        for (i = 0, len = fileList.length; i < len; i++) {
          file = fileList[i];
          (file => {
            var dstElement, srcElement;
            srcElement = path.join(srcDir, file);
            dstElement = path.join(dstDir, file);
            return promiseList.push(this.prename(srcElement, dstElement));
          })(file);
        }
        return q.all(promiseList);
      });
    }

    // Verifies that a file HSA1 hash is the expected one
    _validateHash(file, hash) {
      return utils.calculateChecksum(file).then(function (checksum) {
        return checksum.toUpperCase() === hash.toUpperCase();
      });
    }

    // Add the contents of a directory to a new zip file
    _zipDirectoryContents(dir, newZipFile) {
      this.logger.info(`Zipping contents of directory ${dir} to ${newZipFile}`);
      return q.Promise((resolve, reject) => {
        return this.pstat(dir).then(function (stats) {
          var error, newZip;
          try {
            if (!stats.isDirectory()) {
              return reject(new Error('Expected a directory, got something else.'));
            } else {
              newZip = archiver.create('zip', {});
              newZip.directory(dir + '/', '');
              newZip.pipe(fs.createWriteStream(newZipFile)).on('error', function (err) {
                return reject(err);
              }).on('finish', function () {
                return resolve();
              });
              return newZip.finalize();
            }
          } catch (error1) {
            error = error1;
            return reject(err);
          }
        });
      });
    }

    // Add the contents of a directory to a new zip file
    _tgzDirectoryContents(dir, newTgzFile) {
      this.logger.info(`TGZing contents of directory ${dir} to ${newTgzFile}`);
      return q.Promise((resolve, reject) => {
        return this.pstat(dir).then(function (stats) {
          if (!stats.isDirectory()) {
            return reject(new Error('Expected a directory, got something else.'));
          } else {
            // files = dir + '/*'
            return utils.packDirectory(dir, newTgzFile).then(function () {
              return resolve();
            });
          }
        }).fail(function (err) {
          return reject(new Error(`TGZing directory: ${err.message}`));
        });
      });
    }

    // Transform a ZIP file to a TGZ file
    // The operation should not exceed the maximum size of the working directory
    _zipToTgz(srcZip, dstTgz, context) {
      var cleanDir, parentDirSize, tmpDir;
      this.logger.debug(`Converting ${srcZip} to ${dstTgz}`);
      tmpDir = null;
      cleanDir = this._getWorkDirs(context.regToken).clean;
      parentDirSize = null;
      return utils.getDirSize(cleanDir).then(_parentDirSize => {
        parentDirSize = _parentDirSize;
        return this._createTemporarySubdir(this.tmpPath);
      }).then(dir => {
        var unzipMaxSize;
        tmpDir = dir;
        this.logger.info(`Extracting zip file : ${srcZip} to ${tmpDir}...`);
        unzipMaxSize = this.maxBundleSize - parentDirSize;
        return utils.unzip(srcZip, tmpDir, unzipMaxSize);
      }).then(() => {
        this.logger.debug(`Extracted ${srcZip}`);
        return this._tgzDirectoryContents(tmpDir, dstTgz);
      }).then(() => {
        return this.logger.debug(`Created ${dstTgz}`);
      }).finally(() => {
        return this._cleanup(tmpDir);
      });
    }

    // Bring to a destination directory the files specified by a git repository,
    // a branch and a sub-folder.
    _getFromGit(gitConfig, dstPath, regToken) {
      var branch, ref, ref1, repo, subdir, tmpCloneDir;
      repo = gitConfig.repository;
      branch = (ref = gitConfig.branch) != null ? ref : 'master';
      subdir = (ref1 = gitConfig.subdir) != null ? ref1 : null;
      this.logger.info(`Cloning ${repo}#${branch}/${subdir} to ${dstPath}...`);
      tmpCloneDir = path.join(dstPath, 'fullRepo');
      return this.pmkdirp(tmpCloneDir).then(() => {
        return this._gitCloneRepository(repo, tmpCloneDir);
      }).then(() => {
        var newRepo;
        this.logger.debug(`Repository ${repo} cloned to ${tmpCloneDir}`);
        newRepo = new Git(tmpCloneDir);
        return this._gitCheckout(newRepo, branch);
      }).then(data => {
        var fullSubdir;
        this.logger.debug(`Repository ${repo} switched to branch ${branch}`);
        if (subdir != null) {
          fullSubdir = path.join(tmpCloneDir, subdir);
        } else {
          fullSubdir = tmpCloneDir;
        }
        return this._moveDirContents(fullSubdir, dstPath);
      }).then(() => {
        return this._cleanup(tmpCloneDir);
      }).fail(err => {
        this.logger.warn(`Cloning repository ${repo}: ${JSON.stringify(err)}`);
        this._addErrorMessage(regToken, `Cloning repository ${repo}: ${JSON.stringify(err)}`);
        return q();
      });
    }

    // Download to a destination directory the file specified by the URL
    _downloadHttpFile(fileURL, dstPath, regToken) {
      this.logger.info(`Downloading file ${fileURL} to ${dstPath}...`);
      return q.Promise((resolve, reject) => {
        var error, needleOptions, stream;
        try {
          needleOptions = {
            follow_max: 5
          };
          stream = needle.get(fileURL, needleOptions);
          return stream.on('error', function (err) {
            return reject(err);
          }).on('header', (statusCode, headers) => {
            var dstFile, filename, out, parsed;
            if (statusCode !== 200 && statusCode !== 301 && statusCode !== 302 && statusCode !== 303) {
              return reject(new Error(`HTTP returned ${statusCode}.`));
            } else {
              parsed = url.parse(fileURL);
              filename = path.basename(parsed.pathname);
              dstFile = path.join(dstPath, filename);
              this.logger.debug(`File destination: ${dstFile}`);
              out = fs.createWriteStream(dstFile, NEEDLE_HTTP_OPTIONS);
              return stream.pipe(out).on('error', function (err) {
                return reject(err);
              }).on('finish', resolve);
            }
          });
        } catch (error1) {
          error = error1;
          return reject(error);
        }
      }).fail(err => {
        this.logger.warn(`Downloading file ${fileURL}: ${err.message}`);
        this._addErrorMessage(regToken, `Downloading file ${fileURL}: ${err.message}`);
        return q();
      });
    }

    // Takes a Bundles.json file and bring all the bundles referenced inside to
    // a local temporary directory.
    _processBundlesFile(bundlesFile, workDir, regToken) {
      this.logger.info(`Processing Bundles.json: ${bundlesFile}...`);
      return utils.jsonFromFile(bundlesFile).then(bundleData => {
        var bun, i, len, promises, ref, ref1, tmpBundleDir;
        this.logger.debug(`Bundles data: ${JSON.stringify(bundleData)}`);
        if (bundleData.spec == null || !this._isBundleManifest(bundleData.spec)) {
          q.reject(new Error('Bundle.json file has no required spec attribute.'));
        }
        promises = [];
        if (((ref = bundleData.bundles) != null ? ref.length : void 0) > 0) {
          ref1 = bundleData.bundles;
          for (i = 0, len = ref1.length; i < len; i++) {
            bun = ref1[i];
            if (bun.repository != null) {
              tmpBundleDir = path.join(workDir, `bundle_${utils.generateId()}`);
              promises.push(this._getFromGit(bun, tmpBundleDir, regToken));
            } else if (bun.site != null) {
              tmpBundleDir = path.join(workDir, `bundle_${utils.generateId()}`);
              promises.push(this._downloadHttpFile(bun.site, workDir, regToken));
            } else {
              this.logger.warn(`Incorrect bundle definition: ${JSON.stringify(bun)}`);
              this._addErrorMessage(regToken, `Incorrect bundle definition: ${JSON.stringify(bun)}`);
              promises.push(q());
            }
          }
        }
        return q.all(promises);
      }).fail(err => {
        this.logger.warn(`Processing Bundles.json: ${err.message}`);
        this._addErrorMessage(regToken, `Processing Bundles.json: ${err.message}`);
        return q();
      });
    }

    // Looks in the 'dstPath' directory for a blob bundle with the name 'codeName'
    _findCode(codeName, dirPath, regToken) {
      this.logger.info(`Looking for a blob bundle named '${codeName}'...`);
      if (!(codeName != null ? codeName.trim().length : void 0) > 0) {
        return q(null);
      }
      // Look for Blob bundle that matches the name
      return this._getDirContents(dirPath).then(dirContents => {
        var codeBundleDir, dir, i, len, promiseChain, ref;
        codeBundleDir = null;
        promiseChain = q();
        ref = dirContents.dirs;
        // List sub-directories, excluding hidden ones (for example .git)
        for (i = 0, len = ref.length; i < len; i++) {
          dir = ref[i];
          if (!utils.startsWith(dir, DOT)) {
            (dir => {
              return promiseChain = promiseChain.finally(() => {
                var manifestFile;
                if (codeBundleDir != null) {
                  // If blob bundles has already be found, skip this sub-dir
                  return q();
                }
                manifestFile = path.join(dirPath, dir, MANIFEST_FILENAME);
                return utils.checkFileExists(manifestFile).then(exists => {
                  if (exists) {
                    return utils.jsonFromFile(manifestFile).then(manifest => {
                      if (this._isBlobManifest(manifest) && manifest.name === codeName) {
                        return codeBundleDir = dir;
                      }
                    });
                  }
                }).fail(err => {
                  this.logger.warn(`Getting code for '${codeName}: ${err.message}`);
                  this._addErrorMessage(regToken, `Getting code for '${codeName}: ${err.message}`);
                  return q();
                });
              });
            })(dir);
          }
        }
        return promiseChain.then(() => {
          if (codeBundleDir == null) {
            return q.reject(new Error(`Unable to find blob bundle matching the code value ${codeName}`));
          } else {
            this.logger.info(`Found blob bundle '${codeName}' at ${codeBundleDir}`);
            return codeBundleDir;
          }
        });
      });
    }

    _checkDirSize(dir, maxSize) {
      return utils.getDirSize(dir).then(function (currentSize) {
        if (currentSize > maxSize) {
          return q.reject(new Error(`Directory ${dir} exceeds max size (${maxSize})`));
        } else {
          return q.resolve(currentSize);
        }
      });
    }

  };

  metadataFromContext = function (context) {
    return {
      // console.log "???????????????????????????????????????????????'"
      // console.log "???????????????????????????????????????????????'"
      // console.log "BUNDLEREGISTRY.metadataFromContext CONTEXT: #{JSON.stringify context, null, 2}"
      // console.log "???????????????????????????????????????????????'"
      // console.log "???????????????????????????????????????????????'"
      owner: context.user.id,
      created: new Date().toISOString(),
      // Function to test the element precedence sorter
      _testDirectorySorter: function () {
        var test;
        // console.log 'SORTED DIRS TO PROCESS: ', dirContents.dirs
        test = ['comp_1_c62c2b62-49ed-4c0f-b27c-6a435c96caf7', 'comp_3_c62c2b62-49ed-4c0f-b27c-6a435c96caf7', 'comp_0_c62c2b62-49ed-4c0f-b27c-6a435c96caf7', 'runt_1_c62c2b62-49ed-4c0f-b27c-6a435c96caf7', 'runt_2_c62c2b62-49ed-4c0f-b27c-6a435c96caf7', 'runt_2_c62c2b62-49ed-4c0f-b27c-6a435c96caf7', 'comp_1_c62c2b62-49ed-4c0f-b27c-6a435c96caf7', 'reso_2_c62c2b62-49ed-4c0f-b27c-6a435c96caf7', 'depl_0_c62c2b62-49ed-4c0f-b27c-6a435c96caf7', 'reso_0_c62c2b62-49ed-4c0f-b27c-6a435c96caf7', 'serv_0_c62c2b62-49ed-4c0f-b27c-6a435c96caf7', 'comp_9_c62c2b62-49ed-4c0f-b27c-6a435c96caf7'];
        // console.log 'TEST: ', test
        return test.sort(this._getPrecedenceSorter());
      }
    };
  };

  // console.log 'TEST: ', test
  module.exports = BundleRegistry;
}).call(undefined);