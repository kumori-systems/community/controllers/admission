'use strict';

(function () {
  /*
  * Copyright 2022 Kumori Systems S.L.
   * Licensed under the EUPL, Version 1.2 or – as soon they
    will be approved by the European Commission - subsequent
    versions of the EUPL (the "Licence");
   * You may not use this work except in compliance with the
    Licence.
   * You may obtain a copy of the Licence at:
     https://joinup.ec.europa.eu/software/page/eupl
   * Unless required by applicable law or agreed to in
    writing, software distributed under the Licence is
    distributed on an "AS IS" basis,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
    express or implied.
   * See the Licence for the specific language governing
    permissions and limitations under the Licence.
   */
  var FINALIZER_REMOVAL_PERIOD_SECONDS,
      KEYCLOAK_DEFAULT_CLIENT_NAME,
      KEYCLOAK_DEFAULT_REALM_LABEL,
      KEYCLOAK_DEFAULT_REALM_NAME,
      KEYCLOAK_K8s_APIGROUP,
      KEYCLOAK_K8s_CRDs,
      KEYCLOAK_K8s_DEFAULT_NAMESPACE,
      KEYCLOAK_K8s_PLURAL_CLIENTS,
      KEYCLOAK_K8s_PLURAL_REALMS,
      KEYCLOAK_K8s_PLURAL_USERS,
      KEYCLOAK_K8s_VERSION,
      KeycloakUser,
      KeycloakUserManager,
      MANDATORY_PASSWORD_CHANGE_PROPERTIES,
      MANDATORY_PASSWORD_CHANGE_PROPERTIES_PRIVILEGED,
      MANDATORY_USER_PROPERTIES,
      q,
      indexOf = [].indexOf;

  q = require('q');

  KeycloakUser = require('./keycloak-user');

  MANDATORY_USER_PROPERTIES = ['username', 'email', 'firstName', 'lastName', 'password', 'enabled', 'groups'];

  MANDATORY_PASSWORD_CHANGE_PROPERTIES = ['oldPassword', 'newPassword'];

  MANDATORY_PASSWORD_CHANGE_PROPERTIES_PRIVILEGED = ['newPassword'];

  KEYCLOAK_K8s_APIGROUP = 'keycloak.org';

  KEYCLOAK_K8s_VERSION = 'v1alpha1';

  KEYCLOAK_K8s_PLURAL_USERS = 'keycloakusers';

  KEYCLOAK_K8s_PLURAL_CLIENTS = 'keycloakclients';

  KEYCLOAK_K8s_PLURAL_REALMS = 'keycloakrealms';

  KEYCLOAK_K8s_CRDs = [KEYCLOAK_K8s_PLURAL_USERS, KEYCLOAK_K8s_PLURAL_CLIENTS, KEYCLOAK_K8s_PLURAL_REALMS];

  KEYCLOAK_K8s_DEFAULT_NAMESPACE = 'keycloak';

  KEYCLOAK_DEFAULT_REALM_NAME = 'KumoriCluster';

  KEYCLOAK_DEFAULT_REALM_LABEL = 'keycloak';

  KEYCLOAK_DEFAULT_CLIENT_NAME = 'admission';

  FINALIZER_REMOVAL_PERIOD_SECONDS = 30;

  KeycloakUserManager = class KeycloakUserManager {
    // This constructor expects the 'config' in Admission keycloakConfig format:
    // {
    //   "namespace": "keycloak",
    //   "realmName": "KumoriCluster",
    //   "realmLabel": "keycloak-sso",
    //   "clientName": "admission"
    // }

    // {
    //   "realm": "KumoriCluster",
    //   "auth-server-url": "http://jferrer-keycloak.test.kumori.cloud:8080/auth",
    //   "ssl-required": "external",
    //   "resource": "admission",
    //   "verify-token-audience": true,
    //   "credentials": {
    //     "secret": "<secret>"
    //   },
    //   "use-resource-role-mappings": true,
    //   "bearerOnly": true
    constructor(config = {}, k8sApiStub) {
      var meth;
      this.config = config;
      this.k8sApiStub = k8sApiStub;
      meth = 'KeycloakUserManager.constructor';
      if (!this.logger) {
        this.logger = {
          error: console.log,
          warn: console.log,
          info: console.log,
          debug: console.log
        };
      }
      this.logger.info(`${meth} CONFIG: ${JSON.stringify(this.config)}`);
      this.k8sNamespace = this.config.namespace || KEYCLOAK_K8s_DEFAULT_NAMESPACE;
      this.realmName = this.config.realmName || KEYCLOAK_DEFAULT_REALM_NAME;
      this.realmLabel = this.config.realmLabel || KEYCLOAK_DEFAULT_REALM_LABEL;
      this.clientName = this.config.clientName || KEYCLOAK_DEFAULT_CLIENT_NAME;
      // Interval for periodically removing 'Finalizers' from Keycloak Operator
      // Resources, so they are reprocessed.
      // NOTE: this is necessary to reconfigure Keycloak after a Pod restart
      this.finalizerRemovalPeriodSeconds = this.config.finalizerRemovalPeriodSeconds || FINALIZER_REMOVAL_PERIOD_SECONDS;
      this.removeFinalizersTimer = null;
    }

    init() {
      // Kick-off timer for removing Finalizers from Keycloak objects
      return this.removeFinalizersTimer = setInterval(() => {
        return this.removeFinalizers();
      }, this.finalizerRemovalPeriodSeconds * 1000);
    }

    terminate() {
      // Stop timer for removing Finalizers from Keycloak objects
      return clearInterval(this.removeFinalizersTimer);
    }

    removeFinalizers() {
      var crd, err, finalizerPatch, i, len, meth, results;
      meth = 'KeycloakUserManager.removeFinalizers';
      try {
        finalizerPatch = [{
          op: 'replace',
          path: '/metadata/finalizers',
          value: []
        }];
        results = [];
        for (i = 0, len = KEYCLOAK_K8s_CRDs.length; i < len; i++) {
          crd = KEYCLOAK_K8s_CRDs[i];
          results.push((crd => {
            return this.k8sApiStub.listCustomResource(KEYCLOAK_K8s_APIGROUP, KEYCLOAK_K8s_VERSION, this.k8sNamespace, crd, null).then(list => {
              var item, j, len1, name, results1;
              results1 = [];
              for (j = 0, len1 = list.length; j < len1; j++) {
                item = list[j];
                name = item.metadata.name;
                results1.push(((name, crd) => {
                  return this.k8sApiStub.patchCustomResource(KEYCLOAK_K8s_APIGROUP, KEYCLOAK_K8s_VERSION, this.k8sNamespace, crd, name, finalizerPatch).then(() => {
                    return true;
                  }).catch(err => {
                    this.logger.error(`${meth} Patching ${crd} ${name}: ${this.beautifyError(err)}`);
                    return true;
                  });
                })(name, crd));
              }
              return results1;
            }).catch(err => {
              this.logger.error(`${meth} Getting list of ${crd}: ${this.beautifyError(err)}`);
              return true;
            });
          })(crd));
        }
        return results;
      } catch (error) {
        err = error;
        this.logger.error(`${meth} ERROR: ${err.message} ${err.stack}`);
        return true;
      }
    }

    //#############################################################################
    //#                           "PUBLIC" METHODS                               ##
    //#############################################################################

    // Returns a promise resolved with an array of user objects like the ones
    // returned by getUser method.
    getUsers() {
      var meth;
      meth = 'KeycloakUserManager.getUsers';
      this.logger.info(`${meth}`);
      return this.k8sApiStub.listCustomResource(KEYCLOAK_K8s_APIGROUP, KEYCLOAK_K8s_VERSION, this.k8sNamespace, KEYCLOAK_K8s_PLURAL_USERS, null).then(list => {
        // console.log "RESPONSE: #{JSON.stringify list}"
        return this._cleanUserList(list);
      }).catch(err => {
        var errMsg;
        errMsg = `Failed to list users: ${this.beautifyError(err)}`;
        this.logger.warn(`${meth} ERROR: ${errMsg}`);
        return q.reject(new Error(errMsg));
      });
    }

    // Returns a promise resolved with an object representing the user matching the
    // provided 'username'.
    // The user object has the following structure:

    //   - username:  <string>
    //   - email:     <string>
    //   - firstName: <string>
    //   - lastName:  <string>
    //   - enabled:   bolean
    //   - groups:    [ <string> ]
    getUser(username) {
      var meth;
      meth = `KeycloakUserManager.getUser(${username})`;
      this.logger.info(`${meth}`);
      return this.k8sApiStub.getCustomResource(KEYCLOAK_K8s_APIGROUP, KEYCLOAK_K8s_VERSION, this.k8sNamespace, KEYCLOAK_K8s_PLURAL_USERS, username).then(user => {
        // console.log "RESPONSE: #{JSON.stringify user}"
        if (user == null) {
          throw new Error("user not found");
        } else {
          return this._cleanUser(user);
        }
      }).catch(err => {
        var errMsg;
        errMsg = `Failed to get user with username '${username}': ${this.beautifyError(err)}`;
        this.logger.warn(`${meth} ERROR: ${errMsg}`);
        return q.reject(new Error(errMsg));
      });
    }

    // Returns a promise resolved with an array of user objects like the ones
    // returned by getUser method.
    getGroups() {
      var meth;
      meth = 'KeycloakUserManager.getGroups';
      this.logger.info(`${meth}`);
      return this.k8sApiStub.getCustomResource(KEYCLOAK_K8s_APIGROUP, KEYCLOAK_K8s_VERSION, this.k8sNamespace, KEYCLOAK_K8s_PLURAL_CLIENTS, this.clientName).then(admClient => {
        // console.log "RESPONSE: #{JSON.stringify admClient}"
        return this._getRolesFromClient(admClient);
      }).catch(err => {
        var errMsg;
        errMsg = `Failed to list groups: ${this.beautifyError(err)}`;
        this.logger.warn(`${meth} ERROR: ${errMsg}`);
        return q.reject(new Error(errMsg));
      });
    }

    // Deletes the user matching the provided 'username'.
    // Returns a promise resolved with 'true' if the user is correctly deleted.
    deleteUser(username) {
      var meth;
      meth = `KeycloakUserManager.deleteUser(${username})`;
      this.logger.info(`${meth}`);
      return this.k8sApiStub.deleteCustomResource(KEYCLOAK_K8s_APIGROUP, KEYCLOAK_K8s_VERSION, this.k8sNamespace, KEYCLOAK_K8s_PLURAL_USERS, username).catch(err => {
        var errMsg;
        errMsg = `Failed to delete user with username '${username}': ${this.beautifyError(err)}`;
        this.logger.warn(`${meth} ERROR: ${errMsg}`);
        return q.reject(new Error(errMsg));
      });
    }

    // Creates a new user. An object with the new user properties must specified.
    // Expected format (all properties are mandatory for now):

    //  - username:   <string>
    //  - email:      <string>
    //  - firstName:  <string>
    //  - lastName:   <string>
    //  - password:   <string>  # TODO: implement a more secure way
    //  - enabled:    <boolean>
    //  - groups:     <list/array of group names>

    // Returns a promise resolved with 'true' if the user is correctly created.
    createUser(userData) {
      var meth;
      meth = 'KeycloakUserManager.createUser';
      this.logger.info(`${meth} User data: ${JSON.stringify(userData)}`);
      // Start a promise chain so synchronous validation exceptions are handled by
      // the promise catch handler.
      return q().then(() => {
        // Check that all mandatory properties are present
        this.logger.info(`${meth} Validating user data...`);
        this._validateUserDataProperties(userData);
        // Check that all groups are valid
        this.logger.info(`${meth} Validating user group names...`);
        return this._validateUserGroupList(userData.groups);
      }).then(() => {
        // Check if user already exists
        this.logger.info(`${meth} Validating username availability...`);
        return this.k8sApiStub.getCustomResource(KEYCLOAK_K8s_APIGROUP, KEYCLOAK_K8s_VERSION, this.k8sNamespace, KEYCLOAK_K8s_PLURAL_USERS, userData.username);
      }).then(previousUser => {
        var newUserCR;
        if (previousUser != null) {
          throw new Error(`user already exists (${userData.username})`);
        } else {
          newUserCR = new KeycloakUser(this.realmLabel, this.k8sNamespace, null, userData.username, userData.password, userData.firstName, userData.lastName, userData.email, userData.enabled, userData.groups, null);
          // console.log "NEW USER CUSTOM RESOURCE:"
          // console.log JSON.stringify newUserCR, null, 2

          // Check if user already exists
          this.logger.info(`${meth} Creating new user...`);
          return this.k8sApiStub.createCustomResource(KEYCLOAK_K8s_APIGROUP, KEYCLOAK_K8s_VERSION, this.k8sNamespace, KEYCLOAK_K8s_PLURAL_USERS, newUserCR);
        }
      }).then(() => {
        this.logger.info(`${meth} New user ${userData.username} created.`);
        return true;
      }).catch(err => {
        var errMsg;
        errMsg = `Failed to create user '${userData.username}': ${this.beautifyError(err)}`;
        this.logger.warn(`${meth} ERROR: ${errMsg}`);
        return q.reject(new Error(errMsg));
      });
    }

    // Updates an existing user, identified by the username.
    // An object with the new user properties must specified.
    // Expected format (all properties are mandatory for now):

    //  - username:   <string>
    //  - email:      <string>
    //  - firstName:  <string>
    //  - lastName:   <string>
    //  - password:   <string>  # TODO: implement a more secure way
    //  - enabled:    <boolean>
    //  - groups:     <list/array of group names>

    // Returns a promise resolved with 'true' if the user is correctly created.
    updateUser(userData) {
      var meth;
      meth = 'KeycloakUserManager.updateUser';
      this.logger.info(`${meth} User data: ${JSON.stringify(userData)}`);
      // Start a promise chain so synchronous validation exceptions are handled by
      // the promise catch handler.
      return q().then(() => {
        // Check that all mandatory properties are present
        this.logger.info(`${meth} Validating user data...`);
        this._validateUserDataProperties(userData);
        // Check that all groups are valid
        this.logger.info(`${meth} Validating user group names...`);
        return this._validateUserGroupList(userData.groups);
      }).then(() => {
        // Check if user already exists
        this.logger.info(`${meth} Validating username existence...`);
        return this.k8sApiStub.getCustomResource(KEYCLOAK_K8s_APIGROUP, KEYCLOAK_K8s_VERSION, this.k8sNamespace, KEYCLOAK_K8s_PLURAL_USERS, userData.username);
      }).then(previousUser => {
        var newUserCR;
        if (previousUser == null) {
          throw new Error(`user not found (${userData.username})`);
        } else {
          this.logger.info(`${meth} Found user, updating it...`);
          // Create new CR object
          newUserCR = new KeycloakUser(this.realmLabel, this.k8sNamespace, null, userData.username, userData.password, userData.firstName, userData.lastName, userData.email, userData.enabled, userData.groups, null);
          // Set the User ID property from previous object version
          newUserCR.spec.user.id = previousUser.spec.user.id;
          // Set resource version to the previous object version
          newUserCR.metadata.resourceVersion = previousUser.metadata.resourceVersion;
          // console.log "NEW USER CUSTOM RESOURCE:"
          // console.log JSON.stringify newUserCR, null, 2

          // Update user
          this.logger.info(`${meth} Updating user...`);
          return this.k8sApiStub.replaceCustomResource(KEYCLOAK_K8s_APIGROUP, KEYCLOAK_K8s_VERSION, this.k8sNamespace, KEYCLOAK_K8s_PLURAL_USERS, newUserCR);
        }
      }).then(() => {
        this.logger.info(`${meth} User ${userData.username} updated.`);
        return true;
      }).catch(err => {
        var errMsg;
        errMsg = `Failed to update user '${userData.username}': ${this.beautifyError(err)}`;
        this.logger.warn(`${meth} ERROR: ${errMsg}`);
        return q.reject(new Error(errMsg));
      });
    }

    // Updates an existing user password.
    // An object with the old and new passwords must specified.
    // Expected format (all properties are mandatory for now):

    //  - newPassword:   <string>
    //  - oldPassword:   <string>

    // If 'privileged' flag is provided, it is not mandatory to provide the old
    // password.

    // Returns a promise resolved with 'true' if the user is correctly updated
    updateUserPassword(username, passwordChangeData, privileged = false) {
      var meth;
      meth = 'KeycloakUserManager.updateUserPassword';
      this.logger.info(`${meth} User: ${username} (privileged=${privileged}`);
      // Start a promise chain so synchronous validation exceptions are handled by
      // the promise catch handler.
      return q().then(() => {
        // Check that all mandatory properties are present
        this.logger.info(`${meth} Validating password change data...`);
        return this._validatePasswordChangeProperties(passwordChangeData, privileged);
      }).then(() => {
        // Get user
        this.logger.info(`${meth} Validating username existence...`);
        return this.k8sApiStub.getCustomResource(KEYCLOAK_K8s_APIGROUP, KEYCLOAK_K8s_VERSION, this.k8sNamespace, KEYCLOAK_K8s_PLURAL_USERS, username);
      }).then(previousUser => {
        var newUserCR, previousPassword, ref, ref1, ref2;
        if (previousUser == null) {
          throw new Error(`user not found (${username})`);
        }
        this.logger.info(`${meth} Found user, updating its password...`);
        // Verify that 'oldPassword' parameter matches the user previous password,
        // unless request is made by an Admin on a different user (privileged
        // operation)
        previousPassword = ((ref = previousUser.spec.user) != null ? (ref1 = ref.credentials) != null ? (ref2 = ref1[0]) != null ? ref2.value : void 0 : void 0 : void 0) || null;
        if (passwordChangeData.oldPassword !== previousPassword && !privileged) {
          throw new Error("incorrect password ('oldPassword' property).");
        }
        // Create new CR object
        newUserCR = new KeycloakUser(this.realmLabel, this.k8sNamespace, previousUser.spec.user.id, previousUser.spec.user.username, passwordChangeData.newPassword, previousUser.spec.user.firstName, previousUser.spec.user.lastName, previousUser.spec.user.email, previousUser.spec.user.enabled, previousUser.spec.user.clientRoles.admission, previousUser.metadata.resourceVersion);
        //# # Set the User ID property from previous object version
        //# newUserCR.spec.user.id = previousUser.spec.user.id

        //# # Set resource version to the previous object version
        //# newUserCR.metadata.resourceVersion =
        //#   previousUser.metadata.resourceVersion

        // Check if user already exists
        this.logger.info(`${meth} Updating user...`);
        return this.k8sApiStub.replaceCustomResource(KEYCLOAK_K8s_APIGROUP, KEYCLOAK_K8s_VERSION, this.k8sNamespace, KEYCLOAK_K8s_PLURAL_USERS, newUserCR);
      }).then(() => {
        this.logger.info(`${meth} User ${username} updated.`);
        return true;
      }).catch(err => {
        var errMsg;
        errMsg = `Failed to update user '${username}': ${this.beautifyError(err)}`;
        this.logger.warn(`${meth} ERROR: ${errMsg}`);
        return q.reject(new Error(errMsg));
      });
    }

    //#############################################################################
    //#                             UTIL METHODS                                 ##
    //#############################################################################
    _cleanUserList(userList) {
      var cleanUserList, i, len, meth, user;
      meth = 'KeycloakUserManager._cleanUserList';
      cleanUserList = [];
      for (i = 0, len = userList.length; i < len; i++) {
        user = userList[i];
        cleanUserList.push(this._cleanUser(user));
      }
      // NOTE: If we want to filter out users with no Admission permissions,
      //       comment the previous statement and uncomment the following block.

      // if not user.spec.user.clientRoles?[@clientName]?
      //   @logger.info "#{meth} Skipping non-admission user:
      //                 username=#{user.spec.user.username}
      //                 clients=#{Object.keys user.spec.user.clientRoles}"
      // else
      //   cleanUserList.push @_cleanUser user
      return cleanUserList;
    }

    _cleanUser(user) {
      var cleanUser, i, len, ref, role;
      cleanUser = {
        username: user.spec.user.username,
        email: user.spec.user.email,
        firstName: user.spec.user.firstName,
        lastName: user.spec.user.lastName,
        enabled: user.spec.user.enabled,
        groups: [],
        status: ""
      };
      ref = user.spec.user.clientRoles[this.clientName] || [];
      for (i = 0, len = ref.length; i < len; i++) {
        role = ref[i];
        if (role === 'administrator') {
          cleanUser.groups.push('administrators');
        }
        if (role === 'developer') {
          cleanUser.groups.push('developers');
        }
      }
      if (user.status == null) {
        cleanUser.status = 'pending creation';
      } else if (user.status.phase === 'reconciled') {
        cleanUser.status = 'ok';
      } else {
        cleanUser.status = `${user.status.phase} - ${user.status.message}`;
      }
      return cleanUser;
    }

    _getRolesFromClient(client) {
      var groups, i, len, ref, roleItem;
      // Groups are Roles, but with a final 's' (for backwards compatibility)
      groups = [];
      ref = client.spec.roles || [];
      for (i = 0, len = ref.length; i < len; i++) {
        roleItem = ref[i];
        // Only take into account client roles (boolean flag)
        if (roleItem.clientRole) {
          if (roleItem.name === 'administrator') {
            groups.push('administrators');
          }
          if (roleItem.name === 'developer') {
            groups.push('developers');
          }
        }
      }
      return groups;
    }

    // Validate all required user properties are present
    _validateUserDataProperties(userData) {
      var errMsg, i, len, meth, missingProps, prop;
      meth = 'KeycloakUserManager._validateUserDataProperties';
      missingProps = [];
      for (i = 0, len = MANDATORY_USER_PROPERTIES.length; i < len; i++) {
        prop = MANDATORY_USER_PROPERTIES[i];
        if (!(prop in userData) || userData[prop] === '') {
          missingProps.push(prop);
        }
      }
      if (missingProps.length !== 0) {
        errMsg = `User data is missing one or more mandatory properties: ${missingProps}`;
        this.logger.warn(`${meth} - ${errMsg}`);
        throw new Error(errMsg);
      }
    }

    // Validate all required password change properties are present
    _validatePasswordChangeProperties(passwordChangeData, privileged) {
      var errMsg, i, len, mandatoryProperties, meth, missingProps, prop;
      meth = 'KeycloakUserManager._validatePasswordChangeProperties';
      mandatoryProperties = MANDATORY_PASSWORD_CHANGE_PROPERTIES;
      if (privileged) {
        mandatoryProperties = MANDATORY_PASSWORD_CHANGE_PROPERTIES_PRIVILEGED;
      }
      missingProps = [];
      for (i = 0, len = mandatoryProperties.length; i < len; i++) {
        prop = mandatoryProperties[i];
        if (!(prop in passwordChangeData) || passwordChangeData[prop] === '') {
          missingProps.push(prop);
        }
      }
      if (missingProps.length !== 0) {
        errMsg = `Missing one or more mandatory properties: ${missingProps}`;
        this.logger.warn(`${meth} - ${errMsg}`);
        throw new Error(errMsg);
      }
    }

    // Validate that all groups in the list exist
    _validateUserGroupList(groupList) {
      var meth;
      meth = 'KeycloakUserManager._validateUserGroupList';
      if (groupList == null) {
        return;
      }
      return this.getGroups().then(validGroups => {
        var errMsg, errors, groupName, i, len;
        errors = [];
        for (i = 0, len = groupList.length; i < len; i++) {
          groupName = groupList[i];
          if (indexOf.call(validGroups, groupName) < 0) {
            errors.push(groupName);
          }
        }
        if (errors.length !== 0) {
          errMsg = `Invalid groups: ${errors}`;
          this.logger.warn(`${meth} - ${errMsg}`);
          return q.reject(new Error(errMsg));
        } else {
          return true;
        }
      });
    }

    beautifyError(err) {
      var errMsg, ref, ref1, ref2, ref3;
      if (((ref = err.response) != null ? ref.status : void 0) != null && ((ref1 = err.response) != null ? ref1.statusText : void 0) != null && ((ref2 = err.response) != null ? (ref3 = ref2.data) != null ? ref3.errorMessage : void 0 : void 0) != null) {
        errMsg = `${err.response.data.errorMessage} (Keycloak API Error: ${err.response.status} ${err.response.statusText})`;
      } else {
        errMsg = `${err.message || err}`;
      }
      return errMsg;
    }

  };

  module.exports = KeycloakUserManager;
}).call(undefined);