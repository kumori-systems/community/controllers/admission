'use strict';

(function () {
  /*
  * Copyright 2022 Kumori Systems S.L.
   * Licensed under the EUPL, Version 1.2 or – as soon they
    will be approved by the European Commission - subsequent
    versions of the EUPL (the "Licence");
   * You may not use this work except in compliance with the
    Licence.
   * You may obtain a copy of the Licence at:
     https://joinup.ec.europa.eu/software/page/eupl
   * Unless required by applicable law or agreed to in
    writing, software distributed under the Licence is
    distributed on an "AS IS" basis,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
    express or implied.
   * See the Licence for the specific language governing
    permissions and limitations under the Licence.
   */
  var KeycloakAdminClient, KeycloakUserManager, MANDATORY_USER_PROPERTIES, _, httpStatusCodes, q;

  q = require('q');

  _ = require('lodash');

  KeycloakAdminClient = require('keycloak-admin').default;

  httpStatusCodes = require('./http-status-codes').HTTP_STATUS_CODES;

  MANDATORY_USER_PROPERTIES = ['username', 'email', 'firstName', 'lastName', 'password', 'enabled', 'groups'];

  KeycloakUserManager = class KeycloakUserManager {
    // This constructor expects the 'config' in Admission keycloakConfig format:
    // {
    //   "realm": "KumoriCluster",
    //   "auth-server-url": "http://jferrer-keycloak.test.kumori.cloud:8080/auth",
    //   "ssl-required": "external",
    //   "resource": "admission",
    //   "verify-token-audience": true,
    //   "credentials": {
    //     "secret": "<secret>"
    //   },
    //   "use-resource-role-mappings": true,
    //   "bearerOnly": true
    // }
    constructor(config) {
      var meth;
      this.config = config;
      meth = 'KeycloakUserManager.constructor';
      this.logger.info(`${meth} CONFIG: ${JSON.stringify(this.config)}`);
      this.kcConfig = {
        baseUrl: this.config['auth-server-url'],
        realmName: this.config['realm'],
        requestConfig: {}
      };
      this.logger.info(`${meth} kcConfig: ${JSON.stringify(this.kcConfig)}`);
      this.kcAdminClient = new KeycloakAdminClient(this.kcConfig);
    }

    //#############################################################################
    //#                           "PUBLIC" METHODS                               ##
    //#############################################################################
    //    ONLY THESE PUBLIC METHOD SHOULD SET THE ACCESS TOKEN OF THE LIBRARY    ##
    //#############################################################################

    // Returns a promise resolved with an array of user objects like the ones
    // returned by getUser method.
    getUsers(accessToken) {
      var meth;
      meth = 'KeycloakUserManager.getUsers';
      this.logger.info(`${meth}`);
      this.kcAdminClient.setAccessToken(accessToken);
      return this._getUsers();
    }

    // Returns a promise resolved with an object representing the user matching the
    // provided 'username'.
    // The user object has the following structure:

    //   - username:  <string>
    //   - email:     <string>
    //   - firstName: <string>
    //   - lastName:  <string>
    //   - enabled:   bolean
    //   - groups:    [ <string> ]
    getUser(username, accessToken) {
      var meth;
      meth = `KeycloakUserManager.getUser(${username})`;
      this.logger.info(`${meth}`);
      this.kcAdminClient.setAccessToken(accessToken);
      return this._getUser(username);
    }

    // Creates a new user. An object with the new user properties must specified.
    // Expected format (all properties are mandatory for now):

    //  - username:   <string>
    //  - email:      <string>
    //  - firstName:  <string>
    //  - lastName:   <string>
    //  - password:   <string>  # TODO: implement a more secure way
    //  - enabled:    <boolean>
    //  - groups:     <list/array of group names>

    // Returns a promise resolved with 'true' if the user is correctly created.
    createUser(userData, accessToken) {
      var meth;
      meth = 'KeycloakUserManager.createUser';
      this.logger.info(`${meth} User data: ${JSON.stringify(userData)}`);
      this.kcAdminClient.setAccessToken(accessToken);
      return this._createUser(userData);
    }

    // Updates an existing user, identified by the username.
    // An object with the new user properties must specified.
    // Expected format (all properties are mandatory for now):

    //  - username:   <string>
    //  - email:      <string>
    //  - firstName:  <string>
    //  - lastName:   <string>
    //  - password:   <string>  # TODO: implement a more secure way
    //  - enabled:    <boolean>
    //  - groups:     <list/array of group names>

    // Returns a promise resolved with 'true' if the user is correctly created.
    updateUser(userData, accessToken) {
      var meth;
      meth = 'KeycloakUserManager.updateUser';
      this.logger.info(`${meth} User data: ${JSON.stringify(userData)}`);
      this.kcAdminClient.setAccessToken(accessToken);
      return this._updateUser(userData);
    }

    // Deletes the user matching the provided 'username'.
    // Returns a promise resolved with 'true' if the user is correctly deleted.
    deleteUser(username, accessToken) {
      var meth;
      meth = `KeycloakUserManager.deleteUser(${username})`;
      this.logger.info(`${meth}`);
      this.kcAdminClient.setAccessToken(accessToken);
      return this._deleteUser(username);
    }

    // Returns a promise resolved with an array of user objects like the ones
    // returned by getUser method.
    getGroups(accessToken) {
      var meth;
      meth = 'KeycloakUserManager.getGroups';
      this.logger.info(`${meth}`);
      this.kcAdminClient.setAccessToken(accessToken);
      return this._getGroups();
    }

    //#############################################################################
    //#                      KEYCLOAK API HELPER METHODS                         ##
    //#############################################################################

    // Returns a promise resolved with the list of groups names/objects the
    // provided user belongs to.
    // If raw is 'true', each group is returned as the original object.
    // If raw is 'false', each group is returned as a single string (its name).
    _getUsers(raw = false) {
      var meth;
      meth = 'KeycloakUserManager._getUsers';
      this.logger.info(`${meth}`);
      return q.promise(async (resolve, reject) => {
        var err, errMsg, userList, userQuery;
        try {
          this.logger.info(`${meth} Getting all users...`);
          userQuery = {};
          userList = await this.kcAdminClient.users.find(userQuery);
          if (raw) {
            return resolve(userList);
          } else {
            return resolve(this._completeUserList(userList));
          }
        } catch (error) {
          err = error;
          errMsg = `Failed to get users: ${this.beautifyError(err)}`;
          this.logger.warn(`${meth} ERROR: ${errMsg}`);
          return reject(new Error(errMsg));
        }
      });
    }

    _getUser(username, raw = false) {
      var meth;
      meth = `KeycloakUserManager._getUser(${username})`;
      this.logger.info(`${meth}`);
      return q.promise(async (resolve, reject) => {
        var err, errMsg, userList, userQuery;
        try {
          this.logger.info(`${meth} Getting user...`);
          userQuery = {
            username: username
          };
          userList = await this.kcAdminClient.users.find(userQuery);
          if (userList.length === 0) {
            throw new Error("user not found.");
          } else if (userList.length > 1) {
            throw new Error(`found ${userList.length} users matching username.`);
          }
          if (raw) {
            return resolve(userList[0]);
          } else {
            return resolve(this._completeUser(userList[0]));
          }
        } catch (error) {
          err = error;
          errMsg = `Failed to get user with username '${username}': ${this.beautifyError(err)}`;
          this.logger.warn(`${meth} ERROR: ${errMsg}`);
          return reject(new Error(errMsg));
        }
      });
    }

    _createUser(userData) {
      var meth;
      meth = 'KeycloakUserManager._createUser';
      this.logger.info(`${meth} User data: ${JSON.stringify(userData)}`);
      return q.promise((resolve, reject) => {
        var err, errMsg;
        try {
          this._validateNewUserData(userData);
          // Get groups mappings for easier manipulation
          this.logger.info(`${meth} Creating groups id-name mappings...`);
          return this._getGroupsMaps().then(async groupsMaps => {
            var groupId, groupName, groupResult, i, len, newUser, ref, userCredentials, userId, userResult;
            this.logger.info(`${meth} Validating user group names...`);
            this._validateGroupList(groupsMaps, userData.groups);
            this.logger.info(`${meth} Creating new user...`);
            userCredentials = {
              temporary: false,
              type: 'password',
              value: userData.password
            };
            newUser = {
              username: userData.username,
              firstName: userData.firstName,
              lastName: userData.lastName,
              email: userData.email,
              enabled: userData.enabled,
              credentials: [userCredentials]
            };
            // Create user
            userResult = await this.kcAdminClient.users.create(newUser);
            // returns { id: "<user_id>" }
            userId = userResult.id;
            this.logger.info(`${meth} Creating new user role bindings...`);
            ref = userData.groups || [];
            // Add user to each of its groups
            for (i = 0, len = ref.length; i < len; i++) {
              groupName = ref[i];
              groupId = groupsMaps.name2id[groupName];
              groupResult = await this._addUserToGroup(userId, groupId);
            }
            return resolve(true);
          }).catch(err => {
            var errMsg;
            // Special case: we must duplicate error handling since there are
            // await calls inside a Promise.then() function
            errMsg = `Failed to create user '${userData.username}': ${this.beautifyError(err)}`;
            this.logger.warn(`${meth} ERROR: ${errMsg}`);
            return reject(new Error(errMsg));
          });
        } catch (error) {
          err = error;
          errMsg = `Failed to create user '${userData.username}': ${this.beautifyError(err)}`;
          this.logger.warn(`${meth} ERROR: ${errMsg}`);
          return reject(new Error(errMsg));
        }
      });
    }

    _updateUser(userData) {
      var meth;
      meth = 'KeycloakUserManager._updateUser';
      this.logger.info(`${meth} User data: ${JSON.stringify(userData)}`);
      return q.promise(async (resolve, reject) => {
        var currentGroup, currentUser, currentUserGroups, err, errMsg, groupId, groupIdsToAdd, groupIdsToRemove, groupResult, groupsMaps, groupsToAdd, i, index, j, k, len, len1, len2, newUserGroups, query, updateUserResult, userCredentials, userId;
        try {
          // @_validateUserData userData
          if ((userData != null ? userData.username : void 0) == null) {
            throw new Error("User data is missing required property 'username'");
          }
          this.logger.info(`${meth} Retrieving current user data...`);
          currentUser = await this._getUser(userData.username, true);
          userId = currentUser.id;
          this.logger.info(`${meth} Got user data (ID: ${userId}).`);
          // Convert password data to credential format
          if (userData.password != null) {
            userCredentials = {
              temporary: false,
              type: 'password',
              value: userData.password
            };
            userData.credentials = [userCredentials];
          }
          delete userData.password;
          // Keep a copy of groups and remove it from user data
          newUserGroups = null;
          if (userData.groups != null) {
            newUserGroups = userData.groups.slice();
            delete userData.groups;
          }
          // Validate user groups
          this.logger.info(`${meth} Validating user group names...`);
          groupsMaps = await this._getGroupsMaps();
          this._validateGroupList(groupsMaps, newUserGroups);
          this.logger.info(`${meth} Updating user data...`);
          // Update user (return empty string on success)
          query = {
            id: userId
          };
          updateUserResult = await this.kcAdminClient.users.update(query, userData);
          this.logger.info(`${meth} User data updated.`);
          // If a group list was supplied, the new list replaces the new one
          if (newUserGroups != null) {
            this.logger.info(`${meth} Preparing to update user groups...`);
            // Determine which groups association must be added and removed
            currentUserGroups = await this._getUserGroups(userId, true);
            this.logger.info(`${meth} Got user current groups.`);
            // Holds group names (later converted to ids)
            groupsToAdd = newUserGroups.slice(); // Make a copy of original list
            // Holds group ids
            groupIdsToRemove = [];
            for (i = 0, len = currentUserGroups.length; i < len; i++) {
              currentGroup = currentUserGroups[i];
              index = groupsToAdd.indexOf(currentGroup.name);
              if (index === -1) {
                // Old group is not in new list. Add it to remove list
                groupIdsToRemove.push(currentGroup.id);
              } else {
                // Old group is in new list. Remove it from add list
                groupsToAdd.splice(index, 1);
              }
            }
            // Convert groupsToAdd name list to ids.
            groupIdsToAdd = groupsToAdd.map(function (g) {
              return groupsMaps.name2id[g];
            });
            this.logger.info(`${meth} Removing groups: ${groupIdsToRemove}`);
            this.logger.info(`${meth} Adding groups: ${groupIdsToAdd}`);
            // Remove obsolete groups associations
            for (j = 0, len1 = groupIdsToRemove.length; j < len1; j++) {
              groupId = groupIdsToRemove[j];
              groupResult = await this._removeUserFromGroup(userId, groupId);
            }
            // Add new groups associations
            for (k = 0, len2 = groupIdsToAdd.length; k < len2; k++) {
              groupId = groupIdsToAdd[k];
              groupResult = await this._addUserToGroup(userId, groupId);
            }
            this.logger.info(`${meth} User groups update finished.`);
          }
          return resolve(true);
        } catch (error) {
          err = error;
          errMsg = `Failed to update user with username '${userData.username}': ${this.beautifyError(err)}`;
          this.logger.warn(`${meth} ERROR: ${errMsg}`);
          return reject(new Error(errMsg));
        }
      });
    }

    _deleteUser(username, accessToken) {
      var meth;
      meth = `KeycloakUserManager._deleteUser(${username})`;
      this.logger.info(`${meth}`);
      return q.promise(async (resolve, reject) => {
        var err, errMsg, query, userId, userList;
        try {
          this.logger.info(`${meth} Deleting user...`);
          // Retrieve user ID
          query = {
            username: username
          };
          userList = await this.kcAdminClient.users.findOne(query);
          // *** WARNING: This returns an array (despite the docs and its name ***
          if (userList.length > 1) {
            throw new Error(`Found ${userList.length} users matching username.`);
          }
          if (userList.length < 1) {
            throw new Error(`User ${username} not found.`);
          }
          userId = userList[0].id;
          this.logger.info(`${meth} Deleting user with ID ${userId}...`);
          await this.kcAdminClient.users.del({
            id: userId
          });
          return resolve(true);
        } catch (error) {
          err = error;
          errMsg = `Failed to delete user with username '${username}': ${this.beautifyError(err)}`;
          this.logger.warn(`${meth} ERROR: ${errMsg}`);
          return reject(new Error(errMsg));
        }
      });
    }

    _getUserGroups(userId, raw = false) {
      var meth;
      meth = `KeycloakUserManager._getUserGroups(${userId})`;
      this.logger.info(`${meth}`);
      return q.promise(async (resolve, reject) => {
        var err, errMsg, groupList, query;
        try {
          query = {
            id: userId
          };
          groupList = await this.kcAdminClient.users.listGroups(query);
          if (raw) {
            return resolve(groupList);
          } else {
            return resolve(this._cleanGroupList(groupList));
          }
        } catch (error) {
          err = error;
          errMsg = `Failed to get groups for user: ${this.beautifyError(err)}`;
          this.logger.warn(`${meth} ERROR: ${errMsg}`);
          return reject(new Error(errMsg));
        }
      });
    }

    // Returns a promise resolved with the list of existing groups names/objects.
    // If raw is 'true', each group is returned as the original object.
    // If raw is 'false', each group is returned as a single string (its name).
    _getGroups(raw = false) {
      var meth;
      meth = 'KeycloakUserManager._getGroups';
      this.logger.info(`${meth}`);
      return q.promise(async (resolve, reject) => {
        var err, errMsg, groupList, groupQuery;
        try {
          this.logger.info(`${meth} Getting all groups...`);
          groupQuery = {};
          groupList = await this.kcAdminClient.groups.find(groupQuery);
          this.logger.info(`${meth} Got all groups...`);
          if (raw) {
            return resolve(groupList);
          } else {
            return resolve(this._cleanGroupList(groupList));
          }
        } catch (error) {
          err = error;
          errMsg = `Failed to get group list: ${this.beautifyError(err)}`;
          this.logger.warn(`${meth} ERROR: ${errMsg}`);
          return reject(new Error(errMsg));
        }
      });
    }

    // Returns a promise resolved with an object containing two dictionnaries
    // representing mapping group IDs with group names and viceversa.
    _getGroupsMaps() {
      var meth;
      meth = 'KeycloakUserManager._getGroupsMaps';
      this.logger.info(`${meth}`);
      return this._getGroups(true).then(rawGroupList => {
        var i, len, maps, rawGroup;
        this.logger.info(`${meth} Got raw group list...`);
        maps = {
          id2name: {},
          name2id: {}
        };
        for (i = 0, len = rawGroupList.length; i < len; i++) {
          rawGroup = rawGroupList[i];
          maps.id2name[rawGroup.id] = rawGroup.name;
          maps.name2id[rawGroup.name] = rawGroup.id;
        }
        return maps;
      });
    }

    // Returns a promise resolved with group ID of the group matching the provided
    // group name.
    _getGroupId(groupName) {
      var meth;
      meth = `KeycloakUserManager._getGroupId(${groupName})`;
      this.logger.info(`${meth}`);
      return q.promise(async (resolve, reject) => {
        var err, errMsg, groupList, query;
        try {
          query = {
            search: groupName
          };
          groupList = await this.kcAdminClient.groups.find(query);
          if (groupList.length > 1) {
            throw new Error(`Found ${groupList.length} groups matching name.`);
          }
          this.logger.debug(`GOT GROUP FOR name=${groupName}: ${JSON.stringify(group)}`);
          return resolve(groupList[0].id);
        } catch (error) {
          err = error;
          errMsg = `Failed to get group id: ${this.beautifyError(err)}`;
          this.logger.warn(`${meth} ERROR: ${errMsg}`);
          return reject(new Error(errMsg));
        }
      });
    }

    // Add a user to a group, both provided as their IDs.
    // Returns a promise resolved with 'true' if user is added to the group.
    _addUserToGroup(userId, groupId) {
      var meth;
      meth = 'KeycloakUserManager._addUserToGroup';
      console.log(`${meth} userId: ${userId} - groupId: ${groupId}`);
      return q.promise(async (resolve, reject) => {
        var data, err, errMsg, result;
        try {
          data = {
            id: userId,
            groupId: groupId
          };
          result = await this.kcAdminClient.users.addToGroup(data);
          return resolve(true);
        } catch (error) {
          err = error;
          errMsg = `Failed to add user to group: ${this.beautifyError(err)}`;
          this.logger.warn(`${meth} ERROR: ${errMsg}`);
          return reject(new Error(errMsg));
        }
      });
    }

    // Add a user to a group, both provided as their IDs.
    // Returns a promise resolved with 'true' if user is added to the group.
    _removeUserFromGroup(userId, groupId) {
      var meth;
      meth = 'KeycloakUserManager._removeUserFromGroup';
      console.log(`${meth} userId: ${userId} - groupId: ${groupId}`);
      return q.promise(async (resolve, reject) => {
        var data, err, errMsg, result;
        try {
          data = {
            id: userId,
            groupId: groupId
          };
          result = await this.kcAdminClient.users.delFromGroup(data);
          return resolve(true);
        } catch (error) {
          err = error;
          errMsg = `Failed to remove user from group: ${this.beautifyError(err)}`;
          this.logger.warn(`${meth} ERROR: ${errMsg}`);
          return reject(new Error(errMsg));
        }
      });
    }

    //#############################################################################
    //#                             HELPER METHODS                               ##
    //#############################################################################

    // Filter to only keep relevant group properties (its name)
    // Raw group returned by Keycloak library:
    //  {
    //    "id": "924b54df-693d-4db6-97d4-5ae358938872",
    //    "name": "developers",
    //    "path": "/developers"
    //  }
    _cleanGroup(rawGroup) {
      var meth;
      meth = 'KeycloakUserManager._cleanGroup';
      this.logger.info(`${meth}`);
      return rawGroup.name;
    }

    // Filter to only keep relevant group properties (its name)
    _cleanGroupList(rawGroupList) {
      var groupList, i, len, meth, rawGroup;
      meth = 'KeycloakUserManager._cleanGroupList';
      this.logger.info(`${meth}`);
      groupList = [];
      for (i = 0, len = rawGroupList.length; i < len; i++) {
        rawGroup = rawGroupList[i];
        groupList.push(this._cleanGroup(rawGroup));
      }
      return groupList;
    }

    // Filter to only keep relevant user properties, plus add user groups.
    // Raw user returned by Keycloak library:
    //  {
    //    "access": {
    //      "manage": true,
    //      "impersonate": false,
    //      "mapRoles": true,
    //      "view": true,
    //      "manageGroupMembership": true
    //    },
    //    "notBefore": 0,
    //    "requiredActions": [],
    //    "disableableCredentialTypes": [
    //      "password"
    //    ],
    //    "email": "javier@kumori.systems",
    //    "id": "7df85f09-940f-446b-9219-9f0a20ece1bb",
    //    "createdTimestamp": 1583495410854,
    //    "username": "javier",
    //    "enabled": true,
    //    "totp": false,
    //    "emailVerified": false,
    //    "firstName": "Javier",
    //    "lastName": "Ferrer"
    //  }
    async _completeUser(rawUser) {
      var err, meth, user, userGroups;
      meth = 'KeycloakUserManager._completeUser';
      this.logger.info(`${meth}`);
      this.logger.info(`${meth} rawUser: ${JSON.stringify(rawUser)}`);
      try {
        userGroups = await this._getUserGroups(rawUser.id);
      } catch (error) {
        err = error;
        this.logger.warn(`${meth} Couldn't get groups for user ${rawUser.id}. Reason: ${err.message || err}`);
        userGroups = [];
      }
      user = {
        username: rawUser.username,
        email: rawUser.email,
        firstName: rawUser.firstName,
        lastName: rawUser.lastName,
        enabled: rawUser.enabled,
        credentials: rawUser.credentials,
        groups: userGroups
      };
      this.logger.info(`${meth} Completed User: ${JSON.stringify(user)}`);
      return user;
    }

    // Filter to only keep relevant user properties, plus add user groups.
    async _completeUserList(rawUserList) {
      var i, len, meth, rawUser, user, userList;
      meth = 'KeycloakUserManager._completeUserList';
      this.logger.info(`${meth}`);
      this.logger.info(`${meth} rawUserList: ${JSON.stringify(rawUserList)}`);
      userList = [];
      for (i = 0, len = rawUserList.length; i < len; i++) {
        rawUser = rawUserList[i];
        user = await this._completeUser(rawUser);
        userList.push(user);
      }
      this.logger.info(`${meth} clean UserList: ${JSON.stringify(userList)}`);
      return userList;
    }

    // Validate all required user properties are present
    _validateNewUserData(userData) {
      var errMsg, i, len, meth, missingProps, prop;
      meth = 'KeycloakUserManager._validateNewUserData';
      missingProps = [];
      for (i = 0, len = MANDATORY_USER_PROPERTIES.length; i < len; i++) {
        prop = MANDATORY_USER_PROPERTIES[i];
        if (!(prop in userData) || userData[prop] === '') {
          missingProps.push(prop);
        }
      }
      if (missingProps.length !== 0) {
        errMsg = `User data is missing one or more mandatory properties: ${missingProps}`;
        this.logger.warn(`${meth} - ${errMsg}`);
        throw new Error(errMsg);
      }
    }

    // Validate that all groups in the list exist
    _validateGroupList(groupsMaps, groupList) {
      var errMsg, errors, groupName, i, len, meth;
      meth = 'KeycloakUserManager._validateGroupList';
      if (groupList == null) {
        return;
      }
      errors = [];
      for (i = 0, len = groupList.length; i < len; i++) {
        groupName = groupList[i];
        if (!(groupName in groupsMaps.name2id)) {
          errors.push(groupName);
        }
      }
      if (errors.length !== 0) {
        errMsg = `Invalid groups: ${errors}`;
        this.logger.warn(`${meth} - ${errMsg}`);
        throw new Error(errMsg);
      } else {}
    }

    beautifyError(err) {
      var errMsg, ref, ref1, ref2, ref3;
      if (((ref = err.response) != null ? ref.status : void 0) != null && ((ref1 = err.response) != null ? ref1.statusText : void 0) != null && ((ref2 = err.response) != null ? (ref3 = ref2.data) != null ? ref3.errorMessage : void 0 : void 0) != null) {
        errMsg = `${err.response.data.errorMessage} (Keycloak API Error: ${err.response.status} ${err.response.statusText})`;
      } else {
        errMsg = `${err.message || err}`;
      }
      return errMsg;
    }

  };

  module.exports = KeycloakUserManager;
}).call(undefined);