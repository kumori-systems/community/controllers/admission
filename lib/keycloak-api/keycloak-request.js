'use strict';

(function () {
  /*
  * Copyright 2022 Kumori Systems S.L.
   * Licensed under the EUPL, Version 1.2 or – as soon they
    will be approved by the European Commission - subsequent
    versions of the EUPL (the "Licence");
   * You may not use this work except in compliance with the
    Licence.
   * You may obtain a copy of the Licence at:
     https://joinup.ec.europa.eu/software/page/eupl
   * Unless required by applicable law or agreed to in
    writing, software distributed under the Licence is
    distributed on an "AS IS" basis,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
    express or implied.
   * See the Licence for the specific language governing
    permissions and limitations under the Licence.
   */
  var KeycloakRequest, path, q, request;

  q = require('q');

  path = require('path');

  request = require('request');

  KeycloakRequest = class KeycloakRequest {
    constructor(config) {
      var meth;
      this.config = config;
      meth = 'KeycloakRequest.constructor';
      if (!this.logger) {
        this.logger = {
          error: console.log,
          warn: console.log,
          info: console.log,
          debug: console.log
        };
      }
      this.logger.info(`${meth} CONFIG: ${JSON.stringify(this.config)}`);
    }

    // Gets an Access Token for the given user credentials (username and password)
    // from Keycloak.

    // Equivalent to the OpenID API call:

    // curl -X POST \
    //    http://{hostname}:8080/auth/realms/{realm}/protocol/openid-connect/token \
    //    -H 'Content-Type: application/x-www-form-urlencoded' \
    //    -d 'username=admin&password=admin&grant_type=password&client_id=admin-cli'
    getAccessToken(username, password) {
      var meth;
      meth = 'KeycloakRequest.getAccessToken';
      return q.promise((resolve, reject) => {
        var formData, postData, url, urlBase, urlRoute;
        // KEYCLOAK GET TOKEN API CALL (OPENID PROTOCOL)

        // Method: POST
        // URL: https://keycloak.example.com/auth/realms/myrealm/protocol/openid-connect/token
        // Body type: x-www-form-urlencoded
        // Form fields:
        // username: username
        // password: password
        // grant_type : password
        // client_id : <my-client-name>
        urlBase = this.config['auth-server-url'];
        urlRoute = path.join('realms', this.config.realm, 'protocol', 'openid-connect', 'token');
        url = `${urlBase}/${urlRoute}`;
        formData = {
          username: username,
          password: password,
          grant_type: 'password',
          client_id: this.config.resource, // 'admission'
          client_secret: this.config.credentials.secret, // '951addd0-d563-439e-aa74-dfcb0e55ba64',
          scope: 'offline_access'
        };
        postData = {
          url: url,
          form: formData
        };
        // @logger.debug "#{meth} Request POST data: #{JSON.stringify postData}"
        return request.post(postData, (error, response, body) => {
          var errMsg, parsedBody, result;
          if (error) {
            errMsg = `Unable to get token for user ${username}: ${error.message}`;
            this.logger.error(`${meth} ERROR: ${errMsg}`);
            return reject(new Error(errMsg));
          } else if (response.statusCode !== 200) {
            errMsg = `Unable to get token for user ${username}: unexpected HTTP status ${response.statusCode}`;
            this.logger.error(`${meth} ERROR: ${errMsg}`);
            return reject(new Error(errMsg));
          } else {
            parsedBody = JSON.parse(body);
            // console.log "GOT TOKEN DATA FROM KEYCLOAK:"
            // console.log "#{JSON.stringify parsedBody}"

            // Returned information has the following format:

            // {
            //   "access_token": "eyJhbG...Z88Q",
            //   "expires_in": 36000,
            //   "refresh_expires_in": 600,
            //   "refresh_token": "eyJhbG...AgNY",
            //   "token_type": "bearer",
            //   "not-before-policy": 0,
            //   "session_state": "49713eaa-0836-4efa-b619-30a6fbb323dc",
            //   "scope": "profile email"
            // }
            result = parsedBody;
            result.user = {};
            return resolve(result);
          }
        });
      });
    }

    // Gets an Access Token for the given user credentials (username and password)
    // from Keycloak.

    // Equivalent to the OpenID API call:

    // curl -X POST \
    //    http://{hostname}:8080/auth/realms/{realm}/protocol/openid-connect/token \
    //    -H 'Content-Type: application/x-www-form-urlencoded' \
    //    -d 'username=admin&password=admin&grant_type=password&client_id=admin-cli'
    refreshAccessToken(refreshToken) {
      var meth;
      meth = 'KeycloakRequest.refreshAccessToken';
      return q.promise((resolve, reject) => {
        var formData, postData, url, urlBase, urlRoute;
        // KEYCLOAK REFRESH TOKEN API CALL (OPENID PROTOCOL)

        // Method: POST
        // URL: https://keycloak.example.com/auth/realms/myrealm/protocol/openid-connect/token
        // Body type: x-www-form-urlencoded
        // Form fields:
        // client_id : <my-client-name>
        // grant_type : refresh_token
        // refresh_token: <my-refresh-token>
        urlBase = this.config['auth-server-url'];
        urlRoute = path.join('realms', this.config.realm, 'protocol', 'openid-connect', 'token');
        url = `${urlBase}/${urlRoute}`;
        formData = {
          refresh_token: refreshToken,
          grant_type: 'refresh_token',
          client_id: this.config.resource, // 'test-webserver'
          client_secret: this.config.credentials.secret // '951addd0-d563-439e-aa74-dfcb0e55ba64',
        };
        postData = {
          url: url,
          form: formData
        };
        // @logger.debug "#{meth} Request POST data: #{JSON.stringify postData}"
        return request.post(postData, (error, response, body) => {
          var errMsg, parsedBody, result;
          if (error) {
            errMsg = `Unable to refresh token : ${error.message}`;
            this.logger.error(`${meth} ERROR: ${errMsg}`);
            return reject(new Error(errMsg));
          } else if (response.statusCode !== 200) {
            errMsg = `Unable to refresh token :unexpected HTTP status ${response.statusCode}`;
            this.logger.error(`${meth} ERROR: ${errMsg}`);
            return reject(new Error(errMsg));
          } else {
            parsedBody = JSON.parse(body);
            // console.log "GOT TOKEN DATA FROM KEYCLOAK:"
            // console.log "#{JSON.stringify parsedBody}"

            // Returned information has the following format:

            // {
            //   "access_token": "eyJhbG...Z88Q",
            //   "expires_in": 36000,
            //   "refresh_expires_in": 600,
            //   "refresh_token": "eyJhbG...AgNY",
            //   "token_type": "bearer",
            //   "not-before-policy": 0,
            //   "session_state": "49713eaa-0836-4efa-b619-30a6fbb323dc",
            //   "scope": "profile email"
            // }
            result = parsedBody;
            result.user = {};
            return resolve(result);
          }
        });
      });
    }

    // Gets user info associated to a given Access Token from Keycloak.

    // Equivalent to the OpenID API call:

    // curl -X POST \
    //    http://{hostname}:8080/auth/realms/{realm}/protocol/openid-connect/userinfo \
    //    -H 'Content-Type: application/x-www-form-urlencoded' \
    //    -d 'access_token=xxxxxxxxxxxxxxx'
    getUserInfo(accessToken) {
      var meth;
      meth = 'KeycloakRequest.getUserInfo';
      this.logger.info(`${meth} Getting user info for token: ${accessToken}`);
      return q.promise((resolve, reject) => {
        var formData, postData, url, urlBase, urlRoute;
        urlBase = this.config['auth-server-url'];
        urlRoute = path.join('realms', this.config.realm, 'protocol', 'openid-connect', 'userinfo');
        url = `${urlBase}/${urlRoute}`;
        formData = {
          access_token: accessToken
        };
        postData = {
          url: url,
          form: formData
        };
        // @logger.debug "#{meth} Request POST data: #{JSON.stringify postData}"
        return request.post(postData, (error, response, body) => {
          var errMsg, parsedBody, userData;
          if (error) {
            errMsg = `Unable to userinfo: ${error.message}`;
            this.logger.error(`${meth} ERROR: ${errMsg}`);
            return reject(new Error(errMsg));
          } else if (response.statusCode !== 200) {
            errMsg = `Unable to userinfo: unexpected HTTP status ${response.statusCode}`;
            this.logger.error(`${meth} ERROR: ${errMsg}`);
            return reject(new Error(errMsg));
          } else {
            parsedBody = JSON.parse(body);
            // console.log "GOT USERINFO FROM KEYCLOAK:"
            // console.log "#{JSON.stringify parsedBody}"

            // Returned information has the following format:

            // {
            //   "sub": "2dfbfa1b-25ee-42f0-9bc7-ede4b412a3e9",
            //   "email_verified": false,
            //   "name": "Alice Liddel",
            //   "groups": [
            //     "offline_access",
            //     "user"
            //   ],
            //   "preferred_username": "alice",
            //   "given_name": "Alice",
            //   "client-groups": [
            //     "developer"
            //   ],
            //   "family_name": "Liddel",
            //   "email": "alice@keycloak.org"
            // }
            userData = {
              email: parsedBody.email,
              id: parsedBody.preferred_username,
              username: parsedBody.preferred_username,
              limits: null,
              name: parsedBody.name,
              roles: parsedBody['client-groups'] || []
            };
            this.logger.debug(`${meth} Final user data: ${JSON.stringify(userData)}`);
            return resolve(userData);
          }
        });
      });
    }

    authenticate(accessToken) {
      return this.getUserInfo(accessToken);
    }

  };

  module.exports = KeycloakRequest;
}).call(undefined);