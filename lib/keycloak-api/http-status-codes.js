"use strict";

(function () {
  /*
  * Copyright 2022 Kumori Systems S.L.
   * Licensed under the EUPL, Version 1.2 or – as soon they
    will be approved by the European Commission - subsequent
    versions of the EUPL (the "Licence");
   * You may not use this work except in compliance with the
    Licence.
   * You may obtain a copy of the Licence at:
     https://joinup.ec.europa.eu/software/page/eupl
   * Unless required by applicable law or agreed to in
    writing, software distributed under the Licence is
    distributed on an "AS IS" basis,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
    express or implied.
   * See the Licence for the specific language governing
    permissions and limitations under the Licence.
   */
  module.exports = Object.freeze({
    HTTP_STATUS_CODES: {
      400: "400 Bad Request",
      401: "401 Unauthorized",
      402: "402 Payment Required",
      403: "403 Forbidden",
      404: "404 Not Found",
      405: "405 Method Not Allowed",
      406: "406 Not Acceptable",
      407: "407 Proxy Authentication Required",
      408: "408 Request Timeout",
      409: "409 Conflict",
      410: "410 Gone",
      411: "411 Length Required",
      412: "412 Precondition Failed",
      413: "413 Request Entity Too Large",
      414: "414 Request-URI Too Long",
      415: "415 Unsupported Media Type",
      416: "416 Requested Range Not Satisfiable",
      417: "417 Expectation Failed",
      418: "418 I'm a teapot (RFC 2324)",
      420: "420 Enhance Your Calm (Twitter)",
      422: "422 Unprocessable Entity (WebDAV)",
      423: "423 Locked (WebDAV)",
      424: "424 Failed Dependency (WebDAV)",
      425: "425 Reserved for WebDAV",
      426: "426 Upgrade Required",
      428: "428 Precondition Required",
      429: "429 Too Many Requests",
      431: "431 Request Header Fields Too Large",
      444: "444 No Response (Nginx)",
      449: "449 Retry With (Microsoft)",
      450: "450 Blocked by Windows Parental Controls (Microsoft)",
      451: "451 Unavailable For Legal Reasons",
      499: "499 Client Closed Request (Nginx)",
      500: "500 Internal Server Error",
      501: "501 Not Implemented",
      502: "502 Bad Gateway",
      503: "503 Service Unavailable",
      504: "504 Gateway Timeout",
      505: "505 HTTP Version Not Supported",
      506: "506 Variant Also Negotiates (Experimental)",
      507: "507 Insufficient Storage (WebDAV)",
      508: "508 Loop Detected (WebDAV)",
      509: "509 Bandwidth Limit Exceeded (Apache)",
      510: "510 Not Extended",
      511: "511 Network Authentication Required",
      598: "598 Network read timeout error",
      599: "599 Network connect timeout error"
    }
  });
}).call(undefined);