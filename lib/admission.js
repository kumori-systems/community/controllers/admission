'use strict';

(function () {
  /*
  * Copyright 2022 Kumori Systems S.L.
   * Licensed under the EUPL, Version 1.2 or – as soon they
    will be approved by the European Commission - subsequent
    versions of the EUPL (the "Licence");
   * You may not use this work except in compliance with the
    Licence.
   * You may obtain a copy of the Licence at:
     https://joinup.ec.europa.eu/software/page/eupl
   * Unless required by applicable law or agreed to in
    writing, software distributed under the Licence is
    distributed on an "AS IS" basis,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
    express or implied.
   * See the Licence for the specific language governing
    permissions and limitations under the Licence.
   */
  var Admission,
      Authorization,
      DockerRegistryProxy,
      EventEmitter,
      ManifestHelper,
      MockLimitChecker,
      q,
      utils,
      indexOf = [].indexOf;

  q = require('q');

  EventEmitter = require('events').EventEmitter;

  ManifestHelper = require('./manifest-helper');

  Authorization = require('./authorization');

  utils = require('./utils');

  DockerRegistryProxy = require('./docker-registry-proxy');

  //###############################################################################
  //#  Temporary mock classes for adapting Admission to KM3 model.               ##
  //###############################################################################
  MockLimitChecker = class MockLimitChecker {
    constructor(config) {
      this.config = config;
    }

    getLimits() {
      return this.config.limits;
    }

    check() {
      return [];
    }

  };

  //###############################################################################
  //#  End of temporary mock classes for adapting Admission to KM3 model.        ##
  //###############################################################################
  Admission = class Admission extends EventEmitter {
    constructor(config, manifestRepository, planner) {
      var base, ref1, ref2, ref3, ref4;
      super();
      this.config = config;
      this.manifestRepository = manifestRepository;
      this.planner = planner;
      this.logger.info('Admission.constructor');
      this.manifestHelper = null;
      this.dockerRegistryProxy = null;
      if (this.config == null) {
        this.config = {};
      }
      // TODO: When planner API publishes information on its limits, this should
      // combined somehow with Planner limits.
      if ((base = this.config).limitChecker == null) {
        base.limitChecker = {
          limits: {}
        };
      }
      // By default, random domains will created as subdomains of RefDomain
      if (((ref1 = this.config.domains) != null ? ref1.random : void 0) != null && ((ref2 = this.config.domains) != null ? ref2.refDomain : void 0) != null) {
        if (((ref3 = this.config.domains) != null ? (ref4 = ref3.random) != null ? ref4.baseDomain : void 0 : void 0) == null) {
          this.config.domains.random.baseDomain = this.config.domains.refDomain;
        }
      }
    }

    init() {
      var dockerRegistryConfig, errMsg, i, index, len, meth, port, ref1;
      meth = 'Admission.init()';
      this.logger.info(meth);
      // Build port dictionnary:  <externalPort> - <internalPort>
      this.config.portDict = {};
      if (this.config.tcpports != null) {
        if (this.config.tcpInternalPorts == null || this.config.tcpports.length !== this.config.tcpInternalPorts.length) {
          errMsg = `Invalid lists of ports: TcpPorts: ${this.config.tcpports} - TcpInternalPorts: ${this.config.tcpInternalPorts}`;
          this.logger.error(`${meth} - ERROR: ${errMsg}`);
          q.reject(new Error(errMsg));
        } else {
          ref1 = this.config.tcpports;
          for (index = i = 0, len = ref1.length; i < len; index = ++i) {
            port = ref1[index];
            this.config.portDict[port] = this.config.tcpInternalPorts[index];
          }
        }
      }
      // If Admission config does not include a ClusterConfiguration section (it
      // shouldn't at present). add one empty
      if (this.config.clusterConfiguration == null) {
        this.config.clusterConfiguration = {};
      }
      // If Planner is an event emitter, register a handler
      if (this.planner.on != null) {
        this.logger.info('Admission.init() - Registering Planner event handler.');
        this.planner.on('planner', evt => {
          this.logger.debug('Admission.onPlannerEvent - forwarding event upwards');
          return this.emit('planner', evt);
        });
      }
      this.authorization = new Authorization(this.manifestRepository, this.planner);
      // Read configuration file and initialize Manifest Storage
      this.manifestHelper = new ManifestHelper();
      this.limitChecker = new MockLimitChecker(this.config.limitChecker, this.manifestHelper);
      this.logger.debug('- Limit Checker initialized.');
      dockerRegistryConfig = this.config.dockerRegistry || {
        username: null,
        password: null,
        mirror: null
      };
      // Create a new Docker registry proxy for image validation
      this.dockerRegistryProxy = new DockerRegistryProxy(dockerRegistryConfig);
      // Initialize element locks
      this.elementLocks = {};
      return q(true);
    }

    terminate() {
      this.logger.info('Admission.terminate()');
      return this.planner.terminate().then(() => {
        this.logger.debug('Planner Terminated.');
        return true;
      });
    }

    //#############################################################################
    //#                              PUBLIC METHODS                              ##
    //#############################################################################

    //###########################
    //#   ELEMENT LOCK CACHE   ##
    //###########################
    acquireElementLockFromRef(elementRef, opID) {
      var elementURN, meth;
      meth = 'Admission.acquireElementLockFromRef';
      this.logger.info(`${meth} - ElementRef: ${JSON.stringify(elementRef)} - OperationID: ${opID}`);
      elementURN = this.manifestHelper.refToUrn(elementRef);
      return this.acquireElementLockFromURN(elementURN, opID);
    }

    acquireElementLockFromURN(elementURN, opID) {
      var meth;
      meth = 'Admission.acquireElementLockFromURN';
      this.logger.info(`${meth} - Element: ${elementURN} - OperationID: ${opID}`);
      return q.Promise((resolve, reject) => {
        var msg;
        if (this.elementLocks[elementURN] != null) {
          // Element has an active lock on it, deny operation
          msg = `element ${elementURN} has an active lock (opID: ${opID})`;
          this.logger.warn(`${meth} - Unable to get lock: ${msg}`);
          return reject(new Error(`Unable to get lock: ${msg}`));
        } else {
          // Element is not locked, lock it
          this.elementLocks[elementURN] = opID;
          this.logger.debug(`${meth} - Added lock to ${elementURN} (opID: ${opID})`);
          this.printElementLocks();
          return resolve();
        }
      });
    }

    releaseElementLockFromRef(elementRef, opID) {
      var elementURN, meth;
      meth = 'Admission.releaseElementLockFromRef';
      this.logger.info(`${meth} - ElementRef: ${JSON.stringify(elementRef)} - OperationID: ${opID}`);
      if (elementRef != null) {
        elementURN = this.manifestHelper.refToUrn(elementRef);
      } else {
        elementURN = "unknownElement";
      }
      return this.releaseElementLockFromURN(elementURN, opID);
    }

    releaseElementLockFromURN(elementURN, opID) {
      var meth;
      meth = 'Admission.releaseElementLockFromURN';
      if (elementURN == null || elementURN === '') {
        elementURN = "unknownElement";
      }
      this.logger.info(`${meth} - Element: ${elementURN} - OperationID: ${opID}`);
      return q.Promise((resolve, reject) => {
        var lockOpID;
        if (this.elementLocks[elementURN] != null) {
          lockOpID = this.elementLocks[elementURN];
          if (opID === lockOpID) {
            this.logger.debug(`${meth} - Releasing lock ${elementURN} (opID: ${opID})`);
            delete this.elementLocks[elementURN];
            this.printElementLocks();
          } else {
            // Element is locked by another operation, do nothing
            this.logger.debug(`${meth} - Element ${elementURN} is locked by a another operation (lockOpID: ${lockOpID} / opID: ${opID})`);
          }
        } else {
          this.logger.debug(`${meth} - Element ${elementURN} had no active lock.`);
        }
        // In all case, return a resolved promise
        return resolve();
      });
    }

    clearElementLocks() {
      var meth;
      meth = 'Admission.clearElementLocks';
      this.logger.warn(`${meth} - Removing all active element locks.`);
      this.elementLocks = {};
      return true;
    }

    printElementLocks() {
      var meth;
      meth = 'Admission.printElementLocks';
      this.logger.info(`${meth} - CURRENT OPERATION LOCKS: ${JSON.stringify(this.elementLocks)}`);
      // console.log '*****************************************************'
      // console.log 'CURRENT OPERATION LOCKS:'
      // console.log ''
      // console.log(JSON.stringify @elementLocks, null, 2)
      // console.log ''
      // console.log '*****************************************************'
      return true;
    }

    getOperationID() {
      var randomStr, timestamp;
      timestamp = this.getDateSuffixLong();
      randomStr = utils.randomHex(8);
      return `${timestamp}_${randomStr}`;
    }

    //###########################
    //#  SOLUTIONS MANAGEMENT  ##
    //###########################
    registerSolution(context, options, manifest = null) {
      var meth, opID, solutionManifest;
      meth = `Admission.registerSolution() ${options.solutionURN}`;
      this.logger.info(meth);
      solutionManifest = null;
      // Calculate a unique ID for this operation
      opID = this.getOperationID();
      // If necessary, read manifest from local file
      return (manifest != null ? q(manifest) : this.loadManifest(options)).then(loadedManifest => {
        var errMsg;
        solutionManifest = loadedManifest;
        // Check that the solution has a ref and a valid ref.domain, or add it
        if (solutionManifest.ref != null) {
          if (solutionManifest.ref.domain != null) {
            // ref.domain must match the id of the user performing the action,
            // except for Admin users
            if (solutionManifest.ref.domain !== context.user.id) {
              if (this.authorization.isAdmin(context)) {
                return this.logger.info(`${meth} - Admin user allowed to use a custom domain.`);
              } else {
                errMsg = `Solution domain doesn't match username: (${solutionManifest.ref.domain} != ${context.user.id}`;
                this.logger.error(`${meth} - ERROR: ${errMsg}`);
                return q.reject(new Error(errMsg));
              }
            }
          } else {
            this.logger.warn(`${meth} - Solution has no domain, adding it.`);
            return solutionManifest.ref.domain = context.user.id;
          }
        } else {
          errMsg = "Solution is missing mandatory property 'ref'.";
          this.logger.error(`${meth} - ERROR: ${errMsg}`);
          throw new Error(errMsg);
        }
      }).then(() => {
        // Aquire lock for the element
        return this.acquireElementLockFromRef(solutionManifest.ref, opID);
      }).then(() => {
        return this.setupSolution(context, options, solutionManifest);
      }).then(() => {
        return this.planner.execSolution(solutionManifest);
      }).then(solutionInfo => {
        this.logger.debug(`${meth} - Solution created: ${JSON.stringify(solutionInfo)}`);
        return solutionInfo;
      }).finally(() => {
        return this.releaseElementLockFromRef((solutionManifest != null ? solutionManifest.ref : void 0) || null, opID);
      });
    }

    deleteSolution(context, options) {
      var meth, opID, solutionManifest, solutionURN, topDeploymentManifest, topDeploymentRef;
      meth = `Admission.deleteSolution ${options.solutionURN}`;
      this.logger.info(meth);
      this.logger.debug(`${meth} - OPTIONS: ${JSON.stringify(options)}`);
      solutionURN = options.solutionURN;
      solutionManifest = null;
      topDeploymentRef = null;
      topDeploymentManifest = null;
      // Calculate a unique ID for this operation
      opID = this.getOperationID();
      // Get lock for the element being deleted
      return this.acquireElementLockFromURN(solutionURN, opID).then(() => {
        return this.getSolutionFromURN(solutionURN, context);
      }).then(manifest => {
        var errMsg;
        if (manifest == null) {
          errMsg = `Solution ${solutionURN} not found.`;
          this.logger.warn(`${meth} - ${errMsg}`);
          return q.reject(new Error(errMsg));
        }
        // Solution exists, check its ownership
        solutionManifest = manifest;
        this.logger.debug(`${meth} - Solution ${solutionURN} exists`);
        if (!this.authorization.isAllowedForManifest(manifest, context, true)) {
          errMsg = "User is not allowed to delete solution.";
          this.logger.warn(`${meth} - ${errMsg}`);
          return q.reject(new Error(errMsg));
        } else {
          // Ownership check is ok, check existing links of the top deployment
          topDeploymentRef = {
            kind: 'deployment',
            domain: solutionManifest.ref.domain,
            name: solutionManifest.top
          };
          return this.getDeploymentFromRef(topDeploymentRef, context);
        }
      }).then(manifest => {
        if (manifest == null) {
          this.logger.info(`${meth} - Top deployment ${this.manifestHelper.refToUrn(topDeploymentRef)} not found. Skipping link verification.`);
          return q({});
        } else {
          this.logger.debug(`${meth} - Checking active links of the top deployment...`);
          topDeploymentManifest = manifest;
          return this.planner.getSolutionLinks(solutionManifest.name, solutionURN, topDeploymentManifest.name, topDeploymentManifest.urn);
        }
      }).then(links => {
        var errMsg, linkManifest, linkName, linkNames, promises;
        linkNames = Object.keys(links);
        if (linkNames.length > 0) {
          // Solution has active links
          if (options.force) {
            this.logger.info(`${meth} Automatically removing existing links...`);
            promises = [];
            for (linkName in links) {
              linkManifest = links[linkName];
              promises.push(this.unlinkServices(context, linkManifest));
            }
            return q.all(promises).then(() => {
              this.logger.info(`${meth} All solution links removed.`);
              return true;
            });
          } else {
            errMsg = `Solution ${solutionURN} has active links: ${linkNames}`;
            this.logger.error(`${meth} - ${errMsg}`);
            return q.reject(new Error(errMsg));
          }
        } else {
          // No links
          return true;
        }
      }).then(() => {
        return this.planner.deleteSolution(solutionManifest.name, 'solution');
      }).then(deleteSolutionInfo => {
        this.logger.debug(`${meth} - Solution deleted.`);
        return deleteSolutionInfo;
      }).finally(() => {
        return this.releaseElementLockFromURN(solutionURN, opID);
      });
    }

    setupSolution(context, options, solutionManifest) {
      var currentTimestamp, errMsg, meth, solutionName, solutionURN;
      meth = 'Admission.setupSolution()';
      this.logger.info(meth);
      this.logger.debug(`${meth} - Options: ${JSON.stringify(options)}`);
      solutionName = null;
      solutionURN = null;
      currentTimestamp = utils.getTimestamp();
      // Check if the manifest includes an 'owner' property and if it matches the
      // ref.domain
      if (solutionManifest.owner != null) {
        if (solutionManifest.owner === '') {
          // Invalid owner received, delete it
          delete solutionManifest.owner;
        } else if (solutionManifest.owner !== solutionManifest.ref.domain) {
          // 'owner' property must match the ref.domain, except for Admin users
          if (this.authorization.isAdmin(context)) {
            this.logger.info(`${meth} - Admin user allowed to provide an owner different from the ref.domain.`);
          } else {
            errMsg = `Solution 'owner' property doesn't match the resource domain: ${solutionManifest.owner} != ${solutionManifest.ref.domain}`;
            this.logger.error(`${meth} - ERROR: ${errMsg}`);
            return q.reject(new Error(errMsg));
          }
        }
      }
      // Variable just used for log traces
      solutionName = solutionManifest.ref.domain + '/' + solutionManifest.ref.name;
      // Validate manifest
      return this.validateSolution(solutionManifest, context).then(() => {
        this.logger.debug(`${meth} - Checking existence of solution: ${solutionName} ...`);
        // Check if solution already exists
        solutionURN = this.manifestHelper.refToUrn(solutionManifest.ref);
        return this.getSolutionFromURN(solutionURN, context);
      }).then(previousManifest => {
        var ref1, ref2;
        if (previousManifest != null) {
          // Deployment already exists, check its ownership
          this.logger.debug(`${meth} - Solution ${solutionName} already exists`);
          if (!this.authorization.isAllowedForManifest(previousManifest, context, true)) {
            errMsg = "User is not allowed to modify solution.";
            this.logger.warn(`${meth} - ${errMsg}`);
            return q.reject(new Error(errMsg));
          } else {
            // Complete deployment information
            if (solutionManifest.owner == null) {
              solutionManifest.owner = previousManifest.owner || ((ref1 = context.user) != null ? ref1.id : void 0);
            }
            solutionManifest.name = previousManifest.name;
            solutionManifest.urn = previousManifest.urn || this.manifestHelper.refToUrn(solutionManifest.ref);
            // Set timestamps
            solutionManifest.creationTimestamp = previousManifest.creationTimestamp || currentTimestamp;
            solutionManifest.lastModification = currentTimestamp;
            return solutionManifest;
          }
        } else {
          // Deployment is new, handle as a creation
          this.logger.debug(`${meth} - Solution ${solutionName} is new`);
          // Add a URN representing the solution for easier searches
          solutionManifest.urn = solutionURN;
          // Add owner property from user information, if not present
          if (solutionManifest.owner == null) {
            solutionManifest.owner = (ref2 = context.user) != null ? ref2.id : void 0;
          }
          // Add timestamps
          solutionManifest.creationTimestamp = currentTimestamp;
          solutionManifest.lastModification = currentTimestamp;
          // Add an internal name
          solutionManifest.name = this.getSolutionID();
          return solutionManifest;
        }
      }).catch(err => {
        this.logger.warn(`${meth} - ${err.message}`);
        return q.reject(err);
      });
    }

    validateSolution(solutionManifest, context) {
      return this.validateSolutionDeployments(solutionManifest, context).then(() => {
        return this.validateSolutionResources(solutionManifest, context);
      }).then(() => {
        return this.validateSolutionLinks(solutionManifest, context);
      }).then(() => {
        return q(solutionManifest);
      });
    }

    solutionQuery(context, params) {
      this.logger.info('Admission.solutionQuery()');
      return q(this.planner.solutionQuery(params));
    }

    getSolution(filter, context) {
      var errMsg, meth, name, params, ref1;
      meth = `Admission.getSolution(${JSON.stringify(filter)})`;
      this.logger.info(`${meth} - User: ${(ref1 = context.user) != null ? ref1.id : void 0}`);
      name = null;
      params = {
        show: 'manifest'
      };
      if (filter.urn != null) {
        name = filter.urn;
        params.urn = filter.urn;
      } else {
        errMsg = `Wrong filter for solution search: ${JSON.stringify(filter)}`;
        this.logger.warn(`${meth} - ${errMsg}`);
        return q.reject(new Error(errMsg));
      }
      this.logger.info(`${meth} - Searching for solution: ${name}`);
      return this.planner.solutionQuery(params).then(solutionList => {
        var deploymentURN;
        if (Object.keys(solutionList).length === 0) {
          this.logger.debug(`${meth} - No solution found for ${name}`);
          return null;
        } else if (Object.keys(solutionList).length > 1) {
          errMsg = `Several matches found for solution ${name}`;
          this.logger.warn(`${meth} - ${errMsg}: ${JSON.stringify(solutionList)}`);
          return q.reject(new Error(errMsg));
        } else {
          deploymentURN = Object.keys(solutionList)[0];
          this.logger.info(`${meth} - Found solution for ${name} : ${deploymentURN}`);
          return solutionList[deploymentURN];
        }
      });
    }

    getSolutionFromURN(urn, context) {
      var filter, meth, ref1;
      meth = `Admission.getSolutionFromURN(${urn})`;
      this.logger.info(`${meth} - User: ${(ref1 = context.user) != null ? ref1.id : void 0}`);
      filter = {
        urn: urn
      };
      return this.getSolution(filter, context);
    }

    getSolutionFromRef(ref, context) {
      var filter, meth, ref1;
      meth = `Admission.getSolutionFromRef(${JSON.stringify(ref)})`;
      this.logger.info(`${meth} - User: ${(ref1 = context.user) != null ? ref1.id : void 0}`);
      filter = {
        urn: this.manifestHelper.refToUrn(ref)
      };
      return this.getSolution(filter, context);
    }

    //#############################
    //#  DEPLOYMENTS MANAGEMENT  ##
    //#############################
    deploy(context, deploymentOptions, manifest = null) {
      this.logger.info('Admission.deploy()');
      return this.setupDeploy(context, deploymentOptions, manifest).then(fullDeployment => {
        this.logger.debug(`after setupDeploy: ${JSON.stringify(fullDeployment)}`);
        return this.planner.execDeployment(fullDeployment).then(deploymentInfo => {
          this.logger.debug(' --> Deployment executed by Planner. ' + `${JSON.stringify(deploymentInfo)}`);
          return deploymentInfo;
        });
      });
    }

    undeploy(context, options) {
      var deploymentManifest, deploymentURN, meth;
      meth = `Admission.undeploy ${options.deploymentURN}`;
      if (options.force) {
        meth += ' (force)';
      }
      this.logger.info(meth);
      this.logger.debug(`UNDEPLOY: ${JSON.stringify(options)}`);
      deploymentURN = options.deploymentURN;
      deploymentManifest = null;
      return this.getDeploymentFromURN(deploymentURN, context).then(manifest => {
        var errMsg;
        if (manifest == null) {
          errMsg = `Deployment ${deploymentURN} not found.`;
          this.logger.warn(`${meth} - ${errMsg}`);
          return q.reject(new Error(errMsg));
        }
        // Deployment exists, check its ownership
        deploymentManifest = manifest;
        this.logger.debug(`${meth} - Deployment ${deploymentURN} exists`);
        if (!this.authorization.isAllowedForManifest(manifest, context, true)) {
          errMsg = "User is not allowed to delete deployment.";
          this.logger.warn(`${meth} - ${errMsg}`);
          return q.reject(new Error(errMsg));
        } else {
          // Ownership check is ok, check existing links
          return this.planner.getDeploymentLinks(manifest);
        }
      }).then(links => {
        var errMsg, linkManifest, linkName, linkNames, promises;
        linkNames = Object.keys(links);
        if (linkNames.length > 0) {
          // Deployment has active links
          if (options.force) {
            this.logger.info(`${meth} Automatically removing existing links...`);
            promises = [];
            for (linkName in links) {
              linkManifest = links[linkName];
              promises.push(this.unlinkServices(context, linkManifest));
            }
            return q.all(promises).then(() => {
              this.logger.info(`${meth} All deployment links removed.`);
              return true;
            });
          } else {
            errMsg = `Deployment ${deploymentURN} has active links: ${linkNames}`;
            this.logger.error(`${meth} - ${errMsg}`);
            return q.reject(new Error(errMsg));
          }
        } else {
          // No links
          return true;
        }
      }).then(() => {
        return this.planner.execUndeployment(deploymentManifest.name, deploymentManifest.ref.kind);
      }).then(undeploymentInfo => {
        this.logger.debug(' --> Undeployment executed by Planner.');
        return undeploymentInfo;
      });
    }

    setupDeploy(context, deploymentOptions, manifest = null) {
      var deploymentManifest, meth, refStr;
      meth = 'Admission.setupDeploy()';
      this.logger.info(meth);
      this.logger.debug(`${meth} - Options: ${JSON.stringify(deploymentOptions)}`);
      refStr = null;
      deploymentManifest = null;
      // If necessary, read manifest from local file
      return (manifest != null ? q(manifest) : this.loadManifest(deploymentOptions)).then(deplManifest => {
        deploymentManifest = deplManifest;
        // Validate manifest
        return this.validateV3Deployment(deploymentManifest, context);
      }).then(() => {
        refStr = JSON.stringify(deploymentManifest.ref);
        this.logger.debug(`${meth} - Checking existence of deployment: ${refStr} ...`);
        // Check if deployment already exists
        return this.getDeploymentFromRef(deploymentManifest.ref, context);
      }).then(previousManifest => {
        var errMsg, ref1, ref2;
        if (previousManifest != null) {
          // Deployment already exists, check its ownership
          this.logger.debug(`${meth} - Deployment ${refStr} already exists`);
          if (!this.authorization.isAllowedForManifest(previousManifest, context, true)) {
            errMsg = "User is not allowed to modify deployment.";
            this.logger.warn(`${meth} - ${errMsg}`);
            return q.reject(new Error(errMsg));
          } else {
            // Complete deployment information
            deploymentManifest.owner = previousManifest.owner || ((ref1 = context.user) != null ? ref1.id : void 0);
            deploymentManifest.name = previousManifest.name;
            deploymentManifest.urn = previousManifest.urn || this.manifestHelper.refToUrn(deploymentManifest.ref);
            return deploymentManifest;
          }
        } else {
          // Deployment is new, handle as a creation
          this.logger.debug(`${meth} - Deployment ${refStr} is new`);
          // Add owner property from user information
          deploymentManifest.owner = (ref2 = context.user) != null ? ref2.id : void 0;
          // Temporarily add a URN representing the ref for easier searches
          // during Kumori Model transition.
          deploymentManifest.urn = this.manifestHelper.refToUrn(deploymentManifest.ref);
          // Add a globally unique name to the deployment manifest (internal ID)
          deploymentManifest.name = this.getDeploymentNameKmv3();
          return deploymentManifest;
        }
      }).catch(err => {
        this.logger.warn(`${meth} - ${err.message}`);
        return q.reject(err);
      });
    }

    deploymentQuery(context, params) {
      this.logger.info('Admission.deploymentQuery()');
      return q(this.planner.deploymentQuery(params));
    }

    getDeployment(filter, context) {
      var errMsg, meth, name, params, ref1;
      meth = `Admission.getDeployment(${JSON.stringify(filter)})`;
      this.logger.info(`${meth} - User: ${(ref1 = context.user) != null ? ref1.id : void 0}`);
      name = null;
      params = {
        show: 'manifest'
      };
      if (filter.ref != null) {
        name = this.manifestHelper.refToUrn(filter.ref);
        params.ref = filter.ref;
      } else if (filter.urn != null) {
        name = filter.urn;
        params.urn = filter.urn;
      } else {
        errMsg = `Wrong filter for deployment search: ${JSON.stringify(filter)}`;
        this.logger.warn(`${meth} - ${errMsg}`);
        return q.reject(new Error(errMsg));
      }
      this.logger.info(`${meth} - Searching for deployment: ${name}`);
      return this.planner.getDeployment(params);
    }

    getDeploymentFromRef(ref, context) {
      var filter, meth, ref1;
      meth = `Admission.getDeploymentFromRef(${JSON.stringify(ref)})`;
      this.logger.info(`${meth} - User: ${(ref1 = context.user) != null ? ref1.id : void 0}`);
      filter = {
        ref: ref
      };
      return this.getDeployment(filter, context);
    }

    getDeploymentFromURN(urn, context) {
      var filter, meth, ref1;
      meth = `Admission.getDeploymentFromURN(${urn})`;
      this.logger.info(`${meth} - User: ${(ref1 = context.user) != null ? ref1.id : void 0}`);
      filter = {
        urn: urn
      };
      return this.getDeployment(filter, context);
    }

    // Expects options object to contain the following properties:
    // - deploymentURN
    // - deploymentName (internal ID)
    // - deploymentRole
    // - instanceName
    restartInstances(context, options) {
      var meth;
      meth = `Admission.restartInstances ${JSON.stringify(options)}`;
      this.logger.info(meth);
      // Restart the instance
      return (options.instanceName != null ? (this.logger.debug(`${meth} - Instance name: ${// Restart the role
      options.instanceName}`), this.planner.restartInstance(options.instanceName)) : (this.logger.debug(`${meth} - Deployment: ${options.deploymentName} Role: ${options.deploymentRole}`), this.planner.restartRole(options.deploymentName, options.deploymentRole))).then(() => {
        this.logger.info(`${meth} - Successfully restarted instances.`);
        return true;
      });
    }

    solutionElementToDeploymentElement(solURN, solRole, solInstance, context) {
      var deplName, deplRole, deplURN, errMsg, instanceName, meth, segments, solRef, topDeplRef, topDeplURN;
      meth = 'Admission.solutionElementToDeploymentElement';
      this.logger.debug(`${meth} - Converting element: Solution URN: ${solURN} - Role: ${solRole} - Instance: solInstance`);
      // The info we need to determine
      deplURN = null;
      deplName = null; // Deployment internal ID
      deplRole = null;
      instanceName = null; // The name of the Pod representing the instance

      // Determine the solution top deployment URN (same as the solution except for
      // the type)
      solRef = this.manifestHelper.urnToRef(solURN);
      topDeplRef = {
        domain: solRef.domain,
        kind: 'deployment',
        name: solRef.name
      };
      topDeplURN = this.manifestHelper.refToUrn(topDeplRef);
      // Determine the URN of the deployment the instance belongs to

      // Find out the subdeployment corresponding to the role
      // - if role name is compound (for example 'first.second.third'), the last
      //   portion is the actual role, and the leading portions are part of the
      //   sub-deployment name.
      // - if role name is simple (for example 'first'), it is the role 'as is'
      //   and the deployment name is the same as the solution
      segments = solRole.split('.');
      if (segments.length === 1) {
        deplRole = solRole;
        deplURN = topDeplURN;
      } else if (segments.length > 1) {
        deplRole = segments.pop(); // Get the last element and remove it
        console.log(`deplRole: ${deplRole}`);
        console.log(`segments: ${segments}`);
        deplURN = `${topDeplURN}.${segments.join('.')}`;
      } else {
        errMsg = `Invalid role name (dots): ${solRole}`;
        this.logger.error(`${meth} - ${errMsg}`);
        return q.reject(new Error(errMsg));
      }
      // Get the affected deployment manifest  (to get its internal ID/name)
      return this.getDeploymentFromURN(deplURN, context).then(manifest => {
        var hashedRoleName;
        if (manifest == null) {
          errMsg = `Deployment ${deplURN} not found.`;
          this.logger.warn(`${meth} - ${errMsg}`);
          return q.reject(new Error(errMsg));
        }
        this.logger.debug(`${meth} - Deployment ${deplURN} exists`);
        deplName = manifest.name;
        if (solInstance != null) {
          // Determine the instance Pod name
          hashedRoleName = this.manifestHelper.hashLabel(deplRole);
          instanceName = `${manifest.name}-${hashedRoleName}-deployment-` + `${solInstance}`;
          this.logger.debug(`${meth} - Instance name: ${instanceName}`);
        } else {
          instanceName = null;
        }
        return true;
      }).then(() => {
        var result;
        result = {
          deploymentURN: deplURN,
          deploymentName: deplName,
          deploymentRole: deplRole,
          instanceName: instanceName
        };
        this.logger.debug(`${meth} - Result: ${JSON.stringify(result)}`);
        return result;
      });
    }

    //###########################
    //#  RESOURCES MANAGEMENT  ##
    //###########################
    registerResource(context, resourceManifest) {
      var currentTimestamp, errMsg, meth, opID, refStr, resourceURN;
      meth = 'Admission.registerResource()';
      this.logger.info(`${meth} Resource Ref: ${JSON.stringify(resourceManifest.ref)}`);
      currentTimestamp = utils.getTimestamp();
      // Check that the resource has a ref and a valid ref.domain, or add it
      if (resourceManifest.ref != null) {
        if (resourceManifest.ref.domain != null) {
          // ref.domain must match the id of the user performing the action,
          // except for Admin users
          if (resourceManifest.ref.domain !== context.user.id) {
            if (this.authorization.isAdmin(context)) {
              this.logger.info(`${meth} - Admin user allowed to use a custom domain.`);
            } else {
              errMsg = `Resource domain doesn't match username: (${resourceManifest.ref.domain} != ${context.user.id}`;
              this.logger.error(`${meth} - ERROR: ${errMsg}`);
              return q.reject(new Error(errMsg));
            }
          }
        } else {
          this.logger.warn(`${meth} - Resource has no domain, adding it.`);
          resourceManifest.ref.domain = context.user.id;
        }
      } else {
        errMsg = "Reseource is missing mandatory property 'ref'.";
        this.logger.error(`${meth} - ERROR: ${errMsg}`);
        return q.reject(new Error(errMsg));
      }
      // Check if the manifest includes an 'owner' property and if it matches the
      // ref.domain
      if (resourceManifest.owner != null) {
        if (resourceManifest.owner === '') {
          // Invalid owner received, delete it
          delete resourceManifest.owner;
        } else if (resourceManifest.owner !== resourceManifest.ref.domain) {
          // 'owner' property must match the ref.domain, except for Admin users
          if (this.authorization.isAdmin(context)) {
            this.logger.info(`${meth} - Admin user allowed to provide an owner different from the ref.domain.`);
          } else {
            errMsg = `Resource 'owner' property doesn't match the resource domain: ${resourceManifest.owner} != ${resourceManifest.ref.domain}`;
            this.logger.error(`${meth} - ERROR: ${errMsg}`);
            return q.reject(new Error(errMsg));
          }
        }
      }
      refStr = JSON.stringify(resourceManifest.ref);
      resourceURN = this.manifestHelper.refToUrn(resourceManifest.ref);
      this.logger.info(`${meth} - Checking existence of resource: ${resourceURN} ...`);
      // Try to acquire lock for the operation
      // Calculate a unique ID for this operation
      opID = this.getOperationID();
      // Acquire lock on the resource
      return this.acquireElementLockFromURN(resourceURN, opID).then(() => {
        // Lock acquired, proceed
        // Check if resource already exists
        return this.manifestRepository.getManifestByURN(resourceURN);
      }).catch(err => {
        var ref1;
        if ((ref1 = err.message) != null ? ref1.includes('Element not found') : void 0) {
          return null;
        } else {
          return q.reject(err);
        }
      }).then(previousManifest => {
        var ref1, ref2, ref3;
        if (previousManifest != null) {
          // Resource already exists, check its ownership
          this.logger.info(`${meth} - Resource ${refStr} already exists`);
          if (!this.authorization.isAllowedForManifest(previousManifest, context, true)) {
            errMsg = "User is not allowed to modify resource.";
            this.logger.warn(`${meth} - ${errMsg}`);
            return q.reject(new Error(errMsg));
          } else if (ref1 = previousManifest.ref.kind, indexOf.call(this.manifestHelper.getUpdatableResourceKinds(), ref1) < 0) {
            errMsg = `Update operation is not supported for type ${previousManifest.ref.kind}.`;
            this.logger.warn(`${meth} - ${errMsg}`);
            return q.reject(new Error(errMsg));
          } else {
            // Complete deployment information
            if (resourceManifest.owner == null) {
              resourceManifest.owner = previousManifest.owner || ((ref2 = context.user) != null ? ref2.id : void 0);
            }
            resourceManifest.name = previousManifest.name;
            resourceManifest.urn = previousManifest.urn;
            resourceManifest.creationTimestamp = previousManifest.creationTimestamp || currentTimestamp;
            resourceManifest.lastModification = currentTimestamp;
            return resourceManifest;
          }
        } else {
          // Resource is new, handle as a creation
          this.logger.info(`${meth} - Resource ${refStr} is new`);
          if (resourceManifest.owner == null) {
            resourceManifest.owner = (ref3 = context.user) != null ? ref3.id : void 0;
          }
          resourceManifest.name = this.generateResourceName();
          resourceManifest.urn = resourceURN;
          resourceManifest.creationTimestamp = currentTimestamp;
          resourceManifest.lastModification = currentTimestamp;
          return resourceManifest;
        }
      }).then(resManifest => {
        return this.validateResource(context, resManifest);
      }).then(resManifest => {
        return this.manifestRepository.storeManifest(resManifest);
      }).then(resName => {
        this.logger.info(`${meth} - Resource ${refStr} stored with name ${resourceManifest.name} and URN ${resourceManifest.urn}`);
        return resourceManifest.urn;
      }).finally(() => {
        return this.releaseElementLockFromURN(resourceURN, opID);
      });
    }

    listResources(filter) {
      this.logger.info(`Admission.listResources() filter = ${JSON.stringify(filter)}`);
      return this.planner.listResources(filter);
    }

    listResourcesInUse(filter, context) {
      this.logger.info(`Admission.listResourcesInUse() filter = ${JSON.stringify(filter)}`);
      return this.planner.listResourcesInUse(filter, context);
    }

    describeResource(resURN) {
      this.logger.info(`Admission.describeResource() Resource URN: ${resURN}`);
      return this.planner.describeResource(resURN);
    }

    //#####################
    //  LINKS MANAGEMENT  #
    //#####################
    linkServices(context, linkManifest) {
      var depl1Channel, depl1ChannelType, depl1Manifest, depl1URN, depl2Channel, depl2ChannelType, depl2Manifest, depl2URN, errMsg, linkID, meth, opID, sol1Ref, sol1URN, sol2Ref, sol2URN;
      meth = `Admission.linkServices ${JSON.stringify(linkManifest.endpoints)}`;
      this.logger.info(`${meth}`);
      linkID = null;
      // Calculate a unique ID for this operation
      opID = this.getOperationID();
      // Validate that if the link manifest contains an owner property it matches
      // the current user ID (unless current user has ADMIN role)
      if (linkManifest.owner != null) {
        if (linkManifest.owner === '') {
          // Invalid owner received, delete it
          delete linkManifest.owner;
        } else if (linkManifest.owner !== context.user.id) {
          // The 'owner' property must match the user ID, except for Admin users
          if (this.authorization.isAdmin(context)) {
            this.logger.info(`${meth} - Admin user allowed to provide an owner other than himself.`);
          } else {
            errMsg = `Link manifest 'owner' property doesn't match the current user id: ${linkManifest.owner} != ${context.user.id}`;
            this.logger.warn(`${meth} - ${errMsg}`);
            return q.reject(new Error(errMsg));
          }
        }
      }
      // Links manifests contain references to Solutions, but links are performed
      // internally on Deployments.

      // Determine top deployments of each linked solution
      sol1Ref = {
        kind: linkManifest.endpoints[0].kind,
        domain: linkManifest.endpoints[0].domain,
        name: linkManifest.endpoints[0].name
      };
      sol1URN = this.manifestHelper.refToUrn(sol1Ref);
      sol2Ref = {
        kind: linkManifest.endpoints[1].kind,
        domain: linkManifest.endpoints[1].domain,
        name: linkManifest.endpoints[1].name
      };
      sol2URN = this.manifestHelper.refToUrn(sol2Ref);
      // Validate that the provided endpoints are solutions
      if (!this.manifestHelper.isSolutionURN(sol1URN)) {
        errMsg = `Invalid endpoint ${sol1URN}.`;
        this.logger.warn(`${meth} - ${errMsg}`);
        return q.reject(new Error(errMsg));
      }
      if (!this.manifestHelper.isSolutionURN(sol2URN)) {
        errMsg = `Invalid endpoint ${sol2URN}.`;
        this.logger.warn(`${meth} - ${errMsg}`);
        return q.reject(new Error(errMsg));
      }
      // Top deployment of solution 1
      depl1URN = null;
      depl1Manifest = null;
      depl1ChannelType = null;
      depl1Channel = linkManifest.endpoints[0].channel;
      // Top deployment of solution 2
      depl2URN = null;
      depl2Manifest = null;
      depl2ChannelType = null;
      depl2Channel = linkManifest.endpoints[1].channel;
      // Perform validations on the first endpoint
      return this.getSolutionFromURN(sol1URN, context).then(manifest => {
        var depl1Ref, sol1Manifest;
        if (manifest == null) {
          errMsg = `Solution ${sol1URN} not found.`;
          this.logger.warn(`${meth} - ${errMsg}`);
          return q.reject(new Error(errMsg));
        }
        // Solution exists, check its ownership
        sol1Manifest = manifest;
        this.logger.debug(`${meth} - Solution ${sol1URN} exists`);
        if (!this.authorization.isAllowedForManifest(manifest, context, true)) {
          errMsg = `User is not allowed to link solution ${sol1URN}.`;
          this.logger.warn(`${meth} - ${errMsg}`);
          return q.reject(new Error(errMsg));
        }
        // Check if the solution is being deleted
        if (sol1Manifest.deletionTimestamp != null) {
          errMsg = `Solution is being deleted (${sol1URN}).`;
          this.logger.warn(`${meth} - ${errMsg}`);
          return q.reject(new Error(errMsg));
        }
        // Determine and get the Solution top deployment
        depl1Ref = {
          kind: 'deployment',
          domain: sol1Manifest.ref.domain,
          name: sol1Manifest.top
        };
        depl1URN = this.manifestHelper.refToUrn(depl1Ref);
        return this.getDeploymentFromURN(depl1URN, context);
      }).then(manifest => {
        var clientChannels, duplexChannels, ref1, ref2, ref3, ref4, ref5, ref6, ref7, ref8, ref9, serverChannels;
        if (manifest == null) {
          errMsg = `Deployment ${depl1URN} not found.`;
          this.logger.warn(`${meth} - ${errMsg}`);
          return q.reject(new Error(errMsg));
        }
        this.logger.debug(`${meth} - Deployment ${depl1URN} found.`);
        depl1Manifest = manifest;
        // Deployment channel is mandatory and must be one of the deployment
        // channels
        serverChannels = ((ref1 = depl1Manifest.artifact) != null ? (ref2 = ref1.description) != null ? (ref3 = ref2.srv) != null ? ref3.server : void 0 : void 0 : void 0) || {};
        clientChannels = ((ref4 = depl1Manifest.artifact) != null ? (ref5 = ref4.description) != null ? (ref6 = ref5.srv) != null ? ref6.client : void 0 : void 0 : void 0) || {};
        duplexChannels = ((ref7 = depl1Manifest.artifact) != null ? (ref8 = ref7.description) != null ? (ref9 = ref8.srv) != null ? ref9.duplex : void 0 : void 0 : void 0) || {};
        if (depl1Channel == null || !(depl1Channel in serverChannels) && !(depl1Channel in clientChannels) && !(depl1Channel in duplexChannels)) {
          errMsg = `Invalid channel '${depl1Channel}' for solution ${sol1URN}.`;
          this.logger.warn(`${meth} - ${errMsg}`);
          return q.reject(new Error(errMsg));
        }
        // Determine the type of the first endpoint channel (client/server/duplex)
        if (depl1Channel in serverChannels) {
          depl1ChannelType = "server";
        } else if (depl1Channel in clientChannels) {
          depl1ChannelType = "client";
        } else {
          depl1ChannelType = "duplex";
        }
        return true;
      }).then(() => {
        // Perform validations on the second endpoint
        return this.getSolutionFromURN(sol2URN, context);
      }).then(manifest => {
        var depl2Ref, sol2Manifest;
        if (manifest == null) {
          errMsg = `Solution ${sol2URN} not found.`;
          this.logger.warn(`${meth} - ${errMsg}`);
          return q.reject(new Error(errMsg));
        }
        // Solution exists, check its ownership
        sol2Manifest = manifest;
        this.logger.debug(`${meth} - Solution ${sol2URN} exists`);
        if (!this.authorization.isAllowedForManifest(manifest, context, true)) {
          errMsg = `User is not allowed to link solution ${sol2URN}.`;
          this.logger.warn(`${meth} - ${errMsg}`);
          return q.reject(new Error(errMsg));
        }
        // Check if the solution is being deleted
        if (sol2Manifest.deletionTimestamp != null) {
          errMsg = `Solution is being deleted (${sol2URN}).`;
          this.logger.warn(`${meth} - ${errMsg}`);
          return q.reject(new Error(errMsg));
        }
        // Determine and get the Solution top deployment
        depl2Ref = {
          kind: 'deployment',
          domain: sol2Manifest.ref.domain,
          name: sol2Manifest.top
        };
        depl2URN = this.manifestHelper.refToUrn(depl2Ref);
        return this.getDeploymentFromURN(depl2URN, context);
      }).then(manifest => {
        var clientChannels, duplexChannels, ref1, ref2, ref3, ref4, ref5, ref6, ref7, ref8, ref9, serverChannels;
        if (manifest == null) {
          errMsg = `Deployment ${depl2URN} not found.`;
          this.logger.warn(`${meth} - ${errMsg}`);
          return q.reject(new Error(errMsg));
        }
        this.logger.debug(`${meth} - Deployment ${depl2URN} found.`);
        depl2Manifest = manifest;
        // Deployment channel is mandatory and must be one of the deployment
        // channels
        serverChannels = ((ref1 = depl2Manifest.artifact) != null ? (ref2 = ref1.description) != null ? (ref3 = ref2.srv) != null ? ref3.server : void 0 : void 0 : void 0) || {};
        clientChannels = ((ref4 = depl2Manifest.artifact) != null ? (ref5 = ref4.description) != null ? (ref6 = ref5.srv) != null ? ref6.client : void 0 : void 0 : void 0) || {};
        duplexChannels = ((ref7 = depl2Manifest.artifact) != null ? (ref8 = ref7.description) != null ? (ref9 = ref8.srv) != null ? ref9.duplex : void 0 : void 0 : void 0) || {};
        if (depl2Channel == null || !(depl2Channel in serverChannels) && !(depl2Channel in clientChannels) && !(depl2Channel in duplexChannels)) {
          errMsg = `Invalid channel '${depl2Channel}' for solution ${sol2URN}.`;
          this.logger.warn(`${meth} - ${errMsg}`);
          return q.reject(new Error(errMsg));
        }
        // Determine the type of the first endpoint channel (client/server/duplex)
        if (depl2Channel in serverChannels) {
          depl2ChannelType = "server";
        } else if (depl2Channel in clientChannels) {
          depl2ChannelType = "client";
        } else {
          depl2ChannelType = "duplex";
        }
        return true;
      }).then(() => {
        // Perform validation on the channel type to be linked

        // Two server channels or two client channels cannot be linked.
        // Also, links between two duplex are not currently allowed.
        if (depl1ChannelType === depl2ChannelType) {
          errMsg = `Two ${depl2ChannelType} channels cannot be linked.`;
          this.logger.warn(`${meth} - ${errMsg}`);
          return q.reject(new Error(errMsg));
        }
        return true;
      }).then(() => {
        // All checks passed, try to acquire lock for the link operation

        // Calculate a link ID
        linkID = this.generateLinkName(linkManifest);
        // Acquire lock on the link
        // ID is not a URN, but link don't have URNs
        return this.acquireElementLockFromURN(linkID, opID);
      }).then(() => {
        var ref1;
        // Lock acquired, proceed with the link operation

        // Add 'owner' property to the link manifest, if it doesn't have one
        if (linkManifest.owner == null) {
          linkManifest.owner = (ref1 = context.user) != null ? ref1.id : void 0;
        }
        linkManifest.endpoints[0].kumoriName = depl1Manifest.name;
        linkManifest.endpoints[1].kumoriName = depl2Manifest.name;
        // Add a new 'name' property to the link manifest to be able to delete it
        // later.
        if (linkManifest.name == null) {
          linkManifest.name = linkID;
          this.logger.debug(`${meth} - Assigned new link name: ${linkManifest.name}`);
        }
        return this.planner.linkServices(linkManifest);
      }).finally(() => {
        // ID is not a URN, but link don't have URNs
        return this.releaseElementLockFromURN(linkID, opID);
      });
    }

    unlinkServices(context, linkManifest) {
      var linkID, meth, opID, prevManifest;
      meth = `Admission.unlinkServices ${JSON.stringify(linkManifest.endpoints)}`;
      this.logger.info(`${meth}`);
      // Calculate a link ID
      linkID = this.generateLinkName(linkManifest);
      // Calculate a unique ID for this operation
      opID = this.getOperationID();
      if (linkManifest.name == null) {
        linkManifest.name = linkID;
        this.logger.debug(`${meth} - Calculated link name: ${linkManifest.name}`);
      }
      prevManifest = null;
      // Search link manifest
      this.logger.debug(`${meth} - Getting existing link manifest`);
      return this.manifestRepository.getManifestByNameAndKind(linkManifest.name, 'link').then(previousManifest => {
        var errMsg;
        prevManifest = previousManifest;
        this.logger.debug(`${meth} - Link manifest found.`);
        // Check if user is allowed to delete link (requires write permission)
        if (!this.authorization.isAllowedForManifest(prevManifest, context, true)) {
          errMsg = `User not authorized (user:${context.user.id} - owner: ${prevManifest.owner})`;
          this.logger.error(`${meth} - ERROR: ${errMsg}`);
          return q.reject(new Error(errMsg));
        } else {
          return this.logger.debug(`${meth} - User is allowed to delete manifest.`);
        }
      }).then(() => {
        // Try to acquire lock for the unlink operation

        // Acquire lock on the link
        // ID is not a URN, but link don't have URNs
        return this.acquireElementLockFromURN(linkID, opID);
      }).then(() => {
        var errMsg;
        // Lock acquired, proceed with the link operation

        // Check element is a Link (a little tricky since there is no 'kind')
        if (prevManifest.name.startsWith('kl-') && 'endpoints' in prevManifest) {
          return this.planner.unlinkServices(linkManifest.name);
        } else {
          errMsg = 'Invalid element type (not a Link)';
          this.logger.error(`${meth} - ERROR: ${errMsg}`);
          return q.reject(new Error(errMsg));
        }
      }).catch(err => {
        var errMsg;
        errMsg = "Unable to find the provided link.";
        this.logger.warn(`${meth} - ERROR: ${errMsg}`);
        throw new Error(errMsg);
      }).finally(() => {
        // ID is not a URN, but link don't have URNs
        return this.releaseElementLockFromURN(linkID, opID);
      });
    }

    unlinkServicesFromURN(context, linkURN) {
      var errMsg, meth;
      meth = `Admission.unlinkServicesFromURN ${linkURN}`;
      this.logger.info(`${meth}`);
      // Currently (after ticket 215) link are not Manifests anymore but operations
      // and as such don't have `ref` identifiers or internal URNs. The only
      // possible way to refer to an existing link is by the whole manifest or by
      // the Kumori internal ID, which is not available to users.

      // For helping Admin users during development, we will temporarily allow
      // passing the internal ID as a URN. For normal users, this method won't be
      // allowed.
      if (this.authorization.isAdmin(context)) {
        return this.unlinkServicesFromName(context, linkURN);
      } else {
        // Reject the operation
        errMsg = "Operation not allowed (links don't have URNs).";
        this.logger.error(`${meth} - ERROR: ${errMsg}`);
        return q.reject(new Error(errMsg));
      }
    }

    unlinkServicesFromName(context, linkName) {
      var errMsg, meth, opID;
      meth = `Admission.unlinkServicesFromName ${linkName}`;
      this.logger.info(`${meth}`);
      // For helping Admin users during development, we will temporarily allow
      // passing the internal ID as a URN. For normal users, this method won't be
      // allowed.
      if (!this.authorization.isAdmin(context)) {
        errMsg = `Operation not allowed for user ${context.user.id}`;
        this.logger.error(`${meth} - ERROR: ${errMsg}`);
        return q.reject(new Error(errMsg));
      }
      // Try to acquire lock for the unlink operation

      // Calculate a unique ID for this operation
      opID = this.getOperationID();
      // Acquire lock on the link
      // ID is not a URN, but link don't have URNs
      return this.acquireElementLockFromURN(linkName, opID).then(() => {
        // Lock acquired, proceed with the link operation
        return this.manifestRepository.getManifestByNameAndKind(linkName, 'link');
      }).then(linkManifest => {
        // Check element is a Link (a little tricky since there is no 'kind')
        if (linkManifest.name.startsWith('kl-') && 'endpoints' in linkManifest) {
          return this.planner.unlinkServices(linkManifest.name);
        } else {
          errMsg = 'Invalid element type (not a Link)';
          this.logger.error(`${meth} - ERROR: ${errMsg}`);
          return q.reject(new Error(errMsg));
        }
      }).finally(() => {
        // ID is not a URN, but link don't have URNs
        return this.releaseElementLockFromURN(linkName, opID);
      });
    }

    //####################
    //   HELPER METHODS  #
    //####################
    setClusterConfiguration(newClusterConfiguration) {
      this.logger.info('Admission.setClusterConfiguration()');
      return this.config.clusterConfiguration = newClusterConfiguration;
    }

    // console.log "CLUSTER CONFIGURATION:"
    // console.log "#{JSON.stringify @config.clusterConfiguration, null, 2}"
    validateV3Deployment(manifest, context) {
      var meth;
      meth = 'Admission.validateV3Deployment()';
      this.logger.info(meth);
      // Validate that all requested resources are OK
      return this.validateV3DeploymentResources(manifest, context).then(deploymentManifest => {
        return this.validateV3DeploymentImages(manifest, context);
      }).then(deploymentManifest => {
        this.logger.info(`${meth} All validations finished.`);
        return deploymentManifest;
      });
    }

    validateV3DeploymentImages(manifest) {
      var contData, contName, errors, imageData, meth, promiseChain, ref1, ref2, ref3, ref4, ref5, ref6, ref7, ref8, roleContainers, roleData, roleName, roleResources, serviceRoles;
      meth = 'Admission.validateV3DeploymentImages()';
      this.logger.info(meth);
      this.logger.info(`${meth} Validating used component docker images`);
      serviceRoles = ((ref1 = manifest.description) != null ? (ref2 = ref1.service) != null ? (ref3 = ref2.description) != null ? ref3.role : void 0 : void 0 : void 0) || {};
      promiseChain = q();
      errors = [];
      for (roleName in serviceRoles) {
        roleData = serviceRoles[roleName];
        roleContainers = ((ref4 = roleData.component) != null ? (ref5 = ref4.description) != null ? ref5.code : void 0 : void 0) || {};
        roleResources = ((ref6 = roleData.artifact) != null ? (ref7 = ref6.description) != null ? (ref8 = ref7.config) != null ? ref8.resource : void 0 : void 0 : void 0) || {};
        for (contName in roleContainers) {
          contData = roleContainers[contName];
          imageData = contData.image;
          ((roleName, contName, imageData, roleResources) => {
            var dockerImage, errMsg, normErr;
            this.logger.info(`${meth} Validating role ${roleName} - container ${contName}`);
            this.logger.info(`${meth} Docker image: ${JSON.stringify(imageData)}`);
            try {
              dockerImage = this.dockerRegistryProxy.normalizeDockerImage(imageData);
              this.logger.info(`${meth} Normalized image: ${JSON.stringify(dockerImage)}`);
              return promiseChain = promiseChain.then(() => {
                return this.validateComponentImage(dockerImage, roleResources, context).then(() => {
                  this.logger.info(`${meth} - Validated role ${roleName} - container ${contName}`);
                  return true;
                }).catch(error => {
                  var errMsg;
                  errMsg = `Failed docker image validation for role '${roleName}' container '${contName}' image '${dockerImage.hub}/${dockerImage.name}:${dockerImage.ref}' ERROR: ${error.message}`;
                  errors.push(errMsg);
                  return true;
                });
              });
            } catch (error1) {
              normErr = error1;
              errMsg = `Failed docker image validation for role '${roleName}' container '${contName}' tag '${imageData.tag}' ERROR: ${normErr.message}`;
              return errors.push(errMsg);
            }
          })(roleName, contName, imageData, roleResources);
        }
      }
      return promiseChain.then(() => {
        var errMsg;
        this.logger.info(`${meth} All component images validations finished.`);
        if (errors.length > 0) {
          errMsg = `The following component docker images could not be validated: ${JSON.stringify(errors)}`;
          this.logger.error(`${meth} ${errMsg}`);
          return q.reject(new Error(errMsg));
        } else {
          this.logger.info(`${meth} All component images validated!`);
          return manifest;
        }
      });
    }

    // Resolves an internal resource name (a key in config.resources) to the actual
    // resource object
    resolveInternalResource(internalResourceName, roleResources) {
      var meth;
      meth = `Admission.resolveInternalResource(${internalResourceName})`;
      this.logger.info(`${meth}`);
      if (internalResourceName in roleResources) {
        this.logger.info(`${meth} Resolved to: ${JSON.stringify(roleResources[internalResourceName])}`);
        return roleResources[internalResourceName];
      } else {
        return null;
      }
    }

    // Expects imageData to contain the following keys:
    //  - hub
    //  - secret
    //  - name
    //  - ref
    validateComponentImage(imageData, roleResources, context) {
      var errMsg, meth, secretResource, secretResourceObj;
      meth = 'Admission.validateComponentImage()';
      this.logger.info(`${meth} Image: ${JSON.stringify(imageData)}`);
      if (imageData.secret != null && imageData.secret !== '') {
        // Resolve the internal resource name
        secretResourceObj = this.resolveInternalResource(imageData.secret, roleResources);
        if (secretResourceObj == null) {
          errMsg = `Internal resource reference '${imageData.secret}' can not be resolved against role configuration.`;
          this.logger.info(`${meth} - ${errMsg}`);
          return q.reject(new Error(errMsg));
        }
        // Normalize the secret resource reference
        secretResource = this.normalizeResourceReference(secretResourceObj.secret, context);
        // Fetch credential from ApiServer secret object
        this.logger.info(`${meth} Fetching Secret: ${secretResource}`);
        return this.planner.getDockerCredentialsFromSecret(secretResource).then(credentials => {
          this.logger.info(`${meth} Got credentials!`);
          this.logger.info(`${meth} Checking image existence...`);
          return this.dockerRegistryProxy.checkImage(imageData.hub, imageData.name, imageData.ref, credentials.username, credentials.password);
        }).then(exists => {
          if (exists) {
            this.logger.info(`${meth} - Image validated!`);
            return true;
          } else {
            this.logger.info(`${meth} - Image could not be found.`);
            return q.reject(new Error("Image could not be validated."));
          }
        });
      } else {
        // No need to retrieve credentials, image is public
        this.logger.info(`${meth} Checking image existence...`);
        return this.dockerRegistryProxy.checkImage(imageData.hub, imageData.name, imageData.ref).then(exists => {
          if (exists) {
            this.logger.info(`${meth} - Image validated!`);
            return true;
          } else {
            this.logger.info(`${meth} - Image could not be found.`);
            return q.reject(new Error("Image could not be validated."));
          }
        });
      }
    }

    // Validates that all the resource used in the solution exist, user has
    // permissions and are available.
    // NOTE: for exclusive use resources, it also checks if the resource is used
    //       multiple times inside the solution.
    validateSolutionResources(solutionManifest, context) {
      var deplName, deplResourceList, errors, exclusiveResourceTypes, i, len, meth, promiseQueue, resURN, solResourcesByDeployment, usedResources;
      meth = 'Admission.validateSolutionResources()';
      this.logger.info(`${meth} MANIFEST: ${JSON.stringify(solutionManifest.ref)}`);
      // Get list of exclusive resource types
      exclusiveResourceTypes = this.manifestHelper.getExclusiveResourceKinds();
      // Calculate a list of referenced resource URNs by deployment
      solResourcesByDeployment = this.getSolutionResourceList(solutionManifest, context);
      promiseQueue = q();
      errors = [];
      usedResources = {};
      for (deplName in solResourcesByDeployment) {
        deplResourceList = solResourcesByDeployment[deplName];
        for (i = 0, len = deplResourceList.length; i < len; i++) {
          resURN = deplResourceList[i];
          ((deplName, resURN) => {
            var errMsg, resID, resRef, resType;
            resID = null;
            resType = null;
            // Basic resource type check; only accept known resource types
            resRef = this.manifestHelper.urnToRef(resURN);
            if (!this.manifestHelper.isResourceKind(resRef.kind)) {
              errMsg = `Resource ${resURN} has an invalid type: ${resRef.kind}.`;
              this.logger.error(`${meth} ERROR: ${errMsg}`);
              errors.push(errMsg);
              return;
            }
            promiseQueue = promiseQueue.then(() => {
              // First, check access to the resource.
              return this.checkResourceAccess(resURN, context).then(checkResult => {
                if (checkResult.error != null) {
                  // Resource doesn't exists and user can't access it
                  return errors.push(checkResult.error);
                } else {
                  // Resource exists and user can access it
                  resID = checkResult.resID;
                  resType = checkResult.resType;
                  // Check If resource is of exclusive use and is used more than
                  // once in the solution
                  if (resID in usedResources && indexOf.call(exclusiveResourceTypes, resType) >= 0) {
                    errMsg = `Resource ${resURN} is used more than once.`;
                    this.logger.error(`${meth} ERROR: ${errMsg}`);
                    return errors.push(errMsg);
                  } else {
                    // Add resource to resource list for checking double use in the
                    // solution.
                    // Add resource to the final resource list
                    return usedResources[resID] = {
                      urn: resURN,
                      type: resType
                    };
                  }
                }
              }).catch(err => {
                errMsg = `Unexpected error while validating resource ${resURN}: ${err.message}`;
                this.logger.error(`${meth} ERROR: ${errMsg}`);
                errors.push(errMsg);
                return true;
              });
            });
            return promiseQueue = promiseQueue.then(() => {
              // Next, if exclusive resources are currently in use.
              if (indexOf.call(exclusiveResourceTypes, resType) >= 0) {
                this.logger.info(`${meth} Validate availability of URN: ${resURN}`);
                return this.planner.isResourceInUseBySolution(resID).then(inUseInfo => {
                  if (inUseInfo.inUse) {
                    if (inUseInfo.usedByURN === this.manifestHelper.refToUrn(solutionManifest.ref)) {
                      this.logger.info(`${meth} Resource in use by the same solution.`);
                    } else {
                      errMsg = `Resource ${resURN} is already in use.`;
                      this.logger.error(`${meth} ERROR: ${errMsg}`);
                      errors.push(errMsg);
                    }
                  }
                  return true;
                });
              }
            });
          })(deplName, resURN);
        }
      }
      return promiseQueue.then(() => {
        var errMsg;
        if (errors.length === 0) {
          this.logger.info(`${meth} All resources OK.`);
          // Add usedResources property to manifest
          solutionManifest.usedResources = usedResources;
          return solutionManifest;
        } else {
          errMsg = `Resources validation found some problems: ${JSON.stringify(errors)}`;
          this.logger.error(`${meth} ${errMsg}`);
          return q.reject(new Error(errMsg));
        }
      });
    }

    // Validates several aspects of the solution deployments
    // - names are unique (no other deployments exist with the same domain/name)
    // - docker images are accessible
    validateSolutionDeployments(solutionManifest, context) {
      var deplData, deplName, imageErrors, meth, nameErrors, promiseChain, ref1;
      meth = `Admission.validateSolutionDeployments() - Solution: ${JSON.stringify(solutionManifest.ref)}`;
      this.logger.info(meth);
      promiseChain = q();
      nameErrors = [];
      imageErrors = [];
      ref1 = solutionManifest.deployments || {};
      // Loop over the solution deployments and:
      // - check their names
      // - check the docker images they use
      for (deplName in ref1) {
        deplData = ref1[deplName];
        ((deplName, deplData) => {
          var contData, contName, deplRoles, imageData, ref2, ref3, ref4, ref5, ref6, ref7, ref8, results, roleContainers, roleData, roleName, roleResources;
          promiseChain = promiseChain.then(() => {
            var deplRef;
            // Compose a Ref for the deployment
            deplRef = {
              kind: 'v3deployment',
              domain: solutionManifest.ref.domain,
              name: deplName
            };
            // Check if the deployment exists and user has permissions on it
            this.logger.debug(`${meth} - Checking deployment name: ${deplName}`);
            return this.checkDeploymentNameAvailability(deplRef, context).then(() => {
              this.logger.debug(`${meth} - Deployment name OK: ${deplName}`);
              return true;
            }).catch(err => {
              var errMsg;
              errMsg = `Deployment name ${deplName} is already in use.`;
              this.logger.warn(`${meth} - ${errMsg} - ERROR: ERROR: ${err.message}`);
              nameErrors.push(errMsg);
              return true;
            });
          });
          deplRoles = ((ref2 = deplData.artifact) != null ? (ref3 = ref2.description) != null ? ref3.role : void 0 : void 0) || {};
          results = [];
          for (roleName in deplRoles) {
            roleData = deplRoles[roleName];
            roleContainers = ((ref4 = roleData.artifact) != null ? (ref5 = ref4.description) != null ? ref5.code : void 0 : void 0) || {};
            roleResources = ((ref6 = roleData.artifact) != null ? (ref7 = ref6.description) != null ? (ref8 = ref7.config) != null ? ref8.resource : void 0 : void 0 : void 0) || {};
            results.push(function () {
              var results1;
              results1 = [];
              for (contName in roleContainers) {
                contData = roleContainers[contName];
                imageData = contData.image;
                results1.push(((roleName, contName, imageData, roleResources) => {
                  var dockerImage, errMsg, normErr;
                  this.logger.info(`${meth} Check ${deplName}/${roleName}/${contName}`);
                  this.logger.info(`${meth} Docker image: ${JSON.stringify(imageData)}`);
                  try {
                    dockerImage = this.dockerRegistryProxy.normalizeDockerImage(imageData);
                    this.logger.info(`${meth} Normalized: ${JSON.stringify(dockerImage)}`);
                    return promiseChain = promiseChain.then(() => {
                      return this.validateComponentImage(dockerImage, roleResources, context).then(() => {
                        this.logger.info(`${meth} Validation OK for ${deplName}/${roleName}/${contName}`);
                        return true;
                      }).catch(err => {
                        var errMsg;
                        errMsg = `Failed docker image validation for deployment/role '${deplName}/${roleName}' container '${contName}' image '${dockerImage.hub}/${dockerImage.name}:${dockerImage.ref}' ERROR: ${err.message}`;
                        imageErrors.push(errMsg);
                        return true;
                      });
                    });
                  } catch (error1) {
                    normErr = error1;
                    errMsg = `Failed docker image validation for role '${roleName}' container '${contName}' tag '${imageData.tag}' ERROR: ${normErr.message}`;
                    return imageErrors.push(errMsg);
                  }
                })(roleName, contName, imageData, roleResources));
              }
              return results1;
            }.call(this));
          }
          return results;
        })(deplName, deplData);
      }
      return promiseChain.then(() => {
        var errMsg;
        this.logger.info(`${meth} All names and docker image validations finished.`);
        if (nameErrors.length > 0 || imageErrors.length > 0) {
          errMsg = "The following errors were found:";
          if (nameErrors.length > 0) {
            errMsg += `${JSON.stringify(nameErrors)}`;
          }
          if (imageErrors.length > 0) {
            errMsg += `${JSON.stringify(imageErrors)}`;
          }
          this.logger.error(`${meth} ${errMsg}`);
          return q.reject(new Error(errMsg));
        } else {
          this.logger.info(`${meth} All deployment names and images validated OK!`);
          return solutionManifest;
        }
      });
    }

    // Validates that the links in the solution are correct.
    // - deployment names and channels exist in the solution
    // - channel types match
    //   - s_c must be a client or duplex
    //   - t_c must be a server or duplex
    //   - both s_c and t_c can't be duplex
    validateSolutionLinks(solutionManifest, context) {
      var depl1ClientChannels, depl1Data, depl1DuplexChannels, depl2Data, depl2DuplexChannels, depl2ServerChannels, errMsg, i, len, linkCounter, linkData, linkErrors, meth, ref1, ref10, ref11, ref12, ref13, ref2, ref3, ref4, ref5, ref6, ref7, ref8, ref9;
      meth = `Admission.validateSolutionLinks() - Solution: ${JSON.stringify(solutionManifest.ref)}`;
      this.logger.info(meth);
      linkErrors = [];
      // Loop over the solution links and:
      // - check the deployment names exist in the solution
      // - check the channel names exist in the deployment
      // - check the channel names are of the correct type
      linkCounter = 0;
      ref1 = solutionManifest.links || [];
      for (i = 0, len = ref1.length; i < len; i++) {
        linkData = ref1[i];
        // Validate link deployments exist in the solution
        if (!(linkData['s_d'] in solutionManifest.deployments)) {
          errMsg = `Link ${linkCounter} source deployment not found in solution: ${linkData['s_d']}`;
          linkErrors.push(errMsg);
        }
        if (!(linkData['t_d'] in solutionManifest.deployments)) {
          errMsg = `Link ${linkCounter} target deployment not found in solution: ${linkData['t_d']}`;
          linkErrors.push(errMsg);
        }
        // If both deployments exist, validate channel types
        if (linkErrors.length === 0) {
          depl1Data = solutionManifest.deployments[linkData['s_d']];
          depl1ClientChannels = ((ref2 = depl1Data.artifact) != null ? (ref3 = ref2.description) != null ? (ref4 = ref3.srv) != null ? ref4.client : void 0 : void 0 : void 0) || {};
          depl1DuplexChannels = ((ref5 = depl1Data.artifact) != null ? (ref6 = ref5.description) != null ? (ref7 = ref6.srv) != null ? ref7.duplex : void 0 : void 0 : void 0) || {};
          // Source channel must be client or duplex
          if (!(linkData['s_c'] in depl1ClientChannels) && !(linkData['s_c'] in depl1DuplexChannels)) {
            errMsg = `Link ${linkCounter} source channel not found in deployment ${linkData['s_d']} client or duplex channels: ${linkData['s_c']}`;
            linkErrors.push(errMsg);
          }
          depl2Data = solutionManifest.deployments[linkData['t_d']];
          depl2ServerChannels = ((ref8 = depl2Data.artifact) != null ? (ref9 = ref8.description) != null ? (ref10 = ref9.srv) != null ? ref10.server : void 0 : void 0 : void 0) || {};
          depl2DuplexChannels = ((ref11 = depl2Data.artifact) != null ? (ref12 = ref11.description) != null ? (ref13 = ref12.srv) != null ? ref13.duplex : void 0 : void 0 : void 0) || {};
          // Target channel must be server or duplex
          if (!(linkData['t_c'] in depl2ServerChannels) && !(linkData['s_c'] in depl2DuplexChannels)) {
            errMsg = `Link ${linkCounter} target channel not found in deployment ${linkData['t_d']} server or duplex channels: ${linkData['t_c']}`;
            linkErrors.push(errMsg);
          }
          // Both channels can't be duplex
          if (linkData['s_c'] in depl1DuplexChannels && linkData['t_c'] in depl2DuplexChannels) {
            errMsg = `Link ${linkCounter} has two duplex channels.`;
            linkErrors.push(errMsg);
          }
        }
        linkCounter++;
      }
      if (linkErrors.length === 0) {
        this.logger.info(`${meth} All solution links validated OK.`);
        return true;
      } else {
        errMsg = `The following errors were found in the solution links section: ${JSON.stringify(linkErrors)}`;
        throw new Error(errMsg);
      }
    }

    validateV3DeploymentResources(deploymentManifest, context) {
      var errors, exclusiveResourceTypes, i, len, meth, promiseQueue, resURN, resourceList;
      meth = 'Admission.validateV3DeploymentResources()';
      this.logger.info(`${meth} MANIFEST: ${JSON.stringify(deploymentManifest)}`);
      // Get list of exclusive resource types
      exclusiveResourceTypes = this.manifestHelper.getExclusiveResourceKinds();
      resourceList = this.getDeploymentResourceList(deploymentManifest, context);
      promiseQueue = q();
      errors = [];
      deploymentManifest.usedResources = {};
      for (i = 0, len = resourceList.length; i < len; i++) {
        resURN = resourceList[i];
        (resURN => {
          var resID, resType;
          this.logger.info(`${meth} Checking resource: ${resURN}`);
          resID = null;
          resType = null;
          promiseQueue = promiseQueue.then(() => {
            this.logger.info(`${meth} Getting manifest for ${resURN}`);
            return this.manifestRepository.getManifest(resURN).then(manif => {
              var errMsg;
              this.logger.info(`${meth} Got manifest for ${resURN}`);
              // Keep resource internal name for later
              resID = manif.name;
              resType = manif.ref.kind;
              this.logger.info(`${meth} Resource ID: ${resID} - TYPE: ${resType}`);
              if (!this.authorization.isAllowedForManifest(manif, context)) {
                errMsg = `Resource ${resURN} not found for user.`;
                this.logger.error(`${meth} ERROR: ${errMsg}`);
                errors.push(errMsg);
              } else {
                deploymentManifest.usedResources[resID] = {
                  urn: resURN,
                  type: resType
                };
              }
              return true;
            }).catch(err => {
              var errMsg;
              errMsg = `Resource ${resURN} can't be retrieved. Reason: ${err.message}`;
              this.logger.error(`${meth} ERROR: ${errMsg}`);
              errors.push(errMsg);
              return true;
            });
          });
          return promiseQueue = promiseQueue.then(() => {
            if (indexOf.call(exclusiveResourceTypes, resType) >= 0) {
              this.logger.info(`${meth} Validate availability of URN: ${resURN}`);
              return this.planner.isResourceInUse(resID).then(inUseInfo => {
                var errMsg;
                if (inUseInfo.inUse) {
                  if (inUseInfo.usedByURN === this.manifestHelper.refToUrn(deploymentManifest.ref)) {
                    this.logger.info(`${meth} Resource in use by the same deployment.`);
                  } else {
                    errMsg = `Resource ${resURN} is already in use.`;
                    this.logger.error(`${meth} ERROR: ${errMsg}`);
                    errors.push(errMsg);
                  }
                }
                return true;
              });
            }
          });
        })(resURN);
      }
      return promiseQueue.then(() => {
        var errMsg;
        if (errors.length === 0) {
          this.logger.info(`${meth} All resources OK.`);
          return deploymentManifest;
        } else {
          errMsg = `Resources validation found some problems: ${JSON.stringify(errors)}`;
          this.logger.error(`${meth} ${errMsg}`);
          return q.reject(new Error(errMsg));
        }
      });
    }

    // Checks if a deployment exists with the provided Ref and if the user (part
    // of the context info) is allowed to modify it.
    // Returns a promise:
    // - resolved (with 'true') if the deployment exists and user is allowed
    // - rejected if the deployments does not exist or user is not allowed
    checkDeploymentNameAvailability(deploymentRef, context) {
      var meth, ref1;
      meth = `Admission.checkDeploymentNameAvailability() - Deployment Ref: ${JSON.stringify(deploymentRef)} - User: ${(ref1 = context.user) != null ? ref1.id : void 0}`;
      this.logger.info(meth);
      this.logger.debug(`${meth} - Checking existence of deployment...`);
      // Check if deployment already exists
      return this.getDeploymentFromRef(deploymentRef, context).then(previousManifest => {
        var errMsg;
        if (previousManifest == null) {
          this.logger.debug(`${meth} - Deployment does not exist.`);
        } else {
          // Deployment already exists, check its ownership
          this.logger.debug(`${meth} - Deployment ${JSON.stringify(deploymentRef)} already exists`);
          if (!this.authorization.isAllowedForManifest(previousManifest, context, true)) {
            errMsg = "User is not allowed to modify deployment.";
            this.logger.warn(`${meth} - ${errMsg}`);
            throw new Error(errMsg);
          } else {
            this.logger.debug(`${meth} - User is allowed to modify deployment.`);
          }
        }
        return true;
      }).catch(err => {
        this.logger.warn(`${meth} - ${err.message}`);
        return q.reject(err);
      });
    }

    // Checks if a resource exists with the provided URN and if the user (part
    // of the context info) is allowed to use it.
    // Returns a promise, always resolved with a 'result' structure with the
    // following properties:
    // - resID: the resource internal ID
    // - resType: the resource type
    // - error: if resource does not exist or user is not allowed
    checkResourceAccess(resURN, context) {
      var meth, ref1, resID, resType;
      meth = `Admission.checkResourceAccess() - Resource: ${resURN} - User: ${(ref1 = context.user) != null ? ref1.id : void 0}`;
      this.logger.info(meth);
      resID = null;
      resType = null;
      this.logger.info(`${meth} - Getting resource manifest...`);
      return this.manifestRepository.getManifest(resURN).then(manif => {
        var errMsg, result;
        resID = manif.name;
        resType = manif.ref.kind;
        this.logger.info(`${meth} Got resource manifest. Resource ID: ${resID} - TYPE: ${resType}`);
        this.logger.info(`${meth} Checking access...`);
        if (!this.authorization.isAllowedForManifest(manif, context)) {
          errMsg = `Resource ${resURN} not found for user.`;
          this.logger.error(`${meth} ERROR: ${errMsg}`);
          result = {
            error: errMsg,
            resID: resID,
            resType: resType
          };
        } else {
          this.logger.info(`${meth} User is allowed to access resource`);
          result = {
            error: null,
            resID: resID,
            resType: resType
          };
        }
        return result;
      }).catch(err => {
        var errMsg, result;
        errMsg = `Resource ${resURN} can't be retrieved. Reason: ${err.message}`;
        this.logger.error(`${meth} ERROR: ${errMsg}`);
        result = {
          error: errMsg,
          resID: resID,
          resType: resType
        };
        return result;
      });
    }

    getDeploymentResourceList(deploymentManifest) {
      var deplResources, meth, ref1, ref2, resourceList;
      meth = 'Admission.getDeploymentResourceList()';
      this.logger.info(`${meth}`);
      resourceList = [];
      deplResources = {};
      // Extract a list of resources used in this deployment
      deplResources = ((ref1 = deploymentManifest.description) != null ? (ref2 = ref1.config) != null ? ref2.resource : void 0 : void 0) || {};
      this.logger.info(`${meth} config.resource: ${JSON.stringify(deplResources)}`);
      resourceList = this.extractResources(deplResources, context);
      this.logger.info(`${meth} ResourceList: ${JSON.stringify(resourceList)}`);
      return resourceList;
    }

    // Get a list of URNs of the resources used in the solution.

    // For each deployment, resources are only extracted from "leaf" elements:
    // - the deployment 'config.resource' section is ignored
    // - if the service is builtin:
    //   - the 'config.resource' section of the service is processed
    // - if the service is not builtin:
    //   - if the service has roles:
    //     - the service 'config.resource' section is ignored
    //     - the 'config.resource' section of each of the roles is processed
    //   - if the service has no roles:
    //     - the 'config.resource' section of the service is ignored
    getSolutionResourceList(solutionManifest, context) {
      var deplArtDesc, deplData, deplName, deplResourceList, meth, ref1, ref2, ref3, ref4, ref5, roleData, roleName, roleResources, serviceResources, solResourcesByDeployment;
      meth = 'Admission.getSolutionResourceList()';
      solResourcesByDeployment = {};
      ref1 = solutionManifest.deployments;
      for (deplName in ref1) {
        deplData = ref1[deplName];
        deplResourceList = [];
        // Get deployment artifact descritpion (service-level)
        deplArtDesc = ((ref2 = deplData.artifact) != null ? ref2.description : void 0) || {};
        // If service is a builtin, use the service config section
        if (deplArtDesc.builtin != null && deplArtDesc.builtin) {
          serviceResources = ((ref3 = deplArtDesc.config) != null ? ref3.resource : void 0) || {};
          // Extract role resources and add them to the deployment list
          this.extractResources(serviceResources, context, deplResourceList);
        } else {
          // If service has roles, use their config sections to extract resources
          if (deplArtDesc.role != null && Object.keys(deplArtDesc.role).length !== 0) {
            ref4 = deplArtDesc.role;
            // Service has roles, use their config sections to extract resources
            for (roleName in ref4) {
              roleData = ref4[roleName];
              roleResources = ((ref5 = roleData.config) != null ? ref5.resource : void 0) || {};
              // Extract role resources and add them to the deployment list
              this.extractResources(roleResources, context, deplResourceList);
            }
          }
        }
        this.logger.info(`${meth} - Extracted resources of deployment ${deplName}: ${JSON.stringify(deplResourceList)}`);
        // Add extracted deployment resources to the solution resources list
        solResourcesByDeployment[deplName] = deplResourceList;
      }
      return solResourcesByDeployment;
    }

    // Resource validation:
    // - port resources must use a valid (exists in configured ports list) and
    //   not-used port number
    // - domain resources use a valid and not-used domain name
    // - secret resources don't need to be validated
    // - Volume resources don't need to be validated
    // - CA resources don't need to be validated
    validateResource(context, manifest) {
      var meth;
      meth = 'Admission.validateResource';
      this.logger.info(`${meth} manifest.name: ${manifest != null ? manifest.name : void 0}`);
      return q.promise((resolve, reject) => {
        var domain, err, errMsg, owner, port, ref1, ref2, resUsingDomain, resUsingPort, uniqueID, validationError;
        try {
          // port validation
          if (this.manifestHelper.isPort(manifest)) {
            port = manifest.description.port;
            owner = manifest.owner || ((ref1 = context.user) != null ? ref1.id : void 0);
            uniqueID = owner + '/' + manifest.ref.domain + '/' + manifest.ref.name;
            // Port number must be on of the allowed ports. If it is, also store
            // the corresponding internal port in the Port manifest
            if (!(port in this.config.portDict)) {
              errMsg = `Validating port resource: ${port} is not allowed`;
              this.logger.info(`${meth} - ${errMsg}`);
              reject(new Error(errMsg));
            } else {
              manifest.description.internalPort = this.config.portDict[port];
            }
            // Port number is not used in any port resource
            resUsingPort = "";
            return this.manifestRepository.listElementType('ports').then(manifests => {
              var k, m, uniqueID2;
              for (k in manifests) {
                m = manifests[k];
                uniqueID2 = m.owner + '/' + m.ref.domain + '/' + m.ref.name;
                // Note that the port is stored as externalPort
                if (uniqueID2 !== uniqueID && m.description.externalPort === port) {
                  resUsingPort = k;
                  break;
                }
              }
              if (resUsingPort !== "") {
                errMsg = `Validating port resource: ${port} is in use`;
                this.logger.info(`${meth} - ${errMsg} by ${resUsingPort}`);
                reject(new Error(errMsg));
              }
              // Port number is OK
              return resolve(manifest);
            }).catch(err => {
              this.logger.error(`${meth} - ${errMsg}`);
              return reject(err);
            });
            // secret resource validation
          } else if (this.manifestHelper.isSecret(manifest)) {
            return resolve(manifest);
            // domain resource validation
          } else if (this.manifestHelper.isDomain(manifest)) {
            // Convert domain name to lowercase
            manifest.description.domain = manifest.description.domain.toLowerCase();
            // Domain name is valid
            domain = manifest.description.domain;
            if (!utils.validateDomainName(domain)) {
              errMsg = `Validating domain resource: domain ${domain} is invalid`;
              this.logger.info(`${meth} - ${errMsg}`);
              reject(new Error(errMsg));
            }
            // Domain is not used in any other domain resource
            owner = manifest.owner || ((ref2 = context.user) != null ? ref2.id : void 0);
            uniqueID = owner + '/' + manifest.ref.domain + '/' + manifest.ref.name;
            resUsingDomain = "";
            return this.manifestRepository.listElementType('domains').then(manifests => {
              var k, m, uniqueID2;
              for (k in manifests) {
                m = manifests[k];
                uniqueID2 = m.owner + '/' + m.ref.domain + '/' + m.ref.name;
                if (uniqueID2 !== uniqueID && m.description.domain.toLowerCase() === domain) {
                  resUsingDomain = k;
                  break;
                }
              }
              if (resUsingDomain !== "") {
                errMsg = `Validating domain resource: ${domain} in use by ${resUsingDomain}`;
                this.logger.info(`${meth} - ${errMsg}`);
                reject(new Error(errMsg));
              }
              // Domain is OK
              return resolve(manifest);
            }).catch(err => {
              this.logger.error(`${meth} - ${errMsg}`);
              return reject(err);
            });
            // volume resource validation
          } else if (this.manifestHelper.isVolume(manifest)) {
            // If persistent storage is disabled, do not allow KukuVolumes
            if (this.config.disablePersistentStorage != null && this.config.disablePersistentStorage === true) {
              errMsg = "Validating volume resource: persistence is disabled.";
              this.logger.warn(`${meth} - ${errMsg}`);
              return reject(new Error(errMsg));
            } else {
              // Validate volume type exists and is OK for persistent volumes
              validationError = this.validateVolumeType(manifest);
              if (validationError != null) {
                errMsg = `Validating volume resource: ${validationError}.`;
                this.logger.warn(`${meth} - ${errMsg}`);
                return reject(new Error(errMsg));
              } else {
                return resolve(manifest);
              }
            }
            // CA resource validation
          } else if (this.manifestHelper.isCA(manifest)) {
            return resolve(manifest);
            // Certificate resource validation
          } else if (this.manifestHelper.isCertificate(manifest)) {
            return resolve(manifest);
          } else {
            return resolve(manifest);
          }
        } catch (error1) {
          err = error1;
          this.logger.error(`${meth} - ${errMsg}`);
          return reject(err);
        }
      });
    }

    validateVolumeType(volumeManifest) {
      var meth, typeProperties, typePropertiesArray, volumeType;
      meth = 'Admission.validateVolumeType()';
      if (volumeManifest.description == null) {
        return "missing description in volume manifest";
      }
      if (volumeManifest.description.type == null) {
        return "missing 'type' property in volume manifest";
      }
      // if not volumeManifest.description.items?
      //   return "missing 'items' property in volume manifest"
      volumeType = volumeManifest.description.type;
      if (this.config.clusterConfiguration.volumeTypes == null || Object.keys(this.config.clusterConfiguration.volumeTypes).length === 0) {
        this.logger.warn(`${meth} No volume type configuration available in cluster. Skipping volume type validation.`);
        return null;
      }
      if (!(volumeType in this.config.clusterConfiguration.volumeTypes)) {
        return `volume type '${volumeType}' not supported in cluster`;
      }
      typeProperties = this.config.clusterConfiguration.volumeTypes[volumeType].properties || "";
      if (typeProperties === "") {
        return `volume type '${volumeType}' is not valid for persistent volumes (no properties)`;
      }
      typePropertiesArray = typeProperties.split(',');
      if (indexOf.call(typePropertiesArray, 'persistent') < 0) {
        return `volume type '${volumeType}' is not valid for persistent volumes (no 'persistent' property)`;
      }
      return null;
    }

    canBeDeleted(urn) {
      this.logger.info('Admission.canBeDeleted()');
      this.logger.debug('Checking if URN is in use: ', urn);
      // Ask planner if URN is currently in use
      return this.isElementInUse(urn).then(function (inUse) {
        return q(!inUse);
      });
    }

    isElementInUse(urn) {
      return this.planner.isElementInUse(urn).then(function (info) {
        return q(info.inUse);
      }).fail(error => {
        this.logger.error(`Error checking if element in use: ${error.message}`);
        this.logger.error('ERROR: ', error.stack);
        return q.reject(error);
      });
    }

    isResourceInUse(resourceName) {
      return this.planner.isResourceInUse(resourceName).then(function (info) {
        return q(info.inUse);
      }).fail(error => {
        this.logger.error(`Error checking if resource in use: ${error.message}`);
        this.logger.error('ERROR: ', error.stack);
        return q.reject(error);
      });
    }

    loadManifest(manifestOptions) {
      this.logger.info('Admission.loadManifest()');
      // if manifestOptions.url?
      //   u.jsonFromUrl manifestOptions.url
      // else if manifestOptions.filename?
      if (manifestOptions.filename != null) {
        return utils.jsonFromFile(manifestOptions.filename);
      } else if (manifestOptions.inline != null) {
        return q(manifestOptions.inline);
      } else {
        return q.reject(new Error('Deployment manifest parameter is missing.'));
      }
    }

    // VERSION WITH FULL DATE (was too long for some Kubernetes IDs)
    getDateSuffixLong() {
      var d, s;
      d = new Date();
      s = d.getFullYear();
      s += ('0' + (d.getMonth() + 1)).slice(-2);
      s += ('0' + d.getDate()).slice(-2);
      s += '_' + ('0' + d.getHours()).slice(-2);
      s += ('0' + d.getMinutes()).slice(-2);
      return s += ('0' + d.getSeconds()).slice(-2);
    }

    // VERSION WITH FULL DATE (was too long for some Kubernetes IDs)
    getDateSuffix() {
      var d, s;
      d = new Date();
      s = ('0' + d.getHours()).slice(-2);
      s += ('0' + d.getMinutes()).slice(-2);
      return s += ('0' + d.getSeconds()).slice(-2);
    }

    getSolutionID() {
      return `ks-${this.getDateSuffix()}-${utils.randomHex(8)}`;
    }

    getDeploymentNameKmv3() {
      return `kd-${this.getDateSuffix()}-${utils.randomHex(8)}`;
    }

    generateResourceName() {
      return `kres-${this.getDateSuffix()}-${utils.randomHex(8)}`;
    }

    // Look for Resources objects in an object at any depth
    extractResources(obj, context, resourceList = []) {
      var k, keys, normResRef, ref1, resURN, v, validKinds;
      validKinds = this.manifestHelper.getValidResourceKinds();
      if (obj == null) {
        return resourceList;
      }
      if ('object' !== typeof obj) {
        return resourceList;
      }
      keys = Object.keys(obj);
      if (keys.length === 1 && (ref1 = keys[0], indexOf.call(validKinds, ref1) >= 0)) {
        // It's a resource object, only keep resource references (strings) not
        // inline resource definitions (objects)
        if ('string' === typeof obj[keys[0]] && obj[keys[0]] !== '') {
          // First and only key (0) is the resource type
          // The object of that key is the resource reference

          // Normalize reference (add a domain if it doesn't include one)
          normResRef = this.normalizeResourceReference(obj[keys[0]], context);
          resURN = 'eslap://' + normResRef.replace('/', `/${keys[0]}/`);
          if (indexOf.call(resourceList, resURN) < 0) {
            resourceList.push(resURN);
          }
        } else {
          for (k in obj) {
            v = obj[k];
            this.extractResources(v, context, resourceList);
          }
        }
      } else {
        for (k in obj) {
          v = obj[k];
          this.extractResources(v, context, resourceList);
        }
      }
      return resourceList;
    }

    // Add a domain to a resource reference if it doesn't have one.
    // Currently, the default domain is the username of who performs the operation
    normalizeResourceReference(resourceReference, context) {
      var newResourceReference;
      if (resourceReference.indexOf('/') > 0) {
        newResourceReference = resourceReference;
      } else {
        newResourceReference = `${context.user.id}/${resourceReference}`;
      }
      return newResourceReference;
    }

    // Generate a deterministic link name, based on the link data

    // The link name is generated by:
    // - creating a string representation of each endpoint (all endpoint elements
    //   concatenated separated by a '/')
    // - ordering the endpoint strings in Lexicographic order and concatenating
    //   them into a new string ('name')
    // - calculating a hash of 'name'
    // - KukuLink name will be 'kl-<calculated_hash>''

    // Sample link manifest endpoints section:
    //   "endpoints": [
    //     {
    //       "name": "helloworlddep",
    //       "kind": "solution",
    //       "domain": "my.examples",
    //       "channel": "hello",
    //       "kumoriName": "kd-065441-53ec4c1b"
    //     },
    //     {
    //       "name": "helloworldinb",
    //       "kind": "solution",
    //       "domain": "my.examples",
    //       "channel": "frontend",
    //       "kumoriName": "kh-083835-557149b5"
    //     }
    //   ]
    generateLinkName(linkManif) {
      var ep0Str, ep0channel, ep1Str, ep1channel, hash, name;
      ep0channel = linkManif.endpoints[0].channel;
      ep0Str = linkManif.endpoints[0].domain + '/' + linkManif.endpoints[0].kind + '/' + linkManif.endpoints[0].name + '/' + ep0channel;
      ep1channel = linkManif.endpoints[1].channel;
      ep1Str = linkManif.endpoints[1].domain + '/' + linkManif.endpoints[1].kind + '/' + linkManif.endpoints[1].name + '/' + ep1channel;
      if (ep0Str <= ep1Str) {
        name = ep0Str + '-' + ep1Str;
      } else {
        name = ep1Str + '-' + ep0Str;
      }
      hash = utils.getHash(name);
      name = `kl-${hash}`;
      return name;
    }

  };

  module.exports = Admission;
}).call(undefined);