'use strict';

(function () {
  /*
  * Copyright 2022 Kumori Systems S.L.
   * Licensed under the EUPL, Version 1.2 or – as soon they
    will be approved by the European Commission - subsequent
    versions of the EUPL (the "Licence");
   * You may not use this work except in compliance with the
    Licence.
   * You may obtain a copy of the Licence at:
     https://joinup.ec.europa.eu/software/page/eupl
   * Unless required by applicable law or agreed to in
    writing, software distributed under the Licence is
    distributed on an "AS IS" basis,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
    express or implied.
   * See the Licence for the specific language governing
    permissions and limitations under the Licence.
   */
  var ADMIN_ROLE,
      AUTHENTICATION_ERROR,
      EMITTED_EVENTS_NAME,
      WebsocketPublisher,
      sio,
      indexOf = [].indexOf;

  sio = require('socket.io');

  ADMIN_ROLE = 'ADMIN';

  EMITTED_EVENTS_NAME = 'ecloud-event';

  AUTHENTICATION_ERROR = 'Authentication error';

  WebsocketPublisher = class WebsocketPublisher {
    constructor(httpServer, httpAuthenticator) {
      this.httpServer = httpServer;
      this.httpAuthenticator = httpAuthenticator;
      this.logger.info('WebsocketPublisher.constructor');
      // Setup a Socket.io websocket to listen on the server
      this.socketIO = sio.listen(this.httpServer);
      // Configure Websocket handshake for authentication
      this.socketIO.use((socket, next) => {
        return this.sioHandshaker(socket, next);
      });
      this.socketIO.on('connection', socket => {
        return this.sioConnectionHandler(socket);
      });
      this.socketsByUserId = {};
      this.socketsByAdminId = {};
    }

    sioHandshaker(socket, next) {
      var authToken, meth, ref, ref1;
      meth = 'WSPublisher.handshake';
      this.logger.info(meth);
      // authHeader = socket?.request?.headers?.authorization
      // if not authHeader?
      //   @logger.info "#{meth} - Access denied for anonymous request."
      //   return next new Error AUTHENTICATION_ERROR
      // [protocol, basic] = authHeader.split ' '
      // if protocol isnt 'Basic'
      //   @logger.info "#{meth} - Access denied for incorrect protocol \
      //     #{protocol} request."
      //   return next new Error AUTHENTICATION_ERROR
      // buf = Buffer.from(basic, 'base64')
      // plain_auth = buf.toString()
      // creds = plain_auth.split(':')
      // authToken = "Bearer #{creds[0]}"
      authToken = socket != null ? (ref = socket.handshake) != null ? (ref1 = ref.query) != null ? ref1.token : void 0 : void 0 : void 0;
      this.logger.info(`${meth} - AuthToken: ${authToken}`);
      if (authToken == null || authToken === '') {
        this.logger.info(`${meth} - Access denied for anonymous request.`);
      }
      return this.httpAuthenticator.authenticate(authToken).then(userData => {
        socket.user = userData;
        this.logger.info(`${meth} - User authenticated: ${JSON.stringify(userData)}`);
        return next();
      }).fail(err => {
        this.logger.info(`${meth} - Access denied for token ${authToken}.`);
        return next(new Error(AUTHENTICATION_ERROR));
      });
    }

    sioConnectionHandler(socket) {
      var meth;
      meth = 'WSPublisher.onConnection';
      this.logger.info(meth);
      if (socket.user == null) {
        this.logger.error(`${meth} - Socket not authenticated correctly.`);
        socket.emit('error', 'Connection not authenticated correctly. Disconnecting.');
        return socket.disconnect();
      } else {
        this.logger.info(`${meth} - Socket ${socket.id} connected. User: ` + `${JSON.stringify(socket.user)}`);
        this.addUserSocket(socket);
        return this.configureSocketHandlers(socket);
      }
    }

    configureSocketHandlers(socket) {
      socket.on('disconnect', reason => {
        return this.socketDisconnectionHandler(socket, reason);
      });
      return socket.on('error', error => {
        return this.socketErrorHandler(socket, error);
      });
    }

    socketDisconnectionHandler(socket, reason) {
      var meth;
      meth = 'WSPublisher.onSocketDisconnection';
      this.logger.info(`${meth} - Socket ${socket.id} - Reason: ${reason}`);
      return this.removeUserSocket(socket);
    }

    socketErrorHandler(socket, error) {
      return this.logger.info(`WSPublisher.onSocketError - User: ${socket.user.id} - ` + `Error: ${error.message}`);
    }

    // Add a socket to the user socket list if it's not already there.

    // The user socket list is a two-level dictionary, by user ID and socket ID:
    // {
    //   "user1": {
    //     "sock1" : socketObj1,
    //     "sock2" : socketObj2,
    //     "sock3" : socketObj3
    //   },
    //   "user2": {
    //     "sock5" : socketObj5,
    //     "sock23" : socketObj23,
    //     "sock12" : socketObj12
    //   }
    // }
    addUserSocket(socket) {
      var isAdmin, meth, userId;
      meth = 'WebsocketPublisher.addUserSocket';
      userId = socket.user.id;
      isAdmin = indexOf.call(socket.user.roles, ADMIN_ROLE) >= 0;
      if (isAdmin) {
        // For now we just add it to Admin list, since it will receive everything,
        // including its own events.
        if (!(userId in this.socketsByAdminId)) {
          this.socketsByAdminId[userId] = {};
        }
        if (!(socket.id in this.socketsByAdminId[userId])) {
          this.socketsByAdminId[userId][socket.id] = socket;
          return this.logger.debug(`${meth} - Socket ${socket.id} added to ${userId} listfor Admins.`);
        } else {
          return this.logger.debug(`${meth} - Socket ${socket.id} already in ${userId} listfor Admins.`);
        }
      } else {
        if (!(userId in this.socketsByUserId)) {
          this.socketsByUserId[userId] = {};
        }
        if (!(socket.id in this.socketsByUserId[userId])) {
          this.socketsByUserId[userId][socket.id] = socket;
          return this.logger.debug(`${meth} - Socket ${socket.id} added to ${userId} list.`);
        } else {
          return this.logger.debug(`${meth} - Socket ${socket.id} already in ${userId} list.`);
        }
      }
    }

    // Removes a socket from the user socket list.
    removeUserSocket(socket) {
      var isAdmin, meth, userId;
      meth = 'WebsocketPublisher.removeUserSocket';
      userId = socket.user.id;
      isAdmin = indexOf.call(socket.user.roles, ADMIN_ROLE) >= 0;
      if (isAdmin) {
        if (userId in this.socketsByAdminId) {
          if (socket.id in this.socketsByAdminId[userId]) {
            delete this.socketsByAdminId[userId][socket.id];
            this.logger.debug(`${meth} - Removed ${socket.id} from ${userId} list forAdmins.`);
            if (Object.keys(this.socketsByAdminId[userId]).length === 0) {
              delete this.socketsByAdminId[userId];
              return this.logger.debug(`${meth} - Removed ${userId} from list for Admins.`);
            }
          } else {
            return this.logger.debug(`${meth} - Socket ${socket.id} not in ${userId} listfor Admins.`);
          }
        } else {
          return this.logger.debug(`${meth} - User ${userId} not in socket list for Admins.`);
        }
      } else {
        if (userId in this.socketsByUserId) {
          if (socket.id in this.socketsByUserId[userId]) {
            delete this.socketsByUserId[userId][socket.id];
            this.logger.debug(`${meth} - Removed ${socket.id} from ${userId} list.`);
            if (Object.keys(this.socketsByUserId[userId]).length === 0) {
              delete this.socketsByUserId[userId];
              return this.logger.debug(`${meth} - Removed ${userId} from list.`);
            }
          } else {
            return this.logger.debug(`${meth} - Socket ${socket.id} not in ${userId} list.`);
          }
        } else {
          return this.logger.debug(`${meth} - User ${userId} not in socket list.`);
        }
      }
    }

    publish(evt) {
      var adminId, adminSockets, err, evtStr, meth, ref, ref1, results, s, sid;
      meth = 'WebsocketPublisher.publish';
      evtStr = JSON.stringify(evt);
      ref = this.socketsByAdminId;
      // Publish event to all admins sockets
      for (adminId in ref) {
        adminSockets = ref[adminId];
        for (sid in adminSockets) {
          s = adminSockets[sid];
          this.logger.debug(`${meth} - Emitting event to socket ${sid}: ${evtStr}`);
          try {
            s.emit(EMITTED_EVENTS_NAME, evt);
          } catch (error1) {
            err = error1;
            this.logger.warn(`${meth} - Error emitting: ${err.message} - ${evtStr}`);
          }
        }
      }
      // Publish event to all owner user sockets
      if (evt.owner == null) {
        return this.logger.debug(`${meth} - Event data has no owner: ${evtStr}`);
      } else if (!(evt.owner in this.socketsByUserId)) {
        return this.logger.debug(`${meth} - Event owner has no active sockets: ${evtStr}`);
      } else {
        this.logger.debug(`${meth} - Emitting event to client sockets: ${evtStr}`);
        ref1 = this.socketsByUserId[evt.owner];
        results = [];
        for (sid in ref1) {
          s = ref1[sid];
          this.logger.debug(`${meth} - Emitting event to socket ${sid}: ${evtStr}`);
          try {
            results.push(s.emit(EMITTED_EVENTS_NAME, evt));
          } catch (error1) {
            err = error1;
            results.push(this.logger.warn(`${meth} - Error emitting: ${err.message} - ${evtStr}`));
          }
        }
        return results;
      }
    }

    reconfigure() {
      return this.logger.info('WebsocketPublisher.reconfigure');
    }

    // TODO
    disconnect() {
      return this.logger.info('WebsocketPublisher.disconnect');
    }

    terminate() {
      return this.logger.info('WebsocketPublisher.terminate');
    }

  };

  module.exports = WebsocketPublisher;
}).call(undefined);