"use strict";

(function () {
  /*
  * Copyright 2020 Kumori Systems S.L.
   * Licensed under the EUPL, Version 1.2 or – as soon they
    will be approved by the European Commission - subsequent
    versions of the EUPL (the "Licence");
   * You may not use this work except in compliance with the
    Licence.
   * You may obtain a copy of the Licence at:
     https://joinup.ec.europa.eu/software/page/eupl
   * Unless required by applicable law or agreed to in
    writing, software distributed under the Licence is
    distributed on an "AS IS" basis,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
    express or implied.
   * See the Licence for the specific language governing
    permissions and limitations under the Licence.
   */
  var ADMIN_ROLES, Authorization, q;

  q = require('q');

  ADMIN_ROLES = ["admin", "administrator", "ADMIN", "ADMINISTRATOR"];

  Authorization = class Authorization {
    constructor(store, planner) {
      this.store = store;
      this.planner = planner;
      this.logger.info('Authorization.constructor');
      this.ownerCache = {};
    }

    isAdmin(context) {
      var adminLabel, i, len, roles;
      roles = context.user.roles;
      for (i = 0, len = ADMIN_ROLES.length; i < len; i++) {
        adminLabel = ADMIN_ROLES[i];
        if (roles.indexOf(adminLabel) !== -1) {
          return true;
        }
      }
      return false;
    }

    // Checks if user is allowed to access a manifest
    isAllowedForManifest(manifest, context, write = false) {
      var meth, ref, ref1;
      meth = `Authorization.isAllowedForManifest name=${manifest.name}, ${(ref = context.user) != null ? ref.id : void 0})`;
      if (write) {
        meth += ', WritePermission';
      }
      if (this.isAdmin(context)) {
        this.logger.info(`${meth} - Access allowed (admin)`);
        return true;
      }
      if (manifest.owner == null) {
        this.logger.info(`${meth} - Access allowed (public element)`);
        return true;
      }
      if (manifest.owner === ((ref1 = context.user) != null ? ref1.id : void 0)) {
        this.logger.info(`${meth} - Access allowed (user is owner)`);
        return true;
      }
      if (!write && manifest.public != null && manifest.public) {
        this.logger.info(`${meth} - Access allowed (resource is public)`);
        return true;
      }
      this.logger.warn(`${meth} - Access denied. Instance owner: ${manifest.owner}`);
      return false;
    }

    // Checks if user is allowed to access an element by type and internal ID
    isAllowed(elementType, id, context) {
      var meth;
      meth = `Authorization.isAllowed ${elementType}=${id}, ${context.user.id})`;
      this.logger.info;
      if (this.isAdmin(context)) {
        this.logger.info(`${meth} - Access allowed (admin)`);
        return q(true);
      }
      if (elementType === 'instance') {
        return this.planner.getInstanceOwner(id).then(owner => {
          if (context.user.id === owner) {
            this.logger.info(`${meth} - Instance owner: ${owner} - Access granted.`);
            return true;
          } else {
            this.logger.info(`${meth} - Instance owner: ${owner} - Access denied.`);
            return false;
          }
        }).catch(err => {
          var errMsg;
          errMsg = `Couldn't verify access: ${err.message}`;
          this.logger.info(`${meth} - ${errMsg}`);
          return false;
        });
      } else if (elementType === 'deployment') {
        return this.planner.getDeploymentOwner(id).then(owner => {
          if (context.user.id === owner) {
            this.logger.info(`${meth} - Deployment owner: ${owner} - Access granted.`);
            return true;
          } else {
            this.logger.info(`${meth} - Deployment owner: ${owner} - Access denied.`);
            return false;
          }
        }).catch(err => {
          var errMsg;
          errMsg = `Couldn't verify access: ${err.message}`;
          this.logger.info(`${meth} - ${errMsg}`);
          return false;
        });
      } else {
        return q(true);
      }
    }

    // Checks if user is allowed to access an element by URN
    isAllowedForURN(elementURN, context, write = false) {
      var meth;
      meth = `Authorization.isAllowedForURN ${elementURN}, ${context.user.id}`;
      if (write) {
        meth += ', WritePermission';
      }
      this.logger.info;
      if (this.isAdmin(context)) {
        this.logger.info(`${meth} - Access allowed (admin)`);
        return q(true);
      }
      // Fetch from manifest store
      return this.store.getManifestByURN(elementURN).then(manifest => {
        this.logger.info(`${meth} - Found element manifest. Owner: ${manifest.owner}`);
        if (context.user.id === manifest.owner) {
          this.logger.info(`${meth} - Access granted.`);
          return true;
        } else if (!write && manifest.public != null && manifest.public) {
          this.logger.info(`${meth} - Access granted (element is public)`);
          return true;
        } else {
          this.logger.info(`${meth} - Access denied.`);
          return false;
        }
      }).catch(err => {
        var errMsg, ref;
        errMsg = `Couldn't verify access: ${err.message}`;
        if ((ref = err.message) != null ? ref.includes('Element not found') : void 0) {
          this.logger.info(`${meth} - Element not found.`);
        } else {
          this.logger.error(`${meth} - ERROR: ${errMsg}`);
        }
        return false;
      });
    }

    // Checks if current user has permission to list resources with the parameters
    // specified in the filter
    canListResources(context, filter) {
      this.logger.debug(`Authorization.canListResources user:${context.user.id}, filter:${JSON.stringify(filter)}`);
      return q.promise((resolve, reject) => {
        if (this.isAdmin(context)) {
          return resolve();
        }
        if (filter.owner == null && filter.urn == null) {
          return reject(new Error(`Unauthorized request from ${context.user.id} (no data in filter)`));
        }
        if (filter.owner != null && context.user.id !== filter.owner) {
          return reject(new Error(`Unauthorized request from ${context.user.id} (asking for owner ${filter.owner})`));
        }
        if (filter.urn != null) {
          return this.isAllowedForURN(filter.urn, context).then(function (isAllowed) {
            if (isAllowed) {
              return resolve();
            } else {
              throw new Error('User not allowed to access resource.');
            }
          }).catch(function (err) {
            return reject(err);
          });
        } else {
          return resolve();
        }
      });
    }

  };

  module.exports = Authorization;
}).call(undefined);