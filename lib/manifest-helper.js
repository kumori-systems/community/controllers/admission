'use strict';

(function () {
  /*
  * Copyright 2020 Kumori Systems S.L.
   * Licensed under the EUPL, Version 1.2 or – as soon they
    will be approved by the European Commission - subsequent
    versions of the EUPL (the "Licence");
   * You may not use this work except in compliance with the
    Licence.
   * You may obtain a copy of the Licence at:
     https://joinup.ec.europa.eu/software/page/eupl
   * Unless required by applicable law or agreed to in
    writing, software distributed under the Licence is
    distributed on an "AS IS" basis,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
    express or implied.
   * See the Licence for the specific language governing
    permissions and limitations under the Licence.
   */
  var EXCLUSIVE_RESOURCE_KINDS,
      KIND_KMV3_DEPLOYMENT,
      KIND_RESOURCE_CA,
      KIND_RESOURCE_CERTIFICATE,
      KIND_RESOURCE_DOMAIN,
      KIND_RESOURCE_PORT,
      KIND_RESOURCE_SECRET,
      KIND_RESOURCE_VOLUME,
      KIND_SOLUTION,
      ManifestHelper,
      UPDATABLE_RESOURCE_KINDS,
      VALID_RESOURCE_KINDS,
      path,
      q,
      url,
      utils,
      indexOf = [].indexOf;

  q = require('q');

  url = require('url');

  path = require('path');

  utils = require('./utils');

  KIND_SOLUTION = 'solution';

  KIND_KMV3_DEPLOYMENT = 'deployment';

  KIND_RESOURCE_CA = "ca";

  KIND_RESOURCE_CERTIFICATE = "certificate";

  KIND_RESOURCE_DOMAIN = "domain";

  KIND_RESOURCE_PORT = "port";

  KIND_RESOURCE_SECRET = "secret";

  KIND_RESOURCE_VOLUME = "volume";

  VALID_RESOURCE_KINDS = [KIND_RESOURCE_CA, KIND_RESOURCE_CERTIFICATE, KIND_RESOURCE_DOMAIN, KIND_RESOURCE_PORT, KIND_RESOURCE_SECRET, KIND_RESOURCE_VOLUME];

  EXCLUSIVE_RESOURCE_KINDS = [KIND_RESOURCE_DOMAIN, KIND_RESOURCE_PORT, KIND_RESOURCE_VOLUME];

  UPDATABLE_RESOURCE_KINDS = [KIND_RESOURCE_CA, KIND_RESOURCE_CERTIFICATE, KIND_RESOURCE_DOMAIN, KIND_RESOURCE_PORT, KIND_RESOURCE_SECRET];

  ManifestHelper = function () {
    class ManifestHelper {
      constructor() {
        this.logger.info('ManifestHelper.constructor()');
      }

      //#############################################################################
      //#  KUMORI URN AND REF HANDLING METHODS                                     ##
      //#############################################################################

      // Converts a Kumori Ref object (with the following properties: domain, kind,
      // name) to a Kumori URN 'eslap://<domain>/<kind>/<name>'
      refToUrn(ref) {
        var p;
        p = path.join(ref.domain, ref.kind, ref.name);
        return 'eslap://' + p;
      }

      // Converts a Kumori URN 'eslap://<domain>/<kind>/<name>' to a Kumori Ref
      // object with the following properties: domain, kind, name
      urnToRef(urn) {
        var customPath, customPathParts, domain, e, errMsg, kind, name, parsedURN, removeLength, result;
        try {
          // url.parse parses a URL in the format <protocol>://<host>/<pathname> and
          // generates an object that includes the following properties (among
          // others):
          // - host
          // - pathname
          parsedURN = url.parse(urn);
          // The 'host' contains our domain
          domain = parsedURN.host;
          // Calculate length of:  <protocol>://<domain>/
          removeLength = parsedURN.protocol.length + 3 + domain.length + 1;
          // Get the full path (unparsed to support #, ?, etc.)
          customPath = urn.substring(removeLength - 1);
          // Split path by /
          customPathParts = customPath.split('/');
          // Get the kind/type
          kind = customPathParts.shift(); // First element is the element kind

          // Get the name (the remaining parts of the path as they were originally)
          name = customPathParts.join('/');
          // Construct the Ref object to be returned
          result = {
            domain: domain,
            kind: kind,
            name: name
          };
          return result;
        } catch (error) {
          e = error;
          errMsg = `Error parsing Kumori URN ${urn}: ${e.message || e.stack}`;
          this.logger.error(`ManifestHelper.urnToRef - ERROR: ${errMsg}`);
          throw new Error(errMsg);
        }
      }

      //####################################################
      //# Check type from Manifest                        ##
      //####################################################
      isSolution(manifest) {
        var ref1;
        return ((ref1 = manifest.ref) != null ? ref1.kind : void 0) === KIND_SOLUTION;
      }

      isKmv3Deployment(manifest) {
        var ref1;
        return ((ref1 = manifest.ref) != null ? ref1.kind : void 0) === KIND_KMV3_DEPLOYMENT;
      }

      isResource(manifest) {
        var ref1, ref2;
        return ref1 = (ref2 = manifest.ref) != null ? ref2.kind : void 0, indexOf.call(VALID_RESOURCE_KINDS, ref1) >= 0;
      }

      isSecret(manifest) {
        var ref1;
        return ((ref1 = manifest.ref) != null ? ref1.kind : void 0) === KIND_RESOURCE_SECRET;
      }

      isPort(manifest) {
        var ref1;
        return ((ref1 = manifest.ref) != null ? ref1.kind : void 0) === KIND_RESOURCE_PORT;
      }

      isVolume(manifest) {
        var ref1;
        return ((ref1 = manifest.ref) != null ? ref1.kind : void 0) === KIND_RESOURCE_VOLUME;
      }

      isDomain(manifest) {
        var ref1;
        return ((ref1 = manifest.ref) != null ? ref1.kind : void 0) === KIND_RESOURCE_DOMAIN;
      }

      isCA(manifest) {
        var ref1;
        return ((ref1 = manifest.ref) != null ? ref1.kind : void 0) === KIND_RESOURCE_CA;
      }

      isCertificate(manifest) {
        var ref1;
        return ((ref1 = manifest.ref) != null ? ref1.kind : void 0) === KIND_RESOURCE_CERTIFICATE;
      }

      isLink(manifest) {
        return manifest.endpoints != null || manifest['s_d'] != null;
      }

      //####################################################
      //# Check type from URN                             ##
      //####################################################
      isSolutionURN(urn) {
        return this.urnToRef(urn).kind === KIND_SOLUTION;
      }

      isKmv3DeploymentURN(urn) {
        return this.urnToRef(urn).kind === KIND_KMV3_DEPLOYMENT;
      }

      isResourceURN(urn) {
        var ref1;
        return ref1 = this.urnToRef(urn).kind, indexOf.call(VALID_RESOURCE_KINDS, ref1) >= 0;
      }

      isSecretURN(urn) {
        return this.urnToRef(urn).kind === KIND_RESOURCE_SECRET;
      }

      isPortURN(urn) {
        return this.urnToRef(urn).kind === KIND_RESOURCE_PORT;
      }

      isVolumeURN(urn) {
        return this.urnToRef(urn).kind === KIND_RESOURCE_VOLUME;
      }

      isDomainURN(urn) {
        return this.urnToRef(urn).kind === KIND_RESOURCE_DOMAIN;
      }

      isCAURN(urn) {
        return this.urnToRef(urn).kind === KIND_RESOURCE_CA;
      }

      isCertificateURN(urn) {
        return this.urnToRef(urn).kind === KIND_RESOURCE_CERTIFICATE;
      }

      // Determine if an element Kind is a valid resource Kind
      isResourceKind(kind) {
        return indexOf.call(VALID_RESOURCE_KINDS, kind) >= 0;
      }

      // Get a list of valid resource Kinds
      getValidResourceKinds() {
        return VALID_RESOURCE_KINDS;
      }

      // Get a list of exclusive resource Kinds
      getExclusiveResourceKinds() {
        return EXCLUSIVE_RESOURCE_KINDS;
      }

      // Get a list of resource Kinds that support updates
      getUpdatableResourceKinds() {
        return UPDATABLE_RESOURCE_KINDS;
      }

      // Get the Kind of the element represented by the Manifest
      getManifestType(manifest) {
        // coffeelint: disable=max_line_length
        if ((manifest != null ? manifest.ref : void 0) != null) {
          if (manifest.ref.kind === KIND_SOLUTION) {
            return ManifestHelper.SOLUTION;
          } else if (manifest.ref.kind === KIND_KMV3_DEPLOYMENT) {
            return ManifestHelper.DEPLOYMENT;
          } else if (manifest.ref.kind === KIND_RESOURCE_SECRET) {
            return ManifestHelper.RESOURCE;
          } else if (manifest.ref.kind === KIND_RESOURCE_PORT) {
            return ManifestHelper.RESOURCE;
          } else if (manifest.ref.kind === KIND_RESOURCE_VOLUME) {
            return ManifestHelper.RESOURCE;
          } else if (manifest.ref.kind === KIND_RESOURCE_DOMAIN) {
            return ManifestHelper.RESOURCE;
          } else if (manifest.ref.kind === KIND_RESOURCE_CA) {
            return ManifestHelper.RESOURCE;
          } else if (manifest.ref.kind === KIND_RESOURCE_CERTIFICATE) {
            return ManifestHelper.RESOURCE;
          } else {
            return ManifestHelper.UNKNOWN;
          }
        } else {
          return ManifestHelper.UNKNOWN;
        }
      }

      // coffeelint: enable=max_line_length

      // Hash kumori label
      hashLabel(labelStr) {
        if (labelStr != null) {
          return utils.getFnvHash(labelStr);
        } else {
          return labelStr;
        }
      }

      // Hash container name
      hashContainerName(containerName) {
        if (containerName != null) {
          return 'k-' + utils.getFnvHash(containerName);
        } else {
          return void 0;
        }
      }

    };

    // Element types
    ManifestHelper.DEPLOYMENT = 0;

    ManifestHelper.SERVICE = 1;

    ManifestHelper.COMPONENT = 2;

    ManifestHelper.RESOURCE = 3;

    ManifestHelper.BLOB = 4;

    ManifestHelper.RUNTIME = 5;

    ManifestHelper.LINK = 6;

    ManifestHelper.BUNDLE = 7;

    ManifestHelper.TEST = 8;

    ManifestHelper.SOLUTION = 9;

    // Unknown type
    ManifestHelper.UNKNOWN = -1;

    return ManifestHelper;
  }.call(this);

  module.exports = ManifestHelper;
}).call(undefined);