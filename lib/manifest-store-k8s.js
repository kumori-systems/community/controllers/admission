'use strict';

(function () {
  /*
  * Copyright 2020 Kumori Systems S.L.
   * Licensed under the EUPL, Version 1.2 or – as soon they
    will be approved by the European Commission - subsequent
    versions of the EUPL (the "Licence");
   * You may not use this work except in compliance with the
    Licence.
   * You may obtain a copy of the Licence at:
     https://joinup.ec.europa.eu/software/page/eupl
   * Unless required by applicable law or agreed to in
    writing, software distributed under the Licence is
    distributed on an "AS IS" basis,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
    express or implied.
   * See the Licence for the specific language governing
    permissions and limitations under the Licence.
   */
  var DEFAULT_CONFIG, K8sApiStub, KukuModel, MOCK_NULL_MANIFEST, ManifestHelper, ManifestStore, ManifestStoreK8S, q, utils;

  q = require('q');

  ManifestHelper = require('./manifest-helper');

  ManifestStore = require('./manifest-store');

  KukuModel = require('./k8s-kuku-model/kuku-model');

  K8sApiStub = require('./k8s-api-stub');

  utils = require('./utils');

  DEFAULT_CONFIG = {
    k8sApi: {
      kubeConfig: {
        clusters: [{
          name: 'minikube-cluster',
          server: 'https://192.168.99.100:8443',
          caFile: '/home/jferrer/.minikube/ca.crt',
          skipTLSVerify: false
        }],
        users: [{
          name: 'minikube',
          certFile: '/home/jferrer/.minikube/client.crt',
          keyFile: '/home/jferrer/.minikube/client.key'
        }, {
          name: 'my-user',
          password: 'my-password'
        }],
        contexts: [{
          name: 'minikube',
          user: 'my-user',
          cluster: 'minikube-cluster'
        }],
        currentContext: 'minikube'
      }
    }
  };

  MOCK_NULL_MANIFEST = {
    spec: "SPEC NULL MANIFEST",
    data: "MOCK NULL MANIFEST"
  };

  ManifestStoreK8S = class ManifestStoreK8S extends ManifestStore {
    constructor(options) {
      var meth;
      super('K8S');
      meth = 'ManifestStoreK8S.constructor';
      this.logger.info(meth);
      this.config = options || DEFAULT_CONFIG;
      // Will need:
      // - a stub for K8S API Server (or configuration for instantiating one)
      // - a KumoriManifest to/from K-Model converter (if done at this level)
      // - algorith to convert URN to K8S-K-Model "selector"
      this.kukuModel = new KukuModel();
      if (this.config.k8sApiStub != null) {
        this.apiServer = this.config.k8sApiStub;
      } else {
        this.apiServer = new K8sApiStub(this.config.k8sApi);
      }
      this.manifestHelper = new ManifestHelper();
    }

    init() {
      var meth;
      meth = 'ManifestStoreK8S.init';
      this.logger.info(meth);
      return q.promise((resolve, reject) => {
        // Inititalize/test K8S API Server stub
        return this.apiServer.init().then(() => {
          this.logger.info(`${meth} - K8s API Server Stub initialized.`);
          this.logger.info(`${meth} - K8S Manifest Store initialized.`);
          return resolve();
        }).catch(err => {
          var errMsg;
          errMsg = `Initializing K8S Manifest Store: ${err.message}`;
          this.logger.error(`${meth} - ${errMsg}`);
          return reject(new Error(errMsg));
        });
      });
    }

    terminate(deleteStorage = false) {
      // Nothing to do in Filew System storage
      return q(true);
    }

    // Expects an Object representing the manifest, *NOT* the JSON
    storeManifest(manifest) {
      var meth, ref1;
      meth = `ManifestStoreK8S.storeManifest ${manifest.name || ((ref1 = manifest.ref) != null ? ref1.name : void 0) || '(no name)'}`;
      this.logger.info(meth);
      return q.promise((resolve, reject) => {
        var err, errMsg, kukuElement;
        if (!manifest) {
          reject(new Error('Storing manifest: manifest is empty.'));
        }
        try {
          kukuElement = this.kukuModel.fromManifest(manifest);
          // console.log "**** KUKUELEMENT FROM MANIFEST: ****"
          // console.log JSON.stringify kukuElement, null, 2

          // Validate the KukuElement (throws error if validation fails)
          kukuElement.validate();
          // Make sure logger, helper, etc. are not included
          delete kukuElement.logger;
          delete kukuElement.helper;
          // @logger.debug "#{meth} - KukuElement from Manifest:
          //                #{JSON.stringify kukuElement}"
          this.logger.info(`${meth} - Created KukuElement from manifest.Storing it...`);
          return this.apiServer.saveKumoriElement(kukuElement).then(() => {
            this.logger.info(`${meth} - Element successfully saved.`);
            return resolve(manifest.name);
          }).catch(err => {
            var errMsg;
            errMsg = `Couldn't save manifest: ${err.message}`;
            this.logger.error(`${meth} - ${errMsg}`);
            return reject(new Error(errMsg));
          });
        } catch (error) {
          err = error;
          errMsg = `Couldn't save manifest: ${err.message}`;
          this.logger.error(`${meth} - ${errMsg}`);
          return reject(new Error(errMsg));
        }
      });
    }

    getManifest(manifestURN) {
      var meth;
      meth = `ManifestStoreK8S.getManifest ${manifestURN}`;
      this.logger.info(meth);
      return this.getManifestByURN(manifestURN);
    }

    getManifestByNameAndKind(manifestName, kind) {
      var meth;
      meth = `ManifestStoreK8S.getManifestByNameAndKind ${kind} ${manifestName}`;
      this.logger.info(meth);
      return q.promise((resolve, reject) => {
        var err, errMsg, plural;
        if (!manifestName) {
          reject(new Error('Getting manifest: manifestName parameter is empty.'));
          return;
        }
        if (!kind) {
          reject(new Error('Getting manifest: kind parameter is empty.'));
          return;
        }
        try {
          plural = this.kukuModel.helper.getPluralFromType(kind);
          if (plural === null) {
            errMsg = `Unable to determine KukuElement plural for type ${kind}`;
            this.logger.error(`${meth} - ERROR: ${errMsg}`);
            return reject(new Error(errMsg));
          }
          return this.apiServer.getKumoriElement(plural, manifestName).then(element => {
            var compressedManifest, manifest, ref1, ref2, ref3;
            if (element == null) {
              throw new Error("Element not found.");
            }
            // Extract original manifest from KukuElement.
            // For backwards compatibility, the manifest is searched in two
            // different places:
            // - the 'kumori/manifest' annotation (deprecated)
            // - the 'spec.kumoriManifest' property
            if (((ref1 = element.spec) != null ? ref1.kumoriManifest : void 0) != null) {
              compressedManifest = element.spec.kumoriManifest;
            } else if (((ref2 = element.metadata) != null ? (ref3 = ref2.annotations) != null ? ref3['kumori/manifest'] : void 0 : void 0) != null) {
              compressedManifest = element.metadata.annotations['kumori/manifest'];
            } else {
              throw new Error("Original manifest not found in KukuElement.");
            }
            manifest = JSON.parse(this.kukuModel.helper.decompressManifest(compressedManifest));
            return resolve(manifest);
          }).catch(err => {
            errMsg = `Couldn't get manifest for ${plural} ${manifestName}: ${err.message}`;
            this.logger.error(`${meth} - ${errMsg}`);
            return reject(new Error(errMsg));
          });
        } catch (error) {
          err = error;
          errMsg = `Couldn't get manifest for ${plural} ${manifestName}: ${err.message}`;
          this.logger.error(`${meth} - ${errMsg}`);
          return reject(new Error(errMsg));
        }
      });
    }

    getManifestByURN(manifestURN) {
      var meth;
      meth = `ManifestStoreK8S.getManifestByURN ${manifestURN}`;
      this.logger.info(meth);
      if (manifestURN === null || manifestURN === 'null') {
        this.logger.info(`${meth} - Returning mock null manifest`);
        return q(MOCK_NULL_MANIFEST);
      }
      return q.promise((resolve, reject) => {
        var domain, err, errMsg, name, plural, ref;
        if (!manifestURN) {
          reject(new Error('Getting manifest: manifestURN parameter is empty.'));
        }
        try {
          ref = this.manifestHelper.urnToRef(manifestURN);
          name = ref.name;
          domain = ref.domain;
          plural = this.kukuModel.helper.getPluralFromType(ref.kind);
          if (plural === null) {
            errMsg = `Unable to determine KukuElement plural for type ${ref.kind}`;
            this.logger.error(`${meth} - ERROR: ${errMsg}`);
            return reject(new Error(errMsg));
          }
          return this.getManifestByPluralNameDomain(plural, name, domain, null).then(manifest => {
            return resolve(manifest);
          }).catch(err => {
            var ref1;
            errMsg = `Couldn't get manifest for ${manifestURN}: ${err.message}`;
            if ((ref1 = err.message) != null ? ref1.includes('Element not found') : void 0) {
              this.logger.info(`${meth} - ${errMsg}`);
            } else {
              this.logger.error(`${meth} - ${errMsg}`);
            }
            return reject(new Error(errMsg));
          });
        } catch (error) {
          err = error;
          errMsg = `Couldn't get manifest for ${manifestURN}: ${err.message}`;
          this.logger.error(`${meth} - ${errMsg}`);
          return reject(new Error(errMsg));
        }
      });
    }

    getManifestByPluralNameDomain(plural, name, domain, owner = null) {
      var meth;
      meth = `ManifestStoreK8S.getManifestByPluralNameDomain ${plural}/${name}/${domain}`;
      this.logger.info(meth);
      return q.promise((resolve, reject) => {
        var err, errMsg, filter;
        if (!plural || !name || !domain) {
          reject(new Error('Getting manifest: any parameter is empty.'));
        }
        try {
          // Search a KukuElement of the proper type that have labels that match the
          // domain and name of the URN.
          filter = {};
          filter['kumori/name'] = this.manifestHelper.hashLabel(name);
          filter['kumori/domain'] = this.manifestHelper.hashLabel(domain);
          if (plural === 'kukudeployments') {
            plural = 'v3deployments';
          }
          return this.listElementType(plural, owner, filter).then(elements => {
            var firstKey, manifest;
            if (Object.keys(elements).length === 0) {
              throw new Error("Element not found.");
            } else if (Object.keys(elements).length > 1) {
              throw new Error("Several elements found matching criteria.");
            } else {
              // The map has only one element
              firstKey = Object.keys(elements)[0];
              manifest = elements[firstKey];
              return resolve(manifest);
            }
          }).catch(err => {
            var errMsg;
            errMsg = `Couldn't get manifest for ${plural}/${name}/${domain}: ${err.message}`;
            this.logger.info(`${meth} - ${errMsg}`);
            return reject(new Error(errMsg));
          });
        } catch (error) {
          err = error;
          errMsg = `Couldn't get manifest for ${plural}/${name}/${domain}: ${err.message}`;
          this.logger.error(`${meth} - ${errMsg}`);
          return reject(new Error(errMsg));
        }
      });
    }

    deleteManifest(manifestURN) {
      var meth;
      meth = `ManifestStoreK8S.deleteManifest ${manifestURN}`;
      this.logger.info(meth);
      return this.deleteManifestByURN(manifestURN);
    }

    deleteManifestByURN(manifestURN) {
      var meth;
      meth = `ManifestStoreK8S.deleteManifestByURN ${manifestURN}`;
      this.logger.info(meth);
      return q.promise((resolve, reject) => {
        var err, errMsg, name, plural, ref;
        if (!manifestURN) {
          reject(new Error('Deleting manifest: manifest URN is empty.'));
        }
        try {
          ref = this.manifestHelper.urnToRef(manifestURN);
          name = ref.name;
          plural = this.kukuModel.helper.getPluralFromType(ref.kind);
          if (plural === null) {
            errMsg = `Unable to determine KukuElement plural for type ${ref.kind}`;
            this.logger.error(`${meth} - ERROR: ${errMsg}`);
            return reject(new Error(errMsg));
          }
          this.logger.info(`${meth} - Deleting KukuElement ${name} (${plural})...`);
          return this.apiServer.deleteKumoriElement(plural, name).then(element => {
            this.logger.info(`${meth} - Deleted KukuElement ${name} (${plural})...`);
            return resolve(manifestURN);
          }).catch(err => {
            errMsg = `Couldn't delete manifest: ${err.message}`;
            this.logger.error(`${meth} - ${errMsg}`);
            return reject(new Error(errMsg));
          });
        } catch (error) {
          err = error;
          errMsg = `Couldn't delete manifest: ${err.message}`;
          this.logger.error(`${meth} - ${errMsg}`);
          return reject(new Error(errMsg));
        }
      });
    }

    deleteManifestByNameAndKind(name, kind) {
      var meth;
      meth = `ManifestStoreK8S.deleteManifestByNameAndKind ${kind} ${name}`;
      this.logger.info(meth);
      return q.promise((resolve, reject) => {
        var err, errMsg, plural;
        if (!name) {
          reject(new Error('Getting manifest: name parameter is empty.'));
          return;
        }
        if (!kind) {
          reject(new Error('Getting manifest: kind parameter is empty.'));
          return;
        }
        try {
          plural = this.kukuModel.helper.getPluralFromType(kind);
          if (plural === null) {
            errMsg = `Unable to determine KukuElement plural for type ${kind}`;
            this.logger.error(`${meth} - ERROR: ${errMsg}`);
            reject(new Error(errMsg));
            return;
          }
          if (plural === 'kukudeployments') {
            plural = 'v3deployments';
          }
          return this.apiServer.deleteKumoriElement(plural, name).then(element => {
            this.logger.info(`${meth} - Deleted KukuElement`);
            return resolve(name);
          }).catch(err => {
            errMsg = `Couldn't delete manifest ${kind} ${name}: ${err.message}`;
            this.logger.error(`${meth} - ${errMsg}`);
            return reject(new Error(errMsg));
          });
        } catch (error) {
          err = error;
          errMsg = `Couldn't delete manifest ${kind} ${name}: ${err.message}`;
          this.logger.error(`${meth} - ${errMsg}`);
          reject(new Error(errMsg));
        }
      });
    }

    updateManifest(manifest) {
      var meth;
      meth = `ManifestStoreK8S.updateManifest ${manifest.name}`;
      this.logger.info(meth);
      // Right now, store overwrites any previously stored manifest
      return this.storeManifest(manifest);
    }

    list(owner = null, filter = {}, includePublic = false) {
      var TYPES, allElements, meth;
      meth = `ManifestStoreK8S.list Owner: ${owner}`;
      this.logger.info(meth);
      if (owner != null && !('kumori/owner' in filter)) {
        filter['kumori/owner'] = this.manifestHelper.hashLabel(owner);
      }
      // TODO: This assumes this naming for element types is know at this level
      TYPES = ['cas', 'certificates', 'domains', 'links', 'ports', 'secrets', 'solutions', 'v3deployments', 'volumes'];
      allElements = null;
      return this.listElementTypes(TYPES, owner, filter).then(elementDict => {
        var publicFilter;
        allElements = elementDict;
        includePublic = true; // TODO: for testing
        if (includePublic) {
          publicFilter = {
            'kumori/public': "true"
          };
          return this.listElementTypes(TYPES, null, publicFilter).then(publicElementsDict => {
            var k, results, v;
            results = [];
            for (k in publicElementsDict) {
              v = publicElementsDict[k];
              // We don't care if there are duplicates since they would be the same
              results.push(allElements[k] = v);
            }
            return results;
          });
        }
      }).then(() => {
        return allElements;
      });
    }

    listElementTypes(types = [], owner = null, filter = {}) {
      var allElements, i, len, meth, promises, t;
      meth = `ManifestStoreK8S.listElementTypes Types: ${types} Owner: ${owner}`;
      this.logger.info(meth);
      if (filter == null) {
        filter = {};
      }
      if (owner != null && !('kumori/owner' in filter)) {
        filter['kumori/owner'] = this.manifestHelper.hashLabel(owner);
      }
      // List all basic KukuElements of the user or public
      allElements = {};
      promises = [];
      for (i = 0, len = types.length; i < len; i++) {
        t = types[i];
        (t => {
          return promises.push(this.listElementType(t, owner, filter).then(elements => {
            var k, v;
            // console.log "LISTED ELEMENTS:"
            for (k in elements) {
              v = elements[k];
              // console.log "   - #{k}"
              allElements[k] = v;
            }
            return true;
          }).catch(err => {
            this.logger.error(`${meth} ERROR: Couldn't get elements of type ${t}: ${err.message}`);
            return true;
          }));
        })(t);
      }
      return q.all(promises).then(() => {
        var isPublic, k, result, v;
        // Return a dictionary where the keys are the elements URNs and the values
        // are the elements metadata (owner and public)
        result = {};
        for (k in allElements) {
          v = allElements[k];
          isPublic = v.public != null ? v.public : false;
          result[k] = {
            owner: v.owner,
            public: isPublic
          };
        }
        this.logger.info(`${meth} - Finished listing all types. Total elements: ${Object.keys(result).length}`);
        return result;
      });
    }

    listElementType(type, owner = null, filter = {}) {
      var labelSelector, meth;
      meth = `ManifestStoreK8S.listElementType ${type} - OWNER: ${owner} - FILTER: ${JSON.stringify(filter)}`;
      this.logger.info(meth);
      if (owner != null && !('kumori/owner' in filter)) {
        filter['kumori/owner'] = this.manifestHelper.hashLabel(owner);
      }
      labelSelector = this.filterToLabelSelector(filter);
      return q.promise((resolve, reject) => {
        var errMsg, plural;
        plural = this.kukuModel.helper.getPluralFromType(type);
        if (plural === null) {
          errMsg = `Unable to determine KukuType plural for type ${type}`;
          this.logger.error(`${meth} - ERROR: ${errMsg}`);
          reject(new Error(errMsg));
        }
        return this.apiServer.listKumoriElements(plural, labelSelector).then(items => {
          var compressedManifest, elements, i, it, len, manifest, ref1, ref2, ref3, ref4;
          this.logger.info(`${meth} - Found ${items.length} results.`);
          elements = {};
          for (i = 0, len = items.length; i < len; i++) {
            it = items[i];
            // For backwards compatibility, the manifest is searched in two
            // different places:
            // - the 'kumori/manifest' annotation (deprecated)
            // - the 'spec.kumoriManifest' property
            if (((ref1 = it.spec) != null ? ref1.kumoriManifest : void 0) != null) {
              compressedManifest = it.spec.kumoriManifest;
            } else if (((ref2 = it.metadata) != null ? (ref3 = ref2.annotations) != null ? ref3['kumori/manifest'] : void 0 : void 0) != null) {
              compressedManifest = it.metadata.annotations['kumori/manifest'];
            } else {
              this.logger.warn(`${meth} - Found item with no ECloud manifest: ${JSON.stringify(it.metadata)}`);
              // Skip it
              continue;
            }
            manifest = JSON.parse(this.kukuModel.helper.decompressManifest(compressedManifest));
            // If object is marked for deletion, add that info to Kumori manifest
            if (((ref4 = it.metadata) != null ? ref4.deletionTimestamp : void 0) != null) {
              manifest.deletionTimestamp = it.metadata.deletionTimestamp;
            }
            if (manifest.urn != null) {
              elements[manifest.urn] = manifest;
            } else {
              elements[manifest.name] = manifest;
            }
          }
          return resolve(elements);
        }).catch(err => {
          errMsg = `Failed to list ${plural} with selector ${labelSelector}: ${err.message}`;
          this.logger.error(`${meth} - ERROR: ${errMsg}`);
          return reject(new Error(errMsg));
        });
      });
    }

    checkElement(elementURN) {
      var meth;
      meth = `ManifestStoreK8S.checkElement ${elementURN}`;
      this.logger.info(meth);
      return q.reject();
    }

    // q.promise (resolve, reject) =>
    //   if not elementURN?.length
    //     reject new Error('Getting manifest: elementURN is empty.')
    //   else
    //     manifestLocator = path.join elementURN, filename
    //     @fetcher.check manifestLocator, 'skip'
    //     .then () ->
    //       resolve()
    //     .fail (err) ->
    //       reject err
    info() {
      return `${this.type} Manifest Store: K8s config=${JSON.stringify(this.config.k8sApi)}`;
    }

    escapeLabelValue(str) {
      var LABEL_ESCAPED_CHARS, k, v;
      LABEL_ESCAPED_CHARS = {
        '@': '__arroba__'
      };
      for (k in LABEL_ESCAPED_CHARS) {
        v = LABEL_ESCAPED_CHARS[k];
        str = str.split(k).join(v);
      }
      return str;
    }

    // Takes a dictionnary and converts it to a Kubernetes LabelSelector.
    // Its support some special syntax in the filter:
    // - label exists (with any value):  <labelName>: '*'
    // - label not equals to value:      <labelName>: '!!<value>'
    filterToLabelSelector(filter = {}) {
      var cleanValue, k, labelSelector, v;
      labelSelector = '';
      if (filter != null) {
        for (k in filter) {
          v = filter[k];
          if (labelSelector !== '') {
            labelSelector += ',';
          }
          // Add filter properties to selector
          if (v === '*') {
            // Only check if label exists
            labelSelector += k;
          } else if (v.startsWith('!!')) {
            // It's a negative equality. Remove the '!!' and use '!=' operator
            cleanValue = v.substring(2);
            labelSelector += k + '!=' + this.escapeLabelValue(cleanValue);
          } else {
            labelSelector += k + '=' + this.escapeLabelValue(v);
          }
        }
      }
      return labelSelector;
    }

    // Temporary solution for dealing with the label renaming
    labelNameConverter(labelName) {
      var LABEL_DICT;
      LABEL_DICT = {
        "kumoriOwner": "kumori/owner",
        "name": "kumori/name",
        "domain": "kumori/domain"
      };
      if (labelName in LABEL_DICT) {
        console.log("");
        console.log("*********************************************************");
        console.log("*********************************************************");
        console.log(`FILTER WITH OLD LABEL: ${labelName}`);
        console.log("*********************************************************");
        console.log("*********************************************************");
        console.log("");
        return LABEL_DICT[labelName];
      } else {
        return null;
      }
    }

  };

  module.exports = ManifestStoreK8S;
}).call(undefined);