'use strict';

(function () {
  /*
  * Copyright 2022 Kumori Systems S.L.
   * Licensed under the EUPL, Version 1.2 or – as soon they
    will be approved by the European Commission - subsequent
    versions of the EUPL (the "Licence");
   * You may not use this work except in compliance with the
    Licence.
   * You may obtain a copy of the Licence at:
     https://joinup.ec.europa.eu/software/page/eupl
   * Unless required by applicable law or agreed to in
    writing, software distributed under the Licence is
    distributed on an "AS IS" basis,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
    express or implied.
   * See the Licence for the specific language governing
    permissions and limitations under the Licence.
   */
  var KIND, KukuElement, KukuLink, PLURAL, utils;

  KukuElement = require('./kuku-element');

  utils = require('../utils');

  // KukuLink resource yaml example:

  // apiVersion: kumori.systems/v1
  // kind: KukuLink
  // metadata:
  //   name: httpinbound-789-deployment-123
  //   namespace: myns
  //   labels:
  //     deployment1: httpinbound-789
  //     channel1: "inbound"
  //     deployment2: deployment-123
  //     channel2: entrypoint
  // spec:
  //   element1:
  //     deployment: httpinbound-789
  //     channel: "inbound"
  //   element2:
  //     deployment: deployment-123
  //     channel: entrypoint

  // These are inherited from super class KumoriElement:
  // GROUP       = 'kumori.systems'
  // VERSION     = 'v1'
  // NAMESPACE   = 'kumori'
  // API_VERSION = "#{GROUP}/#{VERSION}"
  KIND = 'KukuLink';

  PLURAL = 'kukulinks';

  KukuLink = class KukuLink extends KukuElement {
    // Class method to instantiate a new object with the values from an ECloud
    // link manifest.
    static fromManifest(manifest, helper) {
      var kukulink;
      // @logger.info "KukuLink.fromManifest #{manifest}"
      // console.log "KukuLink.fromManifest #{manifest}"

      // console.log "LINK MANIFEST NAME: #{manifest.name}"

      // Instantiate KukuLink
      kukulink = new KukuLink(manifest, helper);
      if (manifest.meta != null) {
        kukulink.setMeta(manifest.meta);
      }
      if (manifest.endpoints != null) {
        kukulink.setElement1(manifest.endpoints[0]);
        kukulink.setElement2(manifest.endpoints[1]);
      }
      kukulink.setManifest(manifest);
      return kukulink;
    }

    constructor(manifest, helper1) {
      var meth;
      super();
      this.helper = helper1;
      meth = 'KukuLink.constructor';
      console.log(`${meth} Name:${manifest.name}`);
      this.apiVersion = this.getApiVersion();
      this.kind = this.getKind();
      this.metadata = {
        name: manifest.name,
        namespace: this.getNamespace(),
        labels: {
          'kumori/owner': utils.getFnvHash(manifest.owner),
          deployment1: null,
          channel1: null,
          deployment2: null,
          channel2: null
        },
        annotations: {
          'kumori/owner': manifest.owner,
          'kumori/lastModification': this.helper.getTimestamp()
        }
      };
      this.spec = {
        meta: {},
        endpoints: []
      };
    }

    // element1:
    //   kind: null
    //   domain: null
    //   name: null
    //   channel: null
    // element2:
    //   kind: null
    //   domain: null
    //   name: null
    //   channel: null
    getKind() {
      return KIND;
    }

    getPlural() {
      return PLURAL;
    }

    setMeta(meta) {
      return this.spec.meta = meta;
    }

    addEnpoint(endpoint) {
      return this.spec.endpoints.push({
        kind: endpoint.kind,
        domain: endpoint.domain,
        name: endpoint.name,
        channel: endpoint.channel
      });
    }

    setElement1(endpoint) {
      this.metadata.annotations.channel1 = endpoint.channel;
      this.metadata.labels.deployment1 = endpoint.kumoriName;
      this.metadata.labels.channel1 = utils.getFnvHash(endpoint.channel);
      this.metadata.labels[endpoint.kumoriName] = 'deployment1';
      return this.addEnpoint(endpoint);
    }

    setElement2(endpoint) {
      this.metadata.annotations.channel2 = endpoint.channel;
      this.metadata.labels.deployment2 = endpoint.kumoriName;
      this.metadata.labels.channel2 = utils.getFnvHash(endpoint.channel);
      this.metadata.labels[endpoint.kumoriName] = 'deployment2';
      return this.addEnpoint(endpoint);
    }

  };

  module.exports = KukuLink;
}).call(undefined);