'use strict';

(function () {
  /*
  * Copyright 2022 Kumori Systems S.L.
   * Licensed under the EUPL, Version 1.2 or – as soon they
    will be approved by the European Commission - subsequent
    versions of the EUPL (the "Licence");
   * You may not use this work except in compliance with the
    Licence.
   * You may obtain a copy of the Licence at:
     https://joinup.ec.europa.eu/software/page/eupl
   * Unless required by applicable law or agreed to in
    writing, software distributed under the Licence is
    distributed on an "AS IS" basis,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
    express or implied.
   * See the Licence for the specific language governing
    permissions and limitations under the Licence.
   */
  var API_VERSION, GROUP, KukuElement, NAMESPACE, VERSION, utils, yaml;

  yaml = require('js-yaml');

  utils = require('../utils');

  GROUP = 'kumori.systems';

  VERSION = 'v1';

  NAMESPACE = 'kumori';

  API_VERSION = `${GROUP}/${VERSION}`;

  KukuElement = class KukuElement {
    constructor() {
      return true;
    }

    setCommonData(manifest) {
      var hash, manifestStr;
      // Calculate a string version of the manifest and its sha1 hash
      manifestStr = JSON.stringify(manifest);
      hash = this.helper.getHash(manifestStr);
      this.apiVersion = this.getApiVersion();
      this.kind = this.getKind();
      return this.metadata = {
        name: manifest.name,
        namespace: this.getNamespace(),
        labels: {
          'kumori/name': utils.getFnvHash(manifest.ref.name),
          'kumori/domain': utils.getFnvHash(manifest.ref.domain),
          'kumori/owner': utils.getFnvHash(manifest.owner)
        },
        annotations: {
          'kumori/name': manifest.ref.name,
          'kumori/domain': manifest.ref.domain,
          'kumori/owner': manifest.owner,
          'kumori/lastModification': this.helper.getTimestamp(),
          'kumori/sha': hash
        }
      };
    }

    getGroup() {
      return GROUP;
    }

    getVersion() {
      return VERSION;
    }

    getApiVersion() {
      return API_VERSION;
    }

    getNamespace() {
      return NAMESPACE;
    }

    setResourceVersion(resourceVersion) {
      return this.metadata.resourceVersion = resourceVersion;
    }

    setManifest(manifest) {
      var compressedManifest, manifestStr;
      manifestStr = JSON.stringify(manifest);
      compressedManifest = this.helper.compressManifest(manifestStr);
      return this.metadata.annotations['kumori/manifest'] = compressedManifest;
    }

    setManifestInSpec(manifest) {
      var compressedManifest, manifestStr;
      manifestStr = JSON.stringify(manifest);
      compressedManifest = this.helper.compressManifest(manifestStr);
      return this.spec.kumoriManifest = compressedManifest;
    }

    serialize(format = 'json') {
      if (format.toLowerCase() === 'json') {
        return JSON.stringify(this);
      } else if (format.toLowerCase() === 'yaml') {
        // We could also use:   yaml.safeDump this
        return yaml.dump(this);
      } else {
        return JSON.stringify(this);
      }
    }

    //#############################################################################
    //#        THE FOLLOWING METHODS CAN BE REDEFINED IN CHILD CLASSES           ##
    //#############################################################################
    validate() {}

    //#############################################################################
    //#        THE FOLLOWING METHODS MUST BE IMPLEMENTED IN CHILD CLASSES        ##
    //#############################################################################

    // Class method to instantiate a new object with the values from an ECloud
    // manifest.
    // This method can be refined in child classes
    // Its purpose is to validate any aspect of the element (e.g. the syntax of
    // its name)
    static fromManifest(manifest, helper) {
      throw new Error('NOT IMPLEMENTED IN BASE ABSTRACT CLASS');
    }

    getKind() {
      throw new Error('NOT IMPLEMENTED IN BASE ABSTRACT CLASS');
    }

    getPlural() {
      throw new Error('NOT IMPLEMENTED IN BASE ABSTRACT CLASS');
    }

  };

  module.exports = KukuElement;
}).call(undefined);