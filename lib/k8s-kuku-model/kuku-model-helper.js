'use strict';

(function () {
  /*
  * Copyright 2020 Kumori Systems S.L.
   * Licensed under the EUPL, Version 1.2 or – as soon they
    will be approved by the European Commission - subsequent
    versions of the EUPL (the "Licence");
   * You may not use this work except in compliance with the
    Licence.
   * You may obtain a copy of the Licence at:
     https://joinup.ec.europa.eu/software/page/eupl
   * Unless required by applicable law or agreed to in
    writing, software distributed under the Licence is
    distributed on an "AS IS" basis,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
    express or implied.
   * See the Licence for the specific language governing
    permissions and limitations under the Licence.
   */
  var ESCAPE_CHARS,
      KukuModelHelper,
      ManifestHelper,
      TYPES_LINK,
      TYPES_RES_CA,
      TYPES_RES_CERT,
      TYPES_RES_DOMAIN,
      TYPES_RES_PORT,
      TYPES_RES_SECRET,
      TYPES_RES_VOLUME,
      TYPES_SOLUTION,
      TYPES_V3DEPLOYMENT,
      regexElementName,
      utils,
      indexOf = [].indexOf;

  utils = require('../utils');

  ManifestHelper = require('../manifest-helper');

  // Regular expressions
  regexElementName = new RegExp('^[a-z0-9]([-a-z0-9]*[a-z0-9])?$');

  ESCAPE_CHARS = {
    '@': '__arroba__'
  };

  TYPES_SOLUTION = ['solution', 'solutions', 'kukusolution', 'kukusolutions'];

  TYPES_V3DEPLOYMENT = ['deployment', 'deployments', 'v3deployment', 'v3deployments'];

  TYPES_LINK = ['link', 'links', 'kukulink', 'kukulinks'];

  TYPES_RES_CERT = ['certificate', 'certificates', 'kukucert', 'kukucerts'];

  TYPES_RES_SECRET = ['secret', 'secrets', 'kukusecret', 'kukusecrets'];

  TYPES_RES_PORT = ['port', 'ports', 'kukuport', 'kukuports'];

  TYPES_RES_DOMAIN = ['domain', 'domains', 'kukudomain', 'kukudomains'];

  TYPES_RES_CA = ['ca', 'cas', 'kukuca', 'kukucas'];

  TYPES_RES_VOLUME = ['volume', 'volumes', 'kukuvolume', 'kukuvolumes'];

  KukuModelHelper = class KukuModelHelper {
    constructor(config = {}) {
      this.config = config;
      // @logger.info 'KukuModelHelper.constructor'
      console.log('KukuModelHelper.constructor');
      this.manifestHelper = new ManifestHelper();
    }

    // Methods for identifying Manifests (delegated on Manifest Helper)
    isSolution(manifest) {
      return this.manifestHelper.isSolution(manifest);
    }

    isKmv3Deployment(manifest) {
      return this.manifestHelper.isKmv3Deployment(manifest);
    }

    isSecret(manifest) {
      return this.manifestHelper.isSecret(manifest);
    }

    isPort(manifest) {
      return this.manifestHelper.isPort(manifest);
    }

    isVolume(manifest) {
      return this.manifestHelper.isVolume(manifest);
    }

    isLink(manifest) {
      return this.manifestHelper.isLink(manifest);
    }

    isDomain(manifest) {
      return this.manifestHelper.isDomain(manifest);
    }

    isCA(manifest) {
      return this.manifestHelper.isCA(manifest);
    }

    isCertificate(manifest) {
      return this.manifestHelper.isCertificate(manifest);
    }

    // Escape forbidden characters from username
    escapeUsername(user) {
      var k, v;
      for (k in ESCAPE_CHARS) {
        v = ESCAPE_CHARS[k];
        user = user.split(k).join(v);
      }
      return user;
    }

    getPluralFromType(type) {
      var ref, ref1, ref2, ref3, ref4, ref5, ref6, ref7, ref8;
      if (ref = type.toLowerCase(), indexOf.call(TYPES_SOLUTION, ref) >= 0) {
        return 'kukusolutions';
      } else if (ref1 = type.toLowerCase(), indexOf.call(TYPES_V3DEPLOYMENT, ref1) >= 0) {
        return 'v3deployments';
      } else if (ref2 = type.toLowerCase(), indexOf.call(TYPES_LINK, ref2) >= 0) {
        return 'kukulinks';
      } else if (ref3 = type.toLowerCase(), indexOf.call(TYPES_RES_CERT, ref3) >= 0) {
        return 'kukucerts';
      } else if (ref4 = type.toLowerCase(), indexOf.call(TYPES_RES_SECRET, ref4) >= 0) {
        return 'kukusecrets';
      } else if (ref5 = type.toLowerCase(), indexOf.call(TYPES_RES_PORT, ref5) >= 0) {
        return 'kukuports';
      } else if (ref6 = type.toLowerCase(), indexOf.call(TYPES_RES_DOMAIN, ref6) >= 0) {
        return 'kukudomains';
      } else if (ref7 = type.toLowerCase(), indexOf.call(TYPES_RES_VOLUME, ref7) >= 0) {
        return 'kukuvolumes';
      } else if (ref8 = type.toLowerCase(), indexOf.call(TYPES_RES_CA, ref8) >= 0) {
        return 'kukucas';
      } else {
        return null;
      }
    }

    compressManifest(manifestStr) {
      return utils.gzipString(manifestStr);
    }

    decompressManifest(manifestStr) {
      return utils.gunzipString(manifestStr);
    }

    isValidElementName(name) {
      // Element names are used as label values and label names (more restricted syntax)

      // Therefore, element names must meet the restrictions of the label names

      // Kubernetes labels names :
      // << DNS Label: an alphanumeric (a-z, and 0-9) string, with a maximum length
      // of 63 characters, with the - character allowed anywhere except the first
      // or last character, suitable for use as a hostname or segment in a domain
      // name. There are two variations:
      // - rfc1035: based on regexp [a-z]([-a-z0-9]*[a-z0-9])?
      // - rfc1123: based on regexp [a-z0-9]([-a-z0-9]*[a-z0-9])?  >>

      // TODO (01/2022): Review the above comments and regexes, since Kubernetes
      // also accepts underscores (_) and dots (.) in the labels names and values.
      if (name == null) {
        return false;
      }
      if (name.length > 63) {
        return false;
      }
      if (!name.match(regexElementName)) {
        return false;
      }
      return true;
    }

    // Returns a timestamp string in the format "2022-01-26T18:50:22Z"
    getTimestamp() {
      return utils.getTimestamp();
    }

    // Returns a hash of the provided string
    getHash(str) {
      return utils.getHash(str);
    }

  };

  module.exports = KukuModelHelper;
}).call(undefined);