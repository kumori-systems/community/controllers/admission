'use strict';

(function () {
  /*
  * Copyright 2022 Kumori Systems S.L.
   * Licensed under the EUPL, Version 1.2 or – as soon they
    will be approved by the European Commission - subsequent
    versions of the EUPL (the "Licence");
   * You may not use this work except in compliance with the
    Licence.
   * You may obtain a copy of the Licence at:
     https://joinup.ec.europa.eu/software/page/eupl
   * Unless required by applicable law or agreed to in
    writing, software distributed under the Licence is
    distributed on an "AS IS" basis,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
    express or implied.
   * See the Licence for the specific language governing
    permissions and limitations under the Licence.
   */
  var KIND, KukuElement, KukuV3Deployment, PLURAL, _;

  _ = require('lodash');

  KukuElement = require('./kuku-element');

  // KukuV3Deployment resource yaml example:

  //   apiVersion: kumori.systems/v1
  //   kind: V3Deployment
  //   metadata:
  //     name: hazelcast-deployment
  //     namespace: kumori
  //     labels:
  //      'kumori/name': hazelcast-deployment
  //      'kumori/domain': integrations.kumori.systems
  //      'kumori/owner': juanjo__arroba__kumori.systems
  //   spec:
  //     spec: ...
  //     ref: ...
  //     description: ...
  //     scope: ...

  // These are inherited from super class KumoriElement:
  // GROUP       = 'kumori.systems'
  // VERSION     = 'v1'
  // NAMESPACE   = 'kumori'
  // API_VERSION = "#{GROUP}/#{VERSION}"
  KIND = 'V3Deployment';

  PLURAL = 'v3deployments';

  KukuV3Deployment = class KukuV3Deployment extends KukuElement {
    static fromManifest(manifest, helper) {
      var kukuv3depl, ref, resID, resInfo, usedResources;
      // Create a list of used resources to add labels later on.
      // Also remove the resource ID added by Admission previously
      usedResources = {};
      if (manifest.usedResources != null) {
        ref = manifest.usedResources;
        for (resID in ref) {
          resInfo = ref[resID];
          usedResources[resID] = 'resource-in-use';
        }
        delete manifest.usedResources;
      }
      kukuv3depl = new KukuV3Deployment(manifest, helper);
      // Add a label for each resource used by this deployment
      kukuv3depl.addResourceLabels(usedResources);
      return kukuv3depl;
    }

    constructor(manifest, helper1) {
      var meth, tmpManifest;
      super();
      this.helper = helper1;
      meth = 'KukuV3Deployment.constructor';
      console.log(`${meth} ID: ${manifest.name} - Domain:${manifest.ref.domain} - Name:${manifest.ref.name}`);
      this.setCommonData(manifest);
      // Set type specific stuff
      this.metadata.annotations['kumori/comment'] = manifest.comment;
      // Remove name and nickname properties before creating the Kuku object
      tmpManifest = _.cloneDeep(manifest);
      delete tmpManifest.name;
      delete tmpManifest.owner;
      delete tmpManifest.urn;
      delete tmpManifest.comment;
      this.spec = tmpManifest;
    }

    getKind() {
      return KIND;
    }

    getPlural() {
      return PLURAL;
    }

    addResourceLabels(usedResources) {
      var k, results, v;
      results = [];
      for (k in usedResources) {
        v = usedResources[k];
        results.push(this.metadata.labels[k] = v);
      }
      return results;
    }

    validate() {
      var name;
      // This name must be a valid element name.
      name = this.metadata.labels['kumori/name'];
      if (!this.helper.isValidElementName(name)) {
        throw new Error(`Deployment name '${name}' is not a valid name.`);
      }
    }

  };

  module.exports = KukuV3Deployment;
}).call(undefined);