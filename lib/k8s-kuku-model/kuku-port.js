'use strict';

(function () {
  /*
  * Copyright 2022 Kumori Systems S.L.
   * Licensed under the EUPL, Version 1.2 or – as soon they
    will be approved by the European Commission - subsequent
    versions of the EUPL (the "Licence");
   * You may not use this work except in compliance with the
    Licence.
   * You may obtain a copy of the Licence at:
     https://joinup.ec.europa.eu/software/page/eupl
   * Unless required by applicable law or agreed to in
    writing, software distributed under the Licence is
    distributed on an "AS IS" basis,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
    express or implied.
   * See the Licence for the specific language governing
    permissions and limitations under the Licence.
   */
  var KIND, KukuElement, KukuPort, PLURAL;

  KukuElement = require('./kuku-element');

  // KukuPort resource yaml example:

  // apiVersion: kumori.systems/v1
  // kind: KukuPort
  // metadata:
  //   annotations:
  //     manifest: <original compressed manifest>
  //     sha: a07228f256ef4c3220560ff04684e8a71f6217d4
  //   labels:
  //     'kumori/name': my9031port
  //     'kumori/domain': myuser.examples
  //     'kumori/owner': myuser__arroba__kumori.cloud
  //   name: kres-191345-5d1e9aa1
  //   namespace: kumori
  // spec:
  //   port: 9031

  // These are inherited from super class KumoriElement:
  // GROUP       = 'kumori.systems'
  // VERSION     = 'v1'
  // NAMESPACE   = 'kumori'
  // API_VERSION = "#{GROUP}/#{VERSION}"
  KIND = 'KukuPort';

  PLURAL = 'kukuports';

  KukuPort = class KukuPort extends KukuElement {
    // Class method to instantiate a new object with the values from an ECloud
    // port resource manifest.
    static fromManifest(manifest, helper) {
      var kukuport;
      kukuport = new KukuPort(manifest, helper);
      if (manifest.public != null && manifest.public) {
        kukuport.setPublic(true);
      } else {
        kukuport.setPublic(false);
      }
      kukuport.setManifest(manifest);
      return kukuport;
    }

    constructor(manifest, helper1) {
      var externalPort, internalPort, meth;
      super();
      this.helper = helper1;
      meth = 'KukuPort.constructor';
      console.log(`${meth} ID: ${manifest.name} - Domain:${manifest.ref.domain} - Name:${manifest.ref.name}`);
      // Remove internalPort property added previously by Admission, but keep its
      // value.
      externalPort = manifest.description.port;
      internalPort = manifest.description.internalPort;
      delete manifest.description.internalPort;
      // Temporary tweak: replace 'port' property with the internal port value,
      // And add a new property 'externalPort' in the KukuPort object spec.
      manifest.description.port = internalPort;
      manifest.description.externalPort = externalPort;
      this.setCommonData(manifest);
      this.spec = manifest.description;
    }

    getKind() {
      return KIND;
    }

    getPlural() {
      return PLURAL;
    }

    setPublic(isPublic) {
      // Note that boolean is converted to string since it's added to a K8s label
      return this.metadata.labels['kumori/public'] = `${isPublic}`;
    }

    validate() {
      var name;
      // Label "name" contains the manifest.json.ref.name.
      // This name must be a valid element name.
      name = this.metadata.labels['kumori/name'];
      if (!this.helper.isValidElementName(name)) {
        throw new Error(`Port name '${name}' is not a valid name.`);
      }
    }

  };

  module.exports = KukuPort;
}).call(undefined);