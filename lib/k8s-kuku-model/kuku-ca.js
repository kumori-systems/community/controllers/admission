'use strict';

(function () {
  /*
  * Copyright 2022 Kumori Systems S.L.
   * Licensed under the EUPL, Version 1.2 or – as soon they
    will be approved by the European Commission - subsequent
    versions of the EUPL (the "Licence");
   * You may not use this work except in compliance with the
    Licence.
   * You may obtain a copy of the Licence at:
     https://joinup.ec.europa.eu/software/page/eupl
   * Unless required by applicable law or agreed to in
    writing, software distributed under the Licence is
    distributed on an "AS IS" basis,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
    express or implied.
   * See the Licence for the specific language governing
    permissions and limitations under the Licence.
   */
  var KIND, KukuCA, KukuElement, PLURAL;

  KukuElement = require('./kuku-element');

  // KukuCA resource yaml example:

  // apiVersion: kumori.systems/v1
  // kind: KukuCa
  // metadata:
  //   name: juanjoca-123
  //   namespace: myns
  //   labels:
  //     'kumori/name': juanjoca
  //     'kumori/domain': juanjo.kumori.systems
  //     'kumori/owner': juanjo__arroba__kumori.systems
  // spec:
  //   ca: H4sIAAAA...shUAAA==

  // These are inherited from super class KumoriElement:
  // GROUP       = 'kumori.systems'
  // VERSION     = 'v1'
  // NAMESPACE   = 'kumori'
  // API_VERSION = "#{GROUP}/#{VERSION}"
  KIND = 'KukuCa';

  PLURAL = 'kukucas';

  KukuCA = class KukuCA extends KukuElement {
    // Class method to instantiate a new object with the values from an ECloud
    // CA resource manifest.
    static fromManifest(manifest, helper) {
      var kukuca;
      kukuca = new KukuCA(manifest, helper);
      if (manifest.public != null && manifest.public) {
        kukuca.setPublic(true);
      } else {
        kukuca.setPublic(false);
      }
      // kukuca.setManifest manifest
      kukuca.setManifestInSpec(manifest);
      return kukuca;
    }

    constructor(manifest, helper1) {
      var meth, ref;
      super();
      this.helper = helper1;
      meth = 'KukuCA.constructor';
      console.log(`${meth} ID: ${manifest.name} - Domain:${manifest.ref.domain} - Name:${manifest.ref.name}`);
      this.setCommonData(manifest);
      this.spec = {};
      if (((ref = manifest.description) != null ? ref.ca : void 0) != null) {
        this.spec.ca = manifest.description.ca;
      }
    }

    getKind() {
      return KIND;
    }

    getPlural() {
      return PLURAL;
    }

    setPublic(isPublic) {
      // Note that boolean is converted to string since it's added to a K8s label
      return this.metadata.labels['kumori/public'] = `${isPublic}`;
    }

    validate() {
      var name;
      // This name must be a valid element name.
      name = this.metadata.labels['kumori/name'];
      if (!this.helper.isValidElementName(name)) {
        throw new Error(`CA name '${name}' is not a valid name.`);
      }
    }

  };

  module.exports = KukuCA;
}).call(undefined);