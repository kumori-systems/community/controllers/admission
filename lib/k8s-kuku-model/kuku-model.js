'use strict';

(function () {
  /*
  * Copyright 2020 Kumori Systems S.L.
   * Licensed under the EUPL, Version 1.2 or – as soon they
    will be approved by the European Commission - subsequent
    versions of the EUPL (the "Licence");
   * You may not use this work except in compliance with the
    Licence.
   * You may obtain a copy of the Licence at:
     https://joinup.ec.europa.eu/software/page/eupl
   * Unless required by applicable law or agreed to in
    writing, software distributed under the Licence is
    distributed on an "AS IS" basis,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
    express or implied.
   * See the Licence for the specific language governing
    permissions and limitations under the Licence.
   */
  var KukuCA, KukuCert, KukuDomain, KukuLink, KukuModel, KukuModelHelper, KukuPort, KukuSecret, KukuSolution, KukuV3Deployment, KukuVolume;

  KukuCA = require('./kuku-ca');

  KukuCert = require('./kuku-cert');

  KukuLink = require('./kuku-link');

  KukuSecret = require('./kuku-secret');

  KukuSolution = require('./kuku-solution');

  KukuPort = require('./kuku-port');

  KukuV3Deployment = require('./kuku-kmv3-deployment');

  KukuDomain = require('./kuku-domain');

  KukuVolume = require('./kuku-volume');

  KukuModelHelper = require('./kuku-model-helper');

  KukuModel = class KukuModel {
    constructor(config = {}) {
      this.config = config;
      this.logger.info('KukuModel.constructor');
      if (this.logger == null) {
        this.logger = {};
        this.logger.error = console.log;
        this.logger.warn = console.log;
        this.logger.info = console.log;
        this.logger.debug = console.log;
        this.logger.silly = console.log;
      }
      this.logger.info('KukuModel.constructor');
      this.helper = new KukuModelHelper();
    }

    // Instantiate a new KukuModel instance from an ECloud manifest.
    fromManifest(manifest) {
      var errMsg, meth;
      meth = 'KukuModel.fromManifest';
      this.logger.info(`${meth}`);
      if (this.helper.isSolution(manifest)) {
        // @logger.info "#{meth} Create KukuSolution of #{JSON.stringify manifest}"
        this.logger.info(`${meth} Create KukuSolution of ${JSON.stringify(manifest.ref || {})}`);
        return KukuSolution.fromManifest(manifest, this.helper);
      } else if (this.helper.isKmv3Deployment(manifest)) {
        // @logger.info "#{meth} Create KukuV3Deployment of #{JSON.stringify manifest}"
        this.logger.info(`${meth} Create KukuV3Deployment of ${JSON.stringify(manifest.ref || {})}`);
        return KukuV3Deployment.fromManifest(manifest, this.helper);
      } else if (this.helper.isLink(manifest)) {
        // @logger.info "#{meth} Create KukuLink of #{JSON.stringify manifest}"
        this.logger.info(`${meth} Create KukuLink of ${JSON.stringify(manifest.ref || {})}`);
        return KukuLink.fromManifest(manifest, this.helper);
      } else if (this.helper.isDomain(manifest)) {
        // @logger.info "#{meth} Create KukuDomain of #{JSON.stringify manifest}"
        this.logger.info(`${meth} Create KukuDomain of ${JSON.stringify(manifest.ref || {})}`);
        return KukuDomain.fromManifest(manifest, this.helper);
      } else if (this.helper.isCertificate(manifest)) {
        // @logger.info "#{meth} Create KukuCert of #{JSON.stringify manifest}"
        this.logger.info(`${meth} Create KukuCert of ${JSON.stringify(manifest.ref || {})}`);
        return KukuCert.fromManifest(manifest, this.helper);
      } else if (this.helper.isSecret(manifest)) {
        // @logger.info "#{meth} Create KukuSecret of #{JSON.stringify manifest}"
        this.logger.info(`${meth} Create KukuSecret of ${JSON.stringify(manifest.ref || {})}`);
        return KukuSecret.fromManifest(manifest, this.helper);
      } else if (this.helper.isPort(manifest)) {
        // @logger.info "#{meth} Create KukuPort of #{JSON.stringify manifest}"
        this.logger.info(`${meth} Create KukuPort of ${JSON.stringify(manifest.ref || {})}`);
        return KukuPort.fromManifest(manifest, this.helper);
      } else if (this.helper.isVolume(manifest)) {
        // @logger.info "#{meth} Create KukuVolume of #{JSON.stringify manifest}"
        this.logger.info(`${meth} Create KukuVolume of ${JSON.stringify(manifest.ref || {})}`);
        return KukuVolume.fromManifest(manifest, this.helper);
      } else if (this.helper.isCA(manifest)) {
        // @logger.info "#{meth} Create KukuCA of #{JSON.stringify manifest}"
        this.logger.info(`${meth} Create KukuCA of ${JSON.stringify(manifest.ref || {})}`);
        return KukuCA.fromManifest(manifest, this.helper);
      } else {
        errMsg = `Element type not supported (${manifest.spec})`;
        this.logger.error(`${meth} - ${errMsg}`);
        throw new Error(errMsg);
      }
    }

  };

  module.exports = KukuModel;
}).call(undefined);