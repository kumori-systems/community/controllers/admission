'use strict';

(function () {
  /*
  * Copyright 2022 Kumori Systems S.L.
   * Licensed under the EUPL, Version 1.2 or – as soon they
    will be approved by the European Commission - subsequent
    versions of the EUPL (the "Licence");
   * You may not use this work except in compliance with the
    Licence.
   * You may obtain a copy of the Licence at:
     https://joinup.ec.europa.eu/software/page/eupl
   * Unless required by applicable law or agreed to in
    writing, software distributed under the Licence is
    distributed on an "AS IS" basis,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
    express or implied.
   * See the Licence for the specific language governing
    permissions and limitations under the Licence.
   */
  var KIND, KukuCert, KukuElement, PLURAL;

  KukuElement = require('./kuku-element');

  // KukuCert resource yaml example:

  // apiVersion: kumori.systems/v1
  // kind: KukuCert
  // metadata:
  //   name: juanjocert-456
  //   namespace: kumoriingress
  //   labels:
  //     domain: juanjo.kumori.systems
  //     name: juanjocert
  //     version: null
  // data:
  //   domain: juanjo.test.kumori.cloud
  //   cert: "LS0t...S0tCg=="
  //   key: "LS0t...S0tLS0K"

  // These are inherited from super class KumoriElement:
  // GROUP       = 'kumori.systems'
  // VERSION     = 'v1'
  // NAMESPACE   = 'kumori'
  // API_VERSION = "#{GROUP}/#{VERSION}"
  KIND = 'KukuCert';

  PLURAL = 'kukucerts';

  KukuCert = class KukuCert extends KukuElement {
    // Class method to instantiate a new object with the values from an ECloud
    // cert resource manifest.
    static fromManifest(manifest, helper) {
      var kukucert, ref, ref1, ref2;
      kukucert = new KukuCert(manifest, helper);
      if (((ref = manifest.description) != null ? ref.domain : void 0) != null) {
        kukucert.setDomain(manifest.description.domain);
      }
      if (((ref1 = manifest.description) != null ? ref1.cert : void 0) != null) {
        kukucert.setCert(manifest.description.cert);
      }
      if (((ref2 = manifest.description) != null ? ref2.key : void 0) != null) {
        kukucert.setKey(manifest.description.key);
      }
      if (manifest.public != null && manifest.public) {
        kukucert.setPublic(true);
      } else {
        kukucert.setPublic(false);
      }
      kukucert.setManifest(manifest);
      return kukucert;
    }

    constructor(manifest, helper1) {
      var meth;
      super();
      this.helper = helper1;
      meth = 'KukuCert.constructor';
      console.log(`${meth} ID: ${manifest.name} - Domain:${manifest.ref.domain} - Name:${manifest.ref.name}`);
      this.setCommonData(manifest);
      this.data = {
        domain: null,
        cert: null,
        key: null
      };
    }

    getKind() {
      return KIND;
    }

    getPlural() {
      return PLURAL;
    }

    setDomain(domain) {
      return this.data.domain = domain;
    }

    setCert(cert) {
      return this.data.cert = cert;
    }

    setKey(key) {
      return this.data.key = key;
    }

    setPublic(isPublic) {
      // Note that boolean is converted to string since it's added to a K8s label
      return this.metadata.labels['kumori/public'] = `${isPublic}`;
    }

    validate() {
      var name;
      // Label "name" contains the manifest.json.ref.name.
      // This name must be a valid element name.
      name = this.metadata.labels['kumori/name'];
      if (!this.helper.isValidElementName(name)) {
        throw new Error(`Certificate name '${name}' is not a valid name.`);
      }
    }

  };

  module.exports = KukuCert;
}).call(undefined);