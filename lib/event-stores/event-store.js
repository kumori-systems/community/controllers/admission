"use strict";

(function () {
  /*
  * Copyright 2022 Kumori Systems S.L.
   * Licensed under the EUPL, Version 1.2 or – as soon they
    will be approved by the European Commission - subsequent
    versions of the EUPL (the "Licence");
   * You may not use this work except in compliance with the
    Licence.
   * You may obtain a copy of the Licence at:
     https://joinup.ec.europa.eu/software/page/eupl
   * Unless required by applicable law or agreed to in
    writing, software distributed under the Licence is
    distributed on an "AS IS" basis,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
    express or implied.
   * See the Licence for the specific language governing
    permissions and limitations under the Licence.
   */
  var EventStore;

  // Abstract class representing a platform Event Store
  EventStore = class EventStore {
    constructor(type) {
      this.type = type;
    }

    // Must return a promise
    init() {}

    // Must return a promise
    terminate() {}

    // Search events in the store based on the provided filters and the client
    // configuration.

    // Filter parameters:

    // - namespace
    // - deployment (internal ID)
    // - fromDate (a Nodejs date)
    // - toDate  (a Nodejs date)

    // Must return a promise, fulfilled with the event list
    getDeploymentEvents(namespace, deploymentID, fromDate, toDate) {}

  };

  module.exports = EventStore;
}).call(undefined);