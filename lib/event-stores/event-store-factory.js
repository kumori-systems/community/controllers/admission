'use strict';

(function () {
  /*
  * Copyright 2022 Kumori Systems S.L.
   * Licensed under the EUPL, Version 1.2 or – as soon they
    will be approved by the European Commission - subsequent
    versions of the EUPL (the "Licence");
   * You may not use this work except in compliance with the
    Licence.
   * You may obtain a copy of the Licence at:
     https://joinup.ec.europa.eu/software/page/eupl
   * Unless required by applicable law or agreed to in
    writing, software distributed under the Licence is
    distributed on an "AS IS" basis,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
    express or implied.
   * See the Licence for the specific language governing
    permissions and limitations under the Licence.
   */
  var EventStoreElasticsearch, EventStoreFactory, EventStoreK8s;

  EventStoreK8s = require('./event-store-k8s');

  EventStoreElasticsearch = require('./event-store-elasticsearch');

  EventStoreFactory = class EventStoreFactory {
    // Static method to create new Manifest Stores
    static create(type, options) {
      switch (type) {
        case 'elasticsearch':
          return new EventStoreElasticsearch(options);
        case 'k8s':
          return new EventStoreK8s(options);
        default:
          throw new Error(`Invalid EventStore type: '${type}'`);
      }
    }

  };

  module.exports = EventStoreFactory;
}).call(undefined);