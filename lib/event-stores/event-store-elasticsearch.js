'use strict';

(function () {
  /*
  * Copyright 2022 Kumori Systems S.L.
   * Licensed under the EUPL, Version 1.2 or – as soon they
    will be approved by the European Commission - subsequent
    versions of the EUPL (the "Licence");
   * You may not use this work except in compliance with the
    Licence.
   * You may obtain a copy of the Licence at:
     https://joinup.ec.europa.eu/software/page/eupl
   * Unless required by applicable law or agreed to in
    writing, software distributed under the Licence is
    distributed on an "AS IS" basis,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
    express or implied.
   * See the Licence for the specific language governing
    permissions and limitations under the Licence.
   */
  var Client, EventStore, EventStoreElasticsearch, q;

  q = require('q');

  Client = require('@elastic/elasticsearch').Client;

  EventStore = require('./event-store');

  // Expected configuration options:
  //   options:
  //     serverURL: 'http://elasticsearch-jferrer.test.kumori.cloud:9200/'
  //     username: ''
  //     password: ''
  //     index: 'kube-events-*'
  //     maxResults: 200
  EventStoreElasticsearch = class EventStoreElasticsearch extends EventStore {
    constructor(options) {
      super('elasticsearch');
      this.index = options.index;
      this.maxResults = options.maxResults;
      this.serverURL = options.serverURL;
      this.username = options.username || '';
      this.password = options.password || '';
      this.clientOptions = {
        node: this.serverURL
      };
      if (this.username !== '' && this.password !== '') {
        this.clientOptions.auth = {
          username: this.username,
          password: this.password
        };
      }
      this.client = null;
    }

    //#############################################################################
    //#                            PUBLIC METHODS                                ##
    //#############################################################################

    // Returns a promise
    init() {
      var meth;
      meth = 'EventStoreElasticsearch.init';
      return q.promise(async (resolve, reject) => {
        var err, errMsg;
        try {
          // Initialize the Elasticsearch client
          this.client = new Client(this.clientOptions);
          // Try to refresh the indices
          await this.client.indices.refresh({
            index: this.index
          });
          return resolve();
        } catch (error) {
          err = error;
          errMsg = `Couldn't initialize Elasticsearch Event Store: ${err.message || err}`;
          this.logger.error(`${meth} - ERROR: ${errMsg}`);
          // Promise was rejected but was changed as part of ticket 1323, to allow
          // users to continue managing deployments even if Elasticsearch is down.
          return resolve();
        }
      });
    }

    // Returns a promise
    terminate() {
      return q.promise(async (resolve, reject) => {
        var err, errMsg;
        try {
          // Close Elasticsearch client
          await this.client.close();
          return resolve();
        } catch (error) {
          err = error;
          errMsg = `Couldn't close Elasticsearch Event Store: ${err.message || err}`;
          return reject(new Error(err));
        }
      });
    }

    // Get events from the store based on the provided filters and the client
    // configuration.
    // Filter parameters:
    // - namespace
    // - deployment (internal ID)
    // - fromDate  (formatted as "2021-05-17T09:00:00Z")
    // - toDate  (formatted as "2021-05-17T09:00:00Z")
    getDeploymentEvents(namespace, deploymentID, fromDate, toDate) {
      var meth, query;
      meth = `EventStoreElasticsearch.getDeploymentEvents deploymentID: ${deploymentID} from: ${fromDate} to: ${toDate}`;
      this.logger.info(`${meth}`);
      // Date must follow ISO 8601 format. In Nodejs: Date().toISOString()

      // Example:
      // - fromDate = "2021-05-17T09:05:00.136Z"
      // - toDate   = "2021-05-17T09:05:15.136Z"

      // By default, search events of the last two days (today and yesterday):
      // - greater or equal than yesterday
      // - lower or equal than today
      if (fromDate == null) {
        fromDate = "now-1d/d";
      }
      if (toDate == null) {
        toDate = "now/d";
      }
      query = {
        bool: {
          // All clauses inside the 'must' array must match
          must: [{
            bool: {
              // Any clause inside the 'should' array can match
              should: [{
                // Elasticsearch tokenizes by word separators including '-'
                // As a workaround, we will just use 'AND' operator by default, so
                // all tokens are mandatory.
                match: {
                  "involvedObject.labels.owner": {
                    query: deploymentID,
                    operator: "AND"
                  }
                }
              }, {
                match: {
                  "involvedObject.labels.kumori/deployment.id": {
                    query: deploymentID,
                    operator: "AND"
                  }
                }
              }, {
                match: {
                  "involvedObject.name": {
                    query: deploymentID,
                    operator: "AND"
                  }
                }
              }]
            }
          }, {
            // Filter by events lastTimestamp (for now, this might be reconsidered)
            range: {
              lastTimestamp: {
                gte: fromDate,
                lte: toDate
              }
            }
          }]
        }
      };
      return this.runQuery(query).then(eventList => {
        return this.cleanEvents(eventList);
      });
    }

    // Get events from the store based on the provided filters and the client
    // configuration.
    // Filter parameters:
    // - namespace
    // - solution (internal ID)
    // - fromDate  (formatted as "2021-05-17T09:00:00Z")
    // - toDate  (formatted as "2021-05-17T09:00:00Z")
    getSolutionEvents(namespace, solutionID, fromDate, toDate) {
      var meth, query;
      meth = `EventStoreElasticsearch.getDeploymentEvents solutionID: ${solutionID} from: ${fromDate} to: ${toDate}`;
      this.logger.info(`${meth}`);
      // Date must follow ISO 8601 format. In Nodejs: Date().toISOString()

      // Example:
      // - fromDate = "2021-05-17T09:05:00.136Z"
      // - toDate   = "2021-05-17T09:05:15.136Z"

      // By default, search events of the last two days (today and yesterday):
      // - greater or equal than yesterday
      // - lower or equal than today
      if (fromDate == null) {
        fromDate = "now-1d/d";
      }
      if (toDate == null) {
        toDate = "now/d";
      }
      query = {
        bool: {
          // All clauses inside the 'must' array must match
          must: [{
            bool: {
              // Any clause inside the 'should' array can match
              should: [{
                // Elasticsearch tokenizes by word separators including '-'
                // As a workaround, we will just use 'AND' operator by default, so
                // all tokens are mandatory.
                match: {
                  "involvedObject.labels.owner": {
                    query: solutionID,
                    operator: "AND"
                  }
                }
              }, {
                match: {
                  "involvedObject.labels.kumori/solution.id": {
                    query: solutionID,
                    operator: "AND"
                  }
                }
              }, {
                match: {
                  "involvedObject.name": {
                    query: solutionID,
                    operator: "AND"
                  }
                }
              }]
            }
          }, {
            // Filter by events lastTimestamp (for now, this might be reconsidered)
            range: {
              lastTimestamp: {
                gte: fromDate,
                lte: toDate
              }
            }
          }]
        }
      };
      return this.runQuery(query).then(eventList => {
        return this.cleanEvents(eventList);
      });
    }

    //#############################################################################
    //#                           PRIVATE METHODS                                ##
    //#############################################################################
    runQuery(query) {
      var meth;
      meth = "EventStoreElasticsearch.runQuery";
      this.logger.debug(`${meth} query: ${JSON.stringify(query)}`);
      return q.promise(async (resolve, reject) => {
        var err, errMsg, ref, ref1, ref2, ref3, ref4, ref5, ref6, response, searchOptions, totalValues;
        try {
          // Refresh indices
          this.logger.debug(`${meth} Refreshing indices...`);
          await this.client.indices.refresh({
            index: 'kube-events-*'
          });
          // Sort value: we will sort by 'creationTimestamp' in descending order,
          // to make sure that if there are more than 'size' results, the most
          // recent ones are returned.

          // NOTE: In Kubernetes when an Event is updated ('count' is incremented
          //       and 'lastTimestamp' is updated) the 'creationTimestamp' property
          //       is also updated.
          searchOptions = {
            index: this.index,
            size: this.maxResults,
            sort: ["metadata.creationTimestamp:desc"],
            body: {
              query: query
            }
          };
          this.logger.debug(`${meth} Performing search with options: ${JSON.stringify(searchOptions)}`);
          response = await this.client.search(searchOptions);
          this.logger.debug(`${meth} Search request returned.`);
          if (response.statusCode != null && response.statusCode !== 200) {
            errMsg = `Query returned HTTP Status code: ${response.statusCode}`;
            this.logger.error(`${meth} ERROR: ${errMsg} ${JSON.stringify(response || {})}`);
            return reject(new Error(Error(errMsg)));
          }
          if (((ref = response.body) != null ? (ref1 = ref.hits) != null ? ref1.hits : void 0 : void 0) == null) {
            errMsg = "Query response contains no 'hits' sections.";
            this.logger.error(`${meth} ERROR: ${errMsg} ${JSON.stringify(response || {})}`);
            return reject(new Error(Error(errMsg)));
          }
          if ((response != null ? (ref2 = response.body) != null ? (ref3 = ref2.hits) != null ? (ref4 = ref3.total) != null ? ref4.value : void 0 : void 0 : void 0 : void 0) != null) {
            totalValues = response.body.hits.total.value;
          } else if ((response != null ? (ref5 = response.body) != null ? (ref6 = ref5.hits) != null ? ref6.total : void 0 : void 0 : void 0) != null) {
            totalValues = response.body.hits.total;
          } else {
            totalValues = '[notAvailable]';
          }
          this.logger.debug(`${meth} Query found ${totalValues} results (returned ${response.body.hits.hits.length}).`);
          return resolve(response.body.hits.hits);
        } catch (error) {
          err = error;
          this.logger.error(`${meth} ERROR: Unexpected error searching events: ${JSON.stringify(err || {})}`);
          // Promise was rejected but was changed as part of ticket 1323, to allow
          // users to continue managing deployments even if Elasticsearch is down.
          return resolve([]);
        }
      });
    }

    cleanEvents(rawList) {
      var cleanEvents, i, len, rawEvt;
      cleanEvents = [];
      for (i = 0, len = rawList.length; i < len; i++) {
        rawEvt = rawList[i];
        cleanEvents.push(rawEvt['_source']);
      }
      return cleanEvents;
    }

  };

  module.exports = EventStoreElasticsearch;
}).call(undefined);