'use strict';

(function () {
  /*
  * Copyright 2022 Kumori Systems S.L.
   * Licensed under the EUPL, Version 1.2 or – as soon they
    will be approved by the European Commission - subsequent
    versions of the EUPL (the "Licence");
   * You may not use this work except in compliance with the
    Licence.
   * You may obtain a copy of the Licence at:
     https://joinup.ec.europa.eu/software/page/eupl
   * Unless required by applicable law or agreed to in
    writing, software distributed under the Licence is
    distributed on an "AS IS" basis,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
    express or implied.
   * See the Licence for the specific language governing
    permissions and limitations under the Licence.
   */
  var JsonParser, NullParser, ProxyLogger, WinstonLogger, WinstonLogstash, configureLogger, defaultparser, errors, generateId, getDefaultParser, getLogger, helper, inject, q, setDefaultParser, setLogger, setLoggerOwner, setParser, setProperty, uuid;

  q = require('q');

  uuid = require('uuid');

  helper = require('./helper');

  errors = require('./errors');

  JsonParser = require('./json-parser');

  NullParser = require('./null-parser');

  WinstonLogger = require('./winston-logger');

  ProxyLogger = require('./proxy-logger');

  WinstonLogstash = require('./winston-logstash');

  // Initial default parser.
  defaultparser = new JsonParser();

  // Get (and create if necessary) the singleton-instance of logger

  getLogger = function (clazzOrModuleName, loggerId) {
    return new ProxyLogger(WinstonLogger.getLogger(loggerId), clazzOrModuleName);
  };

  // Set the owner of the singleton logger, a metadata fixed a singleton-logger
  // level.
  // Owner is a meta-data fixed a singleton-logger level. Represents the PROCESS
  // (slap-agent, router-agent, a component instance...) that create

  setLoggerOwner = function (owner) {
    if (typeof owner === 'string') {
      return WinstonLogger.getLogger().addFixedMetaData('owner', owner);
    }
  };

  // Inject the `logger` property to the given classes or objects.

  // Parameters:
  // * `items`: array of classes/objects where the default logger will be injected.
  // * `singletonLogger`: (optional) winston-logger instance to use (otherwise,
  //                      a factory will be used)
  inject = function (items, singletonLogger) {
    return helper.inject(items, singletonLogger);
  };

  // Inject method - old name

  setLogger = function (items, singletonLogger) {
    return helper.inject(items, singletonLogger);
  };

  // Sets/changes a configuration for logger

  configureLogger = function (config) {
    var singletonLogger;
    singletonLogger = WinstonLogger.getLogger();
    return singletonLogger.reconfigure(config);
  };

  // Gets the parser injected to classes.

  // Returns: the parser injected to classes.
  getDefaultParser = function () {
    return defaultparser;
  };

  // Sets the parser injected to classes.

  // Parameters:
  // * `parser`: the new parser injected to classes.
  setDefaultParser = function (parser) {
    return defaultparser = parser;
  };

  // Inject the `parser` property to the given classes or objects. The initial
  // value will be the default parser.

  // Parameters:
  // * `items`: array of classes/objects where the default parser will be injected.
  setParser = function (items) {
    var i, item, items2, len;
    if (!(items instanceof Array)) {
      throw new Error(errors.ARRAY_EXPECTED);
    }
    // Avoid errors when calling setParser twice
    items2 = [];
    for (i = 0, len = items.length; i < len; i++) {
      item = items[i];
      if (typeof item === 'function' && !item.prototype.hasOwnProperty('parser') || typeof item === 'object' && item.parser == null) {
        items2.push(item);
      }
    }
    return setProperty(items2, 'parser', getDefaultParser);
  };

  // Defines a new property directly on an object

  setProperty = function (items, name, getDefault) {
    return helper.setProperty(items, name, getDefault);
  };

  // Generates a random ID

  // Returns: the new ID
  generateId = function () {
    return uuid.v4();
  };

  exports.setLoggerOwner = setLoggerOwner;

  exports.getLogger = getLogger;

  exports.setLogger = setLogger;

  exports.inject = inject;

  exports.configureLogger = configureLogger;

  exports.generateId = generateId;

  exports.setProperty = setProperty;

  // Exports our own transports
  exports.WinstonLogstash = WinstonLogstash;

  // For now, we include the parser in k-logger
  exports.setParser = setParser;

  exports.setDefaultParser = setDefaultParser;

  exports.getDefaultParser = getDefaultParser;

  exports.JsonParser = JsonParser;

  exports.NullParser = NullParser;
}).call(undefined);