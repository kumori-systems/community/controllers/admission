'use strict';

(function () {
  /*
  * Copyright 2022 Kumori Systems S.L.
   * Licensed under the EUPL, Version 1.2 or – as soon they
    will be approved by the European Commission - subsequent
    versions of the EUPL (the "Licence");
   * You may not use this work except in compliance with the
    Licence.
   * You may obtain a copy of the Licence at:
     https://joinup.ec.europa.eu/software/page/eupl
   * Unless required by applicable law or agreed to in
    writing, software distributed under the Licence is
    distributed on an "AS IS" basis,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
    express or implied.
   * See the Licence for the specific language governing
    permissions and limitations under the Licence.
   */
  var LEVELS, ProxyLogger, helper, stackTrace;

  stackTrace = require('stack-trace'); // --> TODO : see _getInvoker

  helper = require('./helper');

  LEVELS = ['silly', 'verbose', 'debug', 'info', 'data', 'warn', 'error'];

  // Proxy, used by a class, to the singleton logger.
  // Instances of class can use the loggers methods (info, warn, ...).
  // ProxyLogger adds additional info to log record: name of class and name of
  // method that wrote it.

  ProxyLogger = class ProxyLogger {
    // Constructor
    // - Logger: class of the singleton logger (injected)
    // - clazz: class that crete (and use) this proxy

    constructor(_logger, clazz) {
      var i, len, level;
      this._logger = _logger;
      this._setClazz(clazz);
      this['close'] = () => {
        return this._logger.close();
      };
      for (i = 0, len = LEVELS.length; i < len; i++) {
        level = LEVELS[i];
        (level => {
          return this[level] = (...args) => {
            var e, metadata, text, value;
            metadata = {};
            metadata.clazz = this._clazz;
            // ONLY FOR TEST PURPOSES. ITS A VERY EXPENSIVE MODE
            // if @_logger.autoMethod then metadata.method = @_getInvoker()
            text = '';
            if (args.length === 1) {
              value = args[0];
              if (value === null) {
                text = 'null';
              } else if (value === void 0) {
                text = 'undefined';
              } else if (typeof value === 'string') {
                text = value;
              } else if (typeof value === 'object') {
                text = JSON.stringify(value);
              } else {
                try {
                  text = value.toString();
                } catch (error) {
                  e = error;
                  text = `proxylogger error = ${e.message}`;
                }
              }
            } else if (args.length > 1) {
              text = Array.apply(null, args);
            }
            return this._logger.log(level, text, metadata);
          };
        })(level);
      }
    }

    // Inject the `logger` property to other clases

    inject(items) {
      return helper.inject(items, this._logger);
    }

    // Returns ID of singleton logger instance

    getId() {
      return this._logger.getId();
    }

    // Returns real logger instance
    getRealLogger() {
      return this._logger;
    }

    // Deprecated --> use setOwner
    // Old code use this sintax:
    //   blablabla.logger.setRuntime {'instance':{'iid':@config.iid}}

    setRuntime(runtime) {
      return this.setOwner(runtime.instance.iid);
    }

    // Sets the owner of the singleton logger, a metadata fixed at singleton-logger
    // level.
    // Represents the PROCESS (slap-agent, router-agent, a component instance...)

    setOwner(owner) {
      if (typeof owner === 'string') {
        return this._logger.addFixedMetaData('owner', owner);
      }
    }

    // Sets the context of the singleton logger, a metadata fixed at
    // singleton-logger level.
    // We can assign the same context to several loggers: for example, assign the
    // deployment-id to all deployed instances.

    setContext(context) {
      if (typeof context === 'string') {
        return this._logger.addFixedMetaData('context', context);
      }
    }

    // Returns current config
    getConfig() {
      return this._logger.getConfig();
    }

    // Sets/changes a configuration for logger

    configure(config) {
      return this._logger.reconfigure(config);
    }

    // "Clazz" is a meta-data fixed at proxy-logger level

    _setClazz(clazz) {
      if (typeof clazz === 'function') {
        return this._clazz = clazz.name;
      } else if (typeof clazz === 'string') {
        return this._clazz = clazz;
      } else {
        return this._clazz = '';
      }
    }

  };

  // "Method" is a meta-data fixed at proxy-logger level. This function get it.
  // TODO: DANGER !!! (jvalero)
  // Esto es **MUY** ineficiente. El método permite obtener la función invocadora
  // para así registrarla en log. PERO es una operación pesada. Lo idóneo será
  // que se fije en la invocación al log.
  // De momento, voy a usarla porque nos conviene facilitar el traceo de log.

  // _getInvoker: () ->
  //   trace = stackTrace.get(@_getInvoker.caller)
  //   return trace[0].getFunctionName()
  module.exports = ProxyLogger;
}).call(undefined);