'use strict';

(function () {
  /*
  * Copyright 2022 Kumori Systems S.L.
   * Licensed under the EUPL, Version 1.2 or – as soon they
    will be approved by the European Commission - subsequent
    versions of the EUPL (the "Licence");
   * You may not use this work except in compliance with the
    Licence.
   * You may obtain a copy of the Licence at:
     https://joinup.ec.europa.eu/software/page/eupl
   * Unless required by applicable law or agreed to in
    writing, software distributed under the Licence is
    distributed on an "AS IS" basis,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
    express or implied.
   * See the Licence for the specific language governing
    permissions and limitations under the Licence.
   */
  var ProxyLogger,
      WinstonLogger,
      errors,
      inject,
      setProperty,
      indexOf = [].indexOf;

  errors = require('./errors');

  WinstonLogger = require('./winston-logger');

  ProxyLogger = require('./proxy-logger');

  // Inject the `logger` property to the given classes or objects.

  // When the logger is injected into a class, it can declare (through
  // @_loggerDependencies function )other classes to  which inject the logger too
  // in a recursive way (addDependencies function).

  // * `items`: array of classes/objects where the default logger will be injected.
  // * `singletonLogger`: (optional) winston-logger instance to use (otherwise,
  //                      a factory will be used)

  inject = function (items, singletonLogger) {
    var addDependencies, clazz, completedItems, i, item, j, len, len1, object, results;
    addDependencies = function (currentItems, item) {
      var dependentItem, dependentItems, i, len, results;
      if (item != null && indexOf.call(currentItems, item) < 0) {
        currentItems.push(item);
        if (item._loggerDependencies != null) {
          dependentItems = item._loggerDependencies();
          if (dependentItems != null && dependentItems instanceof Array) {
            results = [];
            for (i = 0, len = dependentItems.length; i < len; i++) {
              dependentItem = dependentItems[i];
              results.push(addDependencies(currentItems, dependentItem));
            }
            return results;
          }
        }
      }
    };
    if (!(items instanceof Array)) {
      throw new Error(errors.ARRAY_EXPECTED);
    }
    if (singletonLogger == null || singletonLogger.constructor == null || singletonLogger.constructor.name !== 'WinstonLogger') {
      singletonLogger = WinstonLogger.getLogger();
    }
    completedItems = [];
    for (i = 0, len = items.length; i < len; i++) {
      item = items[i];
      addDependencies(completedItems, item);
    }
    results = [];
    for (j = 0, len1 = completedItems.length; j < len1; j++) {
      item = completedItems[j];
      if (typeof item === 'function') {
        clazz = item;
        results.push(function (clazz) {
          // TO BE REVIEWED: This has been added to avoid errors when calling
          // inject twice.
          if (!clazz.prototype.hasOwnProperty('logger')) {
            return setProperty([clazz], 'logger', function () {
              var logger;
              logger = new ProxyLogger(singletonLogger, clazz);
              return logger;
            });
          }
        }(clazz));
      } else if (typeof item === 'object') {
        object = item;
        results.push(function (object) {
          if (object.logger == null) {
            return setProperty([object], 'logger', function () {
              var logger;
              logger = new ProxyLogger(singletonLogger, object.constructor.name);
              return logger;
            });
          }
        }(object));
      } else {
        results.push(void 0);
      }
    }
    return results;
  };

  // Defines a new property directly on an object

  setProperty = function (items, name, getDefault) {
    var _name, clazz, i, item, len, object, results;
    if (!(items instanceof Array)) {
      throw new Error(errors.ARRAY_EXPECTED);
    }
    results = [];
    for (i = 0, len = items.length; i < len; i++) {
      item = items[i];
      _name = '_' + name;
      if (typeof item === 'function') {
        clazz = item;
        results.push(Object.defineProperty(clazz.prototype, name, {
          get: function () {
            if (!this[_name]) {
              this[_name] = getDefault();
            }
            return this[_name];
          },
          set: function (value) {
            //if not parser instanceof Parser
            //    throw errors.PARSER_INVALID
            return this[_name] = value;
          }
        }));
      } else if (typeof item === 'object') {
        object = item;
        results.push(Object.defineProperty(object, name, {
          get: function () {
            if (!this[_name]) {
              this[_name] = getDefault();
            }
            return this[_name];
          },
          set: function (value) {
            //if not parser instanceof Parser
            //    throw errors.PARSER_INVALID
            return this[_name] = value;
          }
        }));
      } else {
        results.push(void 0);
      }
    }
    return results;
  };

  exports.inject = inject;

  exports.setProperty = setProperty;
}).call(undefined);