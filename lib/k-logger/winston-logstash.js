'use strict';

(function () {
  /*
  * Copyright 2022 Kumori Systems S.L.
   * Licensed under the EUPL, Version 1.2 or – as soon they
    will be approved by the European Commission - subsequent
    versions of the EUPL (the "Licence");
   * You may not use this work except in compliance with the
    Licence.
   * You may obtain a copy of the Licence at:
     https://joinup.ec.europa.eu/software/page/eupl
   * Unless required by applicable law or agreed to in
    writing, software distributed under the Licence is
    distributed on an "AS IS" basis,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
    express or implied.
   * See the Licence for the specific language governing
    permissions and limitations under the Licence.
   */
  var ECONNREFUSED_REGEXP,
      KEEP_ALIVE_INITIAL_DELAY,
      MAX_LOG_QUEUE,
      WinstonLogstash,
      common,
      fs,
      net,
      os,
      path,
      tls,
      util,
      winston,
      boundMethodCheck = function (instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new Error('Bound instance method accessed before binding');
    }
  };

  os = require('os');

  fs = require('fs');

  net = require('net');

  tls = require('tls');

  path = require('path');

  util = require('util');

  winston = require('winston');

  common = require('winston/lib/winston/common');

  ECONNREFUSED_REGEXP = /ECONNREFUSED/;

  KEEP_ALIVE_INITIAL_DELAY = 60 * 1000;

  MAX_LOG_QUEUE = 10000;

  WinstonLogstash = class WinstonLogstash extends winston.Transport {
    constructor(options) {
      var p, ref, ref1, ref10, ref11, ref12, ref13, ref2, ref3, ref4, ref5, ref6, ref7, ref8, ref9;
      super(options);
      this.onSocketConnect = this.onSocketConnect.bind(this);
      this.onSocketTimeout = this.onSocketTimeout.bind(this);
      this.onSocketError = this.onSocketError.bind(this);
      this.onSocketClose = this.onSocketClose.bind(this);
      this.options = options;
      if (this.options == null) {
        this.options = {};
      }
      this.name = 'logstash';
      this.localhost = (ref = this.options.localhost) != null ? ref : os.hostname();
      this.host = (ref1 = this.options.host) != null ? ref1 : '127.0.0.1';
      this.port = (ref2 = this.options.port) != null ? ref2 : 28777;
      this.node_name = (ref3 = this.options.node_name) != null ? ref3 : process.title;
      this.pid = (ref4 = this.options.pid) != null ? ref4 : process.pid;
      this.retries = -1;
      // Default is infinite retries (value -1)
      this.max_connect_retries = 'number' === typeof this.options.max_connect_retries ? this.options.max_connect_retries : -1;
      this.timeout_connect_retries = 'number' === typeof this.options.timeout_connect_retries ? this.options.timeout_connect_retries : 1000;
      // Support for winston build in logstash format
      // https:#github.com/flatiron/winston/blob/master/lib/winston/common.js#L149
      this.logstash = (ref5 = this.options.logstash) != null ? ref5 : false;
      // SSL Settings
      this.ssl_enable = (ref6 = this.options.ssl_enable) != null ? ref6 : false;
      this.ssl_key = (ref7 = this.options.ssl_key) != null ? ref7 : '';
      this.ssl_cert = (ref8 = this.options.ssl_cert) != null ? ref8 : '';
      this.ca = (ref9 = this.options.ca) != null ? ref9 : '';
      this.ssl_passphrase = (ref10 = this.options.ssl_passphrase) != null ? ref10 : '';
      this.rejectUnauthorized = this.options.rejectUnauthorized === true;
      // Connection state
      this.log_queue = [];
      this.connected = false;
      this.socket = null;
      // Miscellaneous @options
      this.strip_colors = (ref11 = this.options.strip_colors) != null ? ref11 : false;
      this.label = (ref12 = this.options.label) != null ? ref12 : this.node_name;
      this.meta_defaults = (ref13 = this.options.meta) != null ? ref13 : {};
      // We want to avoid copy-by-reference for meta defaults, so make sure it's a
      // flat object.
      for (p in this.meta_defaults) {
        if (typeof this.meta_defaults[p] === 'object') {
          delete this.meta_defaults[p];
        }
      }
      this.connect();
    }

    log(level, msg, meta, callback) {
      var i, len, log_entry, property;
      meta = winston.clone(meta != null ? meta : {});
      log_entry = null;
      if (callback == null) {
        callback = function () {};
      }
      for (property in this.meta_defaults) {
        meta[property] = this.meta_defaults[property];
      }
      if (this.silent) {
        return callback(null, true);
      }
      if (this.strip_colors) {
        msg = msg.stripColors;
        // Get rid of colors on meta properties
        if (typeof meta === 'object') {
          for (i = 0, len = meta.length; i < len; i++) {
            property = meta[i];
            meta[property] = meta[property].stripColors;
          }
        }
      }
      log_entry = common.log({
        level: level,
        message: msg,
        node_name: this.node_name,
        meta: meta,
        timestamp: this.timestamp,
        json: true,
        label: this.label
      });
      if (this.connected) {
        return this.sendLog(log_entry, () => {
          this.emit('logged');
          return callback(null, true);
        });
      } else if (this.log_queue.length < MAX_LOG_QUEUE) {
        return this.log_queue.push({
          message: log_entry,
          callback: err => {
            this.emit('logged');
            return callback(null, true);
          }
        });
      } else {
        // This traces will be lost
        return console.log('WinstonLogstash - Loosing traces!!');
      }
    }

    connect() {
      var caList, e, i, item, len, options, ref, tryReconnect;
      if (this.connected) {
        return;
      }
      tryReconnect = true;
      options = {};
      this.retries++;
      this.connecting = true;
      this.terminating = false;
      if (this.ssl_enable) {
        options = {
          key: this.ssl_key != null ? fs.readFileSync(this.ssl_key).toString() : null,
          cert: this.ssl_cert != null ? fs.readFileSync(this.ssl_cert).toString() : null,
          passphrase: this.ssl_passphrase != null ? this.ssl_passphrase : null,
          rejectUnauthorized: this.rejectUnauthorized === true
        };
        // TODO: this was supposed to be a list of files.
        if (this.ca != null) {
          caList = [];
          if ('string' === typeof this.ca && this.ca !== '') {
            caList = [fs.readFileSync(this.ca).toString()];
          } else if (Array.isArray(this.ca)) {
            ref = this.ca;
            for (i = 0, len = ref.length; i < len; i++) {
              item = ref[i];
              caList.push(fs.readFileSync(item).toString());
            }
          }
          options.ca = caList;
        } else {
          options.ca = null;
        }
        console.log('WinstonLogstash - CONNECTING TLS SOCKET....');
        try {
          // console.log "WinstonLogstash - TLS OPTIONS: #{JSON.stringify options}"
          this.socket = new tls.connect(this.port, this.host, options, () => {
            console.log('WinstonLogstash - CONNECTED TLS SOCKET!');
            this.socket.setEncoding('UTF-8');
            this.announce();
            return this.connecting = false;
          });
        } catch (error) {
          e = error;
          console.log(`WinstonLogstash - ERROR!!!!! ${e} ${e.message}`);
        }
      } else {
        this.socket = new net.Socket();
      }
      this.socket.on('error', this.onSocketError);
      this.socket.on('timeout', this.onSocketTimeout);
      this.socket.on('connect', this.onSocketConnect);
      this.socket.on('close', this.onSocketClose);
      if (!this.ssl_enable) {
        return this.socket.connect(this.port, this.host, () => {
          this.announce();
          this.connecting = false;
          // Updating from:
          // https://github.com/jaakkos/winston-logstash/pull/48
          return this.socket.setKeepAlive(true, KEEP_ALIVE_INITIAL_DELAY);
        });
      }
    }

    close() {
      this.terminating = true;
      if (this.connected && this.socket) {
        this.connected = false;
        this.socket.end();
        this.socket.destroy();
        return this.socket = null;
      }
    }

    announce() {
      this.connected = true;
      this.flush();
      if (this.terminating) {
        return this.close();
      }
    }

    flush() {
      var i, len, queueItem, ref;
      ref = this.log_queue;
      for (i = 0, len = ref.length; i < len; i++) {
        queueItem = ref[i];
        this.sendLog(queueItem.message, queueItem.callback);
        this.emit('logged');
      }
      return this.log_queue.length = 0;
    }

    sendLog(message, callback) {
      var err;
      callback = callback || function () {}; // Do nothing
      if (this.socket != null) {
        try {
          this.socket.write(message + '\n');
        } catch (error) {
          err = error;
          console.log(`WinstonLogstash.sendLog - ${err.message}`);
        }
      } else {
        console.log('WinstonLogstash.sendLog - @socket is null. Skipping send.');
      }
      // Do not emit the error upwards since it might end up making a loop
      return callback();
    }

    onSocketConnect() {
      boundMethodCheck(this, WinstonLogstash);
      console.log('WinstonLogstash - SOCKET CONNECTED!');
      return this.retries = 0;
    }

    onSocketTimeout() {
      boundMethodCheck(this, WinstonLogstash);
      if (this.socket.readyState !== 'open') {
        return this.socket.destroy();
      }
    }

    onSocketError(err) {
      var closedSocket;
      boundMethodCheck(this, WinstonLogstash);
      console.log(`WinstonLogstash - SOCKET ERROR: ${err}`, err);
      this.connecting = false;
      this.connected = false;
      closedSocket = this.socket;
      this.socket = null;
      try {
        if (typeof closedSocket !== 'undefined' && closedSocket !== null) {
          closedSocket.destroy();
        }
      } catch (error) {
        err = error;
        console.log('WinstonLogstash.onSocketError - socket.destroy failed.', err);
      }
      return this.emit('error', err);
    }

    onSocketClose(had_error) {
      var closedSocket, err;
      boundMethodCheck(this, WinstonLogstash);
      if (this.terminating) {
        return;
      }
      this.connected = false;
      closedSocket = this.socket;
      this.socket = null;
      try {
        if (typeof closedSocket !== 'undefined' && closedSocket !== null) {
          closedSocket.destroy();
        }
      } catch (error) {
        err = error;
        console.log('WinstonLogstash.onSocketClose - socket.destroy failed.', err);
      }
      if (this.max_connect_retries < 0 || this.retries < this.max_connect_retries) {
        if (!this.connecting) {
          this.emit('error', new Error('Transport disconnected. Trying to reconnect...'));
          return setTimeout(() => {
            return this.connect();
          }, this.timeout_connect_retries);
        }
      } else {
        // Unable to connect to server, drop all queued messages, go into silent
        // mode and emit a warning upwards.
        this.log_queue = [];
        this.silent = true;
        return this.emit('error', new Error('Max retries reached, transport in silent mode, OFFLINE'));
      }
    }

  };

  // Define a getter so that `winston.transports.Syslog`
  // is available and thus backwards compatible.
  winston.transports.Logstash = WinstonLogstash;

  module.exports = WinstonLogstash;
}).call(undefined);