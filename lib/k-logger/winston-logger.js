'use strict';

(function () {
  /*
  * Copyright 2022 Kumori Systems S.L.
   * Licensed under the EUPL, Version 1.2 or – as soon they
    will be approved by the European Commission - subsequent
    versions of the EUPL (the "Licence");
   * You may not use this work except in compliance with the
    Licence.
   * You may obtain a copy of the Licence at:
     https://joinup.ec.europa.eu/software/page/eupl
   * Unless required by applicable law or agreed to in
    writing, software distributed under the Licence is
    distributed on an "AS IS" basis,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
    express or implied.
   * See the Licence for the specific language governing
    permissions and limitations under the Licence.
   */
  /*
  Configuration sample:
  {
    'handleExceptions': false
    'auto-method': false
    'vm': 'aaaa'
    'context': 'bbbb'
    'transports': {
      'console': {
        'level': 'info'
        'colorize': true
        'prettyPrint': false
      }
      'file': {
        'level': 'info'
        'filename': 'slap.log'
        'maxSize': 50*1024*1024
        'maxFiles': 100
        'tailable': true
        'zippedArchive': false
      }
      'http': {
        'level': 'info'
        'host': null
        'port': null
      }
      'logstash': {
        'level': 'info'
        'max_connect_retries': 10
        'timeout_connect_retries': 1000
        'host': null
        'port': null
        'ssl_enable': false
        'ssl_cert': ''  # Path to client certificate
        'ssl_key': ''   # Path to client key
        'ca': [ '' ]        # Paths to server certificate CAs (to validate it)
        'ssl_passphrase': ''
        'rejectUnauthorized': false  # Reject unauthorized connections
      }
    }
  }
  */
  var DEFAULT_CONFIG, LOGGER_CONFIG_PATH, WinstonLogger, _, fs, instances, path, q, singletonInstance, uuid, winston;

  winston = require('winston');

  uuid = require('uuid');

  path = require('path');

  fs = require('fs');

  q = require('q');

  _ = require('lodash');

  // This require makes 'winston.transports.Logstash' available
  require('./winston-logstash');

  LOGGER_CONFIG_PATH = 'config/logger-config.json';

  DEFAULT_CONFIG = {
    'handleExceptions': false,
    'vm': 'not defined',
    'auto-method': false,
    'transports': {
      'file': {
        'level': 'info',
        'filename': 'slap.log',
        'maxSize': 50 * 1024 * 1024,
        'maxFiles': 100,
        'tailable': true,
        'zippedArchive': false
      }
    }
  };

  // WinstonLogger class extends winston.logger, simply configuring it
  // using a config json file.
  // This module exports a singleton instance.

  WinstonLogger = class WinstonLogger extends winston.Logger {
    constructor(loggerId1) {
      super({
        transports: []
      });
      this.loggerId = loggerId1;
      this._meta = {};
      this.reconfigure(this._getDefaultConfig());
      this.log('info', `Winston Logger created (id=${this.loggerId})`, {});
      this.maxLevel = null;
      this.config = null;
    }

    getId() {
      return this.loggerId;
    }

    log(level, message, meta) {
      var e, key, ref, value;
      try {
        if (this.maxLevel == null || this.levels[level] <= this.maxLevel) {
          ref = this._meta;
          for (key in ref) {
            value = ref[key];
            meta[key] = value;
          }
          meta['slap_timestamp'] = new Date();
          return super.log(level, message, meta);
        }
      } catch (error) {
        e = error;
        return console.log(`Winston logger error: ${e.message}`);
      }
    }

    addFixedMetaData(dataName, dataValue) {
      return this._meta[dataName] = dataValue;
    }

    getConfig() {
      return this.config;
    }

    reconfigure(config) {
      var e, key, logstashTransport, options, ref, ref1, ref2, ref3, ref4, ref5, ref6, transport, transportLevel, transports;
      this.log('info', `Winston Logger reconfiguring (id=${this.loggerId}) ${JSON.stringify(config)})`, {});
      try {
        if (config === null) {
          config = DEFAULT_CONFIG;
        }
        transports = [];
        // Backward compatibility. Check if its an old-version configuration
        if (config.transports == null) {
          config = this._updateVersion(config);
        }
        this.config = config;
        if (config.transports['console'] != null) {
          options = _.cloneDeep(config.transports['console']);
          options['handleExceptions'] = (ref = config['handleExceptions']) != null ? ref : false;
          if (options['level'] == null) {
            options['level'] = 'info';
          }
          transports.push(new winston.transports.Console(options));
        }
        if (config.transports['file'] != null) {
          options = _.cloneDeep(config.transports['file']);
          options['handleExceptions'] = (ref1 = config['handleExceptions']) != null ? ref1 : false;
          if (options['level'] == null) {
            options['level'] = 'info';
          }
          if (options['maxsize'] == null) {
            options['maxsize'] = 50 * 1024 * 1024; //50MB
          }
          if (options['maxFiles'] == null) {
            options['maxFiles'] = '100';
          }
          if (options['tailable'] == null) {
            options['tailable'] = true;
          }
          if (options['zippedArchive'] == null) {
            options['zippedArchive'] = false;
          }
          transports.push(new winston.transports.File(options));
        }
        if (config.transports['http'] != null) {
          options = _.cloneDeep(config.transports['http']);
          options['handleExceptions'] = (ref2 = config['handleExceptions']) != null ? ref2 : false;
          if (options['level'] == null) {
            options['level'] = 'info';
          }
          transports.push(new winston.transports.Http(options));
        }
        if (config.transports['logstash'] != null) {
          options = _.cloneDeep(config.transports['logstash']);
          options['handleExceptions'] = (ref3 = config['handleExceptions']) != null ? ref3 : false;
          if (options['level'] == null) {
            options['level'] = 'info';
          }
          if (options['max_connect_retries'] == null) {
            options['max_connect_retries'] = -1; // Infinite retries
          }
          if (options['timeout_connect_retries'] == null) {
            options['timeout_connect_retries'] = 1000;
          }
          if (options['ssl_enable'] == null) {
            options['ssl_enable'] = false;
          }
          if (options['ssl_cert'] == null) {
            options['ssl_cert'] = '';
          }
          if (options['ssl_key'] == null) {
            options['ssl_key'] = '';
          }
          if (options['ca'] == null) {
            options['ca'] = [''];
          }
          if (options['ssl_passphrase'] == null) {
            options['ssl_passphrase'] = '';
          }
          if (options['rejectUnauthorized'] == null) {
            options['rejectUnauthorized'] = false;
          }
          logstashTransport = new winston.transports.Logstash(options);
          logstashTransport.on('error', err => {
            return this.log('error', `Logstash Winston transport warning: ${err.message}`, {});
          });
          transports.push(logstashTransport);
        }
        ref4 = this.transports;
        // Because Winston bug ...
        for (key in ref4) {
          transport = ref4[key];
          if (transport.name === 'file') {
            transport._buffer.length = 0;
          }
        }
        this.configure({
          transports: transports
        });
        this.autoMethod = (ref5 = config['auto-method']) != null ? ref5 : false;
        this.addFixedMetaData('vm', config['vm']);
        if (config['context'] != null) {
          this.addFixedMetaData('context', config['context']);
        } else if (this._meta['context'] == null) {
          this.addFixedMetaData('context', '');
        }
        ref6 = this.transports;
        // Improve winston-level management
        for (key in ref6) {
          transport = ref6[key];
          transportLevel = this.levels[transport.level];
          if (this.maxLevel == null) {
            this.maxLevel = transportLevel;
          } else if (transportLevel > this.maxLevel) {
            this.maxLevel = transportLevel;
          }
        }
        return this.log('info', `Winston Logger reconfigured (id=${this.loggerId}, level=${this.maxLevel})`, {});
      } catch (error) {
        e = error;
        return this.log('error', `Winston Logger reconfiguring error: ${e.message}`, {});
      }
    }

    _getDefaultConfig() {
      var base, config, err, tokens;
      base = __dirname.split('/node_modules/')[0];
      tokens = base.split('/src');
      if (tokens.length === 1) {
        base = tokens[0];
      }
      path = base + '/' + LOGGER_CONFIG_PATH;
      config = null;
      try {
        config = fs.readFileSync(path, 'utf8');
        return JSON.parse(config);
      } catch (error) {
        err = error;
        console.log('Using default config');
        return DEFAULT_CONFIG;
      }
    }

    _updateVersion(config) {
      var options, ref, ref1, ref10, ref11, ref12, ref13, ref14, ref15, ref16, ref17, ref18, ref19, ref2, ref3, ref4, ref5, ref6, ref7, ref8, ref9, updatedConfig;
      updatedConfig = {
        transports: {}
      };
      updatedConfig['handleExceptions'] = (ref = config['handleExceptions']) != null ? ref : false;
      updatedConfig['vm'] = (ref1 = config['vm']) != null ? ref1 : 'not defined';
      updatedConfig['auto-method'] = (ref2 = config['auto-method']) != null ? ref2 : false;
      updatedConfig['context'] = config['context'];
      if (config['console-log'] === true) {
        options = {};
        options['level'] = (ref3 = config['console-level']) != null ? ref3 : 'info';
        options['colorize'] = (ref4 = config['colorize']) != null ? ref4 : false;
        options['prettyPrint'] = (ref5 = config['prettyPrint']) != null ? ref5 : false;
        updatedConfig.transports['console'] = options;
      }
      if (config['file-log'] === true) {
        options = {};
        options['level'] = (ref6 = config['file-level']) != null ? ref6 : 'info';
        options['filename'] = (ref7 = config['file-filename']) != null ? ref7 : false;
        options['maxSize'] = (ref8 = config['file-maxSize']) != null ? ref8 : 50 * 1024 * 1024;
        options['maxFiles'] = (ref9 = config['file-maxFiles']) != null ? ref9 : 100;
        options['zippedArchive'] = (ref10 = config['file-zipOldFiles']) != null ? ref10 : false;
        updatedConfig.transports['file'] = options;
      }
      if (config['http-log'] === true) {
        options = {};
        options['level'] = (ref11 = config['http-level']) != null ? ref11 : 'info';
        options['host'] = config['http-host'];
        options['port'] = config['http-port'];
        updatedConfig.transports['http'] = options;
      }
      if (config['logstash-log'] === true) {
        options = {};
        options['level'] = (ref12 = config['logstash-level']) != null ? ref12 : 'info';
        options['host'] = config['logstash-host'];
        options['port'] = config['logstash-port'];
        options['max_connect_retries'] = (ref13 = config['max_connect_retries']) != null ? ref13 : -1; // Infinite retries
        options['timeout_connect_retries'] = (ref14 = config['timeout_connect_retries']) != null ? ref14 : 1000;
        options['ssl_enable'] = config['logstash-ssl_enable'] = false;
        options['ssl_cert'] = (ref15 = options['logstash-ssl_cert']) != null ? ref15 : '';
        options['ssl_key'] = (ref16 = options['logstash-ssl_key']) != null ? ref16 : '';
        options['ca'] = (ref17 = options['logstash-ca']) != null ? ref17 : [''];
        options['ssl_passphrase'] = (ref18 = options['logstash-ssl_passphrase']) != null ? ref18 : '';
        options['rejectUnauthorized'] = (ref19 = options['logstash-rejectUnauthorized']) != null ? ref19 : false;
        updatedConfig.transports['logstash'] = options;
      }
      return updatedConfig;
    }

  };

  instances = {};

  singletonInstance = null;

  module.exports.getLogger = function (loggerId) {
    var instance;
    if (loggerId != null) {
      instance = instances[loggerId];
      if (instance == null) {
        instance = new WinstonLogger(loggerId);
        instances[loggerId] = instance;
        if (singletonInstance == null) {
          singletonInstance = instance;
        }
      }
      return instance;
    } else {
      if (singletonInstance == null) {
        loggerId = uuid.v4();
        singletonInstance = new WinstonLogger(loggerId);
      }
      return singletonInstance;
    }
  };
}).call(undefined);