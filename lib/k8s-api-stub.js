'use strict';

(function () {
  /*
  * Copyright 2022 Kumori Systems S.L.
   * Licensed under the EUPL, Version 1.2 or – as soon they
    will be approved by the European Commission - subsequent
    versions of the EUPL (the "Licence");
   * You may not use this work except in compliance with the
    Licence.
   * You may obtain a copy of the Licence at:
     https://joinup.ec.europa.eu/software/page/eupl
   * Unless required by applicable law or agreed to in
    writing, software distributed under the Licence is
    distributed on an "AS IS" basis,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
    express or implied.
   * See the Licence for the specific language governing
    permissions and limitations under the Licence.
   */
  var ADMISSION_FIELD_MANAGER_NAME,
      DEFAULT_CLUSTER,
      DEFAULT_CONTEXT,
      DEFAULT_KUBECONFIG,
      DEFAULT_PROPAGATION_POLICY,
      DEFAULT_USER,
      EventEmitter,
      K8sApiStub,
      k8s,
      q,
      request,
      stream,
      boundMethodCheck = function (instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new Error('Bound instance method accessed before binding');
    }
  };

  q = require('q');

  k8s = require('@kubernetes/client-node');

  stream = require('stream');

  request = require('request');

  EventEmitter = require('events').EventEmitter;

  // FOR TESTIG IN MINIKUBE @ LOCALHOST

  // apiVersion: v1
  // clusters:
  // - cluster:
  //     certificate-authority: /home/jferrer/.minikube/ca.crt
  //     server: https://192.168.99.100:8443
  //   name: minikube
  // contexts:
  // - context:
  //     cluster: minikube
  //     user: minikube
  //   name: minikube
  // current-context: minikube
  // kind: Config
  // preferences: {}
  // users:
  // - name: minikube
  //   user:
  //     client-certificate: /home/jferrer/.minikube/client.crt
  //     client-key: /home/jferrer/.minikube/client.key
  DEFAULT_CLUSTER = {
    // name: 'my-server'
    // server: 'http://server.com'
    // caBundle: '.....CERTIFICATE.....'
    caFile: '/home/jferrer/.minikube/ca.crt',
    server: 'https://192.168.99.100:8443',
    name: 'minikube',
    skipTLSVerify: false
  };

  DEFAULT_USER = {
    // name: 'my-user'
    // password: 'my-password'
    name: 'minikube',
    certFile: '/home/jferrer/.minikube/client.crt',
    keyFile: '/home/jferrer/.minikube/client.key'
  };

  DEFAULT_CONTEXT = {
    // name: 'my-context'
    // user: DEFAULT_USER.name
    // cluster: DEFAULT_CLUSTER.name
    name: 'minikube',
    user: DEFAULT_USER.name,
    cluster: DEFAULT_CLUSTER.name
  };

  DEFAULT_KUBECONFIG = {
    clusters: [DEFAULT_CLUSTER],
    users: [DEFAULT_USER],
    contexts: [DEFAULT_CONTEXT],
    currentContext: DEFAULT_CONTEXT.name
  };

  DEFAULT_PROPAGATION_POLICY = "Foreground";

  ADMISSION_FIELD_MANAGER_NAME = "kumori-admission";

  K8sApiStub = function () {
    var cleanConditions, extractErrorMsg, quantityToScalar, totalForResource;

    class K8sApiStub extends EventEmitter {
      // Expected configuration example:

      // {
      //   watch: true,
      //   kubeConfigFile: "/path/to/.kubeconfig",
      //   kubeConfig: {
      //     clusters: [{
      //       name: 'minikube-cluster',
      //       server: 'https://192.168.99.100:8443',
      //       caFile: '/home/jferrer/.minikube/ca.crt',
      //       skipTLSVerify: false
      //     }],
      //     users: [{
      //       name: 'minikube',
      //       certFile: '/home/jferrer/.minikube/client.crt',
      //       keyFile: '/home/jferrer/.minikube/client.key'
      //     },{
      //         name: 'my-user',
      //         password: 'my-password'
      //     }],
      //     contexts: [
      //       name: 'minikube',
      //       user: 'my-user',
      //       cluster: 'minikube-cluster'
      //     ],
      //     currentContext: 'minikube'
      //   }
      constructor(config = {}) {
        var base, meth;
        super();
        this.watchHandler = this.watchHandler.bind(this);
        // console.log "\n%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
        // console.log "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
        // console.log "%%%%  EVENT TYPE: #{evtType} - OBJECT: #{JSON.stringify evtObj}"
        // console.log "%%%%  UPDATING LAST VERSION TO: #{@lastResourceVersion}"
        // console.log "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
        // console.log "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n"
        /*
          try {
            if (!obj) {
              let msg = `received undefined object for phase ${phase}`
              throw new Error(msg)
            }
            if (obj.metadata.resourceVersion && (obj.metadata.resourceVersion.localeCompare(this.lastResourceVersion || '0') > 0)) {
              this.lastResourceVersion = obj.metadata.resourceVersion
              // console.log(`\n\n\n---->Set lastResourceVersion: ${this.lastResourceVersion}<----\n\n\n`)
            }
             switch (phase) {
              case 'ADDED': {
                this.emit('add', obj)
                break
              }
              case 'MODIFIED': {
                this.emit('update', obj)
                break
              }
              case 'DELETED': {
                this.emit('delete', obj)
                break
              }
              case 'ERROR': {
                this.lastResourceVersion = undefined
                let aux: any = obj
                this.doneHandler(aux as Error)
                break
              }
              default: {
                let msg = `phase ${phase} unknown`
                throw new Error(msg)
              }
            }
          } catch (error) {
            let msg = `KukuInformer.watchHandler. Error: ${error.message}`
            this.emit('error', new Error(msg))
          }
        }
        */
        this.doneHandler = this.doneHandler.bind(this);
        this.config = config;
        // For testing
        if (this.logger == null) {
          this.logger = {};
          this.logger.error = console.log;
          this.logger.warn = console.log;
          this.logger.info = console.log;
          this.logger.debug = console.log;
          this.logger.silly = console.log;
        }
        meth = 'k8sApiStub.constructor';
        this.logger.info(meth);
        this.initialized = false;
        // Check if no config
        if (this.config == null) {
          this.config = {};
        }
        // Unless explicitly configured, don't listen to K8s events
        if ((base = this.config).watch == null) {
          base.watch = false;
        }
        // if not @config.kubeConfig?
        //   @logger.info "#{meth} - No kubeConfig found. Use default configuration..."
        //   @config.kubeConfig = DEFAULT_KUBECONFIG
        this.logger.info(`${meth} - Config=${JSON.stringify(this.config)}`);
        // Ignore TLS errors (required for testing).
        process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = 0;
        this.kConfig = new k8s.KubeConfig();
        if (this.config.kubeConfigFile != null) {
          // Load configuration into KubeConfig object from specified file
          this.logger.info(`${meth} - Loading K8s config from file: ${this.config.kubeConfigFile}`);
          this.kConfig.loadFromFile(this.config.kubeConfigFile);
        } else if (this.config.kubeConfig) {
          // Load configuration into KubeConfig object from configuration values
          this.logger.info(`${meth} - Loading K8s config from received configuration.`);
          this.kConfig.loadFromOptions(this.config.kubeConfig);
        } else {
          // No configuration provided, try to load it from  KUBECONFIG environment
          // variable
          this.logger.info(`${meth} - Loading K8s config from default (ENV var).`);
          this.kConfig.loadFromDefault();
        }
        this.logger.info(`${meth} - K8s configuration: ${JSON.stringify(this.kConfig)}`);
        // Create API client objects
        this.k8sApi = this.kConfig.makeApiClient(k8s.CoreV1Api);
        this.k8sAppsApi = this.kConfig.makeApiClient(k8s.AppsV1Api);
        this.k8sCustomApi = this.kConfig.makeApiClient(k8s.CustomObjectsApi);
        this.k8sBatchApi = this.kConfig.makeApiClient(k8s.BatchV1Api);
        this.k8sNetApi = this.kConfig.makeApiClient(k8s.NetworkingV1Api);
        this.k8sWatcher = null;
        if (this.config.watch) {
          this.logger.info(`${meth} - K8s events watcher is on.`);
          this.k8sWatcher = new k8s.Watch(this.kConfig);
        }
        this.k8sLog = new k8s.Log(this.kConfig);
      }

      init() {
        var meth;
        meth = 'k8sApiStub.init';
        this.logger.info(meth);
        if (!this.initialized) {
          if (this.config.watch) {
            this.startWatching();
          }
          this.initialized = true;
        }
        return q(true);
      }

      // @k8sApi.listNamespacedPod('default')
      // .then (res) =>
      //   console.log "PODS FOR NAMESPACE 'default'"
      //   console.log res.body
      //   @k8sApi.listPodForAllNamespaces()
      // .then (res) =>
      //   console.log "PODS FOR ALL NAMESPACES"
      //   console.log res.body
      //   return true
      // .catch (err) =>
      //   console.log "ERROR GETTING NAMESPACE: #{err.message || err}"
      //   console.log "ERROR: #{JSON.stringify err}"
      //   return true
      terminate() {
        var meth;
        meth = 'k8sApiStub.terminate';
        this.logger.info(meth);
        if (this.config.watch) {
          this.stopWatching();
        }
        this.initialized = false;
        return q(true);
      }

      //#############################################################################
      //#                METHODS FOR RECEIVING LIVE EVENTS STREAM                  ##
      //#############################################################################
      startWatching() {
        var apiPath, meth, queryParams;
        meth = 'k8sApiStub.startWatching';
        this.logger.info(meth);
        // apiPath = '/api/v1/namespaces'
        apiPath = '/apis/kumori.systems/v1/namespaces/kumori/kukucomponents';
        // apiPath = '/apis/kumori.systems/v1/namespaces/kumori'
        queryParams = {};
        if (this.lastResourceVersion != null) {
          queryParams.resourceVersion = this.lastResourceVersion;
        }
        this.logger.info(`${meth} - Calling watch...`);
        // The watcher REQ object must be kept to be able to terminate it later
        this.k8sWatcherReq = this.k8sWatcher.watch(apiPath, queryParams, this.watchHandler, this.doneHandler);
        return this.logger.info(`${meth} - Watch started!`);
      }

      stopWatching() {
        var err, meth;
        meth = 'k8sApiStub.startWatching';
        this.logger.info(meth);
        try {
          if (this.k8sWatcherReq != null) {
            this.logger.info(`${meth} - Aborting watch request...`);
            this.k8sWatcherReq.abort();
            this.logger.info(`${meth} - Aborted watch request.`);
          }
          this.k8sWatcher = null;
          return this.k8sWatcherReq = null;
        } catch (error1) {
          err = error1;
          this.logger.error(`${meth} - ERROR: ${err.message || err}`);
          return true;
        }
      }

      watchHandler(evtType, evtObj) {
        var meth, ref;
        boundMethodCheck(this, K8sApiStub);
        meth = 'k8sApiStub.watchHandler';
        this.logger.info(meth);
        this.logger.info(`${meth} - TYPE: ${evtType} - OBJECT: ${JSON.stringify(evtObj)}`);
        if ((evtObj != null ? (ref = evtObj.metadata) != null ? ref.resourceVersion : void 0 : void 0) != null) {
          return this.lastResourceVersion = evtObj.metadata.resourceVersion;
        }
      }

      doneHandler(error) {
        var err, meth;
        boundMethodCheck(this, K8sApiStub);
        meth = 'k8sApiStub.doneHandler';
        this.logger.info(meth);
        this.logger.info(`${meth} - RAW ERROR: ${error}`);
        if (this.k8sWatcherReq) {
          this.logger.info(`${meth} - Reconnecting watcher...`);
          try {
            this.k8sWatcherReq.end();
          } catch (error1) {
            err = error1;
            this.logger.info(`${meth} - Previous request can't be closed: \${err.message}`);
          }
          this.k8sWatcherReq = void 0;
        }
        if (error) {
          this.emit('error', error);
        }
        return this.startWatching();
      }

      // @logger.info "#{meth} - ERROR: #{error.message} - #{error.stack}"
      /*
      if (this.req) {
        try {
          this.req.end()
        } catch (error) {
          console.log(`KukuInformer. Previous request found but can't be closed: ${error.message}`)
        }
        this.req = undefined
      }
      if (error) {
        this.emit('error', error)
      }
      this.watch()
      }
      */
      //#############################################################################
      //#                PUBLIC METHODS TO BE USED FROM OUTSIDE                    ##
      //#############################################################################
      getKumoriElement(plural, name = '') {
        var group, meth, namespace, version;
        meth = 'k8sApiStub.getKumoriElement';
        this.logger.debug(`${meth} - plural=${plural} name=${name}`);
        if (name === '') {
          return q.reject(new Error(`${meth} - Missing mandatory 'name' parameter.`));
        }
        group = 'kumori.systems';
        version = 'v1';
        namespace = 'kumori';
        return this.k8sCustomApi.getNamespacedCustomObject(group, version, namespace, plural, name).then(res => {
          this.logger.info(`${meth} - Got object ${name} (${plural}).`);
          // @logger.info "#{meth} - Got object #{name} (#{plural}):
          //               #{JSON.stringify res.body, null, 2}"
          // console.log "-----------------------------------------------------"
          // console.log "KUMORI ELEMENT:"
          // console.log "#{JSON.stringify res.body}"
          // console.log "-----------------------------------------------------"
          return res.body;
        }).catch(err => {
          var errMsg, ref, ref1;
          if (((ref = err.response) != null ? (ref1 = ref.body) != null ? ref1.reason : void 0 : void 0) === 'NotFound') {
            this.logger.info(`${meth} - Object ${name} (${plural}) not found.`);
            return q(null);
          } else {
            errMsg = `Getting object ${name} (${plural}): ${JSON.stringify(err)}`;
            this.logger.error(`${meth} - ${errMsg}`);
            return q.reject(new Error(errMsg));
          }
        });
      }

      listKumoriElements(plural, selector) {
        var e, group, meth, namespace, version, watchForChanges;
        meth = 'k8sApiStub.listKumoriElements';
        this.logger.debug(`${meth} - plural=${plural} selector: ${JSON.stringify(selector)}`);
        group = 'kumori.systems';
        version = 'v1';
        namespace = 'kumori';
        watchForChanges = false;
        try {
          return this.k8sCustomApi.listNamespacedCustomObject(group, version, namespace, plural, void 0, void 0, void 0, void 0, selector, 0, void 0, void 0, 0, watchForChanges).then(res => {
            // pretty // allowWatchBookmarks // _continue // fieldSelector // labelSelector // limit // resourceVersion // resourceVersionMatch // timeoutSeconds // watch
            var ref;
            this.logger.info(`${meth} - Got ${res.body.items.length} objects.`);
            // @logger.debug "#{meth} - Elements: #{JSON.stringify res.body.items}"
            if (((ref = res.body) != null ? ref.items : void 0) != null) {
              return res.body.items;
            } else {
              throw new Error("List API call returned no item list.");
            }
          }).catch(err => {
            var errMsg;
            errMsg = `Getting '${plural}' objects with selector ${JSON.stringify(selector)}: ${JSON.stringify(err)}`;
            this.logger.error(`${meth} - ${errMsg}`);
            return q.reject(new Error(errMsg));
          });
        } catch (error1) {
          e = error1;
          throw e;
        }
      }

      createKumoriElement(kukuElement) {
        var body, group, kind, meth, name, namespace, plural, version;
        meth = 'k8sApiStub.createKumoriElement';
        kind = kukuElement.getKind();
        group = kukuElement.getGroup();
        version = kukuElement.getVersion();
        namespace = kukuElement.getNamespace();
        plural = kukuElement.getPlural();
        name = kukuElement.metadata.name;
        body = kukuElement;
        // New object is expected to be in JSON
        // body      = kukuElement.serialize('json')
        this.logger.info(`${meth} - Creating: group=${group} version=${version} namespace=${namespace} plural=${plural} name=${name}`);
        return this.k8sCustomApi.createNamespacedCustomObject(group, version, namespace, plural, body, void 0, void 0, ADMISSION_FIELD_MANAGER_NAME).then(res => {
          this.logger.info(`${meth} - Successfully created object: ${name} (${kind})`);
          // console.log "-----------------------------------------------------"
          // console.log "RESULT:"
          // console.log "#{JSON.stringify res, null, 2}"
          // console.log "-----------------------------------------------------"
          return true;
        }).catch(err => {
          var errMsg, ref;
          // console.log "********************"
          // console.log JSON.stringify err
          // console.log "********************"
          errMsg = ((ref = err.body) != null ? ref.message : void 0) || err.message || JSON.stringify(err);
          this.logger.error(`${meth} - Creating ${name} (${kind}): ${errMsg}`);
          return q.reject(new Error(`${meth} - Creating ${name} (${kind}): ${errMsg}`));
        });
      }

      replaceKumoriElement(kukuElement) {
        var body, group, kind, meth, name, namespace, plural, resourceVersion, version;
        meth = 'k8sApiStub.replaceKumoriElement';
        kind = kukuElement.getKind();
        group = kukuElement.getGroup();
        version = kukuElement.getVersion();
        namespace = kukuElement.getNamespace();
        plural = kukuElement.getPlural();
        name = kukuElement.metadata.name;
        resourceVersion = kukuElement.metadata.resourceVersion;
        body = kukuElement;
        // New object is expected to be in JSON
        // body      = kukuElement.serialize('json')
        this.logger.info(`${meth} - Replacing: group=${group} version=${version} namespace=${namespace} plural=${plural} name=${name} resourceVersion=${resourceVersion}`);
        return this.k8sCustomApi.replaceNamespacedCustomObject(group, version, namespace, plural, name, body, void 0, ADMISSION_FIELD_MANAGER_NAME).then(res => {
          this.logger.info(`${meth} - Successfully replaced object: ${name} (${kind})`);
          // console.log "-----------------------------------------------------"
          // console.log "RESULT:"
          // console.log "#{JSON.stringify res, null, 2}"
          // console.log "-----------------------------------------------------"
          return true;
        }).catch(err => {
          var errMsg, ref;
          errMsg = ((ref = err.body) != null ? ref.message : void 0) || err.message || JSON.stringify(err);
          this.logger.error(`${meth} - Replacing ${name} (${kind}): ${errMsg}`);
          return q.reject(new Error(`${meth} - Replacing ${name} (${kind}): ${errMsg}`));
        });
      }

      saveKumoriElement(kukuElement) {
        var meth;
        meth = 'k8sApiStub.saveKumoriElement';
        this.logger.info(`${meth} - Saving name=${kukuElement.metadata.name}`);
        // If element already exists, then it is replaced by the new one.
        // This requires to get the existing element current 'resourceVersion' and
        // send it along in the 'replace' request.

        // Try to read the element
        return this.getKumoriElement(kukuElement.getPlural(), kukuElement.metadata.name).then(existingElement => {
          var errMsg;
          if (existingElement != null) {
            this.logger.info(`${meth} - Element ${kukuElement.metadata.name} exists.`);
            // @logger.info "#{JSON.stringify existingElement}"
            if (existingElement.metadata.deletionTimestamp != null) {
              errMsg = 'Element is currently being deleted.';
              this.logger.error(`${meth} - Updating ${kukuElement.metadata.name}: ${errMsg}`);
              return q.reject(new Error(`${meth} - ${errMsg}`));
            } else {
              kukuElement.setResourceVersion(existingElement.metadata.resourceVersion);
              return this.replaceKumoriElement(kukuElement);
            }
          } else {
            this.logger.info(`${meth} - Element ${kukuElement.metadata.name} is new.`);
            return this.createKumoriElement(kukuElement);
          }
        });
      }

      deleteKumoriElement(plural, name = '') {
        var group, meth, namespace, version;
        meth = 'k8sApiStub.deleteKumoriElement';
        this.logger.debug(`${meth} - plural=${plural} name=${name}`);
        if (name === '') {
          return q.reject(new Error(`${meth} - Missing mandatory 'name' parameter.`));
        }
        group = 'kumori.systems';
        version = 'v1';
        namespace = 'kumori';
        return this.k8sCustomApi.deleteNamespacedCustomObject(group, version, namespace, plural, name, void 0, void 0, DEFAULT_PROPAGATION_POLICY, void 0, void 0, void 0).then(res => {
          // gracePeriodSeconds // orphanDependents // dryRun // body // options
          this.logger.info(`${meth} - Deleted object ${name} (${plural})`);
          // @logger.info "#{meth} - Deleted object #{name} (#{plural}):
          //               #{JSON.stringify res.body, null, 2}"
          // console.log "-----------------------------------------------------"
          // console.log "KUMORI ELEMENT:"
          // console.log "#{JSON.stringify res.body}"
          // console.log "-----------------------------------------------------"
          return res.body;
        }).catch(err => {
          var errMsg, ref, ref1;
          if (((ref = err.response) != null ? (ref1 = ref.body) != null ? ref1.reason : void 0 : void 0) === 'NotFound') {
            this.logger.info(`${meth} - Object ${name} (${plural}) not found.`);
            return q(null);
          } else {
            errMsg = `Deleting object ${name} (${plural}): ${JSON.stringify(err)}`;
            this.logger.error(`${meth} - ${errMsg}`);
            return q.reject(new Error(errMsg));
          }
        });
      }

      getService(namespace, serviceName) {
        var meth;
        meth = `k8sApiStub.getService ${namespace}/${serviceName}`;
        this.logger.info(meth);
        return this.k8sApi.readNamespacedService(serviceName, namespace).then(res => {
          // @logger.info "#{meth} - Got Service: #{JSON.stringify res.body}"
          return res.body;
        }).catch(err => {
          var errMsg, ref, ref1;
          if (((ref = err.response) != null ? (ref1 = ref.body) != null ? ref1.reason : void 0 : void 0) === 'NotFound') {
            errMsg = `Couldn't get Service ${namespace}/${serviceName}: NotFound`;
            this.logger.warn(`${meth} - ${errMsg}`);
            return q.reject(new Error(errMsg));
          } else {
            errMsg = `Couldn't get Service ${namespace}/${serviceName}: ${extractErrorMsg(err)}`;
            this.logger.warn(`${meth} - ${errMsg}`);
            return q.reject(new Error(errMsg));
          }
        });
      }

      deleteService(namespace, serviceName) {
        var meth;
        meth = `k8sApiStub.deleteService ${namespace}/${serviceName}`;
        this.logger.info(meth);
        return this.k8sApi.deleteNamespacedService(serviceName, namespace, void 0, void 0, void 0, void 0, DEFAULT_PROPAGATION_POLICY, void 0, void 0).then(res => {
          // pretty // dryRun // gracePeriodSeconds // orphanDependents // body // options
          // @logger.info "#{meth} - Deleted Service: #{JSON.stringify res.body}"
          this.logger.info(`${meth} - Deleted Service: ${namespace}/${serviceName}`);
          return res.body;
        }).catch(err => {
          var errMsg, ref, ref1;
          if (((ref = err.response) != null ? (ref1 = ref.body) != null ? ref1.reason : void 0 : void 0) === 'NotFound') {
            this.logger.info(`${meth} - Service ${namespace}/${serviceName} not found.`);
            return q(null);
          } else {
            errMsg = `Deleting service ${namespace}/${serviceName}: ${JSON.stringify(err)}`;
            this.logger.error(`${meth} - ${errMsg}`);
            return q.reject(new Error(errMsg));
          }
        });
      }

      getConfigMap(namespace, configMapName) {
        var meth;
        meth = `k8sApiStub.getConfigMap ${namespace}/${configMapName}`;
        this.logger.info(meth);
        return this.k8sApi.readNamespacedConfigMap(configMapName, namespace).then(res => {
          this.logger.info(`${meth} - Got ConfigMap: ${JSON.stringify(res.body)}`);
          return res.body;
        }).catch(err => {
          var errMsg;
          errMsg = `Couldn't get ConfigMap ${namespace}/${configMapName}: ${extractErrorMsg(err)}`;
          this.logger.warn(`${meth} - ${errMsg}`);
          return q.reject(new Error(errMsg));
        });
      }

      createConfigMap(namespace, configMapManifest) {
        var meth;
        meth = `k8sApiStub.createConfigMap ${namespace}/${configMapManifest.metadata.name}`;
        this.logger.info(meth);
        return this.k8sApi.createNamespacedConfigMap(namespace, configMapManifest, void 0, void 0, ADMISSION_FIELD_MANAGER_NAME, void 0, void 0).then(res => {
          // pretty // dryRun // fieldManager // fieldValidation // options
          this.logger.info(`${meth} - Successfully created ConfigMap: ${configMapManifest.metadata.name}`);
          return res.body;
        }).catch(err => {
          var errMsg, ref;
          errMsg = ((ref = err.body) != null ? ref.message : void 0) || err.message || JSON.stringify(err);
          this.logger.error(`${meth} - Creating ${configMapManifest.metadata.name}: ${errMsg}`);
          return q.reject(new Error(`${meth} - Creating ${configMapManifest.metadata.name}: ${errMsg}`));
        });
      }

      createJob(namespace, jobManifest) {
        var meth;
        meth = `k8sApiStub.createJob ${namespace}/${jobManifest.metadata.name}`;
        this.logger.info(meth);
        return this.k8sBatchApi.createNamespacedJob(namespace, jobManifest, void 0, void 0, ADMISSION_FIELD_MANAGER_NAME, void 0, void 0).then(res => {
          // pretty // dryRun // fieldManager // fieldValidation // options
          this.logger.info(`${meth} - Successfully created Job: ${jobManifest.metadata.name}`);
          return res.body;
        }).catch(err => {
          var errMsg, ref;
          errMsg = ((ref = err.body) != null ? ref.message : void 0) || err.message || JSON.stringify(err);
          this.logger.error(`${meth} - Creating ${jobManifest.metadata.name}: ${errMsg}`);
          return q.reject(new Error(`${meth} - Creating ${jobManifest.metadata.name}: ${errMsg}`));
        });
      }

      getJobs(namespace, selector) {
        var meth;
        meth = "k8sApiStub.getJobs";
        this.logger.info(`${meth} - Selector: ${JSON.stringify(selector)}`);
        return this.k8sBatchApi.listNamespacedJob(namespace, void 0, void 0, void 0, void 0, selector, void 0, void 0, void 0, void 0, void 0, void 0).then(res => {
          // pretty // allowWatchBookmarks // _continue // fieldSelector // labelSelector // limit // resourceVersion // resourceVersionMatch // timeoutSeconds // watch // options
          this.logger.info(`${meth} - Got ${res.body.items.length} objects.`);
          return res.body.items;
        }).catch(err => {
          var errMsg, ref;
          errMsg = `Listing Jobs for namespace ${namespace} : ` + (((ref = err.body) != null ? ref.message : void 0) || err.message || JSON.stringify(err));
          this.logger.error(`${meth} - ${errMsg}`);
          return q.reject(new Error(`${meth} - ${errMsg}`));
        });
      }

      getSecret(namespace, secretName) {
        var meth;
        meth = `k8sApiStub.getSecret ${namespace}/${secretName}`;
        this.logger.info(meth);
        return this.k8sApi.readNamespacedSecret(secretName, namespace).then(res => {
          this.logger.info(`${meth} - Got Secret: ${JSON.stringify(res.body)}`);
          return res.body;
        }).catch(err => {
          var errMsg;
          errMsg = `Couldn't get secret ${namespace}/${secretName}: ${extractErrorMsg(err)}`;
          this.logger.warn(`${meth} - ${errMsg}`);
          return q.reject(new Error(errMsg));
        });
      }

      getEvents(namespace) {
        var meth;
        meth = `k8sApiStub.getEvents ${namespace}`;
        this.logger.info(meth);
        return this.k8sApi.listNamespacedEvent(namespace).then(res => {
          this.logger.info(`${meth} - Got ${res.body.items.length} objects.`);
          // console.log "-----------------------------------------------------"
          // console.log "LISTED EVENTS FOR NAMESPACE #{namespace}:"
          // console.log "#{JSON.stringify res.body}"
          // console.log "-----------------------------------------------------"
          return res.body.items;
        }).catch(err => {
          var errMsg, ref;
          errMsg = `Listing events for namespace ${namespace} : ` + (((ref = err.body) != null ? ref.message : void 0) || err.message || JSON.stringify(err));
          this.logger.error(`${meth} - ${errMsg}`);
          return q.reject(new Error(`${meth} - ${errMsg}`));
        });
      }

      getPods(namespace = null, labelSelector = null) {
        var meth, ns;
        ns = namespace || 'ALL NAMESPACES';
        meth = `k8sApiStub.getPods ${ns} Selector: ${JSON.stringify(labelSelector)}`;
        this.logger.info(meth);
        return (namespace != null ? this.k8sApi.listNamespacedPod(namespace, null, null, null, null, labelSelector) : this.k8sApi.listPodForAllNamespaces(null, null, null, labelSelector)).then(res => {
          this.logger.info(`${meth} - Found ${res.body.items.length} Pods.`);
          // console.log "PODS FOR NAMESPACE: #{ns}"
          // @printPodList res.body
          return res.body.items;
        }).catch(err => {
          var errMsg;
          errMsg = `Couldn't get Pod list: ${JSON.stringify(err)}`;
          this.logger.error(`${meth} - ${errMsg}`);
          return q.reject(new Error(errMsg));
        });
      }

      getPod(namespace, podName) {
        var meth;
        meth = `k8sApiStub.getPod ${namespace}/${podName}`;
        this.logger.info(meth);
        return this.k8sApi.readNamespacedPod(podName, namespace).then(res => {
          this.logger.info(`${meth} - Got Pod: ${JSON.stringify(res.body)}`);
          return res.body;
        }).catch(err => {
          var errMsg;
          errMsg = `Couldn't get pod ${namespace}/${podName}: ${extractErrorMsg(err)}`;
          this.logger.warn(`${meth} - ${errMsg}`);
          return q.reject(new Error(errMsg));
        });
      }

      listPods(namespace = null) {
        var ns;
        ns = namespace || 'ALL NAMESPACES';
        return (namespace != null ? this.k8sApi.listNamespacedPod(namespace) : this.k8sApi.listPodForAllNamespaces()).then(res => {
          // console.log "PODS FOR NAMESPACE: #{ns}"
          // @printPodList res.body
          return res.body.items;
        }).catch(err => {
          // console.log "ERROR GETTING PODS FOR NAMESPACE '#{ns}': #{err.message || err}"
          // console.log "ERROR: #{JSON.stringify err}"
          this.logger.error(`${meth} ERROR: ${err.message || err}`);
          return q.reject(err);
        });
      }

      listIngresses(namespace = null) {
        var meth, ns;
        ns = namespace || 'ALL NAMESPACES';
        meth = `k8sApiStub.listIngresses ${ns}`;
        this.logger.info(meth);
        return (namespace != null ? this.k8sNetApi.listNamespacedIngress(namespace, null, null, null, null, labelSelector) : this.k8sNetApi.listIngressForAllNamespaces(null, null, null, null)).then(res => {
          this.logger.info(`${meth} - Found ${res.body.items.length} Ingresses.`);
          // console.log "INGRESSES FOR NAMESPACE: #{ns}"
          // @printPodList res.body
          return res.body.items;
        }).catch(err => {
          var errMsg;
          errMsg = `Couldn't get Ingress list: ${JSON.stringify(err)}`;
          this.logger.error(`${meth} - ${errMsg}`);
          return q.reject(new Error(errMsg));
        });
      }

      deleteIngress(namespace, ingressName) {
        var meth;
        meth = `k8sApiStub.deleteIngress ${namespace}/${ingressName}`;
        this.logger.info(meth);
        return this.k8sNetApi.deleteNamespacedIngress(ingressName, namespace, void 0, void 0, void 0, void 0, DEFAULT_PROPAGATION_POLICY, void 0, void 0).then(res => {
          // pretty // dryRun // gracePeriodSeconds // orphanDependents // body // options
          this.logger.info(`${meth} - Deleted ingress: ${namespace}/${ingressName}`);
          return res.body;
        }).catch(err => {
          var errMsg, ref, ref1;
          if (((ref = err.response) != null ? (ref1 = ref.body) != null ? ref1.reason : void 0 : void 0) === 'NotFound') {
            this.logger.info(`${meth} - Ingress ${namespace}/${ingressName} not found.`);
            return q(null);
          } else {
            errMsg = `Deleting Ingress ${namespace}/${ingressName}: ${JSON.stringify(err)}`;
            this.logger.error(`${meth} - ${errMsg}`);
            return q.reject(new Error(errMsg));
          }
        });
      }

      listNodes() {
        return this.k8sApi.listNode().then(res => {
          // console.log "GOT NODES:"
          // console.log JSON.stringify res.body
          return res.body.items;
        }).catch(err => {
          // console.log "ERROR GETTING NODES: #{err.message || err}"
          // console.log "ERROR: #{JSON.stringify err}"
          this.logger.error(`${meth} ERROR: ${err.message || err}`);
          return q.reject(err);
        });
      }

      getNodesMetrics() {
        var meth;
        meth = 'k8sApiStub.getNodesMetrics';
        return q.promise((resolve, reject) => {
          var clusterURL, nodeMetricsURL, opts;
          opts = {};
          this.kConfig.applyToRequest(opts);
          clusterURL = this.kConfig.getCurrentCluster().server;
          nodeMetricsURL = `${clusterURL}/apis/metrics.k8s.io/v1beta1/nodes`;
          return request.get(nodeMetricsURL, opts, (error, response, body) => {
            var errMsg, i, it, len, nodeData, nodeName, parsedBody, ref, ref1, ref2, result;
            if (error) {
              errMsg = `Unable to node metrics data: ${error.message}`;
              this.logger.error(`${meth} ERROR: ${errMsg}`);
              return resolve(null);
            } else if (response.statusCode !== 200) {
              errMsg = `Unable to node metrics data: unexpected HTTP status ${response.statusCode}`;
              this.logger.error(`${meth} ERROR: ${errMsg}`);
              return resolve(null);
            } else {
              // Body is a string representation of a JSON object:
              //   {
              //     "kind": "NodeMetricsList",
              //     "apiVersion": "metrics.k8s.io/v1beta1"
              //     },
              //     "items": [
              //       <THE ACTUAL DATA WE'RE INTERESTED IN>
              //     ],
              //     [...]
              //   }
              parsedBody = JSON.parse(body);
              // Create a dictionnary where the key is the node name and the value an
              // object with the relevant metric data
              result = {};
              ref = parsedBody.items || [];
              for (i = 0, len = ref.length; i < len; i++) {
                it = ref[i];
                nodeName = it.metadata.name;
                nodeData = {
                  usage: {
                    cpu: quantityToScalar(((ref1 = it.usage) != null ? ref1.cpu : void 0) || 0),
                    memory: quantityToScalar(((ref2 = it.usage) != null ? ref2.memory : void 0) || 0)
                  }
                };
                result[nodeName] = nodeData;
              }
              return resolve(result);
            }
          });
        });
      }

      getPodMetrics(namespace, podName) {
        var meth;
        meth = `k8sApiStub.getPodMetrics(${namespace}, ${podName})`;
        this.logger.debug(meth);
        return q.promise((resolve, reject) => {
          var clusterURL, opts, podMetricsURL;
          opts = {};
          this.kConfig.applyToRequest(opts);
          clusterURL = this.kConfig.getCurrentCluster().server;
          podMetricsURL = `${clusterURL}/apis/metrics.k8s.io/v1beta1/namespaces/` + `${namespace}/pods/${podName}`;
          return request.get(podMetricsURL, opts, (error, response, body) => {
            var errMsg, parsedBody, podData, result;
            if (error) {
              errMsg = `Unable to pod metrics data: ${error.message}`;
              this.logger.error(`${meth} ERROR: ${errMsg}`);
              return resolve(null);
            } else if (response.statusCode === 404) {
              errMsg = `Unable to get pod metrics data: pod not found (Status Code: ${response.statusCode})`;
              this.logger.warn(`${meth} ERROR: ${errMsg}`);
              return resolve(null);
            } else if (response.statusCode !== 200) {
              errMsg = `Unable to get pod metrics data: unexpected HTTP status ${response.statusCode}`;
              this.logger.error(`${meth} ERROR: ${errMsg}`);
              return resolve(null);
            } else {
              // Body is a string representation of a JSON object:
              //   {
              //     "kind": "PodMetrics",
              //     "apiVersion": "metrics.k8s.io/v1beta1",
              //     "metadata": {
              //       "name": "kd-114635-98173648-frontend-deployment-d5c8b44c4-x65hr",
              //       "namespace": "kumori"
              //     },
              //     "timestamp": "2020-04-27T16:37:37Z",
              //     "window": "5m0s",
              //     "containers": [
              //       {
              //         "name": "frontend",
              //         "usage": {
              //           "cpu": "0",
              //           "memory": "2588Ki"
              //         }
              //       }
              //     ]
              //   }
              parsedBody = JSON.parse(body);
              // Create a dictionnary where the key is the pod name and the value an
              // object with the relevant metric data
              result = {};
              podData = this.aggregatePodMetric(parsedBody);
              return resolve(podData);
            }
          });
        });
      }

      getPodsMetrics() {
        var meth;
        meth = 'k8sApiStub.getPodsMetrics';
        return q.promise((resolve, reject) => {
          var clusterURL, opts, podMetricsURL;
          opts = {};
          this.kConfig.applyToRequest(opts);
          clusterURL = this.kConfig.getCurrentCluster().server;
          podMetricsURL = `${clusterURL}/apis/metrics.k8s.io/v1beta1/pods`;
          return request.get(podMetricsURL, opts, (error, response, body) => {
            var errMsg, i, it, len, parsedBody, podData, podName, ref, result;
            if (error) {
              errMsg = `Unable to pod metrics data: ${error.message}`;
              this.logger.error(`${meth} ERROR: ${errMsg}`);
              return resolve(null);
            } else if (response.statusCode !== 200) {
              errMsg = `Unable to pod metrics data: unexpected HTTP status ${response.statusCode}`;
              this.logger.error(`${meth} ERROR: ${errMsg}`);
              return resolve(null);
            } else {
              // Body is a string representation of a JSON object:
              //   {
              //     "kind": "PodMetricsList",
              //     "apiVersion": "metrics.k8s.io/v1beta1"
              //     },
              //     "items": [
              //       <THE ACTUAL DATA WE'RE INTERESTED IN>
              //     ],
              //     [...]
              //   }
              parsedBody = JSON.parse(body);
              // Create a dictionnary where the key is the pod name and the value an
              // object with the relevant metric data
              result = {};
              ref = parsedBody.items || [];
              for (i = 0, len = ref.length; i < len; i++) {
                it = ref[i];
                podName = it.metadata.name;
                podData = this.aggregatePodMetric(it);
                result[podName] = podData;
              }
              return resolve(result);
            }
          });
        });
      }

      getPvcMetric(namespace, pvcName, metricName) {
        var meth;
        meth = `k8sApiStub.getPvcMetric(${namespace}, ${pvcName}, ${metricName})`;
        this.logger.debug(meth);
        return q.promise((resolve, reject) => {
          var clusterURL, opts, pvcMetricURL;
          opts = {};
          this.kConfig.applyToRequest(opts);
          clusterURL = this.kConfig.getCurrentCluster().server;
          pvcMetricURL = `${clusterURL}/apis/custom.metrics.k8s.io/v1beta1/` + `namespaces/${namespace}/` + `persistentvolumeclaims/${pvcName}/${metricName}`;
          return request.get(pvcMetricURL, opts, (error, response, body) => {
            var errMsg, parsedBody;
            if (error) {
              errMsg = `Unable to PVC metrics data: ${error.message || error}`;
              this.logger.error(`${meth} ERROR: ${errMsg}`);
              return resolve(null);
            } else if (response.statusCode === 404) {
              errMsg = `Unable to get PVC metrics data: PVC not found (Status Code: ${response.statusCode})`;
              this.logger.warn(`${meth} ERROR: ${errMsg}`);
              return resolve(null);
            } else if (response.statusCode !== 200) {
              errMsg = `Unable to get PVC metrics data: unexpected HTTP status ${response.statusCode}`;
              this.logger.error(`${meth} ERROR: ${errMsg}`);
              return resolve(null);
            } else {
              // Body is a string representation of a JSON object:
              //   {
              //       "kind": "MetricValueList",
              //       "apiVersion": "custom.metrics.k8s.io/v1beta1",
              //       "metadata":
              //       {
              //           "selfLink": "..."
              //       },
              //       "items":
              //       [
              //           {
              //               "describedObject":
              //               {
              //                   "kind": "PersistentVolumeClaim",
              //                   "namespace": "kumori",
              //                   "name": "c0volume0",
              //                   "apiVersion": "/v1"
              //               },
              //               "metricName": "kubelet_volume_stats_capacity_bytes",
              //               "timestamp": "2021-10-27T18:12:59Z",
              //               "value": "1023303680"
              //           }
              //       ]
              //   }
              parsedBody = JSON.parse(body);
              if (parsedBody.items == null || parsedBody.items.length < 1) {
                // No metrics returned
                errMsg = "Unable to get PVC metrics data: no metrics returned.";
                this.logger.error(`${meth} ERROR: ${errMsg}`);
                return resolve(null);
              } else if (parsedBody.items.length > 1) {
                // More than one metric returned
                this.logger.warn(`${meth} More than one metric returned. Using the first item in the list.`);
                return resolve(parsedBody.items[0]);
              } else {
                // Exactly one metric returned
                return resolve(parsedBody.items[0]);
              }
            }
          });
        });
      }

      getPvcMetrics(namespace, pvcName) {
        var VOLUME_METRICS_NAMES, meth, metricLabel, metricName, promiseChain, pvcMetrics;
        meth = `k8sApiStub.getPvcMetrics(${namespace}, ${pvcName})`;
        this.logger.debug(meth);
        VOLUME_METRICS_NAMES = {
          availableBytes: 'kubelet_volume_stats_available_bytes',
          capacityBytes: 'kubelet_volume_stats_capacity_bytes'
        };
        pvcMetrics = {
          availableBytes: null,
          capacityBytes: null,
          usagePercent: null
        };
        promiseChain = q();
        for (metricName in VOLUME_METRICS_NAMES) {
          metricLabel = VOLUME_METRICS_NAMES[metricName];
          ((metricName, metricLabel) => {
            return promiseChain = promiseChain.then(() => {
              return this.getPvcMetric(namespace, pvcName, metricLabel).then(metricsResult => {
                pvcMetrics[metricName] = metricsResult.value;
                return true;
              });
            });
          })(metricName, metricLabel);
        }
        return promiseChain.then(() => {
          var avail, cap, rawUsagePercent;
          this.logger.info(`${meth} - Retrieved metrics: ${JSON.stringify(pvcMetrics)}`);
          // If values exist, calculate usage percent
          if (pvcMetrics.availableBytes != null && pvcMetrics.capacityBytes != null) {
            cap = pvcMetrics.capacityBytes;
            avail = pvcMetrics.availableBytes;
            rawUsagePercent = (cap - avail) / cap * 100;
            // Round percent to 2 decimals and convert to string to be consistent
            // with the metrics format (also strings)
            pvcMetrics.usagePercent = `${Math.round(rawUsagePercent * 100) / 100}`;
          }
          return pvcMetrics;
        });
      }

      aggregatePodMetric(rawPodMetric) {
        var cont, contData, i, len, podData, ref, ref1, ref2;
        podData = {
          namespace: rawPodMetric.metadata.namespace,
          containers: {},
          usage: {
            cpu: 0,
            memory: 0
          }
        };
        ref = rawPodMetric.containers;
        for (i = 0, len = ref.length; i < len; i++) {
          cont = ref[i];
          contData = {
            usage: {
              cpu: 0,
              memory: 0
            }
          };
          // Add container resources to pod totals
          // PROBLEM: they are K8s Quantities --> function to convert
          contData.usage.cpu = quantityToScalar(((ref1 = cont.usage) != null ? ref1.cpu : void 0) || 0);
          contData.usage.memory = quantityToScalar(((ref2 = cont.usage) != null ? ref2.memory : void 0) || 0);
          podData.usage.cpu += contData.usage.cpu;
          podData.usage.memory += contData.usage.memory;
          podData.containers[cont.name] = contData;
        }
        // Round to fix JS strange behaviour when adding decimal numbers
        podData.usage.cpu = Math.round(podData.usage.cpu * 100) / 100;
        podData.usage.memory = Math.round(podData.usage.memory * 100) / 100;
        return podData;
      }

      topNodes() {
        var MAINTENANCE_MARK_LABEL, MASTER_NODE_MARK_LABEL, allPods, meth, nodesMetrics, podsMetrics, result;
        meth = 'k8sApiStub.topNodes';
        this.logger.debug(`${meth}`);
        MASTER_NODE_MARK_LABEL = 'node-role.kubernetes.io/control-plane';
        MAINTENANCE_MARK_LABEL = 'kumori/maintenance';
        result = {};
        allPods = [];
        podsMetrics = {};
        nodesMetrics = {};
        // List all pods
        return this.listPods().then(podList => {
          allPods = podList;
          this.logger.debug(`${meth} All pods: ${JSON.stringify(allPods)}`);
          // List real-time metrics from all pods
          return this.getPodsMetrics();
        }).then(metrics => {
          podsMetrics = metrics || {};
          // List real-time metrics from all nodes
          return this.getNodesMetrics();
        }).then(metrics => {
          nodesMetrics = metrics || {};
          // List all nodes
          return this.listNodes();
        }).then(nodeList => {
          var availCPU, availMem, cont, contName, contUsage, i, j, k, len, len1, len2, measuredCPU, measuredMem, node, nodeData, nodeName, nodePods, pod, podCPUTotals, podData, podMemTotals, podName, ref, ref1, ref10, ref11, ref12, ref13, ref14, ref15, ref16, ref17, ref18, ref19, ref2, ref20, ref21, ref22, ref23, ref24, ref25, ref26, ref27, ref28, ref29, ref3, ref30, ref31, ref32, ref4, ref5, ref6, ref7, ref8, ref9, totalCPU, totalMem, totalPodCPULimit, totalPodCPURequest, totalPodMemLimit, totalPodMemRequest;
          this.logger.debug(`${meth} All nodes: ${JSON.stringify(nodeList)}`);
          for (i = 0, len = nodeList.length; i < len; i++) {
            node = nodeList[i];
            nodeName = (ref = node.metadata) != null ? ref.name : void 0;
            // console.log "#{meth} - NODE: #{nodeName}"
            nodeData = {
              role: null,
              maintenance: false,
              usage: {
                cpu: null,
                memory: null
              },
              pods: {},
              conditions: {},
              taints: []
            };
            // Mark it as Master or Worker
            if (MASTER_NODE_MARK_LABEL in node.metadata.labels) {
              nodeData.role = 'master';
            } else {
              nodeData.role = 'worker';
            }
            // Determine if node is in maintenance mode
            if (MAINTENANCE_MARK_LABEL in node.metadata.labels && node.metadata.labels[MAINTENANCE_MARK_LABEL] === 'true') {
              nodeData.maintenance = true;
            }
            // Take the actual metrics
            measuredCPU = ((ref1 = nodesMetrics[nodeName]) != null ? (ref2 = ref1.usage) != null ? ref2.cpu : void 0 : void 0) || 0;
            measuredMem = ((ref3 = nodesMetrics[nodeName]) != null ? (ref4 = ref3.usage) != null ? ref4.memory : void 0 : void 0) || 0;
            // Take the node total
            totalCPU = quantityToScalar((ref5 = node.status) != null ? (ref6 = ref5.capacity) != null ? ref6.cpu : void 0 : void 0);
            totalMem = quantityToScalar((ref7 = node.status) != null ? (ref8 = ref7.capacity) != null ? ref8.memory : void 0 : void 0);
            // Take the node available/allocatable
            availCPU = quantityToScalar((ref9 = node.status) != null ? (ref10 = ref9.allocatable) != null ? ref10.cpu : void 0 : void 0);
            availMem = quantityToScalar((ref11 = node.status) != null ? (ref12 = ref11.allocatable) != null ? ref12.memory : void 0 : void 0);
            // Calculate allocated from current scheduled pods
            totalPodCPURequest = 0.0;
            totalPodCPULimit = 0.0;
            totalPodMemRequest = 0;
            totalPodMemLimit = 0;
            nodePods = allPods.filter(function (pod) {
              var ref13, ref14;
              return ((ref13 = pod.spec) != null ? ref13.nodeName : void 0) === nodeName && ((ref14 = pod.status) != null ? ref14.phase : void 0) === 'Running';
            });
            // console.log "#{meth} - NODE PODS: #{JSON.stringify nodePods}"
            for (j = 0, len1 = nodePods.length; j < len1; j++) {
              pod = nodePods[j];
              podName = pod.metadata.name;
              podData = {
                namespace: pod.metadata.namespace,
                labels: pod.metadata.labels,
                phase: pod.status.phase,
                conditions: cleanConditions(pod.status.conditions),
                containers: {},
                // usage: podsMetrics[podName].usage
                usage: {}
              };
              ref14 = ((ref13 = pod.spec) != null ? ref13.containers : void 0) || [];
              // console.log "#{meth} - POD: #{pod.metadata.name}"
              for (k = 0, len2 = ref14.length; k < len2; k++) {
                cont = ref14[k];
                contName = cont.name;
                contUsage = {
                  cpu: {},
                  memory: {}
                };
                if (((ref15 = podsMetrics[podName]) != null ? (ref16 = ref15.containers[contName]) != null ? ref16.usage : void 0 : void 0) != null) {
                  contUsage.cpu.measured = podsMetrics[podName].containers[contName].usage.cpu;
                  contUsage.memory.measured = podsMetrics[podName].containers[contName].usage.memory;
                }
                if (cont.resources != null) {
                  if (cont.resources.requests != null) {
                    if ((ref17 = cont.resources.requests) != null ? ref17.cpu : void 0) {
                      contUsage.cpu.request = quantityToScalar((ref18 = cont.resources.requests) != null ? ref18.cpu : void 0);
                    }
                    if ((ref19 = cont.resources.requests) != null ? ref19.memory : void 0) {
                      contUsage.memory.request = quantityToScalar((ref20 = cont.resources.requests) != null ? ref20.memory : void 0);
                    }
                  }
                  if (cont.resources.limits != null) {
                    if ((ref21 = cont.resources.limits) != null ? ref21.cpu : void 0) {
                      contUsage.cpu.limit = quantityToScalar((ref22 = cont.resources.limits) != null ? ref22.cpu : void 0);
                    }
                    if ((ref23 = cont.resources.limits) != null ? ref23.memory : void 0) {
                      contUsage.memory.limit = quantityToScalar((ref24 = cont.resources.limits) != null ? ref24.memory : void 0);
                    }
                  }
                }
                podData.containers[contName] = {
                  usage: contUsage
                };
              }
              podCPUTotals = totalForResource(pod, 'cpu');
              totalPodCPURequest += podCPUTotals.request;
              totalPodCPULimit += podCPUTotals.limit;
              podMemTotals = totalForResource(pod, 'memory');
              totalPodMemRequest += podMemTotals.request;
              totalPodMemLimit += podMemTotals.limit;
              podData.usage = {
                cpu: {
                  measured: ((ref25 = podsMetrics[podName]) != null ? (ref26 = ref25.usage) != null ? ref26.cpu : void 0 : void 0) || 0,
                  request: podCPUTotals.request,
                  limit: podCPUTotals.limit
                },
                memory: {
                  measured: ((ref27 = podsMetrics[podName]) != null ? (ref28 = ref27.usage) != null ? ref28.memory : void 0 : void 0) || 0,
                  request: podMemTotals.request,
                  limit: podMemTotals.limit
                }
              };
              // Add pod info to Node pod list
              nodeData.pods[podName] = podData;
            }
            nodeData.usage.cpu = {
              measured: ((ref29 = nodesMetrics[nodeName]) != null ? (ref30 = ref29.usage) != null ? ref30.cpu : void 0 : void 0) || 0,
              capacity: totalCPU,
              alloctable: availCPU,
              requestTotal: Math.round(totalPodCPURequest * 100) / 100,
              limitTotal: Math.round(totalPodCPULimit * 100) / 100
            };
            nodeData.usage.memory = {
              measured: ((ref31 = nodesMetrics[nodeName]) != null ? (ref32 = ref31.usage) != null ? ref32.memory : void 0 : void 0) || 0,
              capacity: totalMem,
              allocatable: availMem,
              requestTotal: totalPodMemRequest,
              limitTotal: totalPodMemLimit
            };
            nodeData.conditions = cleanConditions(node.status.conditions);
            nodeData.taints = node.spec.taints || [];
            result[nodeName] = nodeData;
          }
          return result;
        }).catch(err => {
          this.logger.error(`${meth} ERROR: ${err.message || err}`);
          return q.reject(err);
        });
      }

      // LogOptions possible properties:
      //  - follow: <boolean>  (requires wStream to be set)
      //  - tailLines: <number>
      //  - sinceSeconds: <number>  (seconds)

      // It is mandatory to provide a WritableStream to avoid storing logs in memory.

      getPodLogs(namespace, podName, container = null, opts = {}, wStream) {
        var errMsg, meth;
        meth = `k8sApiStub.getPodLogs pod=${podName} - container=${container}`;
        this.logger.info(`${meth} - Options: ${JSON.stringify(opts)}`);
        // Check a stream is provided if follow is true
        if (wStream == null) {
          errMsg = 'Missing mandatory stream.';
          this.logger.error(`${meth} - ${errMsg}`);
          return q.reject(new Error(errMsg));
        }
        // Temporarily hard coded since AdmissionRestApi is not aware if namespaces
        if (namespace == null) {
          namespace = 'kumori';
        }
        return q.promise((resolve, reject) => {
          var abortLogRequest, done, k8sLogRequest;
          // Callback handler for resolving the promise if the K8sLog request ends.
          done = err => {
            if (err) {
              this.logger.info(`K8sLog request Done with error: ${err}`);
              return reject(new Error(`${err.message || err}`));
            } else {
              this.logger.info("K8sLog request DONE.");
              return resolve("K8sLog request Done.");
            }
          };
          if (!opts.follow) {
            // This scenario is simpler since K8s library handles the disconnection
            this.logger.debug(`${meth} - Starting K8sLog request (no follow)`);
            return k8sLogRequest = this.k8sLog.log(namespace, podName, container, wStream, done, opts);
          } else {
            // Reference to the ApiServer request object (for aborting it if needed)
            k8sLogRequest = null;
            // Handlers and utility method to detect if the stream is closed and
            // aborting the current ApiServer request, that otherwise would still be
            // connected.
            abortLogRequest = reason => {
              var err;
              this.logger.info(`${meth} - Writable stream event '${reason}' received. Closing ApiServer Log request`);
              try {
                if ((k8sLogRequest != null ? k8sLogRequest.abort : void 0) != null) {
                  k8sLogRequest.abort();
                }
              } catch (error1) {
                err = error1;
                this.logger.info(`${meth} - Error aborting: ${err.message} ${err.stack}`);
              }
              this.logger.info(`${meth} - ApiServer Log request closed.`);
              return resolve(`Writable stream ${reason}`);
            };
            // Logs must be followed (streamed) until the stream is closed
            wStream.on('end', () => {
              return abortLogRequest('end');
            });
            wStream.on('finish', () => {
              return abortLogRequest('finish');
            });
            wStream.on('close', () => {
              return abortLogRequest('close');
            });
            // Temporary intermediary Stream for testing
            //# st = new stream.Writable
            //#   write: (chunk, encoding, callback) ->
            //#     # console.log "--> k8sLog.log receiving data..."
            //#     if not wStream?
            //#       # Print without additional newline (logs already have newlines)
            //#       console.log "CHUNK: #{chunk.toString()}"
            //#       process.stdout.write chunk.toString()
            //#     else
            //#       console.log "CHUNK: #{chunk.toString()}"
            //#       wStream.write chunk.toString()
            //#     callback()

            // Only for non streamed responses (errors, etc)
            done = err => {
              if (err) {
                this.logger.info(`K8sLog request Done with error: ${err}`);
                return reject(new Error(`${err.message || err}`));
              } else {
                this.logger.info("K8sLog request DONE.");
                return resolve("K8sLog request Done.");
              }
            };
            this.logger.debug(`${meth} - Starting K8sLog request (with follow)`);
            return k8sLogRequest = this.k8sLog.log(namespace, podName, container, wStream, done, opts);
          }
        });
      }

      // console.log "#{meth} - AFTER K8sLog request"
      execPod(namespace, podName, container, command, tty, streams = {}) {
        var meth, resultPromise;
        meth = `k8sApiStub.execPod pod=${podName} - container=${container} - command='${JSON.stringify(command)}'`;
        this.logger.info(meth);
        // Temporarily hard coded since AdmissionRestApi is not aware if namespaces
        if (namespace == null) {
          namespace = 'kumori';
        }
        return resultPromise = q.promise((resolve, reject) => {
          var done, ref, ref1, ref2;
          // Create a new K8s command executor (K8s library)
          this.k8sExec = new k8s.Exec(this.kConfig);
          // Callback called by K8s library to indicate the exec process is finished.
          // Sample status values:
          // - On success, status is: { "metadata": {}, "status": "Success" }
          // - On failure:
          //   {
          //     "metadata": {},
          //     "status": "Failure",
          //     "message": "command terminated with non-zero exit code: Error executing in Docker Container: 126",
          //     "reason": "NonZeroExitCode",
          //     "details": {
          //       "causes": [ { "reason": "ExitCode", "message": "126" } ]
          //     }
          //   }
          done = status => {
            this.logger.info(`${meth} - Command finished, done called: ${JSON.stringify(status)}`);
            return resolve(status);
          };
          this.logger.info(`${meth} - Calling K8s ApiServer exec...`);
          return this.k8sExec.exec(namespace, podName, container, command, (ref = streams.out) != null ? ref : null, (ref1 = streams.err) != null ? ref1 : null, (ref2 = streams.in) != null ? ref2 : null, tty, done).then(k8sWS => {
            // Promise is resolved once the connection with K8s is established
            // The promise is resolved with a Websocket
            this.logger.info(`${meth} - K8s ApiServer exec session started (got ws)`);
            // Set a handler to detect client disconnection (end of 'in' stream)
            return streams.in.on('end', () => {
              this.logger.info(`${meth} - Detected 'in' stream end (disconnection). Resolving exec promise...`);
              return resolve({
                status: "Client disconnected"
              });
            });
            //#############################
            //#      FOR DEBUGGING       ##
            //#############################
            // # K8s Websocket event hadlers
            // k8sWS.onopen = () ->
            //   console.log "K8S WS onOpen"
            // k8sWS.onclose = (closeEvt) ->
            //   # A 'CloseEvent' contains a code, a reason and a target object
            //   # Websocket status codes info:
            //   #   https://tools.ietf.org/html/rfc6455#section-7.4.1
            //   if closeEvt.code is 1000
            //     console.log "K8S WS onClose - Normal exit (code: 1000)"
            //   else
            //     console.log "K8S WS onClose (closeEvt.code} -
            //                  Reason: #{closeEvt.reason})"
            // k8sWS.onerror = (err) ->
            //   console.log "K8S WS onError: #{err.message || err}"
            //   # ws.close()
            // # WARNING: implementing 'onmessage' will overwrite the library default
            // # implementation! Alternatively, set a listener to 'message' events.
            // # k8sWS.onmessage = (data) ->
            // #   streamNumber = data.data.readInt8(0)
            // #   cleanData = data.data.slice 1
            // #   console.log "K8S WS onMessage (stream #{streamNumber})"
            // #   process.stdout.write cleanData.toString()
            // #   ws.send data.data
            //#############################
            //#   END OF FOR DEBUGGING   ##
            //#############################
          }).catch(err => {
            var errMsg;
            errMsg = `Couldn't execute command '${command}' in pod ${podName} and container '${container != null ? container : 'default'}'. Error: ${err.message}`;
            this.logger.info(`${meth} - ${errMsg}`);
            return reject(new Error(errMsg));
          });
        });
      }

      execValidationCheck(podName, container) {
        var meth, namespace;
        meth = `k8sApiStub.execValidationCheck instance=${podName} - container=${container}`;
        this.logger.info(meth);
        namespace = 'kumori';
        return this.getPod(namespace, podName).then(pod => {
          var cont, found, i, len, ref, ref1;
          if (((ref = pod.status) != null ? ref.phase : void 0) !== 'Running') {
            throw new Error(`Instance ${podName} is not running.`);
          }
          if (container == null) {
            // Not specifying a container is only OK if Pod only has one container
            if (pod.spec.containers.length > 1) {
              throw new Error(`Instance ${podName} has several containers. A container must be specified.`);
            }
          } else {
            // If container was specified, it must exist in the Pod
            found = false;
            ref1 = pod.spec.containers;
            for (i = 0, len = ref1.length; i < len; i++) {
              cont = ref1[i];
              if (cont.name === container) {
                found = true;
                break;
              }
            }
            if (!found) {
              throw new Error(`Container ${container} not found in instance ${podName}`);
            }
          }
          return true;
        }).catch(err => {
          this.logger.warn(`${meth} - Failed validation: ${err.message}`);
          return q.reject(new Error(err.message));
        });
      }

      getControllerRevisions(namespace = null, labelSelector = null) {
        var meth, ns;
        ns = namespace || 'kumori';
        meth = `k8sApiStub.getControllerRevisions Namespace: ${ns} - Selector: ${JSON.stringify(labelSelector)}`;
        this.logger.info(meth);
        return (namespace != null ? this.k8sAppsApi.listNamespacedControllerRevision(namespace, null, null, null, null, labelSelector) : this.k8sAppsApi.listControllerRevisionForAllNamespaces(null, null, null, labelSelector)).then(res => {
          this.logger.info(`${meth} - Found ${res.body.items.length} ControllerRevisions.`);
          return res.body.items;
        }).catch(err => {
          var errMsg;
          errMsg = `Couldn't get ControllerRevision list: ${JSON.stringify(err)}`;
          this.logger.error(`${meth} - ${errMsg}`);
          return q.reject(new Error(errMsg));
        });
      }

      getControllerRevision(namespace, name) {
        var meth, ns;
        ns = namespace || 'kumori';
        meth = `k8sApiStub.getControllerRevision ${ns}/${name}`;
        this.logger.info(meth);
        return this.k8sAppsApi.readNamespacedControllerRevision(name, ns).then(res => {
          this.logger.info(`${meth} - Got ControllerRevision: ${JSON.stringify(res.body)}`);
          return res.body;
        }).catch(err => {
          var errMsg;
          errMsg = `Couldn't get ControllerRevision ${ns}/${name}: ${extractErrorMsg(err)}`;
          this.logger.warn(`${meth} - ${errMsg}`);
          return q.reject(new Error(errMsg));
        });
      }

      getDeployments(namespace = null, labelSelector = null) {
        var meth, ns;
        ns = namespace || 'kumori';
        meth = `k8sApiStub.getDeployments Namespace: ${ns} - Selector: ${JSON.stringify(labelSelector)}`;
        this.logger.info(meth);
        return (namespace != null ? this.k8sAppsApi.listNamespacedDeployment(namespace, null, null, null, null, labelSelector) : this.k8sAppsApi.listDeploymentForAllNamespaces(null, null, null, labelSelector)).then(res => {
          this.logger.info(`${meth} - Found ${res.body.items.length} Deployments.`);
          return res.body.items;
        }).catch(err => {
          var errMsg;
          errMsg = `Couldn't get Deployment list: ${JSON.stringify(err)}`;
          this.logger.error(`${meth} - ${errMsg}`);
          return q.reject(new Error(errMsg));
        });
      }

      getStatefulSets(namespace = null, labelSelector = null) {
        var meth, ns;
        ns = namespace || 'kumori';
        meth = `k8sApiStub.getStatefulSets Namespace: ${ns} - Selector: ${JSON.stringify(labelSelector)}`;
        this.logger.info(meth);
        return (namespace != null ? this.k8sAppsApi.listNamespacedStatefulSet(namespace, null, null, null, null, labelSelector) : this.k8sAppsApi.listStatefulSetForAllNamespaces(null, null, null, labelSelector)).then(res => {
          this.logger.info(`${meth} - Found ${res.body.items.length} StatefulSets.`);
          return res.body.items;
        }).catch(err => {
          var errMsg;
          errMsg = `Couldn't get StatefulSet list: ${JSON.stringify(err)}`;
          this.logger.error(`${meth} - ${errMsg}`);
          return q.reject(new Error(errMsg));
        });
      }

      patchDeployment(namespace, name, patch) {
        var elementId, meth, options;
        meth = 'k8sApiStub.patchDeployment';
        elementId = `${namespace}/deployments/${name}`;
        this.logger.info(`${meth} - Applying patch to: ${elementId}`);
        options = {
          headers: {
            'Content-type': 'application/json-patch+json'
          }
        };
        // 'Content-type': k8s.PatchUtils.PATCH_FORMAT_JSON_PATCH}};
        return this.k8sAppsApi.patchNamespacedDeployment(name, namespace, patch, void 0, void 0, ADMISSION_FIELD_MANAGER_NAME, void 0, void 0, options).then(res => {
          // pretty // dryRun // fieldValidation // force
          this.logger.info(`${meth} - Successfully patched object: ${elementId}`);
          // console.log "-----------------------------------------------------"
          // console.log "RESULT:"
          // console.log "#{JSON.stringify res, null, 2}"
          // console.log "-----------------------------------------------------"
          return true;
        }).catch(err => {
          var errMsg, ref;
          errMsg = ((ref = err.body) != null ? ref.message : void 0) || err.message || JSON.stringify(err);
          this.logger.error(`${meth} - Patching ${elementId}: ${errMsg}`);
          return q.reject(new Error(`${meth} - Replacing ${elementId}: ${errMsg}`));
        });
      }

      patchStatefulSet(namespace, name, patch) {
        var elementId, meth, options;
        meth = 'k8sApiStub.patchStatefulSet';
        elementId = `${namespace}/statefulsets/${name}`;
        this.logger.info(`${meth} - Applying patch to: ${elementId}`);
        options = {
          headers: {
            'Content-type': 'application/json-patch+json'
          }
        };
        // 'Content-type': k8s.PatchUtils.PATCH_FORMAT_JSON_PATCH}};
        return this.k8sAppsApi.patchNamespacedStatefulSet(name, namespace, patch, void 0, void 0, ADMISSION_FIELD_MANAGER_NAME, void 0, void 0, options).then(res => {
          // pretty // dryRun // fieldValidation // force
          this.logger.info(`${meth} - Successfully patched object: ${elementId}`);
          // console.log "-----------------------------------------------------"
          // console.log "RESULT:"
          // console.log "#{JSON.stringify res, null, 2}"
          // console.log "-----------------------------------------------------"
          return true;
        }).catch(err => {
          var errMsg, ref;
          errMsg = ((ref = err.body) != null ? ref.message : void 0) || err.message || JSON.stringify(err);
          this.logger.error(`${meth} - Patching ${elementId}: ${errMsg}`);
          return q.reject(new Error(`${meth} - Replacing ${elementId}: ${errMsg}`));
        });
      }

      evictPod(namespace, podName) {
        var body, elementId, meth;
        meth = 'k8sApiStub.evictPod';
        elementId = `${namespace}/pods/${podName}`;
        this.logger.info(`${meth} - Creating eviction for: ${elementId}`);
        body = {
          apiVersion: 'policy/v1beta1',
          kind: 'Eviction',
          metadata: {
            name: podName,
            namespace: namespace
          }
        };
        return this.k8sApi.createNamespacedPodEviction(podName, namespace, body, void 0, ADMISSION_FIELD_MANAGER_NAME, void 0, void 0, void 0).then(res => {
          this.logger.info(`${meth} - Successfully created eviction for: ${elementId}`);
          // console.log "-----------------------------------------------------"
          // console.log "RESULT:"
          // console.log "#{JSON.stringify res, null, 2}"
          // console.log "-----------------------------------------------------"
          return true;
        }).catch(err => {
          var errMsg, msg, ref, ref1, ref2, ref3, ref4;
          // console.log "ERROR: #{err}"
          // console.log "ERROR: #{err.body?.message}"
          // console.log "ERROR: #{err.message}"
          // console.log "ERROR: #{JSON.stringify err}"
          if (((ref = err.response) != null ? (ref1 = ref.body) != null ? ref1.message : void 0 : void 0) != null) {
            msg = (ref2 = err.response) != null ? (ref3 = ref2.body) != null ? ref3.message : void 0 : void 0;
            if (msg.toLowerCase().indexOf('disruption budget') > 0) {
              errMsg = 'Unable to evict pod due to resilience constraints.';
            } else {
              errMsg = msg;
            }
          } else {
            errMsg = ((ref4 = err.body) != null ? ref4.message : void 0) || err.message || JSON.stringify(err);
          }
          this.logger.error(`${meth} - Creating eviction for ${elementId}: ${errMsg}`);
          return q.reject(new Error(`Evicting ${podName}: ${errMsg}`));
        });
      }

      //#############################################################################
      //#              PUBLIC METHODS FOR MANAGING NON-KUMORI CRDs                 ##
      //#                                                                          ##
      //# - Initially used for managing Keycloak Operator CRDs                     ##
      //#############################################################################
      getCustomResource(group, version, namespace, plural, name) {
        var elementId, meth, missing;
        meth = 'k8sApiStub.getCustomResource';
        elementId = `${group}/${version}/${namespace}/${plural}/${name}`;
        this.logger.debug(`${meth} - ${elementId}`);
        missing = [];
        if (group === '') {
          missing.push('group');
        }
        if (version === '') {
          missing.push('version');
        }
        if (namespace === '') {
          missing.push('namespace');
        }
        if (plural === '') {
          missing.push('plural');
        }
        if (name === '') {
          missing.push('name');
        }
        if (missing.length !== 0) {
          return q.reject(new Error(`${meth} - Missing mandatory parameters: ${missing}`));
        }
        return this.k8sCustomApi.getNamespacedCustomObject(group, version, namespace, plural, name).then(res => {
          this.logger.info(`${meth} - Got object ${elementId}`);
          // console.log "-----------------------------------------------------"
          // console.log "ELEMENT:"
          // console.log "#{JSON.stringify res.body}"
          // console.log "-----------------------------------------------------"
          return res.body;
        }).catch(err => {
          var errMsg, ref, ref1;
          if (((ref = err.response) != null ? (ref1 = ref.body) != null ? ref1.reason : void 0 : void 0) === 'NotFound') {
            this.logger.info(`${meth} - Object ${elementId} not found.`);
            return q(null);
          } else {
            errMsg = `Getting object ${elementId}: ${JSON.stringify(err)}`;
            this.logger.error(`${meth} - ${errMsg}`);
            return q.reject(new Error(errMsg));
          }
        });
      }

      listCustomResource(group, version, namespace, plural, selector) {
        var e, meth, type, watchForChanges;
        meth = 'k8sApiStub.listCustomResource';
        type = `${group}/${version}/${namespace}/${plural}`;
        this.logger.debug(`${meth} - ${type} selector: ${JSON.stringify(selector)}`);
        watchForChanges = false;
        try {
          return this.k8sCustomApi.listNamespacedCustomObject(group, version, namespace, plural, void 0, void 0, void 0, void 0, selector, 0, void 0, void 0, 0, watchForChanges).then(res => {
            // pretty // allowWatchBookmarks // _continue // fieldSelector // labelSelector // limit // resourceVersion // resourceVersionMatch // timeoutSeconds // watch
            var ref;
            this.logger.info(`${meth} - Got ${res.body.items.length} objects.`);
            // @logger.debug "#{meth} - Elements: #{JSON.stringify res.body.items}"
            if (((ref = res.body) != null ? ref.items : void 0) != null) {
              return res.body.items;
            } else {
              throw new Error("List API call returned no item list.");
            }
          }).catch(err => {
            var errMsg;
            errMsg = `Getting '${type}' objects with selector ${JSON.stringify(selector)}: ${JSON.stringify(err)}`;
            this.logger.error(`${meth} - ${errMsg}`);
            return q.reject(new Error(errMsg));
          });
        } catch (error1) {
          e = error1;
          throw e;
        }
      }

      createCustomResource(group, version, namespace, plural, element) {
        var elementId, elementName, meth;
        meth = 'k8sApiStub.createCustomResource';
        elementName = element.metadata.name;
        elementId = `${group}/${version}/${namespace}/${plural}/${elementName}`;
        this.logger.info(`${meth} - Creating: ${elementId}`);
        return this.k8sCustomApi.createNamespacedCustomObject(group, version, namespace, plural, element, void 0, void 0, ADMISSION_FIELD_MANAGER_NAME).then(res => {
          this.logger.info(`${meth} - Successfully created object: ${elementId}`);
          // console.log "-----------------------------------------------------"
          // console.log "RESULT:"
          // console.log "#{JSON.stringify res, null, 2}"
          // console.log "-----------------------------------------------------"
          return true;
        }).catch(err => {
          var errMsg, ref;
          errMsg = ((ref = err.body) != null ? ref.message : void 0) || err.message || JSON.stringify(err);
          this.logger.error(`${meth} - Creating ${elementId}: ${errMsg}`);
          return q.reject(new Error(`${meth} - Creating ${elementId}: ${errMsg}`));
        });
      }

      replaceCustomResource(group, version, namespace, plural, element) {
        var elementId, elementName, meth;
        meth = 'k8sApiStub.replaceCustomResource';
        elementName = element.metadata.name;
        elementId = `${group}/${version}/${namespace}/${plural}/${elementName}`;
        this.logger.info(`${meth} - Replacing: ${elementId}`);
        return this.k8sCustomApi.replaceNamespacedCustomObject(group, version, namespace, plural, elementName, element, void 0, ADMISSION_FIELD_MANAGER_NAME).then(res => {
          this.logger.info(`${meth} - Successfully replaced object: ${elementId}`);
          // console.log "-----------------------------------------------------"
          // console.log "RESULT:"
          // console.log "#{JSON.stringify res, null, 2}"
          // console.log "-----------------------------------------------------"
          return true;
        }).catch(err => {
          var errMsg, ref;
          errMsg = ((ref = err.body) != null ? ref.message : void 0) || err.message || JSON.stringify(err);
          this.logger.error(`${meth} - Replacing ${elementId}: ${errMsg}`);
          return q.reject(new Error(`${meth} - Replacing ${elementId}: ${errMsg}`));
        });
      }

      saveCustomResource(group, version, namespace, plural, element) {
        var elementId, elementName, meth;
        meth = 'k8sApiStub.saveCustomResource';
        elementName = element.metadata.name;
        elementId = `${group}/${version}/${namespace}/${plural}/${elementName}`;
        this.logger.info(`${meth} - Saving ${elementId}`);
        // If element already exists, then it is replaced by the new one.
        // This requires to get the existing element current 'resourceVersion' and
        // send it along in the 'replace' request.

        // Try to read the element
        return this.getCustomResource(group, version, namespace, plural, elementName).then(existingElement => {
          if (existingElement != null) {
            this.logger.info(`${meth} - Element ${elementId} exists.`);
            // @logger.info "#{JSON.stringify existingElement}"
            element.setResourceVersion(existingElement.metadata.resourceVersion);
            return this.replaceCustomResource(group, version, namespace, plural, element);
          } else {
            this.logger.info(`${meth} - Element ${elementId} is new.`);
            return this.createCustomResource(group, version, namespace, plural, element);
          }
        });
      }

      deleteCustomResource(group, version, namespace, plural, name) {
        var elementId, meth, missing;
        meth = 'k8sApiStub.deleteCustomResource';
        elementId = `${group}/${version}/${namespace}/${plural}/${name}`;
        this.logger.debug(`${meth} - ${elementId}`);
        missing = [];
        if (group === '') {
          missing.push('group');
        }
        if (version === '') {
          missing.push('version');
        }
        if (namespace === '') {
          missing.push('namespace');
        }
        if (plural === '') {
          missing.push('plural');
        }
        if (name === '') {
          missing.push('name');
        }
        if (missing.length !== 0) {
          return q.reject(new Error(`${meth} - Missing mandatory parameters: ${missing}`));
        }
        return this.k8sCustomApi.deleteNamespacedCustomObject(group, version, namespace, plural, name, void 0, void 0, void 0, void 0, void 0, void 0).then(res => {
          // gracePeriodSeconds // orphanDependents // propagationPolicy // dryRun // body // options
          this.logger.info(`${meth} - Deleted object ${elementId}: ${JSON.stringify(res.body, null, 2)}`);
          // console.log "-----------------------------------------------------"
          // console.log "ELEMENT:"
          // console.log "#{JSON.stringify res.body}"
          // console.log "-----------------------------------------------------"
          return true;
        }).catch(err => {
          var errMsg, ref, ref1;
          if (((ref = err.response) != null ? (ref1 = ref.body) != null ? ref1.reason : void 0 : void 0) === 'NotFound') {
            this.logger.info(`${meth} - Object ${elementId} not found.`);
            return q(null);
          } else {
            errMsg = `Deleting object ${elementId}: ${JSON.stringify(err)}`;
            this.logger.error(`${meth} - ${errMsg}`);
            return q.reject(new Error(errMsg));
          }
        });
      }

      patchCustomResource(group, version, namespace, plural, elementName, patch) {
        var elementId, meth, options;
        meth = 'k8sApiStub.patchCustomResource';
        elementId = `${group}/${version}/${namespace}/${plural}/${elementName}`;
        this.logger.info(`${meth} - Applying patch to: ${elementId}`);
        options = {
          headers: {
            'Content-type': 'application/json-patch+json'
          }
        };
        // 'Content-type': k8s.PatchUtils.PATCH_FORMAT_JSON_PATCH}};
        return this.k8sCustomApi.patchNamespacedCustomObject(group, version, namespace, plural, elementName, patch, void 0, ADMISSION_FIELD_MANAGER_NAME, void 0, options).then(res => {
          this.logger.info(`${meth} - Successfully patched object: ${elementId}`);
          // console.log "-----------------------------------------------------"
          // console.log "RESULT:"
          // console.log "#{JSON.stringify res, null, 2}"
          // console.log "-----------------------------------------------------"
          return true;
        }).catch(err => {
          var errMsg, ref;
          errMsg = ((ref = err.body) != null ? ref.message : void 0) || err.message || JSON.stringify(err);
          this.logger.error(`${meth} - Patching ${elementId}: ${errMsg}`);
          return q.reject(new Error(`${meth} - Replacing ${elementId}: ${errMsg}`));
        });
      }

      //#############################################################################
      //#                           TESTING METHODS                                ##
      //#############################################################################
      altGetPodLogs(namespace, podName) {
        var container, follow, meth, name;
        meth = `k8sApiStub.altGetPodLogs pod=${podName} - container=${container}`;
        name = "kucontroller-b8bd46bff-j47sw";
        namespace = 'kumori';
        container = null;
        follow = false;
        return this.k8sApi.readNamespacedPodLog(name, namespace, container, follow).then(res => {
          return [];
          // Logs are returned as body text
          // console.log "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
          // console.log "BODY:"
          // console.log res.body
          // console.log "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
        }).catch(err => {
          var errMsg;
          errMsg = `Getting logs for Pod ${name}: ${JSON.stringify(err)}`;
          this.logger.error(`${meth} - ${errMsg}`);
          return q.reject(new Error(errMsg));
        });
      }

      getKumoriElements(plural) {
        var group, meth, name, namespace, version;
        meth = 'k8sApiStub.getKumoriElement';
        this.logger.debug(`${meth} - plural=${plural}`);
        group = 'kumori.systems';
        version = 'v1';
        namespace = 'kumori';
        name = '';
        return this.k8sCustomApi.getNamespacedCustomObject(group, version, namespace, plural, name).then(res => {
          this.logger.info(`${meth} - Got ${res.body.items.length} objects: ${JSON.stringify(res.body.items, null, 2)}`);
          // console.log "-----------------------------------------------------"
          // console.log "KUMORI ELEMENT:"
          // console.log "#{JSON.stringify res.body.items}"
          // console.log "-----------------------------------------------------"
          return res.body.items;
        }).catch(err => {
          var errMsg;
          errMsg = `Getting '${plural}' objects: ${JSON.stringify(err)}`;
          this.logger.error(`${meth} - ${errMsg}`);
          return q.reject(new Error(errMsg));
        });
      }

      listEvents() {
        var meth;
        meth = "k8sApiStub.listEvents";
        this.logger.info(meth);
        return this.k8sApi.listEventForAllNamespaces().then(res => {
          this.logger.info(`${meth} - Got ${res.body.items.length} objects.`);
          // console.log "-----------------------------------------------------"
          // console.log "LISTED EVENTS:"
          // console.log "#{JSON.stringify res.body}"
          // console.log "-----------------------------------------------------"
          return res.body.items;
        }).catch(err => {
          var errMsg, ref;
          errMsg = "Listing events: " + (((ref = err.body) != null ? ref.message : void 0) || err.message || JSON.stringify(err));
          this.logger.error(`${meth} - ${errMsg}`);
          return q.reject(new Error(`${meth} - ${errMsg}`));
        });
      }

      getCustomResourceObject() {
        var group, name, plural, version;
        group = 'kumori.systems';
        version = 'v1';
        plural = 'kumoricomponents';
        name = '';
        return this.k8sCustomApi.getClusterCustomObject(group, version, plural, name).then(res => {
          // console.log "-----------------------------------------------------"
          // console.log "CUSTOM RESOURCES:"
          // console.log "#{JSON.stringify res.body}"
          // console.log "-----------------------------------------------------"
          return true;
        }).catch(err => {
          var errMsg;
          // console.log "ERROR GETTING CUSTOM RESOURCES: #{err.message || err}"
          // console.log "ERROR: #{JSON.stringify err}"
          errMsg = `Listing custom resources: ${err.message || err}`;
          this.logger.error(`${meth} - ${errMsg}`);
          return q.reject(new Error(`${meth} - ${errMsg}`));
        });
      }

    };

    // test: (multiYaml) ->
    //   data = k8s.loadAllYaml multiYaml
    //   console.log "-----------------------------------------------------"
    //   console.log "LOADED FROM MULTI_YAML_STRING: #{JSON.stringify data}"
    //   console.log "-----------------------------------------------------"
    //   i = 0
    //   for item in data
    //     i++
    //     console.log "ITEM #{i}"
    //     console.log "API VERSION : #{item.apiVersion}"
    //     console.log "KIND        : #{item.kind}"
    //     console.log "METADATA    :"
    //     for metaKey, metaValue of item.metadata
    //       if metaKey isnt 'labels'
    //         console.log "  - #{metaKey}: #{metaValue}"
    //       else
    //         console.log "  - labels:"
    //         for labelName, labelValue of item.metadata.labels
    //           console.log "    - #{labelName}: #{labelValue}"
    //     console.log "-----------------------------------------------------"

    // printPodList: (podList) ->
    //   for item in (podList.items || [])
    //     console.log "-----------------------------------------------------"
    //     console.log "POD:"
    //     console.log "  Metadata: #{JSON.stringify item.metadata}"
    //     console.log "  Spec    : #{JSON.stringify item.spec}"
    //     console.log "  Status  : #{JSON.stringify item.status}"
    //     console.log "-----------------------------------------------------"

    //#############################################################################
    //#                              HELPER FUNCTIONS                            ##
    //#############################################################################
    quantityToScalar = function (quantity) {
      var cleanNumberStr, num, number;
      if (quantity == null) {
        return 0;
      }
      if (quantity === 0) {
        return 0;
      }
      if (quantity.endsWith('m')) {
        // Rounded to two decimals
        cleanNumberStr = quantity.substr(0, quantity.length - 1);
        number = parseInt(cleanNumberStr, 10) / 1000.0;
        return Math.round(number * 100) / 100;
      }
      if (quantity.endsWith('Ki')) {
        cleanNumberStr = quantity.substr(0, quantity.length - 2);
        return parseInt(cleanNumberStr, 10) * 1024;
      }
      if (quantity.endsWith('K')) {
        cleanNumberStr = quantity.substr(0, quantity.length - 1);
        return parseInt(cleanNumberStr, 10) * 1000;
      }
      if (quantity.endsWith('Mi')) {
        cleanNumberStr = quantity.substr(0, quantity.length - 2);
        return parseInt(cleanNumberStr, 10) * 1024 * 1024;
      }
      if (quantity.endsWith('M')) {
        cleanNumberStr = quantity.substr(0, quantity.length - 1);
        return parseInt(cleanNumberStr, 10) * 1000 * 1000;
      }
      if (quantity.endsWith('Gi')) {
        cleanNumberStr = quantity.substr(0, quantity.length - 2);
        return parseInt(cleanNumberStr, 10) * 1024 * 1024 * 1024;
      }
      if (quantity.endsWith('G')) {
        cleanNumberStr = quantity.substr(0, quantity.length - 1);
        return parseInt(cleanNumberStr, 10) * 1024 * 1000 * 1000;
      }
      if (quantity.endsWith('Ti')) {
        cleanNumberStr = quantity.substr(0, quantity.length - 2);
        return parseInt(cleanNumberStr, 10) * 1024 * 1024 * 1024 * 1024;
      }
      if (quantity.endsWith('T')) {
        cleanNumberStr = quantity.substr(0, quantity.length - 1);
        return parseInt(cleanNumberStr, 10) * 1024 * 1000 * 1000 * 1000;
      }
      if (quantity.endsWith('Pi')) {
        cleanNumberStr = quantity.substr(0, quantity.length - 2);
        return parseInt(cleanNumberStr, 10) * 1024 * 1024 * 1024 * 1024 * 1024;
      }
      if (quantity.endsWith('P')) {
        cleanNumberStr = quantity.substr(0, quantity.length - 1);
        return parseInt(cleanNumberStr, 10) * 1024 * 1000 * 1000 * 1000 * 1000;
      }
      num = parseInt(quantity, 10);
      if (isNaN(num)) {
        throw new Error(`Unknown quantity ${quantity}`);
      } else {
        return num;
      }
    };

    totalForResource = function (pod, resource) {
      var cont, i, len, ref, ref1, total;
      total = {
        request: 0,
        limit: 0,
        resourceType: resource
      };
      ref1 = ((ref = pod.spec) != null ? ref.containers : void 0) || [];
      for (i = 0, len = ref1.length; i < len; i++) {
        cont = ref1[i];
        if (cont.resources != null) {
          if (cont.resources.requests != null) {
            total.request += quantityToScalar(cont.resources.requests[resource]);
          }
          if (cont.resources.limits != null) {
            total.limit += quantityToScalar(cont.resources.limits[resource]);
          }
        }
      }
      return total;
    };

    cleanConditions = function (originalConditions) {
      var conditions, i, len, oc, ref;
      conditions = {};
      ref = originalConditions || [];
      for (i = 0, len = ref.length; i < len; i++) {
        oc = ref[i];
        conditions[oc.type] = oc.status;
      }
      return conditions;
    };

    extractErrorMsg = function (k8sApiError) {
      var body, e, errMsg, ref, statusCodeMsg;
      errMsg = 'Unknown error (unable to determine error message)';
      if (k8sApiError == null) {
        errMsg = 'Unknown error (API returned no info)';
      } else if ('string' === typeof k8sApiError) {
        errMsg = k8sApiError;
      } else {
        if (k8sApiError.statusCode != null && k8sApiError.statusCode !== '') {
          statusCodeMsg = ` (StatusCode ${k8sApiError.statusCode})`;
        } else {
          statusCodeMsg = '';
        }
        if (k8sApiError.message != null) {
          errMsg = k8sApiError.message;
        } else if (((ref = k8sApiError.response) != null ? ref.body : void 0) != null) {
          body = k8sApiError.response.body;
          if (body.message != null && body.reason != null) {
            errMsg = `${body.reason}: ${body.message}`;
          } else {
            try {
              errMsg = `Original error body: ${JSON.stringify(body)}`;
            } catch (error1) {
              e = error1;
              errMsg = "Original error body could not be parsed";
            }
          }
        } else {
          try {
            errMsg = `Original error object: ${JSON.stringify(k8sApiError)}`;
          } catch (error1) {
            e = error1;
            errMsg = "Original error object could not be parsed";
          }
        }
        errMsg = errMsg + statusCodeMsg;
      }
      return errMsg;
    };

    return K8sApiStub;
  }.call(this);

  module.exports = K8sApiStub;
}).call(undefined);