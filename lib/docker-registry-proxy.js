'use strict';

(function () {
  /*
  * Copyright 2022 Kumori Systems S.L.
   * Licensed under the EUPL, Version 1.2 or – as soon they
    will be approved by the European Commission - subsequent
    versions of the EUPL (the "Licence");
   * You may not use this work except in compliance with the
    Licence.
   * You may obtain a copy of the Licence at:
     https://joinup.ec.europa.eu/software/page/eupl
   * Unless required by applicable law or agreed to in
    writing, software distributed under the Licence is
    distributed on an "AS IS" basis,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
    express or implied.
   * See the Licence for the specific language governing
    permissions and limitations under the Licence.
   */
  var DOCKER_DEFAULT_NAMESPACE, DOCKER_DEFAULT_REGISTRY, DOCKER_DEFAULT_TAG, DOCKER_HUB_ALT_REGISTRY, DOCKER_REGISTRY_URL, DockerRegistryProxy, path, q, querystring, request;

  q = require('q');

  path = require('path');

  request = require('request');

  querystring = require('querystring');

  DOCKER_DEFAULT_REGISTRY = "docker.io";

  DOCKER_DEFAULT_NAMESPACE = "library";

  DOCKER_DEFAULT_TAG = "latest";

  DOCKER_REGISTRY_URL = "index.docker.io";

  DOCKER_HUB_ALT_REGISTRY = "registry.hub.docker.com";

  DockerRegistryProxy = class DockerRegistryProxy {
    // Configuration object is expected to have the following format:
    //   "dockerRegistry": {
    //     "username": "dockerhub-username",
    //     "password": "dockerhub-password",
    //     "mirror": "https://docker-registry.example.com"
    //   }
    constructor(config) {
      var meth;
      meth = 'DockerRegistryProxy.constructor';
      if (config.mirror != null && config.mirror !== '') {
        this.dockerRegistryMirror = config.mirror;
      } else {
        this.dockerRegistryMirror = null;
      }
      if (config.dockerHubUsername != null && config.dockerHubUsername !== '') {
        this.dockerHubUsername = config.dockerHubUsername;
      } else {
        this.dockerHubUsername = null;
      }
      if (config.dockerHubPassword != null && config.dockerHubPassword !== '') {
        this.dockerHubPassword = config.dockerHubPassword;
      } else {
        this.dockerHubPassword = null;
      }
      if (this.dockerRegistryMirror != null && this.dockerHubUsername != null) {
        this.logger.warn(`${meth} Docker registry configuration includes both a registry mirror and DockerHub credentials. Credentials will be ignored.`);
      }
      if (!this.logger) {
        this.logger = {
          error: console.log,
          warn: console.log,
          info: console.log,
          debug: console.log
        };
      }
      this.logger.info(`${meth} CONFIG: ${JSON.stringify(config)}`);
    }

    normalizeDockerImage(imageData) {
      var dockerImage, errMsg, meth, nameParts;
      meth = 'DockerRegistryProxy.normalizeDockerImage()';
      this.logger.info(`${meth} Docker image data: ${JSON.stringify(imageData)}`);
      // Check image tag string for blank spaces at the beginning or at the end
      if (imageData.tag.startsWith(' ') || imageData.tag.endsWith(' ')) {
        errMsg = `Image tag cannot start or end with spaces: '${imageData.tag}'`;
        this.logger.warn(`${meth} - ${errMsg}`);
        throw new Error(errMsg);
      }
      dockerImage = {
        hub: imageData.hub.name,
        secret: imageData.hub.secret,
        name: null,
        ref: null
      };
      // If no Docker registry is specified, default to Docker Hub (docker.io)
      if (dockerImage.hub == null || dockerImage.hub === '') {
        dockerImage.hub = DOCKER_DEFAULT_REGISTRY;
      } else if (dockerImage.hub === DOCKER_HUB_ALT_REGISTRY) {
        // Fix for registry mirror not allowing pull from 'registry.hub.docker.com'
        dockerImage.hub = DOCKER_DEFAULT_REGISTRY;
      }
      // NOT NECESSARY ANYMORE (13/01/2021) - LEFT FOR REFERENCE
      // Overwrite the value in the original data
      // imageData.hub.name = DOCKER_DEFAULT_REGISTRY

      // Extract tag or set default to 'latest'
      nameParts = imageData.tag.split(':');
      if (nameParts.length === 2) {
        dockerImage.name = nameParts[0];
        dockerImage.ref = nameParts[1];
      } else {
        dockerImage.name = nameParts[0];
        dockerImage.ref = DOCKER_DEFAULT_TAG;
      }
      // If image is from DockerHub and has no namespace, add default 'library'
      if (dockerImage.hub === DOCKER_DEFAULT_REGISTRY) {
        nameParts = dockerImage.name.split('/');
        if (nameParts.length === 1) {
          dockerImage.name = DOCKER_DEFAULT_NAMESPACE + '/' + nameParts[0];
        }
      }
      this.logger.info(`${meth} Normalized: ${JSON.stringify(dockerImage)}`);
      return dockerImage;
    }

    // Checks if a docker image exists in a Docker repository.

    // If conditions are met to use a registry mirror (image is hosted on
    // DockerHub, a mirror configured and image credentials have not been
    // provided), a first attempt at validating the image is done via the mirror.
    // In any other case or if the mirror validation fails, the image is validated
    // using its corresponding registry.
    checkImage(registryURL, imageName, imageRef, username = null, password = null, token = null) {
      var meth, useMirror;
      if (registryURL == null || registryURL === '') {
        registryURL = DOCKER_DEFAULT_REGISTRY;
        this.logger.info(`${meth} - Replaced empty registry URL: ${registryURL}`);
      }
      meth = `DockerRegistryProxy.checkImage ${registryURL}/${imageName}:${imageRef}`;
      this.logger.info(`${meth} - Token? ${token != null} - Username: ${username}`);
      useMirror = false;
      if (registryURL === DOCKER_DEFAULT_REGISTRY && this.dockerRegistryMirror != null && username == null && password == null) {
        useMirror = true;
      }
      return (useMirror ? (this.logger.info(`${meth} - Trying to validate image using configured mirror.`), this.performImageCheck(registryURL, imageName, imageRef, null, null, null, true)) : q(false)).then(validated => {
        // Use mirror
        if (validated) {
          return true;
        } else {
          this.logger.info(`${meth} - Trying to validate image (no mirror).`);
          return this.performImageCheck(registryURL, imageName, imageRef, username, password, null, false); // Do not use mirror
        }
      });
    }

    // Perform the actual image check according to the configuration (useMirror).

    // This method takes care of contacting the registry and authenticated when
    // and where required by the registry response.
    performImageCheck(registryURL, imageName, imageRef, username = null, password = null, token = null, useMirror = false) {
      var meth;
      meth = `DockerRegistryProxy.performImageCheck ${registryURL}/${imageName}:${imageRef}`;
      this.logger.info(`${meth} - Token? ${token != null} - Username: ${username} - Use mirror? ${useMirror}`);
      // If registry is official docker hub (docker.io), do some configuration
      // adjustments to use the correct values:

      // - if credentials (username/password) are provided
      //   - always use DockerHub registry (index.docker.io)
      // - if credentials are NOT provided
      //   - if a mirror is configured and useMirror is true
      //     - use the mirror URL
      //   - if a mirror is NOT configured
      //     - use DockerHub registry (index.docker.io)
      //     - if DockerHub credentials are configured in Admission use them
      if (registryURL === DOCKER_DEFAULT_REGISTRY) {
        if (username != null && password != null) {
          // DockerHub credentials provided, use DockerHub registry
          registryURL = DOCKER_REGISTRY_URL;
          this.logger.info(`${meth} - Replaced DockerHub registry URL: ${registryURL}`);
        } else {
          // No DockerHub credentials provided
          if (this.dockerRegistryMirror != null && useMirror) {
            // Registry mirror is configured, use it
            registryURL = this.dockerRegistryMirror;
            this.logger.info(`${meth} - Replaced DockerHub registry URL to use mirror: ${registryURL}`);
          } else {
            // Registry mirror not configured, use DockerHub registry
            registryURL = DOCKER_REGISTRY_URL;
            this.logger.info(`${meth} - Replaced DockerHub registry URL: ${registryURL}`);
            // If Admission configuration includes DockerHub credentials, use them
            if (this.dockerHubUsername != null && this.dockerHubPassword != null) {
              username = this.dockerHubUsername;
              password = this.dockerHubPassword;
              this.logger.info(`${meth} - Replaced DockerHub username: ${username}`);
              this.logger.info(`${meth} - Replaced DockerHub password: <hidden>`);
            }
          }
        }
      }
      // Make sure the registry URL contains a protocol section (default to https)
      if (!registryURL.startsWith('http://') && !registryURL.startsWith('https://')) {
        registryURL = 'https://' + registryURL;
      }
      return q.promise((resolve, reject) => {
        var getOpts;
        // 'Accept' header has been added due to some images not working OK with
        // the default type.
        getOpts = {
          method: 'GET',
          headers: {
            'Accept': 'application/vnd.oci.image.index.v1+json, ' + 'application/vnd.oci.image.manifest.v1+json,' + 'application/vnd.docker.distribution.manifest.v2+json,' + 'application/vnd.docker.distribution.manifest.list.v2+json'
          },
          url: `${registryURL}/v2/${imageName}/manifests/${imageRef}`
        };
        if (token != null && token !== '') {
          getOpts.auth = {
            bearer: token
          };
        }
        this.logger.info(`${meth} Trying to get image manifest from URL: ${getOpts.url}`);
        return request(getOpts, (error, response, body) => {
          var parseError, parsedBody, parsedWAH, ref, ref1, wwwAuthHeader;
          this.logger.info(`${meth} Response status code: ${response != null ? response.statusCode : void 0}`);
          if (error) {
            this.logger.error(`${meth} Unable to get image manifest (request error). ERROR: ${error.message}`);
            return resolve(false);
          } else if (response.statusCode === 200) {
            this.logger.info(`${meth} Got image manifest (Status code 200 OK)`);
            // @logger.debug "#{meth} HEADERS: #{JSON.stringify response.headers}"
            parsedBody = null;
            try {
              parsedBody = JSON.parse(body);
            } catch (error1) {
              parseError = error1;
              // The request returns a non-JSON response, probably a wrong URL
              this.logger.error(`${meth} Docker registry returned a non-JSON response. Body: ${JSON.stringify(parsedBody)}`);
              resolve(false);
            }
            if (parsedBody.schemaVersion == null) {
              // The JSON response does not have the required properties
              this.logger.error(`${meth} Docker registry returned a non-compliant response. Body: ${JSON.stringify(parsedBody)}`);
              resolve(false);
            }
            // @logger.debug "#{meth} BODY: #{JSON.stringify parsedBody}"
            return resolve(true);
          } else if (response.statusCode === 404) {
            this.logger.info(`${meth} Got Status code: 404 Not Found`);
            // If image doesn't exist, a detailed response is received. Check it.
            // Example body:
            //   {
            //     "errors": [
            //       {
            //         "code": "MANIFEST_UNKNOWN",
            //         "message": "manifest unknown",
            //         "detail": {
            //           "Tag": "0.0.144"
            //         }
            //       }
            //     ]
            //   }

            // NOTE (ticket303): Docker Hub behaves differently for multilevel
            // image requests (not supported by their registry) and return an ugly
            // HTTP 404 error page.
            parsedBody = null;
            try {
              parsedBody = JSON.parse(body);
            } catch (error1) {
              parseError = error1;
              // For example, multilevel image request to DockerHub
              this.logger.info(`${meth} Docker registry returned a non-JSON response.`);
              parsedBody = "Non-JSON response";
            }
            if ((parsedBody != null ? (ref = parsedBody.errors) != null ? (ref1 = ref[0]) != null ? ref1.code : void 0 : void 0 : void 0) === 'MANIFEST_UNKNOWN') {
              this.logger.error(`${meth} Unable to get image manifest (404 Not Found) Error: ${parsedBody.errors[0].message}`);
              return resolve(false);
            } else {
              this.logger.error(`${meth} Unable to get image manifest (404 Not Found with unexpected response body). Body: ${JSON.stringify(parsedBody)}`);
              return resolve(false);
            }
          } else if (response.statusCode === 401) {
            this.logger.info(`${meth} Got Status code: 401 Unauthorized.`);
            // This might mean that it is the first unauthorized attempt, necessary
            // to retrieve authentication challenge details in the response (part
            // of the Registry authentication  protocol).
            if (token != null) {
              // If token was present, it was not the initial attempt, but a real
              // failure.
              this.logger.error(`${meth} Unable to get image (401 Unauthorized in request with token)`);
              return resolve(false);
            } else if (response.headers['www-authenticate'] == null) {
              this.logger.error(`${meth} Unable to get image manifest (No authentication header returned). Make sure the image ${registryURL}/${imageName}:${imageRef} exists.`);
              return resolve(false);
            } else {
              // It was the initial attempt. Login, get a token then retry
              this.logger.info(`${meth} Checking www-authenticate header...`);
              // @logger.debug "#{meth} HEADERS: #{JSON.stringify response.headers}"
              wwwAuthHeader = response.headers['www-authenticate'];
              this.logger.info(`${meth} Found www-authenticate header: ${wwwAuthHeader}`);
              // Split www-authenticate header in parts. It looks like this:
              //   Bearer realm="<auth-endpoint",service="<auth-service>",scope="<auth_scope>"
              // Example:
              //   Bearer realm="https://auth.docker.io/token",service="registry.docker.io",scope="repository:kumori/calccachecache:pull"
              parsedWAH = this.parseWAH(wwwAuthHeader);
              this.logger.info(`${meth} Parsed www-authenticate header: ${JSON.stringify(parsedWAH)}`);
              this.logger.info(`${meth} Performing authentication login...`);
              return this.authenticate(parsedWAH, username, password).then(token => {
                this.logger.info(`${meth} - Got auth token. Retrying to get manifest.`);
                // Repeat the check but including the obtained token
                return this.performImageCheck(registryURL, imageName, imageRef, null, null, token, useMirror);
              }).then(exists => {
                return resolve(exists);
              }).catch(err => {
                this.logger.error(`${meth} Unable to get image manifest. Error: ${err.message}`);
                return resolve(false);
              });
            }
          } else if (response.statusCode === 429) {
            this.logger.error(`${meth} Unable to get image manifest (RateLimit exceeded - status code ${response.statusCode}`);
            return resolve(false);
          } else {
            this.logger.error(`${meth} Unable to get image manifest (unexpected HTTP status code ${response.statusCode}`);
            return resolve(false);
          }
        });
      });
    }

    authenticate(authConfig, username, password) {
      var meth;
      meth = "DockerRegistryProxy.authenticate";
      this.logger.info(`${meth} - Auth config: ${JSON.stringify(authConfig)}`);
      return q.promise((resolve, reject) => {
        var getOpts, getParams, paramsString;
        getParams = {
          service: authConfig.service,
          scope: authConfig.scope
        };
        paramsString = querystring.stringify(getParams);
        this.logger.info(`${meth} - Auth URL: ${authConfig.realm}?${paramsString}`);
        getOpts = {
          method: 'GET',
          url: `${authConfig.realm}?${paramsString}`
        };
        if (username != null && password != null && username !== '' && password !== '') {
          getOpts.auth = {
            user: username,
            pass: password,
            sendImmediately: true
          };
        }
        this.logger.info(`${meth} Sending authentication request...`);
        return request(getOpts, (error, response, body) => {
          var errMsg, parsedBody;
          this.logger.info(`${meth} Response status code: ${response != null ? response.statusCode : void 0}`);
          if (error) {
            errMsg = `Unable to authenticate: ${error.message} ${error.stack}`;
            this.logger.error(`${meth} ERROR: ${errMsg}`);
            return reject(new Error(errMsg));
          } else if (response.statusCode !== 200) {
            errMsg = `Unable to authenticate: unexpected HTTP status code ${response.statusCode}`;
            this.logger.error(`${meth} ERROR: ${errMsg}`);
            return reject(new Error(errMsg));
          } else {
            // Login OK, extract token from response body
            this.logger.info(`${meth} Got status code: 200 OK.`);
            // @logger.debug "#{meth} HEADERS: #{JSON.stringify response.headers}"
            parsedBody = JSON.parse(body);
            // @logger.debug "#{meth} BODY: #{JSON.stringify parsedBody}"
            if ((parsedBody != null ? parsedBody.token : void 0) != null) {
              this.logger.info(`${meth} Got token!`);
              return resolve(parsedBody.token);
            } else if ((parsedBody != null ? parsedBody.access_token : void 0) != null) {
              this.logger.info(`${meth} Got token!`);
              return resolve(parsedBody.access_token);
            } else {
              errMsg = `Unable to authenticate: no token or access_token property found in successful response body. BODY: ${JSON.stringify(parsedBody)}`;
              this.logger.error(`${meth} ERROR: ${errMsg}`);
              return reject(new Error(errMsg));
            }
          }
        });
      });
    }

    parseWAH(wwwAthenticateHeader) {
      var data, errMsg, i, len, meth, parts, sp, spparts, subparts;
      meth = "DockerRegistryProxy.parseWAH";
      this.logger.info(`${meth} Header: ${wwwAthenticateHeader}`);
      parts = wwwAthenticateHeader.split(' ');
      if (parts.length !== 2) {
        errMsg = "Unable to parse www-authenticate header (wrong number of parts)";
        this.logger.error(`${meth} - ERROR: ${errMsg}`);
        throw new Error(errMsg);
      }
      if (parts[0].toLowerCase() !== 'bearer') {
        errMsg = `Unable to parse www-authenticate header (unexpected type '${parts[0]}')`;
        this.logger.error(`${meth} - ERROR: ${errMsg}`);
        throw new Error(errMsg);
      }
      subparts = parts[1].split(',');
      if (subparts.length !== 3) {
        errMsg = "Unable to parse www-authenticate header (wrong number of subparts)";
        this.logger.error(`${meth} - ERROR: ${errMsg}`);
        throw new Error(errMsg);
      }
      data = {};
      for (i = 0, len = subparts.length; i < len; i++) {
        sp = subparts[i];
        // spparts = sp.split /([",=])/
        spparts = sp.split(/[",=]/);
        spparts = spparts.filter(function (x) {
          return x;
        });
        if (spparts.length !== 2) {
          errMsg = "Unable to parse www-authenticate header (wrong number of sub-subparts)";
          this.logger.error(`${meth} - ERROR: ${errMsg}`);
          throw new Error(errMsg);
        }
        data[spparts[0].toLowerCase()] = spparts[1].toLowerCase();
      }
      // console.log "WAH DATA: #{JSON.stringify data, null, 2}"
      return data;
    }

  };

  module.exports = DockerRegistryProxy;
}).call(undefined);