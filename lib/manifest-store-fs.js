'use strict';

(function () {
  /*
  * Copyright 2022 Kumori Systems S.L.
   * Licensed under the EUPL, Version 1.2 or – as soon they
    will be approved by the European Commission - subsequent
    versions of the EUPL (the "Licence");
   * You may not use this work except in compliance with the
    Licence.
   * You may obtain a copy of the Licence at:
     https://joinup.ec.europa.eu/software/page/eupl
   * Unless required by applicable law or agreed to in
    writing, software distributed under the Licence is
    distributed on an "AS IS" basis,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
    express or implied.
   * See the Licence for the specific language governing
    permissions and limitations under the Licence.
   */
  var DEFAULT_MANIFEST_FILENAME, ManifestStore, ManifestStoreFS, fs, mkdirp, path, q, rmdir, url;

  fs = require('fs');

  url = require('url');

  path = require('path');

  mkdirp = require('mkdirp');

  rmdir = require('rmdir');

  q = require('q');

  ManifestStore = require('./manifest-store');

  DEFAULT_MANIFEST_FILENAME = 'manifest.json';

  ManifestStoreFS = class ManifestStoreFS extends ManifestStore {
    constructor(options) {
      super('FS');
      this.storePath = options.storePath;
    }

    init() {
      return q.promise((resolve, reject) => {
        // Make sure specified storage directory exists (synchronously)
        return mkdirp(this.storePath, function (err) {
          if (err) {
            return reject(new Error('Initializing Manifest Store: ' + err.message));
          } else {
            return resolve(this.storepath);
          }
        });
      });
    }

    terminate(deleteStorage = false) {
      // Nothing to do in Filew System storage
      return q(true);
    }

    getManifest(manifestURN) {
      return q.promise((resolve, reject) => {
        var err, filename, parsed;
        if (!manifestURN) {
          reject(new Error('Getting manifest: manifestURN is empty.'));
        }
        try {
          parsed = url.parse(manifestURN);
          filename = path.join(this.storePath, parsed.host, parsed.pathname, DEFAULT_MANIFEST_FILENAME);
          // TODO: check if manifest exists ?
          return fs.readFile(filename, function (error, rawData) {
            var err;
            if (error) {
              return reject(new Error('Reading manifest from file: ' + error.message));
            } else {
              try {
                parsed = JSON.parse(rawData);
                return resolve(parsed);
              } catch (error1) {
                err = error1;
                return reject(new Error('Parsing manifest: ' + err));
              }
            }
          });
        } catch (error1) {
          err = error1;
          return reject(new Error('Parsing manifest URN: ' + err));
        }
      });
    }

    // Expects an Object representing the manifest, *NOT* the JSON
    storeManifest(manifest) {
      return q.promise((resolve, reject) => {
        var e, filename, manifestURN, parsed;
        if (!manifest) {
          reject(new Error('Storing manifest: manifest is empty.'));
        }
        try {
          manifestURN = manifest.name;
          parsed = url.parse(manifestURN);
          filename = path.join(this.storePath, parsed.host, parsed.pathname, DEFAULT_MANIFEST_FILENAME);
          // Make sure the destination directory exists
          return mkdirp(path.dirname(filename), function (error, made) {
            if (error) {
              return reject(new Error('Creating subdirectory: ' + error.message));
            } else {
              return fs.writeFile(filename, JSON.stringify(manifest), function (err) {
                if (err) {
                  return reject(new Error('Storing Service Application: ' + err.message));
                } else {
                  return resolve(manifestURN);
                }
              });
            }
          });
        } catch (error1) {
          e = error1;
          return reject(new Error('Determining storage filename: ' + e));
        }
      });
    }

    deleteManifest(manifestURN) {
      return q.promise((resolve, reject) => {
        var e, filename, parsed;
        if (!manifestURN) {
          reject(new Error('Deleting manifest: manifest URN is empty.'));
        }
        try {
          parsed = url.parse(manifestURN);
          filename = path.join(this.storePath, parsed.host, parsed.pathname, DEFAULT_MANIFEST_FILENAME);
          // Make sure the destination directory exists
          return rmdir(path.dirname(filename), function (error, dirs, files) {
            if (error) {
              return reject(new Error('Removing subdirectory: ' + error.message));
            } else {
              return resolve(manifestURN);
            }
          });
        } catch (error1) {
          e = error1;
          return reject(new Error('Determining storage filename: ' + e));
        }
      });
    }

    updateManifest(manifest) {
      // Right now, store overwrites any previously stored manifest
      return this.storeManifest(manifest);
    }

    list() {}

    // TODO
    info() {
      return `${this.type} Manifest Store (Path: ${this.storePath})`;
    }

  };

  module.exports = ManifestStoreFS;
}).call(undefined);