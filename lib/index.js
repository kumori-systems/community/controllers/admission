'use strict';

(function () {
  /*
  * Copyright 2022 Kumori Systems S.L.
   * Licensed under the EUPL, Version 1.2 or – as soon they
    will be approved by the European Commission - subsequent
    versions of the EUPL (the "Licence");
   * You may not use this work except in compliance with the
    Licence.
   * You may obtain a copy of the Licence at:
     https://joinup.ec.europa.eu/software/page/eupl
   * Unless required by applicable law or agreed to in
    writing, software distributed under the Licence is
    distributed on an "AS IS" basis,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
    express or implied.
   * See the Licence for the specific language governing
    permissions and limitations under the Licence.
   */
  var Admission, Authorization, BundleRegistry, DockerRegistryProxy, EventStoreElasticsearch, EventStoreK8s, K8sApiStub, KeycloakRequest, KeycloakUserManager, KukuElement, KukuModel, ManifestHelper, ManifestStore, ManifestValidatorMock, PlannerStub, Rescheduler, ReschedulerDescheduler, ReschedulerFactory, RestAPI, WebsocketPublisher, klogger, logger;

  klogger = require('./k-logger/index');

  RestAPI = require('./admission-rest-api');

  Admission = require('./admission');

  ManifestStore = require('./manifest-store');

  BundleRegistry = require('./bundle-registry');

  ManifestHelper = require('./manifest-helper');

  ManifestValidatorMock = require('./manifest-validator-mock');

  PlannerStub = require('./planner-stub');

  Authorization = require('./authorization');

  WebsocketPublisher = require('./websocket-publisher');

  K8sApiStub = require('./k8s-api-stub');

  DockerRegistryProxy = require('./docker-registry-proxy');

  KukuModel = require('./k8s-kuku-model/kuku-model');

  KukuElement = require('./k8s-kuku-model/kuku-element');

  KeycloakRequest = require('./keycloak-api/keycloak-request');

  KeycloakUserManager = require('./keycloak-api/keycloak-user-manager');

  EventStoreK8s = require('./event-stores/event-store-k8s');

  EventStoreElasticsearch = require('./event-stores/event-store-elasticsearch');

  ReschedulerFactory = require('./reschedulers/rescheduler-factory');

  Rescheduler = require('./reschedulers/rescheduler');

  ReschedulerDescheduler = require('./reschedulers/rescheduler-descheduler');

  klogger.setLogger([Admission, RestAPI, BundleRegistry, K8sApiStub, ManifestStore, ManifestHelper, ManifestValidatorMock, Authorization, PlannerStub, KukuModel, KukuElement, KeycloakRequest, KeycloakUserManager, WebsocketPublisher, DockerRegistryProxy, EventStoreK8s, EventStoreElasticsearch, ReschedulerDescheduler]);

  klogger.setParser([RestAPI, BundleRegistry, ManifestStore]);

  klogger.setLoggerOwner('admission-server');

  logger = klogger.getLogger('admission-server');

  logger.configure({
    transports: {
      file: {
        'level': 'debug',
        'filename': 'slap.log'
      },
      console: {
        'level': 'debug',
        'colorize': true
      }
    }
  });

  // module.exports.Admission = Admission
  module.exports.AdmissionRestAPI = RestAPI;
}).call(undefined);