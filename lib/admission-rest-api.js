'use strict';

(function () {
  /*
  * Copyright 2022 Kumori Systems S.L.
   * Licensed under the EUPL, Version 1.2 or – as soon they
    will be approved by the European Commission - subsequent
    versions of the EUPL (the "Licence");
   * You may not use this work except in compliance with the
    Licence.
   * You may obtain a copy of the Licence at:
     https://joinup.ec.europa.eu/software/page/eupl
   * Unless required by applicable law or agreed to in
    writing, software distributed under the Licence is
    distributed on an "AS IS" basis,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
    express or implied.
   * See the Licence for the specific language governing
    permissions and limitations under the Licence.
   */
  var Admission,
      AdmissionRestAPI,
      BundleRegistry,
      CLUSTER_CONFIG_CONFIGMAP,
      DEFAULT_REST_TIMEOUT,
      DEFAULT_TMP_PATH,
      EPEHEMERAL_WS_TOKEN_LIFETIME,
      EventEmitter,
      INGRESS_SERVICE_OBJECT_PREFIX,
      K8sApiStub,
      KUMORI_NAMESPACE,
      KeycloakRequest,
      ManifestHelper,
      ManifestStoreFactory,
      PlannerStub,
      ReschedulerFactory,
      SHUTDOWN_RETRY_WAIT_MILLIS,
      base64urlDecode,
      base64urlUnescape,
      cleanupFiles,
      decodeJwt,
      express,
      getAuthData,
      getFromReq,
      httpProxyMiddleware,
      loadFile,
      q,
      startsWith,
      stream,
      utils,
      boundMethodCheck = function (instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new Error('Bound instance method accessed before binding');
    }
  },
      indexOf = [].indexOf;

  q = require('q');

  stream = require('stream');

  express = require('express');

  EventEmitter = require('events').EventEmitter;

  httpProxyMiddleware = require('http-proxy-middleware');

  BundleRegistry = require('./bundle-registry');

  Admission = require('./admission');

  PlannerStub = require('./planner-stub');

  ManifestStoreFactory = require('./manifest-store-factory');

  ManifestHelper = require('./manifest-helper');

  K8sApiStub = require('./k8s-api-stub');

  KeycloakRequest = require('./keycloak-api/keycloak-request');

  ReschedulerFactory = require('./reschedulers/rescheduler-factory');

  utils = require('./utils');

  DEFAULT_TMP_PATH = '/tmp/slap/admission';

  DEFAULT_REST_TIMEOUT = 30 * 60 * 1000;

  EPEHEMERAL_WS_TOKEN_LIFETIME = 30 * 1000; // Milliseconds

  KUMORI_NAMESPACE = 'kumori';

  CLUSTER_CONFIG_CONFIGMAP = 'cluster-config';

  INGRESS_SERVICE_OBJECT_PREFIX = "ingress-lb-service-";

  SHUTDOWN_RETRY_WAIT_MILLIS = 5000;

  AdmissionRestAPI = class AdmissionRestAPI extends EventEmitter {
    constructor(config = {}, planner, keycloak, keycloakLogin, keycloakUserManager) {
      var base, base1, k8sApiConfig, meth, mrConfig, ref;
      super();
      //#############################################################################
      //#                         DEV TEST METHODS (TEMPORARY)                     ##
      //#############################################################################
      this.test = this.test.bind(this);
      this.testList = this.testList.bind(this);
      this.testListNodes = this.testListNodes.bind(this);
      this.testTopNodes = this.testTopNodes.bind(this);
      this.testNodeMetrics = this.testNodeMetrics.bind(this);
      this.testPodMetrics = this.testPodMetrics.bind(this);
      this.testPodLogs = this.testPodLogs.bind(this);
      this.testListPods = this.testListPods.bind(this);
      this.testEvents = this.testEvents.bind(this);
      this.testRead = this.testRead.bind(this);
      this.testInstanceOwner = this.testInstanceOwner.bind(this);
      this.testListControllerRevisions = this.testListControllerRevisions.bind(this);
      this.testGetControllerRevision = this.testGetControllerRevision.bind(this);
      this.testGetVolumeMetrics = this.testGetVolumeMetrics.bind(this);
      //#############################################################################
      //#                  MIDDLEWARE FOR USER MANAGEMENT DISABLED                 ##
      //#############################################################################
      this.checkUserMgmt = this.checkUserMgmt.bind(this);
      //#############################################################################
      //#                    MIDDLEWARE FOR RESCHEDULER DISABLED                   ##
      //#############################################################################
      this.checkRescheduling = this.checkRescheduling.bind(this);
      //#############################################################################
      //#                    MIDDLEWARE FOR RESCHEDULER DISABLED                   ##
      //#############################################################################
      this.checkAlarmsMgmt = this.checkAlarmsMgmt.bind(this);
      //#############################################################################
      //#            MIDDLEWARE FOR LOGGING USAGE OF DEPRECATED METHOD             ##
      //#############################################################################
      this.deprecated = this.deprecated.bind(this);
      this.processShutdown = this.processShutdown.bind(this);
      this.shutdownOperations = this.shutdownOperations.bind(this);
      this.shutdownRemoveSolutions = this.shutdownRemoveSolutions.bind(this);
      this.shutdownRemoveIngresses = this.shutdownRemoveIngresses.bind(this);
      this.shutdownRemoveVolumes = this.shutdownRemoveVolumes.bind(this);
      this.shutdownRemoveIngressLBService = this.shutdownRemoveIngressLBService.bind(this);
      this.shutdown = this.shutdown.bind(this);
      this.maintenance = this.maintenance.bind(this);
      this.reschedule = this.reschedule.bind(this);
      this.getRescheduleJobs = this.getRescheduleJobs.bind(this);
      this.clearOperationLocks = this.clearOperationLocks.bind(this);
      //#############################################################################
      //#                 AUTHENTICATION API IMPLEMENTATION METHODS                ##
      //#############################################################################
      this.refreshToken = this.refreshToken.bind(this);
      this.login = this.login.bind(this);
      // Old version of login that relied on keycloak-connect middleware but was
      // failing in browser (Dashboard with AdmissionClient) due to CORS headers not
      // being set by Keycloak server.

      // Kept for future reference.
      this.oldLogin = this.oldLogin.bind(this);
      //#############################################################################
      //#                 USER MANAGEMENT API IMPLEMENTATION METHODS               ##
      //#############################################################################
      this.listUsers = this.listUsers.bind(this);
      this.getUser = this.getUser.bind(this);
      this.deleteUser = this.deleteUser.bind(this);
      this.createUser = this.createUser.bind(this);
      this.updateUser = this.updateUser.bind(this);
      this.updateUserPassword = this.updateUserPassword.bind(this);
      this.listGroups = this.listGroups.bind(this);
      //#############################################################################
      //#                    ADMISSION API IMPLEMENTATION METHODS                  ##
      //#############################################################################
      this.deploymentQuery = this.deploymentQuery.bind(this);
      this.solutionQuery = this.solutionQuery.bind(this);
      this.registerSolution = this.registerSolution.bind(this);
      this.deleteSolution = this.deleteSolution.bind(this);
      this.deploy = this.deploy.bind(this);
      this.undeploy = this.undeploy.bind(this);
      this.restartSolutionInstances = this.restartSolutionInstances.bind(this);
      this.restartInstances = this.restartInstances.bind(this);
      this.unregister = this.unregister.bind(this);
      this.readManifest = this.readManifest.bind(this);
      this.listLinks = this.listLinks.bind(this);
      this.linkServices = this.linkServices.bind(this);
      this.unlinkServices = this.unlinkServices.bind(this);
      this.registerBundle = this.registerBundle.bind(this);
      this.registerResource = this.registerResource.bind(this);
      this.deleteResource = this.deleteResource.bind(this);
      this.listResources = this.listResources.bind(this);
      this.listResourcesInUse = this.listResourcesInUse.bind(this);
      this.describeResource = this.describeResource.bind(this);
      this.listElements = this.listElements.bind(this);
      this.getClusterInfo = this.getClusterInfo.bind(this);
      this.getClusterConfig = this.getClusterConfig.bind(this);
      this.readClusterConfig = this.readClusterConfig.bind(this);
      this.getSolutionInstanceLogs = this.getSolutionInstanceLogs.bind(this);
      this.execSolutionInstanceCommandGet = this.execSolutionInstanceCommandGet.bind(this);
      this.execSolutionInstanceCommand = this.execSolutionInstanceCommand.bind(this);
      this.getInstanceLogs = this.getInstanceLogs.bind(this);
      this.execInstanceCommandGet = this.execInstanceCommandGet.bind(this);
      this.execInstanceCommand = this.execInstanceCommand.bind(this);
      this.getSolutionRevision = this.getSolutionRevision.bind(this);
      this.getDeploymentRevision = this.getDeploymentRevision.bind(this);
      this.config = config;
      this.planner = planner;
      this.keycloak = keycloak;
      this.keycloakLogin = keycloakLogin;
      this.keycloakUserManager = keycloakUserManager;
      meth = 'AdmissionRestAPI.constructor';
      this.logger.info(`${meth} Config: ${JSON.stringify(this.config)}`);
      if ((base = this.config).tmpPath == null) {
        base.tmpPath = DEFAULT_TMP_PATH;
      }
      // Instantiate a Manifest helper
      this.manifestHelper = new ManifestHelper();
      // Instantiate a Kubernetes API Server stub
      k8sApiConfig = this.config.manifestRepository.config.k8sApi || null;
      this.k8sApiStub = new K8sApiStub(k8sApiConfig);
      // Pass K8sApiStub to KeycloakUserManager (now uses Operator CRDs)
      this.keycloakUserManager.k8sApiStub = this.k8sApiStub;
      // Instantiate a Manifest repository
      mrConfig = this.config.manifestRepository;
      mrConfig.config.k8sApiStub = this.k8sApiStub;
      this.manifestRepository = ManifestStoreFactory.create(mrConfig.type, mrConfig.config);
      // Instantiate a "Planner" Stub (will simulate old Planner actions)
      this.plannerStub = new PlannerStub(this.k8sApiStub, this.manifestRepository, this.config.eventStore);
      // Instantiate a BundleRegistry
      this.bundleRegistry = new BundleRegistry(null, this.manifestRepository, this.config.maxBundleSize, this.config.tmpPath);
      // Instantiate an Admission
      this.adm = new Admission(this.config, this.manifestRepository, this.plannerStub);
      if ((base1 = this.config).restTimeout == null) {
        base1.restTimeout = DEFAULT_REST_TIMEOUT;
      }
      this.apiRequestCount = 0;
      this.anonymous = (ref = this.config.acs) != null ? ref['anonymous-user'] : void 0;
      // Object for keeping track of valid Websocket tokens
      this.ephemeralWebsocketTokens = {};
      // Variable to keep track of maintenance status
      this.downForMaintenance = false;
      // Last read Cluster Configuration (from Etcd ConfigMap)
      this.latestClusterConfiguration = null;
      // Represents the status of the shutdown process
      this.shutdownStatus = {
        initiated: false,
        completed: false,
        messages: [],
        // State transitions flags
        solutionsDeleted: false,
        volumesDeleted: false,
        ingressesDeleted: false,
        ingressLoadBalancerDeleted: false
      };
    }

    init() {
      var meth;
      meth = 'AdmissionRestAPI.init()';
      this.logger.info(meth);
      // Reset active websocket token
      this.ephemeralWebsocketTokens = {};
      return this.bundleRegistry.init().then(() => {
        return this.k8sApiStub.init();
      }).then(() => {
        return this.manifestRepository.init();
      }).then(() => {
        return this.plannerStub.init();
      }).then(() => {
        this.adm.init();
        return this.authorization = this.adm.authorization;
      }).then(() => {
        return this.readClusterConfig();
      }).then(() => {
        var alarmProxy;
        //#########################################################################
        //#                          Login API Router                            ##
        //#########################################################################
        this.loginRouter = express.Router();
        // @loginRouter.get '/login', @keycloakLogin.protect('realm:user'), @login
        this.loginRouter.get('/login', this.login);
        // Temporary method for Dashboard to work
        // @loginRouter.get '/tokens/refresh', @keycloak.protect('realm:user'), @refreshToken
        this.loginRouter.post('/tokens/refresh', this.refreshToken);
        //#########################################################################
        //#                        Admission API Router                          ##
        //#########################################################################
        this.router = express.Router();
        // Add a custom middleware for managing API disabling for maintenance
        this.router.use((req, res, next) => {
          var maintenanceMsg;
          if (this.downForMaintenance) {
            maintenanceMsg = 'Admission Server is down for maintenance.';
            // Retry-After header to 5 minutes
            res.set('Retry-After', '300');
            return res.status(503).send(maintenanceMsg);
          } else {
            return next();
          }
        });
        // Unprotected API routes (don't require authenticated user)
        this.router.get('/clusterconfig', this.getClusterConfig);
        // Protect all Admission API routes (require authenticated in "user" realm)

        // More fine grained protection can be applied to each route
        // Example:  @router.post '/test', @keycloak.protect('realm:user'), @test
        this.router.use(this.keycloak.protect('realm:user'));
        // Admission API routes
        this.router.post('/bundles', this.registerBundle);
        this.router.get('/registries', this.listElements);
        this.router.get('/registries/:urn?', this.readManifest);
        this.router.delete('/registries/:urn?', this.unregister);
        this.router.post('/solutions', this.registerSolution);
        this.router.get('/solutions/:urn?', this.solutionQuery);
        this.router.get('/solutions/:urn/revisions/:rev?', this.getSolutionRevision);
        this.router.delete('/solutions/:urn?:force?', this.deleteSolution);
        this.router.delete('/solutions/:urn/roles/:role/instances/:instance?', this.restartSolutionInstances);
        this.router.get('/solutions/:urn/roles/:role/instances/:instance/:container?/logs', this.getSolutionInstanceLogs);
        this.router.get('/solutions/:urn/roles/:role/instances/:instance/:container?/exec', this.execSolutionInstanceCommandGet);
        this.router.ws('/solutions/:urn/roles/:role/instances/:instance/exec', this.execSolutionInstanceCommand);
        this.router.get('/links', this.listLinks);
        this.router.post('/links', this.linkServices);
        this.router.delete('/links:urn?', this.unlinkServices);
        this.router.post('/resources', this.registerResource);
        this.router.get('/resources/inuse/:owner?:urn?', this.listResourcesInUse);
        this.router.get('/resources/:owner?', this.listResources);
        this.router.get('/resources/:urn/details', this.describeResource);
        this.router.delete('/resources/:urn', this.deleteResource);
        //#########################################################
        //#      CURRENTLY DEPRECATED API METHODS (NOT USED      ##
        //#########################################################
        this.router.delete('/deployments/:urn?:force?', this.deprecated, this.undeploy);
        this.router.delete('/deployments/:urn/roles/:role/instances/:instance?', this.deprecated, this.restartInstances);
        this.router.post('/deployments', this.deprecated, this.deploy);
        this.router.get('/deployments', this.deprecated, this.deploymentQuery);
        this.router.get('/deployments/:urn?', this.deprecated, this.deploymentQuery);
        this.router.get('/deployments/:urn/revisions/:rev?', this.deprecated, this.getDeploymentRevision);
        this.router.get('/logs/:instance/:container?', this.deprecated, this.getInstanceLogs);
        this.router.get('/instances/:instance/logs', this.deprecated, this.getInstanceLogs);
        this.router.get('/instances/:instance/exec', this.deprecated, this.execInstanceCommandGet);
        // Websocket route (express-ws)
        this.router.ws('/instances/:instance/exec', this.deprecated, this.execInstanceCommand);
        //#########################################################
        //#  END OF CURRENTLY DEPRECATED API METHODS (NOT USED   ##
        //#########################################################

        // Cluster management API routes
        this.router.get('/clusterinfo', this.getClusterInfo);
        // TODO: The protect method could already check access based on
        // Example: @keycloak.protect('admin-users')
        //          --> where 'admin-users' is a custom Client role

        // User management API routes
        // These routes are disabled when no authentication is configured
        this.router.get('/users', this.keycloak.protect('admission:administrator'), this.checkUserMgmt, this.listUsers);
        this.router.get('/users/:username', this.keycloak.protect('admission:administrator'), this.checkUserMgmt, this.getUser);
        this.router.post('/users', this.keycloak.protect('admission:administrator'), this.checkUserMgmt, this.createUser);
        this.router.put('/users', this.keycloak.protect('admission:administrator'), this.checkUserMgmt, this.updateUser);
        this.router.delete('/users/:username', this.keycloak.protect('admission:administrator'), this.checkUserMgmt, this.deleteUser);
        this.router.get('/groups', this.keycloak.protect('admission:administrator'), this.checkUserMgmt, this.listGroups);
        // Special non-admin method for users to update their own passwords
        this.router.patch('/users/:username/password', this.keycloak.protect('realm:user'), this.updateUserPassword);
        // TODO:
        // - Consider managing groups

        // Alarms API routes. RestAPI is implemented by kualarm-controller, so
        // Admission just proxify requests using the http-proxy-middledware package.
        // Note: the interceptor "fixRequestBody" is used to fix proxied POST
        // requests when bodyParser is applied before this middleware.
        alarmProxy = httpProxyMiddleware.createProxyMiddleware({
          target: this.config.alarmsRestAPI.serverURL,
          changeOrigin: true,
          pathRewrite: {
            ['^/admission/alarms']: ''
          },
          onProxyReq: httpProxyMiddleware.fixRequestBody
        });
        this.router.use('/alarms', this.keycloak.protect('admission:administrator'), this.checkAlarmsMgmt, alarmProxy);
        // # ROUTES FOR TESTING (should be removed)

        // @router.get '/test',                     @test
        // @router.get '/test2',                    @test
        // @router.get '/testRead',                 @testRead
        // @router.get '/list',                     @testList
        // @router.get '/events',                   @testEvents
        // @router.get '/listNodes',                @testListNodes
        // @router.get '/listPods',                 @testListPods
        // @router.get '/topNodes',                 @testTopNodes
        // @router.get '/nodeMetrics',              @testNodeMetrics
        // @router.get '/podMetrics',               @testPodMetrics
        // @router.get '/podLogs',                  @testPodLogs
        // @router.get '/testInstance/:instance?',  @testInstanceOwner
        // @router.get '/listRevisions',            @testListControllerRevisions
        // @router.get '/listOneRevision/:rev',     @testGetControllerRevision
        // @router.get '/testGetPvcMetrics/:pvc',   @testGetVolumeMetrics

        //#########################################################################
        //#                       Management API Router                          ##
        //#########################################################################
        this.managementRouter = express.Router();
        // Management API routes
        // @managementRouter.get '/test', keycloak.protect('realm:user'), @test
        this.managementRouter.get('/health', this.health);
        this.managementRouter.get('/ready', this.ready);
        this.managementRouter.get('/maintenance/:action?', this.keycloak.protect('admission:administrator'), this.maintenance);
        this.managementRouter.get('/reschedule/:type', this.keycloak.protect('admission:administrator'), this.checkRescheduling, this.getRescheduleJobs);
        this.managementRouter.post('/reschedule/:type', this.keycloak.protect('admission:administrator'), this.checkRescheduling, this.reschedule);
        this.managementRouter.post('/clearlocks', this.keycloak.protect('admission:administrator'), this.clearOperationLocks);
        return this.managementRouter.post('/shutdown', this.keycloak.protect('admission:administrator'), this.shutdown);
      });
    }

    terminate() {
      var meth;
      meth = 'AdmissionRestAPI.terminate()';
      this.logger.info(meth);
      this.bundleRegistry.close();
      this.keycloakUserManager.terminate();
      return this.adm.terminate().then(() => {
        this.logger.info(`${meth} - Terminated.`);
        return true;
      });
    }

    setupContext(req) {
      var authData, context, meth, ref, ref1;
      meth = 'AdmissionRestAPI.setupContext()';
      this.logger.debug({ meth });
      this.logger.debug(`${meth} - `);
      authData = getAuthData(req);
      if (authData !== null) {
        context = {
          user: authData.user
        };
      } else if (((ref = this.config.authentication) != null ? ref.keycloakConfig : void 0) != null) {
        // If authentication is enabled but no auth data, this must be from a
        // non-protected path. Create a guest user with no permissions.
        context = {
          user: {
            id: 'anonymous',
            name: 'Anonymous unauthenticated user',
            email: '',
            roles: ['GUEST'],
            limits: null
          }
        };
      } else if (((ref1 = this.config.authentication) != null ? ref1.clientCertificateConfig : void 0) != null) {
        // If authentication is enabled but no auth data, this must be from a
        // non-protected path. Create a guest user with no permissions.
        context = {
          user: {
            id: 'anonymous',
            name: 'Anonymous unauthenticated user',
            email: '',
            roles: ['GUEST'],
            limits: null
          }
        };
      } else {
        // If authentication is disabled, return a mock user with full rights
        context = {
          user: {
            id: 'anonymous',
            name: 'Anonymous user (authentication disabled)',
            email: '',
            roles: ['GUEST', 'DEVEL', 'ADMIN'],
            limits: null
          }
        };
      }
      this.logger.debug(`${meth} - Context: ${JSON.stringify(context)}`);
      return context;
    }

    getRouter() {
      return this.router;
    }

    getManagementRouter() {
      return this.managementRouter;
    }

    getLoginRouter() {
      return this.loginRouter;
    }

    test(req, res) {
      var context, msg, userData;
      boundMethodCheck(this, AdmissionRestAPI);
      userData = getUserData(req);
      context = this.setupContext(req);
      msg = `Access granted to user ${context.user.id} (Roles: ${context.user.roles})`;
      return res.json({
        message: msg,
        context: context,
        userData: userData
      });
    }

    escapeLabelValue(str) {
      var LABEL_ESCAPED_CHARS, k, v;
      LABEL_ESCAPED_CHARS = {
        '@': '__arroba__'
      };
      for (k in LABEL_ESCAPED_CHARS) {
        v = LABEL_ESCAPED_CHARS[k];
        str = str.split(k).join(v);
      }
      return str;
    }

    testList(req, res) {
      var context, elementPlural, filter, k, labelSelector, meth, owner, ref, ref1, results, v;
      boundMethodCheck(this, AdmissionRestAPI);
      meth = 'AdmissionRestAPI.testList';
      context = this.setupContext(req);
      this.logger.info(`${meth} - User: ${context.user.id || context.user.name || ''}`);
      results = [];
      owner = context != null ? (ref = context.user) != null ? ref.id : void 0 : void 0;
      filter = {
        'kumori/owner': (context != null ? (ref1 = context.user) != null ? ref1.id : void 0 : void 0) || 'ANONYMOUS'
      };
      // labelSelector = 'domain=jferrer.dev.testing'
      labelSelector = '';
      for (k in filter) {
        v = filter[k];
        if (labelSelector !== '') {
          labelSelector += ',';
        }
        labelSelector += k + '=' + this.escapeLabelValue(v);
      }
      // elementPlural = 'kukucomponents'
      // labelSelector = 'domain=jferrer.dev.testing'
      elementPlural = 'kukuvhosts';
      return this.k8sApiStub.listKumoriElements(elementPlural, labelSelector).then(items => {
        var i, it, len, manifest, manifests, ref2, ref3, ref4, response;
        console.log(`LIST FINISHED OK AND RETURNED ${items.length} RESULTS`);
        manifests = [];
        for (i = 0, len = items.length; i < len; i++) {
          it = items[i];
          if (((ref2 = it.spec) != null ? ref2.kumoriManifest : void 0) != null) {
            manifest = JSON.parse(utils.gunzipString(it.spec.kumoriManifest));
            manifests.push(manifest);
          } else if (((ref3 = it.metadata) != null ? (ref4 = ref3.annotations) != null ? ref4['kumori/manifest'] : void 0 : void 0) != null) {
            manifest = JSON.parse(utils.gunzipString(it.metadata.annotations['kumori/manifest']));
            manifests.push(manifest);
          } else {
            console.log("WARNING: ITEM HAS NO ORIGINAL ECLOUD MANIFEST!");
          }
        }
        response = {
          success: true,
          message: `GOT ${manifests.length} MANIFESTS`,
          data: manifests
        };
        res.send(response);
        return true;
      }).catch(err => {
        var response;
        console.log("LIST FAILED");
        console.log(`ERROR: ${err.message} - ${err.stack}`);
        response = {
          success: false,
          message: 'LIST FAILED',
          data: results
        };
        res.send(response);
        return true;
      });
    }

    testListNodes(req, res) {
      var context, filter, meth, owner, ref, ref1, results;
      boundMethodCheck(this, AdmissionRestAPI);
      meth = 'AdmissionRestAPI.testListNodes';
      context = this.setupContext(req);
      this.logger.info(`${meth} - User: ${context.user.id || context.user.name || ''}`);
      results = [];
      owner = context != null ? (ref = context.user) != null ? ref.id : void 0 : void 0;
      filter = {
        'kumori/owner': (context != null ? (ref1 = context.user) != null ? ref1.id : void 0 : void 0) || 'ANONYMOUS'
      };
      return this.k8sApiStub.listNodes().then(items => {
        var response;
        console.log(`LIST FINISHED OK AND RETURNED ${items.length} RESULTS`);
        response = {
          success: true,
          message: `GOT ${items} NODES`,
          data: items
        };
        res.send(response);
        return true;
      }).catch(err => {
        var response;
        console.log("LIST FAILED");
        console.log(`ERROR: ${err.message} - ${err.stack}`);
        response = {
          success: false,
          message: 'LIST FAILED',
          data: results
        };
        res.send(response);
        return true;
      });
    }

    testTopNodes(req, res) {
      var context, filter, meth, owner, ref, ref1, results;
      boundMethodCheck(this, AdmissionRestAPI);
      meth = 'AdmissionRestAPI.testTopNodes';
      context = this.setupContext(req);
      this.logger.info(`${meth} - User: ${context.user.id || context.user.name || ''}`);
      results = [];
      owner = context != null ? (ref = context.user) != null ? ref.id : void 0 : void 0;
      filter = {
        'kumori/owner': (context != null ? (ref1 = context.user) != null ? ref1.id : void 0 : void 0) || 'ANONYMOUS'
      };
      // q.reject new Error 'NOT IMPLEMENTED'
      return this.k8sApiStub.topNodes().then(items => {
        var response;
        console.log(`TOP FINISHED AND RETURNED ${items}`);
        response = {
          success: true,
          message: "GOT TOP NODES DATA",
          data: items
        };
        res.send(response);
        return true;
      }).catch(err => {
        var response;
        console.log("TOP FAILED");
        console.log(`ERROR: ${err.message} - ${err.stack}`);
        response = {
          success: false,
          message: 'TOP FAILED',
          data: results
        };
        res.send(response);
        return true;
      });
    }

    testNodeMetrics(req, res) {
      var context, filter, meth, owner, ref, ref1, results;
      boundMethodCheck(this, AdmissionRestAPI);
      meth = 'AdmissionRestAPI.testNodeMetrics';
      context = this.setupContext(req);
      this.logger.info(`${meth} - User: ${context.user.id || context.user.name || ''}`);
      results = [];
      owner = context != null ? (ref = context.user) != null ? ref.id : void 0 : void 0;
      filter = {
        'kumori/owner': (context != null ? (ref1 = context.user) != null ? ref1.id : void 0 : void 0) || 'ANONYMOUS'
      };
      // q.reject new Error 'NOT IMPLEMENTED'
      return this.k8sApiStub.getNodesMetrics().then(items => {
        var response;
        console.log(`GET NODE METRICS FINISHED AND RETURNED ${items}`);
        response = {
          success: true,
          message: "GOT NODE METRICS DATA",
          data: items
        };
        res.send(response);
        return true;
      }).catch(err => {
        var response;
        console.log("GET NODE METRICS FAILED");
        console.log(`ERROR: ${err.message} - ${err.stack}`);
        response = {
          success: false,
          message: 'GET NODE METRICS FAILED',
          data: results
        };
        res.send(response);
        return true;
      });
    }

    testPodMetrics(req, res) {
      var context, filter, meth, owner, ref, ref1, results;
      boundMethodCheck(this, AdmissionRestAPI);
      meth = 'AdmissionRestAPI.testPodMetrics';
      context = this.setupContext(req);
      this.logger.info(`${meth} - User: ${context.user.id || context.user.name || ''}`);
      results = [];
      owner = context != null ? (ref = context.user) != null ? ref.id : void 0 : void 0;
      filter = {
        'kumori/owner': (context != null ? (ref1 = context.user) != null ? ref1.id : void 0 : void 0) || 'ANONYMOUS'
      };
      // q.reject new Error 'NOT IMPLEMENTED'
      return this.k8sApiStub.getPodsMetrics().then(items => {
        var response;
        console.log(`GET POD METRICS FINISHED AND RETURNED ${items}`);
        console.log(`DATA: ${JSON.stringify(items)}`);
        response = {
          success: true,
          message: "GOT POD METRICS DATA",
          data: items
        };
        res.send(response);
        return true;
      }).catch(err => {
        var response;
        console.log("GET POD METRICS FAILED");
        console.log(`ERROR: ${err.message} - ${err.stack}`);
        response = {
          success: false,
          message: 'GET POD METRICS FAILED',
          data: results
        };
        res.send(response);
        return true;
      });
    }

    testPodLogs(req, res) {
      var context, filter, meth, owner, ref, ref1, results;
      boundMethodCheck(this, AdmissionRestAPI);
      meth = 'AdmissionRestAPI.testPodLogs';
      context = this.setupContext(req);
      this.logger.info(`${meth} - User: ${context.user.id || context.user.name || ''}`);
      results = [];
      owner = context != null ? (ref = context.user) != null ? ref.id : void 0 : void 0;
      filter = {
        'kumori/owner': (context != null ? (ref1 = context.user) != null ? ref1.id : void 0 : void 0) || 'ANONYMOUS'
      };
      // q.reject new Error 'NOT IMPLEMENTED'
      return this.k8sApiStub.altGetPodLogs().then(items => {
        var response;
        console.log(`GET POD LOGS FINISHED AND RETURNED ${items}`);
        console.log(`DATA: ${JSON.stringify(items)}`);
        response = {
          success: true,
          message: "GOT POD LOGS DATA",
          data: items
        };
        res.send(response);
        return true;
      }).catch(err => {
        var response;
        console.log("GET POD LOGS FAILED");
        console.log(`ERROR: ${err.message} - ${err.stack}`);
        response = {
          success: false,
          message: 'GET POD LOGS FAILED',
          data: results
        };
        res.send(response);
        return true;
      });
    }

    testListPods(req, res) {
      var context, filter, meth, owner, ref, ref1, results;
      boundMethodCheck(this, AdmissionRestAPI);
      meth = 'AdmissionRestAPI.testListPods';
      context = this.setupContext(req);
      this.logger.info(`${meth} - User: ${context.user.id || context.user.name || ''}`);
      results = [];
      owner = context != null ? (ref = context.user) != null ? ref.id : void 0 : void 0;
      filter = {
        'kumori/owner': (context != null ? (ref1 = context.user) != null ? ref1.id : void 0 : void 0) || 'ANONYMOUS'
      };
      return this.k8sApiStub.listPods().then(items => {
        var response;
        console.log(`LIST FINISHED OK AND RETURNED ${items.length} RESULTS`);
        response = {
          success: true,
          message: `GOT ${items} PODS`,
          data: items
        };
        res.send(response);
        return true;
      }).catch(err => {
        var response;
        console.log("LIST FAILED");
        console.log(`ERROR: ${err.message} - ${err.stack}`);
        response = {
          success: false,
          message: 'LIST FAILED',
          data: results
        };
        res.send(response);
        return true;
      });
    }

    testEvents(req, res) {
      var context, filter, meth, owner, ref, ref1, results;
      boundMethodCheck(this, AdmissionRestAPI);
      meth = 'AdmissionRestAPI.testEvents';
      context = this.setupContext(req);
      this.logger.info(`${meth} - User: ${context.user.id || context.user.name || ''}`);
      results = [];
      owner = context != null ? (ref = context.user) != null ? ref.id : void 0 : void 0;
      filter = {
        'kumori/owner': (context != null ? (ref1 = context.user) != null ? ref1.id : void 0 : void 0) || 'ANONYMOUS'
      };
      return this.k8sApiStub.listEvents().then(items => {
        var response;
        console.log(`LIST EVENTS FINISHED OK AND RETURNED ${items.length} RESULTS`);
        response = {
          success: true,
          message: "GOT EVENTS",
          data: items
        };
        res.send(response);
        return true;
      }).catch(err => {
        var response;
        console.log("LIST EVENTS FAILED");
        console.log(`ERROR: ${err.message} - ${err.stack}`);
        response = {
          success: false,
          message: 'LIST EVENTS FAILED',
          data: results
        };
        res.send(response);
        return true;
      });
    }

    testRead(req, res) {
      var context, i, len, meth, promises, results, urn, urns;
      boundMethodCheck(this, AdmissionRestAPI);
      meth = 'AdmissionRestAPI.testRead';
      context = this.setupContext(req);
      this.logger.info(`${meth} - User: ${context.user.id || context.user.name || ''}`);
      urns = ['eslap://jferrer.dev.testing/components/first/1_0_0', 'eslap://jferrer.dev.testing/services/myfirst/1_0_0'];
      promises = [];
      results = {};
      for (i = 0, len = urns.length; i < len; i++) {
        urn = urns[i];
        (urn => {
          return promises.push((console.log(`READING MANIFEST: ${urn}`), this.manifestRepository.getManifest(urn).then(manifest => {
            console.log(`${JSON.stringify(manifest, null, 2)}`);
            results[urn] = manifest;
            return true;
          }).catch(err => {
            return console.log(`ERROR: ${err.message}`);
          }).catch(err => {
            console.log(`ERROR: ${err.message}`);
            results[urn] = `ERROR: ${err.message}`;
            return true;
          })));
        })(urn);
      }
      return q.allSettled(promises).then(() => {
        var response;
        console.log(`RESULTS: ${JSON.stringify(results, null, 2)}`);
        response = {
          success: true,
          message: 'GOT MANIFESTS',
          data: results
        };
        res.send(response);
        return true;
      });
    }

    testInstanceOwner(req, res) {
      var context, errMsg, instanceId, meth, response;
      boundMethodCheck(this, AdmissionRestAPI);
      meth = 'AdmissionRestAPI.testInstanceOwner';
      context = this.setupContext(req);
      console.log(`${meth} - User: ${context.user.id || context.user.name || ''}`);
      // Retrieve request parameters
      instanceId = getFromReq(req, 'instance');
      console.log(`${meth} - Parameters: instanceId=${instanceId}`);
      // Validate mandatory parameter instanceId is present
      if (instanceId == null) {
        errMsg = 'Missing mandatory parameter (instance ID)';
        console.log(`${meth} - ERROR: ${errMsg}`);
        response = {
          success: false,
          message: errMsg
        };
        res.send(response);
        return true;
      }
      // Validate user has permissions to access the requested instance
      console.log(`${meth} - Checking user permissions to access instance ${instanceId}`);
      return this.authorization.isAllowed('instance', instanceId, context).then(allowed => {
        var msg;
        if (!allowed) {
          errMsg = "User is not allowed to access instance.";
          console.log(`${meth} - ERROR: ${errMsg}`);
          response = {
            success: false,
            message: errMsg
          };
          res.send(response);
          return true;
        } else {
          msg = `User ${context.user.id} can access instance ${instanceId}.`;
          console.log(`${meth} - ${msg}`);
          response = {
            success: true,
            message: msg,
            data: null
          };
          res.send(response);
          return true;
        }
      }).catch(err => {
        console.log(`${meth} - ERROR: ${err.message} - ${err.stack}`);
        response = {
          success: false,
          message: 'Instance permissions check failed',
          data: results
        };
        res.send(response);
        return true;
      });
    }

    testListControllerRevisions(req, res) {
      var context, filter, meth, owner, ref, ref1, results;
      boundMethodCheck(this, AdmissionRestAPI);
      meth = 'AdmissionRestAPI.testListControllerRevisions';
      context = this.setupContext(req);
      this.logger.info(`${meth} - User: ${context.user.id || context.user.name || ''}`);
      results = [];
      owner = context != null ? (ref = context.user) != null ? ref.id : void 0 : void 0;
      filter = {
        'kumori/owner': (context != null ? (ref1 = context.user) != null ? ref1.id : void 0 : void 0) || 'ANONYMOUS'
      };
      return this.k8sApiStub.getControllerRevisions(KUMORI_NAMESPACE).then(items => {
        var history, hit, i, it, j, len, len1, list, newItem, ref2, response;
        console.log(`LIST FINISHED OK AND RETURNED ${items.length} RESULTS`);
        console.log("CONVERTING LIST TO SIMPLIFIED LIST");
        list = [];
        for (i = 0, len = items.length; i < len; i++) {
          it = items[i];
          if (((ref2 = it.metadata) != null ? ref2.annotations : void 0) != null && 'kumori/history' in it.metadata.annotations) {
            console.log(JSON.stringify(it, null, 2));
            history = JSON.parse(it.metadata.annotations['kumori/history']);
            for (j = 0, len1 = history.length; j < len1; j++) {
              hit = history[j];
              newItem = {
                name: it.metadata.name,
                revision: hit.revision,
                timestamp: hit.timestamp,
                comment: hit.comment
              };
              list.push(newItem);
            }
          }
        }
        console.log(`SIMPLIFIED LIST : ${JSON.stringify(list, null, 2)}`);
        response = {
          success: true,
          message: `GOT ${items.length || 0} CONTROLLERREVISIONS`,
          data: list
        };
        res.send(response);
        return true;
      }).catch(err => {
        var response;
        console.log("LIST FAILED");
        console.log(`ERROR: ${err.message} - ${err.stack}`);
        response = {
          success: false,
          message: 'LIST FAILED',
          data: results
        };
        res.send(response);
        return true;
      });
    }

    testGetControllerRevision(req, res) {
      var context, filter, meth, owner, ref, ref1, revName;
      boundMethodCheck(this, AdmissionRestAPI);
      meth = 'AdmissionRestAPI.testGetControllerRevision';
      context = this.setupContext(req);
      this.logger.info(`${meth} - User: ${context.user.id || context.user.name || ''}`);
      revName = getFromReq(req, 'rev');
      revName = 'kd-083221-552575c6-6bdb9d7d5';
      owner = context != null ? (ref = context.user) != null ? ref.id : void 0 : void 0;
      filter = {
        'kumori/owner': (context != null ? (ref1 = context.user) != null ? ref1.id : void 0 : void 0) || 'ANONYMOUS'
      };
      return this.k8sApiStub.getControllerRevision(KUMORI_NAMESPACE, revName).then(result => {
        var response;
        console.log("RETRIEVED REVISION");
        response = {
          success: true,
          message: "GOT CONTROLLERREVISIONS",
          data: result
        };
        res.send(response);
        return true;
      }).catch(err => {
        var response;
        console.log("GET FAILED");
        console.log(`ERROR: ${err.message} - ${err.stack}`);
        response = {
          success: false,
          message: 'GET FAILED',
          data: {}
        };
        res.send(response);
        return true;
      });
    }

    testGetVolumeMetrics(req, res) {
      var context, filter, meth, owner, pvcName, pvcvName, ref, ref1, result;
      boundMethodCheck(this, AdmissionRestAPI);
      meth = 'AdmissionRestAPI.testGetVolumeMetrics';
      context = this.setupContext(req);
      this.logger.info(`${meth} - User: ${context.user.id || context.user.name || ''}`);
      pvcName = getFromReq(req, 'pvc');
      pvcvName = 'c0volume0-kd-164933-0b08f516-worker000-deployment-0';
      owner = context != null ? (ref = context.user) != null ? ref.id : void 0 : void 0;
      filter = {
        'kumori/owner': (context != null ? (ref1 = context.user) != null ? ref1.id : void 0 : void 0) || 'ANONYMOUS'
      };
      result = {};
      return this.k8sApiStub.getPvcMetrics(KUMORI_NAMESPACE, pvcName).then(metrics => {
        var response;
        console.log("RETRIEVED VOLUME METRICS");
        response = {
          success: true,
          message: "GOT VOLUME METRICS",
          data: metrics
        };
        res.send(response);
        return true;
      }).catch(err => {
        var response;
        console.log("GET VOLUME METRICS FAILED");
        console.log(`ERROR: ${err.message} - ${err.stack}`);
        response = {
          success: false,
          message: 'GET VOLUME METRICS FAILED',
          data: {}
        };
        res.send(response);
        return true;
      });
    }

    //#############################################################################
    //#                     END OF DEV TEST METHODS (TEMPORARY)                  ##
    //#############################################################################

    //#############################################################################
    //#                     GENERIC "NOT IMPLMENTED" API METHODS                 ##
    //#############################################################################
    apiNotImplemented(req, res) {
      var response;
      response = {
        success: false,
        message: 'API METHOD NOT IMPLEMENTED YET',
        data: {}
      };
      res.send(response);
      return true;
    }

    checkUserMgmt(req, res, next) {
      var meth, msg, ref, response;
      boundMethodCheck(this, AdmissionRestAPI);
      meth = 'AdmissionRestAPI.checkUserMgmt';
      if (((ref = this.config.authentication) != null ? ref.keycloakConfig : void 0) != null) {
        return next();
      } else {
        msg = "User management is currently disabled in Admission.";
        this.logger.warn(`${meth} - ${msg}`);
        response = {
          success: false,
          message: msg
        };
        return res.json(response);
      }
    }

    checkRescheduling(req, res, next) {
      var meth, msg, ref, ref1, response;
      boundMethodCheck(this, AdmissionRestAPI);
      meth = 'AdmissionRestAPI.checkRescheduling';
      if (((ref = this.config.rescheduling) != null ? ref.enabled : void 0) != null && ((ref1 = this.config.rescheduling) != null ? ref1.enabled : void 0)) {
        return next();
      } else {
        msg = "Rescheduling is currently disabled in the cluster.";
        this.logger.warn(`${meth} - ${msg}`);
        response = {
          success: false,
          message: msg
        };
        return res.json(response);
      }
    }

    checkAlarmsMgmt(req, res, next) {
      var meth, msg, ref, ref1, response;
      boundMethodCheck(this, AdmissionRestAPI);
      meth = 'AdmissionRestAPI.checkAlarmsMgmt';
      if (((ref = this.config.alarmsRestAPI) != null ? ref.enabled : void 0) != null && ((ref1 = this.config.alarmsRestAPI) != null ? ref1.enabled : void 0)) {
        return next();
      } else {
        msg = "Alarms are currently disabled in the cluster.";
        this.logger.warn(`${meth} - ${msg}`);
        response = {
          success: false,
          message: msg
        };
        return res.json(response);
      }
    }

    deprecated(req, res, next) {
      boundMethodCheck(this, AdmissionRestAPI);
      this.logger.warn("*************************************************************");
      this.logger.warn(`** DEPRECATED API METHOD USED: ${req.method} ${req.path}`);
      this.logger.warn("*************************************************************");
      return next();
    }

    //#############################################################################
    //#                   MANAGEMENT API IMPLEMENTATION METHODS                  ##
    //#############################################################################
    health(req, res) {
      res.status(200).json({
        status: "healthy"
      });
      return true;
    }

    ready(req, res) {
      res.status(200).json({
        status: "ready"
      });
      return true;
    }

    processShutdown(context) {
      var meth;
      boundMethodCheck(this, AdmissionRestAPI);
      meth = 'AdmissionRestAPI.processShutdown';
      this.logger.info(meth);
      // If shutdown has been completed just return
      if (this.shutdownStatus.completed) {
        this.logger.info(`${meth} - Admission cluster shutdown is complete.`);
        return q();
      }
      // If shutdown has been triggered, just return the current status
      if (this.shutdownStatus.initiated) {
        this.logger.info(`${meth} - Admission cluster shutdown is ongoing.`);
        return q();
      }
      // Initiate the shutdown process, but leave it run in the background and
      // return
      this.shutdownOperations(context).done();
      return q();
    }

    shutdownOperations(context) {
      var meth;
      boundMethodCheck(this, AdmissionRestAPI);
      meth = 'AdmissionRestAPI.shutdownOperations';
      this.logger.info(meth);
      // This method will launch the necessary cleanup operations
      this.shutdownStatus.initiated = true;
      return q().then(() => {
        // Delete solutions
        return this.shutdownRemoveSolutions(context);
      }).then(() => {
        // If all solutions are deleted, delete all Volumes
        if (this.shutdownStatus.solutionsDeleted) {
          return this.shutdownRemoveVolumes(context);
        } else {
          return true;
        }
      }).then(() => {
        // If all solutions and volumes are deleted, delete all Ingress objects
        if (this.shutdownStatus.solutionsDeleted && this.shutdownStatus.volumesDeleted) {
          return this.shutdownRemoveIngresses(context);
        } else {
          return true;
        }
      }).then(() => {
        // If all solutions, volumes and ingresses are deleted, delete the Ingress
        // LoadBalancer Kubernetes service
        if (this.shutdownStatus.solutionsDeleted && this.shutdownStatus.volumesDeleted && this.shutdownStatus.ingressesDeleted) {
          return this.shutdownRemoveIngressLBService(context);
        } else {
          return true;
        }
      }).then(() => {
        // If all solutions, volumes ingresses and the Ingress LoadBalancer service
        // are deleted, then the cleanup is complete.
        // If not, wait and make a recursive call to this same method.
        if (this.shutdownStatus.solutionsDeleted && this.shutdownStatus.volumesDeleted && this.shutdownStatus.ingressesDeleted && this.shutdownStatus.ingressLoadBalancerDeleted) {
          this.shutdownStatus.completed = true;
          return true;
        } else {
          // Wait for some seconds and make a recursive call to this same method
          return q.delay(SHUTDOWN_RETRY_WAIT_MILLIS).then(() => {
            return this.shutdownOperations(context);
          });
        }
      }).catch(err => {
        // Ignore any error (just log it) and retry.
        this.logger.warn(`${meth} - ERROR (ignored and retrying): ${err.message}`);
        // Wait for some seconds and make a recursive call to this same method
        return q.delay(SHUTDOWN_RETRY_WAIT_MILLIS).then(() => {
          return this.shutdownOperations(context);
        });
      });
    }

    shutdownRemoveSolutions(context) {
      var errors, meth;
      boundMethodCheck(this, AdmissionRestAPI);
      meth = 'AdmissionRestAPI.shutdownRemoveSolutions';
      this.logger.info(meth);
      if (this.shutdownStatus.solutionsDeleted) {
        this.logger.info(`${meth} - Solutions already deleted, nothing to do.`);
        return q(true);
      }
      errors = [];
      // List all Solutions
      return this.manifestRepository.listElementType('solutions').then(solutionList => {
        var msg, promiseChain, solData, solURN;
        if (Object.keys(solutionList).length === 0) {
          // No solutions found
          msg = "No solutions found.";
          this.shutdownStatus.messages.push(`${utils.getTimestamp()} ${msg}`);
          this.logger.debug(`${meth} - ${msg}`);
          this.shutdownStatus.solutionsDeleted = true;
          return true;
        }
        this.logger.info(`${meth} - Found ${Object.keys(solutionList).length} solutions.`);
        // Delete all solutions (chain operations as promises)
        promiseChain = q();
        for (solURN in solutionList) {
          solData = solutionList[solURN];
          if (solData.deletionTimestamp != null) {
            // If solution is already being deleted, skip it
            continue;
          } else {
            ((solURN, solData) => {
              return promiseChain = promiseChain.finally(() => {
                msg = `Deleting solution: ${solURN}`;
                this.shutdownStatus.messages.push(`${utils.getTimestamp()} ${msg}`);
                this.logger.debug(`${meth} - ${msg}`);
                return this.adm.deleteSolution(context, {
                  solutionURN: solURN,
                  force: true
                }).then(solutionDeletionInfo => {
                  msg = `Deleted solution: ${solURN}`;
                  this.shutdownStatus.messages.push(`${utils.getTimestamp()} ${msg}`);
                  this.logger.debug(`${meth} - ${msg}`);
                  return true;
                }).fail(error => {
                  msg = `Failed to delete solution ${solURN} : ${err.message || err}`;
                  this.shutdownStatus.messages.push(`${utils.getTimestamp()} ${msg}`);
                  errors.push(msg);
                  this.logger.error(`ERROR: ${msg} - `, error.stack || error);
                  return true;
                });
              });
            })(solURN, solData);
          }
        }
        return promiseChain.finally(() => {
          if (errors.length > 0) {
            // Some errors detected deleting the solutions
            msg = `All solutions deletion processed. Numer of errors: ${errors.length}`;
            this.shutdownStatus.messages.push(`${utils.getTimestamp()} ${msg}`);
            this.logger.info(`${meth} ${msg}`);
            return false;
          } else {
            msg = "All solutions deletion processed. No errors detected.";
            this.shutdownStatus.messages.push(`${utils.getTimestamp()} ${msg}`);
            this.logger.info(`${meth} ${msg}`);
            return true;
          }
        });
      });
    }

    shutdownRemoveIngresses(context) {
      var errors, meth;
      boundMethodCheck(this, AdmissionRestAPI);
      meth = 'AdmissionRestAPI.shutdownRemoveIngresses';
      this.logger.info(meth);
      if (this.shutdownStatus.ingressesDeleted) {
        this.logger.info(`${meth} - Ingress objects already deleted, nothing to do.`);
        return q(true);
      }
      errors = [];
      // List all Solutions
      return this.k8sApiStub.listIngresses().then(items => {
        var i, item, len, msg, name, ns, promiseChain, ref;
        if (items == null || items.length === 0) {
          // No ingresses found
          msg = "No Ingress objects found.";
          this.shutdownStatus.messages.push(`${utils.getTimestamp()} ${msg}`);
          this.logger.debug(`${meth} - ${msg}`);
          this.shutdownStatus.ingressesDeleted = true;
          return true;
        }
        this.logger.info(`${meth} - Found ${items.length} Ingress objects.`);
        // Delete all ingresses (chain operations as promises)
        promiseChain = q();
        for (i = 0, len = items.length; i < len; i++) {
          item = items[i];
          if (((ref = item.metadata) != null ? ref.deletionTimestamp : void 0) != null) {
            // If ingress is already being deleted, skip it
            continue;
          } else {
            ns = item.metadata.namespace;
            name = item.metadata.name;
            ((ns, name) => {
              return promiseChain = promiseChain.finally(() => {
                msg = `Deleting Ingress object ${ns}/${name}...`;
                this.shutdownStatus.messages.push(`${utils.getTimestamp()} ${msg}`);
                this.logger.debug(`${meth} - ${msg}`);
                return this.k8sApiStub.deleteIngress(ns, name).then(() => {
                  msg = `Deleted ingress: ${ns}/${name}`;
                  this.shutdownStatus.messages.push(`${utils.getTimestamp()} ${msg}`);
                  this.logger.debug(`${meth} - ${msg}`);
                  return true;
                }).catch(error => {
                  msg = `Failed to delete Ingress ${ns}/${name} : ${err.message || err}`;
                  this.shutdownStatus.messages.push(`${utils.getTimestamp()} ${msg}`);
                  errors.push(msg);
                  this.logger.error(`ERROR: ${msg} - `, error.stack || error);
                  return true;
                });
              });
            })(ns, name);
          }
        }
        return promiseChain.finally(() => {
          if (errors.length > 0) {
            // Some errors detected deleting the ingresses
            msg = `All ingresses deletion processed. Numer of errors: ${errors.length}`;
            this.shutdownStatus.messages.push(`${utils.getTimestamp()} ${msg}`);
            this.logger.info(`${meth} ${msg}`);
            return false;
          } else {
            msg = "All ingresses deletion processed. No errors detected.";
            this.shutdownStatus.messages.push(`${utils.getTimestamp()} ${msg}`);
            this.logger.info(`${meth} ${msg}`);
            return true;
          }
        });
      });
    }

    shutdownRemoveVolumes(context) {
      var errors, meth;
      boundMethodCheck(this, AdmissionRestAPI);
      meth = 'AdmissionRestAPI.shutdownRemoveVolumes';
      this.logger.info(meth);
      if (this.shutdownStatus.volumesDeleted) {
        this.logger.info(`${meth} - Volumes already deleted, nothing to do.`);
        return q(true);
      }
      errors = [];
      // List all Volumes
      return this.manifestRepository.listElementType('volumes').then(volumeList => {
        var msg, promiseChain, volData, volURN;
        if (Object.keys(volumeList).length === 0) {
          // No volumes found
          msg = "No volumes found.";
          this.shutdownStatus.messages.push(`${utils.getTimestamp()} ${msg}`);
          this.logger.debug(`${meth} - ${msg}`);
          this.shutdownStatus.volumesDeleted = true;
          return true;
        }
        this.logger.info(`${meth} - Found ${Object.keys(volumeList).length} volumes.`);
        // Delete all volumes (chain operations as promises)
        promiseChain = q();
        for (volURN in volumeList) {
          volData = volumeList[volURN];
          if (volData.deletionTimestamp != null) {
            // If volume is already being deleted, skip it
            continue;
          } else {
            ((volURN, volData) => {
              return promiseChain = promiseChain.finally(() => {
                msg = `Deleting volume: ${volURN} (${volData.name})`;
                this.shutdownStatus.messages.push(`${utils.getTimestamp()} ${msg}`);
                this.logger.debug(`${meth} - ${msg}`);
                return this.k8sApiStub.deleteKumoriElement('kukuvolumes', volData.name).then(volumeDeletionInfo => {
                  msg = `Deleted volume: ${volURN}`;
                  this.shutdownStatus.messages.push(`${utils.getTimestamp()} ${msg}`);
                  this.logger.debug(`${meth} - ${msg}`);
                  return true;
                }).fail(error => {
                  msg = `Failed to delete volume ${volURN} : ${err.message || err}`;
                  this.shutdownStatus.messages.push(`${utils.getTimestamp()} ${msg}`);
                  errors.push(msg);
                  this.logger.error(`ERROR: ${msg} - `, error.stack || error);
                  return true;
                });
              });
            })(volURN, volData);
          }
        }
        return promiseChain.finally(() => {
          if (errors.length > 0) {
            // Some errors detected deleting the volumes
            msg = `All volumes deletion processed. Numer of errors: ${errors.length}`;
            this.shutdownStatus.messages.push(`${utils.getTimestamp()} ${msg}`);
            this.logger.info(`${meth} ${msg}`);
            return false;
          } else {
            msg = "All volumes deletion processed. No errors detected.";
            this.shutdownStatus.messages.push(`${utils.getTimestamp()} ${msg}`);
            this.logger.info(`${meth} ${msg}`);
            return true;
          }
        });
      });
    }

    shutdownRemoveIngressLBService(context) {
      var clusterName, ingressServiceName, meth;
      boundMethodCheck(this, AdmissionRestAPI);
      meth = 'AdmissionRestAPI.shutdownRemoveIngressLBService';
      this.logger.info(meth);
      if (this.shutdownStatus.ingressLoadBalancerDeleted) {
        this.logger.info(`${meth} - Ingress LB Service already deleted, nothing to do.`);
        return q(true);
      }
      // Determine the Ingress Service name
      clusterName = this.latestClusterConfiguration.clusterName;
      ingressServiceName = INGRESS_SERVICE_OBJECT_PREFIX + clusterName;
      // Check if the Ingress Service still exists
      return this.k8sApiStub.getService(KUMORI_NAMESPACE, ingressServiceName).then(ingressService => {
        if (ingressService.metadata.deletionTimestamp != null) {
          // If Ingress Service is already being deleted, skip it
          this.logger.info(`${meth} - Kubernetes Service ${ingressServiceName} exists and is already marked for deletion.`);
          return true;
        } else {
          // Ingress Service is not marked for deletion, delete it
          this.logger.info(`${meth} - Kubernetes Service ${ingressServiceName} exists, delete it...`);
          this.shutdownStatus.messages.push(`${utils.getTimestamp()} Deleting Ingress Service object.`);
          return this.k8sApiStub.deleteService(KUMORI_NAMESPACE, ingressServiceName).then(() => {
            this.logger.info(`${meth} - Kubernetes Service ${ingressServiceName} has been marked for deletion.`);
            return true;
          }).catch(error => {
            var errMsg;
            errMsg = `Failed to delete Kubernetes Service ${ingressServiceName}. ERROR: ${error.message}`;
            this.shutdownStatus.messages.push(`${utils.getTimestamp()} ${errMsg}`);
            this.logger.error(`${meth} - ${errMsg}`);
            return true;
          });
        }
      }).catch(error => {
        var errMsg, msg, ref, ref1;
        // Check if the error is due to the object not existing (HTTP status 404)
        if (((ref = error.message) != null ? ref.includes('NotFound') : void 0) || ((ref1 = error.message) != null ? ref1.includes('StatusCode 404') : void 0)) {
          // Service does not exist (has been deleted), mark it
          this.shutdownStatus.ingressLoadBalancerDeleted = true;
          msg = `Kubernetes Service ${ingressServiceName} does not exist.`;
          this.logger.info(`${meth} - ${msg}`);
          this.shutdownStatus.messages.push(`${utils.getTimestamp()} ${msg}`);
          return true;
        } else {
          // Some other error while getting the Service object
          errMsg = `Failed to get Kubernetes Service ${ingressServiceName}. ERROR: ${error.message}`;
          this.shutdownStatus.messages.push(`${utils.getTimestamp()} ${errMsg}`);
          this.logger.error(`${meth} - ${errMsg}`);
          return true;
        }
      });
    }

    shutdown(req, res) {
      var context, meth;
      boundMethodCheck(this, AdmissionRestAPI);
      meth = 'AdmissionRestAPI.shutdown';
      this.logger.info(meth);
      context = this.setupContext(req);
      this.logger.info(`${meth} - User: ${context.user.id || context.user.name || ''}`);
      // The shutdown process will be carried out in the background, so this call
      // will return immediately
      return this.processShutdown(context).then(() => {
        var response;
        this.logger.info(`${meth} - Processed shutdown request.`);
        response = {
          success: true,
          message: "Cluster shutdown request processed.",
          data: this.shutdownStatus
        };
        res.json(response);
        return true;
      }).catch(error => {
        var response;
        this.logger.error(`${meth} - ERROR: ${error.message}`, error.stack);
        response = {
          success: false,
          message: "Failed to process shutdown request.",
          error: error.stack
        };
        res.json(response);
        return false;
      });
    }

    maintenance(req, res) {
      var action, context, errMsg, meth, response;
      boundMethodCheck(this, AdmissionRestAPI);
      meth = 'AdmissionRestAPI.maintenance';
      this.logger.info(meth);
      context = this.setupContext(req);
      action = getFromReq(req, 'action');
      this.logger.info(`${meth} - User: ${context.user.id || context.user.name || ''} - Action: ${action}`);
      if (action == null) {
        errMsg = "missing mandatory parameter: action";
        this.logger.warn(`${meth} - ERROR: ${errMsg}`);
        response = {
          success: false,
          message: `Unable to process maintenance request: ${errMsg}`
        };
        return res.json(response);
      } else if ((action != null ? action.toLowerCase() : void 0) === 'offline') {
        // Set maintenance mode for Admission API
        this.downForMaintenance = true;
        response = {
          success: true,
          message: 'Admission API is now down for maintenance.',
          data: null
        };
        return res.json(response);
      } else if ((action != null ? action.toLowerCase() : void 0) === 'online') {
        // Unset maintenance mode for Admission API
        this.downForMaintenance = false;
        response = {
          success: true,
          message: 'Admission API is now online.',
          data: null
        };
        return res.json(response);
      } else {
        errMsg = `unknown maintenance action: ${action}`;
        this.logger.warn(`${meth} - ERROR: ${errMsg}`);
        response = {
          success: false,
          message: `Unable to process action: ${errMsg}`
        };
        return res.send(response);
      }
    }

    reschedule(req, res) {
      var context, errMsg, meth, rescheduleConfig, rescheduleType, rescheduler, reschedulerOptions, response;
      boundMethodCheck(this, AdmissionRestAPI);
      meth = 'AdmissionRestAPI.reschedule';
      this.logger.info(meth);
      context = this.setupContext(req);
      rescheduleType = getFromReq(req, 'type');
      this.logger.info(`${meth} - User: ${context.user.id || context.user.name || ''} - Type: ${rescheduleType}`);
      // Get reschedule type from request
      if (rescheduleType == null) {
        errMsg = "missing mandatory parameter: type";
        this.logger.warn(`${meth} - ERROR: ${errMsg}`);
        response = {
          success: false,
          message: `Unable to process reschedule request: ${errMsg}`
        };
        res.json(response);
        return;
      }
      // Get config provided
      rescheduleConfig = req.body;
      if (rescheduleConfig == null) {
        rescheduleConfig = null;
        this.logger.info(`${meth} - No reschedule configuration provided. Defaults will be used if available.`);
      }
      // Instantiate a rescheduler object and create a job
      reschedulerOptions = {
        k8sApiStub: this.k8sApiStub
      };
      rescheduler = ReschedulerFactory.create(rescheduleType, reschedulerOptions);
      return rescheduler.createJob(rescheduleConfig).then(result => {
        this.logger.info(`${meth} - Reschedule job created.`);
        response = {
          success: true,
          message: "Reschedule request successfully processed.",
          data: null
        };
        res.json(response);
        return true;
      }).catch(error => {
        this.logger.error(`${meth} - ERROR: ${error.message}`, error.stack);
        response = {
          success: false,
          message: `Failed to process reschedule request: ${error.message}`,
          error: error.stack
        };
        res.json(response);
        return false;
      });
    }

    getRescheduleJobs(req, res) {
      var context, errMsg, meth, rescheduleType, rescheduler, reschedulerOptions, response;
      boundMethodCheck(this, AdmissionRestAPI);
      meth = 'AdmissionRestAPI.getRescheduleJobs';
      this.logger.info(meth);
      context = this.setupContext(req);
      rescheduleType = getFromReq(req, 'type');
      this.logger.info(`${meth} - User: ${context.user.id || context.user.name || ''} - Type: ${rescheduleType}`);
      // Get reschedule type from request
      if (rescheduleType == null) {
        errMsg = "missing mandatory parameter: type";
        this.logger.warn(`${meth} - ERROR: ${errMsg}`);
        response = {
          success: false,
          message: `Unable to process reschedule request: ${errMsg}`
        };
        res.json(response);
        return;
      }
      // Instantiate a rescheduler object and get job list
      reschedulerOptions = {
        k8sApiStub: this.k8sApiStub
      };
      rescheduler = ReschedulerFactory.create(rescheduleType, reschedulerOptions);
      return rescheduler.listJobs().then(result => {
        this.logger.info(`${meth} - Got job list.`);
        response = {
          success: true,
          message: "Retrieved reschedule job list.",
          data: result
        };
        res.json(response);
        return true;
      }).catch(error => {
        this.logger.error(`${meth} - ERROR: ${error.message}`, error.stack);
        response = {
          success: false,
          message: `Failed to process reschedule request: ${error.message}`,
          error: error.stack
        };
        res.json(response);
        return false;
      });
    }

    clearOperationLocks(req, res) {
      var context, meth, response;
      boundMethodCheck(this, AdmissionRestAPI);
      meth = 'AdmissionRestAPI.clearOperationLocks';
      this.logger.info(meth);
      context = this.setupContext(req);
      this.logger.info(`${meth} - User: ${context.user.id || context.user.name || ''} - Clear operation locks`);
      this.adm.clearElementLocks();
      response = {
        success: true,
        message: 'Operation locks cleared.',
        data: null
      };
      res.json(response);
      return true;
    }

    refreshToken(req, res) {
      var kcReq, meth, msg, ref, refreshToken, result;
      boundMethodCheck(this, AdmissionRestAPI);
      meth = 'AdmissionRestAPI.refreshToken';
      this.logger.info(meth);
      if (((ref = this.config.authentication) != null ? ref.keycloakConfig : void 0) != null) {
        // Refresh token is received as POST data
        if (!('refresh_token' in req.body)) {
          msg = "Mandatory 'request_token' parameter not found.";
          this.logger.warn(`${meth} - ${msg}`);
          res.json({
            message: msg
          });
          return;
        }
        if (!('grant_type' in req.body)) {
          msg = "Mandatory 'grant_type' parameter not found.";
          this.logger.warn(`${meth} - ${msg}`);
          res.json({
            message: msg
          });
          return;
        }
        if (req.body['grant_type'] !== 'refresh_token') {
          msg = `Unsupported 'grant_type' for refresh token method (${req.body['grant_type']})`;
          this.logger.warn(`${meth} - ${msg}`);
          res.json({
            message: msg
          });
          return;
        }
        refreshToken = req.body['refresh_token'];
        kcReq = new KeycloakRequest(this.config.authentication.keycloakConfig);
        this.logger.info(`${meth} - GETTING FRESH TOKEN FROM KEYCLOAK...`);
        result = null;
        return kcReq.refreshAccessToken(refreshToken).then(data => {
          result = data;
          this.logger.info(`${meth} - GOT KEYCLOAK DATA`);
          // @logger.info "#{meth} - GOT KEYCLOAK DATA: #{JSON.stringify data}"
          // res.json { message: data }
          return kcReq.getUserInfo(data.access_token);
        }).then(data => {
          this.logger.info(`${meth} - GOT KEYCLOAK USER INFO`);
          // @logger.info "#{meth} - GOT KEYCLOAK USER INFO: #{JSON.stringify data}"
          result.user = data;
          // @logger.info "#{meth} - RETURNING: #{JSON.stringify result}"
          return res.json(result);
        }).catch(err => {
          // console.log "*** LOGIN *** - KEYCLOAK ERROR: #{err.message} #{err.stack}"
          msg = `Login failed. Unable to login on Keycloak: ${err.message}`;
          this.logger.debug(`${meth} - ${msg}`);
          res.json({
            message: msg
          });
        });
      } else {
        // No authentication configured in Admission

        // Return a warning stating that authentication is disabled
        msg = "Token-based authentication is currently disabled in Admission.";
        this.logger.warn(`${meth} - ${msg}`);
        res.json({
          message: msg
        });
      }
    }

    login(req, res) {
      var auth, authorization, kcReq, meth, msg, parts, password, ref, ref1, result, token, username;
      boundMethodCheck(this, AdmissionRestAPI);
      meth = 'AdmissionRestAPI.login';
      // console.log "REQUEST HEADERS: #{JSON.stringify req.headers}"
      // console.log "REQUEST BODY: #{JSON.stringify req.body}"
      if (((ref = this.config.authentication) != null ? ref.keycloakConfig : void 0) != null) {
        // "authorization":"Basic dXN1YXJpbzpwYXNzc3Nzc3Nz"
        if (((ref1 = req.headers) != null ? ref1.authorization : void 0) != null) {
          authorization = req.headers.authorization;
          token = authorization.split(/\s+/).pop();
          auth = Buffer.from(token, 'base64').toString();
          parts = auth.split(/:/);
          username = parts[0];
          password = parts[1];
          // console.log "LOGIN DATA: user:#{username}  password:#{password}"
          kcReq = new KeycloakRequest(this.config.authentication.keycloakConfig);
          this.logger.info(`${meth} - GETTING KEYCLOAK DATA...`);
          result = null;
          return kcReq.getAccessToken(username, password).then(data => {
            result = data;
            this.logger.info(`${meth} - GOT KEYCLOAK DATA`);
            // @logger.info "#{meth} - GOT KEYCLOAK DATA: #{JSON.stringify data}"
            // res.json { message: data }
            return kcReq.getUserInfo(data.access_token);
          }).then(data => {
            this.logger.info(`${meth} - GOT KEYCLOAK USER INFO`);
            // @logger.info "#{meth} - GOT KEYCLOAK USER INFO: #{JSON.stringify data}"
            result.user = data;
            // @logger.info "#{meth} - RETURNING: #{JSON.stringify result}"
            return res.json(result);
          }).catch(err => {
            var msg;
            // console.log "*** LOGIN *** - KEYCLOAK ERROR: #{err.message} #{err.stack}"
            msg = `Login failed. Unable to login on Keycloak: ${err.message}`;
            this.logger.debug(`${meth} - ${msg}`);
            return res.json({
              message: msg
            });
          });
        } else {
          // console.log "*** LOGIN *** - LOGIN FAILED (no authorization header)"
          msg = "Login failed. Unable to retrieve authorization header.";
          this.logger.debug(`${meth} - ${msg}`);
          return res.json({
            message: msg
          });
        }
      } else {
        // No authentication configured in Admission

        // Return a warning stating that authentication is disabled
        msg = "Token-based authentication is currently disabled in Admission.";
        this.logger.warn(`${meth} - ${msg}`);
        return res.json({
          message: msg
        });
      }
    }

    oldLogin(req, res) {
      var authData, context, meth, msg;
      boundMethodCheck(this, AdmissionRestAPI);
      // For backwards compatibility with ACS, this should return an object like:
      //     access_token: token
      //     token_type: 'Bearer'
      //     expires_in: @config.accessTokenTTL
      //     refresh_token: refresh
      //     user: user

      // where user object is:
      //   - email: "slapbot@iti.es"
      //   - id: "slapbot@iti.es"
      //   - limits: {max_bundle_size: -1, max_storage_space: -1, ...}
      //   - name: "slapbot"
      //   - roles: ["ADMIN"]

      // Currently, it returns:

      // {
      //   "message": "Access granted to user alice@keycloak.org (Roles: developer)",
      //   "context": {
      //     "user": {
      //       "id": "alice@keycloak.org",
      //       "roles": [
      //         "developer"
      //       ]
      //     }
      //   },
      //   "userData": {
      //     "username": "alice",
      //     "name": "Alice Liddel",
      //     "email": "alice@keycloak.org",
      //     "groups": [
      //       "offline_access",
      //       "user"
      //     ],
      //     "kumoriGroups": [
      //       "developer"
      //     ]
      //   }
      // }
      meth = 'AdmissionRestAPI.login';
      authData = getAuthData(req);
      context = this.setupContext(req);
      console.log(`*** LOGIN *** - AUTH DATA: ${JSON.stringify(authData, null, 2)}`);
      console.log(`*** LOGIN *** - CONTEXT: ${JSON.stringify(context, null, 2)}`);
      if (authData === null) {
        console.log("*** LOGIN *** - LOGIN FAILED (authData is null)");
        msg = "Login failed. Unable to retrieve authentication data.";
        this.logger.debug(`${meth} - ${msg}`);
        return res.json({
          message: msg
        });
      } else {
        console.log(`*** LOGIN *** - RETURNING: ${JSON.stringify(authData, null, 2)}`);
        this.logger.debug(`${meth} - Successful login for user: ${authData.user.id}`);
        // msg = "Login successfull for user #{context.user.id} (Roles: #{context.user.roles})"
        // msg = "Access granted to user #{context.user.id} (Roles: #{context.user.roles})"
        // res.json { message: msg, context: context, userData: userData }
        return res.json(authData);
      }
    }

    listUsers(req, res) {
      var accessToken, context, meth, results;
      boundMethodCheck(this, AdmissionRestAPI);
      meth = 'AdmissionRestAPI.listUsers';
      context = this.setupContext(req);
      this.logger.info(`${meth} - User: ${context.user.id || context.user.name || ''}`);
      results = [];
      accessToken = req.kauth.grant.access_token.token;
      return this.keycloakUserManager.getUsers(accessToken).then(data => {
        var response;
        // console.log "LIST USERS FINISHED OK AND RETURNED #{data.length} RESULTS"
        response = {
          success: true,
          message: `GOT ${data.length} USERS.`,
          data: data
        };
        res.send(response);
        return true;
      }).catch(err => {
        var response;
        this.logger.error(`ERROR: ${err.message || err}`);
        response = {
          success: false,
          message: `ERROR: ${err.message || err}`
        };
        res.send(response);
        return true;
      });
    }

    getUser(req, res) {
      var accessToken, context, errMsg, meth, response, username;
      boundMethodCheck(this, AdmissionRestAPI);
      meth = 'AdmissionRestAPI.getUser';
      context = this.setupContext(req);
      this.logger.info(`${meth} - User: ${context.user.id || context.user.name || ''}`);
      username = getFromReq(req, 'username');
      if (!username) {
        errMsg = 'Missing mandatory parameter (username)';
        this.logger.error(`${meth} ERROR: ${errMsg}`);
        response = {
          success: false,
          message: errMsg
        };
        res.send(response);
        return q();
      }
      accessToken = req.kauth.grant.access_token.token;
      return this.keycloakUserManager.getUser(username, accessToken).then(data => {
        // console.log "GET USER #{username} FINISHED OK AND RETURNED #{data}"
        response = {
          success: true,
          message: `GOT USER ${username}.`,
          data: data
        };
        res.send(response);
        return true;
      }).catch(err => {
        this.logger.error(`ERROR: ${err.message || err}`);
        response = {
          success: false,
          message: `ERROR: ${err.message || err}`
        };
        res.send(response);
        return true;
      });
    }

    deleteUser(req, res) {
      var accessToken, context, errMsg, meth, response, username;
      boundMethodCheck(this, AdmissionRestAPI);
      meth = 'AdmissionRestAPI.deleteUser';
      context = this.setupContext(req);
      this.logger.info(`${meth} - User: ${context.user.id || context.user.name || ''}`);
      username = getFromReq(req, 'username');
      if (!username) {
        errMsg = 'Missing mandatory parameter (username)';
        this.logger.error(`${meth} ERROR: ${errMsg}`);
        response = {
          success: false,
          message: errMsg
        };
        res.send(response);
        return q();
      }
      accessToken = req.kauth.grant.access_token.token;
      return this.keycloakUserManager.deleteUser(username, accessToken).then(data => {
        // console.log "DELETE USER #{username} FINISHED OK AND RETURNED #{data}"
        response = {
          success: true,
          message: `DELETED USER ${username}.`,
          data: data
        };
        res.send(response);
        return true;
      }).catch(err => {
        this.logger.error(`ERROR: ${err.message || err}`);
        response = {
          success: false,
          message: `ERROR: ${err.message || err}`
        };
        res.send(response);
        return true;
      });
    }

    createUser(req, res) {
      var accessToken, context, errMsg, meth, response, results, user;
      boundMethodCheck(this, AdmissionRestAPI);
      meth = 'AdmissionRestAPI.createUser';
      context = this.setupContext(req);
      this.logger.info(`${meth} - User: ${context.user.id || context.user.name || ''}`);
      results = [];
      user = req.body;
      if (!user) {
        errMsg = 'Missing mandatory user details.';
        this.logger.error(`${meth} ERROR: ${errMsg}`);
        response = {
          success: false,
          message: errMsg
        };
        res.send(response);
        return q();
      }
      accessToken = req.kauth.grant.access_token.token;
      return this.keycloakUserManager.createUser(user, accessToken).then(data => {
        // console.log "CREATE USER #{user.username} FINISHED OK"
        response = {
          success: true,
          message: `CREATED USER ${user.username}.`,
          data: data
        };
        res.send(response);
        return true;
      }).catch(err => {
        this.logger.error(`ERROR: ${err.message || err}`);
        response = {
          success: false,
          message: `ERROR: ${err.message || err}`
        };
        res.send(response);
        return true;
      });
    }

    updateUser(req, res) {
      var accessToken, context, errMsg, meth, response, results, user;
      boundMethodCheck(this, AdmissionRestAPI);
      meth = 'AdmissionRestAPI.updateUser';
      context = this.setupContext(req);
      this.logger.info(`${meth} - User: ${context.user.id || context.user.name || ''}`);
      results = [];
      user = req.body;
      if (!user) {
        errMsg = 'Missing mandatory user details.';
        this.logger.error(`${meth} ERROR: ${errMsg}`);
        response = {
          success: false,
          message: errMsg
        };
        res.send(response);
        return q();
      }
      accessToken = req.kauth.grant.access_token.token;
      return this.keycloakUserManager.updateUser(user, accessToken).then(data => {
        // console.log "UPDATE USER #{user.username} FINISHED OK"
        response = {
          success: true,
          message: `UPDATED USER ${user.username}.`,
          data: data
        };
        res.send(response);
        return true;
      }).catch(err => {
        this.logger.error(`ERROR: ${err.message || err}`);
        response = {
          success: false,
          message: `ERROR: ${err.message || err}`
        };
        res.send(response);
        return true;
      });
    }

    updateUserPassword(req, res) {
      var callerUsername, context, errMsg, isAdmin, isPrivileged, meth, passwordChangeData, ref, response, username;
      boundMethodCheck(this, AdmissionRestAPI);
      meth = 'AdmissionRestAPI.updateUserPassword';
      context = this.setupContext(req);
      this.logger.info(`${meth} - User: ${context.user.id || context.user.name || ''}`);
      username = getFromReq(req, 'username');
      if (!username) {
        errMsg = 'Missing mandatory parameter (username)';
        this.logger.error(`${meth} ERROR: ${errMsg}`);
        response = {
          success: false,
          message: errMsg
        };
        res.send(response);
        return q();
      }
      callerUsername = context != null ? (ref = context.user) != null ? ref.username : void 0 : void 0;
      isAdmin = this.authorization.isAdmin(context);
      // A privileged operation is one requested by an admin user that modifies the
      // password of a user other than himself.
      isPrivileged = isAdmin && username !== callerUsername;
      // Only accept operation on the caller's own user (unless admin)
      if (username !== callerUsername && !isAdmin) {
        errMsg = `Operation not allowed on user ${username} (current user ${callerUsername}).`;
        this.logger.error(`${meth} ERROR: ${errMsg}`);
        response = {
          success: false,
          message: errMsg
        };
        res.send(response);
        return q();
      }
      passwordChangeData = req.body;
      if (!passwordChangeData) {
        errMsg = 'Missing mandatory password change data.';
        this.logger.error(`${meth} ERROR: ${errMsg}`);
        response = {
          success: false,
          message: errMsg
        };
        res.send(response);
        return q();
      }
      return this.keycloakUserManager.updateUserPassword(username, passwordChangeData, isPrivileged).then(data => {
        // console.log "UPDATE USER #{user.username} FINISHED OK"
        response = {
          success: true,
          message: `UPDATED PASSWORD FOR USER ${username}.`,
          data: data
        };
        res.send(response);
        return true;
      }).catch(err => {
        this.logger.error(`ERROR: ${err.message || err}`);
        response = {
          success: false,
          message: `ERROR: ${err.message || err}`
        };
        res.send(response);
        return true;
      });
    }

    listGroups(req, res) {
      var accessToken, context, meth, results;
      boundMethodCheck(this, AdmissionRestAPI);
      meth = 'AdmissionRestAPI.listGroups';
      context = this.setupContext(req);
      this.logger.info(`${meth} - User: ${context.user.id || context.user.name || ''}`);
      results = [];
      accessToken = req.kauth.grant.access_token.token;
      return this.keycloakUserManager.getGroups(accessToken).then(data => {
        var response;
        // console.log "LIST GROUPS FINISHED OK. RETURNED #{data.length} RESULTS"
        response = {
          success: true,
          message: `GOT ${data.length} GROUPS.`,
          data: data
        };
        res.send(response);
        return true;
      }).catch(err => {
        var response;
        this.logger.error(`ERROR: ${err.message || err}`);
        response = {
          success: false,
          message: `ERROR: ${err.message || err}`
        };
        res.send(response);
        return true;
      });
    }

    deploymentQuery(req, res) {
      var context, meth, owner, queryParams, ref, urn;
      boundMethodCheck(this, AdmissionRestAPI);
      meth = 'AdmissionRestAPI.deploymentQuery';
      this.apiRequestCount++;
      context = this.setupContext(req);
      this.logger.info(`${meth} - User: ${context.user.id || context.user.name || ''}`);
      queryParams = {};
      owner = context != null ? (ref = context.user) != null ? ref.id : void 0 : void 0;
      if (!this.authorization.isAdmin(context)) {
        queryParams.owner = context.user.id;
      }
      urn = getFromReq(req, 'urn');
      if (urn != null) {
        // If getting a specific URN, return all available data
        queryParams.urn = urn;
        queryParams.show = 'extended';
      } else {
        // If listing all deployments, return only a urn list unless a specific
        // format is specified
        if (!(req.query.show != null && req.query.show !== '')) {
          queryParams.show = 'urn';
        } else {
          queryParams.show = req.query.show;
        }
      }
      // @logger.debug "#{meth} REQ: #{JSON.stringify req.headers, null, 2}"
      this.logger.info(`${meth} URN: ${urn}`);
      this.logger.debug(`${meth} req.query: ${JSON.stringify(req.query)}`);
      this.logger.debug(`${meth} queryParams: ${JSON.stringify(queryParams)}`);
      return this.adm.deploymentQuery(context, queryParams).then(function (info) {
        return res.json({
          success: true,
          message: 'SUCCESSFULLY PROCESSED DEPLOYMENT INFO.',
          data: info
        });
      }).fail(error => {
        this.logger.error('ERROR:', error.stack || error);
        return res.send({
          success: false,
          message: `ERROR PROCESSING DEPLOYMENTS QUERY: ${error.message}`
        });
      });
    }

    solutionQuery(req, res) {
      var context, meth, owner, queryParams, ref, urn;
      boundMethodCheck(this, AdmissionRestAPI);
      meth = 'AdmissionRestAPI.solutionQuery';
      this.apiRequestCount++;
      context = this.setupContext(req);
      this.logger.info(`${meth} - User: ${context.user.id || context.user.name || ''}`);
      queryParams = {};
      owner = context != null ? (ref = context.user) != null ? ref.id : void 0 : void 0;
      if (!this.authorization.isAdmin(context)) {
        queryParams.owner = context.user.id;
      }
      urn = getFromReq(req, 'urn');
      if (urn != null) {
        // If getting a specific URN, return all available data
        queryParams.urn = urn;
        queryParams.show = 'extended';
      } else {
        // If listing all deployments, return only a urn list unless a specific
        // format is specified
        if (!(req.query.show != null && req.query.show !== '')) {
          queryParams.show = 'urn';
        } else {
          queryParams.show = req.query.show;
        }
      }
      // @logger.debug "#{meth} REQ: #{JSON.stringify req.headers, null, 2}"
      this.logger.info(`${meth} URN: ${urn}`);
      this.logger.debug(`${meth} req.query: ${JSON.stringify(req.query)}`);
      this.logger.debug(`${meth} queryParams: ${JSON.stringify(queryParams)}`);
      return this.adm.solutionQuery(context, queryParams).then(function (info) {
        return res.json({
          success: true,
          message: 'SUCCESSFULLY PROCESSED SOLUTION INFO.',
          data: info
        });
      }).fail(error => {
        this.logger.error('ERROR:', error.stack || error);
        return res.send({
          success: false,
          message: `ERROR PROCESSING SOLUTIONS QUERY: ${error.message}`
        });
      });
    }

    registerSolution(req, res) {
      var context, filesToCleanup, meth, options, ref, ref1, response;
      boundMethodCheck(this, AdmissionRestAPI);
      meth = 'AdmissionRestAPI.registerSolution';
      this.apiRequestCount++;
      context = this.setupContext(req);
      this.logger.info(`${meth} - User: ${context.user.id || context.user.name || ''}`);
      filesToCleanup = [];
      // @logger.debug "#{meth} REQ: #{JSON.stringify req.headers, null, 2}"
      this.logger.debug(`${meth} - DATA: ${JSON.stringify(req.body)}`);
      if (this.config.restTimeout !== -1) {
        res.setTimeout(this.config.restTimeout, () => {
          res.send({
            success: false,
            message: "ADMISSION SOLUTION CREATION TIMEOUT"
          });
          return res.send = m => {
            return this.logger.warn(`HTTP Response Already Sent ${JSON.stringify(m)}`);
          };
        });
      }
      options = null;
      if (((ref = req.files) != null ? (ref1 = ref[0]) != null ? ref1.fieldname : void 0 : void 0) === 'inline') {
        options = {
          filename: req.files[0].path
        };
        filesToCleanup.push(req.files[0].path);
      } else {
        this.logger.error(`${meth} - ERROR: Missing mandatory parameter (inline file)`);
        response = {
          success: false,
          message: 'ERROR PROCESSING SOLUTION: Missing mandatory parameter (inline file)'
        };
        res.send(response);
        // Cleanup uploaded files from temporary directory
        cleanupFiles.call(this, filesToCleanup);
        return;
      }
      // Proceed to registering the solution in Admission
      return this.adm.registerSolution(context, options).then(solutionRegistrationInfo => {
        this.logger.debug(`${meth} - Processed Solution: ${JSON.stringify(solutionRegistrationInfo)}`);
        response = {
          success: true,
          message: 'SUCCESSFULLY PROCESSED SOLUTION: ' + `${solutionRegistrationInfo.solutionURN}`,
          data: solutionRegistrationInfo
        };
        return res.send(response);
      }).fail(error => {
        this.logger.error(`${meth} - ERROR: ${error.message}`, error.stack);
        response = {
          success: false,
          message: `ERROR PROCESSING SOLUTION: ${error.message}`,
          error: error.stack
        };
        return res.send(response);
      }).finally(() => {
        // Cleanup uploaded files from temporary directory
        return cleanupFiles.call(this, filesToCleanup);
      });
    }

    deleteSolution(req, res) {
      var context, errMsg, force, meth, owner, ref, response, urn;
      boundMethodCheck(this, AdmissionRestAPI);
      meth = 'AdmissionRestAPI.deleteSolution';
      this.apiRequestCount++;
      context = this.setupContext(req);
      this.logger.info(`${meth} - User: ${context.user.id || context.user.name || ''}`);
      owner = context != null ? (ref = context.user) != null ? ref.id : void 0 : void 0;
      urn = getFromReq(req, 'urn');
      force = getFromReq(req, 'force');
      force = force != null && force === 'yes';
      this.logger.info(`${meth} URN: ${urn} (force: ${force})`);
      // @logger.debug "#{meth} REQ: #{JSON.stringify req.headers, null, 2}"
      this.logger.debug(`${meth} - QUERY: ${JSON.stringify(req.query)}`);
      if (!urn) {
        errMsg = 'Missing mandatory parameter (solution URN)';
        this.logger.error(`${meth} - ERROR: ${errMsg}`);
        response = {
          success: false,
          message: errMsg
        };
        res.send(response);
        return q();
      } else {
        if (this.config.restTimeout !== -1) {
          res.setTimeout(this.config.restTimeout, () => {
            res.send({
              success: false,
              message: 'ADMISSION DELETE SOLUTION TIMEOUT'
            });
            return res.send = m => {
              return this.logger.warn(`HTTP Response Already Sent ${JSON.stringify(m)}`);
            };
          });
        }
        return this.adm.deleteSolution(context, {
          solutionURN: urn,
          force: force
        }).then(solutionDeletionInfo => {
          this.logger.debug(`${meth} - Deleted solution: ${solutionDeletionInfo}`);
          response = {
            success: true,
            message: `SUCCESSFULLY DELETED SOLUTION: ${urn}`,
            data: solutionDeletionInfo
          };
          return res.send(response);
        }).fail(error => {
          this.logger.error('ERROR:', error.stack || error);
          response = {
            success: false,
            message: `ERROR DELETING SOLUTION: ${error.message}`
          };
          return res.send(response);
        });
      }
    }

    deploy(req, res) {
      var context, filesToCleanup, meth, options, ref, ref1, response;
      boundMethodCheck(this, AdmissionRestAPI);
      meth = 'AdmissionRestAPI.deploy';
      this.apiRequestCount++;
      context = this.setupContext(req);
      this.logger.info(`${meth} - User: ${context.user.id || context.user.name || ''}`);
      filesToCleanup = [];
      // @logger.debug "#{meth} REQ: #{JSON.stringify req.headers, null, 2}"
      this.logger.debug(`${meth} DATA: ${JSON.stringify(req.body)}`);
      if (this.config.restTimeout !== -1) {
        res.setTimeout(this.config.restTimeout, () => {
          res.send({
            success: false,
            message: 'ADMISSION DEPLOY TIMEOUT'
          });
          return res.send = m => {
            return this.logger.warn(`HTTP Response Already Sent ${JSON.stringify(m)}`);
          };
        });
      }
      options = null;
      if (((ref = req.files) != null ? (ref1 = ref[0]) != null ? ref1.fieldname : void 0 : void 0) === 'inline') {
        options = {
          filename: req.files[0].path
        };
        filesToCleanup.push(req.files[0].path);
      } else if (req.body.url) {
        options = {
          url: req.body.url
        };
      } else if (req.body.filename) {
        options = {
          filename: req.body.filename
        };
      }
      //console.log "options", options
      if (options != null) {
        return this.adm.deploy(context, options).then(deploymentInfo => {
          var response;
          this.logger.debug(`${meth} - Processed deploy: ${JSON.stringify(deploymentInfo)}`);
          response = {
            success: true,
            message: 'SUCCESSFULLY PROCESSED DEPLOYMENT: ' + `${deploymentInfo.deploymentURN}`,
            data: deploymentInfo
          };
          return res.send(response);
        }).fail(error => {
          var response;
          this.logger.error(`ERROR: ${error.message}`, error.stack);
          response = {
            success: false,
            message: `ERROR PROCESSING DEPLOYMENT: ${error.message}`,
            error: error.stack
          };
          return res.send(response);
        }).finally(() => {
          // Cleanup uploaded files from temporary directory
          return cleanupFiles.call(this, filesToCleanup);
        });
      } else {
        this.logger.error('ERROR: Missing mandatory parameter (URL or Filename)');
        response = {
          success: false,
          message: 'ERROR PROCESSING DEPLOYMENT: Missing mandatory parameter (URL or Filename)'
        };
        res.send(response);
        // Cleanup uploaded files from temporary directory
        return cleanupFiles.call(this, filesToCleanup);
      }
    }

    undeploy(req, res) {
      var context, errMsg, force, meth, owner, ref, response, urn;
      boundMethodCheck(this, AdmissionRestAPI);
      meth = 'AdmissionRestAPI.undeploy';
      this.apiRequestCount++;
      context = this.setupContext(req);
      this.logger.info(`${meth} - User: ${context.user.id || context.user.name || ''}`);
      owner = context != null ? (ref = context.user) != null ? ref.id : void 0 : void 0;
      urn = getFromReq(req, 'urn');
      force = getFromReq(req, 'force');
      force = force != null && force === 'yes';
      this.logger.info(`${meth} URN: ${urn} (force: ${force})`);
      // @logger.debug "#{meth} REQ: #{JSON.stringify req.headers, null, 2}"
      this.logger.debug(`${meth} QUERY: ${JSON.stringify(req.query)}`);
      if (!urn) {
        errMsg = 'Missing mandatory parameter (deployment URN)';
        this.logger.error(`ERROR: ${errMsg}`);
        response = {
          success: false,
          message: errMsg
        };
        res.send(response);
        return q();
      } else {
        // Check permissions for writing (deleting)
        return this.authorization.isAllowedForURN(urn, context, true).then(isAllowed => {
          if (!isAllowed) {
            return q.reject(new Error(`Unauthorized request to ${urn}`));
          } else {
            this.logger.info(`${meth} - User is allowed to access element ${urn}`);
            return true;
          }
        }).then(() => {
          if (this.config.restTimeout !== -1) {
            res.setTimeout(this.config.restTimeout, () => {
              res.send({
                success: false,
                message: 'ADMISSION UNDEPLOY TIMEOUT'
              });
              return res.send = m => {
                return this.logger.warn(`HTTP Response Already Sent ${JSON.stringify(m)}`);
              };
            });
          }
          return this.adm.undeploy(context, {
            deploymentURN: urn,
            force: force
          });
        }).then(undeploymentInfo => {
          this.logger.debug(`${meth} - Processed undeploy: ${undeploymentInfo}`);
          response = {
            success: true,
            message: `SUCCESSFULLY PROCESSED UNDEPLOYMENT: ${urn}`,
            data: undeploymentInfo
          };
          return res.send(response);
        }).fail(error => {
          this.logger.error('ERROR:', error.stack || error);
          response = {
            success: false,
            message: `ERROR PROCESSING UNDEPLOYMENT: ${error.message}`
          };
          return res.send(response);
        });
      }
    }

    restartSolutionInstances(req, res) {
      var context, errMsg, force, instance, meth, owner, ref, response, role, urn;
      boundMethodCheck(this, AdmissionRestAPI);
      meth = 'AdmissionRestAPI.restartSolutionInstances';
      this.apiRequestCount++;
      context = this.setupContext(req);
      this.logger.info(`${meth} - User: ${context.user.id || context.user.name || ''}`);
      owner = context != null ? (ref = context.user) != null ? ref.id : void 0 : void 0;
      urn = getFromReq(req, 'urn');
      role = getFromReq(req, 'role');
      instance = getFromReq(req, 'instance');
      force = force != null && force === 'yes';
      this.logger.info(`${meth} - URN: ${urn}  ROLE: ${role}  INSTANCE: ${instance}`);
      // @logger.debug "#{meth} PARAMS: #{JSON.stringify req.params}"
      // @logger.debug "#{meth} QUERY: #{JSON.stringify req.query}"
      if (urn == null) {
        errMsg = 'Missing mandatory parameter (solution URN)';
        this.logger.error(`ERROR: ${errMsg}`);
        response = {
          success: false,
          message: errMsg
        };
        res.send(response);
        return true;
      }
      if (role == null) {
        errMsg = 'Missing mandatory parameter (solution role)';
        this.logger.error(`ERROR: ${errMsg}`);
        response = {
          success: false,
          message: errMsg
        };
        res.send(response);
        return true;
      }
      // Validate user has permissions to access the requested solution
      this.logger.info(`${meth} - Checking user permissions to access solution ${urn}`);
      return this.authorization.isAllowedForURN(urn, context).then(isAllowed => {
        if (!isAllowed) {
          errMsg = 'User is not allowed to access solution.';
          this.logger.debug(`${meth} - ${errMsg}`);
          throw new Error(errMsg);
        }
        this.logger.info(`${meth} - User is allowed to access element ${urn}`);
        return true;
      }).then(() => {
        // Convert solution+role+instance into deployment+role+instance
        return this.adm.solutionElementToDeploymentElement(urn, role, instance, context);
      }).then(deploymentInstanceData => {
        // Perform restart in Admission
        return this.adm.restartInstances(context, deploymentInstanceData);
      }).then(restartInfo => {
        var element;
        this.logger.debug(`${meth} - Processed restart: ${restartInfo}`);
        element = instance != null ? instance : {
          [`${role}/${instance}`]: role
        };
        response = {
          success: true,
          message: `SUCCESSFULLY PROCESSED RESTART: ${urn}/${element}`,
          data: null
        };
        res.send(response);
        return true;
      }).catch(err => {
        errMsg = `Error while processing restart: ${err.message}`;
        this.logger.error(`${meth} - ERROR: ${errMsg}`);
        response = {
          success: false,
          message: errMsg
        };
        res.send(response);
        return true;
      });
    }

    restartInstances(req, res) {
      var context, errMsg, force, instance, meth, owner, ref, response, role, urn;
      boundMethodCheck(this, AdmissionRestAPI);
      meth = 'AdmissionRestAPI.restartInstances';
      this.apiRequestCount++;
      context = this.setupContext(req);
      this.logger.info(`${meth} - User: ${context.user.id || context.user.name || ''}`);
      owner = context != null ? (ref = context.user) != null ? ref.id : void 0 : void 0;
      urn = getFromReq(req, 'urn');
      role = getFromReq(req, 'role');
      instance = getFromReq(req, 'instance');
      force = force != null && force === 'yes';
      this.logger.info(`${meth} URN: ${urn}  ROLE: ${role}  INSTANCE: ${instance}`);
      // @logger.debug "#{meth} REQ: #{JSON.stringify req.headers, null, 2}"
      this.logger.debug(`${meth} PARAMS: ${JSON.stringify(req.params)}`);
      this.logger.debug(`${meth} QUERY: ${JSON.stringify(req.query)}`);
      if (!urn) {
        errMsg = 'Missing mandatory parameter (deployment URN)';
        this.logger.error(`ERROR: ${errMsg}`);
        response = {
          success: false,
          message: errMsg
        };
        res.send(response);
        return q();
      } else if (!role) {
        errMsg = 'Missing mandatory parameter (deployment role)';
        this.logger.error(`ERROR: ${errMsg}`);
        response = {
          success: false,
          message: errMsg
        };
        res.send(response);
        return q();
      } else {
        return this.authorization.isAllowedForURN(urn, context).then(isAllowed => {
          if (!isAllowed) {
            return q.reject(new Error(`Unauthorized request to ${urn}`));
          } else {
            this.logger.info(`${meth} - User is allowed to access element ${urn}`);
            return true;
          }
        }).then(() => {
          // If an instance was provided, check if user has permissions
          if (instance != null) {
            return this.authorization.isAllowed('instance', instance, context).then(allowed => {
              if (!allowed) {
                throw new Error('User is not allowed to access instance.');
              } else {
                return true;
              }
            });
          }
        }).then(() => {
          // Perform restart in Admission
          return this.adm.restartInstances(context, {
            deploymentURN: urn,
            role: role,
            instance: instance
          });
        }).then(restartInfo => {
          var element;
          this.logger.debug(`${meth} - Processed restart: ${restartInfo}`);
          element = instance != null ? instance : {
            [`${role}/${instance}`]: role
          };
          response = {
            success: true,
            message: `SUCCESSFULLY PROCESSED RESTART: ${urn}/${element}`,
            data: null
          };
          return res.send(response);
        }).fail(error => {
          this.logger.error('ERROR:', error.stack || error);
          response = {
            success: false,
            message: `ERROR PROCESSING RESTART: ${error.message}`
          };
          return res.send(response);
        });
      }
    }

    unregister(req, res) {
      var context, elementType, manifestName, meth, owner, ref, urn;
      boundMethodCheck(this, AdmissionRestAPI);
      meth = 'AdmissionRestAPI.unregister';
      this.apiRequestCount++;
      context = this.setupContext(req);
      this.logger.info(`${meth} - User: ${context.user.id || context.user.name || ''}`);
      owner = context != null ? (ref = context.user) != null ? ref.id : void 0 : void 0;
      urn = getFromReq(req, 'urn');
      this.logger.info(`${meth} URN: ${urn} - OWNER: ${owner}`);
      // @logger.debug "REQ: #{JSON.stringify req.headers, null, 2}"
      this.logger.debug(`QUERY: ${JSON.stringify(req.query)}`);
      this.logger.debug(`PARAMS: ${JSON.stringify(req.params)}`);
      manifestName = null;
      elementType = null;
      // Check user permissions for writing (deleting)
      return this.authorization.isAllowedForURN(urn, context, true).then(isAllowed => {
        if (!isAllowed) {
          return q.reject(new Error('Unauthorized'));
        } else {
          return this.logger.info(`${meth} - User is allowed to access element ${urn}`);
        }
      }).then(() => {
        this.logger.info(`${meth} Getting manifest for URN ${urn}`);
        console.log("GETTING MANIFEST");
        return this.manifestRepository.getManifest(urn);
      }).then(manifest => {
        var ALLOWED_TYPES, elementName, errMsg;
        this.logger.info(`${meth} Manifest (URN ${urn}) : ${JSON.stringify(manifest)}`);
        ALLOWED_TYPES = [ManifestHelper.COMPONENT, ManifestHelper.SERVICE, ManifestHelper.RESOURCE];
        elementName = manifest.name;
        elementType = this.manifestHelper.getManifestType(manifest);
        this.logger.info(`${meth} ElementType: ${elementType} - URN: ${urn}`);
        if (indexOf.call(ALLOWED_TYPES, elementType) < 0) {
          errMsg = `Error unregistering ${urn}: Unregister operation not supported for element type.`;
          this.logger.error(`${meth} - ${errMsg}`);
          throw new Error(errMsg);
        } else {
          return this.adm.canBeDeleted(urn);
        }
      }).then(canBeDeleted => {
        this.logger.info(`${meth} Checking if element can be deleted - URN: ${urn}`);
        if (!canBeDeleted) {
          return q.reject(new Error(`Component ${urn} is in use.`));
        } else {
          return this.manifestRepository.deleteManifest(urn);
        }
      }).then(() => {
        var response;
        response = {
          success: true,
          message: 'Unregistered succesfully'
        };
        return res.send(response);
      }).fail(error => {
        var message, response;
        this.logger.error('Error unregistering:', error, error.stack);
        message = error.message || error;
        if (message.indexOf('Error code 23') > 0) {
          message = `Error reading element ${urn} manifest. Check the URN.`;
        }
        response = {
          success: false,
          message: message
        };
        return res.send(response);
      });
    }

    readManifest(req, res) {
      var context, data, meth, urn;
      boundMethodCheck(this, AdmissionRestAPI);
      meth = 'AdmissionRestAPI.readManifest';
      this.apiRequestCount++;
      context = this.setupContext(req);
      this.logger.info(`${meth} - User: ${context.user.id || context.user.name || ''}`);
      urn = getFromReq(req, 'urn');
      // @logger.debug "REQ: #{JSON.stringify req.headers, null, 2}"
      this.logger.debug(`QUERY: ${JSON.stringify(req.query)}`);
      this.logger.debug(`PARAMS: ${JSON.stringify(req.params)}`);
      this.logger.debug(`${meth} - URN: ${urn}`);
      data = null;
      return q().then(() => {
        // Check if user has permissions to access manifest
        return this.authorization.isAllowedForURN(urn, context);
      }).then(isAllowed => {
        var errMsg;
        if (!isAllowed) {
          errMsg = "User is not allowed to access element.";
          this.logger.warn(`${meth} ERROR: ${errMsg}`);
          return q.reject(new Error(errMsg));
        } else {
          return this.manifestRepository.getManifest(urn);
        }
      }).then(function (manifest) {
        var response;
        response = {
          success: true,
          message: 'Manifest obtained',
          data: manifest
        };
        return res.send(response);
      }).fail(error => {
        var response;
        this.logger.error(`Error accessing ${urn} - ${error.message}`);
        response = {
          success: false,
          message: error.message
        };
        return res.send(response);
      });
    }

    listLinks(req, res) {
      var context, meth;
      boundMethodCheck(this, AdmissionRestAPI);
      meth = 'AdmissionRestAPI.listLinks';
      this.apiRequestCount++;
      context = this.setupContext(req);
      this.logger.info(`${meth} - User: ${context.user.id || context.user.name || ''}`);
      return this.manifestRepository.listElementType('kukulinks', context.owner, null).then(elements => {
        var response;
        response = {
          success: true,
          message: 'Successfully got list of links.',
          data: elements
        };
        return res.send(response);
      }).fail(error => {
        var response;
        this.logger.error(`Error getting list of links: ${error.message}`);
        response = {
          success: false,
          message: error.message
        };
        return res.send(response);
      });
    }

    linkServices(req, res) {
      var context, endpoints, filesToCleanup, meth;
      boundMethodCheck(this, AdmissionRestAPI);
      meth = 'AdmissionRestAPI.linkServices';
      this.apiRequestCount++;
      context = this.setupContext(req);
      this.logger.info(`${meth} - User: ${context.user.id || context.user.name || ''}`);
      filesToCleanup = [];
      endpoints = null;
      return q().then(() => {
        var file, ref, ref1;
        if (((ref = req.files) != null ? (ref1 = ref[0]) != null ? ref1.fieldname : void 0 : void 0) === 'linkManifest') {
          file = req.files[0];
          filesToCleanup.push(file.path);
          this.logger.info(`${meth} file = ${file.originalname}`);
          this.logger.debug(`${meth} request = ${JSON.stringify(req.headers, null, 2)}`);
          return loadFile(file.path);
        } else {
          return q.reject(new Error('Link a service requires a file'));
        }
      }).then(linkManifest => {
        if (linkManifest.endpoints == null) {
          this.logger.error(`${meth} Missing endpoints section in manifest.`);
          return q.reject(new Error('Missing endpoints section in manifest.'));
        } else if (linkManifest.endpoints.length !== 2) {
          this.logger.error(`${meth} Wrong number of endpoints: ${linkManifest.endpoints.length}`);
          return q.reject(new Error(`Wrong number of endpoints: ${linkManifest.endpoints.length}`));
        } else {
          endpoints = linkManifest.endpoints;
          return this.adm.linkServices(context, linkManifest);
        }
      }).then(function (info) {
        var response;
        response = {
          success: true,
          message: `SUCCESSFULLY LINKED SERVICES: ${JSON.stringify(endpoints)}`,
          data: info
        };
        return res.json(response);
      }).fail(error => {
        var response;
        this.logger.error('ERROR:', error.stack || error);
        response = {
          success: false,
          message: `ERROR LINKING SERVICES: ${error.message}`
        };
        return res.send(response);
      }).finally(() => {
        // Cleanup uploaded files from temporary directory
        return cleanupFiles.call(this, filesToCleanup);
      });
    }

    unlinkServices(req, res) {
      var context, endpoints, file, filesToCleanup, meth, ref, ref1, ref2, urn;
      boundMethodCheck(this, AdmissionRestAPI);
      meth = 'AdmissionRestAPI.unlinkServices';
      this.apiRequestCount++;
      context = this.setupContext(req);
      this.logger.info(`${meth} - User: ${context.user.id || context.user.name || ''}`);
      urn = getFromReq(req, 'urn');
      filesToCleanup = [];
      endpoints = null;
      // If a link name is provided, it takes precedence over file upload
      return (urn != null ? (this.logger.debug(`${meth} - Link URN: ${urn}`), this.adm.unlinkServicesFromURN(context, urn)) : (((ref = req.files) != null ? (ref1 = ref[0]) != null ? ref1.fieldname : void 0 : void 0) === 'linkManifest' ? (file = req.files[0], filesToCleanup.push(file.path), this.logger.info(`${meth} file = ${file.originalname}`), this.logger.debug(`${meth} request = ${JSON.stringify(req.headers, null, 2)}`), loadFile(file.path)) : (req != null ? (ref2 = req.query) != null ? ref2.linkManifest : void 0 : void 0) != null ? q(JSON.parse(req.query.linkManifest)) : q.reject(new Error('Missing parameter linkManifest'))).then(linkManifest => {
        if (linkManifest.endpoints == null) {
          this.logger.error(`${meth} Missing endpoints section in manifest.`);
          return q.reject(new Error('Missing endpoints section in manifest.'));
        } else if (linkManifest.endpoints.length !== 2) {
          this.logger.error(`${meth} Wrong number of endpoints: ${linkManifest.endpoints.length}`);
          return q.reject(new Error(`Wrong number of endpoints: ${linkManifest.endpoints.length}`));
        } else {
          endpoints = linkManifest.endpoints;
          return this.adm.unlinkServices(context, linkManifest);
        }
      })).then(function (info) {
        var response;
        response = {
          success: true,
          message: `SUCCESSFULLY UNLINKED SERVICES: ${urn || JSON.stringify(endpoints)}`,
          data: info
        };
        return res.json(response);
      }).fail(error => {
        var response;
        this.logger.error(`${meth} ERROR: ${error.stack || error}`);
        response = {
          success: false,
          message: `ERROR UNLINKING SERVICES: ${error.message}`
        };
        return res.send(response);
      }).finally(() => {
        // Cleanup uploaded files from temporary directory
        return cleanupFiles.call(this, filesToCleanup);
      });
    }

    registerBundle(req, res, performDeployments = false) {
      var bundlesFile, context, deploymentNameToURN, file, filesToCleanup, fullResults, meth, ref, ref1, ref2, ref3;
      boundMethodCheck(this, AdmissionRestAPI);
      meth = 'AdmissionRestAPI.registerBundle()';
      this.apiRequestCount++;
      context = this.setupContext(req);
      filesToCleanup = [];
      this.logger.info(`${meth} - User: ${context.user.id || context.user.name || ''}`);
      if (this.config.restTimeout !== -1) {
        res.setTimeout(this.config.restTimeout, () => {
          res.send({
            success: false,
            message: 'ADMISSION REGISTER BUNDLE TIMEOUT'
          });
          return res.send = m => {
            return this.logger.warn(`${meth} HTTP Response Already Sent ${JSON.stringify(m)}`);
          };
        });
      }
      // @logger.debug "#{meth} - REQ: #{JSON.stringify req.headers, null, 2}"
      this.logger.debug(`${meth} - REQ FILES: ${JSON.stringify(req.files, null, 2)}`);
      bundlesFile = null;
      fullResults = null;
      deploymentNameToURN = {};
      return (((ref = req.files) != null ? (ref1 = ref[0]) != null ? ref1.fieldname : void 0 : void 0) === 'bundlesJson' ? (file = req.files[0], this.logger.debug(`File ${file.originalname} uploaded.`), bundlesFile = file.originalname, filesToCleanup.push(file.path), this.bundleRegistry.registerBundleJson(context, file.path)) : ((ref2 = req.files) != null ? (ref3 = ref2[0]) != null ? ref3.fieldname : void 0 : void 0) === 'bundlesZip' ? (file = req.files[0], this.logger.debug(`File ${file.originalname} uploaded.`), bundlesFile = file.originalname, filesToCleanup.push(file.path), this.bundleRegistry.registerBundleZip(context, file.path)) : q.reject(new Error('Bundle registration requires a JSON or zip file.'))).then(result => {
        var i, len, ref4, ref5, ref6, solManifest, solutionChain;
        fullResults = result;
        // Handle deferred Solution manifests
        solutionChain = q();
        if (((ref4 = fullResults.pendingActions) != null ? (ref5 = ref4.solutions) != null ? ref5.length : void 0 : void 0) > 0) {
          this.logger.debug('Processing pending solutions from bundle...');
          fullResults.solutions = {
            errors: [],
            successful: []
          };
          ref6 = fullResults.pendingActions.solutions;
          for (i = 0, len = ref6.length; i < len; i++) {
            solManifest = ref6[i];
            (solManifest => {
              return solutionChain = solutionChain.then(() => {
                // Pass the Solution manifest to Admission
                return this.adm.registerSolution(context, {}, solManifest).then(solutionInfo => {
                  this.logger.info(`Solution creation finished: ${solutionInfo.solutionURN}`);
                  return fullResults.solutions.successful.push(solutionInfo);
                }).fail(err => {
                  var ref7, ref8;
                  this.logger.error(`Solution creation error: ${(ref7 = err.stack) != null ? ref7 : err}`);
                  fullResults.solutions.errors.push({
                    message: `ERROR IN SOLUTION: ${(ref8 = err.message) != null ? ref8 : err}`,
                    error: err.stack
                  });
                  return q();
                });
              });
            })(solManifest);
          }
        }
        return solutionChain;
      }).then(() => {
        var depManifest, deploymentChain, i, len, ref4, ref5, ref6;
        // Handle deferred Deployment manifests
        deploymentChain = q();
        if (((ref4 = fullResults.pendingActions) != null ? (ref5 = ref4.deployments) != null ? ref5.length : void 0 : void 0) > 0) {
          this.logger.debug('Processing pending deployments from bundle...');
          fullResults.deployments = {
            errors: [],
            successful: []
          };
          ref6 = fullResults.pendingActions.deployments;
          for (i = 0, len = ref6.length; i < len; i++) {
            depManifest = ref6[i];
            (depManifest => {
              var action, name;
              action = null;
              if (!this.manifestHelper.isKmv3Deployment(depManifest)) {
                name = depManifest.name;
                if (depManifest.nickname == null) {
                  depManifest.nickname = name;
                }
                delete depManifest.name;
              }
              if (performDeployments) {
                return deploymentChain = deploymentChain.then(() => {
                  return this.adm.deploy(context, {
                    simulation: false
                  }, depManifest).then(deploymentInfo => {
                    this.logger.info(`Deployment finished: ${deploymentInfo.deploymentURN}`);
                    if (name == null) {
                      name = deploymentInfo.deploymentURN;
                    }
                    deploymentNameToURN[name] = deploymentInfo.deploymentURN;
                    return fullResults.deployments.successful.push(deploymentInfo);
                  }).fail(err => {
                    var ref7, ref8;
                    this.logger.error(`Deployment error: ${(ref7 = err.stack) != null ? ref7 : err}`);
                    fullResults.deployments.errors.push({
                      message: `ERROR IN DEPLOYMENT: ${(ref8 = err.message) != null ? ref8 : err}`,
                      error: err.stack
                    });
                    return q();
                  });
                });
              } else {
                action = this.adm.setupDeploy;
                return deploymentChain = deploymentChain.then(() => {
                  return this.adm.setupDeploy(context, {
                    simulation: false
                  }, depManifest).then(deploymentInfo => {
                    this.logger.info(`Deployment finished: ${deploymentInfo.name}`);
                    if (name == null) {
                      name = deploymentInfo.deploymentURN;
                    }
                    deploymentNameToURN[name] = deploymentInfo.name;
                    return fullResults.deployments.successful.push(deploymentInfo);
                  }).fail(err => {
                    var ref7, ref8;
                    this.logger.error(`Deployment error: ${(ref7 = err != null ? err.stack : void 0) != null ? ref7 : err}`);
                    fullResults.deployments.errors.push({
                      message: `ERROR IN DEPLOYMENT: ${(ref8 = err.message) != null ? ref8 : err}`,
                      error: err.stack
                    });
                    return q();
                  });
                });
              }
            })(depManifest);
          }
        }
        return deploymentChain;
      }).then(() => {
        var i, len, linkErrors, linkManifest, linkPromises, ref4, ref5, ref6;
        // Process pending LinkService manifests
        linkPromises = [];
        linkErrors = 0;
        if (((ref4 = fullResults.pendingActions) != null ? (ref5 = ref4.links) != null ? ref5.length : void 0 : void 0) > 0) {
          this.logger.debug('Processing pending links from bundle...');
          fullResults.links = {
            errors: [],
            successful: []
          };
          ref6 = fullResults.pendingActions.links;
          for (i = 0, len = ref6.length; i < len; i++) {
            linkManifest = ref6[i];
            (linkManifest => {
              var depName1, depName2, errorMsg, ref10, ref7, ref8, ref9, result;
              // Replace local deployment name with actual deployment URN
              if (((ref7 = linkManifest.endpoints) != null ? (ref8 = ref7[0]) != null ? ref8.deployment : void 0 : void 0) == null) {
                errorMsg = 'Link manifest is missing endpoint[0] deployment.';
                this.logger.warn(errorMsg);
                result = {
                  error: errorMsg,
                  manifest: linkManifest
                };
                fullResults.links.errors.push(result);
                return q();
              } else if (((ref9 = linkManifest.endpoints) != null ? (ref10 = ref9[1]) != null ? ref10.deployment : void 0 : void 0) == null) {
                errorMsg = 'Link manifest is missing endpoint[1] deployment.';
                this.logger.warn(errorMsg);
                result = {
                  error: errorMsg,
                  manifest: linkManifest
                };
                fullResults.links.errors.push(result);
                return q();
              } else {
                // If endpoint deployments are local names (deployments in the same
                // bundle), then replace local names with deployment URNs.
                depName1 = linkManifest.endpoints[0].deployment;
                depName2 = linkManifest.endpoints[1].deployment;
                if (depName1 in deploymentNameToURN) {
                  linkManifest.endpoints[0].deployment = deploymentNameToURN[depName1];
                }
                if (depName2 in deploymentNameToURN) {
                  linkManifest.endpoints[1].deployment = deploymentNameToURN[depName2];
                }
                this.logger.debug(`Linking: ${JSON.stringify(linkManifest)}`);
                return linkPromises.push(this.adm.linkServices(context, linkManifest).then(linkResult => {
                  this.logger.info(`Link succeeded: ${JSON.stringify(linkManifest.endpoints)}`);
                  return fullResults.links.successful.push(linkManifest);
                }).fail(err => {
                  var ref11;
                  linkErrors++;
                  this.logger.error(`Service link error: ${(ref11 = err.stack) != null ? ref11 : err}`);
                  fullResults.links.errors.push({
                    message: `ERROR IN SERVICE LINK: ${err.message}`,
                    error: err.stack
                  });
                  return q();
                }));
              }
            })(linkManifest);
          }
        }
        return q.all(linkPromises);
      }).then(() => {
        this.logger.info("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
        this.logger.info(`${meth} - FINISHED`);
        return this.logger.info("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
      }).then(() => {
        var ref10, ref11, ref12, ref13, ref14, ref15, ref4, ref5, ref6, ref7, ref8, ref9, response;
        this.logger.info(`${meth} - Sending response`);
        response = {
          success: null,
          message: null,
          data: {}
        };
        if (fullResults.registered.length > 0 || ((ref4 = fullResults.solutions) != null ? (ref5 = ref4.successful) != null ? ref5.length : void 0 : void 0) > 0 || ((ref6 = fullResults.deployments) != null ? (ref7 = ref6.successful) != null ? ref7.length : void 0 : void 0) > 0 || ((ref8 = fullResults.links) != null ? (ref9 = ref8.successful) != null ? ref9.length : void 0 : void 0) > 0) {
          response.success = true;
        } else {
          response.success = false;
        }
        if (fullResults.errors.length > 0 || ((ref10 = fullResults.deployments) != null ? (ref11 = ref10.errors) != null ? ref11.length : void 0 : void 0) > 0 || ((ref12 = fullResults.solutions) != null ? (ref13 = ref12.errors) != null ? ref13.length : void 0 : void 0) > 0 || ((ref14 = fullResults.links) != null ? (ref15 = ref14.errors) != null ? ref15.length : void 0 : void 0) > 0) {
          response.message = 'Bundle registration found some errors.';
        } else {
          response.message = 'Bundle registration finished with no errors.';
        }
        response.data = {
          successful: fullResults.registered,
          errors: fullResults.errors
        };
        if (fullResults.solutions != null) {
          response.data.solutions = fullResults.solutions;
        }
        if (fullResults.deployments != null) {
          response.data.deployments = fullResults.deployments;
        }
        if (fullResults.links != null) {
          response.data.links = fullResults.links;
        }
        return res.send(response);
      }).fail(error => {
        var response;
        this.logger.error(`Error registering ${bundlesFile}`, error, error.stack);
        response = {
          success: false,
          message: error.message
        };
        return res.send(response);
      }).fail(error => {
        return this.logger.error(`Error registering ${bundlesFile}:`, error.stack);
      }).finally(() => {
        // Cleanup uploaded files from temporary directory
        return cleanupFiles.call(this, filesToCleanup);
      });
    }

    registerResource(req, res) {
      var context, endpoints, filesToCleanup, meth;
      boundMethodCheck(this, AdmissionRestAPI);
      meth = 'AdmissionRestAPI.registerResource';
      this.apiRequestCount++;
      context = this.setupContext(req);
      this.logger.info(`${meth} - User: ${context.user.id || context.user.name || ''}`);
      filesToCleanup = [];
      endpoints = null;
      return q().then(() => {
        var file, ref, ref1;
        if (((ref = req.files) != null ? (ref1 = ref[0]) != null ? ref1.fieldname : void 0 : void 0) === 'resourceManifest') {
          file = req.files[0];
          filesToCleanup.push(file.path);
          this.logger.info(`${meth} file = ${file.originalname}`);
          this.logger.debug(`${meth} request = ${JSON.stringify(req.headers, null, 2)}`);
          return loadFile(file.path);
        } else {
          return q.reject(new Error('Resource registration requires a file.'));
        }
      }).then(resourceManifest => {
        var errMsg;
        if (this.manifestHelper.isResource(resourceManifest)) {
          return this.adm.registerResource(context, resourceManifest);
        } else {
          errMsg = "File doesn't contain a valid resource definition.";
          this.logger.error(`${meth} ${errMsg}.`);
          return q.reject(new Error(errMsg));
        }
      }).then(function (info) {
        var response;
        response = {
          success: true,
          message: "SUCCESSFULLY REGISTERED RESOURCE.",
          data: info
        };
        return res.json(response);
      }).fail(error => {
        var response;
        this.logger.error('ERROR:', error.stack || error);
        response = {
          success: false,
          message: `ERROR REGISTERING RESOURCE: ${error.message}`
        };
        return res.send(response);
      }).finally(() => {
        // Cleanup uploaded files from temporary directory
        return cleanupFiles.call(this, filesToCleanup);
      });
    }

    deleteResource(req, res) {
      var context, elementManifest, errMsg, meth, opID, response, urn;
      boundMethodCheck(this, AdmissionRestAPI);
      meth = 'AdmissionRestAPI.deleteResource()';
      context = this.setupContext(req);
      this.logger.info(`${meth} - User: ${context.user.id || context.user.name || ''}`);
      // Compose the filter from the parameters
      urn = getFromReq(req, 'urn');
      this.logger.info(`${meth} - URN: ${urn}`);
      if (urn == null) {
        errMsg = 'Missing mandatory parameter (resource URN)';
        this.logger.error(`ERROR: ${errMsg}`);
        response = {
          success: false,
          message: errMsg
        };
        res.send(response);
        return true;
      }
      elementManifest = null;
      // Calculate a unique ID for this operation
      opID = this.adm.getOperationID();
      // Check user permissions on element for writing (deleting)
      return this.authorization.isAllowedForURN(urn, context, true).then(isAllowed => {
        if (!isAllowed) {
          errMsg = "Resource does not exist or user is not allowed to access it.";
          this.logger.warn(`${meth} ERROR: ${errMsg}`);
          return q.reject(new Error(errMsg));
        } else {
          this.logger.info(`${meth} - User is allowed to access resource ${urn}`);
          // Get element manifest
          return this.manifestRepository.getManifest(urn);
        }
      }).then(manifest => {
        var elementType, resKind, resName;
        this.logger.info(`${meth} Got manifest for ${urn}`);
        elementManifest = manifest;
        elementType = this.manifestHelper.getManifestType(manifest);
        // Check it's a resource
        if (elementType !== ManifestHelper.RESOURCE) {
          errMsg = `Wrong element type (${elementType}).`;
          this.logger.error(`${meth} - ${errMsg}`);
          throw new Error(errMsg);
        } else {
          this.logger.info(`${meth} - Element type checked OK - ${urn}`);
          // Keep resource internal name for later
          resName = elementManifest.name;
          resKind = elementManifest.ref.kind;
          this.logger.info(`${meth} Resource ID: ${resName} - TYPE: ${resKind}`);
          // Check resource is not in use
          return this.adm.isResourceInUse(resName);
        }
      }).then(inUse => {
        if (inUse) {
          errMsg = "Resource is in use.";
          this.logger.error(`${meth} ERROR: ${errMsg}`);
          return q.reject(new Error(errMsg));
        } else {
          this.logger.info(`${meth} - Resource is not in use - ${urn}`);
          // Get lock for the element being deleted
          return this.adm.acquireElementLockFromURN(elementManifest.name, opID);
        }
      }).then(() => {
        var resKind, resName;
        // Delete the manifest
        resName = elementManifest.name;
        resKind = elementManifest.ref.kind;
        return this.manifestRepository.deleteManifestByNameAndKind(resName, resKind);
      }).then(() => {
        this.logger.info(`${meth} - Resource deleted - ${urn}`);
        response = {
          success: true,
          message: 'Resource deleted succesfully'
        };
        res.send(response);
        return true;
      }).catch(error => {
        errMsg = `Unable to delete resource ${urn}: ${error.message}`;
        this.logger.error(`${meth} ERROR: ${errMsg} - ${error.stack}`);
        response = {
          success: false,
          message: errMsg
        };
        res.send(response);
        return true;
      }).finally(() => {
        return this.adm.releaseElementLockFromURN(elementManifest.name, opID);
      });
    }

    listResources(req, res) {
      var context, filter, meth, owner;
      boundMethodCheck(this, AdmissionRestAPI);
      meth = 'AdmissionRestAPI.listResources()';
      this.apiRequestCount++;
      context = this.setupContext(req);
      this.logger.info(`${meth} - User: ${context.user.id || context.user.name || ''}`);
      owner = getFromReq(req, 'owner');
      filter = {};
      if (owner != null) {
        filter.owner = owner;
      } else if (!this.authorization.isAdmin(context)) {
        filter.owner = context.user.id;
      }
      return this.adm.listResources(filter).then(function (resources) {
        return res.json({
          success: true,
          message: 'SUCCESSFULLY PROCESSED LIST RESOURCES.',
          data: resources
        });
      }).fail(error => {
        this.logger.error('ERROR:', error.stack || error);
        return res.json({
          success: false,
          message: `ERROR LISTING RESOURCES: ${error.message}`
        });
      });
    }

    listResourcesInUse(req, res) {
      var context, filter, meth, owner, urn;
      boundMethodCheck(this, AdmissionRestAPI);
      meth = 'AdmissionRestAPI.listResourcesInUse()';
      this.apiRequestCount++;
      context = this.setupContext(req);
      this.logger.info(`${meth} - User: ${context.user.id || context.user.name || ''}`);
      // Compose the filter from the parameters
      owner = getFromReq(req, 'owner');
      urn = getFromReq(req, 'urn');
      filter = {};
      if (owner != null) {
        filter.owner = owner;
      } else if (!this.authorization.isAdmin(context)) {
        filter.owner = context.user.id;
      }
      if (urn != null) {
        filter.urn = urn;
      }
      this.logger.info(`${meth} filter = ${JSON.stringify(filter)}`);
      // Check if current user has permission to process the filter
      return this.authorization.canListResources(context, filter).then(() => {
        return this.adm.listResourcesInUse(filter, context);
      }).then(function (resources) {
        return res.json({
          success: true,
          message: 'SUCCESSFULLY PROCESSED LIST RESOURCES.',
          data: resources
        });
      }).fail(error => {
        this.logger.error('ERROR:', error.stack || error);
        return res.json({
          success: false,
          message: `ERROR LISTING RESOURCES: ${error.message}`
        });
      });
    }

    describeResource(req, res) {
      var context, meth, resourceURN;
      boundMethodCheck(this, AdmissionRestAPI);
      meth = 'AdmissionRestAPI.describeResource()';
      this.apiRequestCount++;
      context = this.setupContext(req);
      this.logger.info(`${meth} - User: ${context.user.id || context.user.name || ''}`);
      resourceURN = getFromReq(req, 'urn');
      // Check if current user has permission to view the resource
      return this.authorization.isAllowedForURN(resourceURN, context).then(isAllowed => {
        var errMsg;
        if (!isAllowed) {
          errMsg = "Resource does not exist or user is not allowed to access it.";
          this.logger.warn(`${meth} ERROR: ${errMsg}`);
          return q.reject(new Error(errMsg));
        } else {
          this.logger.info(`${meth} - User is allowed to access resource ${resourceURN}`);
          // Get element description
          return this.adm.describeResource(resourceURN);
        }
      }).then(function (resourceDescription) {
        return res.json({
          success: true,
          message: 'SUCCESSFULLY PROCESSED DESCRIBE RESOURCE.',
          data: resourceDescription
        });
      }).fail(error => {
        this.logger.error('ERROR:', error.stack || error);
        return res.json({
          success: false,
          message: `ERROR DESCRIBING RESOURCE: ${error.message}`
        });
      });
    }

    listElements(req, res) {
      var context, meth, owner;
      boundMethodCheck(this, AdmissionRestAPI);
      this.apiRequestCount++;
      meth = 'AdmissionRestAPI.listElements()';
      this.apiRequestCount++;
      context = this.setupContext(req);
      this.logger.info(`${meth} - User: ${context.user.id || context.user.name || ''}`);
      owner = null;
      if (!this.authorization.isAdmin(context)) {
        owner = context.user.id;
      }
      return this.manifestRepository.list(owner).then(function (info) {
        return res.json({
          success: true,
          message: 'SUCCESSFULLY PROCESSED LIST ELEMENTS.',
          data: info
        });
      }).fail(error => {
        var response;
        this.logger.error('Error listing bundles: ', error, error.stack);
        response = {
          success: false,
          message: error.message
        };
        return res.send(response);
      });
    }

    getClusterInfo(req, res) {
      var context, errMsg, meth, owner, ref, response, results;
      boundMethodCheck(this, AdmissionRestAPI);
      meth = 'AdmissionRestAPI.getClusterInfo';
      context = this.setupContext(req);
      this.logger.info(`${meth} - User: ${context.user.id || context.user.name || ''}`);
      results = [];
      owner = context != null ? (ref = context.user) != null ? ref.id : void 0 : void 0;
      if (!this.authorization.isAdmin(context)) {
        errMsg = `Access denied. Admin role required (user ${owner})`;
        this.logger.error(`${meth} - Error: ${errMsg}`);
        response = {
          success: false,
          message: errMsg
        };
        res.send(response);
        return true;
      }
      // q.reject new Error 'NOT IMPLEMENTED'
      return this.k8sApiStub.topNodes().then(clusterInfo => {
        response = {
          success: true,
          message: "Cluster information retrieved successfully.",
          data: clusterInfo
        };
        res.send(response);
        return true;
      }).catch(err => {
        errMsg = `Couldn't get cluster information: ${err.message})`;
        this.logger.error(`${meth} - Error: ${errMsg}`);
        response = {
          success: false,
          message: errMsg
        };
        res.send(response);
        return true;
      });
    }

    getClusterConfig(req, res) {
      var context, meth;
      boundMethodCheck(this, AdmissionRestAPI);
      meth = 'AdmissionRestAPI.getClusterConfig';
      context = this.setupContext(req);
      this.logger.info(`${meth} - User: ${context.user.id || context.user.name || ''}`);
      return this.readClusterConfig().then(clusterConfiguration => {
        var response;
        response = {
          success: true,
          message: "Cluster configuration retrieved successfully.",
          data: clusterConfiguration
        };
        res.send(response);
        return true;
      }).catch(err => {
        var errMsg, response;
        errMsg = `Couldn't get cluster configuration: ${err.message})`;
        this.logger.error(`${meth} - Error: ${errMsg}`);
        response = {
          success: false,
          message: errMsg
        };
        res.send(response);
        return true;
      });
    }

    readClusterConfig() {
      var meth;
      boundMethodCheck(this, AdmissionRestAPI);
      meth = 'AdmissionRestAPI.readClusterConfig';
      this.logger.debug(`${meth}`);
      return this.k8sApiStub.getConfigMap(KUMORI_NAMESPACE, CLUSTER_CONFIG_CONFIGMAP).then(configMap => {
        var clusterConfigStr, parsedClusterConfiguration, ref;
        this.logger.debug(`${meth} - Found cluster configuration.`);
        // Extract policy from 'cluster-config.yaml' property
        if ((configMap != null ? (ref = configMap.data) != null ? ref['cluster-config.yaml'] : void 0 : void 0) != null) {
          clusterConfigStr = configMap.data['cluster-config.yaml'];
          this.logger.debug(`${meth} - Configuration (string): ${clusterConfigStr}`);
          parsedClusterConfiguration = utils.yamlParse(clusterConfigStr);
          this.updateClusterConfiguration(parsedClusterConfiguration);
          return parsedClusterConfiguration;
        } else {
          throw new Error('Cluster configuration not found.');
        }
      }).catch(err => {
        throw new Error(err.message);
      });
    }

    updateClusterConfiguration(newClusterConfiguration) {
      var meth;
      meth = 'AdmissionRestAPI.updateClusterConfiguration';
      this.logger.debug(`${meth}`);
      this.latestClusterConfiguration = newClusterConfiguration;
      return this.adm.setClusterConfiguration(newClusterConfiguration);
    }

    getSolutionInstanceLogs(req, res) {
      var container, context, errMsg, follow, instanceId, logOptions, meth, previous, response, role, sinceSeconds, tailLines, urn;
      boundMethodCheck(this, AdmissionRestAPI);
      meth = 'AdmissionRestAPI.getSolutionInstanceLogs';
      context = this.setupContext(req);
      this.logger.info(`${meth} - User: ${context.user.id || context.user.name || ''}`);
      // Handlers to log client disconnections (just for information)
      req.on("close", () => {
        return this.logger.info(`${meth} - Http connection closed by the client.`);
      });
      req.on("end", () => {
        return this.logger.info(`${meth} - Http connection ended.`);
      });
      // Retrieve request parameters
      urn = getFromReq(req, 'urn');
      role = getFromReq(req, 'role');
      instanceId = getFromReq(req, 'instance');
      container = getFromReq(req, 'container');
      follow = getFromReq(req, 'follow') === 'yes';
      previous = getFromReq(req, 'previous') === 'yes';
      tailLines = getFromReq(req, 'tail');
      sinceSeconds = getFromReq(req, 'since');
      this.logger.info(`${meth} - Parameters: URN: ${urn} - Role: ${role} - instanceId=${instanceId} - container: ${container} - follow: ${follow} - tailLines: ${tailLines} - sinceSeconds: ${sinceSeconds} - previous: ${previous}`);
      // If follow mode is on, don't let the connection timeout
      if (follow) {
        req.setTimeout(30 * 24 * 60 * 60 * 1000); // 30 days
      }

      // Prepare options to pass to log retriever
      // As a WritableStream we pass the HTTP response object
      logOptions = {
        follow: follow,
        previous: previous,
        tailLines: tailLines,
        sinceSeconds: sinceSeconds
      };
      // Validate mandatory parameters are present
      if (urn == null) {
        errMsg = 'Missing mandatory parameter (solution URN)';
        this.logger.error(`ERROR: ${errMsg}`);
        response = {
          success: false,
          message: errMsg
        };
        res.send(response);
        return true;
      }
      if (role == null) {
        errMsg = 'Missing mandatory parameter (solution role)';
        this.logger.error(`ERROR: ${errMsg}`);
        response = {
          success: false,
          message: errMsg
        };
        res.send(response);
        return true;
      }
      if (instanceId == null) {
        errMsg = 'Missing mandatory parameter (instance ID)';
        this.logger.error(`ERROR: ${errMsg}`);
        response = {
          success: false,
          message: errMsg
        };
        res.send(response);
        return true;
      }
      // Validate user has permissions to access the requested solution
      this.logger.info(`${meth} - Checking user permissions to access solution ${urn}`);
      return this.authorization.isAllowedForURN(urn, context).then(isAllowed => {
        if (!isAllowed) {
          errMsg = 'User is not allowed to access solution.';
          this.logger.debug(`${meth} - ${errMsg}`);
          throw new Error(errMsg);
        }
        this.logger.info(`${meth} - User is allowed to access element ${urn}`);
        // Convert solution+role+instance into deployment+role+instance
        return this.adm.solutionElementToDeploymentElement(urn, role, instanceId, context);
      }).then(deploymentInstanceData => {
        var realContainerName, realInstanceId;
        realInstanceId = deploymentInstanceData.instanceName;
        realContainerName = this.manifestHelper.hashContainerName(container);
        res.writeHead(200, {
          'Content-Type': 'text/plain; charset=utf-8'
        });
        return this.k8sApiStub.getPodLogs(null, realInstanceId, realContainerName, logOptions, res).catch(err => {
          var ref, ref1;
          if ((ref = err.message) != null ? ref.includes("previous terminated container") : void 0) {
            errMsg = 'Previous container not found.';
          } else if ((ref1 = err.message) != null ? ref1.includes("is not valid for pod") : void 0) {
            errMsg = 'Container not valid for instance.';
          } else {
            errMsg = "Unable to get instance logs.";
            this.logger.error(`ERROR: Unexpected error: ${err.message}`);
          }
          this.logger.error(`ERROR: ${errMsg}`);
          response = {
            success: false,
            message: errMsg
          };
          res.write(JSON.stringify(response));
          res.end();
          return true;
        });
      }).then(status => {
        this.logger.info(`${meth} - Finished log retrieval finished. (${status})`);
        return true;
      }).catch(err => {
        errMsg = `Error while tailing logs: ${err}`;
        this.logger.error(`ERROR: ${errMsg}`);
        response = {
          success: false,
          message: errMsg
        };
        res.send(response);
        return true;
      });
    }

    execSolutionInstanceCommandGet(req, res) {
      var columns, command, container, context, errMsg, instanceId, meth, outColumns, outRows, realInstanceId, response, role, rows, tty, urn;
      boundMethodCheck(this, AdmissionRestAPI);
      meth = 'AdmissionRestAPI.execSolutionInstanceCommandGet';
      context = this.setupContext(req);
      this.logger.info(`${meth} - User: ${context.user.id || context.user.name || ''}`);
      // Retrieve request parameters
      urn = getFromReq(req, 'urn');
      role = getFromReq(req, 'role');
      instanceId = getFromReq(req, 'instance');
      container = getFromReq(req, 'container');
      command = getFromReq(req, 'command');
      tty = getFromReq(req, 'tty') === 'yes';
      outRows = getFromReq(req, 'rows');
      outColumns = getFromReq(req, 'columns');
      this.logger.info(`${meth} - Parameters: URN: ${urn} - Role: ${role} - instanceId=${instanceId} - container: ${container} - command: ${JSON.stringify(command)} - tty: ${tty} - outRows: ${outRows} - outColumns: ${outColumns}`);
      rows = parseInt(outRows);
      columns = parseInt(outColumns);
      // Validate mandatory parameters are present
      if (urn == null) {
        errMsg = 'Missing mandatory parameter (solution URN)';
        this.logger.error(`ERROR: ${errMsg}`);
        response = {
          success: false,
          message: errMsg
        };
        res.send(response);
        return true;
      }
      if (role == null) {
        errMsg = 'Missing mandatory parameter (solution role)';
        this.logger.error(`ERROR: ${errMsg}`);
        response = {
          success: false,
          message: errMsg
        };
        res.send(response);
        return true;
      }
      if (instanceId == null) {
        errMsg = 'Missing mandatory parameter (instance ID)';
        this.logger.error(`ERROR: ${errMsg}`);
        response = {
          success: false,
          message: errMsg
        };
        res.send(response);
        return true;
      }
      if (command == null) {
        errMsg = 'Missing mandatory parameter (command)';
        this.logger.error(`ERROR: ${errMsg}`);
        response = {
          success: false,
          message: errMsg
        };
        res.send(response);
        return true;
      }
      if (isNaN(rows) || isNaN(columns)) {
        errMsg = `Output stream size contains non numeric values. outRows: ${outRows} - outColumns: ${outColumns}`;
        this.logger.error(`ERROR: ${errMsg}`);
        response = {
          success: false,
          message: errMsg
        };
        res.send(response);
        return true;
      }
      // Validate user has permissions to access the requested solution
      this.logger.info(`${meth} - Checking user permissions to access solution ${urn}`);
      realInstanceId = null;
      return this.authorization.isAllowedForURN(urn, context).then(isAllowed => {
        if (!isAllowed) {
          errMsg = 'User is not allowed to access solution.';
          this.logger.debug(`${meth} - ${errMsg}`);
          throw new Error(errMsg);
        }
        this.logger.info(`${meth} - User is allowed to access element ${urn}`);
        // Convert solution+role+instance into deployment+role+instance
        return this.adm.solutionElementToDeploymentElement(urn, role, instanceId, context);
      }).then(deploymentInstanceData => {
        var realContainerName;
        realInstanceId = deploymentInstanceData.instanceName;
        realContainerName = this.manifestHelper.hashContainerName(container);
        this.logger.info(`${meth} - Checking instance and container data...`);
        return this.k8sApiStub.execValidationCheck(realInstanceId, realContainerName);
      }).then(() => {
        response = {
          success: true,
          message: `Websocket access is allowed to instance ${realInstanceId}.`,
          data: {
            wsSessionToken: this.generateEphemeralExecSessionToken(context.user.id)
          }
        };
        res.send(response);
        return true;
      }).catch(err => {
        errMsg = `Error running pre-exec checks: ${err.message}`;
        this.logger.error(`${meth} - ERROR: ${errMsg}`);
        response = {
          success: false,
          message: errMsg
        };
        res.send(response);
        return true;
      });
    }

    execSolutionInstanceCommand(ws, req) {
      var cleanup, columns, command, container, context, errMsg, instanceId, meth, myStreams, outColumns, outRows, role, rows, tty, urn, wsSessionErr, wsToken;
      boundMethodCheck(this, AdmissionRestAPI);
      meth = 'AdmissionRestAPI.execSolutionInstanceCommand';
      context = this.setupContext(req);
      this.logger.info(`${meth} - User: ${context.user.id || context.user.name || ''}`);
      // Exec requests are unlimited in time
      req.setTimeout(30 * 24 * 60 * 60 * 1000); // 30 days

      // Initialize stream holder
      myStreams = {};
      // Validate ephemeral Websocket session token
      wsToken = getFromReq(req, 'session_token');
      this.logger.info(`${meth} - Validating WS session token: ${wsToken}`);
      wsSessionErr = null;
      if (wsToken == null) {
        wsSessionErr = 'Missing mandatory parameter (session_token).';
      } else if (!(wsToken in this.ephemeralWebsocketTokens)) {
        wsSessionErr = 'Websocket session token is expired or invalid.';
      } else if (this.ephemeralWebsocketTokens[wsToken] !== context.user.id) {
        wsSessionErr = "Websocket session token doesn't belong to user.";
      }
      if (wsSessionErr != null) {
        this.logger.error(`ERROR: ${wsSessionErr}`);
        ws.close(4000, wsSessionErr);
        return true;
      }
      // Retrieve request parameters
      urn = getFromReq(req, 'urn');
      role = getFromReq(req, 'role');
      instanceId = getFromReq(req, 'instance');
      container = getFromReq(req, 'container');
      command = getFromReq(req, 'command');
      tty = getFromReq(req, 'tty') === 'yes';
      outRows = getFromReq(req, 'rows');
      outColumns = getFromReq(req, 'columns');
      this.logger.info(`${meth} - Parameters: URN: ${urn} - Role: ${role} - instanceId=${instanceId} - container: ${container} - command: ${JSON.stringify(command)} - tty: ${tty} - outRows: ${outRows} - outColumns: ${outColumns}`);
      rows = parseInt(outRows);
      columns = parseInt(outColumns);
      if (urn == null) {
        errMsg = 'Missing mandatory parameter (solution URN)';
        this.logger.error(`ERROR: ${errMsg}`);
        ws.close(4000, errMsg);
        return true;
      }
      if (role == null) {
        errMsg = 'Missing mandatory parameter (solution role)';
        this.logger.error(`ERROR: ${errMsg}`);
        ws.close(4000, errMsg);
        return true;
      }
      if (instanceId == null) {
        errMsg = 'Missing mandatory parameter (instance ID)';
        this.logger.error(`ERROR: ${errMsg}`);
        ws.close(4000, errMsg);
        return true;
      }
      if (command == null) {
        errMsg = 'Missing mandatory parameter (command)';
        this.logger.error(`ERROR: ${errMsg}`);
        ws.close(4000, errMsg);
        return true;
      }
      if (isNaN(rows) || isNaN(columns)) {
        errMsg = `Output stream size contains non numeric values. outRows: ${outRows} - outColumns: ${outColumns}`;
        this.logger.error(`ERROR: ${errMsg}`);
        ws.close(4000, errMsg);
        return true;
      }
      // Cleanup function to be called when WS session is finished
      cleanup = (code, reason) => {
        var err;
        this.logger.info(`${meth} - Clean-up (Code: ${code}, Reason: ${reason})`);
        try {
          ws.removeAllListeners();
          ws.close(code, reason);
          ws.terminate();
          // If streams have been initialized (check any stream), destroy them
          if (myStreams.out != null) {
            myStreams.out.removeAllListeners();
            myStreams.err.removeAllListeners();
            myStreams.in.removeAllListeners();
            myStreams.out.destroy();
            myStreams.err.destroy();
            myStreams.in.destroy();
          }
        } catch (error1) {
          err = error1;
          this.logger.info(`${meth} - Error during clean-up: ${err.message} - ${err.stack})`);
        }
        return true;
      };
      // Validate user has permissions to access the requested solution
      this.logger.info(`${meth} - Checking user permissions to access solution ${urn}`);
      return this.authorization.isAllowedForURN(urn, context).then(isAllowed => {
        if (!isAllowed) {
          errMsg = 'User is not allowed to access solution.';
          this.logger.debug(`${meth} - ${errMsg}`);
          throw new Error(errMsg);
        }
        this.logger.info(`${meth} - User is allowed to access element ${urn}`);
        // Convert solution+role+instance into deployment+role+instance
        return this.adm.solutionElementToDeploymentElement(urn, role, instanceId, context);
      }).then(deploymentInstanceData => {
        var rInStream, rInStreamOpts, realContainerName, realInstanceId, wErrStream, wErrStreamOpts, wOutStream, wOutStreamOpts;
        realInstanceId = deploymentInstanceData.instanceName;
        realContainerName = this.manifestHelper.hashContainerName(container);
        // Create readable stream for sending commands ('stdin')
        rInStreamOpts = {
          encoding: 'utf8',
          read: function (size) {}
        };
        rInStream = new stream.Readable(rInStreamOpts);
        // Create writable stream for handling received output ('stdout')
        wOutStreamOpts = {
          defaultEncoding: 'utf8',
          write: function (chunk, encoding, callback) {
            // console.log "wOutStream.write - #{chunk}"
            ws.send("OUT:" + chunk);
            return process.nextTick(callback);
          },
          writev: function (chunks, callback) {
            // console.log "wOutStream.writev - #{chunks}"
            return process.nextTick(callback);
          }
        };
        wOutStream = new stream.Writable(wOutStreamOpts);
        wOutStream.rows = rows;
        wOutStream.columns = columns;
        wOutStream.on('error', function () {});
        // Create writable stream for handling received errors ('stderr')
        wErrStreamOpts = {
          defaultEncoding: 'utf8',
          write: function (chunk, encoding, callback) {
            // console.log "wErrStream.write - #{chunk}"
            ws.send("ERR:" + chunk);
            return process.nextTick(callback);
          },
          writev: function (chunks, callback) {
            //console.log "wErrStream.writev - #{chunks}"
            return process.nextTick(callback);
          }
        };
        wErrStream = new stream.Writable(wErrStreamOpts);
        wErrStream.on('error', function () {});
        // Prepare streams to use in K8s exec connection
        myStreams = {
          out: wOutStream,
          err: wErrStream,
          in: rInStream
        };
        // Client Websocket event handlers
        ws.on('message', msg => {
          var newColumns, newRows, toks;
          // A message from the client is considered a comman to execute
          // console.log "ClientWS --> onMessage (stdin): #{msg}"
          if (msg != null ? msg.startsWith("RESIZE:") : void 0) {
            this.logger.info(`${meth} - Output resize received: ${msg}`);
            msg = msg.slice(7); // Remove 'RESIZE:'
            toks = msg.split(',');
            if (toks.length === 2) {
              newRows = parseInt(toks[0]);
              newColumns = parseInt(toks[1]);
              if (!isNaN(rows) && !isNaN(columns)) {
                wOutStream.rows = newRows;
                wOutStream.columns = newColumns;
                return wOutStream.emit('resize');
              }
            }
          } else {
            return rInStream.push(msg);
          }
        });
        ws.on('open', () => {
          return this.logger.info(`${meth} - Websocket with client is open!`);
        });
        ws.on('close', (code, reason) => {
          // The client closed its websocket, cleanup
          this.logger.info(`${meth} - Client Websocket closed! Code: ${code} - Reason: ${reason}`);
          // Sending 'null' is equivalent as signaling the end of input
          return rInStream.push(null);
        });
        ws.on('error', err => {
          // En error ocurred in WS communication
          this.logger.info(`${meth} - Client Websocket error: ${err}`);
          // Sending 'null' is equivalent as signaling the end of input
          return rInStream.push(null);
        });
        // Call K8s API stub for executing the command
        return this.k8sApiStub.execPod(null, realInstanceId, realContainerName, command, tty, myStreams).then(status => {
          this.logger.info(`${meth} Exec command finished with status: ${JSON.stringify(status)}`);
          return cleanup(1000, "Command finished.");
        });
      }).catch(err => {
        errMsg = `Command could not be executed. Error: ${err.message || err}`;
        this.logger.error(`${meth} ${errMsg}`);
        return cleanup(4000, errMsg);
      });
    }

    getInstanceLogs(req, res) {
      var container, context, errMsg, follow, instanceId, meth, previous, response, sinceSeconds, tailLines;
      boundMethodCheck(this, AdmissionRestAPI);
      meth = 'AdmissionRestAPI.getInstanceLogs';
      context = this.setupContext(req);
      this.logger.info(`${meth} - User: ${context.user.id || context.user.name || ''}`);
      // Handlers to log client disconnections (just for information)
      req.on("close", () => {
        return this.logger.info(`${meth} - Http connection closed by the client.`);
      });
      req.on("end", () => {
        return this.logger.info(`${meth} - Http connection ended.`);
      });
      // Retrieve request parameters
      instanceId = getFromReq(req, 'instance');
      container = getFromReq(req, 'container');
      follow = getFromReq(req, 'follow') === 'yes';
      previous = getFromReq(req, 'previous') === 'yes';
      tailLines = getFromReq(req, 'tail');
      sinceSeconds = getFromReq(req, 'since');
      this.logger.info(`${meth} - Parameters: instanceId=${instanceId} - container: ${container} - follow: ${follow} - tailLines: ${tailLines} - sinceSeconds: ${sinceSeconds} previous: ${previous}`);
      // If follow mode is on, don't let the connection timeout
      if (follow) {
        req.setTimeout(30 * 24 * 60 * 60 * 1000); // 30 days
      }

      // Validate mandatory parameter instanceId is present
      if (instanceId == null) {
        errMsg = 'Missing mandatory parameter (instance ID)';
        this.logger.error(`ERROR: ${errMsg}`);
        response = {
          success: false,
          message: errMsg
        };
        res.send(response);
        return true;
      }
      // Validate user has permissions to access the requested instance
      this.logger.info(`${meth} - Checking user permissions to access instance ${instanceId}`);
      this.authorization.isAllowed('instance', instanceId, context);
      return q(true).then(allowed => {
        var logOptions;
        if (!allowed) {
          errMsg = "User is not allowed to access instance.";
          this.logger.error(`${meth} - ${errMsg}`);
          response = {
            success: false,
            message: errMsg
          };
          res.send(response);
          return true;
        }
        // Prepare options to pass to log retriever
        // As a WritableStream we pass the HTTP response object
        logOptions = {
          follow: follow,
          previous: previous,
          tailLines: tailLines,
          sinceSeconds: sinceSeconds
        };
        res.writeHead(200, {
          'Content-Type': 'text/plain; charset=utf-8'
        });
        return this.k8sApiStub.getPodLogs(null, instanceId, container, logOptions, res).catch(err => {
          var ref, ref1;
          if ((ref = err.message) != null ? ref.includes("previous terminated container") : void 0) {
            errMsg = 'Previous container not found.';
          } else if ((ref1 = err.message) != null ? ref1.includes("is not valid for pod") : void 0) {
            errMsg = 'Container not valid for instance.';
          } else {
            errMsg = "Unable to get instance logs.";
            this.logger.error(`ERROR: Unexpected error: ${err.message}`);
          }
          this.logger.error(`ERROR: ${errMsg}`);
          response = {
            success: false,
            message: errMsg
          };
          res.write(JSON.stringify(response));
          res.end();
          return true;
        });
      }).then(status => {
        this.logger.info(`${meth} - Finished log retrieval finished. (${status})`);
        return true;
      }).catch(err => {
        errMsg = `Error while tailing logs: ${err}`;
        this.logger.error(`ERROR: ${errMsg}`);
        response = {
          success: false,
          message: errMsg
        };
        res.send(response);
        return true;
      });
    }

    execInstanceCommandGet(req, res) {
      var columns, command, container, context, errMsg, instanceId, meth, outColumns, outRows, response, rows, tty;
      boundMethodCheck(this, AdmissionRestAPI);
      meth = 'AdmissionRestAPI.execInstanceCommandGet';
      context = this.setupContext(req);
      this.logger.info(`${meth} - User: ${context.user.id || context.user.name || ''}`);
      // Retrieve request parameters
      instanceId = getFromReq(req, 'instance');
      container = getFromReq(req, 'container');
      command = getFromReq(req, 'command');
      tty = getFromReq(req, 'tty') === 'yes';
      outRows = getFromReq(req, 'rows');
      outColumns = getFromReq(req, 'columns');
      this.logger.info(`${meth} - Parameters: instanceId=${instanceId} - container: ${container} - command: ${command} - tty: ${tty} - outRows: ${outRows} - outColumns: ${outColumns}`);
      rows = parseInt(outRows);
      columns = parseInt(outColumns);
      // Validate mandatory parameter instanceId is present
      if (instanceId == null) {
        errMsg = 'Missing mandatory parameter (instance ID)';
        this.logger.error(`ERROR: ${errMsg}`);
        response = {
          success: false,
          message: errMsg
        };
        res.send(response);
        return true;
      }
      if (command == null) {
        errMsg = 'Missing mandatory parameter (command)';
        this.logger.error(`ERROR: ${errMsg}`);
        response = {
          success: false,
          message: errMsg
        };
        res.send(response);
        return true;
      }
      if (isNaN(rows) || isNaN(columns)) {
        errMsg = `Output stream size contains non numeric values. outRows: ${outRows} - outColumns: ${outColumns}`;
        this.logger.error(`ERROR: ${errMsg}`);
        response = {
          success: false,
          message: errMsg
        };
        res.send(response);
        return true;
      }
      this.logger.info(`${meth} - Checking instance and container data...`);
      return this.k8sApiStub.execValidationCheck(instanceId, container).then(() => {
        // Validate user has permissions to access the requested instance
        this.logger.info(`${meth} - Checking user permissions to access instance ${instanceId}...`);
        return this.authorization.isAllowed('instance', instanceId, context);
      }).then(allowed => {
        if (!allowed) {
          throw new Error("User is not allowed to access instance.");
        }
        response = {
          success: true,
          message: `Websocket access is allowed to instance ${instanceId}.`,
          data: {
            wsSessionToken: this.generateEphemeralExecSessionToken(context.user.id)
          }
        };
        res.send(response);
        return true;
      }).catch(err => {
        errMsg = `Error running pre-exec checks: ${err.message}`;
        this.logger.error(`${meth} - ERROR: ${errMsg}`);
        response = {
          success: false,
          message: errMsg
        };
        res.send(response);
        return true;
      });
    }

    execInstanceCommand(ws, req) {
      var cleanup, columns, command, container, context, errMsg, instanceId, meth, myStreams, outColumns, outRows, rows, tty, wsSessionErr, wsToken;
      boundMethodCheck(this, AdmissionRestAPI);
      meth = 'AdmissionRestAPI.execInstanceCommand';
      context = this.setupContext(req);
      this.logger.info(`${meth} - User: ${context.user.id || context.user.name || ''}`);
      // Exec requests are unlimited in time
      req.setTimeout(30 * 24 * 60 * 60 * 1000); // 30 days

      // Initialize stream holder
      myStreams = {};
      // Validate ephemeral Websocket session token
      wsToken = getFromReq(req, 'session_token');
      this.logger.info(`${meth} - Validating WS session token: ${wsToken}`);
      wsSessionErr = null;
      if (wsToken == null) {
        wsSessionErr = 'Missing mandatory parameter (session_token).';
      } else if (!(wsToken in this.ephemeralWebsocketTokens)) {
        wsSessionErr = 'Websocket session token is expired or invalid.';
      } else if (this.ephemeralWebsocketTokens[wsToken] !== context.user.id) {
        wsSessionErr = "Websocket session token doesn't belong to user.";
      }
      if (wsSessionErr != null) {
        this.logger.error(`ERROR: ${wsSessionErr}`);
        ws.close(4000, wsSessionErr);
        return true;
      }
      // Retrieve request parameters
      instanceId = getFromReq(req, 'instance');
      container = getFromReq(req, 'container');
      command = getFromReq(req, 'command');
      tty = getFromReq(req, 'tty') === 'yes';
      outRows = getFromReq(req, 'rows');
      outColumns = getFromReq(req, 'columns');
      this.logger.info(`${meth} - Parameters: instanceId=${instanceId} - container: ${container} - command: ${command} - tty: ${tty} - outRows: ${outRows} - outColumns: ${outColumns}`);
      rows = parseInt(outRows);
      columns = parseInt(outColumns);
      if (instanceId == null) {
        errMsg = 'Missing mandatory parameter (instance ID)';
        this.logger.error(`ERROR: ${errMsg}`);
        ws.close(4000, errMsg);
        return true;
      }
      if (command == null) {
        errMsg = 'Missing mandatory parameter (command)';
        this.logger.error(`ERROR: ${errMsg}`);
        ws.close(4000, errMsg);
        return true;
      }
      if (isNaN(rows) || isNaN(columns)) {
        errMsg = `Output stream size contains non numeric values. outRows: ${outRows} - outColumns: ${outColumns}`;
        this.logger.error(`ERROR: ${errMsg}`);
        ws.close(4000, errMsg);
        return true;
      }
      // Cleanup function to be called when WS session is finished
      cleanup = (code, reason) => {
        var err;
        this.logger.info(`${meth} - Clean-up (Code: ${code}, Reason: ${reason})`);
        try {
          ws.removeAllListeners();
          ws.close(code, reason);
          ws.terminate();
          // If streams have been initialized (check any stream), destroy them
          if (myStreams.out != null) {
            myStreams.out.removeAllListeners();
            myStreams.err.removeAllListeners();
            myStreams.in.removeAllListeners();
            myStreams.out.destroy();
            myStreams.err.destroy();
            myStreams.in.destroy();
          }
        } catch (error1) {
          err = error1;
          this.logger.info(`${meth} - Error during clean-up: ${err.message} - ${err.stack})`);
        }
        return true;
      };
      // Validate user has permissions to access the requested instance
      this.logger.info(`${meth} - Checking user permissions to access instance ${instanceId}`);
      return this.authorization.isAllowed('instance', instanceId, context).then(allowed => {
        var rInStream, rInStreamOpts, wErrStream, wErrStreamOpts, wOutStream, wOutStreamOpts;
        if (!allowed) {
          errMsg = "User is not allowed to access instance.";
          this.logger.error(`${meth} - ${errMsg}`);
          ws.close(4000, errMsg);
          return true;
        } else {
          // Create readable stream for sending commands ('stdin')
          rInStreamOpts = {
            encoding: 'utf8',
            read: function (size) {}
          };
          rInStream = new stream.Readable(rInStreamOpts);
          // Create writable stream for handling received output ('stdout')
          wOutStreamOpts = {
            defaultEncoding: 'utf8',
            write: function (chunk, encoding, callback) {
              // console.log "wOutStream.write - #{chunk}"
              ws.send("OUT:" + chunk);
              return process.nextTick(callback);
            },
            writev: function (chunks, callback) {
              // console.log "wOutStream.writev - #{chunks}"
              return process.nextTick(callback);
            }
          };
          wOutStream = new stream.Writable(wOutStreamOpts);
          wOutStream.rows = rows;
          wOutStream.columns = columns;
          wOutStream.on('error', function () {});
          // Create writable stream for handling received errors ('stderr')
          wErrStreamOpts = {
            defaultEncoding: 'utf8',
            write: function (chunk, encoding, callback) {
              // console.log "wErrStream.write - #{chunk}"
              ws.send("ERR:" + chunk);
              return process.nextTick(callback);
            },
            writev: function (chunks, callback) {
              //console.log "wErrStream.writev - #{chunks}"
              return process.nextTick(callback);
            }
          };
          wErrStream = new stream.Writable(wErrStreamOpts);
          wErrStream.on('error', function () {});
          // Prepare streams to use in K8s exec connection
          myStreams = {
            out: wOutStream,
            err: wErrStream,
            in: rInStream
          };
          // Client Websocket event handlers
          ws.on('message', msg => {
            var newColumns, newRows, toks;
            // A message from the client is considered a comman to execute
            // console.log "ClientWS --> onMessage (stdin): #{msg}"
            if (msg != null ? msg.startsWith("RESIZE:") : void 0) {
              this.logger.info(`${meth} - Output resize received: ${msg}`);
              msg = msg.slice(7); // Remove 'RESIZE:'
              toks = msg.split(',');
              if (toks.length === 2) {
                newRows = parseInt(toks[0]);
                newColumns = parseInt(toks[1]);
                if (!isNaN(rows) && !isNaN(columns)) {
                  wOutStream.rows = newRows;
                  wOutStream.columns = newColumns;
                  return wOutStream.emit('resize');
                }
              }
            } else {
              return rInStream.push(msg);
            }
          });
          ws.on('open', () => {
            return this.logger.info(`${meth} - Websocket with client is open!`);
          });
          ws.on('close', (code, reason) => {
            // The client closed its websocket, cleanup
            this.logger.info(`${meth} - Client Websocket closed! Code: ${code} - Reason: ${reason}`);
            // Sending 'null' is equivalent as signaling the end of input
            return rInStream.push(null);
          });
          ws.on('error', err => {
            // En error ocurred in WS communication
            this.logger.info(`${meth} - Client Websocket error: ${err}`);
            // Sending 'null' is equivalent as signaling the end of input
            return rInStream.push(null);
          });
          // Call K8s API stub for executing the command
          return this.k8sApiStub.execPod(null, instanceId, container, command, tty, myStreams).then(status => {
            this.logger.info(`${meth} Exec command finished with status: ${JSON.stringify(status)}`);
            return cleanup(1000, "Command finished.");
          });
        }
      }).catch(err => {
        errMsg = `Command could not be executed. Error: ${err.message || err}`;
        this.logger.error(`${meth} ${errMsg}`);
        return cleanup(4000, errMsg);
      });
    }

    generateEphemeralExecSessionToken(user) {
      var meth, token;
      meth = `generateEphemeralExecSessionToken user: ${user}`;
      this.logger.debug(meth);
      token = utils.randomHex(10);
      this.ephemeralWebsocketTokens[token] = user;
      this.logger.debug(`${meth} - Generated new token for user (will expire in ${EPEHEMERAL_WS_TOKEN_LIFETIME / 1000} seconds.`);
      // Self destroy after lifetime
      setTimeout(() => {
        delete this.ephemeralWebsocketTokens[token];
        return this.logger.debug(`${meth} - Token expired : ${token}`);
      }, EPEHEMERAL_WS_TOKEN_LIFETIME);
      return token;
    }

    getSolutionRevision(req, res) {
      var context, errMsg, meth, response, rev, solRef, urn;
      boundMethodCheck(this, AdmissionRestAPI);
      meth = 'AdmissionRestAPI.getSolutionRevision';
      context = this.setupContext(req);
      this.logger.info(`${meth} - User: ${context.user.id || context.user.name || ''}`);
      // Retrieve request parameters
      urn = getFromReq(req, 'urn');
      rev = getFromReq(req, 'rev');
      this.logger.info(`${meth} - Parameters: urn=${urn} - rev=${rev}`);
      // Validate mandatory parameter urn is present
      if (urn == null) {
        errMsg = 'Missing mandatory parameter (solution URN)';
        this.logger.error(`ERROR: ${errMsg}`);
        response = {
          success: false,
          message: errMsg
        };
        res.send(response);
        return true;
      }
      solRef = this.manifestHelper.urnToRef(urn);
      this.logger.debug(`${meth} - solRef = ${JSON.stringify(solRef)}`);
      // Validate URN is of a solution
      if (solRef.kind !== 'solution') {
        errMsg = 'Invalid URN (not a solution)';
        this.logger.error(`ERROR: ${errMsg}`);
        response = {
          success: false,
          message: errMsg
        };
        res.send(response);
        return true;
      }
      // Validate user has permissions to access the requested solution
      this.logger.info(`${meth} - Checking user permissions to access solution ${urn}`);
      // Check permissions
      return this.authorization.isAllowedForURN(urn, context).then(isAllowed => {
        if (!isAllowed) {
          errMsg = "User is not allowed to access solution.";
          this.logger.info(`${meth} - ${errMsg}`);
          throw new Error(errMsg);
        }
      }).then(() => {
        var labelSelector;
        if (rev == null) {
          // Return a list of all revisions of the solution
          // The last part of the selector is for excluding revisions of objects
          // that are derived from the solution itself.
          labelSelector = '';
          labelSelector += "kumori/domain=" + this.manifestHelper.hashLabel(solRef.domain);
          labelSelector += ",kumori/name=" + this.manifestHelper.hashLabel(solRef.name);
          labelSelector += ",!kumori/solution.id";
          return this.k8sApiStub.getControllerRevisions(KUMORI_NAMESPACE, labelSelector).then(items => {
            var history, hit, i, it, j, len, len1, list, newItem, ref;
            this.logger.debug(`${meth} - Got list of revisions (${items.length})`);
            list = [];
            // From the list of ControllerRevisions that matched the LabelSelector,
            // discard the ones that don't have the Kumori history annotation.
            // With the remaining ones, create a simplified list based on the
            // history.
            for (i = 0, len = items.length; i < len; i++) {
              it = items[i];
              if (((ref = it.metadata) != null ? ref.annotations : void 0) != null && 'kumori/history' in it.metadata.annotations) {
                history = JSON.parse(it.metadata.annotations['kumori/history']);
                for (j = 0, len1 = history.length; j < len1; j++) {
                  hit = history[j];
                  newItem = {
                    name: it.metadata.name,
                    revision: hit.revision,
                    timestamp: hit.timestamp,
                    comment: hit.comment
                  };
                  list.push(newItem);
                }
              }
            }
            this.logger.debug(`${meth} - Returning list: ${JSON.stringify(list)}`);
            response = {
              success: true,
              message: "solution revisions retrieved successfully.",
              data: list
            };
            res.send(response);
            return true;
          });
        } else {
          // Return a specific revision
          return this.k8sApiStub.getControllerRevision(KUMORI_NAMESPACE, rev).then(revision => {
            var compressedManifest, manifest, ref, ref1, ref2;
            this.logger.debug(`${meth} - Got revision: ${JSON.stringify(revision)}`);
            // Verify that the revision contains the original manifest
            if (((ref = revision.spec) != null ? ref.kumoriManifest : void 0) != null) {
              compressedManifest = revision.spec.kumoriManifest;
            }
            if (((ref1 = revision.metadata) != null ? (ref2 = ref1.annotations) != null ? ref2['kumori/manifest'] : void 0 : void 0) != null) {
              compressedManifest = revision.metadata.annotations['kumori/manifest'];
            } else {
              errMsg = "No manifest data found in revision.";
              this.logger.error(`${meth} - ${errMsg}`);
              throw new Error(errMsg);
            }
            // Extract the original manifest from the revision
            manifest = JSON.parse(utils.gunzipString(compressedManifest));
            this.logger.debug(`${meth} - Manifest Name: ${manifest.urn}`);
            // Check that the revision belongs to the requested solution
            if (manifest.urn !== urn) {
              errMsg = "Revision belongs to a different solution.";
              this.logger.error(`${meth} - ${errMsg}`);
              throw new Error(errMsg);
            }
            response = {
              success: true,
              message: "Solution revision retrieved successfully.",
              data: manifest
            };
            res.send(response);
            return true;
          });
        }
      }).catch(err => {
        errMsg = `Couldn't get solution revisions: ${err.message}`;
        this.logger.error(`${meth} - Error: ${errMsg}`);
        response = {
          success: false,
          message: errMsg
        };
        res.send(response);
        return true;
      });
    }

    getDeploymentRevision(req, res) {
      var context, deplRef, errMsg, meth, response, rev, urn;
      boundMethodCheck(this, AdmissionRestAPI);
      meth = 'AdmissionRestAPI.getDeploymentRevision';
      context = this.setupContext(req);
      this.logger.info(`${meth} - User: ${context.user.id || context.user.name || ''}`);
      // Retrieve request parameters
      urn = getFromReq(req, 'urn');
      rev = getFromReq(req, 'rev');
      this.logger.info(`${meth} - Parameters: urn=${urn} - rev=${rev}`);
      // Validate mandatory parameter instanceId is present
      if (urn == null) {
        errMsg = 'Missing mandatory parameter (deployment URN)';
        this.logger.error(`ERROR: ${errMsg}`);
        response = {
          success: false,
          message: errMsg
        };
        res.send(response);
        return true;
      }
      deplRef = this.manifestHelper.urnToRef(urn);
      this.logger.debug(`${meth} - deplRef = ${JSON.stringify(deplRef)}`);
      // Validate user has permissions to access the requested deployment
      this.logger.info(`${meth} - Checking user permissions to access deployment ${urn}`);
      // Check permissions
      return this.authorization.isAllowed('deployment', urn, context).then(allowed => {
        if (!allowed) {
          errMsg = "User is not allowed to access deployment.";
          this.logger.info(`${meth} - ${errMsg}`);
          throw new Error(errMsg);
        }
      }).then(() => {
        var labelSelector;
        if (rev == null) {
          // Return a list of all revisions of the deployment
          labelSelector = '';
          labelSelector += `kumori/domain=${deplRef.domain}`;
          labelSelector += `,kumori/name=${deplRef.name}`;
          return this.k8sApiStub.getControllerRevisions(KUMORI_NAMESPACE, labelSelector).then(items => {
            var history, hit, i, it, j, len, len1, list, newItem, ref;
            this.logger.debug(`${meth} - Got list of revisions (${items.length})`);
            list = [];
            // From the list of ControllerRevisions that matched the LabelSelector,
            // discard the ones that don't have the Kumori history annotation.
            // With the remaining ones, create a simplified list based on the
            // history.
            for (i = 0, len = items.length; i < len; i++) {
              it = items[i];
              if (((ref = it.metadata) != null ? ref.annotations : void 0) != null && 'kumori/history' in it.metadata.annotations) {
                history = JSON.parse(it.metadata.annotations['kumori/history']);
                for (j = 0, len1 = history.length; j < len1; j++) {
                  hit = history[j];
                  newItem = {
                    name: it.metadata.name,
                    revision: hit.revision,
                    timestamp: hit.timestamp,
                    comment: hit.comment
                  };
                  list.push(newItem);
                }
              }
            }
            this.logger.debug(`${meth} - Returning list: ${JSON.stringify(list)}`);
            response = {
              success: true,
              message: "Deployment revisions retrieved successfully.",
              data: list
            };
            res.send(response);
            return true;
          });
        } else {
          // Return a specific revision
          return this.k8sApiStub.getControllerRevision(KUMORI_NAMESPACE, rev).then(revision => {
            var manifest, ref, ref1;
            this.logger.debug(`${meth} - Got revision: ${JSON.stringify(revision)}`);
            // Verify that the revision contains the original manifest
            if (((ref = revision.metadata) != null ? (ref1 = ref.annotations) != null ? ref1.manifest : void 0 : void 0) == null) {
              errMsg = "No manifest data found in revision.";
              this.logger.error(`${meth} - ${errMsg}`);
              throw new Error(errMsg);
            }
            // Extract the original manifest from the revision
            manifest = JSON.parse(utils.gunzipString(revision.metadata.annotations.manifest));
            this.logger.debug(`${meth} - MANIFEST NAME: ${manifest.urn}`);
            // Check that the revision belongs to the requested deployment
            if (manifest.urn !== urn) {
              errMsg = "Revision belongs to a different deployment.";
              this.logger.error(`${meth} - ${errMsg}`);
              throw new Error(errMsg);
            }
            response = {
              success: true,
              message: "Deployment revision retrieved successfully.",
              data: manifest
            };
            res.send(response);
            return true;
          });
        }
      }).catch(err => {
        errMsg = `Couldn't get deployment revisions: ${err.message}`;
        this.logger.error(`${meth} - Error: ${errMsg}`);
        response = {
          success: false,
          message: errMsg
        };
        res.send(response);
        return true;
      });
    }

  };

  //###############################################################################
  //#                           HELPER FUNCTIONS                                 ##
  //###############################################################################
  startsWith = function (str, prefix) {
    return str.slice(0, +(prefix.length - 1) + 1 || 9e9) === prefix;
  };

  decodeJwt = function (token) {
    var header, headerSeg, payload, payloadSeg, segments, signatureSeg;
    segments = token.split('.');
    if (segments.length !== 3) {
      throw new Error(`Token has wrong number of segments (${segments.length}).`);
    }
    // All segment should be base64
    headerSeg = segments[0];
    payloadSeg = segments[1];
    signatureSeg = segments[2];
    // base64 decode and parse JSON
    header = JSON.parse(base64urlDecode(headerSeg));
    payload = JSON.parse(base64urlDecode(payloadSeg));
    return {
      header: header,
      payload: payload,
      signature: signatureSeg
    };
  };

  base64urlDecode = function (str) {
    return Buffer.from(base64urlUnescape(str), 'base64').toString();
  };

  base64urlUnescape = function (str) {
    str += Array(5 - str.length % 4).join('=');
    return str.replace(/\-/g, '+').replace(/_/g, '/');
  };

  // Returns an object with the following User data:

  // - access_token: (example: "njrfo4fnonf2f...")
  // - token_type: (example 'bearer')
  // - expires_in: (example 60)
  // - refresh_token: (example: "njrfo4fnonf2f...")
  // - user:
  //   - username (example: "alice")
  //   - name (example: "Alice Liddel")
  //   - email (example: "alice@keycloak.org")
  //   - groups (example: ["offline_access","user"])
  //   - kumoriGroups (example: ["developer"])
  getAuthData = function (req) {
    var accessTokenData, authData, ref;
    // console.log "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
    // console.log "getAuthData - REQ KEYS: #{Object.keys req}"
    // console.log "getAuthData - KAUTH: #{JSON.stringify req.kauth}"
    // console.log "getAuthData - SESSION: #{JSON.stringify req.session}"
    // console.log "getAuthData - CCAUTH: #{JSON.stringify req.clientcertauth}"
    // console.log "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
    if (((ref = req.kauth) != null ? ref.grant : void 0) != null) {
      // console.log "getAuthData - KAUTH: #{JSON.stringify req.kauth.grant,}"
      accessTokenData = req.kauth.grant.access_token.content;
      authData = {
        access_token: req.kauth.grant.access_token.token,
        token_type: req.kauth.grant.token_type,
        expires_in: req.kauth.grant.expires_in,
        refresh_token: null,
        user: {
          id: accessTokenData.preferred_username,
          username: accessTokenData.preferred_username,
          name: accessTokenData.name,
          email: accessTokenData.email,
          groups: accessTokenData.groups || [],
          kumoriGroups: accessTokenData['client-groups'] || [],
          roles: accessTokenData['client-groups'] || [],
          limits: null
        }
      };
      if (req.kauth.grant.refresh_token != null) {
        authData.refresh_token = req.kauth.grant.refresh_token.token;
      }
      // console.log "REQUEST AUTH DATA: #{JSON.stringify authData}"
      return authData;
    } else if (req.clientcertauth != null) {
      authData = req.clientcertauth;
      console.log(`REQUEST AUTH DATA FROM CLIENT CERT: ${JSON.stringify(authData)}`);
      return authData;
    } else {
      // console.log "getAuthData - NO KAUTH.GRANT --> returning null"
      return null;
    }
  };

  // Load a file and return its content
  loadFile = function (path) {
    var content, error;
    content = null;
    try {
      content = require(path);
      return q.resolve(content);
    } catch (error1) {
      error = error1;
      return q.reject(error);
    }
  };

  cleanupFiles = function (fileList) {
    var file, i, len, promises;
    promises = [];
    for (i = 0, len = fileList.length; i < len; i++) {
      file = fileList[i];
      promises.push(utils.deleteFile(file));
    }
    return q.all(promises);
  };

  // Generic for getting a named parameter from request URL.
  // If the value is URI-encoded, it decodes it
  getFromReq = function (req, name) {
    var cleanValues, i, len, ref, ref1, ref2, v, value;
    value = (ref = (ref1 = req.params) != null ? ref1[name] : void 0) != null ? ref : (ref2 = req.query) != null ? ref2[name] : void 0;
    if (value == null || value === '') {
      return void 0;
    } else if ('string' === typeof value) {
      // If param is a string, URI-decode it after replacing + with space
      // Reference: https://stackoverflow.com/a/20247435
      return decodeURIComponent(value.replace(/\+/g, ' '));
    } else if (Array.isArray(value)) {
      // If param is an array, return it but with any strings URI-decoded
      cleanValues = [];
      for (i = 0, len = value.length; i < len; i++) {
        v = value[i];
        if ('string' === typeof v) {
          cleanValues.push(decodeURIComponent(v.replace(/\+/g, ' ')));
        } else {
          cleanValues.push(v);
        }
      }
      return cleanValues;
    } else {
      return value;
    }
  };

  module.exports = AdmissionRestAPI;
}).call(undefined);