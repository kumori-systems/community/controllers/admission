'use strict';

(function () {
  /*
  * Copyright 2022 Kumori Systems S.L.
   * Licensed under the EUPL, Version 1.2 or – as soon they
    will be approved by the European Commission - subsequent
    versions of the EUPL (the "Licence");
   * You may not use this work except in compliance with the
    Licence.
   * You may obtain a copy of the Licence at:
     https://joinup.ec.europa.eu/software/page/eupl
   * Unless required by applicable law or agreed to in
    writing, software distributed under the Licence is
    distributed on an "AS IS" basis,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
    express or implied.
   * See the Licence for the specific language governing
    permissions and limitations under the Licence.
   */
  var DOMAIN_REGEX,
      _unzstd,
      _unzstdToStream,
      _zstd,
      _zstdFromStream,
      arrayDifference,
      arrayEqual,
      arrayIntersection,
      arrayMerge,
      calculateChecksum,
      checkFileExists,
      copyFile,
      crypto,
      deleteFile,
      endsWith,
      extendObject,
      fnv1Hash,
      fnv1aHash,
      fs,
      generateId,
      getDateSuffix,
      getDirContents,
      getDirSize,
      getFnvHash,
      getFolderSize,
      getHash,
      getTimestamp,
      gunzipString,
      gzipString,
      isArray,
      isObject,
      isString,
      jsonFromFile,
      jsonStringFromFile,
      mkdirp,
      moment,
      packDirectory,
      path,
      preaddir,
      pstat,
      q,
      randomHex,
      randomInt,
      renameFile,
      replacePattern,
      replacePatternInJSON,
      saveFile,
      startsWith,
      tar,
      tarzstd,
      untarzstd,
      untgz,
      unzip,
      unzstd,
      uuid,
      validateDomainName,
      yaml,
      yamlParse,
      yamlStringify,
      yauzl,
      yazl,
      zipDir,
      zipFile,
      zlib,
      zstandard,
      zstd,
      indexOf = [].indexOf;

  q = require('q');

  fs = require('fs');

  tar = require('tar-fs');

  path = require('path');

  uuid = require('uuid');

  zlib = require('zlib');

  yaml = require('js-yaml');

  yazl = require('yazl');

  yauzl = require('yauzl');

  mkdirp = require('mkdirp');

  crypto = require('crypto');

  moment = require('moment');

  zstandard = require('node-zstandard');

  getFolderSize = require('get-folder-size');

  // Regular expression for validating domain names (of type A, not international)
  DOMAIN_REGEX = /^\b((?=[a-z0-9-]{1,63}\.)[a-z0-9]+(-[a-z0-9]+)*\.)+[a-z]{2,63}\b$/;

  // Define some promisified versions of common functions
  pstat = q.denodeify(fs.stat);

  preaddir = q.denodeify(fs.readdir);

  //###############################################################################
  //#                               JSON UTILITIES                               ##
  //###############################################################################

  // Read and parse JSON from a file. Returns a promise
  jsonFromFile = function (filename) {
    return q.promise(function (resolve, reject) {
      return fs.readFile(filename, function (error, rawData) {
        var err, parsed;
        if (error) {
          return reject(new Error(`Reading file ${filename}: ` + error.message));
        } else {
          try {
            parsed = JSON.parse(rawData);
            return resolve(parsed);
          } catch (error1) {
            err = error1;
            return reject(new Error(`Parsing JSON data ${filename}: ` + err));
          }
        }
      });
    });
  };

  // Read JSON from a file, no parsing. Returns a promise
  jsonStringFromFile = function (filename) {
    return q.promise(function (resolve, reject) {
      return fs.readFile(filename, function (error, rawData) {
        if (error) {
          return reject(new Error('Reading file: ' + error.message));
        } else {
          return resolve(rawData);
        }
      });
    });
  };

  //###############################################################################
  //#                               YAML UTILITIES                               ##
  //###############################################################################

  // Convert a YAML formatted string to an Object
  yamlParse = function (yamlStr) {
    var e, errMsg, yamlObj;
    try {
      yamlObj = yaml.load(yamlStr);
      return yamlObj;
    } catch (error1) {
      e = error1;
      errMsg = `Couldn't parse YAML: \"${yamlStr}\". Reason: ${e.message}`;
      throw new Error(errMsg);
    }
  };

  // Convert an object to a YAML formatted string
  yamlStringify = function (yamlObj) {
    var e, errMsg;
    try {
      return yaml.dump(yamlObj);
    } catch (error1) {
      e = error1;
      errMsg = `Couldn't stringify YAML object. Reason: ${e.message}`;
      throw new Error(errMsg);
    }
  };

  //###############################################################################
  //#                               FILE UTILITIES                               ##
  //###############################################################################

  // Save a string to a file. Returns a promise
  saveFile = function (data, filename) {
    return q.promise(function (resolve, reject) {
      return fs.writeFile(filename, data, function (error) {
        if (error) {
          return reject(new Error(`Saving file '${filename}': ` + error.message));
        } else {
          return resolve(filename);
        }
      });
    });
  };

  // Delete a file. Returns a promise
  deleteFile = function (filename) {
    return q.promise(function (resolve, reject) {
      return fs.unlink(filename, function (error) {
        if (error && !error.code === 'ENOENT') {
          reject(new Error('Deleting file: ' + error.message));
        }
        return resolve();
      });
    });
  };

  // Renames a file or directory. Returns a promise
  renameFile = function (oldPath, newPath) {
    return q.promise(function (resolve, reject) {
      fs.rename(oldPath, newPath, function (error) {
        return reject(new Error('Renaming file: ' + error.message));
      });
      return resolve();
    });
  };

  // Copy a file. Returns a promise
  copyFile = function (srcPath, dstPath) {
    return q.promise(function (resolve, reject) {
      var readStream, writeStream;
      readStream = fs.createReadStream(srcPath);
      readStream.on('error', reject);
      writeStream = fs.createWriteStream(dstPath);
      writeStream.on('error', reject);
      writeStream.on('finish', resolve);
      return readStream.pipe(writeStream);
    });
  };

  // Check if a file or directory exists. Returns a promise
  checkFileExists = function (filename) {
    return q.promise(function (resolve, reject) {
      return fs.stat(filename, function (error, stats) {
        if (error && !error.code === 'ENOENT') {
          return reject(new Error('Checking file: ' + error.message));
        } else if (error) {
          return resolve(false);
        } else {
          return resolve(true);
        }
      });
    });
  };

  // Returns a promise. If the operation is successful, the promise resolves with
  // an object with the contents of a directory. Options can be used to
  // configure if search is recursive (default is off) and if items are returned as
  // absolute (default) or relative paths.

  // The object returned contains three lists of items matching the options:
  //  - files     : list of all the files found
  //  - dirs      : list of all the non-empty subdirectories found
  //  - emptyDirs : list of all the empty subdirectories found
  getDirContents = function (dir, options = null, result = null, depth = 0) {
    var DEFAULT_OPTIONS;
    DEFAULT_OPTIONS = {
      recursive: false,
      relative: false
    };
    if (dir == null) {
      return q.reject(new Error('Required parameter "dir" is missing.'));
    }
    if (result == null) {
      if (depth === 0) {
        result = {
          files: [],
          dirs: [],
          emptyDirs: []
        };
      } else {
        return q.reject(new Error(`Previous result is null in depth ${depth}`));
      }
    }
    // Assign default values
    if (options == null) {
      options = DEFAULT_OPTIONS;
    }
    if (options.recursive == null) {
      options.recursive = DEFAULT_OPTIONS.recursive;
    }
    if (options.relative == null) {
      options.relative = DEFAULT_OPTIONS.relative;
    }
    return preaddir(dir).then(function (elements) {
      var el, j, len1, promiseList;
      promiseList = [];
      if (elements.length === 0) {
        if (depth !== 0) {
          result.emptyDirs.push(dir);
        }
      } else {
        if (depth !== 0) {
          result.dirs.push(dir);
        }
        for (j = 0, len1 = elements.length; j < len1; j++) {
          el = elements[j];
          (function (el) {
            var elPath;
            elPath = path.join(dir, el);
            return promiseList.push(pstat(elPath).then(function (fileStats) {
              if (fileStats.isFile()) {
                return result.files.push(elPath);
              } else if (fileStats.isDirectory()) {
                if (options.recursive) {
                  return getDirContents(elPath, options, result, depth + 1);
                } else {
                  // If not in recursive mode, find out if directory is empty
                  return preaddir(elPath).then(function (els) {
                    if (els.length === 0) {
                      return result.emptyDirs.push(elPath);
                    } else {
                      return result.dirs.push(elPath);
                    }
                  });
                }
              }
            }));
          })(el);
        }
      }
      return q.all(promiseList);
    }).then(function () {
      var d, f;
      // Ensure consistent results by ordering items
      result.files.sort(function (x, y) {
        return x > y;
      });
      result.dirs.sort(function (x, y) {
        return x > y;
      });
      result.emptyDirs.sort(function (x, y) {
        return x > y;
      });
      if (options.relative && depth === 0) {
        result.files = function () {
          var j, len1, ref, results;
          ref = result.files;
          results = [];
          for (j = 0, len1 = ref.length; j < len1; j++) {
            f = ref[j];
            results.push(path.relative(dir, f));
          }
          return results;
        }();
        result.dirs = function () {
          var j, len1, ref, results;
          ref = result.dirs;
          results = [];
          for (j = 0, len1 = ref.length; j < len1; j++) {
            d = ref[j];
            results.push(path.relative(dir, d));
          }
          return results;
        }();
        result.emptyDirs = function () {
          var j, len1, ref, results;
          ref = result.emptyDirs;
          results = [];
          for (j = 0, len1 = ref.length; j < len1; j++) {
            d = ref[j];
            results.push(path.relative(dir, d));
          }
          return results;
        }();
        return result;
      } else {
        return result;
      }
    }).fail(function (err) {
      if (depth === 0) {
        return q.reject(new Error(`Failed to list directory ${dir}: ${err.stack}`));
      } else {
        return q.reject(err);
      }
    });
  };

  // Get the size of a folder (returned via promise)
  getDirSize = function (dir) {
    return q.promise(function (resolve, reject) {
      return getFolderSize(dir, function (err, size) {
        if (err != null) {
          return reject(err);
        } else {
          return resolve(size);
        }
      });
    });
  };

  //###############################################################################
  //#                          COMPRESSION UTILITIES                             ##
  //###############################################################################
  _zstd = q.denodeify(zstandard.compress);

  zstd = function (srcFile, zstFile) {
    return _zstd(srcFile, zstFile);
  };

  _unzstd = q.denodeify(zstandard.decompress);

  unzstd = function (zstFile, destFile) {
    return _unzstd(zstFile, destFile);
  };

  _zstdFromStream = q.denodeify(zstandard.compressStreamToFile);

  tarzstd = function (srcPath, zstFile) {
    return q.promise(function (resolve, reject) {
      return pstat(srcPath).then(function (status) {
        var tarStream;
        tarStream = tar.pack(srcPath);
        tarStream.on('error', function (error) {
          return reject(error);
        });
        return _zstdFromStream(tarStream, zstFile);
      }).then(function (status) {
        return status.on('error', function (error) {
          return reject(error);
        }).on('end', function (error) {
          return resolve(zstFile);
        });
      }).fail(function (err) {
        return reject(err);
      });
    });
  };

  _unzstdToStream = q.denodeify(zstandard.decompressFileToStream);

  untarzstd = function (zstFile, destPath) {
    return q.promise(function (resolve, reject) {
      return pstat(zstFile).then(function (status) {
        return _unzstdToStream(zstFile, tar.extract(destPath));
      }).then(function (status) {
        return status.on('error', function (err) {
          return reject(err);
        }).on('finish', function () {
          return resolve(destPath);
        });
      }).fail(function (err) {
        return reject(err);
      });
    });
  };

  // Unpack a tgz file to a directory. Returns a promise.
  untgz = function (tgzFile, dstPath) {
    return q.Promise(function (resolve, reject) {
      return pstat(tgzFile).then(function (status) {
        var input;
        input = fs.createReadStream(tgzFile);
        return input.pipe(zlib.createGunzip()).on('error', function (err) {
          return reject(err);
        }).pipe(tar.extract(dstPath)).on('error', function (err) {
          return reject(err);
        }).on('finish', resolve);
      }).fail(function (err) {
        return reject(err);
      });
    });
  };

  // Extract a zip file to a directory.
  // Returns a promise.
  // If a maximum size is specified, the operation fails (as soon as possible)
  // during the process when that maximum is reached.
  // Note: When an error occurs, the generated directories/files are not deleted
  unzip = function (zipFile, dstPath, maxSize) {
    var currentSize;
    currentSize = 0;
    return q.promise(function (resolve, reject) {
      return pstat(zipFile).then(function (status) {
        var e;
        try {
          return yauzl.open(zipFile, {
            lazyEntries: true
          }, function (err, zipObject) {
            var e;
            if (err) {
              reject(err); // --> 2nd try-catch: readEntry() throw error when file not exists
            }
            try {
              zipObject.readEntry();
              zipObject.on('entry', function (entry) {
                var dest;
                dest = `${dstPath}/${entry.fileName}`;
                if (/\/$/.test(dest)) {
                  return mkdirp(dest, function (err) {
                    if (err) {
                      reject(err);
                    }
                    return zipObject.readEntry();
                  });
                } else {
                  currentSize = currentSize + entry.uncompressedSize;
                  if (maxSize != null && currentSize > maxSize) {
                    reject(new Error(`Processing zip file ${zipFile}: Max size (${maxSize}) excedeed`));
                    return;
                  }
                  return zipObject.openReadStream(entry, function (err, readStream) {
                    if (err) {
                      reject(err);
                    }
                    return mkdirp(path.dirname(dest), function (err) {
                      var mode, writeStream;
                      if (err) {
                        reject(err);
                      }
                      mode = (entry.externalFileAttributes >> 16 & 0xfff).toString(8);
                      writeStream = fs.createWriteStream(dest, {
                        mode: mode
                      });
                      writeStream.on('close', function () {
                        return zipObject.readEntry();
                      });
                      readStream.on('error', function (err) {
                        return reject(new Error(`Processing zip file ${zipFile}: ${err}`));
                      });
                      writeStream.on('error', function (err) {
                        return reject(new Error(`Processing zip file ${zipFile}: ${err}`));
                      });
                      return readStream.pipe(writeStream);
                    });
                  });
                }
              });
              zipObject.on('end', function () {
                zipObject.close();
                return resolve(currentSize);
              });
              return zipObject.on('error', function (err) {
                return reject(new Error(`Processing zip file ${zipFile}: ${err}`));
              });
            } catch (error1) {
              e = error1;
              return reject(new Error(`Processing zip file ${zipFile}: ${e}`));
            }
          });
        } catch (error1) {
          e = error1;
          return reject(new Error(`Opening zip file ${zipFile}: ${e}`));
        }
      }).fail(function (err) {
        return reject(err);
      });
    });
  };

  // Create a zip file with the contents of the given directory
  zipDir = function (srcPath, zipFile) {
    return q.promise(function (resolve, reject) {
      return pstat(srcPath).then(function (status) {
        var addedSomething, getDirOptions;
        addedSomething = false;
        getDirOptions = {
          relative: false,
          recursive: true
        };
        return getDirContents(srcPath, getDirOptions);
      }).then(function (listResult) {
        var addedSomething, d, e, f, i, j, k, len1, len2, ref, ref1, relative, ws, zip;
        try {
          zip = new yazl.ZipFile();
          ref = listResult.files;
          for (i = j = 0, len1 = ref.length; j < len1; i = ++j) {
            f = ref[i];
            relative = path.relative(srcPath, f);
            zip.addFile(f, relative);
            addedSomething = true;
          }
          ref1 = listResult.emptyDirs;
          for (i = k = 0, len2 = ref1.length; k < len2; i = ++k) {
            d = ref1[i];
            relative = path.relative(srcPath, d);
            zip.addEmptyDirectory(relative);
            addedSomething = true;
          }
          if (addedSomething) {
            ws = fs.createWriteStream(zipFile);
            zip.outputStream.pipe(ws).on('close', function () {
              return resolve(zipFile);
            });
            return zip.end();
          } else {
            throw new Error(`Directory ${srcPath} is empty.`);
          }
        } catch (error1) {
          e = error1;
          return reject(new Error(`Creating zip file ${zipFile}: ${e.message}`));
        }
      }).fail(function (err) {
        return reject(err);
      });
    });
  };

  // Create a zip file from a single file (comress the file). If no zip file is
  // provided, the original name will be used, replacing the extension with '.zip'.
  zipFile = function (srcFile, zipFile = null) {
    return q.promise(function (resolve, reject) {
      return pstat(srcFile).then(function (fileStats) {
        var basename, e, extension, regex, ws, zip;
        if (fileStats.isFile()) {
          try {
            zip = new yazl.ZipFile();
            basename = path.basename(srcFile);
            zip.addFile(srcFile, basename);
            if (zipFile == null) {
              extension = path.extname(srcFile);
              regex = new RegExp(extension + '$');
              zipFile = srcFile.replace(regex, '.zip');
            }
            ws = fs.createWriteStream(zipFile);
            zip.outputStream.pipe(ws).on('close', function () {
              return resolve(zipFile);
            });
            return zip.end();
          } catch (error1) {
            e = error1;
            return reject(new Error(`Creating zip file ${zipFile}: ${e.message}`));
          }
        } else {
          return reject(new Error(`Expected a file not a directory (${srcFile})`));
        }
      }).fail(function (err) {
        return reject(err);
      });
    });
  };

  // Pack and compress a directory to a file. Returns a promise.
  packDirectory = function (srcPath, dstPath) {
    return q.promise(function (resolve, reject) {
      return pstat(srcPath).then(function (status) {
        var gzip, out;
        gzip = zlib.createGzip();
        out = fs.createWriteStream(dstPath);
        tar.pack(srcPath).on('error', function (error) {
          return reject(error);
        }).pipe(gzip).pipe(out);
        out.on('finish', function () {
          return resolve(dstPath);
        });
        return out.on('error', function (error) {
          return reject(error);
        });
      }).fail(function (err) {
        return reject(err);
      });
    });
  };

  // Compress a string using gzip and base64 encode the result
  gzipString = function (str) {
    var opts;
    // Use max compression level
    opts = {
      level: 9
    };
    return zlib.gzipSync(str, opts).toString('base64');
  };

  // Decompress a base64 encoded gzipped string
  gunzipString = function (str) {
    return zlib.gunzipSync(Buffer.from(str, 'base64'));
  };

  //###############################################################################
  //#                             STRING UTILITIES                               ##
  //###############################################################################

  // Replace all matches of a RegExp in a string with another string.
  replacePattern = function (str, pattern, value) {
    return str.replace(new RegExp(pattern, 'g'), value);
  };

  // Replace all matches of a RegExp in a JSON object with a string, using
  // object stringification.
  replacePatternInJSON = function (jsonObj, pattern, value) {
    return JSON.parse(replacePattern(JSON.stringify(jsonObj), pattern, value));
  };

  // Checks if a string starts with another string
  startsWith = function (str, prefix) {
    return str.slice(0, +(prefix.length - 1) + 1 || 9e9) === prefix;
  };

  // Checks if a string ends with another string
  endsWith = function (str, suffix) {
    return str.slice(-suffix.length) === suffix;
  };

  //###############################################################################
  //#                              ARRAY UTILITIES                               ##
  //###############################################################################

  // WARNING: Always remember that arrays of Objects always work by reference!

  // Example:
  //   a = {obj: 0}
  //   b = [a]
  //   b.indexOf {obj: 0}      <-------  will return -1 (not found)

  // Returns an array containing the elements that exist in both a and b
  arrayIntersection = function (a, b) {
    var j, len1, results, value;
    if (a.length > b.length) {
      [a, b] = [b, a];
    }
    results = [];
    for (j = 0, len1 = a.length; j < len1; j++) {
      value = a[j];
      if (indexOf.call(b, value) >= 0) {
        results.push(value);
      }
    }
    return results;
  };

  // Returns an array containing the elements that exist EXCLUSIVELY in a or b
  arrayDifference = function (a, b) {
    return a.concat(b).filter(function (i) {
      return a.indexOf(i) < 0 || b.indexOf(i) < 0;
    });
  };

  // Returns an array containing the elements of a and b combined.
  // A flag is available to allow or remove duplicates in the result.
  arrayMerge = function (a, b, allowDuplicates = false) {
    var j, key, m, output, ref, results, value;
    m = a.concat(b);
    if (allowDuplicates) {
      return m;
    } else {
      output = {};
      for (key = j = 0, ref = m.length; 0 <= ref ? j < ref : j > ref; key = 0 <= ref ? ++j : --j) {
        output[m[key]] = m[key];
      }
      results = [];
      for (key in output) {
        value = output[key];
        results.push(value);
      }
      return results;
    }
  };

  // Compares two arrays
  arrayEqual = function (a, b) {
    return a.length === b.length && a.every(function (elem, i) {
      return elem === b[i];
    });
  };

  //###############################################################################
  //#                           HOTCHPOTCH UTILITIES                             ##
  //###############################################################################

  // Generates a random ID
  generateId = function () {
    return uuid.v4();
  };

  // Returns a random integer within the specified range
  randomInt = function (low, high) {
    return Math.floor(Math.random() * (high - low) + low);
  };

  // Returns a random hexadecimal string of the specified length
  randomHex = function (len) {
    return crypto.randomBytes(Math.ceil(len / 2)).toString('hex').slice(0, len);
  };

  // Returns a SHA1 hash in hex format from the provided string
  getHash = function (str) {
    return crypto.createHash('sha1').update(str).digest('hex');
  };

  // Returns a FNV-1 hash in hex format (of length 8) from the provided string
  getFnvHash = function (str) {
    var hashed;
    hashed = fnv1Hash(str);
    return hashed.toString(16).padStart(8, "0");
  };

  // Calculates a 32 bit FNV-1 hash (an unsigned 32 bit integer)
  fnv1Hash = str => {
    var OFFSET_BASIS, data, hashint, i, j, ref;
    // The seed, 32 bit offset_basis = 2,166,136,261 = 0x811C9DC5
    OFFSET_BASIS = 2166136261;
    data = new Buffer(str);
    hashint = OFFSET_BASIS;
    for (i = j = 0, ref = data.length - 1; 0 <= ref ? j <= ref : j >= ref; i = 0 <= ref ? ++j : --j) {
      hashint += (hashint << 1) + (hashint << 4) + (hashint << 7) + (hashint << 8) + (hashint << 24);
      hashint = hashint ^ data[i];
    }
    // Return an unsigned 32 bit integer
    return hashint >>> 0;
  };

  // Calculates a 32 bit FNV-1a hash (an unsigned 32 bit integer)
  fnv1aHash = str => {
    var OFFSET_BASIS, data, hashint, i, j, ref;
    // The seed, 32 bit offset_basis = 2,166,136,261 = 0x811C9DC5
    OFFSET_BASIS = 2166136261;
    data = new Buffer(str);
    hashint = OFFSET_BASIS;
    for (i = j = 0, ref = data.length - 1; 0 <= ref ? j <= ref : j >= ref; i = 0 <= ref ? ++j : --j) {
      hashint = hashint ^ data[i];
      hashint += (hashint << 1) + (hashint << 4) + (hashint << 7) + (hashint << 8) + (hashint << 24);
    }
    // Return an unsigned 32 bit integer
    return hashint >>> 0;
  };

  calculateChecksum = function (filename) {
    return q.promise(function (resolve, reject) {
      var hash, stream;
      hash = crypto.createHash('sha1');
      stream = fs.createReadStream(filename);
      stream.on('data', function (data) {
        return hash.update(data);
      });
      stream.on('end', function () {
        var checksum;
        checksum = hash.digest('hex');
        return resolve(checksum);
      });
      return stream.on('error', function (error) {
        return reject(error);
      });
    });
  };

  // Returns a brief humand readable timestamp string, usually used as suffix for
  // file names.
  getDateSuffix = function () {
    var d, s;
    d = new Date();
    s = d.getFullYear();
    s += ('0' + (d.getMonth() + 1)).slice(-2);
    s += ('0' + d.getDate()).slice(-2);
    s += '_' + ('0' + d.getHours()).slice(-2);
    s += ('0' + d.getMinutes()).slice(-2);
    return s += ('0' + d.getSeconds()).slice(-2);
  };

  // Returns a timestamp string in the format "2022-01-26T18:50:22Z"
  getTimestamp = function (format = null) {
    var now;
    now = new Date().getTime();
    if (format != null && format !== '') {
      return moment.utc(now).format(format);
    } else {
      // format() with no specific format returns a string representation in the
      // ISO8601 format: YYYY-MM-DDTHH:mm:ssZ
      return moment.utc(now).format();
    }
  };

  isObject = function (a) {
    return !!a && a.constructor === Object;
  };

  isString = function (val) {
    return typeof val === 'string' || !!val && typeof val === 'object' && Object.prototype.toString.call(val) === '[object String]';
  };

  isArray = function (val) {
    return val.constructor === Array;
  };

  // Adds properties to object
  extendObject = function (obj, properties) {
    var key, val;
    for (key in properties) {
      val = properties[key];
      obj[key] = val;
    }
    return obj;
  };

  validateDomainName = function (domainName) {
    return DOMAIN_REGEX.test(domainName);
  };

  exports.jsonFromFile = jsonFromFile;

  exports.jsonStringFromFile = jsonStringFromFile;

  exports.arrayEqual = arrayEqual;

  exports.calculateChecksum = calculateChecksum;

  exports.saveFile = saveFile;

  exports.deleteFile = deleteFile;

  exports.renameFile = renameFile;

  exports.copyFile = copyFile;

  exports.checkFileExists = checkFileExists;

  exports.getDirContents = getDirContents;

  exports.getDirSize = getDirSize;

  exports.zstd = zstd;

  exports.unzstd = unzstd;

  exports.tarzstd = tarzstd;

  exports.untarzstd = untarzstd;

  exports.untgz = untgz;

  exports.unzip = unzip;

  exports.zipDir = zipDir;

  exports.zipFile = zipFile;

  exports.packDirectory = packDirectory;

  exports.replacePattern = replacePattern;

  exports.replacePatternInJSON = replacePatternInJSON;

  exports.startsWith = startsWith;

  exports.endsWith = endsWith;

  exports.randomInt = randomInt;

  exports.randomHex = randomHex;

  exports.getHash = getHash;

  exports.getFnvHash = getFnvHash;

  exports.getDateSuffix = getDateSuffix;

  exports.getTimestamp = getTimestamp;

  exports.isString = isString;

  exports.isObject = isObject;

  exports.isArray = isArray;

  exports.extendObject = extendObject;

  exports.arrayIntersection = arrayIntersection;

  exports.arrayDifference = arrayDifference;

  exports.arrayMerge = arrayMerge;

  exports.generateId = generateId;

  exports.gzipString = gzipString;

  exports.gunzipString = gunzipString;

  exports.validateDomainName = validateDomainName;

  exports.yamlParse = yamlParse;

  exports.yamlStringify = yamlStringify;
}).call(undefined);