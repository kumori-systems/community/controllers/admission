###
* Copyright 2022 Kumori Systems S.L.

* Licensed under the EUPL, Version 1.2 or – as soon they
  will be approved by the European Commission - subsequent
  versions of the EUPL (the "Licence");

* You may not use this work except in compliance with the
  Licence.

* You may obtain a copy of the Licence at:

  https://joinup.ec.europa.eu/software/page/eupl

* Unless required by applicable law or agreed to in
  writing, software distributed under the Licence is
  distributed on an "AS IS" basis,

* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
  express or implied.

* See the Licence for the specific language governing
  permissions and limitations under the Licence.
###

q           = require 'q'

DockerRegistryProxy = require '../src/docker-registry-proxy'


drp = new DockerRegistryProxy {} 


testImages =
  imageRegK8s:
    hub:
      name: "registry.k8s.io"
      secret: ""
    tag: "git-sync/git-sync:v3.6.5"
  imageDockerHubTemporalioBuildX:
    hub:
      name: ""
      secret: ""
    tag: "temporalio/admin-tools:1.20.1"


testImages2 =
  imageDockerHub:
    hub:
      name: "registry.hub.docker.com"
      secret: ""
    tag: "kumori/calccachecache:0.0.1"
  imageDockerHubPrivate:
    hub:
      name: "registry.hub.docker.com"
      secret: "dockerhub-kumori"
    tag: "kumori/spekt8:custom"
  imageDockerHubNETag:
    hub:
      name: "registry.hub.docker.com"
      secret: ""
    tag: "kumori/calccachecache:0.0.144"
  imageDockerHubNETag:
    hub:
      name: "registry.hub.docker.com"
      secret: ""
    tag: "kumori/calccachecache:0.0.144"
  imageDockerHubNE:
    hub:
      name: "registry.hub.docker.com"
      secret: ""
    tag: "fumorizzzz/calc:0.0.1"
  imageDockerHubNEDeep:
    hub:
      name: "registry.hub.docker.com"
      secret: ""
    tag: "fumorizzzz/sub/calc:0.0.1"
  imageDockerHubNoNamespace:
    hub:
      name: "registry.hub.docker.com"
      secret: ""
    tag: "calc:0.0.1"
  imageDockerHubExplicitLibrary:
    hub:
      name: "registry.hub.docker.com"
      secret: ""
    tag: "library/ubuntu:20.10"
  imageDockerIO:
    hub:
      name: "docker.io"
      secret: ""
    tag: "kumori/calccachecache:0.0.1"
  imageDockerIONETag:
    hub:
      name: "docker.io"
      secret: ""
    tag: "kumori/calccachecache:0.0.144"
  imageDockerIONETag:
    hub:
      name: "docker.io"
      secret: ""
    tag: "kumori/calccachecache:0.0.144"
  imageDockerIONE:
    hub:
      name: "docker.io"
      secret: ""
    tag: "fumorizzzz/calc:0.0.1"
  imageDockerIONEDeep:
    hub:
      name: "docker.io"
      secret: ""
    tag: "fumorizzzz/sub/calc:0.0.1"
  imageDockerIONoNamespace:
    hub:
      name: "docker.io"
      secret: ""
    tag: "calc:0.0.1"
  imageDockerIOExplicitLibrary:
    hub:
      name: "docker.io"
      secret: ""
    tag: "library/ubuntu:20.10"
  imageNoHub:
    hub:
      name: ""
      secret: ""
    tag: "kumori/calccachecache:0.0.1"
  imageNoHubNETag:
    hub:
      name: ""
      secret: ""
    tag: "kumori/calccachecache:0.0.144"
  imageNoHubNETag:
    hub:
      name: ""
      secret: ""
    tag: "kumori/calccachecache:0.0.144"
  imageNoHubNE:
    hub:
      name: ""
      secret: ""
    tag: "fumorizzzz/calc:0.0.1"
  imageNoHubNEDeep:
    hub:
      name: ""
      secret: ""
    tag: "fumorizzzz/sub/calc:0.0.1"
  imageNoHubNoNamespace:
    hub:
      name: ""
      secret: ""
    tag: "calc:0.0.1"
  imageNoHubExplicitLibrary:
    hub:
      name: ""
      secret: ""
    tag: "library/ubuntu:20.10"
  imageGitLab:
    hub:
      name: "registry.gitlab.com"
      secret: "kumorikv3"
    tag: "kumori/kv3/admission:v0.2.8"
  imageGitLabNEDeep:
    hub:
      name: "registry.gitlab.com"
      secret: "kumorikv3"
    tag: "fumorizzzz/sub/sub2/sub3/calc:v0.0.12"




imageDockerHub =
  registryURL: 'registry.hub.docker.com'
  imageName:   'kumori/calccachecache'
  imageRef:    '0.0.1'
  # imageRef:    '0.0.144'
  username:    ''
  password:    ''

imageGitLab =
  registryURL: 'registry.gitlab.com'
  imageName:   'kumori/kv3/admission'
  # imageRef:    '0.0.1'
  imageRef:    'v0.2.1'
  username:    'kumorikv3'
  password:    '<put-a-valid-token-here>'

imageDockerHubNE =
  registryURL: 'registry.hub.docker.com'
  imageName:   'fumorizzzz/calc'
  imageRef:    '0.0.12'
  # imageRef:    '0.0.144'
  username:    ''
  password:    ''

imageDockerHubNEDeep =
  registryURL: 'registry.hub.docker.com'
  imageName:   'fumorizzzz/sub/calc'
  imageRef:    '0.0.12'
  # imageRef:    '0.0.144'
  username:    ''
  password:    ''

imageGitLabNEDeep =
  registryURL: 'registry.gitlab.com'
  imageName:   'fumorizzzz/sub/sub2/sub3/calc'
  imageRef:    '0.0.12'
  # imageRef:    '0.0.144'
  username:    ''
  password:    ''

# EDICOM : registry.hub.docker.com/sys/apps/containers/kubernetes/hazelcast:oss-3.12.6
imageDockerHubEdicomError =
  registryURL: 'registry.hub.docker.com'
  imageName:   '/sys/apps/containers/kubernetes/hazelcast'
  imageRef:    'oss-3.12.6'
  # imageRef:    '0.0.144'
  username:    ''
  password:    ''

imageDockerIO =
  registryURL: 'docker.io'
  # registryURL: 'registry.docker.io'
  imageName:   'kumori/calccachecache'
  imageRef:    '0.0.1'
  # imageRef:    '0.0.144'
  username:    ''
  password:    ''

imageDockerIONoNamespace =
  registryURL: 'docker.io'
  imageName:   'library/ubuntu'
  imageRef:    '20.10'
  # imageRef:    '0.0.144'
  username:    ''
  password:    ''

imageDockerIONoNamespace =
  registryURL: 'docker.io'
  imageName:   'library/ubuntu'
  imageRef:    '20.10'
  # imageRef:    '0.0.144'
  username:    ''
  password:    ''

imageDockerIONE =
  registryURL: 'docker.io'
  imageName:   'fumorizzzz/calc'
  imageRef:    '0.0.12'
  # imageRef:    '0.0.144'
  username:    ''
  password:    ''

imageDockerIONEDeep =
  registryURL: 'docker.io'
  imageName:   'fumorizzzz/sub/calc'
  imageRef:    '0.0.12'
  # imageRef:    '0.0.144'
  username:    ''
  password:    ''

# i = imageDockerHub
# i = imageGitLab
# i = imageDockerHubNE
# i = imageDockerHubNEDeep
# i = imageDockerHubEdicomError
# i = imageGitLabNEDeep
i = imageDockerIO
# i = imageDockerIONoNamespace


testNormalize = (img) ->
  norm = drp.normalizeDockerImage img
  console.log "NORMALIZED: #{JSON.stringify norm, null, 2}"
  console.log "NORMALIZED hub    : #{norm.hub}"
  console.log "NORMALIZED secret : #{norm.secret}"
  console.log "NORMALIZED name   : #{norm.name}"
  console.log "NORMALIZED ref    : #{norm.ref}"
  norm



testComplete = () ->
  promiseQueue = q()

  for imgId, imgData of testImages
    # Uncomment next line to only test images with secrets
    # continue if imgData.hub.secret is ''
    do (imgId, imgData) =>
      promiseQueue = promiseQueue.then () =>
        console.log "****************************************************************"
        console.log "----------------------------------------------------------------"
        console.log "IMAGE DATA: #{JSON.stringify imgData, null, 2}"
        norm = testNormalize imgData
        console.log "----------------------------------------------------------------"

        if norm.secret is 'kumorikv3'
          username = 'kumorikv3'
          password = '<put a valid token here>'
        else if norm.secret is 'dockerhub-kumori'
          username = 'kumori'
          password = '<put a valid token here>'
        else
          username = ''
          password = ''

        drp.checkImage norm.hub, norm.name, norm.ref, username, password
      .then (exists) ->
        if exists
          console.log "\n--> IMAGE EXISTS!"
        else
          console.log "\n--> IMAGE DOESN'T EXIST!"
        console.log "****************************************************************"
      .catch (err) ->
        console.log "\n\n"
        console.log "CATCH: #{err.message}"
        console.log "****************************************************************"

  return promiseQueue



testComplete()
.then () ->
  console.log ""
  console.log "FINISHED"
  console.log ""
.done()


# drp.checkImage i.registryURL, i.imageName, i.imageRef, i.username, i.password
# .then (exists) ->
#   console.log "\n\n"
#   console.log "IMAGE: #{i.registryURL}/#{i.imageName}/#{i.imageRef}"
#   console.log "CREDENTIALS: #{i.username} - #{i.password}"
#   if exists
#     console.log "\n--> IMAGE EXISTS!"
#   else
#     console.log "\n--> IMAGE DOESN'T EXIST!"
# .catch (err) ->
#   console.log "\n\n"
#   console.log "CATCH: #{err.message}"
# .done()
