# index = require '../src/index'
index = require '../src/index'

EventStoreElasticsearch = require '../src/event-stores/event-store-elasticsearch'

config =
  serverURL: 'http://elasticsearch-jferrer.test.kumori.cloud:9200/'
  username: ''
  password: ''
  index: 'kube-events-*'
  maxResults: 200


prettyPrintResults = (results) ->
  if results?.length is 0
    console.log "----------------------------------------------------------------------------------"
    console.log "No events found."
    console.log "----------------------------------------------------------------------------------"
  else
    for result, index in results
      console.log "----------------------------------------------------------------------------------"
      evt = result['_source']
      console.log "----------------------------------------------------------------------------------"
      console.log "Result #{index}"
      prettyPrintEvent evt
      console.log "----------------------------------------------------------------------------------"



prettyPrintEvent = (evt) ->
  console.log "- Creation timestamp:  #{evt.metadata?.creationTimestamp}"
  console.log "- First timestamp:     #{evt.firstTimestamp}"
  console.log "- Last timestamp:      #{evt.lastTimestamp}"
  console.log "- Event time:          #{evt.eventTime}"
  console.log "- Count:               #{evt.count}"
  if evt.source.host?
    console.log "- Emitted by:          #{evt.source.component} (#{evt.source.host})"
  else
    console.log "- Emitted by:          #{evt.source.component}"
  console.log "- Reporting component: #{evt.reportingComponent}"
  console.log "- Reporting instance:  #{evt.reportingInstance}"

  console.log "- Type:    #{evt.type}"
  console.log "- Reason:  #{evt.reason}"
  console.log "- Message: #{evt.message}"
  console.log "- Involved object:"
  console.log "  - Namespace: #{evt.involvedObject.namespace}"
  console.log "  - Kind:      #{evt.involvedObject.kind}"
  console.log "  - Name:      #{evt.involvedObject.name}"
  console.log "  - Labels:"
  for labelName, labelValue of evt.involvedObject.labels
    console.log "    - #{labelName}: #{labelValue}"




ese = new EventStoreElasticsearch config

ese.init()
.then () =>
  ese.getDeploymentEvents null, "kd-090509-4360d258", null, null
.then (results) =>
  # console.log "RESULTS: #{JSON.stringify results}"
  prettyPrintResults results
.fail (err) =>
  console.log "ERROR: #{err.message}"
.done()

