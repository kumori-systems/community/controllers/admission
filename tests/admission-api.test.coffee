###
* Copyright 2022 Kumori Systems S.L.

* Licensed under the EUPL, Version 1.2 or – as soon they
  will be approved by the European Commission - subsequent
  versions of the EUPL (the "Licence");

* You may not use this work except in compliance with the
  Licence.

* You may obtain a copy of the Licence at:

  https://joinup.ec.europa.eu/software/page/eupl

* Unless required by applicable law or agreed to in
  writing, software distributed under the Licence is
  distributed on an "AS IS" basis,

* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
  express or implied.

* See the Licence for the specific language governing
  permissions and limitations under the Licence.
###

q            = require 'q'
fs           = require 'fs'
path         = require 'path'
mkdirp       = require 'mkdirp'
rimraf       = require 'rimraf'
moment       = require 'moment'
multer       = require 'multer'
express      = require 'express'
supertest    = require 'supertest'
bodyParser   = require 'body-parser'
EventEmitter = require('events').EventEmitter
extensions   = require './expect-extensions'
http         = require 'http'

index     = require '..'
klogger   = require 'k-logger'
kutils    = require 'k-utils'

ComponentRegistry          = require '../lib/component-registry'
ManagedComponentRegistry   = require '../lib/managed-component-registry'
UnmanagedComponentRegistry = require '../lib/unmanaged-component-registry'

expect.extend extensions

AdmissionRestAPI = index.AdmissionRestAPI

logger = null

ADMISSION_CFG       = path.join __dirname, 'config/admission.json'
LIMITS_CFG          = path.join __dirname, 'config/limits.json'
FILES_PATH          = path.join __dirname, 'files'
ADMISSION_FILES     = path.join FILES_PATH, 'admission-tests'
# REMOTE_STORAGE_ROOT = path.join FILES_PATH, 'remoteStorageForListing/remote'
# LOCAL_STORAGE_ROOT  = path.join FILES_PATH, 'remoteStorageForListing/local'
MANIFEST_STORE_REMOTE = path.join __dirname, 'manifest-storage/remote'
MANIFEST_STORE_LOCAL  = path.join __dirname, 'manifest-storage/local'
IMAGE_STORE_REMOTE    = path.join __dirname, 'image-storage/remote'
IMAGE_STORE_LOCAL     = path.join __dirname, 'image-storage/local'

BUNDLES_DIR                = "#{FILES_PATH}/bundles"
BUNDLES_JSON_FILE          = "#{BUNDLES_DIR}/Bundles.json"
COMPONENT_BUNDLE_ZIP_FILE  = "#{BUNDLES_DIR}/component_bundle_cfe.zip"
COMPONENT_DOCKER_ZIP_FILE  = "#{BUNDLES_DIR}/component_bundle_cfe_docker.zip"
INEXISTENT_BUNDLE_ZIP_FILE = "#{BUNDLES_DIR}/idontexist.zip"
DEPLOYMENT_BUNDLE_ZIP_FILE = "#{BUNDLES_DIR}/deploy_bundle.zip"
LINK_BUNDLE_ZIP_FILE       = "#{BUNDLES_DIR}/bundle_with_link.zip"
TEST_BUNDLE_ZIP_FILE       = "#{BUNDLES_DIR}/test_bundle.zip"
SPREAD_ZIP_BUNDLE          = "#{FILES_PATH}/spread/deploy_bundle.zip"

ZIPBOMB_DEPLOYMENT_BUNDLE_ZIP_FILE = "#{BUNDLES_DIR}/zipbomb_deploy_bundle.zip"


RUNTIME_PREFIX_MANAGED   = 'slap://slapdomain/runtimes/managed'
RUNTIME_PREFIX_UNMANAGED = 'slap://slapdomain/runtimes/unmanaged'
RUNTIME_PREFIX_DELTA     = 'slap://slapdomain/runtimes/delta'

admCfg = require ADMISSION_CFG
admCfg.imageFetcher.config.remoteImageStore.path = IMAGE_STORE_REMOTE
admCfg.imageFetcher.config.localImageStore.path  = IMAGE_STORE_LOCAL
admCfg.manifestRepository.config.remoteImageStore.path = MANIFEST_STORE_REMOTE
admCfg.manifestRepository.config.localImageStore.path  = MANIFEST_STORE_LOCAL
admCfg.tempPath = TMP_PATH
limitsCfg = require LIMITS_CFG
admCfg.limitChecker = limitsCfg

TMP_PATH = "#{__dirname}/tmp"
FAKE_UPLOADS_PATH = "#{TMP_PATH}/uploads"


HTTPSEP_0_0_1_DIR =
  path.join MANIFEST_STORE_REMOTE, 'slapdomain/components/httpsep/0_0_1'
HTTPSEP_0_0_1_MANIFEST_STORAGE =
  path.join HTTPSEP_0_0_1_DIR, 'manifest.json'
HTTPSEP_0_0_1_MANIFEST =
  path.join FILES_PATH, 'sep-files/httpsep_0_0_1_manifest.json'

HTTPSEP_0_0_1_IMAGE_DIR =
  path.join IMAGE_STORE_REMOTE, 'slapdomain/components/httpsep/0_0_1'
HTTPSEP_0_0_1_IMAGE_STORAGE =
  path.join HTTPSEP_0_0_1_IMAGE_DIR, 'image.tgz'
HTTPSEP_0_0_1_IMAGE =
  path.join FILES_PATH, 'sep-files/httpsep_0_0_1_image.tgz'

HTTPINBOUND_1_0_0_DIR =
  path.join MANIFEST_STORE_REMOTE, 'eslap.cloud/services/http/inbound/1_0_0'
HTTPINBOUND_1_0_0_MANIFEST_STORAGE =
  path.join HTTPINBOUND_1_0_0_DIR, 'manifest.json'
HTTPINBOUND_1_0_0_MANIFEST =
  path.join FILES_PATH, 'sep-files/httpinbound_1_0_0_manifest.json'

NATIVE_RUNTIME_0_0_1_DIR =
  path.join MANIFEST_STORE_REMOTE, 'slapdomain/runtimes/managed/nodejs/0_0_1'
NATIVE_RUNTIME_0_0_1_MANIFEST_STORAGE =
  path.join NATIVE_RUNTIME_0_0_1_DIR, 'manifest.json'
NATIVE_RUNTIME_0_0_1_MANIFEST =
  path.join FILES_PATH, 'runtime-files/native_nodejs_0_0_1_manifest.json'


NATIVE_RUNTIME_1_0_0_DIR =
  path.join MANIFEST_STORE_REMOTE, 'eslap.cloud/runtime/native/1_0_0'
NATIVE_RUNTIME_1_0_0_MANIFEST_STORAGE =
  path.join NATIVE_RUNTIME_1_0_0_DIR, 'manifest.json'
NATIVE_RUNTIME_1_0_0_MANIFEST =
  path.join FILES_PATH, 'runtime-files/native_nodejs_1_0_0_manifest.json'


HTTP_ENTRYPOINT_FULL_DEPLOYMENT_DIR =
  path.join MANIFEST_STORE_REMOTE
  , 'eslap.cloud/deployments/20160613_082226/58c5cda4'
HTTP_ENTRYPOINT_FULL_DEPLOYMENT_MANIFEST_STORAGE =
  path.join HTTP_ENTRYPOINT_FULL_DEPLOYMENT_DIR, 'manifest.json'
HTTP_ENTRYPOINT_FULL_DEPLOYMENT_MANIFEST =
  path.join ADMISSION_FILES, 'httpep_deployment_manifest.json'

SERVICE_LINK_FILE = path.join ADMISSION_FILES, 'service_link_manifest.json'
SERVICE_LINK_FILE_WITH_ERROR =
  path.join ADMISSION_FILES, 'service_link_manifest_missing_one.json'
SERVICE_LINK_FILE_WITH_ERROR2 =
  path.join ADMISSION_FILES, 'service_link_manifest_wrong_urn.json'

RESOURCE_VHOST_DIR =
  path.join MANIFEST_STORE_REMOTE
  , 'sampleservicecalculator/resources/vhost/www_mesie_com'
RESOURCE_VHOST_MANIFEST_STORAGE =
  path.join RESOURCE_VHOST_DIR, 'manifest.json'
RESOURCE_VHOST_MANIFEST =
  path.join FILES_PATH, 'spread/resources/vhost/Manifest.json'

RESOURCE_PERSISTENT_DIR =
  path.join MANIFEST_STORE_REMOTE
  , 'sampleservicecalculator/resources/volumes/persistent'
RESOURCE_PERSISTENT_MANIFEST_STORAGE =
  path.join RESOURCE_PERSISTENT_DIR, 'manifest.json'
RESOURCE_PERSISTENT_MANIFEST =
  path.join FILES_PATH, 'spread/resources/persistent_volume/Manifest.json'


# spread/resources/vhost/Manifest.json

used_folders = [
  TMP_PATH
  FAKE_UPLOADS_PATH
  IMAGE_STORE_REMOTE
  IMAGE_STORE_LOCAL
  MANIFEST_STORE_REMOTE
  MANIFEST_STORE_LOCAL
  HTTPSEP_0_0_1_DIR
  HTTPSEP_0_0_1_IMAGE_DIR
  HTTPINBOUND_1_0_0_DIR
  NATIVE_RUNTIME_0_0_1_DIR
  NATIVE_RUNTIME_1_0_0_DIR
  HTTP_ENTRYPOINT_FULL_DEPLOYMENT_DIR
  RESOURCE_VHOST_DIR
  RESOURCE_PERSISTENT_DIR
]

SERVICE_SPEC_1_0_0 = 'http://eslap.cloud/manifest/deployment/1_0_0'

# Used for reconfigDeploy test.
# Info (in Plantaco) about an existing deploy
MANIFIESTACO =
    name: 'slap://mydomain/deployments/111/222',
    servicename: 'slap://mydomain/services/myapplication/0_0_1',
    configuration:
      resources: {}
      parameters:
        cfe:
          sample_parameter: 'deploy_value_1'
          default_parameter: 'custom_value_1'
        front:
          sample_parameter: 'deploy_value_2'
          sample_json: data: 'thedata'
    roles: cfe: {}
    service:
      roles: [
        {name: 'cfe',component: 'slap://mydomain/components/cfe/0_0_1'}
        {name: 'front',component: 'slap://mydomain/components/front/0_0_1'}
      ]
      configuration:
        resources: []
        parameters: [
          {name: "cfe",type: "eslap://eslap.cloud/parameter/json/1_0_0"}
          {name: "front",type: "eslap://eslap.cloud/parameter/json/1_0_0"}
        ]
    components:
      'slap://mydomain/components/cfe/0_0_1':
        name: 'slap://mydomain/components/cfe/0_0_1'
        runtime: 'eslap://eslap.cloud/runtime/native/1_0_1'
        configuration:
          resources: []
          parameters: [{
              name: "sample_parameter"
              type: "eslap://eslap.cloud/parameter/string/1_0_0"
          },{
              name: "default_parameter"
              type: "eslap://eslap.cloud/parameter/string/1_0_0"
              default: "default_value_1"
          }]
      'slap://mydomain/components/front/0_0_1':
        name: 'slap://mydomain/components/cfe/0_0_1'
        runtime: 'eslap://eslap.cloud/runtime/native/1_0_1'
        configuration:
          resources: []
          parameters: [{
              name: "sample_parameter"
              type: "eslap://eslap.cloud/parameter/string/1_0_0"
          },{
              name: "sample_json"
              type: "eslap://eslap.cloud/parameter/json/1_0_0"
          }]

MANIFIESTACO =
  manifest:
    name: 'slap://mydomain/deployments/111/222'
    versions:
      'http://eslap.cloud/manifest/deployment/1_0_0': MANIFIESTACO

DEPLOYMENT_SPEC_1_0_0 = 'http://eslap.cloud/manifest/deployment/1_0_0'
MANIFIESTACO_LINK1 =
  manifest:
    name: 'slap://eslap.cloud/deployments/20160613_082226/123456'
    versions: {}

MANIFIESTACO_LINK2 =
  manifest:
    name: 'slap://eslap.cloud/deployments/20160613_082226/58c5cda4'
    versions: {}

MANIFIESTACO_LINK1.manifest.versions[DEPLOYMENT_SPEC_1_0_0] =
  name: 'slap://eslap.cloud/deployments/20160613_082226/123456'
  service:
    spec: 'http://eslap.cloud/manifest/service/1_0_0'
    channels:
      provides: [{
        name: 'service',
        type: 'eslap://eslap.cloud/endpoint/reply/1_0_0',
        protocol: 'eslap://eslap.cloud/protocol/message/http/1_0_0'
      }],
      requires: []

MANIFIESTACO_LINK2.manifest.versions[DEPLOYMENT_SPEC_1_0_0] =
  name: 'slap://eslap.cloud/deployments/20160613_082226/58c5cda4'
  service:
    spec: 'http://eslap.cloud/manifest/service/1_0_0'
    channels:
      requires: [{
        name: 'frontend',
        type: 'eslap://eslap.cloud/endpoint/request/1_0_0',
        protocol: 'eslap://eslap.cloud/protocol/message/http/1_0_0'
      }],
      provides: []


EXPECTED_CALCULATED_DEPLOYMENT_RECONFIG = {
  action: 'reconfig',
  'components-configuration': {
    __service:
      cfe: {
        sample_parameter: 'reconfigured_value_1'
      }
      front: {
        sample_parameter: 'deploy_value_2',
        sample_json: { moredata: 'moredata' }
      }
    cfe: {
      sample_parameter: 'reconfigured_value_1'
      default_parameter: 'default_value_1'
    }
    front: {
      sample_parameter: 'deploy_value_2',
      sample_json: { moredata: 'moredata' }
    }
  },
  'components-resources': { __service: {}, cfe: {}, front: {} },
  deploymentUrn: 'slap://mydomain/deployments/111/222'
}

FAKE_STATE =
  deployedServices: {}
  deployedInstances: {}
  allocatedNodes: {}
  linkedServices: []


ERROR_MSG_LINK_MISSING_SERVICE =
  'ERROR LINKING SERVICES: Wrong number of endpoints: 1'
ERROR_MSG_UNLINK_NOT_IMPLMENTED =
  'ERROR UNLINKING SERVICES: Service unlink is not implemented yet.'

ADMISSION_EVENT_LIST = [
  'event1'
  'event2'
  'event3'
  'event4'
]

# Our restapi middleware
class MockPlanner extends EventEmitter
  constructor: () ->
    super()

  linkServices: (linkManifest) ->
    @emit 'planner', {
      timestamp: @_getTimeStamp
      owner: 'somebody'
      entity: 'blabla'
      type: 'service'
      name: 'link'
      data: linkManifest
    }
    q true

  unlinkServices: (linkManifest) ->
    @emit 'planner', {
      timestamp: @_getTimeStamp
      owner: 'somebody'
      entity: 'blabla'
      type: 'service'
      name: 'unlink'
      data: linkManifest
    }
    q true

  modifyDeploy: (params) ->
    switch params.action
      when 'reconfig'
        @calculatedDeploymentReconfig = params
        q.resolve()
      when 'manualScaling'
        q.resolve()
      else
        q.reject new Error "Invalid action #{params.action}"

  deploymentQuery: (params)->
    q {}


  deploymentInfoEx: (deploymentUrn) ->
    # URNs don't arrive encoded, since encoding is done in planner-stub
    if deploymentUrn is MANIFIESTACO_LINK1.manifest.name
      q.resolve MANIFIESTACO_LINK1
    else if deploymentUrn is MANIFIESTACO_LINK2.manifest.name
      q.resolve MANIFIESTACO_LINK2
    else if deploymentUrn is MANIFIESTACO.manifest.name
      q.resolve MANIFIESTACO
    else
      q.resolve {}

  execDeployment: () ->
    q.resolve {
      deploymentURN: 'my_deploymentURN'
      name: 'my_name'
    }

  listResourcesInUse: (filter) ->
    q.resolve {}

  getStampState: () ->
    q.resolve FAKE_STATE

  isElementInUse: () ->
    q.resolve {
      inUse: false
    }

  removeResource: (urn) ->
    q()

  _getTimeStamp: () ->
    now = new Date().getTime()
    moment.utc(now).format 'YYYYMMDDTHHmmssZ'



pstat   = q.denodeify fs.stat
pmkdirp = q.denodeify mkdirp
prmdir  = q.denodeify rimraf


class FakeHttpResponse

  @response = null
  @status = 400

  setTimeout: (millis, callback) ->
    setTimeout () ->
      console.log 'Fake HTTP response timeout fired!'
      callback()
    , millis

  status: (value) ->
    @status = value
    return this

  send: (response) ->
    @response = response

  json: (response) ->
    @response = response


componentRegistries = null

getComponentRegistry = (manifest) ->
  if not manifest? or not manifest.runtime?
    componentRegistries.default
  else if kutils.startsWith manifest.runtime, RUNTIME_PREFIX_MANAGED
    componentRegistries.managed
  else if kutils.startsWith manifest.runtime, RUNTIME_PREFIX_UNMANAGED
    componentRegistries.unmanaged
  else if kutils.startsWith manifest.runtime, RUNTIME_PREFIX_DELTA
    componentRegistries.unmanaged
  else
    componentRegistries.default


fakeUploadFile = (filename) ->
  extension = filename.split('.').pop()
  tmpName =
    path.join FAKE_UPLOADS_PATH, "upl_#{ kutils.generateId() }.#{extension}"
  kutils.copyFile filename, tmpName
  .then () ->
    return tmpName


describe 'Admission REST API', ->

  clearStorage = () ->
    promises = (prmdir f for f in used_folders)
    q.all promises
    .then ->
      promises = (pmkdirp f for f in used_folders)
      q.all promises
    .then ->
      kutils.copyFile HTTPSEP_0_0_1_MANIFEST, HTTPSEP_0_0_1_MANIFEST_STORAGE
    .then ->
      kutils.copyFile HTTPSEP_0_0_1_IMAGE, HTTPSEP_0_0_1_IMAGE_STORAGE
    .then ->
      kutils.copyFile HTTPINBOUND_1_0_0_MANIFEST
      , HTTPINBOUND_1_0_0_MANIFEST_STORAGE
    .then ->
      kutils.copyFile NATIVE_RUNTIME_0_0_1_MANIFEST
      , NATIVE_RUNTIME_0_0_1_MANIFEST_STORAGE
    .then ->
      kutils.copyFile NATIVE_RUNTIME_1_0_0_MANIFEST
      , NATIVE_RUNTIME_1_0_0_MANIFEST_STORAGE
    .then ->
      kutils.copyFile HTTP_ENTRYPOINT_FULL_DEPLOYMENT_MANIFEST
      , HTTP_ENTRYPOINT_FULL_DEPLOYMENT_MANIFEST_STORAGE
    # .then ->
    #   kutils.copyFile RESOURCE_VHOST_MANIFEST
    #   , RESOURCE_VHOST_MANIFEST_STORAGE

  managedComponentRegistry = new ManagedComponentRegistry \
    admCfg.imageFetcher
    , admCfg.manifestRepository
    , TMP_PATH
  unmanagedComponentRegistry = new UnmanagedComponentRegistry \
    admCfg.imageFetcher
    , admCfg.manifestRepository
    , TMP_PATH

  componentRegistries =
    managed: managedComponentRegistry
    unmanaged: unmanagedComponentRegistry
    default: managedComponentRegistry

  server = app = null
  admissionApi = null
  managementApi = null
  admissionRestApi = null
  port = kutils.randomInt 8090, 8190
  testToken = null
  keepStorageAfterTest = false
  mockPlanner = null
  receivedPlannerEventList = null
  receivedAdmEventList = null
  receivedResEventList = null
  lastRegistryListCount = null

  beforeAll () ->
    klogger.setLoggerOwner 'admission-api.test'
    logger = klogger.getLogger 'admission-api.test'
    logger.configure {
      transports: {
        file : { 'level': 'debug', 'filename': 'slap.log' }
        # console : { 'level' : 'debug', 'colorize': true }
      }
    }
    klogger.setLogger [AdmissionRestAPI]

    mockPlanner = new MockPlanner()
    clearStorage()
    .then () ->
      app = express()
      storage = multer.diskStorage {
        destination: '/tmp'
        filename: (req, file, cb) ->
          name = file.fieldname + '-' + \
                 kutils.generateId() + \
                 path.extname(file.originalname)
          cb null, name
      }
      upload = multer({ storage: storage }).any()
      app.use bodyParser.json()
      app.use bodyParser.urlencoded({ extended: true })
      app.use upload

      # Our restapi middleware
      admissionRestApi = new AdmissionRestAPI admCfg, mockPlanner
      admissionRestApi.on 'planner', (evt) -> receivedPlannerEventList.push evt
      admissionRestApi.on 'admission', (evt) -> receivedAdmEventList.push evt
      admissionRestApi.on 'resource', (evt) ->
        if receivedResEventList? then receivedResEventList.push evt
      admissionRestApi.init()
    .then () ->
      app.use '/admission', admissionRestApi.getRouter()
      app.use '/management', admissionRestApi.getManagementRouter()

      # Basic error handler
      app.use (req, res, next) ->
        return res.status(404).send('Not Found')
      logger.info "ADMISSION listening at port #{port}"
      server = http.createServer(app)
      server.listen port

      admissionApi  = supertest "http://localhost:#{port}/admission"
      managementApi = supertest "http://localhost:#{port}/management"
  , 10000


  beforeEach () ->
    if keepStorageAfterTest
      keepStorageAfterTest = false
      q()
    else
      clearStorage()


  afterAll ->
    # this.timeout 20000
    # try
    #   server.close()
    # catch error
    #   console.log "ERROR", error
    promises = (prmdir f for f in used_folders)
    promises.push (q.denodeify(server.close))()
    promises.push q.Promise (resolve, reject) ->
      server.close (error) ->
        if error?
          reject error
        else
          resolve()
    q.all promises
  , 20000


  it 'ComponentRegistry is correctly selected for Managed', () ->
    reg = getComponentRegistry {
      runtime: 'slap://slapdomain/runtimes/managed'
    }
    expect(reg).toBeInstanceOf ManagedComponentRegistry


  it 'ComponentRegistry is correctly selected for Unmanaged', () ->
    reg = getComponentRegistry {
      runtime: 'slap://slapdomain/runtimes/unmanaged'
    }
    expect(reg).toBeInstanceOf UnmanagedComponentRegistry


  it 'ComponentRegistry is correctly selected for Delta', () ->
    reg = getComponentRegistry {
      runtime: 'slap://slapdomain/runtimes/delta'
    }
    expect(reg).toBeInstanceOf UnmanagedComponentRegistry


  it 'ComponentRegistry is correctly selected for default', () ->
    reg = getComponentRegistry {
      runtime: 'slap://slapdomain/runtimes/fake'
    }
    expect(reg).toBeInstanceOf ManagedComponentRegistry


  it 'Admission REST API emits Admission events upwards', () ->
    # Clear received event list before starting test
    receivedPlannerEventList = []
    promiseChain = q()
    for evt in ADMISSION_EVENT_LIST
      do (evt) ->
        promiseChain = promiseChain.then () ->
          # Simulate events being emitted by inner Admission instance
          admissionRestApi.adm.emit 'planner', evt
          q.delay 500
    promiseChain.then () ->
      q.delay 500
    .then () ->
      expect(receivedPlannerEventList).toHaveLength 4
      expect(receivedPlannerEventList).toEqual ADMISSION_EVENT_LIST
  , 10000


  it 'Test Admission REST API - list registered bundles', (done) ->
    admissionApi.get '/registries'
    .expect 200
    .expect 'Content-Type', /json/
    .end (err, res) ->
      if err
        done err
      else
        # console.log 'LIST: ', res.body
        expect(res).toHaveProperty 'body.success', true
        expect(res).toHaveProperty 'body.data'
        expect(res.body.data).toBeInstanceOf Array
        expect(res.body.data.length).toBeGreaterThanOrEqual 0
        lastRegistryListCount = res.body.data.length
        expect(res).toHaveProperty 'body.message'
          , 'SUCCESSFULLY PROCESSED LIST BUNDLES.'
        done()
  , 10000


  it 'Test Admission REST API - list bundles (with metadata error)', (done) ->

    # Modify an element metadata.json file to contain wrong JSON value, to
    # force an error.
    fakeMetadata = '{ "spec": "wrong JSON example"'
    metadataFile = path.join NATIVE_RUNTIME_0_0_1_DIR, 'metadata.json'
    fs.writeFileSync metadataFile, fakeMetadata

    # Clear ownership cache
    admissionRestApi.authorization.ownerCache = {}

    admissionApi.get '/registries'
    .expect 200
    .expect 'Content-Type', /json/
    .end (err, res) ->
      if err
        done err
      else
        console.log 'LIST: ', res.body
        expect(res).toHaveProperty 'body.success', true
        expect(res).toHaveProperty 'body.data'
        expect(res.body.data).toBeInstanceOf Array
        expect(res.body.data.length).toBeGreaterThanOrEqual 0
        # Result should contain the same number of results as the last call to
        # method '/registries'
        expect(res.body.data.length).toBe lastRegistryListCount
        expect(res).toHaveProperty 'body.message'
          , 'SUCCESSFULLY PROCESSED LIST BUNDLES.'
        done()
  , 10000


  it 'Test Admission REST API - reconfig components', (done) ->
    receivedAdmEventList = []
    admissionApi.put '/deployments/configuration'
    .attach('inline', 'tests/files/deploy_reconfig.json')
    .expect 200
    .expect 'Content-Type', /json/
    .end (err, res) ->
      if err
        done err
      else
        # console.log JSON.stringify res.body
        expect(res).toHaveProperty 'body.success', true
        expect(res).toHaveProperty 'body.message', 'Modification processed'
        expect(mockPlanner).toHaveProperty 'calculatedDeploymentReconfig'
        , EXPECTED_CALCULATED_DEPLOYMENT_RECONFIG
        expect(receivedAdmEventList).toHaveLength 1
        expect(receivedAdmEventList).toHaveProperty [0, 'name'], 'reconfig'
        done()


  it 'Test Admission REST API - scale instances', (done) ->
    receivedAdmEventList = []
    admissionApi.put '/deployments/configuration'
    .attach('inline', 'tests/files/deploy_scaleInstances.json')
    .expect 200
    .expect 'Content-Type', /json/
    .end (err, res) ->
      if err
        done err
      else
        expect(res).toHaveProperty 'body.success', true
        expect(res).toHaveProperty 'body.message', 'Modification processed'
        expect(receivedAdmEventList).toHaveLength 1
        expect(receivedAdmEventList).toHaveProperty [0, 'name']
        , 'manual-scaling'
        done()


  it 'Register ZIP bundle', (done) ->
    receivedAdmEventList = []
    admissionApi.post '/bundles'
    .attach('bundlesZip', COMPONENT_BUNDLE_ZIP_FILE)
    .expect 200
    .expect 'Content-Type', /json/
    .end (err, res) ->
      if err
        done err
      else
        # console.log 'RESULTADO: ', JSON.stringify res.body, null, 2
        expect(res).toHaveProperty 'body.success', true
        expect(res).toHaveProperty 'body.data.errors'
        expect(res).toHaveProperty 'body.data.successful'
        expect(res.body.data.errors).toHaveLength 0
        expect(res.body.data.successful).toHaveLength 1
        expect(receivedAdmEventList).toHaveLength 1
        expect(receivedAdmEventList).toHaveProperty [0, 'name']
        , 'register-bundle'
        expect(receivedAdmEventList).toHaveProperty [
          0, 'data', 'result', 'data', 'successful', 0
        ], 'Registered element: eslap://multisep/components/cfe/0_0_1'
        done()
  , 30000

  it 'Register ZIP bundle with Docker component', (done) ->
    receivedAdmEventList = []
    admissionApi.post '/bundles'
    .attach('bundlesZip', COMPONENT_DOCKER_ZIP_FILE)
    .expect 200
    .expect 'Content-Type', /json/
    .end (err, res) ->
      if err
        # console.log "ERROR", err.stack
        done err
      else
        # console.log 'RESULTADO: ', JSON.stringify res.body, null, 2
        expect(res).toHaveProperty 'body.success', true
        expect(res).toHaveProperty 'body.data.errors'
        expect(res).toHaveProperty 'body.data.successful'
        expect(res.body.data.errors).toHaveLength 0
        expect(res.body.data.successful).toHaveLength 1
        expect(receivedAdmEventList).toHaveLength 1
        expect(receivedAdmEventList).toHaveProperty [0, 'name']
        , 'register-bundle'
        expect(receivedAdmEventList).toHaveProperty [
          0, 'data', 'result', 'data', 'successful', 0
        ], 'Registered element: eslap://multisep/components/cfedocker/0_0_1'
        done()
  , 30000


  it 'Register JSON bundle', (done) ->
    receivedAdmEventList = []
    admissionApi.post '/bundles'
    .attach('bundlesJson', BUNDLES_JSON_FILE)
    .expect 200
    .expect 'Content-Type', /json/
    .end (err, res) ->
      if err
        done err
      else
        # console.log 'RESULTADO: ', JSON.stringify res.body, null, 2
        expect(res).toHaveProperty 'body.success', true
        expect(res).toHaveProperty 'body.data.errors'
        expect(res).toHaveProperty 'body.data.successful'
        expect(res.body.data.errors).toHaveLength 3
        expect(res.body.data.successful).toHaveLength 3
        expect(receivedAdmEventList).toHaveLength 1
        expect(receivedAdmEventList).toHaveProperty [0, 'name']
        , 'register-bundle'
        result = receivedAdmEventList[0].data.result
        expect(result.data.successful).toHaveLength 3
        expect(result.data.errors).toHaveLength 3
        done()
  , 30000


  it 'Register ZIP bundle with Deployment', (done) ->
    admissionApi.post '/bundles'
    .attach('bundlesZip', DEPLOYMENT_BUNDLE_ZIP_FILE)
    .expect 200
    .expect 'Content-Type', /json/
    .end (err, res) ->
      if err
        done err
      else
        expect(res).toHaveProperty 'body.success', true
        expect(res).toHaveProperty 'body.data.errors'
        expect(res).toHaveProperty 'body.data.successful'
        expect(res).toHaveProperty 'body.data.deployments'
        expect(res.body.data.errors).toHaveLength 0
        expect(res.body.data.successful).toHaveLength 6
        depls = res.body.data.deployments
        expect(depls).toHaveProperty 'successful'
        expect(depls).toHaveProperty 'errors'
        expect(depls.successful).toHaveLength 2
        expect(depls.errors).toHaveLength 0
        expect(depls).toHaveProperty ['successful', 0, 'name']
        done()
  , 60000


  it 'Register ZIP bundle with Service Link', (done) ->
    admissionApi.post '/bundles'
    .attach('bundlesZip', LINK_BUNDLE_ZIP_FILE)
    .expect 200
    .expect 'Content-Type', /json/
    .end (err, res) ->
      if err
        done err
      else
        # console.log 'RESULTADO: ', JSON.stringify res.body, null, 2
        expect(res).toHaveProperty 'body.success', true
        expect(res).toHaveProperty 'body.data.errors'
        expect(res).toHaveProperty 'body.data.successful'
        expect(res).toHaveProperty 'body.data.deployments'
        expect(res).toHaveProperty 'body.data.links'
        expect(res.body.data.errors).toHaveLength 0
        expect(res.body.data.successful).toHaveLength 5
        depls = res.body.data.deployments
        expect(depls).toHaveProperty 'successful'
        expect(depls).toHaveProperty 'errors'
        expect(depls.successful).toHaveLength 2
        expect(depls.errors).toHaveLength 0
        expect(depls).toHaveProperty ['successful', 0, 'name']
        links = res.body.data.links
        expect(links).toHaveProperty 'successful'
        expect(links).toHaveProperty 'errors'
        expect(links.successful).toHaveLength 0
        expect(links.errors).toHaveLength 1
        done()
  , 30000


  it 'Register ZIP bundle with Test', (done) ->
    keepStorageAfterTest = true
    admissionApi.post '/bundles'
    .attach('bundlesZip', TEST_BUNDLE_ZIP_FILE)
    .expect 200
    .expect 'Content-Type', /json/
    .end (err, res) ->
      if err
        done err
      else
        # console.log 'RESULTADO: ', JSON.stringify res.body, null, 2
        expect(res).toHaveProperty 'body.success', true
        expect(res).toHaveProperty 'body.data.errors'
        expect(res).toHaveProperty 'body.data.successful'
        expect(res).toHaveProperty 'body.data.testToken'
        expect(res).toHaveProperty 'body.data.deployments'
        expect(res.body.data.errors).toHaveLength 0
        expect(res.body.data.successful).toHaveLength 7
        depls = res.body.data.deployments
        expect(depls).toHaveProperty 'successful'
        expect(depls).toHaveProperty 'errors'
        expect(depls.successful).toHaveLength 2
        expect(depls.errors).toHaveLength 0
        expect(depls).toHaveProperty ['successful', 0, 'name']
        expect(res.body.data.testToken).toBeDefined()
        expect(res.body.data.testToken.length).toBeGreaterThan 0
        testToken = res.body.data.testToken
        done()
  , 60000


  it 'List bundles with vs without cache', () ->
    keepStorageAfterTest = true
    # Clear ownership cache
    admissionRestApi.authorization.ownerCache = {}
    # Make a first request and measure response time
    fakeResponse = new FakeHttpResponse()
    fakeRequest =
      user:
        id: 'elvis@graceland.com'
        roles: ['USER']
    afterFirst = null
    beforeSecond = null
    afterSecond = null
    beforeFirst = Date.now()
    admissionRestApi.listBundles fakeRequest, fakeResponse, false
    .then () ->
      afterFirst = Date.now()
      result = fakeResponse.response
      # console.log 'RESULTADO: ', JSON.stringify result, null, 2
      expect(result).toHaveProperty 'success', true
      # Make a second request and measure response time
      fakeResponse = new FakeHttpResponse()
      fakeRequest =
        user:
          id: 'elvis@graceland.com'
          roles: ['USER']
      beforeSecond = Date.now()
      admissionRestApi.listBundles fakeRequest, fakeResponse, false
    .then () ->
      afterSecond = Date.now()
      result = fakeResponse.response
      # console.log 'RESULTADO: ', JSON.stringify result, null, 2
      expect(result).toHaveProperty 'success', true
      durationFirst = afterFirst - beforeFirst
      durationSecond = afterSecond - beforeSecond
      # console.log "FIRST: #{durationFirst} millis"
      # console.log "SECOND: #{durationSecond} millis"
      expect(durationSecond).toBeLessThan durationFirst
  , 60000


  it 'No HTTP - Release test context', () ->
    keepStorageAfterTest = true
    if not testToken?
      return Promise.resolve()
    tempPath = path.join IMAGE_STORE_REMOTE, 'temporary', testToken
    try
      fs.statSync tempPath
    catch err
      return Promise.reject new Error "Directory should exist: #{tempPath}"
    fakeResponse = new FakeHttpResponse()
    fakeRequest =
      query:
        urn: testToken
      user: admCfg.acs?['anonymous-user']
    admissionRestApi.releaseTestContext fakeRequest, fakeResponse
    .then () ->
      result = fakeResponse.response
      # console.log 'RESULTADO: ', JSON.stringify result, null, 2
      expect(result).toHaveProperty 'success', true
      expect(result).toHaveProperty 'message'
      , 'Successfully released test context'
      expect () ->
        fs.statSync tempPath
      .toThrow()
  , 60000


  it 'No HTTP - Release test context - Empty context', () ->
    emptyToken = ''
    EXPECTED_ERROR = 'Missing test context'
    fakeResponse = new FakeHttpResponse()
    fakeRequest =
      query:
        urn: emptyToken
      user: admCfg.acs?['anonymous-user']
    admissionRestApi.releaseTestContext fakeRequest, fakeResponse
    .then () ->
      result = fakeResponse.response
      # console.log 'RESULTADO: ', JSON.stringify result, null, 2
      expect(result).toHaveProperty 'success', false
      expect(result).toHaveProperty 'message'
      expect(result.message).toMatch EXPECTED_ERROR
  , 30000


  it 'No HTTP - Release test context - No context', () ->
    EXPECTED_ERROR = 'Missing test context'
    fakeResponse = new FakeHttpResponse()
    fakeRequest =
      query:
        wrongParameter: testToken
      user: admCfg.acs?['anonymous-user']
    admissionRestApi.releaseTestContext fakeRequest, fakeResponse
    .then () ->
      result = fakeResponse.response
      # console.log 'RESULTADO: ', JSON.stringify result, null, 2
      expect(result).toHaveProperty 'success', false
      expect(result).toHaveProperty 'message'
      expect(result.message).toMatch EXPECTED_ERROR
  , 30000


  it.skip 'No HTTP - Link services (impostor)', () ->
    fakeResponse = new FakeHttpResponse()
    fakeUploadFile SERVICE_LINK_FILE
    .then (uploadedFile) ->
      fakeRequest =
        files:
          linkManifest:
            originalname: SERVICE_LINK_FILE
            path: uploadedFile
        user: admCfg.acs?['anonymous-user']
      admissionRestApi.linkServices fakeRequest, fakeResponse
    .then () ->
      # console.log 'RESULTADO: ', JSON.stringify result, null, 2
      result = fakeResponse.response
      expect(result).toHaveProperty 'success', true
      expect(result).toHaveProperty 'data.newConfiguration.deploymentUrn'
      expect(result).toHaveProperty 'data.newConfiguration.entrypoints'
  , 30000


  it 'No HTTP - Link services', () ->
    receivedAdmEventList = []
    fakeResponse = new FakeHttpResponse()
    fakeUploadFile SERVICE_LINK_FILE
    .then (uploadedFile) ->
      fakeRequest =
        files: [{
          fieldname: 'linkManifest'
          originalname: SERVICE_LINK_FILE
          path: uploadedFile
        }]
        user: admCfg.acs?['anonymous-user']
      admissionRestApi.linkServices fakeRequest, fakeResponse
    .then () ->
      result = fakeResponse.response
      # console.log 'RESULTADO: ', JSON.stringify result, null, 2
      expect(result).toHaveProperty 'success', true
      expect(result).toHaveProperty 'message'
      expect(result.message).toMatch 'SUCCESSFULLY LINKED SERVICES'
      expect(receivedAdmEventList).toHaveLength 1
      expect(receivedAdmEventList).toHaveProperty [0, 'name'], 'link'
      expect(receivedAdmEventList).toHaveProperty [
        0, 'data', 'result', 'success'
      ], true
  , 30000


  it 'No HTTP - Link services - Unknown deployment URN', () ->
    receivedAdmEventList = []
    EXPECTED_ERROR = 'ERROR LINKING SERVICES: Unknown deployment'
    fakeResponse = new FakeHttpResponse()
    fakeUploadFile SERVICE_LINK_FILE_WITH_ERROR2
    .then (uploadedFile) ->
      fakeRequest =
        files: [{
          fieldname: 'linkManifest'
          originalname: SERVICE_LINK_FILE_WITH_ERROR2
          path: uploadedFile
        }]
        user: admCfg.acs?['anonymous-user']
      admissionRestApi.linkServices fakeRequest, fakeResponse
    .then () ->
      result = fakeResponse.response
      # console.log 'RESULTADO: ', JSON.stringify result, null, 2
      expect(result).toHaveProperty 'success', false
      expect(result).toHaveProperty 'message'
      expect(result.message).toMatch EXPECTED_ERROR
      expect(receivedAdmEventList).toHaveLength 1
      expect(receivedAdmEventList).toHaveProperty [0, 'name'], 'link'
      expect(receivedAdmEventList).toHaveProperty [
        0, 'data', 'result', 'success'
      ], false
  , 30000

  it 'No HTTP - Link services one service missing', () ->
    fakeResponse = new FakeHttpResponse()
    fakeUploadFile SERVICE_LINK_FILE_WITH_ERROR
    .then (uploadedFile) ->
      fakeRequest =
        files: [{
          fieldname: 'linkManifest'
          originalname: SERVICE_LINK_FILE_WITH_ERROR
          path: uploadedFile
        }]
        user: admCfg.acs?['anonymous-user']
      admissionRestApi.linkServices fakeRequest, fakeResponse
    .then () ->
      result = fakeResponse.response
      # console.log 'RESULTADO: ', JSON.stringify result, null, 2
      expect(result).toHaveProperty 'success', false
      expect(result).toHaveProperty 'message', ERROR_MSG_LINK_MISSING_SERVICE
  , 30000


  it 'No HTTP - Unlink services', () ->
    receivedAdmEventList = []
    fakeResponse = new FakeHttpResponse()
    fakeUploadFile SERVICE_LINK_FILE
    .then (uploadedFile) ->
      fakeRequest =
        files: [{
          fieldname: 'linkManifest'
          originalname: SERVICE_LINK_FILE
          path: uploadedFile
        }]
        user: admCfg.acs?['anonymous-user']
      admissionRestApi.unlinkServices fakeRequest, fakeResponse
    .then () ->
      result = fakeResponse.response
      # console.log 'RESULTADO: ', JSON.stringify result, null, 2
      expect(result).toHaveProperty 'success', true
      expect(result).toHaveProperty 'message'
      expect(result.message).toMatch 'SUCCESSFULLY UNLINKED SERVICES'
      expect(receivedAdmEventList).toHaveLength 1
      expect(receivedAdmEventList).toHaveProperty [0, 'name'], 'unlink'
      expect(receivedAdmEventList).toHaveProperty [
        0, 'data', 'result', 'success'
      ], true
  , 30000


  it 'No HTTP - Unlink services with querystring', () ->
    fakeResponse = new FakeHttpResponse()
    fakeUploadFile SERVICE_LINK_FILE
    .then (uploadedFile) ->
      fakeRequest =
        files: [{
          fieldname: 'linkManifest'
          originalname: SERVICE_LINK_FILE
          path: uploadedFile
        }]
        user: admCfg.acs?['anonymous-user']
      admissionRestApi.linkServices fakeRequest, fakeResponse
    .then () ->
      fakeResponse = new FakeHttpResponse()
      fakeUploadFile SERVICE_LINK_FILE
      .then (uploadedFile) ->
        fakeRequest =
          query:
            linkManifest: JSON.stringify require SERVICE_LINK_FILE
          user: admCfg.acs?['anonymous-user']
        # console.log JSON.stringify fakeRequest
        admissionRestApi.unlinkServices fakeRequest, fakeResponse
    .then () ->
      result = fakeResponse.response
      # console.log 'RESULTADO: ', JSON.stringify result, null, 2
      expect(result).toHaveProperty 'success', true
      expect(result).toHaveProperty 'message'
      expect(result.message).toMatch 'SUCCESSFULLY UNLINKED SERVICES'
  , 30000


  it 'No HTTP - Register ZIP bundle and Spread Configuration', () ->
    cc_service_expected =
      worker:
        paramJSONDeploy: 'deploy-value'
      prefix: 'pre-'
      sufix:'-post'
    cc_worker_expected =
      paramDefaultComponent: 'component-value'
      paramDefaultRole: 'role-value'
      paramJSONDeploy: 'deploy-value'
      paramModule: 'pre---post'
      paramDefaultNumber: 7.6
      paramDefaultArray: [4, 5]
      paramDefaultInteger: 95
      paramDefaultVhost:
        domain: 'patatin.com'
        port: 80
    cr_worker_expected =
      forever:
        type: 'eslap://eslap.cloud/resource/volume/persistent/1_0_0',
        name: 'eslap://sampleservicecalculator/resources/volumes/persistent',
        parameters:
          size: '10'
      temporal:
        type: 'eslap://eslap.cloud/resource/volume/volatile/1_0_0',
        parameters:
          size: '1'

    fakeResponse = new FakeHttpResponse()
    fakeUploadFile SPREAD_ZIP_BUNDLE
    .then (uploadedFile) ->
      fakeRequest =
        files: [{
          fieldname: 'bundlesZip'
          originalname: SPREAD_ZIP_BUNDLE
          path: uploadedFile
        }]
        user: admCfg.acs?['anonymous-user']
      # console.log 'Registrando bundle...'
      admissionRestApi.registerBundle fakeRequest, fakeResponse, false
    .then () ->
      result = fakeResponse.response
      # console.log 'RESULTADO: ', JSON.stringify result, null, 2
      for d in result.data.deployments.successful
        if kutils.startsWith d.name, \
        'slap://sampleservicecalculator/deployments'
          cc = d['components-configuration']
          cr = d['components-resources']
          expect(cc['__service']).toEqual cc_service_expected
          expect(cc['cfe']).toEqual {}
          expect(cc['worker']).toEqual cc_worker_expected
          expect(cr['worker']).toEqual cr_worker_expected
  , 60000


  it 'No HTTP - Register ZIP bundle exceeding size limit', () ->
    # Size limit: tests/config/admission.json/maxBundleSize
    receivedAdmEventList = []
    fakeResponse = new FakeHttpResponse()
    fakeUploadFile ZIPBOMB_DEPLOYMENT_BUNDLE_ZIP_FILE
    .then (uploadedFile) ->
      fakeRequest =
        files: [{
          fieldname: 'bundlesZip'
          originalname: ZIPBOMB_DEPLOYMENT_BUNDLE_ZIP_FILE
          path: uploadedFile
        }]
        user: admCfg.acs?['anonymous-user']
      admissionRestApi.registerBundle fakeRequest, fakeResponse, false
    .then (result) ->
      # Expected two errors:
      # - There is a zip-bomb in the bundle
      # - There is a zip-bomp in the worker component
      errors = result.data.errors
      expect(errors).toHaveLength 2
      expect(errors[0].startsWith 'Extracting zip file').toBeTruthy()
      expect(errors[0].endsWith 'excedeed').toBeTruthy()
      expect(errors[1].startsWith 'Registering bundle').toBeTruthy()
      expect(errors[1].endsWith 'excedeed').toBeTruthy()
      # Expected one error deploying: component worker isnt available
      depErrors = result.data.deployments.errors
      expect(depErrors).toHaveLength 1
      expect(depErrors[0].error.startsWith 'Error: Rsync failed').toBeTruthy()
      # Expected events
      expect(receivedAdmEventList).toHaveLength 1
      expect(receivedAdmEventList).toHaveProperty [0, 'name'], 'register-bundle'
      result = receivedAdmEventList[0].data.result
      expect(result.data.successful).toHaveLength 5
      expect(result.data.errors).toHaveLength 2
  , 60000


  it 'No HTTP - Undeploy with empty deployment URN', () ->
    EXPECTED_ERROR = 'Missing mandatory parameter (deployment URN)'
    receivedAdmEventList = []
    fakeResponse = new FakeHttpResponse()
    fakeRequest =
      query:
        urn: ''
      user: admCfg.acs?['anonymous-user']
    admissionRestApi.undeploy fakeRequest, fakeResponse
    .then () ->
      result = fakeResponse.response
      # console.log 'RESULTADO: ', JSON.stringify result, null, 2
      expect(result).toHaveProperty 'success', false
      expect(result).toHaveProperty 'message'
      expect(result.message).toMatch EXPECTED_ERROR
  , 30000


  it 'Test Management REST API - Get Stamp state', (done) ->
    managementApi.get '/state'
    .expect 200
    .expect 'Content-Type', /json/
    .end (err, res) ->
      if err
        done err
      else
        expect(res).toHaveProperty 'body.success', true
        expect(res).toHaveProperty 'body.message'
        expect(res.body.message)
        .toMatch 'SUCCESSFULLY PROCESSED GET STAMP STATE.'
        expect(res).toHaveProperty 'body.data.deployedServices'
        expect(res).toHaveProperty 'body.data.deployedInstances'
        expect(res).toHaveProperty 'body.data.allocatedNodes'
        expect(res).toHaveProperty 'body.data.linkedServices'
        done()


  it 'List resources of all users by admin', () ->
    fakeResponse = new FakeHttpResponse()
    fakeRequest =
      query:
        owner: ''
      user: admCfg.acs?['anonymous-user']
    admissionRestApi.listResourcesInUse fakeRequest, fakeResponse
    .then () ->
      expect(fakeResponse).toHaveProperty 'response.success', true


  it 'List resources of user john by john', () ->
    fakeResponse = new FakeHttpResponse()
    fakeRequest =
      query:
        owner: 'john@kumori.systems'
      user: { "id": "john@kumori.systems", "roles": ["DEVELOPER"] }
    admissionRestApi.listResourcesInUse fakeRequest, fakeResponse
    .then () ->
      expect(fakeResponse).toHaveProperty 'response.success', true


  it 'List resources of user maria by john (will fail)', () ->
    expectedError = 'ERROR LISTING RESOURCES: Unauthorized request from \
                     john@kumori.systems'
    fakeResponse = new FakeHttpResponse()
    fakeRequest =
      query:
        owner: 'maria@kumori.systems'
      user: { "id": "john@kumori.systems", "roles": ["DEVELOPER"] }
    admissionRestApi.listResourcesInUse fakeRequest, fakeResponse
    .then () ->
      expect(fakeResponse).toHaveProperty 'response.success', false
      expect(fakeResponse).toHaveProperty 'response.message', expectedError

  it 'Test Admission REST API - delete resource (not a volume)', (done) ->
    receivedAdmEventList = []
    receivedResEventList = []
    RESOURCE_URN = \
      'eslap://sampleservicecalculator/resources/vhost/www_mesie_com'
    EXPECTED_MSG = 'Unregistered succesfully'
    fakeResponse = new FakeHttpResponse()
    fakeRequest =
      query:
        urn: RESOURCE_URN
      user: admCfg.acs?['anonymous-user']
    originalRemoveResource = mockPlanner.removeResource
    mockPlanner.removeResource = () ->
      q.reject new Error 'Planner.removeResource called'
    kutils.copyFile RESOURCE_VHOST_MANIFEST, RESOURCE_VHOST_MANIFEST_STORAGE
    .then ->
      admissionRestApi.unregister fakeRequest, fakeResponse
    .then () ->
      result = fakeResponse.response
      # console.log 'RESULTADO: ', JSON.stringify result, null, 2
      expect(result).toHaveProperty 'success', true
      expect(result).toHaveProperty 'message', EXPECTED_MSG
      expect(receivedAdmEventList).toHaveLength 1
      expect(receivedAdmEventList).toHaveProperty [0, 'name'], 'unregister'
      expect(receivedAdmEventList).toHaveProperty [
        0, 'data', 'result', 'success'
      ], true
      expect(receivedResEventList).toHaveLength 1
      expect(receivedResEventList).toHaveProperty [0, 'name'], 'resourceremoved'
      mockPlanner.removeResource = originalRemoveResource
      fs.readFile RESOURCE_VHOST_MANIFEST_STORAGE, (error, data) ->
        try
          expect(error).toHaveProperty 'code', 'ENOENT'
          done()
        catch err
          done err
    .fail (err) ->
      # console.log "ERROR: #{err.stack}"
      mockPlanner.removeResource = originalRemoveResource
      done err
  , 30000


  # If the resource is a volume then the planner must be informed to remove
  # the volume from the resource manager.
  it 'Test Admission REST API - delete resource (a volume)', () ->
    receivedAdmEventList = []
    receivedResEventList = []
    RESOURCE_URN = \
      'eslap://sampleservicecalculator/resources/volumes/persistent'
    EXPECTED_MSG = 'Unregistered succesfully'
    fakeResponse = new FakeHttpResponse()
    fakeRequest =
      query:
        urn: RESOURCE_URN
      user: admCfg.acs?['anonymous-user']
    plannerCalled = false
    originalRemoveResource = mockPlanner.removeResource
    mockPlanner.removeResource = () ->
      plannerCalled = true
      q()
    kutils.copyFile RESOURCE_PERSISTENT_MANIFEST
    , RESOURCE_PERSISTENT_MANIFEST_STORAGE
    .then ->
      admissionRestApi.unregister fakeRequest, fakeResponse
    .then () ->
      if not plannerCalled
        done new Error 'Planner not called'
        return
      result = fakeResponse.response
      # console.log 'RESULTADO: ', JSON.stringify result, null, 2
      expect(result).toHaveProperty 'success', true
      expect(result).toHaveProperty 'message', EXPECTED_MSG
      expect(receivedAdmEventList).toHaveLength 1
      expect(receivedAdmEventList).toHaveProperty [0, 'name'], 'unregister'
      expect(receivedAdmEventList).toHaveProperty [
        0, 'data', 'result', 'success'
      ], true
      expect(receivedResEventList).toHaveLength 1
      expect(receivedResEventList).toHaveProperty [0, 'name'], 'resourceremoved'
      mockPlanner.removeResource = originalRemoveResource
      expect () ->
        fs.readFileSync RESOURCE_VHOST_MANIFEST_STORAGE
      .toThrowCode 'ENOENT'
      # fs.readFile RESOURCE_VHOST_MANIFEST_STORAGE, (error, data) ->
      #   try
      #     expect(error).toHaveProperty 'code', 'ENOENT'
      #     done()
      #   catch err
      #     done err
    .fail (err) ->
      console.log "ERROR: #{err.stack}"
      console.log "ERROR: #{JSON.stringify err, null, 2}"
      mockPlanner.removeResource = originalRemoveResource
      throw err
  , 30000

  it 'Test Admission REST API - delete resource - Wrong URN', () ->
    receivedAdmEventList = []
    RESOURCE_URN  = 'eslap://eslap.cloud/rezourze/vhost/www_mesie_com'
    EXPECTED_ERROR = "Error reading element #{RESOURCE_URN} manifest. \
                    Check the URN."
    fakeResponse = new FakeHttpResponse()
    fakeRequest =
      query:
        urn: RESOURCE_URN
      user: admCfg.acs?['anonymous-user']
    # kutils.copyFile RESOURCE_VHOST_MANIFEST, RESOURCE_VHOST_MANIFEST_STORAGE
    # .then ->
    #   admissionRestApi.unregister fakeRequest, fakeResponse
    admissionRestApi.unregister fakeRequest, fakeResponse
    .then () ->
      result = fakeResponse.response
      # console.log 'RESULTADO: ', JSON.stringify result, null, 2
      expect(result).toHaveProperty 'success', false
      expect(result).toHaveProperty 'message', EXPECTED_ERROR
      expect(receivedAdmEventList).toHaveLength 1
      expect(receivedAdmEventList).toHaveProperty [0, 'name'], 'unregister'
      expect(receivedAdmEventList).toHaveProperty [
        0, 'data', 'result', 'success'
      ], false
  , 30000

  it 'Test Admission REST API - delete resource - Wrong spec', () ->
    receivedAdmEventList = []
    RESOURCE_URN = \
      'eslap://sampleservicecalculator/resources/vhost/www_mesie_com'
    EXPECTED_ERROR = 'Invalid spec field in element manifest'
    fakeResponse = new FakeHttpResponse()
    fakeRequest =
      query:
        urn: RESOURCE_URN
      user: admCfg.acs?['anonymous-user']
    kutils.copyFile RESOURCE_VHOST_MANIFEST, RESOURCE_VHOST_MANIFEST_STORAGE
    .then ->
      manifestContent = require RESOURCE_VHOST_MANIFEST_STORAGE
      manifestContent.spec = "eslap://eslap.cloud/rezourze/vhost/1_0_0"
      fs.writeFileSync RESOURCE_VHOST_MANIFEST_STORAGE
        , JSON.stringify(manifestContent, null, 2)
      admissionRestApi.unregister fakeRequest, fakeResponse
    .then () ->
      result = fakeResponse.response
      # console.log 'RESULTADO: ', JSON.stringify result, null, 2
      expect(result).toHaveProperty 'success', false
      expect(result).toHaveProperty 'message', EXPECTED_ERROR
      expect(receivedAdmEventList).toHaveLength 1
      expect(receivedAdmEventList).toHaveProperty [0, 'name'], 'unregister'
      expect(receivedAdmEventList).toHaveProperty [
        0, 'data', 'result', 'success'
      ], false
  , 30000

  it 'Test Admission REST API - delete resource - Element in use', (done) ->
    receivedAdmEventList = []
    RESOURCE_URN = \
      'eslap://sampleservicecalculator/resources/vhost/www_mesie_com'
    EXPECTED_ERROR = "Resource #{RESOURCE_URN} is in use."
    originalIsElementInUse = mockPlanner.isElementInUse
    mockPlanner.isElementInUse = () ->
      q.resolve {
        inUse: true
      }
    fakeResponse = new FakeHttpResponse()
    fakeRequest =
      query:
        urn: RESOURCE_URN
      user: admCfg.acs?['anonymous-user']
    kutils.copyFile RESOURCE_VHOST_MANIFEST, RESOURCE_VHOST_MANIFEST_STORAGE
    .then ->
      admissionRestApi.unregister fakeRequest, fakeResponse
    .then () ->
      result = fakeResponse.response
      # console.log 'RESULTADO: ', JSON.stringify result, null, 2
      expect(result).toHaveProperty 'success', false
      expect(result).toHaveProperty 'message', EXPECTED_ERROR
      expect(receivedAdmEventList).toHaveLength 1
      expect(receivedAdmEventList).toHaveProperty [0, 'name'], 'unregister'
      expect(receivedAdmEventList).toHaveProperty [
        0, 'data', 'result', 'success'
      ], false
      mockPlanner.isElementInUse = originalIsElementInUse
      done()
    .fail (err) ->
      console.log "ERROR: #{err.stack}"
      console.log "ERROR: #{JSON.stringify err, null, 2}"
      mockPlanner.isElementInUse = originalIsElementInUse
      done err
  , 30000


  it 'Test Admission REST API - delete component', () ->
    receivedAdmEventList = []
    COMPONENT_URN = 'eslap://slapdomain/components/httpsep/0_0_1'
    EXPECTED_MSG = 'Unregistered succesfully'
    fakeResponse = new FakeHttpResponse()
    fakeRequest =
      query:
        urn: COMPONENT_URN
      user: admCfg.acs?['anonymous-user']
    admissionRestApi.unregister fakeRequest, fakeResponse
    .then () ->
      result = fakeResponse.response
      # console.log 'RESULTADO: ', JSON.stringify result, null, 2
      expect(result).toHaveProperty 'success', true
      expect(result).toHaveProperty 'message', EXPECTED_MSG
      expect(receivedAdmEventList).toHaveLength 1
      expect(receivedAdmEventList).toHaveProperty [0, 'name'], 'unregister'
      expect(receivedAdmEventList).toHaveProperty [
        0, 'data', 'result', 'success'
      ], true
  , 30000


  it 'Test Admission REST API - delete component - Wrong URN 1', () ->
    receivedAdmEventList = []
    COMPONENT_URN  = 'eslap://eslap.cloud/komponent/soviet/0_0_1'
    EXPECTED_ERROR = "Error reading element #{COMPONENT_URN} manifest. \
                      Check the URN."
    fakeResponse = new FakeHttpResponse()
    fakeRequest =
      query:
        urn: COMPONENT_URN
      user: admCfg.acs?['anonymous-user']
    admissionRestApi.unregister fakeRequest, fakeResponse
    .then () ->
      result = fakeResponse.response
      # console.log 'RESULTADO: ', JSON.stringify result, null, 2
      expect(result).toHaveProperty 'success', false
      expect(result).toHaveProperty 'message', EXPECTED_ERROR
      expect(receivedAdmEventList).toHaveLength 1
      expect(receivedAdmEventList).toHaveProperty [0, 'name'], 'unregister'
      expect(receivedAdmEventList).toHaveProperty [
        0, 'data', 'result', 'success'
      ], false
  , 30000


  it 'Test Admission REST API - delete component - Wrong URN 2', () ->
    COMPONENT_URN  = 'bofetada://eslap.cloud/component/http/sep/0_0_1'
    EXPECTED_ERROR = "Error reading element #{COMPONENT_URN} manifest. \
                      Check the URN."
    fakeResponse = new FakeHttpResponse()
    fakeRequest =
      query:
        urn: COMPONENT_URN
      user: admCfg.acs?['anonymous-user']
    admissionRestApi.unregister fakeRequest, fakeResponse
    .then () ->
      result = fakeResponse.response
      # console.log 'RESULTADO: ', JSON.stringify result, null, 2
      expect(result).toHaveProperty 'success', false
      expect(result).toHaveProperty 'message', EXPECTED_ERROR
  , 30000

  it 'Test Admission REST API - delete component - Element in use', () ->
    receivedAdmEventList = []
    COMPONENT_URN = 'eslap://slapdomain/components/httpsep/0_0_1'
    EXPECTED_ERROR = "Component #{COMPONENT_URN} is in use."
    originalIsElementInUse = mockPlanner.isElementInUse
    mockPlanner.isElementInUse = () ->
      q.resolve {
        inUse: true
      }
    fakeResponse = new FakeHttpResponse()
    fakeRequest =
      query:
        urn: COMPONENT_URN
      user: admCfg.acs?['anonymous-user']
    admissionRestApi.unregister fakeRequest, fakeResponse
    .then () ->
      result = fakeResponse.response
      expect(result).toHaveProperty 'success', false
      expect(result).toHaveProperty 'message', EXPECTED_ERROR
      mockPlanner.isElementInUse = originalIsElementInUse
    .fail (err) ->
      mockPlanner.isElementInUse = originalIsElementInUse
      throw err
  , 30000


  it 'Test Admission REST API - delete service', () ->
    SERVICE_URN  = 'eslap://eslap.cloud/services/http/inbound/1_0_0'
    EXPECTED_MSG = 'Unregistered succesfully'
    fakeResponse = new FakeHttpResponse()
    fakeRequest =
      query:
        urn: SERVICE_URN
      user: admCfg.acs?['anonymous-user']
    admissionRestApi.unregister fakeRequest, fakeResponse
    .then () ->
      result = fakeResponse.response
      # console.log 'RESULTADO: ', JSON.stringify result, null, 2
      expect(result).toHaveProperty 'success', true
      expect(result).toHaveProperty 'message', EXPECTED_MSG
  , 10000


  it 'Test Admission REST API - delete service - Wrong URN 1', () ->
    SERVICE_URN    = 'eslap://eslap.cloud/servicios/castizo/1_0_0'
    EXPECTED_ERROR = "Error reading element #{SERVICE_URN} manifest. \
                      Check the URN."
    fakeResponse = new FakeHttpResponse()
    fakeRequest =
      query:
        urn: SERVICE_URN
      user: admCfg.acs?['anonymous-user']
    admissionRestApi.unregister fakeRequest, fakeResponse
    .then () ->
      result = fakeResponse.response
      # console.log 'RESULTADO: ', JSON.stringify result, null, 2
      expect(result).toHaveProperty 'success', false
      expect(result).toHaveProperty 'message', EXPECTED_ERROR
  , 30000


  it 'Test Admission REST API - delete service - Wrong URN 2', () ->
    receivedAdmEventList = []
    SERVICE_URN    = 'http://eslap.cloud/services/http/sep/1_0_0'
    EXPECTED_ERROR = "Error reading element #{SERVICE_URN} manifest. \
                      Check the URN."
    fakeResponse = new FakeHttpResponse()
    fakeRequest =
      query:
        urn: SERVICE_URN
      user: admCfg.acs?['anonymous-user']
    admissionRestApi.unregister fakeRequest, fakeResponse
    .then () ->
      result = fakeResponse.response
      # console.log 'RESULTADO: ', JSON.stringify result, null, 2
      expect(result).toHaveProperty 'success', false
      expect(result).toHaveProperty 'message', EXPECTED_ERROR
  , 30000


  it 'Test Admission REST API - delete service - Wrong URN 3', () ->
    SERVICE_URN    = 'eslap://eslap-cloud/services/http/sep/1_0_0'
    EXPECTED_ERROR = "Error reading element #{SERVICE_URN} manifest. \
                      Check the URN."
    fakeResponse = new FakeHttpResponse()
    fakeRequest =
      query:
        urn: SERVICE_URN
      user: admCfg.acs?['anonymous-user']
    admissionRestApi.unregister fakeRequest, fakeResponse
    .then () ->
      result = fakeResponse.response
      # console.log 'RESULTADO: ', JSON.stringify result, null, 2
      expect(result).toHaveProperty 'success', false
      expect(result).toHaveProperty 'message', EXPECTED_ERROR
  , 30000

  it 'Test Admission REST API - delete service - Element in use', () ->
    SERVICE_URN  = 'eslap://eslap.cloud/services/http/inbound/1_0_0'
    EXPECTED_ERROR = "Service #{SERVICE_URN} is in use."
    originalIsElementInUse = mockPlanner.isElementInUse
    mockPlanner.isElementInUse = () ->
      q.resolve {
        inUse: true
      }
    fakeResponse = new FakeHttpResponse()
    fakeRequest =
      query:
        urn: SERVICE_URN
      user: admCfg.acs?['anonymous-user']
    admissionRestApi.unregister fakeRequest, fakeResponse
    .then () ->
      result = fakeResponse.response
      # console.log 'RESULTADO: ', JSON.stringify result, null, 2
      expect(result).toHaveProperty 'success', false
      expect(result).toHaveProperty 'message', EXPECTED_ERROR
      mockPlanner.isElementInUse = originalIsElementInUse
    .fail (err) ->
      mockPlanner.isElementInUse = originalIsElementInUse
      throw err
  , 10000
