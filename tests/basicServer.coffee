###
* Copyright 2022 Kumori Systems S.L.

* Licensed under the EUPL, Version 1.2 or – as soon they
  will be approved by the European Commission - subsequent
  versions of the EUPL (the "Licence");

* You may not use this work except in compliance with the
  Licence.

* You may obtain a copy of the Licence at:

  https://joinup.ec.europa.eu/software/page/eupl

* Unless required by applicable law or agreed to in
  writing, software distributed under the Licence is
  distributed on an "AS IS" basis,

* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
  express or implied.

* See the Licence for the specific language governing
  permissions and limitations under the Licence.
###

_            = require 'lodash'
http         = require 'http'
cors         = require 'cors'
path         = require 'path'
multer       = require 'multer'
express      = require 'express'
bodyParser   = require 'body-parser'

session      = require 'express-session'
Keycloak     = require 'keycloak-connect'


EventEmitter = require('events').EventEmitter

kutils = require 'k-utils'

require './index'
AdmissionRestAPI = require './admission-rest-api'


# DEFAULT_PORT = 8085
DEFAULT_PORT = 3000

DEFAULT_ADM_CONFIG =
  restTimeout: -1
  maxBundleSize: 52428800
  manifestRepository :
    type : "k8s"
    config :
      k8sApi:
        kubeConfig:
          clusters: [{
            name: 'minikube-cluster'
            server: 'https://192.168.99.100:8443'
            caFile: '/home/jferrer/.minikube/ca.crt'
            skipTLSVerify: false
          }]
          users: [{
            name: 'minikube',
            certFile: '/home/jferrer/.minikube/client.crt'
            keyFile: '/home/jferrer/.minikube/client.key'
          },{
              name: 'my-user'
              password: 'my-password'
          }]
          contexts: [
            name: 'minikube'
            user: 'my-user'
            cluster: 'minikube-cluster'
          ]
          currentContext: 'minikube'
  domains :
    refDomain : "test.kumori.cloud"
    random :
      baseDomain : "test.deployedin.cloud"
      prefix : ""
      join : "-"
      words :  2
      sufixNumbers: 4
  authentication:
    keycloakConfig:
      "realm": "quickstart"
      "auth-server-url": "http://jferrer-keycloak.test.kumori.cloud:8080/auth"
      "ssl-required": "external"
      "resource": "test-webserver"
      "verify-token-audience": true
      "credentials":
        "secret": "951addd0-d563-439e-aa74-dfcb0e55ba64"
      "use-resource-role-mappings": true
      "confidential-port": 0
      bearerOnly: true

      #
      #
      # "realm": "quickstart",
      # "bearer-only": true,
      # "auth-server-url": "http://jferrer-keycloak.test.kumori.cloud:8080/auth",
      # "ssl-required": "external",
      # "resource": "test-webserver",
      # "verify-token-audience": true,
      # "use-resource-role-mappings": true,
      # "confidential-port": 0
      #
      #
      # 'realm': 'quickstart'
      # 'auth-server-url': 'http://jferrer-keycloak.test.kumori.cloud:8080/auth'
      # 'ssl-required': 'external'
      # 'resource': 'test-webserver'
      # 'public-client': true
      # 'confidential-port': 0
      #
      #
      # realm: 'quickstart'
      # clientId: 'myclient'
      # bearerOnly: true
      # serverUrl: 'http://localhost:8080/auth'
      # realmPublicKey: 'MIIBIjANB...'


      # KEYCLOAK CONFIG WITH "PUBLIC":
      #
      #       'realm': 'quickstart'
      #       'auth-server-url': 'http://jferrer-keycloak.test.kumori.cloud:8080/auth'
      #       'ssl-required': 'external'
      #       'resource': 'test-webserver'
      #       'public-client': true
      #       'confidential-port': 0
      # KEYCLOAK CONFIG WITH BEARER-ONLY:
      #
      #       "realm": "quickstart",
      #       "bearer-only": true,
      #       "auth-server-url": "http://jferrer-keycloak.test.kumori.cloud:8080/auth",
      #       "ssl-required": "external",
      #       "resource": "test-webserver",
      #       "verify-token-audience": true,
      #       "use-resource-role-mappings": true,
      #       "confidential-port": 0
      #
      # KEYCLOAK CONFIG WITH "CONFIDENTIAL":
      #
      #

      #       "realm": "quickstart",
      #       "auth-server-url": "http://jferrer-keycloak.test.kumori.cloud:8080/auth",
      #       "ssl-required": "external",
      #       "resource": "test-webserver",
      #       "verify-token-audience": true,
      #       "credentials": {
      #         "secret": "<secret>"
      #       },
      #       "use-resource-role-mappings": true,
      #       "confidential-port": 0
################################################################################
################################################################################
class MockKeycloak
  constructor: () ->
  init: () ->
  protect: () ->
    return (req, res, next) -> next()
  middleware: () ->
    return (req, res, next) ->
      req.session['keycloak-token'] = 'anonymous'
      next()

################################################################################
################################################################################



class AdmissionServer

  constructor: (config) ->
    console.log "AdmissionServer.constructor CONFIG: #{JSON.stringify config, null, 2}"
    @config = config || {}

    @config.port ?= DEFAULT_PORT
    @config.admission ?= DEFAULT_ADM_CONFIG

    @app = null
    @server = null
    @admissionRestApi = null


  init: () ->
    console.log "INIT"
    meth = 'AdmissionServer.init'

    console.log "#{meth} - Setting up default error handlers..."

    # Configure default unexpected error handlers
    process
    .on 'unhandledRejection', (reason, p) =>
      console.log '************************************************************'
      console.log "UnhandledRejection: reason=#{reason} at Promise", p
      console.log '************************************************************'
    .on 'uncaughtException', (err) =>
      console.log '************************************************************'
      console.log "UNCAUGHT EXCEPTION: ", err
      console.log '************************************************************'
      process.exit 1
    console.log "#{meth} - Default error handlers set!"


    # Configure Multer upload
    multerStorage = multer.diskStorage {
      destination: '/tmp'
      filename: (req, file, cb) ->
        name = file.fieldname + '-' + \
               kutils.generateId() + \
               path.extname(file.originalname)
        cb null, name
    }
    upload = multer({ storage: multerStorage }).any()

    console.log "CREATING EXPRESS..."
    @app = express()
    @app.use bodyParser.json()
    @app.use bodyParser.urlencoded({ extended: true })
    corsOptions =
      optionsSuccessStatus: 200
    @app.use cors(corsOptions)
    @app.use upload


    # Create a session store to be used by both the express-session middleware
    # and the keycloak middleware.
    memoryStore = new session.MemoryStore()
    @app.use(session({
      secret: 'some secret',
      resave: false,
      saveUninitialized: true,
      store: memoryStore
    }))

    keycloak = null
    if @config.admission.authentication?.keycloakConfig?
      keycloakConfig = @config.admission.authentication.keycloakConfig
      console.log "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
      console.log "KEYCLOAK CONFIG:"
      console.log "#{JSON.stringify keycloakConfig, null, 2}"
      console.log "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"

      # Provide the session store to the Keycloak so that sessions
      # can be invalidated from the Keycloak console callback.
      #
      # If no configuration is passed, it is assumed that a kycloak.json file
      # exists in root dir.
      keycloak = new Keycloak({ store: memoryStore }, keycloakConfig)

      keycloakConfigLogin = _.cloneDeep keycloakConfig
      keycloakConfigLogin.bearerOnly = false
      keycloakConfigLogin['enable-cors'] = true
      keycloakConfigLogin['public-client'] = true
      console.log "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
      console.log "KEYCLOAK LOGIN CONFIG:"
      console.log "#{JSON.stringify keycloakConfigLogin, null, 2}"
      console.log "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
      keycloakLogin = new Keycloak({ store: memoryStore }, keycloakConfigLogin)
    else
      keycloak = new MockKeycloak()
      keycloakLogin = new MockKeycloak()

    # keycloak.init { flow: "hybrid" }

    # Specifies that the user-accessible application URL to logout should be
    # mounted at /logout
    # Specifies that Keycloak console callbacks should target the root URL.
    @app.use keycloak.middleware({
      logout: '/logout',
      admin: '/'
    })

    @app.use keycloakLogin.middleware({
      logout: '/logout',
      admin: '/'
    })


    # Create Admission Rest API
    admCfg = @config.admission || DEFAULT_ADM_CONFIG
    mockPlanner = {}
    @admissionRestApi = new AdmissionRestAPI admCfg, mockPlanner, keycloak, keycloakLogin

    @receivedPlannerEventList = []
    @receivedAdmEventList = []
    @receivedResEventList = []

    # AdmissionRestApi Event handlers
    @admissionRestApi.on 'planner', (evt) => @receivedPlannerEventList.push evt
    @admissionRestApi.on 'admission', (evt) => @receivedAdmEventList.push evt
    @admissionRestApi.on 'resource', (evt) => @receivedResEventList.push evt

    # Initialize Admission Rest API
    @admissionRestApi.init()
    .then () =>
      @app.use '/admission',  @admissionRestApi.getRouter()
      @app.use '/management', @admissionRestApi.getManagementRouter()
      @app.use '/login', @admissionRestApi.getLoginRouter()

      # Basic error handler
      @app.use (req, res, next) ->
        return res.status(404).send('Not Found')

      console.log "#{meth} - AdmissionServer listening at port #{@config.port}"
      @server = http.createServer(@app)
      @server.listen @config.port
      return true
    .fail (err) =>
      console.log "#{meth} Error: #{err.message || err}"
      console.log "#{err.stack}"
      return true
    .done()




################################################################################
##  TEST CODE ##
################################################################################
ADMISSION_CONFIG_FILE = '/home/ubuntu/jferrer/keycloak/admission/admission-server-config.json'

admConfig = require ADMISSION_CONFIG_FILE

admServer = new AdmissionServer admConfig
admServer.init()

