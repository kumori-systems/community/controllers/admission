###
* Copyright 2022 Kumori Systems S.L.

* Licensed under the EUPL, Version 1.2 or – as soon they
  will be approved by the European Commission - subsequent
  versions of the EUPL (the "Licence");

* You may not use this work except in compliance with the
  Licence.

* You may obtain a copy of the Licence at:

  https://joinup.ec.europa.eu/software/page/eupl

* Unless required by applicable law or agreed to in
  writing, software distributed under the Licence is
  distributed on an "AS IS" basis,

* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
  express or implied.

* See the Licence for the specific language governing
  permissions and limitations under the Licence.
###

q           = require 'q'
k8s         = require '@kubernetes/client-node'
stream      = require 'stream'
request     = require 'request'
readline    = require 'readline'
WebSocket   = require 'ws'
querystring = require 'querystring'


ADMISSION_API_URL = "http://<admission-url>:3003/admission"

TOKEN = "<auth-token>"

INSTANCE_ID = "kd-114635-98173648-frontend-deployment-d5c8b44c4-q62h9"

class AdmissionExecTester

  constructor: ()->

  getWSSessionToken: () ->

    instanceId = INSTANCE_ID

    outRows =  process.stdout.rows
    outColumns =  process.stdout.columns
    console.log "STDOUT ROWS: #{outRows}"
    console.log "STDOUT COLUMNS: #{outColumns}"

    query =
      access_token: TOKEN
      container: null
      command: "ls"
      tty: null
      rows: outRows
      columns: outColumns
    queryString = querystring.stringify query

    URL = "#{ADMISSION_API_URL}/instances/#{instanceId}/exec?#{queryString}"
    console.log "URL: #{URL}"

    q.promise (resolve, reject) =>
      reqOpts =
        url: URL
        headers:
          'Authorization': "bearer #{TOKEN}"
      request.get reqOpts, (err, response, body) =>
        if err
          console.error "GET error: #{err}"
          reject new Error err
        else
          console.log "GET status code: #{response.statusCode}"
          console.log "GET BODY: #{body}"
          if 400 <= response.statusCode <= 499
            errMsg = "HTTP request failed: #{response.statusCode} #{body}"
            reject new Error errMsg
          else
            parsedBody = JSON.parse body
            if parsedBody.success
              resolve parsedBody.data.wsSessionToken
            else
              errMsg = "Validations failed: #{parsedBody.message}"
              reject new Error errMsg


  testWS: (wsSessionToken) ->

    instanceId = INSTANCE_ID
    container = null
    # command = [ "ls", "-l" ]
    command = "/bin/sh"
    tty = 'yes'

    outRows =  process.stdout.rows
    outColumns =  process.stdout.columns
    console.log "STDOUT ROWS: #{outRows}"
    console.log "STDOUT COLUMNS: #{outColumns}"

    query =
      session_token: wsSessionToken
      # access_token: TOKEN
      container: container
      command: command
      tty: tty
      rows: outRows
      columns: outColumns
    queryString = querystring.stringify query


    URL = "#{ADMISSION_API_URL}/instances/#{instanceId}/exec?#{queryString}"
    console.log "URL: #{URL}"


    q.promise (resolve, reject) =>
      try
        wsOpts =
          perMessageDeflate: false
          headers:
            'Authorization': "bearer #{TOKEN}"

        console.log "Connecting to WS at #{URL}"
        console.log "Establishing Websocket connection with #{URL}."
        ws = new WebSocket URL, wsOpts
        console.log "Connection accepted by server."
      catch err
        console.log "ERROR: #{err}"

      # Websocket handlers that might be of interest...
      ws.on 'upgrade', (httpIM) -> console.log "--> onUpgrade"
      ws.on 'unexpected-response', (request, response) ->
        console.log "--> onUnexpectedResponse -
                     #{response.statusCode} #{response.statusMessage}"
      ws.on 'open', () -> console.log "--> onOpen"

      # Print to stdout every "raw" message received
      ws.on 'message', (msg) ->
        if msg.startsWith 'OUT:'
          process.stdout.write msg.slice(4).toString()
        else
          process.stdout.write msg.toString()

      ws.on 'error', (err) ->
        console.log "--> onError: #{err}"
        reject new Error err

      # Create an stdin terminal reader for reading commands
      process.stdin.setRawMode true
      process.stdin.setEncoding 'utf8'
      process.stdin.resume()
      ccc = 0 # Counter for CTRL+C
      process.stdin.on 'data', (key) ->
        # console.log "."
        # console.log "KEY: #{key}"
        if key is '\u0003'
          ccc++
        if ccc is 3
          process.exit()
        ws.send key


      process.stdout.on 'resize', () =>
        outRows =  process.stdout.rows
        outColumns =  process.stdout.columns
        ws.send "RESIZE:#{outRows},#{outColumns}"

      # OLD sender based on full lines
      #
      # readOptions =
      #   input: process.stdin
      #   output: process.stdout
      #   terminal: false
      # rl = readline.createInterface readOptions
      # # Send via the websocket each read line
      # rl.on 'line', (line) ->
      #   # console.log "READ FROM STDIN: #{line}"
      #   ws.send line

      ws.on 'close', (code, reason) ->
        console.log "--> onClose (Code: #{code} - Reason: #{reason})"
        # rl.close()
        resolve { code: code, reason: reason}


tester = new AdmissionExecTester()

tester.getWSSessionToken()
.then (wsSessionToken) =>
  # tester.testSSH()
  # tester.testCommand()
  # tester.testNoCommand()
  # tester.testWS()
  tester.testWS wsSessionToken
.then () =>
  console.log "Test finished (THEN)."
  process.exit 0
.catch (err) ->
  console.log "ERROR: #{err.message} - #{err.stack}"
  process.exit 0


