###
* Copyright 2022 Kumori Systems S.L.

* Licensed under the EUPL, Version 1.2 or – as soon they
  will be approved by the European Commission - subsequent
  versions of the EUPL (the "Licence");

* You may not use this work except in compliance with the
  Licence.

* You may obtain a copy of the Licence at:

  https://joinup.ec.europa.eu/software/page/eupl

* Unless required by applicable law or agreed to in
  writing, software distributed under the Licence is
  distributed on an "AS IS" basis,

* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
  express or implied.

* See the Licence for the specific language governing
  permissions and limitations under the Licence.
###

url = require 'url'
http = require 'http'

TOKEN = "<auth-token>"

instanceId = "kd-114635-98173648-frontend-deployment-d5c8b44c4-xl8hk"

reqData =
  protocol: 'http:'
  hostname: 'test-webserver.test.kumori.cloud'
  pathname: "/admission/logs/#{instanceId}"
  query:
    follow: 'yes'
    tail: 10
    since: 600

reqURL = url.parse url.format reqData


opts =
  protocol: reqData.protocol
  hostname: reqData.hostname
  port: 3003
  # path: "/admission/followLogs/#{instanceId}",
  path: reqURL.path
  headers:
    Authorization: "bearer #{TOKEN}"


http.get opts, (res) =>
  res.on 'response', () ->
    console.log '\n\n******************************************************'
    console.log "HTTP RESPONSE: #{response.statusCode}"
    console.log '******************************************************\n\n'
  res.on 'data', (chunk) ->
    # body += chunk
    process.stdout.write chunk.toString()

  res.on 'end', () ->
    console.log '\n\n******************************************************'
    console.log 'END OF HTTP REQUEST'
    console.log '******************************************************\n\n'

