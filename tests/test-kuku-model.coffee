###
* Copyright 2022 Kumori Systems S.L.

* Licensed under the EUPL, Version 1.2 or – as soon they
  will be approved by the European Commission - subsequent
  versions of the EUPL (the "Licence");

* You may not use this work except in compliance with the
  Licence.

* You may obtain a copy of the Licence at:

  https://joinup.ec.europa.eu/software/page/eupl

* Unless required by applicable law or agreed to in
  writing, software distributed under the Licence is
  distributed on an "AS IS" basis,

* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
  express or implied.

* See the Licence for the specific language governing
  permissions and limitations under the Licence.
###

index = require '../src/index'
KukuModel       = require '../src/k8s-kuku-model/kuku-model'
KukuService     = require '../src/k8s-kuku-model/kuku-service'
KukuComponent   = require '../src/k8s-kuku-model/kuku-component'
KukuDeployment  = require '../src/k8s-kuku-model/kuku-deployment'
KukuModelHelper = require '../src/k8s-kuku-model/kuku-model-helper'


################################################################################
################################################################################
##    DEV TESTING                                                             ##
################################################################################
################################################################################
COMPONENT_MANIFEST = '
{
  "spec": "http://eslap.cloud/manifest/component/2_0_0",
  "name": "eslap://kuku.kumori.systems/component/asciiart/converter/0_0_1",
  "code": {
      "name": "converter",
      "image": "jbgisbert/converter:1.0.0",
      "secret": "jbgisbert-dockerhub-secret",
      "env": {
        "LOGZIO_TOKEN": "Configuration.Parameters.logzioToken."
      },
      "filesystem": [{
        "path": "/usr/share/miapp/miconf.json",
        "data": "Configuration.Parameters.configfile"
      }]
  },
  "channels": {
    "requires": [ ],
    "provides": [{
      "name": "entrypoint",
      "type": "eslap://eslap.cloud/channel/listen/1_0_0",
      "protocol": "eslap://eslap.cloud/protocol/tcp/1_0_0",
      "port": 5432
    }]
  },
  "configuration": {
    "parameters": [{
      "name": "logzioToken",
      "type": "eslap://eslap.cloud/parameter/string/1_0_0"
    },{
      "name": "configFile",
      "type": "eslap://eslap.cloud/parameter/file/1_0_0"
    }]
  }
}
'

COMPONENT_MANIFEST_V1 = '
{
  "spec": "http://eslap.cloud/manifest/component/1_0_0",
  "name":"eslap://eslap.cloud/components/admission/1_0_1",
  "runtime": "eslap://eslap.cloud/runtime/native/dev/privileged/2_0_0",
  "code":"docker:kumori-systems/myimage",
  "channels":{
    "provides":[{
      "name":"sepdest",
      "type": "eslap://eslap.cloud/channel/reply/1_0_0",
      "protocol":"eslap://eslap.cloud/protocol/message/http/1_0_0",
      "port":1234
    }],
    "requires":[{
      "name":"acs",
      "type": "eslap://eslap.cloud/channel/request/1_0_0",
      "protocol":"eslap://eslap.cloud/protocol/message/http/1_0_0",
      "port":1122
    },
    {
      "name":"planner",
      "type": "eslap://eslap.cloud/channel/request/1_0_0",
      "protocol":"eslap://eslap.cloud/protocol/message/http/1_0_0",
      "port":5678
    }

  ]
  },
  "configuration":{
    "resources": [
      {
        "type": "eslap://eslap.cloud/resource/volume/volatile/1_0_0",
        "name": "workvolume"
      }
    ],
    "parameters": [{
      "name":"config",
      "type":"eslap://eslap.cloud/parameter/json/1_0_0"
    },{
      "name": "klogger",
      "type": "eslap://eslap.cloud/parameter/json/1_0_0"
    }]
  },
  "profile": {
    "threadability": "*"
  }
}
'


SERVICE_MANIFEST = '
{
  "spec": "http://eslap.cloud/manifest/service/1_0_0",
  "name": "eslap://eslap.cloud/services/admission/1_0_1",
  "configuration": {
    "resources": [
      {
      "name": "workvolume",
      "type": "eslap://eslap.cloud/resource/volume/volatile/1_0_0"
      }
    ],
    "parameters": [
      {
        "name": "admission",
        "type": "eslap://eslap.cloud/parameter/json/1_0_0"
      }
    ]
  },
  "roles": [
    {
      "name": "admission",
      "component": "eslap://eslap.cloud/components/admission/1_0_1",
      "resources": {
        "workvolume":  "workvolume"
      },
      "parameters": {}
    }
  ],
  "channels": {
    "requires": [
      {
        "name": "service-acs",
        "type": "eslap://eslap.cloud/channel/request/1_0_0",
        "protocol": "eslap://eslap.cloud/protocol/message/http/1_0_0"
      },
      {
        "name": "service-planner",
        "type": "eslap://eslap.cloud/channel/request/1_0_0",
        "protocol": "eslap://eslap.cloud/protocol/message/http/1_0_0"
      }
    ],
    "provides": [
      {
        "name": "http-admission",
        "type": "eslap://eslap.cloud/channel/reply/1_0_0",
        "protocol": "eslap://eslap.cloud/protocol/message/http/1_0_0"
      }
    ]
  },
  "connectors": [
    {
      "name": "mylittleconnector1",
      "type": "eslap://eslap.cloud/connector/loadbalancer/1_0_0",
      "depended": [
        {
          "endpoint": "http-admission"
        }
      ],
      "provided": [
        {
          "role": "admission",
          "endpoint": "sepdest"
        }
      ]
    },
    {
      "name": "mylittleconnector2",
      "type": "eslap://eslap.cloud/connector/loadbalancer/1_0_0",
      "provided": [
        {
          "endpoint": "service-acs"
        }
      ],
      "depended": [
        {
          "role": "admission",
          "endpoint": "acs"
        }
      ]
    },
    {
      "name": "mylittleconnector3",
      "type": "eslap://eslap.cloud/connector/loadbalancer/1_0_0",
      "provided": [
        {
          "endpoint": "service-planner"
        }
      ],
      "depended": [
        {
          "role": "admission",
          "endpoint": "planner"
        }
      ]
    }
  ]
}
'


SERVICE_MANIFEST_V1 = '
{
  "spec": "http://eslap.cloud/manifest/service/1_0_0",
  "name": "eslap://eslap.cloud/services/admission/1_0_1",
  "configuration": {
    "resources": [
      {
      "name": "workvolume",
      "type": "eslap://eslap.cloud/resource/volume/volatile/1_0_0"
      }
    ],
    "parameters": [
      {
        "name": "admission",
        "type": "eslap://eslap.cloud/parameter/json/1_0_0"
      }
    ]
  },
  "roles": [
    {
      "name": "admission",
      "component": "eslap://eslap.cloud/components/admission/1_0_1",
      "resources": {
        "workvolume":  "workvolume"
      },
      "parameters": {}
    }
  ],
  "channels": {
    "requires": [
      {
        "name": "service-acs",
        "type": "eslap://eslap.cloud/channel/request/1_0_0",
        "protocol": "eslap://eslap.cloud/protocol/message/http/1_0_0"
      },
      {
        "name": "service-planner",
        "type": "eslap://eslap.cloud/channel/request/1_0_0",
        "protocol": "eslap://eslap.cloud/protocol/message/http/1_0_0"
      }
    ],
    "provides": [
      {
        "name": "http-admission",
        "type": "eslap://eslap.cloud/channel/reply/1_0_0",
        "protocol": "eslap://eslap.cloud/protocol/message/http/1_0_0"
      }
    ]
  },
  "connectors": [
    {
      "type": "eslap://eslap.cloud/connector/loadbalancer/1_0_0",
      "depended": [
        {
          "endpoint": "http-admission"
        }
      ],
      "provided": [
        {
          "role": "admission",
          "endpoint": "sepdest"
        }
      ]
    },
    {
      "type": "eslap://eslap.cloud/connector/loadbalancer/1_0_0",
      "provided": [
        {
          "endpoint": "service-acs"
        }
      ],
      "depended": [
        {
          "role": "admission",
          "endpoint": "acs"
        }
      ]
    },
    {
      "type": "eslap://eslap.cloud/connector/loadbalancer/1_0_0",
      "provided": [
        {
          "endpoint": "service-planner"
        }
      ],
      "depended": [
        {
          "role": "admission",
          "endpoint": "planner"
        }
      ]
    }
  ]
}
'


DEPLOYMENT_MANIFEST = '
{
  "spec": "http://eslap.cloud/manifest/deployment/1_0_0",
  "servicename": "eslap://eslap.cloud/services/admission/1_0_0",
  "name": "admission-1",
  "configuration": {
    "resources": {
      "workvolume": {
        "size":"20"
      }
    },
    "parameters": {
      "admission":{
        "config": {
          "maxBundleSize": 524288000,
          "acs":{
            "anonymous-user":{
              "id": "admin-eslap@iti.es",
              "roles": ["ADMIN"]
            }
          },
          "planner":{
          },
          "imageFetcher": {
            "type": "blob",
            "config": {
              "remoteImageStore": {
                "path": "/storage/image-storage/remote",
                "imageFilename": "image.tgz"
              },
              "localImageStore": {
                "path": "/storage/image-storage/local",
                "imageFilename": "image.tgz"
              }
            }
          },
          "manifestRepository": {
            "type": "rsync",
            "config": {
              "remoteImageStore": {
                "path": "/storage/manifest-storage/remote",
                "imageFilename": "manifest.json"
              },
              "localImageStore": {
                "path": "/storage/manifest-storage/local",
                "imageFilename": "manifest.json"
              }
            }
          }
        },
        "klogger": {
          "transports": {
            "file": {
              "level" : "info",
              "filename" : "slap.log",
              "maxSize": 52428800,
              "maxFiles": 10,
              "tailable": true,
              "zippedArchive": false
            }
          },
          "runtime": true
        }
      }
    }
  },
  "roles": {
    "admission": {
      "resources": {
        "__instances": 1,
        "__cpu": 1,
        "__memory": 2,
        "__ioperf": 2,
        "__iopsintensive": true,
        "__bandwidth": 1,
        "__resilience": 1
      }
    }
  }
}
'

compManifest = JSON.parse COMPONENT_MANIFEST
servManifest = JSON.parse SERVICE_MANIFEST
deplManifest = JSON.parse DEPLOYMENT_MANIFEST

OUTPUT_FORMAT = 'json'
OUTPUT_FORMAT = 'yaml'

km = new KukuModel()

element = km.fromManifest compManifest
console.log "YAML: \n#{element.serialize(OUTPUT_FORMAT)}"

console.log "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
console.log "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
element = km.fromManifest servManifest
console.log "YAML: \n#{element.serialize(OUTPUT_FORMAT)}"

console.log "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
console.log "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
element = km.fromManifest deplManifest
console.log "YAML: \n#{element.serialize(OUTPUT_FORMAT)}"


