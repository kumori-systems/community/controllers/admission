###
* Copyright 2022 Kumori Systems S.L.

* Licensed under the EUPL, Version 1.2 or – as soon they
  will be approved by the European Commission - subsequent
  versions of the EUPL (the "Licence");

* You may not use this work except in compliance with the
  Licence.

* You may obtain a copy of the Licence at:

  https://joinup.ec.europa.eu/software/page/eupl

* Unless required by applicable law or agreed to in
  writing, software distributed under the Licence is
  distributed on an "AS IS" basis,

* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
  express or implied.

* See the Licence for the specific language governing
  permissions and limitations under the Licence.
###

http       = require 'http'
express    = require 'express'
expressWs  = require 'express-ws'
bodyParser = require 'body-parser'

k8s        = require '@kubernetes/client-node'
stream     = require 'stream'
K8sApiStub = require '../src/k8s-api-stub'



instanceId = "kd-114635-98173648-frontend-deployment-d5c8b44c4-q62h9"

CONFIG =
  kubeConfigFile: "/home/myuser/kubeconfig"
  watch: false

DEFAULT_PORT = 5000

Readable = stream.Readable
Writable = stream.Writable



class TestServer

  constructor: (@config = {}) ->
    meth = 'TestServer.constructor'

    @config.k8sConfig = CONFIG
    @config.port ?= DEFAULT_PORT
    console.log "#{meth} - Using port #{@config.port} (default)"

    @app = null
    @server = null
    @admissionRestApi = null
    @wsp = null

    @k8sApiStub  = new K8sApiStub @config.k8sConfig


  init: () ->
    meth = 'TestServer.init'
    console.log "#{meth} - Setting up default error handlers..."

    # Configure default unexpected error handlers
    process
    .on 'unhandledRejection', (reason, p) =>
      console.log '************************************************************'
      console.log "UnhandledRejection: reason=#{reason} at Promise", p
      console.log '************************************************************'
    .on 'uncaughtException', (err) =>
      console.log '************************************************************'
      console.log "UNCAUGHT EXCEPTION: ", err
      console.log '************************************************************'
      process.exit 1
    console.log "#{meth} - Default error handlers set!"


    console.log "CREATING EXPRESS..."
    @app = express()
    @expressWs = expressWs @app

    @app.use (req, res, next) ->
      console.log "*****************************************"
      console.log "**        MIDDLEWARE WORKS!            **"
      console.log "*****************************************"
      req.authData =
        user: 'javier'
      next()


    testRouter = express.Router()
    testRouter.get '/ping', @ping

    testRouter.ws '/exec', @execPure

    testRouter.ws '/execOW', @execOW

    @app.use '/test', testRouter


    @app.use (req, res, next) ->
      return res.status(404).send('Not Found')

    @app.use @logErrors

    console.log "#{meth} - AdmissionServer listening at port #{@config.port}"
    # @server = http.createServer(@app)
    # @server.listen @config.port
    @app.listen @config.port
    return true



  logErrors: (err, req, res, next) =>
    console.error err.stack
    next err


  ping: (req, res) =>
    msg = "PING --> PONG"
    console.log "ping --> pong"
    res.json { message: msg }

  execPure: (ws, req) =>
    msg = "EXEC RECEIVED"
    console.log "exec --> "
    console.log 'Auth user:', req.authData.user
    console.log "WS Status: #{ws.readyState}"
    console.log "WS CONNECTING : #{ws.CONNECTING}"
    console.log "WS OPEN       : #{ws.OPEN}"
    console.log "WS CLOSING    : #{ws.CLOSING}"
    console.log "WS CLOSED     : #{ws.CLOSED}"

    # Get the following parameters from API path:
    #
    #   - instanceId (podName)
    #
    # Get the following parameters from query:
    #
    #   - containerName : <string>
    #   - command : <string> or <string[]>
    #   - tty : <boolean> (default is false)


    ############################################################################
    ##   EN ADMISSION-REST-API ?  Funcion (ws) que cree los stream            ##
    ############################################################################

    # Create readable stream for sending commands ('stdin')
    rInStreamOpts =
      read: (size) ->
      # destroy: () ->
      #   console.log "rInStream.destroy"
    rInStream = new Readable rInStreamOpts
    # rInStream.on 'data', (data) =>
    #   console.log "rInStream.onData"
    # rInStream.on 'end', () =>
    #   console.log "rInStream.onEnd"

    # Create writable stream for handling received output ('stdout')
    wOutStreamOpts =
      write: (chunk, encoding, callback) ->
        console.log "wOutStream.write - #{chunk}"
        # ws.send chunk
        ws.send "OUT:" + chunk
        process.nextTick callback
      writev: (chunks, callback) ->
        console.log "wOutStream.writev - #{chunks}"
        process.nextTick callback
    wOutStream = new Writable wOutStreamOpts
    wOutStream.on 'error', ->

    # Create writable stream for handling received errors ('stderr')
    wErrStreamOpts =
      write: (chunk, encoding, callback) ->
        console.log "wErrStream.write - #{chunk}"
        # ws.send chunk
        ws.send "ERR:" + chunk
        process.nextTick callback
      writev: (chunks, callback) ->
        console.log "wErrStream.writev - #{chunks}"
        process.nextTick callback
    wErrStream = new Writable wErrStreamOpts


    # Client Websocket event handlers
    ws.on 'message', (msg) ->
      # A message from the client is considered a comman to execute
      console.log "ClientWS --> onMessage (stdin): #{msg}"
      # rStream.push msg + "\r"
      rInStream.push msg + "\n"
      # ws.send "Hello, client."
      # process.stdout.write

    ws.on 'open', () ->
      # TODO - Streams should be initialized here?
      console.log "ClientWS --> onOpen"

    ws.on 'close', (code, reason) ->
      # The client closed it websocket, cleanup
      # TODO
      console.log "ClientWS --> onClose  (Code: #{code} - Reason: #{reason})"
      console.log "ClientWS --> onClose  Destroying streams..."

      rInStream.push(null)
      # wOutStream.end('Client disconnected.')
      # wErrStream.end('Client disconnected.')

      console.log "ClientWS --> onClose  Streams destroyed!"

    ws.on 'error', (err) ->
      # En error ocurred in WS communication
      # TODO
      console.log "ClientWS --> onError: #{err}"


    # Prepare streams to use in K8s exec connection
    myStreams =
      out: wOutStream # process.stdout
      err: wErrStream # process.stderr
      in:  rInStream


    cleanup = (code, reason) =>
      console.log "Post exec cleanup"
      ws.removeAllListeners()
      ws.close code, reason
      ws.terminate()
      wOutStream.removeAllListeners()
      wErrStream.removeAllListeners()
      rInStream.removeAllListeners()
      wOutStream.destroy()
      wErrStream.destroy()
      rInStream.destroy()


    command = '/bin/sh'
    tty = true

    # @k8sApiStub.execPod null, instanceId, null, 'tail kk', true, myStreams
    @k8sApiStub.execPod null, instanceId, null, '/bin/sh', true, myStreams
    .then (status) =>
      console.log "k8sApiStub.execPod.then - Command finished with status:
                   #{JSON.stringify status}"
      cleanup 1000, "Command finished."
      # [...]
    .catch (err) =>
      console.log "k8sApiStub.execPod.catch - Command could not be executed.
                   Error: #{err.message || err}"
      cleanup 1011, "Error executing command."
      # [...]




  ###
  execOW: (ws, req) =>
    msg = "EXEC RECEIVED"
    console.log "exec --> "
    console.log 'Auth user:', req.authData.user


    rInStreamOpts =
      read: (size) ->
      destroy: () -> console.log "rInStream.destroy"

    rInStream = new Readable rInStreamOpts
    # rInStream.resume()

    rInStream.on 'data', (data) =>
      console.log "rInStream.onData"

    rInStream.on 'end', () =>
      console.log "rInStream.onEnd"


    # wOutStreamOpts =
    #   write: (chunk, encoding, callback) ->
    #     console.log "wOutStream.write - #{chunk}"
    #     # ws.send chunk
    #     ws.send "OUT:" + chunk
    #     process.nextTick callback
    #   writev: (chunks, callback) ->
    #     console.log "wOutStream.writev - #{chunks}"
    #     process.nextTick callback
    # wOutStream = new Writable wOutStreamOpts


    # wErrStreamOpts =
    #   write: (chunk, encoding, callback) ->
    #     console.log "wErrStream.write - #{chunk}"
    #     # ws.send chunk
    #     ws.send "ERR:" + chunk
    #     process.nextTick callback
    #   writev: (chunks, callback) ->
    #     console.log "wErrStream.writev - #{chunks}"
    #     process.nextTick callback
    # wErrStream = new Writable wErrStreamOpts


    # readableStream = new stream.Readable()
    # readableStream.read = () ->
    # readableStream
    # .resume()
    # .on 'end', () =>
    #   console.log 'REMOTE STDIN READABLE reached the end.'


    ws.on 'message', (msg) ->
      console.log "WS.onMessage --> FROM CLIENT STDIN: #{msg}"
      # rStream.push msg + "\r"
      rInStream.push msg + "\n"
      # ws.send "Hello, client."
      # process.stdout.write

    ws.on 'open', () ->
      console.log "exec --> onOpen"

    # ws.on 'connection', () ->
    #   console.log "exec --> onConnection"
    #   ws.send 'Welcome'

    ws.on 'close', () ->
      console.log "exec --> onClose"

    ws.on 'error', (err) ->
      console.log "exec --> onError: #{err}"

    myStreams =
      out: process.stdout
      err: process.stderr
      in:  rInStream
      # in:  process.stdin
      # out: process.stdout
      # err: process.stderr
      # in:  process.stdin

    ret = @k8sApiStub.execPod null, instanceId, null, '/bin/sh', false, myStreams
    console.log "RETURN TYPE: #{typeof ret}"
    ret
    .then (k8sWS) =>
      console.log "k8sApiStub.execPod.then - resolved with K8s ApiServer websocket"
      k8sWS.onopen = () ->
        console.log "K8S WS onOpen"
      k8sWS.onclose = () ->
        console.log "K8S WS onClose"
        ws.close()
      k8sWS.onerror = (err) ->
        console.log "K8S WS onError: #{err.message || err}"
        ws.close()
      k8sWS.onmessage = (data) ->
        streamNumber = data.data.readInt8(0)
        cleanData = data.data.slice 1
        console.log "K8S WS onMessage (stream #{streamNumber})"
        process.stdout.write cleanData.toString()
        ws.send data.data
    .catch (err) =>
      console.log "K8S EXEC ERROR: #{err.message || err.stack}"
  ###




s = new TestServer()
s.init()

