###
* Copyright 2022 Kumori Systems S.L.

* Licensed under the EUPL, Version 1.2 or – as soon they
  will be approved by the European Commission - subsequent
  versions of the EUPL (the "Licence");

* You may not use this work except in compliance with the
  Licence.

* You may obtain a copy of the Licence at:

  https://joinup.ec.europa.eu/software/page/eupl

* Unless required by applicable law or agreed to in
  writing, software distributed under the Licence is
  distributed on an "AS IS" basis,

* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
  express or implied.

* See the Licence for the specific language governing
  permissions and limitations under the Licence.
###

q             = require 'q'
fs            = require 'fs'
tar           = require 'tar-fs'
path          = require 'path'
uuid          = require 'uuid'
zlib          = require 'zlib'
yaml          = require 'js-yaml'
yazl          = require 'yazl'
yauzl         = require 'yauzl'
mkdirp        = require 'mkdirp'
crypto        = require 'crypto'
moment        = require 'moment'
zstandard     = require 'node-zstandard'
getFolderSize = require 'get-folder-size'


# Regular expression for validating domain names (of type A, not international)
DOMAIN_REGEX = /^\b((?=[a-z0-9-]{1,63}\.)[a-z0-9]+(-[a-z0-9]+)*\.)+[a-z]{2,63}\b$/


# Define some promisified versions of common functions
pstat    = q.denodeify fs.stat
preaddir = q.denodeify fs.readdir

################################################################################
##                               JSON UTILITIES                               ##
################################################################################

# Read and parse JSON from a file. Returns a promise
jsonFromFile = (filename) ->
  q.promise (resolve, reject) ->
    fs.readFile filename, (error, rawData) ->
      if (error)
        reject new Error("Reading file #{filename}: " + error.message)
      else
        try
          parsed = JSON.parse rawData
          resolve parsed
        catch err
          reject new Error("Parsing JSON data #{filename}: " + err)


# Read JSON from a file, no parsing. Returns a promise
jsonStringFromFile = (filename) ->
  q.promise (resolve, reject) ->
    fs.readFile filename, (error, rawData) ->
      if (error)
        reject new Error('Reading file: ' + error.message)
      else
        resolve rawData


################################################################################
##                               YAML UTILITIES                               ##
################################################################################

# Convert a YAML formatted string to an Object
yamlParse = (yamlStr) ->
  try
    yamlObj = yaml.load yamlStr
    return yamlObj
  catch e
    errMsg = "Couldn't parse YAML: \"#{yamlStr}\". Reason: #{e.message}"
    throw new Error errMsg

# Convert an object to a YAML formatted string
yamlStringify = (yamlObj) ->
  try
    return yaml.dump yamlObj
  catch e
    errMsg = "Couldn't stringify YAML object. Reason: #{e.message}"
    throw new Error errMsg


################################################################################
##                               FILE UTILITIES                               ##
################################################################################

# Save a string to a file. Returns a promise
saveFile = (data, filename) ->
  q.promise (resolve, reject) ->
    fs.writeFile filename, data, (error) ->
      if error
        reject new Error("Saving file '#{filename}': " + error.message)
      else
        resolve filename


# Delete a file. Returns a promise
deleteFile = (filename) ->
  q.promise (resolve, reject) ->
    fs.unlink filename, (error) ->
      if error and not error.code is 'ENOENT'
        reject new Error('Deleting file: ' + error.message)
      resolve()


# Renames a file or directory. Returns a promise
renameFile = (oldPath, newPath) ->
  q.promise (resolve, reject) ->
    fs.rename oldPath, newPath, (error) ->
      reject new Error('Renaming file: ' + error.message)
    resolve()


# Copy a file. Returns a promise
copyFile = (srcPath, dstPath) ->
  q.promise (resolve, reject) ->
    readStream = fs.createReadStream srcPath
    readStream.on 'error', reject
    writeStream = fs.createWriteStream dstPath
    writeStream.on 'error', reject
    writeStream.on 'finish', resolve
    readStream.pipe writeStream


# Check if a file or directory exists. Returns a promise
checkFileExists = (filename) ->
  q.promise (resolve, reject) ->
    fs.stat filename, (error, stats) ->
      if error and not error.code is 'ENOENT'
        reject new Error('Checking file: ' + error.message)
      else if error
        resolve false
      else
        resolve true

# Returns a promise. If the operation is successful, the promise resolves with
# an object with the contents of a directory. Options can be used to
# configure if search is recursive (default is off) and if items are returned as
# absolute (default) or relative paths.
#
# The object returned contains three lists of items matching the options:
#  - files     : list of all the files found
#  - dirs      : list of all the non-empty subdirectories found
#  - emptyDirs : list of all the empty subdirectories found
getDirContents = (dir, options = null, result = null, depth = 0) ->
  DEFAULT_OPTIONS =
    recursive: false
    relative: false

  if not dir?
    return q.reject new Error 'Required parameter "dir" is missing.'

  if not result?
    if depth is 0
      result =
        files: []
        dirs: []
        emptyDirs: []
    else
      return q.reject new Error "Previous result is null in depth #{depth}"

  # Assign default values
  options ?= DEFAULT_OPTIONS
  options.recursive ?= DEFAULT_OPTIONS.recursive
  options.relative ?= DEFAULT_OPTIONS.relative

  preaddir dir
  .then (elements) ->
    promiseList = []
    if elements.length is 0
      if depth isnt 0 then result.emptyDirs.push dir
    else
      if depth isnt 0 then result.dirs.push dir
      for el in elements
        do (el) ->
          elPath = path.join dir, el
          promiseList.push (\
            pstat elPath
            .then (fileStats) ->
              if fileStats.isFile()
                result.files.push elPath
              else if fileStats.isDirectory()
                if options.recursive
                  getDirContents elPath, options, result, depth+1
                else
                  # If not in recursive mode, find out if directory is empty
                  preaddir elPath
                  .then (els) ->
                    if els.length is 0
                      result.emptyDirs.push elPath
                    else
                      result.dirs.push elPath
            )
    q.all promiseList
  .then () ->
    # Ensure consistent results by ordering items
    result.files.sort (x,y) -> x > y
    result.dirs.sort (x,y) -> x > y
    result.emptyDirs.sort (x,y) -> x > y
    if options.relative and depth is 0
      result.files     = (path.relative(dir, f) for f in result.files)
      result.dirs      = (path.relative(dir, d) for d in result.dirs)
      result.emptyDirs = (path.relative(dir, d) for d in result.emptyDirs)
      return result
    else
      return result
  .fail (err) ->
    if depth is 0
      q.reject new Error "Failed to list directory #{dir}: #{err.stack}"
    else
      q.reject err


# Get the size of a folder (returned via promise)
getDirSize = (dir) ->
  return q.promise (resolve, reject) ->
    getFolderSize dir, (err, size) ->
      if err? then reject err
      else resolve size


################################################################################
##                          COMPRESSION UTILITIES                             ##
################################################################################

_zstd = q.denodeify zstandard.compress

zstd = (srcFile, zstFile) ->
  _zstd srcFile, zstFile

_unzstd = q.denodeify zstandard.decompress

unzstd = (zstFile, destFile) ->
  _unzstd zstFile, destFile

_zstdFromStream = q.denodeify zstandard.compressStreamToFile

tarzstd = (srcPath, zstFile) ->
  q.promise (resolve, reject) ->
    pstat srcPath
    .then (status) ->
      tarStream = tar.pack srcPath
      tarStream.on 'error', (error) ->
        reject error
      _zstdFromStream tarStream, zstFile
    .then (status) ->
      status
      .on 'error', (error) ->
        reject error
      .on 'end', (error) ->
        resolve zstFile
    .fail (err) ->
      reject err

_unzstdToStream = q.denodeify zstandard.decompressFileToStream

untarzstd = (zstFile, destPath) ->
  q.promise (resolve, reject) ->
    pstat zstFile
    .then (status) ->
      _unzstdToStream zstFile, tar.extract(destPath)
    .then (status) ->
      status
      .on 'error', (err) ->
        reject err
      .on 'finish', () ->
        resolve destPath
    .fail (err) ->
      reject err

# Unpack a tgz file to a directory. Returns a promise.
untgz = (tgzFile, dstPath) ->
  q.Promise (resolve, reject) ->
    pstat tgzFile
    .then (status) ->
      input = fs.createReadStream tgzFile
      input.pipe zlib.createGunzip()
      .on 'error', (err) ->
        reject err
      .pipe tar.extract(dstPath)
      .on 'error', (err) ->
        reject err
      .on 'finish', resolve
    .fail (err) ->
      reject err


# Extract a zip file to a directory.
# Returns a promise.
# If a maximum size is specified, the operation fails (as soon as possible)
# during the process when that maximum is reached.
# Note: When an error occurs, the generated directories/files are not deleted
unzip = (zipFile, dstPath, maxSize) ->
  currentSize = 0
  return q.promise (resolve, reject) ->
    pstat zipFile
    .then (status) ->
      try
        yauzl.open zipFile, {lazyEntries: true}, (err, zipObject) ->
          if (err) then reject err
          try # --> 2nd try-catch: readEntry() throw error when file not exists
            zipObject.readEntry()
            zipObject.on 'entry', (entry) ->
              dest = "#{dstPath}/#{entry.fileName}"
              if (/\/$/.test(dest))
                mkdirp dest, (err) ->
                  if (err) then reject err
                  zipObject.readEntry()
              else
                currentSize = currentSize + entry.uncompressedSize
                if maxSize? and currentSize > maxSize
                  reject new Error "Processing zip file #{zipFile}: \
                                    Max size (#{maxSize}) excedeed"
                  return
                zipObject.openReadStream entry, (err, readStream) ->
                  if (err) then reject  err
                  mkdirp path.dirname(dest), (err) ->
                    if (err) then reject err
                    mode=(entry.externalFileAttributes >> 16 & 0xfff).toString(8)
                    writeStream = fs.createWriteStream dest, {mode: mode}
                    writeStream.on 'close', () ->
                      zipObject.readEntry()
                    readStream.on 'error', (err) ->
                      reject new Error "Processing zip file #{zipFile}: #{err}"
                    writeStream.on 'error', (err) ->
                      reject new Error "Processing zip file #{zipFile}: #{err}"
                    readStream.pipe writeStream
            zipObject.on 'end', () ->
              zipObject.close()
              resolve currentSize
            zipObject.on 'error', (err) ->
              reject new Error "Processing zip file #{zipFile}: #{err}"
          catch e
            reject new Error "Processing zip file #{zipFile}: #{e}"
      catch e
        reject new Error "Opening zip file #{zipFile}: #{e}"
    .fail (err) ->
      reject err


# Create a zip file with the contents of the given directory
zipDir = (srcPath, zipFile) ->
  return q.promise (resolve, reject) ->
    pstat srcPath
    .then (status) ->
      addedSomething = false
      getDirOptions =
        relative: false
        recursive: true
      getDirContents srcPath, getDirOptions
    .then (listResult) ->
      try
        zip = new yazl.ZipFile()
        for f, i in listResult.files
          relative = path.relative srcPath, f
          zip.addFile f, relative
          addedSomething = true
        for d, i in listResult.emptyDirs
          relative = path.relative srcPath, d
          zip.addEmptyDirectory relative
          addedSomething = true
        if addedSomething
          ws = fs.createWriteStream zipFile
          zip.outputStream.pipe ws
          .on 'close', () ->
            resolve zipFile
          zip.end()
        else
          throw new Error "Directory #{srcPath} is empty."
      catch e
        reject new Error "Creating zip file #{zipFile}: #{e.message}"
    .fail (err) ->
      reject err


# Create a zip file from a single file (comress the file). If no zip file is
# provided, the original name will be used, replacing the extension with '.zip'.
zipFile = (srcFile, zipFile = null) ->
  return q.promise (resolve, reject) ->
    pstat srcFile
    .then (fileStats) ->
      if fileStats.isFile()
        try
          zip = new yazl.ZipFile()
          basename  = path.basename srcFile
          zip.addFile srcFile, basename

          if not zipFile?
            extension = path.extname srcFile
            regex = new RegExp(extension + '$')
            zipFile = srcFile.replace regex, '.zip'
          ws = fs.createWriteStream zipFile
          zip.outputStream.pipe ws
          .on 'close', () ->
            resolve zipFile
          zip.end()
        catch e
          reject new Error "Creating zip file #{zipFile}: #{e.message}"
      else
        reject new Error "Expected a file not a directory (#{srcFile})"
    .fail (err) ->
      reject err


# Pack and compress a directory to a file. Returns a promise.
packDirectory = (srcPath, dstPath) ->
  q.promise (resolve, reject) ->
    pstat srcPath
    .then (status) ->
      gzip = zlib.createGzip()
      out = fs.createWriteStream dstPath
      tar.pack(srcPath)
      .on 'error', (error) ->
        reject error
      .pipe(gzip).pipe(out)
      out.on 'finish', ->
        resolve dstPath
      out.on 'error', (error) ->
        reject error
    .fail (err) ->
      reject err


# Compress a string using gzip and base64 encode the result
gzipString = (str) ->
  # Use max compression level
  opts =
    level: 9
  zlib.gzipSync(str, opts).toString('base64')


# Decompress a base64 encoded gzipped string
gunzipString = (str) ->
  zlib.gunzipSync Buffer.from(str, 'base64')

################################################################################
##                             STRING UTILITIES                               ##
################################################################################

# Replace all matches of a RegExp in a string with another string.
replacePattern = (str, pattern, value) ->
  str.replace new RegExp(pattern, 'g'), value


# Replace all matches of a RegExp in a JSON object with a string, using
# object stringification.
replacePatternInJSON = (jsonObj, pattern, value) ->
  JSON.parse(replacePattern((JSON.stringify jsonObj), pattern, value))


# Checks if a string starts with another string
startsWith = (str, prefix) ->
  str[..(prefix.length-1)] is prefix

# Checks if a string ends with another string
endsWith = (str, suffix) ->
  str[-(suffix.length) ..] is suffix



################################################################################
##                              ARRAY UTILITIES                               ##
################################################################################

# WARNING: Always remember that arrays of Objects always work by reference!
#
# Example:
#   a = {obj: 0}
#   b = [a]
#   b.indexOf {obj: 0}      <-------  will return -1 (not found)

# Returns an array containing the elements that exist in both a and b
arrayIntersection = (a, b) ->
  [a, b] = [b, a] if a.length > b.length
  value for value in a when value in b


# Returns an array containing the elements that exist EXCLUSIVELY in a or b
arrayDifference = (a, b) ->
  (a.concat b).filter (i) -> a.indexOf(i) < 0 or b.indexOf(i) < 0


# Returns an array containing the elements of a and b combined.
# A flag is available to allow or remove duplicates in the result.
arrayMerge = (a, b, allowDuplicates = false) ->
  m = a.concat b
  if allowDuplicates
    m
  else
    output = {}
    output[m[key]] = m[key] for key in [0...m.length]
    value for key, value of output


# Compares two arrays
arrayEqual = (a, b) ->
  a.length is b.length and a.every (elem, i) -> elem is b[i]


################################################################################
##                           HOTCHPOTCH UTILITIES                             ##
################################################################################

# Generates a random ID
generateId = ->
  uuid.v4()


# Returns a random integer within the specified range
randomInt = (low, high) ->
  Math.floor (Math.random() * (high - low) + low)


# Returns a random hexadecimal string of the specified length
randomHex = (len) ->
  crypto.randomBytes(Math.ceil(len / 2))
  .toString('hex')
  .slice(0,len)


# Returns a SHA1 hash in hex format from the provided string
getHash = (str) ->
  return crypto.createHash('sha1').update(str).digest('hex')


# Returns a FNV-1 hash in hex format (of length 8) from the provided string
getFnvHash = (str) ->
  hashed = fnv1Hash str
  return hashed.toString(16).padStart(8, "0")


# Calculates a 32 bit FNV-1 hash (an unsigned 32 bit integer)
fnv1Hash = (str) =>
  # The seed, 32 bit offset_basis = 2,166,136,261 = 0x811C9DC5
  OFFSET_BASIS = 2166136261

  data = new Buffer str

  hashint = OFFSET_BASIS
  for i in [0..data.length - 1]
    hashint += (hashint << 1) + (hashint << 4) + (hashint << 7) + (hashint << 8) + (hashint << 24)
    hashint = hashint ^ data[i]

  # Return an unsigned 32 bit integer
  return hashint >>> 0


# Calculates a 32 bit FNV-1a hash (an unsigned 32 bit integer)
fnv1aHash = (str) =>
  # The seed, 32 bit offset_basis = 2,166,136,261 = 0x811C9DC5
  OFFSET_BASIS = 2166136261

  data = new Buffer str

  hashint = OFFSET_BASIS
  for i in [0..data.length - 1]
    hashint = hashint ^ data[i]
    hashint += (hashint << 1) + (hashint << 4) + (hashint << 7) + (hashint << 8) + (hashint << 24)

  # Return an unsigned 32 bit integer
  return hashint >>> 0



calculateChecksum = (filename) ->
  q.promise (resolve, reject) ->
    hash = crypto.createHash 'sha1'
    stream = fs.createReadStream filename
    stream.on 'data', (data) ->
      hash.update data
    stream.on 'end', () ->
      checksum = hash.digest 'hex'
      resolve checksum
    stream.on 'error', (error) ->
      reject error


# Returns a brief humand readable timestamp string, usually used as suffix for
# file names.
getDateSuffix = () ->
  d = new Date()
  s = d.getFullYear()
  s += ('0' + (d.getMonth() + 1)).slice(-2)
  s += ('0' + d.getDate()).slice(-2)
  s += '_' + ('0' + d.getHours()).slice(-2)
  s += ('0' + d.getMinutes()).slice(-2)
  s += ('0' + d.getSeconds()).slice(-2)


# Returns a timestamp string in the format "2022-01-26T18:50:22Z"
getTimestamp = (format = null) ->
  now = new Date().getTime()

  if format? and (format isnt '')
    return moment.utc(now).format(format)
  else
    # format() with no specific format returns a string representation in the
    # ISO8601 format: YYYY-MM-DDTHH:mm:ssZ
    return moment.utc(now).format()


isObject = (a) ->
  (!!a) and (a.constructor == Object)


isString = (val) ->
  typeof val is 'string' || ((!!val && typeof val is 'object') && \
    Object.prototype.toString.call(val) is '[object String]')


isArray = (val) ->
  val.constructor is Array


# Adds properties to object
extendObject = (obj, properties) ->
  for key, val of properties
    obj[key] = val
  obj

validateDomainName = (domainName) ->
  DOMAIN_REGEX.test domainName


exports.jsonFromFile             = jsonFromFile
exports.jsonStringFromFile       = jsonStringFromFile
exports.arrayEqual               = arrayEqual
exports.calculateChecksum        = calculateChecksum
exports.saveFile                 = saveFile
exports.deleteFile               = deleteFile
exports.renameFile               = renameFile
exports.copyFile                 = copyFile
exports.checkFileExists          = checkFileExists
exports.getDirContents           = getDirContents
exports.getDirSize               = getDirSize
exports.zstd                     = zstd
exports.unzstd                   = unzstd
exports.tarzstd                  = tarzstd
exports.untarzstd                = untarzstd
exports.untgz                    = untgz
exports.unzip                    = unzip
exports.zipDir                   = zipDir
exports.zipFile                  = zipFile
exports.packDirectory            = packDirectory
exports.replacePattern           = replacePattern
exports.replacePatternInJSON     = replacePatternInJSON
exports.startsWith               = startsWith
exports.endsWith                 = endsWith
exports.randomInt                = randomInt
exports.randomHex                = randomHex
exports.getHash                  = getHash
exports.getFnvHash               = getFnvHash
exports.getDateSuffix            = getDateSuffix
exports.getTimestamp             = getTimestamp
exports.isString                 = isString
exports.isObject                 = isObject
exports.isArray                  = isArray
exports.extendObject             = extendObject
exports.arrayIntersection        = arrayIntersection
exports.arrayDifference          = arrayDifference
exports.arrayMerge               = arrayMerge
exports.generateId               = generateId
exports.gzipString               = gzipString
exports.gunzipString             = gunzipString
exports.validateDomainName       = validateDomainName
exports.yamlParse                = yamlParse
exports.yamlStringify            = yamlStringify
