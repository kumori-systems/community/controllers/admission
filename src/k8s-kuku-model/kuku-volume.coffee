###
* Copyright 2022 Kumori Systems S.L.

* Licensed under the EUPL, Version 1.2 or – as soon they
  will be approved by the European Commission - subsequent
  versions of the EUPL (the "Licence");

* You may not use this work except in compliance with the
  Licence.

* You may obtain a copy of the Licence at:

  https://joinup.ec.europa.eu/software/page/eupl

* Unless required by applicable law or agreed to in
  writing, software distributed under the Licence is
  distributed on an "AS IS" basis,

* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
  express or implied.

* See the Licence for the specific language governing
  permissions and limitations under the Licence.
###

KukuElement = require './kuku-element'

# This method expects an object with the following format:
#
#    {
#      "ref": {
#        "name": "myvolume",
#        "kind": "volume",
#        "domain": "kumori.examples"
#      },
#      "description": {
#        "items": 5,
#        "parameters": [],
#        "size": "10M",
#        "type": "persistent"
#      },
#      "owner": "devel@kumori.cloud",
#      "name": "kres-144240-c450c4fb",
#      "urn": "eslap://kumori.examples/volume/myvolume"
#    }
#
#
# It will generate a KukuVolume (sample YAML):
#
#   apiVersion: kumori.systems/v1
#   kind: KukuVolume
#   metadata:
#     name: kv-101337-b4760ca2
#     namespace: kumori
#     labels:
#       'kumori/name': myvolume
#       'kumori/domain': jbgisbert.examples
#       'kumori/owner': devel__arroba__kumori.cloud
#     annotations:
#       'kumori/manifest': ...
#       'kumori/lastModification': ...
#   spec:
#     items: 5
#     parameters: []
#     size: 10M
#     type: persistent
#   status:
#     items: 1


# These are inherited from super class KumoriElement:
# GROUP       = 'kumori.systems'
# VERSION     = 'v1'
# NAMESPACE   = 'kumori'
# API_VERSION = "#{GROUP}/#{VERSION}"

KIND   = 'KukuVolume'
PLURAL = 'kukuvolumes'

DEFAULT_ITEMS      = -1
DEFAULT_SIZE_UNIT  = 'Gi'


class KukuVolume extends KukuElement

  # Class method to instantiate a new object with the values from a Kumori
  # volume resource manifest.
  @fromManifest: (manifest, helper) ->
    # @logger.info "KukuVolume.fromManifest #{manifest}"
    # console.log "KukuVolume.fromManifest #{manifest}"

    kukuVolume = new KukuVolume manifest, helper

    # Set KukuVolume size. If no unit is provided, add default unit
    if manifest.description?.size?
      size = manifest.description.size
      # If size is not a string cast it to a string
      if 'string' isnt typeof size
        size = "#{size}"

      # If size string contains only numbers, add the default unit
      if /^[0-9]+$/.test(size)
        size = "#{size}#{DEFAULT_SIZE_UNIT}"

      kukuVolume.setSize size


    # Set KukuVolume items. Default is -1 (unlimited)
    if manifest.description?.items?
      items = manifest.description.items
    else
      items = DEFAULT_ITEMS
    kukuVolume.setItems items


    # Set KukuVolume type. Default is 'persistent'
    if manifest.description?.type?
      kukuVolume.setType manifest.description.type


    # Set KukuVolume parameters.
    if manifest.description?.parameters?
      kukuVolume.setParameters manifest.description.parameters


    kukuVolume.setManifest manifest

    return kukuVolume


  constructor: (manifest, @helper) ->
    super()
    meth = 'KukuVolume.constructor'

    console.log "#{meth} ID: #{manifest.name} - Domain:#{manifest.ref.domain}
                 - Name:#{manifest.ref.name}"

    @setCommonData manifest

    @spec =
      items: null
      parameters: []
      size: null
      type: null



  getKind: () ->
    return KIND


  getPlural: () ->
    return PLURAL


  setType: (type) ->
    @spec.type = type


  setItems: (items) ->
    @spec.items = items


  setSize: (size) ->
    @spec.size = size


  setParameters: (parameters) ->
    @spec.parameters = parameters


module.exports = KukuVolume
