###
* Copyright 2022 Kumori Systems S.L.

* Licensed under the EUPL, Version 1.2 or – as soon they
  will be approved by the European Commission - subsequent
  versions of the EUPL (the "Licence");

* You may not use this work except in compliance with the
  Licence.

* You may obtain a copy of the Licence at:

  https://joinup.ec.europa.eu/software/page/eupl

* Unless required by applicable law or agreed to in
  writing, software distributed under the Licence is
  distributed on an "AS IS" basis,

* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
  express or implied.

* See the Licence for the specific language governing
  permissions and limitations under the Licence.
###

KukuElement = require './kuku-element'

# KukuDomain resource yaml example:
#
# apiVersion: kumori.systems/v1
# kind: KukuDomain
# metadata:
#   name: juanjodomain-123
#   namespace: myns
#   labels:
#     'kumori/name': juanjodomain
#     'kumori/domain': juanjo.kumori.systems
#     'kumori/owner': juanjo__arroba__kumori.cloud
#   annotations:
#     'kumori/manifest': ...
#     'kumori/lastModification': ...
# spec:
#   domain: juanjo.test.kumori.cloud



# These are inherited from super class KumoriElement:
# GROUP       = 'kumori.systems'
# VERSION     = 'v1'
# NAMESPACE   = 'kumori'
# API_VERSION = "#{GROUP}/#{VERSION}"

KIND   = 'KukuDomain'
PLURAL = 'kukudomains'


class KukuDomain extends KukuElement

  # Class method to instantiate a new object with the values from an ECloud
  # domain resource manifest.
  @fromManifest: (manifest, helper) ->

    kukudomain = new KukuDomain manifest, helper

    if manifest.public? and manifest.public
      kukudomain.setPublic true
    else
      kukudomain.setPublic false

    kukudomain.setManifest manifest

    return kukudomain


  constructor: (manifest, @helper) ->
    super()
    meth = 'KukuDomain.constructor'

    console.log "#{meth} ID: #{manifest.name} - Domain:#{manifest.ref.domain}
                 - Name:#{manifest.ref.name}"

    @setCommonData manifest

    @spec = {}

    if manifest.description?.domain?
      @spec.domain = manifest.description.domain


  getKind: () ->
    return KIND


  getPlural: () ->
    return PLURAL


  setPublic: (isPublic) ->
    # Note that boolean is converted to string since it's added to a K8s label
    @metadata.labels['kumori/public'] = "#{isPublic}"


  validate: () ->
    # Label "name" contains the manifest.json.ref.name.
    # This name must be a valid element name.
    name = @metadata.labels['kumori/name']
    if not @helper.isValidElementName(name)
      throw new Error "Domain name '#{name}' is not a valid name."
    return

module.exports = KukuDomain
