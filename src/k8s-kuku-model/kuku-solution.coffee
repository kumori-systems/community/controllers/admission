###
* Copyright 2022 Kumori Systems S.L.

* Licensed under the EUPL, Version 1.2 or – as soon they
  will be approved by the European Commission - subsequent
  versions of the EUPL (the "Licence");

* You may not use this work except in compliance with the
  Licence.

* You may obtain a copy of the Licence at:

  https://joinup.ec.europa.eu/software/page/eupl

* Unless required by applicable law or agreed to in
  writing, software distributed under the Licence is
  distributed on an "AS IS" basis,

* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
  express or implied.

* See the Licence for the specific language governing
  permissions and limitations under the Licence.
###

_           = require 'lodash'
KukuElement = require './kuku-element'

# KukuSolution resource yaml example:
#
#   apiVersion: kumori.systems/v1
#   kind: KukuSolution
#   metadata:
#     name: calculator-hazelcast-solution
#     namespace: kumori
#     labels:
#      kumori/name: hash of the 'kumori/name' annotation
#      kumori/domain: hash of the 'kumori/domain' annotation
#      kumori/owner: hash of the 'kumori/owner' annotation
#     annotations:
#      kumori/name: calculator-hazelcast-solution
#      kumori/domain: integrations.kumori.systems
#      kumori/owner: juanjo__arroba__kumori.systems
#      [...]
#   spec:
#     [...]

# These are inherited from super class KumoriElement:
# GROUP       = 'kumori.systems'
# VERSION     = 'v1'
# NAMESPACE   = 'kumori'
# API_VERSION = "#{GROUP}/#{VERSION}"


KIND   = 'KukuSolution'
PLURAL = 'kukusolutions'


class KukuSolution extends KukuElement

  @fromManifest: (manifest, helper) ->

    # Create a list of used resources to add labels later on.
    # Also remove the resource ID added by Admission previously
    usedResources = {}

    if manifest.usedResources?
      for resID, resInfo of manifest.usedResources
        usedResources[resID] = 'resource-in-use'
      delete manifest.usedResources


    solution = new KukuSolution manifest, helper

    # Add a label for each resource used by this solution
    solution.addResourceLabels usedResources

    solution.setManifest manifest

    return solution


  constructor: (manifest, @helper) ->
    super()
    meth = 'KukuSolution.constructor'

    console.log "#{meth} ID: #{manifest.name} - Domain:#{manifest.ref.domain}
                 - Name:#{manifest.ref.name}"

    @setCommonData manifest

    # Set type specific stuff
    @metadata.annotations['kumori/comment'] = manifest.comment

    # Remove unnecessary properties before putting the data in  the Kuku object
    tmpManifest = _.cloneDeep manifest
    delete tmpManifest.name
    delete tmpManifest.owner
    delete tmpManifest.urn
    delete tmpManifest.comment
    delete tmpManifest.creationTimestamp
    delete tmpManifest.lastModification
    @spec = tmpManifest


  getKind: () ->
    return KIND


  getPlural: () ->
    return PLURAL


  addResourceLabels: (usedResources) ->
    for k, v of usedResources
      @metadata.labels[k] = v


  validate: () ->
    # Labels "name" and ".name" contain the name of the solution (used
    # in manifest.ref.name).
    # This name must be a valid element name.
    # name = @metadata.labels['name']
    # if not @helper.isValidElementName(name)
    #   throw new Error "Solution name '#{name}' is not a valid name."
    # solutionName = @metadata.labels['kumori/solution.name']
    # if not @helper.isValidElementName(solutionName)
    #   throw new Error "Solution name '#{solutionName}' is not a valid name."
    return



module.exports = KukuSolution
