###
* Copyright 2022 Kumori Systems S.L.

* Licensed under the EUPL, Version 1.2 or – as soon they
  will be approved by the European Commission - subsequent
  versions of the EUPL (the "Licence");

* You may not use this work except in compliance with the
  Licence.

* You may obtain a copy of the Licence at:

  https://joinup.ec.europa.eu/software/page/eupl

* Unless required by applicable law or agreed to in
  writing, software distributed under the Licence is
  distributed on an "AS IS" basis,

* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
  express or implied.

* See the Licence for the specific language governing
  permissions and limitations under the Licence.
###

_           = require 'lodash'
KukuElement = require './kuku-element'

# KukuV3Deployment resource yaml example:
#
#   apiVersion: kumori.systems/v1
#   kind: V3Deployment
#   metadata:
#     name: hazelcast-deployment
#     namespace: kumori
#     labels:
#      'kumori/name': hazelcast-deployment
#      'kumori/domain': integrations.kumori.systems
#      'kumori/owner': juanjo__arroba__kumori.systems
#   spec:
#     spec: ...
#     ref: ...
#     description: ...
#     scope: ...

# These are inherited from super class KumoriElement:
# GROUP       = 'kumori.systems'
# VERSION     = 'v1'
# NAMESPACE   = 'kumori'
# API_VERSION = "#{GROUP}/#{VERSION}"


KIND   = 'V3Deployment'
PLURAL = 'v3deployments'


class KukuV3Deployment extends KukuElement

  @fromManifest: (manifest, helper) ->

    # Create a list of used resources to add labels later on.
    # Also remove the resource ID added by Admission previously
    usedResources = {}

    if manifest.usedResources?
      for resID, resInfo of manifest.usedResources
        usedResources[resID] = 'resource-in-use'
      delete manifest.usedResources


    kukuv3depl = new KukuV3Deployment manifest, helper

    # Add a label for each resource used by this deployment
    kukuv3depl.addResourceLabels usedResources

    return kukuv3depl


  constructor: (manifest, @helper) ->
    super()
    meth = 'KukuV3Deployment.constructor'

    console.log "#{meth} ID: #{manifest.name} - Domain:#{manifest.ref.domain}
                 - Name:#{manifest.ref.name}"

    @setCommonData manifest

    # Set type specific stuff
    @metadata.annotations['kumori/comment'] = manifest.comment

    # Remove name and nickname properties before creating the Kuku object
    tmpManifest = _.cloneDeep manifest
    delete tmpManifest.name
    delete tmpManifest.owner
    delete tmpManifest.urn
    delete tmpManifest.comment
    @spec = tmpManifest


  getKind: () ->
    return KIND


  getPlural: () ->
    return PLURAL


  addResourceLabels: (usedResources) ->
    for k, v of usedResources
      @metadata.labels[k] = v


  validate: () ->
    # This name must be a valid element name.
    name = @metadata.labels['kumori/name']
    if not @helper.isValidElementName(name)
      throw new Error "Deployment name '#{name}' is not a valid name."
    return



module.exports = KukuV3Deployment
