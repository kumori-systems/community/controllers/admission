###
* Copyright 2020 Kumori Systems S.L.

* Licensed under the EUPL, Version 1.2 or – as soon they
  will be approved by the European Commission - subsequent
  versions of the EUPL (the "Licence");

* You may not use this work except in compliance with the
  Licence.

* You may obtain a copy of the Licence at:

  https://joinup.ec.europa.eu/software/page/eupl

* Unless required by applicable law or agreed to in
  writing, software distributed under the Licence is
  distributed on an "AS IS" basis,

* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
  express or implied.

* See the Licence for the specific language governing
  permissions and limitations under the Licence.
###


utils          = require '../utils'
ManifestHelper = require '../manifest-helper'

# Regular expressions
regexElementName = new RegExp('^[a-z0-9]([-a-z0-9]*[a-z0-9])?$')


ESCAPE_CHARS = {
  '@': '__arroba__'
}


TYPES_SOLUTION = [
  'solution'
  'solutions'
  'kukusolution'
  'kukusolutions'
]
TYPES_V3DEPLOYMENT = [
  'deployment'
  'deployments'
  'v3deployment'
  'v3deployments'
]
TYPES_LINK = [
  'link'
  'links'
  'kukulink'
  'kukulinks'
]
TYPES_RES_CERT = [
  'certificate'
  'certificates'
  'kukucert'
  'kukucerts'
]
TYPES_RES_SECRET = [
  'secret'
  'secrets'
  'kukusecret'
  'kukusecrets'
]
TYPES_RES_PORT = [
  'port'
  'ports'
  'kukuport'
  'kukuports'
]
TYPES_RES_DOMAIN = [
  'domain'
  'domains'
  'kukudomain'
  'kukudomains'
]
TYPES_RES_CA = [
  'ca'
  'cas'
  'kukuca'
  'kukucas'
]
TYPES_RES_VOLUME = [
  'volume'
  'volumes'
  'kukuvolume'
  'kukuvolumes'
]



class KukuModelHelper


  constructor: (@config = {})->
    # @logger.info 'KukuModelHelper.constructor'
    console.log 'KukuModelHelper.constructor'
    @manifestHelper = new ManifestHelper()


  # Methods for identifying Manifests (delegated on Manifest Helper)
  isSolution: (manifest)       -> @manifestHelper.isSolution manifest
  isKmv3Deployment: (manifest) -> @manifestHelper.isKmv3Deployment manifest
  isSecret: (manifest)         -> @manifestHelper.isSecret manifest
  isPort: (manifest)           -> @manifestHelper.isPort manifest
  isVolume: (manifest)         -> @manifestHelper.isVolume manifest
  isLink: (manifest)           -> @manifestHelper.isLink manifest
  isDomain: (manifest)         -> @manifestHelper.isDomain manifest
  isCA: (manifest)             -> @manifestHelper.isCA manifest
  isCertificate: (manifest)    -> @manifestHelper.isCertificate manifest


  # Escape forbidden characters from username
  escapeUsername: (user) ->
    for k, v of ESCAPE_CHARS
      user = user.split(k).join(v)
    return user


  getPluralFromType: (type) ->
    if type.toLowerCase() in TYPES_SOLUTION
      return 'kukusolutions'
    else if type.toLowerCase() in TYPES_V3DEPLOYMENT
      return 'v3deployments'
    else if type.toLowerCase() in TYPES_LINK
      return 'kukulinks'
    else if type.toLowerCase() in TYPES_RES_CERT
      return 'kukucerts'
    else if type.toLowerCase() in TYPES_RES_SECRET
      return 'kukusecrets'
    else if type.toLowerCase() in TYPES_RES_PORT
      return 'kukuports'
    else if type.toLowerCase() in TYPES_RES_DOMAIN
      return 'kukudomains'
    else if type.toLowerCase() in TYPES_RES_VOLUME
      return 'kukuvolumes'
    else if type.toLowerCase() in TYPES_RES_CA
      return 'kukucas'
    else
      return null


  compressManifest: (manifestStr) ->
    return utils.gzipString manifestStr


  decompressManifest: (manifestStr) ->
    return utils.gunzipString manifestStr


  isValidElementName: (name) ->
    # Element names are used as label values and label names (more restricted syntax)
    #
    # Therefore, element names must meet the restrictions of the label names
    #
    # Kubernetes labels names :
    # << DNS Label: an alphanumeric (a-z, and 0-9) string, with a maximum length
    # of 63 characters, with the - character allowed anywhere except the first
    # or last character, suitable for use as a hostname or segment in a domain
    # name. There are two variations:
    # - rfc1035: based on regexp [a-z]([-a-z0-9]*[a-z0-9])?
    # - rfc1123: based on regexp [a-z0-9]([-a-z0-9]*[a-z0-9])?  >>
    #
    # TODO (01/2022): Review the above comments and regexes, since Kubernetes
    # also accepts underscores (_) and dots (.) in the labels names and values.
    if not name?
      return false
    if name.length > 63
      return false
    if not name.match regexElementName
      return false
    return true


  # Returns a timestamp string in the format "2022-01-26T18:50:22Z"
  getTimestamp: () ->
    utils.getTimestamp()


  # Returns a hash of the provided string
  getHash: (str) ->
    return utils.getHash str


module.exports = KukuModelHelper
