###
* Copyright 2022 Kumori Systems S.L.

* Licensed under the EUPL, Version 1.2 or – as soon they
  will be approved by the European Commission - subsequent
  versions of the EUPL (the "Licence");

* You may not use this work except in compliance with the
  Licence.

* You may obtain a copy of the Licence at:

  https://joinup.ec.europa.eu/software/page/eupl

* Unless required by applicable law or agreed to in
  writing, software distributed under the Licence is
  distributed on an "AS IS" basis,

* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
  express or implied.

* See the Licence for the specific language governing
  permissions and limitations under the Licence.
###

KukuElement = require './kuku-element'

# KukuCert resource yaml example:
#
# apiVersion: kumori.systems/v1
# kind: KukuCert
# metadata:
#   name: juanjocert-456
#   namespace: kumoriingress
#   labels:
#     domain: juanjo.kumori.systems
#     name: juanjocert
#     version: null
# data:
#   domain: juanjo.test.kumori.cloud
#   cert: "LS0t...S0tCg=="
#   key: "LS0t...S0tLS0K"



# These are inherited from super class KumoriElement:
# GROUP       = 'kumori.systems'
# VERSION     = 'v1'
# NAMESPACE   = 'kumori'
# API_VERSION = "#{GROUP}/#{VERSION}"

KIND   = 'KukuCert'
PLURAL = 'kukucerts'


class KukuCert extends KukuElement

  # Class method to instantiate a new object with the values from an ECloud
  # cert resource manifest.
  @fromManifest: (manifest, helper) ->

    kukucert = new KukuCert manifest, helper

    if manifest.description?.domain?
      kukucert.setDomain manifest.description.domain

    if manifest.description?.cert?
      kukucert.setCert manifest.description.cert

    if manifest.description?.key?
      kukucert.setKey manifest.description.key

    if manifest.public? and manifest.public
      kukucert.setPublic true
    else
      kukucert.setPublic false

    kukucert.setManifest manifest

    return kukucert



  constructor: (manifest, @helper) ->
    super()
    meth = 'KukuCert.constructor'

    console.log "#{meth} ID: #{manifest.name} - Domain:#{manifest.ref.domain}
                 - Name:#{manifest.ref.name}"

    @setCommonData manifest

    @data =
      domain: null
      cert: null
      key: null


  getKind: () ->
    return KIND


  getPlural: () ->
    return PLURAL


  setDomain: (domain) ->
    @data.domain = domain


  setCert: (cert) ->
    @data.cert = cert


  setKey: (key) ->
    @data.key = key


  setPublic: (isPublic) ->
    # Note that boolean is converted to string since it's added to a K8s label
    @metadata.labels['kumori/public'] = "#{isPublic}"


  validate: () ->
    # Label "name" contains the manifest.json.ref.name.
    # This name must be a valid element name.
    name = @metadata.labels['kumori/name']
    if not @helper.isValidElementName(name)
      throw new Error "Certificate name '#{name}' is not a valid name."
    return


module.exports = KukuCert
