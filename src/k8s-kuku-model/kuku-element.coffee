###
* Copyright 2022 Kumori Systems S.L.

* Licensed under the EUPL, Version 1.2 or – as soon they
  will be approved by the European Commission - subsequent
  versions of the EUPL (the "Licence");

* You may not use this work except in compliance with the
  Licence.

* You may obtain a copy of the Licence at:

  https://joinup.ec.europa.eu/software/page/eupl

* Unless required by applicable law or agreed to in
  writing, software distributed under the Licence is
  distributed on an "AS IS" basis,

* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
  express or implied.

* See the Licence for the specific language governing
  permissions and limitations under the Licence.
###

yaml  = require 'js-yaml'
utils = require '../utils'

GROUP       = 'kumori.systems'
VERSION     = 'v1'
NAMESPACE   = 'kumori'
API_VERSION = "#{GROUP}/#{VERSION}"


class KukuElement

  constructor: () ->
    return true


  setCommonData: (manifest) ->

    # Calculate a string version of the manifest and its sha1 hash
    manifestStr = JSON.stringify manifest
    hash = @helper.getHash manifestStr


    @apiVersion = @getApiVersion()
    @kind = @getKind()
    @metadata =
      name: manifest.name
      namespace: @getNamespace()
      labels:
        'kumori/name': utils.getFnvHash manifest.ref.name
        'kumori/domain': utils.getFnvHash manifest.ref.domain
        'kumori/owner': utils.getFnvHash manifest.owner
      annotations:
        'kumori/name': manifest.ref.name
        'kumori/domain': manifest.ref.domain
        'kumori/owner': manifest.owner
        'kumori/lastModification': @helper.getTimestamp()
        'kumori/sha': hash


  getGroup: () ->
    return GROUP


  getVersion: () ->
    return VERSION


  getApiVersion: () ->
    return API_VERSION


  getNamespace: () ->
    return NAMESPACE


  setResourceVersion: (resourceVersion) ->
    @metadata.resourceVersion = resourceVersion


  setManifest: (manifest) ->
    manifestStr = JSON.stringify manifest
    compressedManifest = @helper.compressManifest manifestStr
    @metadata.annotations['kumori/manifest'] = compressedManifest


  setManifestInSpec: (manifest) ->
    manifestStr = JSON.stringify manifest
    compressedManifest = @helper.compressManifest manifestStr
    @spec.kumoriManifest = compressedManifest


  serialize: (format = 'json') ->
    if format.toLowerCase() is 'json'
      return JSON.stringify this
    else if format.toLowerCase() is 'yaml'
      # We could also use:   yaml.safeDump this
      return yaml.dump this
    else
      return JSON.stringify this


  ##############################################################################
  ##        THE FOLLOWING METHODS CAN BE REDEFINED IN CHILD CLASSES           ##
  ##############################################################################

  validate: () ->
    # This method can be refined in child classes
    # Its purpose is to validate any aspect of the element (e.g. the syntax of
    # its name)
    return


  ##############################################################################
  ##        THE FOLLOWING METHODS MUST BE IMPLEMENTED IN CHILD CLASSES        ##
  ##############################################################################

  # Class method to instantiate a new object with the values from an ECloud
  # manifest.
  @fromManifest: (manifest, helper) ->
    throw new Error 'NOT IMPLEMENTED IN BASE ABSTRACT CLASS'


  getKind: () ->
    throw new Error 'NOT IMPLEMENTED IN BASE ABSTRACT CLASS'


  getPlural: () ->
    throw new Error 'NOT IMPLEMENTED IN BASE ABSTRACT CLASS'



module.exports = KukuElement
