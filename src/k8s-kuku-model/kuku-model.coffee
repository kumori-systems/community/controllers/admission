###
* Copyright 2020 Kumori Systems S.L.

* Licensed under the EUPL, Version 1.2 or – as soon they
  will be approved by the European Commission - subsequent
  versions of the EUPL (the "Licence");

* You may not use this work except in compliance with the
  Licence.

* You may obtain a copy of the Licence at:

  https://joinup.ec.europa.eu/software/page/eupl

* Unless required by applicable law or agreed to in
  writing, software distributed under the Licence is
  distributed on an "AS IS" basis,

* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
  express or implied.

* See the Licence for the specific language governing
  permissions and limitations under the Licence.
###

KukuCA               = require './kuku-ca'
KukuCert             = require './kuku-cert'
KukuLink             = require './kuku-link'
KukuSecret           = require './kuku-secret'
KukuSolution         = require './kuku-solution'
KukuPort             = require './kuku-port'
KukuV3Deployment     = require './kuku-kmv3-deployment'
KukuDomain           = require './kuku-domain'
KukuVolume           = require './kuku-volume'

KukuModelHelper      = require './kuku-model-helper'

class KukuModel

  constructor: (@config = {})->
    @logger.info 'KukuModel.constructor'
    if not @logger?
      @logger = {}
      @logger.error = console.log
      @logger.warn  = console.log
      @logger.info  = console.log
      @logger.debug = console.log
      @logger.silly = console.log

    @logger.info 'KukuModel.constructor'
    @helper = new KukuModelHelper()


  # Instantiate a new KukuModel instance from an ECloud manifest.
  fromManifest: (manifest) ->
    meth = 'KukuModel.fromManifest'
    @logger.info "#{meth}"

    if @helper.isSolution manifest
      # @logger.info "#{meth} Create KukuSolution of #{JSON.stringify manifest}"
      @logger.info "#{meth} Create KukuSolution of #{JSON.stringify manifest.ref || {}}"
      return KukuSolution.fromManifest manifest, @helper
    else if @helper.isKmv3Deployment manifest
      # @logger.info "#{meth} Create KukuV3Deployment of #{JSON.stringify manifest}"
      @logger.info "#{meth} Create KukuV3Deployment of #{JSON.stringify manifest.ref || {}}"
      return KukuV3Deployment.fromManifest manifest, @helper
    else if @helper.isLink manifest
      # @logger.info "#{meth} Create KukuLink of #{JSON.stringify manifest}"
      @logger.info "#{meth} Create KukuLink of #{JSON.stringify manifest.ref || {}}"
      return KukuLink.fromManifest manifest, @helper
    else if @helper.isDomain manifest
      # @logger.info "#{meth} Create KukuDomain of #{JSON.stringify manifest}"
      @logger.info "#{meth} Create KukuDomain of #{JSON.stringify manifest.ref || {}}"
      return KukuDomain.fromManifest manifest, @helper
    else if @helper.isCertificate manifest
      # @logger.info "#{meth} Create KukuCert of #{JSON.stringify manifest}"
      @logger.info "#{meth} Create KukuCert of #{JSON.stringify manifest.ref || {}}"
      return KukuCert.fromManifest manifest, @helper
    else if @helper.isSecret manifest
      # @logger.info "#{meth} Create KukuSecret of #{JSON.stringify manifest}"
      @logger.info "#{meth} Create KukuSecret of #{JSON.stringify manifest.ref || {}}"
      return KukuSecret.fromManifest manifest, @helper
    else if @helper.isPort manifest
      # @logger.info "#{meth} Create KukuPort of #{JSON.stringify manifest}"
      @logger.info "#{meth} Create KukuPort of #{JSON.stringify manifest.ref || {}}"
      return KukuPort.fromManifest manifest, @helper
    else if @helper.isVolume manifest
      # @logger.info "#{meth} Create KukuVolume of #{JSON.stringify manifest}"
      @logger.info "#{meth} Create KukuVolume of #{JSON.stringify manifest.ref || {}}"
      return KukuVolume.fromManifest manifest, @helper
    else if @helper.isCA manifest
      # @logger.info "#{meth} Create KukuCA of #{JSON.stringify manifest}"
      @logger.info "#{meth} Create KukuCA of #{JSON.stringify manifest.ref || {}}"
      return KukuCA.fromManifest manifest, @helper
    else
      errMsg = "Element type not supported (#{manifest.spec})"
      @logger.error "#{meth} - #{errMsg}"
      throw new Error errMsg


module.exports = KukuModel

