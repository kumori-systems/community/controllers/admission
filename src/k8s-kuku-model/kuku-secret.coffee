###
* Copyright 2022 Kumori Systems S.L.

* Licensed under the EUPL, Version 1.2 or – as soon they
  will be approved by the European Commission - subsequent
  versions of the EUPL (the "Licence");

* You may not use this work except in compliance with the
  Licence.

* You may obtain a copy of the Licence at:

  https://joinup.ec.europa.eu/software/page/eupl

* Unless required by applicable law or agreed to in
  writing, software distributed under the Licence is
  distributed on an "AS IS" basis,

* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
  express or implied.

* See the Licence for the specific language governing
  permissions and limitations under the Licence.
###

KukuElement = require './kuku-element'

# KukuSecret resource yaml example:
#
# apiVersion: kumori.systems/v1
# kind: KukuSecret
# metadata:
#   name: secretenv
#   namespace: kumori
#   labels:
#     'kumori/name': mysecret
#     'kumori/domain': jb.kumori.systems
#     'kumori/owner': jbgisbert__arroba__kumori.systems
# data: cG90YXRv


# These are inherited from super class KumoriElement:
# GROUP       = 'kumori.systems'
# VERSION     = 'v1'
# NAMESPACE   = 'kumori'
# API_VERSION = "#{GROUP}/#{VERSION}"

KIND   = 'KukuSecret'
PLURAL = 'kukusecrets'


class KukuSecret extends KukuElement

  # Class method to instantiate a new object with the values from an ECloud
  # secret resource manifest.
  @fromManifest: (manifest, helper) ->

    # console.log "SECRET NAME: #{manifest.name}"
    kukusecret = new KukuSecret manifest, helper

    if manifest.public? and manifest.public
      kukusecret.setPublic true
    else
      kukusecret.setPublic false

    kukusecret.setManifest manifest

    return kukusecret


  constructor: (manifest, @helper) ->
    super()
    meth = 'KukuSecret.constructor'

    console.log "#{meth} ID: #{manifest.name} - Domain:#{manifest.ref.domain}
                 - Name:#{manifest.ref.name}"

    @setCommonData manifest

    @spec = {}
    @data = manifest.description.data


  getKind: () ->
    return KIND


  getPlural: () ->
    return PLURAL


  setPublic: (isPublic) ->
    # Note that boolean is converted to string since it's added to a K8s label
    @metadata.labels['kumori/public'] = "#{isPublic}"


  validate: () ->
    # This name must be a valid element name.
    name = @metadata.labels['kumori/name']
    if not @helper.isValidElementName(name)
      throw new Error "Secret name '#{name}' is not a valid name."
    return


module.exports = KukuSecret
