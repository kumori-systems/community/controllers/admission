###
* Copyright 2021 Kumori Systems S.L.

* Licensed under the EUPL, Version 1.2 or – as soon they
  will be approved by the European Commission - subsequent
  versions of the EUPL (the "Licence");

* You may not use this work except in compliance with the
  Licence.

* You may obtain a copy of the Licence at:

  https://joinup.ec.europa.eu/software/page/eupl

* Unless required by applicable law or agreed to in
  writing, software distributed under the Licence is
  distributed on an "AS IS" basis,

* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
  express or implied.

* See the Licence for the specific language governing
  permissions and limitations under the Licence.
###

_           = require 'lodash'
q           = require 'q'
moment      = require 'moment'
Rescheduler = require './rescheduler'

utils = require '../utils'

DESCHEDULER_NAMESPACE = "kube-system"
DESCHEDULER_CONFIGMAP = "descheduler-policy-configmap"

JOB_DESCRIPTION_LABEL = 'kumori-rescheduler-job'

CONFIGMAP_TEMPLATE =
  apiVersion: 'v1'
  kind: 'ConfigMap'
  metadata:
    name: 'descheduler-job-policy-configmap-__SUFFIX__'
    namespace: 'kube-system'
    ownerReferences: [{
      apiVersion: 'batch/v1'
      kind: 'Job'
      name: 'descheduler-job-__SUFFIX__'
      uid: ''
      blockOwnerDeletion: true
    }]
  data:
    "policy.yaml": ''


JOB_TEMPLATE =
  apiVersion: 'batch/v1'
  kind: 'Job'
  metadata:
    name: 'descheduler-job-__SUFFIX__'
    namespace: 'kube-system'
    labels:
      description: JOB_DESCRIPTION_LABEL
      reschedulerType: 'descheduler'
  spec:
    ttlSecondsAfterFinished: 60*60   # One hour
    parallelism: 1
    completions: 1
    template:
      metadata:
        name: 'descheduler-job-pod-__SUFFIX__'
      spec:
        priorityClassName: 'system-cluster-critical'
        containers: [{
          name: 'descheduler'
          image: 'k8s.gcr.io/descheduler/descheduler:v0.22.0'
          volumeMounts: [{
            mountPath: '/policy-dir'
            name: 'policy-volume'
          }]
          command: [ '/bin/descheduler' ]
          args: [
            '--policy-config-file'
            '/policy-dir/policy.yaml'
            '--v'
            '4'
          ]
          resources:
            limits:
              cpu: '500m'
              memory: '256Mi'
            requests:
              cpu: '500m'
              memory: '256Mi'
          securityContext:
            allowPrivilegeEscalation: false
            capabilities:
              drop:
                - 'ALL'
            privileged: false
            readOnlyRootFilesystem: true
            runAsNonRoot: true
        }]
        restartPolicy: 'Never'
        serviceAccountName: 'descheduler-sa'
        terminationGracePeriodSeconds: 30
        tolerations: [{
          effect: 'NoSchedule'
          key: 'node-role.kubernetes.io/master'
          operator: 'Exists'
        },
        {
          effect: 'NoExecute'
          key: 'node.kubernetes.io/not-ready'
          operator: 'Exists'
          tolerationSeconds: 300
        },
        {
          effect: 'NoExecute'
          key: 'node.kubernetes.io/unreachable'
          operator: 'Exists'
          tolerationSeconds: 300
        }]
        volumes: [{
          name: 'policy-volume'
          configMap:
            name: 'descheduler-job-policy-configmap-__SUFFIX__'
          }]

TIMESTAMP_FORMAT = 'YYYYMMDDHHmmss'


class ReschedulerDescheduler extends Rescheduler

  # Expected configuration options:
  #   options:
  #     k8sApiStub: <API stub object>
  constructor: (options) ->
    super 'descheduler'
    @k8sApiStub = options.k8sApiStub


  ##############################################################################
  ##                            PUBLIC METHODS                                ##
  ##############################################################################

  # Returns a promise
  init: () ->
    q true


  # Returns a promise
  terminate: () ->
    q true


  # Creates a rescheduling job and returns a promise
  createJob: (jobConfig = {}) ->
    meth = 'ReschedulerDescheduler.createJob'
    @logger.info "#{meth} - New job config: #{JSON.stringify jobConfig}"

    @validateJobConfig jobConfig
    @logger.info "#{meth} - New job config validated."

    # Create a timestamp to use as suffix
    timestamp = utils.getTimestamp TIMESTAMP_FORMAT

    newPolicy = null

    # Retrieve current Descheduler configuration from the cluster
    @getDeschedulerPolicy()
    .then (deschedulerPolicy) =>
      if not deschedulerPolicy?
        errMsg = "Unable to retrieve current Descheduler configuration."
        @logger.warn "#{meth} - #{errMsg}"
        throw new Error errMsg

      @logger.debug "#{meth} - Found descheduler policy:
                     #{utils.yamlStringify deschedulerPolicy}"

      # Create a new policy by combining the cluster policy and the provided
      # job configuration
      @logger.info "#{meth} - Creating new policy..."
      newPolicy = @mergePolicies deschedulerPolicy, jobConfig
      @logger.info "#{meth} - New policy: #{utils.yamlStringify newPolicy}"

      jobManifest = @prepareJobManifest timestamp

      @logger.info "#{meth} - Creating Job..."
      @createK8sJob jobManifest
    .then (jobObject) =>
      @logger.info "#{meth} - Created Job."
      jobUID = jobObject.metadata.uid
      @logger.info "#{meth} - Job UID: #{jobUID}"
      configMapManifest =
        @prepareConfigMapManifest newPolicy, timestamp, jobUID
      @logger.info "#{meth} - Creating ConfigMap..."
      @createK8sConfigMap configMapManifest
    .then (configMapObject) =>
      @logger.info "#{meth} - Created ConfigMap."
      return true


  # List existing rescheduling jobs (independently of their state)
  listJobs: () ->
    meth = 'ReschedulerDescheduler.listJob'
    @logger.info "#{meth} - Getting Job list"

    @getK8sJobs()
    .then (jobList) =>
      @logger.info "#{meth} - Retrieved list of descheduler Jobs."
      return jobList



  ##############################################################################
  ##                           PRIVATE METHODS                                ##
  ##############################################################################


  prepareJobManifest: (suffix) ->
    meth = 'ReschedulerDescheduler.prepareJobManifest'
    @logger.info "#{meth}"

    jobManifest = _.cloneDeep JOB_TEMPLATE
    jobManifest.metadata.name =
      jobManifest.metadata.name.replace '__SUFFIX__', suffix
    jobManifest.spec.template.metadata.name =
      jobManifest.spec.template.metadata.name.replace '__SUFFIX__', suffix
    jobManifest.spec.template.spec.volumes[0].configMap.name =
      jobManifest.spec.template.spec.volumes[0].configMap.name.replace '__SUFFIX__', suffix

    @logger.debug "#{meth} - Job manifest: \n#{utils.yamlStringify jobManifest}"
    return jobManifest


  prepareConfigMapManifest: (policy, suffix, jobUID) ->
    meth = 'ReschedulerDescheduler.prepareConfigMapManifest'
    @logger.info "#{meth}"

    # Prepare ConfigMap manifest
    configMapManifest = _.cloneDeep CONFIGMAP_TEMPLATE
    configMapManifest.metadata.name =
      configMapManifest.metadata.name.replace '__SUFFIX__', suffix

    configMapManifest.metadata.ownerReferences[0].name =
      configMapManifest.metadata.ownerReferences[0].name.replace '__SUFFIX__', suffix
    configMapManifest.metadata.ownerReferences[0].uid = jobUID
    configMapManifest.data['policy.yaml'] = utils.yamlStringify policy

    @logger.debug "#{meth} - ConfigMap manifest: \n#{utils.yamlStringify configMapManifest}"
    return configMapManifest



  # SAMPLE CLUSTER CONFIGURATION
  #
  # kind: DeschedulerPolicy
  # evictLocalStoragePods: false
  # evictSystemCriticalPods: false
  # ignorePvcPods: false
  # nodeSelector: kumori/role=worker
  # strategies:
  #   RemoveDuplicates:
  #     enabled: false
  #   LowNodeUtilization:
  #     enabled: false
  #     params:
  #       thresholdPriorityClassName: kumori-platform
  #       nodeResourceUtilizationThresholds:
  #         thresholds:
  #           cpu: 30
  #           memory: 30
  #           pods: 30
  #         targetThresholds:
  #           cpu: 60
  #           memory: 60
  #           pods: 60
  #   HighNodeUtilization:
  #     enabled: false
  #   RemovePodsViolatingInterPodAntiAffinity:
  #     enabled: false
  #   RemovePodsViolatingNodeAffinity:
  #     enabled: false
  #   RemovePodsViolatingNodeTaints:
  #     enabled: false
  #   RemovePodsViolatingTopologySpreadConstraint:
  #     enabled: true
  #     params:
  #       includeSoftConstraints: true
  #       nodeFit: true
  #       thresholdPriorityClassName: kumori-platform
  #       namespaces:
  #         include:
  #           - kumori
  #   RemovePodsHavingTooManyRestarts:
  #     enabled: false
  #   PodLifeTime:
  #     enabled: false
  #
  #
  #
  # SAMPLE JOB CONFIG
  #
  # {
  #   "lowNodeUtilization": {
  #     "enabled": false,
  #     "threshold": {
  #       "cpu": 30,
  #       "memory": 30,
  #       "pods": 30
  #     },
  #     "target": {
  #       "cpu": 60,
  #       "memory": 60,
  #       "pods": 60
  #     }
  #   },
  #   "spreadConstraintsViolation": {
  #     "enabled": true
  #   }
  # }
  mergePolicies: (deschedulerPolicy, jobConfig = {}) ->

    # If custom job config is empty return the original one untouched
    if Object.keys(jobConfig).length is 0
      return deschedulerPolicy

    # Combine both objects. BE CAREFUL WITH CASE DIFFERENCES !
    if jobConfig.lowNodeUtilization?.enabled?
      deschedulerPolicy.strategies.LowNodeUtilization.enabled =
        jobConfig.lowNodeUtilization.enabled
    if jobConfig.lowNodeUtilization?.threshold?
      deschedulerPolicy.strategies.LowNodeUtilization.params.nodeResourceUtilizationThresholds.thresholds =
        jobConfig.lowNodeUtilization.threshold
    if jobConfig.lowNodeUtilization?.target?
      deschedulerPolicy.strategies.LowNodeUtilization.params.nodeResourceUtilizationThresholds.targetThresholds =
        jobConfig.lowNodeUtilization.target

    if jobConfig.spreadConstraintsViolation?.enabled?
      deschedulerPolicy.strategies.RemovePodsViolatingTopologySpreadConstraint.enabled =
        jobConfig.spreadConstraintsViolation.enabled

    return deschedulerPolicy


  getDeschedulerPolicy: () ->
    meth = 'ReschedulerDescheduler.getDeschedulerPolicy'
    @logger.info "#{meth} - Looking for Descheduler configuration..."
    @k8sApiStub.getConfigMap DESCHEDULER_NAMESPACE, DESCHEDULER_CONFIGMAP
    .then (configMap) =>
      @logger.info "#{meth} - Found Descheduler configuration."
      # Extract policy from 'data."policy.yaml"' property'
      if configMap?.data?['policy.yaml']?
        policyStr = configMap.data['policy.yaml']
        @logger.debug "#{meth} - Descheduler policy (string): #{policyStr}"
        parsedPolicy = @parseYaml policyStr
        return parsedPolicy
      else
        @logger.warn "#{meth} - Descheduler policy not found."
        return null
    .catch (err) =>
      @logger.warn "#{meth} - Descheduler configuration not found in cluster."
      return null


  createK8sConfigMap: (configMapManifest) ->
    @k8sApiStub.createConfigMap DESCHEDULER_NAMESPACE, configMapManifest


  createK8sJob: (jobManifest) ->
    @k8sApiStub.createJob DESCHEDULER_NAMESPACE, jobManifest


  getK8sJobs: () ->
    selector =
      description: JOB_DESCRIPTION_LABEL
      reschedulerType: @type

    @k8sApiStub.getJobs DESCHEDULER_NAMESPACE, selector
    .then (list) =>
      for item in list
        delete item.metadata.managedFields
      return list


  validateJobConfig: (jobConfig) ->
    # Not implemented
    return jobConfig


  parseYaml: (yamlStr) ->
    meth = 'ReschedulerDescheduler.parseYaml'
    @logger.info "#{meth}"
    try
      yamlObj = utils.yamlParse yamlStr
      return yamlObj
    catch e
      @logger.warn "#{meth} - Couldn't parse YAML: \"#{yamlStr}\". Reason:
                    #{e.message}"
      return null



module.exports = ReschedulerDescheduler
