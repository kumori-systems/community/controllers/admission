###
* Copyright 2020 Kumori Systems S.L.

* Licensed under the EUPL, Version 1.2 or – as soon they
  will be approved by the European Commission - subsequent
  versions of the EUPL (the "Licence");

* You may not use this work except in compliance with the
  Licence.

* You may obtain a copy of the Licence at:

  https://joinup.ec.europa.eu/software/page/eupl

* Unless required by applicable law or agreed to in
  writing, software distributed under the Licence is
  distributed on an "AS IS" basis,

* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
  express or implied.

* See the Licence for the specific language governing
  permissions and limitations under the Licence.
###

q    = require 'q'

ADMIN_ROLES  = [ "admin", "administrator", "ADMIN", "ADMINISTRATOR" ]

class Authorization

  constructor: (@store, @planner)->
    @logger.info 'Authorization.constructor'
    @ownerCache = {}


  isAdmin: (context)->
    roles = context.user.roles
    for adminLabel in ADMIN_ROLES
      if roles.indexOf(adminLabel) isnt -1
        return true
    return false


  # Checks if user is allowed to access a manifest
  isAllowedForManifest: (manifest, context, write = false) ->
    meth = "Authorization.isAllowedForManifest name=#{manifest.name},
            #{context.user?.id})"
    if write
      meth += ', WritePermission'

    if @isAdmin(context)
      @logger.info "#{meth} - Access allowed (admin)"
      return true

    if not manifest.owner?
      @logger.info "#{meth} - Access allowed (public element)"
      return true

    if manifest.owner is context.user?.id
      @logger.info "#{meth} - Access allowed (user is owner)"
      return true

    if (not write) and manifest.public? and manifest.public
      @logger.info "#{meth} - Access allowed (resource is public)"
      return true

    @logger.warn "#{meth} - Access denied. Instance owner: #{manifest.owner}"
    return false


  # Checks if user is allowed to access an element by type and internal ID
  isAllowed: (elementType, id, context) ->
    meth = "Authorization.isAllowed #{elementType}=#{id}, #{context.user.id})"
    @logger.info

    if @isAdmin context
      @logger.info "#{meth} - Access allowed (admin)"
      return q true

    if elementType is 'instance'
      @planner.getInstanceOwner id
      .then (owner) =>
        if context.user.id is owner
          @logger.info "#{meth} - Instance owner: #{owner} - Access granted."
          return true
        else
          @logger.info "#{meth} - Instance owner: #{owner} - Access denied."
          return false
      .catch (err) =>
        errMsg = "Couldn't verify access: #{err.message}"
        @logger.info "#{meth} - #{errMsg}"
        return false
    else if elementType is 'deployment'
      @planner.getDeploymentOwner id
      .then (owner) =>
        if context.user.id is owner
          @logger.info "#{meth} - Deployment owner: #{owner} - Access granted."
          return true
        else
          @logger.info "#{meth} - Deployment owner: #{owner} - Access denied."
          return false
      .catch (err) =>
        errMsg = "Couldn't verify access: #{err.message}"
        @logger.info "#{meth} - #{errMsg}"
        return false
    else
      q true


  # Checks if user is allowed to access an element by URN
  isAllowedForURN: (elementURN, context, write = false) ->
    meth = "Authorization.isAllowedForURN #{elementURN}, #{context.user.id}"
    if write
      meth += ', WritePermission'
    @logger.info

    if @isAdmin context
      @logger.info "#{meth} - Access allowed (admin)"
      return q true

    # Fetch from manifest store
    @store.getManifestByURN elementURN
    .then (manifest) =>
      @logger.info "#{meth} - Found element manifest. Owner: #{manifest.owner}"
      if context.user.id is manifest.owner
        @logger.info "#{meth} - Access granted."
        return true
      else if (not write) and manifest.public? and manifest.public
        @logger.info "#{meth} - Access granted (element is public)"
        return true

      else
        @logger.info "#{meth} - Access denied."
        return false
    .catch (err) =>
      errMsg = "Couldn't verify access: #{err.message}"
      if err.message?.includes 'Element not found'
        @logger.info "#{meth} - Element not found."
      else
        @logger.error "#{meth} - ERROR: #{errMsg}"
      return false



  # Checks if current user has permission to list resources with the parameters
  # specified in the filter
  canListResources: (context, filter) ->
    @logger.debug  "Authorization.canListResources user:#{context.user.id}, \
                    filter:#{JSON.stringify filter}"

    return q.promise (resolve, reject) =>

      if @isAdmin context
        return resolve()

      if (not filter.owner?) and (not filter.urn?)
        return reject new Error "Unauthorized request from #{context.user.id}
                                 (no data in filter)"

      if filter.owner? and (context.user.id isnt filter.owner)
        return reject new Error "Unauthorized request from #{context.user.id}
                                 (asking for owner #{filter.owner})"

      if filter.urn?
        @isAllowedForURN filter.urn, context
        .then (isAllowed) ->
          if isAllowed
            return resolve()
          else
            throw new Error 'User not allowed to access resource.'
        .catch (err) ->
          return reject err
      else
        resolve()



module.exports = Authorization
