###
* Copyright 2022 Kumori Systems S.L.

* Licensed under the EUPL, Version 1.2 or – as soon they
  will be approved by the European Commission - subsequent
  versions of the EUPL (the "Licence");

* You may not use this work except in compliance with the
  Licence.

* You may obtain a copy of the Licence at:

  https://joinup.ec.europa.eu/software/page/eupl

* Unless required by applicable law or agreed to in
  writing, software distributed under the Licence is
  distributed on an "AS IS" basis,

* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
  express or implied.

* See the Licence for the specific language governing
  permissions and limitations under the Licence.
###

sio = require 'socket.io'

ADMIN_ROLE = 'ADMIN'

EMITTED_EVENTS_NAME = 'ecloud-event'
AUTHENTICATION_ERROR = 'Authentication error'


class WebsocketPublisher

  constructor: (@httpServer, @httpAuthenticator) ->
    @logger.info 'WebsocketPublisher.constructor'

    # Setup a Socket.io websocket to listen on the server
    @socketIO = sio.listen @httpServer

    # Configure Websocket handshake for authentication
    @socketIO.use (socket, next) =>
      @sioHandshaker socket, next

    @socketIO.on 'connection', (socket) =>
      @sioConnectionHandler socket

    @socketsByUserId = {}
    @socketsByAdminId = {}


  sioHandshaker: (socket, next) ->
    meth = 'WSPublisher.handshake'
    @logger.info meth
    # authHeader = socket?.request?.headers?.authorization
    # if not authHeader?
    #   @logger.info "#{meth} - Access denied for anonymous request."
    #   return next new Error AUTHENTICATION_ERROR
    # [protocol, basic] = authHeader.split ' '
    # if protocol isnt 'Basic'
    #   @logger.info "#{meth} - Access denied for incorrect protocol \
    #     #{protocol} request."
    #   return next new Error AUTHENTICATION_ERROR
    # buf = Buffer.from(basic, 'base64')
    # plain_auth = buf.toString()
    # creds = plain_auth.split(':')
    # authToken = "Bearer #{creds[0]}"

    authToken = socket?.handshake?.query?.token
    @logger.info "#{meth} - AuthToken: #{authToken}"
    if (not authToken?) or (authToken is '')
      @logger.info "#{meth} - Access denied for anonymous request."
    @httpAuthenticator.authenticate authToken
    .then (userData) =>
      socket.user = userData
      @logger.info "#{meth} - User authenticated: #{JSON.stringify userData}"
      next()
    .fail (err) =>
      @logger.info "#{meth} - Access denied for token #{authToken}."
      next new Error AUTHENTICATION_ERROR

  sioConnectionHandler: (socket) ->
    meth = 'WSPublisher.onConnection'
    @logger.info meth
    if not socket.user?
      @logger.error "#{meth} - Socket not authenticated correctly."
      socket.emit 'error'
      ,'Connection not authenticated correctly. Disconnecting.'
      socket.disconnect()
    else
      @logger.info "#{meth} - Socket #{socket.id} connected. User: " +
        "#{JSON.stringify socket.user}"
      @addUserSocket socket
      @configureSocketHandlers socket


  configureSocketHandlers: (socket) ->

    socket.on 'disconnect', (reason) =>
      @socketDisconnectionHandler socket, reason

    socket.on 'error', (error) =>
      @socketErrorHandler socket, error


  socketDisconnectionHandler: (socket, reason) ->
    meth = 'WSPublisher.onSocketDisconnection'
    @logger.info "#{meth} - Socket #{socket.id} - Reason: #{reason}"
    @removeUserSocket socket


  socketErrorHandler: (socket, error) ->
    @logger.info "WSPublisher.onSocketError - User: #{socket.user.id} - " +
      "Error: #{error.message}"


  # Add a socket to the user socket list if it's not already there.
  #
  # The user socket list is a two-level dictionary, by user ID and socket ID:
  # {
  #   "user1": {
  #     "sock1" : socketObj1,
  #     "sock2" : socketObj2,
  #     "sock3" : socketObj3
  #   },
  #   "user2": {
  #     "sock5" : socketObj5,
  #     "sock23" : socketObj23,
  #     "sock12" : socketObj12
  #   }
  # }
  addUserSocket: (socket) ->
    meth = 'WebsocketPublisher.addUserSocket'
    userId = socket.user.id
    isAdmin = (ADMIN_ROLE in socket.user.roles)
    if isAdmin
      # For now we just add it to Admin list, since it will receive everything,
      # including its own events.
      if userId not of @socketsByAdminId
        @socketsByAdminId[userId] = {}
      if socket.id not of @socketsByAdminId[userId]
        @socketsByAdminId[userId][socket.id] = socket
        @logger.debug "#{meth} - Socket #{socket.id} added to #{userId} list\
                      for Admins."
      else
        @logger.debug "#{meth} - Socket #{socket.id} already in #{userId} list\
                      for Admins."
    else
      if userId not of @socketsByUserId
        @socketsByUserId[userId] = {}
      if socket.id not of @socketsByUserId[userId]
        @socketsByUserId[userId][socket.id] = socket
        @logger.debug "#{meth} - Socket #{socket.id} added to #{userId} list."
      else
        @logger.debug "#{meth} - Socket #{socket.id} already in #{userId} list."


  # Removes a socket from the user socket list.
  removeUserSocket: (socket) ->
    meth = 'WebsocketPublisher.removeUserSocket'
    userId = socket.user.id
    isAdmin = (ADMIN_ROLE in socket.user.roles)
    if isAdmin
      if userId of @socketsByAdminId
        if socket.id of @socketsByAdminId[userId]
          delete @socketsByAdminId[userId][socket.id]
          @logger.debug "#{meth} - Removed #{socket.id} from #{userId} list for\
                        Admins."
          if Object.keys(@socketsByAdminId[userId]).length is 0
            delete @socketsByAdminId[userId]
            @logger.debug "#{meth} - Removed #{userId} from list for Admins."
        else
          @logger.debug "#{meth} - Socket #{socket.id} not in #{userId} list\
                        for Admins."
      else
        @logger.debug "#{meth} - User #{userId} not in socket list for Admins."
    else
      if userId of @socketsByUserId
        if socket.id of @socketsByUserId[userId]
          delete @socketsByUserId[userId][socket.id]
          @logger.debug "#{meth} - Removed #{socket.id} from #{userId} list."
          if Object.keys(@socketsByUserId[userId]).length is 0
            delete @socketsByUserId[userId]
            @logger.debug "#{meth} - Removed #{userId} from list."
        else
          @logger.debug "#{meth} - Socket #{socket.id} not in #{userId} list."
      else
        @logger.debug "#{meth} - User #{userId} not in socket list."


  publish: (evt) ->
    meth = 'WebsocketPublisher.publish'
    evtStr = JSON.stringify evt
    # Publish event to all admins sockets
    for adminId, adminSockets of @socketsByAdminId
      for sid, s of adminSockets
        @logger.debug "#{meth} - Emitting event to socket #{sid}: #{evtStr}"
        try
          s.emit EMITTED_EVENTS_NAME, evt
        catch err
          @logger.warn "#{meth} - Error emitting: #{err.message} - #{evtStr}"

    # Publish event to all owner user sockets
    if not evt.owner?
      @logger.debug "#{meth} - Event data has no owner: #{evtStr}"
    else if evt.owner not of @socketsByUserId
      @logger.debug "#{meth} - Event owner has no active sockets: #{evtStr}"
    else
      @logger.debug "#{meth} - Emitting event to client sockets: #{evtStr}"
      for sid, s of @socketsByUserId[evt.owner]
        @logger.debug "#{meth} - Emitting event to socket #{sid}: #{evtStr}"
        try
          s.emit EMITTED_EVENTS_NAME, evt
        catch err
          @logger.warn "#{meth} - Error emitting: #{err.message} - #{evtStr}"


  reconfigure: ->
    @logger.info 'WebsocketPublisher.reconfigure'
    # TODO

  disconnect: ->
    @logger.info 'WebsocketPublisher.disconnect'

  terminate: ->
    @logger.info 'WebsocketPublisher.terminate'



module.exports = WebsocketPublisher
