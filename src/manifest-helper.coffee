###
* Copyright 2020 Kumori Systems S.L.

* Licensed under the EUPL, Version 1.2 or – as soon they
  will be approved by the European Commission - subsequent
  versions of the EUPL (the "Licence");

* You may not use this work except in compliance with the
  Licence.

* You may obtain a copy of the Licence at:

  https://joinup.ec.europa.eu/software/page/eupl

* Unless required by applicable law or agreed to in
  writing, software distributed under the Licence is
  distributed on an "AS IS" basis,

* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
  express or implied.

* See the Licence for the specific language governing
  permissions and limitations under the Licence.
###

q     = require 'q'
url   = require 'url'
path  = require 'path'
utils = require './utils'


KIND_SOLUTION             = 'solution'
KIND_KMV3_DEPLOYMENT      = 'deployment'
KIND_RESOURCE_CA          = "ca"
KIND_RESOURCE_CERTIFICATE = "certificate"
KIND_RESOURCE_DOMAIN      = "domain"
KIND_RESOURCE_PORT        = "port"
KIND_RESOURCE_SECRET      = "secret"
KIND_RESOURCE_VOLUME      = "volume"

VALID_RESOURCE_KINDS = [
  KIND_RESOURCE_CA
  KIND_RESOURCE_CERTIFICATE
  KIND_RESOURCE_DOMAIN
  KIND_RESOURCE_PORT
  KIND_RESOURCE_SECRET
  KIND_RESOURCE_VOLUME
]

EXCLUSIVE_RESOURCE_KINDS = [
  KIND_RESOURCE_DOMAIN
  KIND_RESOURCE_PORT
  KIND_RESOURCE_VOLUME
]

UPDATABLE_RESOURCE_KINDS = [
  KIND_RESOURCE_CA
  KIND_RESOURCE_CERTIFICATE
  KIND_RESOURCE_DOMAIN
  KIND_RESOURCE_PORT
  KIND_RESOURCE_SECRET
]



class ManifestHelper

  # Element types
  @DEPLOYMENT: 0
  @SERVICE: 1
  @COMPONENT: 2
  @RESOURCE: 3
  @BLOB: 4
  @RUNTIME: 5
  @LINK: 6
  @BUNDLE: 7
  @TEST: 8
  @SOLUTION: 9

  # Unknown type
  @UNKNOWN: -1

  constructor: () ->
    @logger.info 'ManifestHelper.constructor()'


  ##############################################################################
  ##  KUMORI URN AND REF HANDLING METHODS                                     ##
  ##############################################################################

  # Converts a Kumori Ref object (with the following properties: domain, kind,
  # name) to a Kumori URN 'eslap://<domain>/<kind>/<name>'
  refToUrn: (ref) ->
    p = path.join ref.domain
      , ref.kind
      , ref.name
    'eslap://' + p


  # Converts a Kumori URN 'eslap://<domain>/<kind>/<name>' to a Kumori Ref
  # object with the following properties: domain, kind, name
  urnToRef: (urn) ->
    try

      # url.parse parses a URL in the format <protocol>://<host>/<pathname> and
      # generates an object that includes the following properties (among
      # others):
      # - host
      # - pathname
      parsedURN = url.parse(urn)

      # The 'host' contains our domain
      domain = parsedURN.host

      # Calculate length of:  <protocol>://<domain>/
      removeLength = parsedURN.protocol.length + 3 + domain.length + 1

      # Get the full path (unparsed to support #, ?, etc.)
      customPath = urn.substring(removeLength - 1)

      # Split path by /
      customPathParts = customPath.split('/')

      # Get the kind/type
      kind = customPathParts.shift()  # First element is the element kind

      # Get the name (the remaining parts of the path as they were originally)
      name = customPathParts.join '/'

      # Construct the Ref object to be returned
      result =
        domain: domain
        kind: kind
        name: name


      return result
    catch e
      errMsg = "Error parsing Kumori URN #{urn}: #{e.message || e.stack}"
      @logger.error "ManifestHelper.urnToRef - ERROR: #{errMsg}"
      throw new Error errMsg



  #####################################################
  ## Check type from Manifest                        ##
  #####################################################
  isSolution: (manifest) ->
    return manifest.ref?.kind is KIND_SOLUTION


  isKmv3Deployment: (manifest) ->
    return manifest.ref?.kind is KIND_KMV3_DEPLOYMENT


  isResource: (manifest) ->
    return manifest.ref?.kind in VALID_RESOURCE_KINDS


  isSecret: (manifest) ->
    return manifest.ref?.kind is KIND_RESOURCE_SECRET


  isPort: (manifest) ->
    return manifest.ref?.kind is KIND_RESOURCE_PORT


  isVolume: (manifest) ->
    return manifest.ref?.kind is KIND_RESOURCE_VOLUME


  isDomain: (manifest) ->
    return manifest.ref?.kind is KIND_RESOURCE_DOMAIN


  isCA: (manifest) ->
    return manifest.ref?.kind is KIND_RESOURCE_CA


  isCertificate: (manifest) ->
    return manifest.ref?.kind is KIND_RESOURCE_CERTIFICATE


  isLink: (manifest) ->
    return (manifest.endpoints?) or (manifest['s_d']?)


  #####################################################
  ## Check type from URN                             ##
  #####################################################
  isSolutionURN: (urn) ->
    return @urnToRef(urn).kind is KIND_SOLUTION


  isKmv3DeploymentURN: (urn) ->
    return @urnToRef(urn).kind is KIND_KMV3_DEPLOYMENT


  isResourceURN: (urn) ->
    return @urnToRef(urn).kind in VALID_RESOURCE_KINDS


  isSecretURN: (urn) ->
    return @urnToRef(urn).kind is KIND_RESOURCE_SECRET


  isPortURN: (urn) ->
    return @urnToRef(urn).kind is KIND_RESOURCE_PORT


  isVolumeURN: (urn) ->
    return @urnToRef(urn).kind is KIND_RESOURCE_VOLUME


  isDomainURN: (urn) ->
    return @urnToRef(urn).kind is KIND_RESOURCE_DOMAIN


  isCAURN: (urn) ->
    return @urnToRef(urn).kind is KIND_RESOURCE_CA


  isCertificateURN: (urn) ->
    return @urnToRef(urn).kind is KIND_RESOURCE_CERTIFICATE



  # Determine if an element Kind is a valid resource Kind
  isResourceKind: (kind) ->
    return kind in VALID_RESOURCE_KINDS


  # Get a list of valid resource Kinds
  getValidResourceKinds: () ->
    return VALID_RESOURCE_KINDS


  # Get a list of exclusive resource Kinds
  getExclusiveResourceKinds: () ->
    return EXCLUSIVE_RESOURCE_KINDS


  # Get a list of resource Kinds that support updates
  getUpdatableResourceKinds: () ->
    return UPDATABLE_RESOURCE_KINDS


  # Get the Kind of the element represented by the Manifest
  getManifestType: (manifest) ->
    # coffeelint: disable=max_line_length
    if manifest?.ref?
      if manifest.ref.kind is KIND_SOLUTION
        return ManifestHelper.SOLUTION
      else if manifest.ref.kind is KIND_KMV3_DEPLOYMENT
        return ManifestHelper.DEPLOYMENT
      else if manifest.ref.kind is KIND_RESOURCE_SECRET
        return ManifestHelper.RESOURCE
      else if manifest.ref.kind is KIND_RESOURCE_PORT
        return ManifestHelper.RESOURCE
      else if manifest.ref.kind is KIND_RESOURCE_VOLUME
        return ManifestHelper.RESOURCE
      else if manifest.ref.kind is KIND_RESOURCE_DOMAIN
        return ManifestHelper.RESOURCE
      else if manifest.ref.kind is KIND_RESOURCE_CA
        return ManifestHelper.RESOURCE
      else if manifest.ref.kind is KIND_RESOURCE_CERTIFICATE
        return ManifestHelper.RESOURCE
      else
        return ManifestHelper.UNKNOWN
    else
      return ManifestHelper.UNKNOWN
    # coffeelint: enable=max_line_length


  # Hash kumori label
  hashLabel: (labelStr) ->
    if labelStr?
      return utils.getFnvHash labelStr
    else
      return labelStr


  # Hash container name
  hashContainerName: (containerName) ->
    if containerName?
      return 'k-' + utils.getFnvHash containerName
    else
      return undefined


module.exports = ManifestHelper
