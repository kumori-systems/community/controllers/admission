###
* Copyright 2022 Kumori Systems S.L.

* Licensed under the EUPL, Version 1.2 or – as soon they
  will be approved by the European Commission - subsequent
  versions of the EUPL (the "Licence");

* You may not use this work except in compliance with the
  Licence.

* You may obtain a copy of the Licence at:

  https://joinup.ec.europa.eu/software/page/eupl

* Unless required by applicable law or agreed to in
  writing, software distributed under the Licence is
  distributed on an "AS IS" basis,

* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
  express or implied.

* See the Licence for the specific language governing
  permissions and limitations under the Licence.
###

q       = require 'q'
k8s     = require '@kubernetes/client-node'
stream  = require 'stream'
request = require 'request'

EventEmitter = require('events').EventEmitter


# FOR TESTIG IN MINIKUBE @ LOCALHOST
#
# apiVersion: v1
# clusters:
# - cluster:
#     certificate-authority: /home/jferrer/.minikube/ca.crt
#     server: https://192.168.99.100:8443
#   name: minikube
# contexts:
# - context:
#     cluster: minikube
#     user: minikube
#   name: minikube
# current-context: minikube
# kind: Config
# preferences: {}
# users:
# - name: minikube
#   user:
#     client-certificate: /home/jferrer/.minikube/client.crt
#     client-key: /home/jferrer/.minikube/client.key


DEFAULT_CLUSTER =
  # name: 'my-server'
  # server: 'http://server.com'
  # caBundle: '.....CERTIFICATE.....'
  caFile: '/home/jferrer/.minikube/ca.crt'
  server: 'https://192.168.99.100:8443'
  name: 'minikube'
  skipTLSVerify: false

DEFAULT_USER =
  # name: 'my-user'
  # password: 'my-password'
  name: 'minikube'
  certFile: '/home/jferrer/.minikube/client.crt'
  keyFile: '/home/jferrer/.minikube/client.key'

DEFAULT_CONTEXT =
  # name: 'my-context'
  # user: DEFAULT_USER.name
  # cluster: DEFAULT_CLUSTER.name
  name: 'minikube'
  user: DEFAULT_USER.name
  cluster: DEFAULT_CLUSTER.name

DEFAULT_KUBECONFIG =
  clusters: [ DEFAULT_CLUSTER ]
  users: [ DEFAULT_USER ]
  contexts: [ DEFAULT_CONTEXT ]
  currentContext: DEFAULT_CONTEXT.name


DEFAULT_PROPAGATION_POLICY="Foreground"

ADMISSION_FIELD_MANAGER_NAME="kumori-admission"


class K8sApiStub extends EventEmitter

  # Expected configuration example:
  #
  # {
  #   watch: true,
  #   kubeConfigFile: "/path/to/.kubeconfig",
  #   kubeConfig: {
  #     clusters: [{
  #       name: 'minikube-cluster',
  #       server: 'https://192.168.99.100:8443',
  #       caFile: '/home/jferrer/.minikube/ca.crt',
  #       skipTLSVerify: false
  #     }],
  #     users: [{
  #       name: 'minikube',
  #       certFile: '/home/jferrer/.minikube/client.crt',
  #       keyFile: '/home/jferrer/.minikube/client.key'
  #     },{
  #         name: 'my-user',
  #         password: 'my-password'
  #     }],
  #     contexts: [
  #       name: 'minikube',
  #       user: 'my-user',
  #       cluster: 'minikube-cluster'
  #     ],
  #     currentContext: 'minikube'
  #   }
  constructor: (@config = {})->
    super()
    # For testing
    if not @logger?
      @logger = {}
      @logger.error = console.log
      @logger.warn  = console.log
      @logger.info  = console.log
      @logger.debug = console.log
      @logger.silly = console.log

    meth = 'k8sApiStub.constructor'
    @logger.info meth

    @initialized = false

    # Check if no config
    @config ?= {}

    # Unless explicitly configured, don't listen to K8s events
    @config.watch ?= false

    # if not @config.kubeConfig?
    #   @logger.info "#{meth} - No kubeConfig found. Use default configuration..."
    #   @config.kubeConfig = DEFAULT_KUBECONFIG

    @logger.info "#{meth} - Config=#{JSON.stringify @config}"

    # Ignore TLS errors (required for testing).
    process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = 0

    @kConfig = new k8s.KubeConfig()


    if @config.kubeConfigFile?
      # Load configuration into KubeConfig object from specified file
      @logger.info "#{meth} - Loading K8s config from file:
                    #{@config.kubeConfigFile}"
      @kConfig.loadFromFile @config.kubeConfigFile
    else if @config.kubeConfig
      # Load configuration into KubeConfig object from configuration values
      @logger.info "#{meth} - Loading K8s config from received configuration."
      @kConfig.loadFromOptions @config.kubeConfig
    else
      # No configuration provided, try to load it from  KUBECONFIG environment
      # variable
      @logger.info "#{meth} - Loading K8s config from default (ENV var)."
      @kConfig.loadFromDefault()

    @logger.info "#{meth} - K8s configuration: #{JSON.stringify @kConfig}"
    # Create API client objects
    @k8sApi       = @kConfig.makeApiClient k8s.CoreV1Api
    @k8sAppsApi   = @kConfig.makeApiClient k8s.AppsV1Api
    @k8sCustomApi = @kConfig.makeApiClient k8s.CustomObjectsApi
    @k8sBatchApi  = @kConfig.makeApiClient k8s.BatchV1Api
    @k8sNetApi    = @kConfig.makeApiClient k8s.NetworkingV1Api

    @k8sWatcher = null
    if @config.watch
      @logger.info "#{meth} - K8s events watcher is on."
      @k8sWatcher = new k8s.Watch @kConfig

    @k8sLog = new k8s.Log @kConfig


  init: () ->
    meth = 'k8sApiStub.init'
    @logger.info meth
    if not @initialized
      if @config.watch then @startWatching()
      @initialized = true
    return q true
    # @k8sApi.listNamespacedPod('default')
    # .then (res) =>
    #   console.log "PODS FOR NAMESPACE 'default'"
    #   console.log res.body
    #   @k8sApi.listPodForAllNamespaces()
    # .then (res) =>
    #   console.log "PODS FOR ALL NAMESPACES"
    #   console.log res.body
    #   return true
    # .catch (err) =>
    #   console.log "ERROR GETTING NAMESPACE: #{err.message || err}"
    #   console.log "ERROR: #{JSON.stringify err}"
    #   return true



  terminate: () ->
    meth = 'k8sApiStub.terminate'
    @logger.info meth
    if @config.watch then @stopWatching()
    @initialized = false
    q true


  ##############################################################################
  ##                METHODS FOR RECEIVING LIVE EVENTS STREAM                  ##
  ##############################################################################
  startWatching: () ->
    meth = 'k8sApiStub.startWatching'
    @logger.info meth
    # apiPath = '/api/v1/namespaces'
    apiPath = '/apis/kumori.systems/v1/namespaces/kumori/kukucomponents'
    # apiPath = '/apis/kumori.systems/v1/namespaces/kumori'
    queryParams = {}

    if @lastResourceVersion?
      queryParams.resourceVersion = @lastResourceVersion

    @logger.info "#{meth} - Calling watch..."
    # The watcher REQ object must be kept to be able to terminate it later
    @k8sWatcherReq =
      @k8sWatcher.watch apiPath, queryParams, @watchHandler, @doneHandler
    @logger.info "#{meth} - Watch started!"


  stopWatching: () ->
    meth = 'k8sApiStub.startWatching'
    @logger.info meth
    try
      if @k8sWatcherReq?
        @logger.info "#{meth} - Aborting watch request..."
        @k8sWatcherReq.abort()
        @logger.info "#{meth} - Aborted watch request."
      @k8sWatcher = null
      @k8sWatcherReq = null
    catch err
      @logger.error "#{meth} - ERROR: #{err.message || err}"
      return true


  watchHandler: (evtType, evtObj) =>
    meth = 'k8sApiStub.watchHandler'
    @logger.info meth
    @logger.info "#{meth} - TYPE: #{evtType} - OBJECT: #{JSON.stringify evtObj}"
    if evtObj?.metadata?.resourceVersion?
      @lastResourceVersion = evtObj.metadata.resourceVersion
    # console.log "\n%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
    # console.log "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
    # console.log "%%%%  EVENT TYPE: #{evtType} - OBJECT: #{JSON.stringify evtObj}"
    # console.log "%%%%  UPDATING LAST VERSION TO: #{@lastResourceVersion}"
    # console.log "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
    # console.log "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n"
    ###
    try {
      if (!obj) {
        let msg = `received undefined object for phase ${phase}`
        throw new Error(msg)
      }
      if (obj.metadata.resourceVersion && (obj.metadata.resourceVersion.localeCompare(this.lastResourceVersion || '0') > 0)) {
        this.lastResourceVersion = obj.metadata.resourceVersion
        // console.log(`\n\n\n---->Set lastResourceVersion: ${this.lastResourceVersion}<----\n\n\n`)
      }

      switch (phase) {
        case 'ADDED': {
          this.emit('add', obj)
          break
        }
        case 'MODIFIED': {
          this.emit('update', obj)
          break
        }
        case 'DELETED': {
          this.emit('delete', obj)
          break
        }
        case 'ERROR': {
          this.lastResourceVersion = undefined
          let aux: any = obj
          this.doneHandler(aux as Error)
          break
        }
        default: {
          let msg = `phase ${phase} unknown`
          throw new Error(msg)
        }
      }
    } catch (error) {
      let msg = `KukuInformer.watchHandler. Error: ${error.message}`
      this.emit('error', new Error(msg))
    }
  }
  ###

  doneHandler: (error) =>
    meth = 'k8sApiStub.doneHandler'
    @logger.info meth
    @logger.info "#{meth} - RAW ERROR: #{error}"
    if @k8sWatcherReq
      @logger.info "#{meth} - Reconnecting watcher..."
      try
        @k8sWatcherReq.end()
      catch err
        @logger.info "#{meth} - Previous request can't be closed: ${err.message}"

      @k8sWatcherReq = undefined
    if error
      @emit('error', error)
    @startWatching()
    # @logger.info "#{meth} - ERROR: #{error.message} - #{error.stack}"
    ###
    if (this.req) {
      try {
        this.req.end()
      } catch (error) {
        console.log(`KukuInformer. Previous request found but can't be closed: ${error.message}`)
      }
      this.req = undefined
    }
    if (error) {
      this.emit('error', error)
    }
    this.watch()
    }
    ###


  ##############################################################################
  ##                PUBLIC METHODS TO BE USED FROM OUTSIDE                    ##
  ##############################################################################

  getKumoriElement: (plural, name = '') ->
    meth = 'k8sApiStub.getKumoriElement'
    @logger.debug "#{meth} - plural=#{plural} name=#{name}"

    if name is ''
      return q.reject new Error "#{meth} - Missing mandatory 'name' parameter."

    group = 'kumori.systems'
    version = 'v1'
    namespace = 'kumori'

    @k8sCustomApi.getNamespacedCustomObject group, version, namespace, plural
    , name
    .then (res) =>
      @logger.info "#{meth} - Got object #{name} (#{plural})."
      # @logger.info "#{meth} - Got object #{name} (#{plural}):
      #               #{JSON.stringify res.body, null, 2}"
      # console.log "-----------------------------------------------------"
      # console.log "KUMORI ELEMENT:"
      # console.log "#{JSON.stringify res.body}"
      # console.log "-----------------------------------------------------"
      return res.body
    .catch (err) =>
      if err.response?.body?.reason is 'NotFound'
        @logger.info "#{meth} - Object #{name} (#{plural}) not found."
        return q null
      else
        errMsg = "Getting object #{name} (#{plural}): #{JSON.stringify err}"
        @logger.error "#{meth} - #{errMsg}"
        q.reject new Error errMsg


  listKumoriElements: (plural, selector) ->
    meth = 'k8sApiStub.listKumoriElements'
    @logger.debug "#{meth} - plural=#{plural} selector:
                   #{JSON.stringify selector}"

    group = 'kumori.systems'
    version = 'v1'
    namespace = 'kumori'

    watchForChanges = false

    try
      @k8sCustomApi.listNamespacedCustomObject group
      , version
      , namespace
      , plural
      , undefined           # pretty
      , undefined           # allowWatchBookmarks
      , undefined           # _continue
      , undefined           # fieldSelector
      , selector            # labelSelector
      , 0                   # limit
      , undefined           # resourceVersion
      , undefined           # resourceVersionMatch
      , 0                   # timeoutSeconds
      , watchForChanges     # watch

      .then (res) =>
        @logger.info "#{meth} - Got #{res.body.items.length} objects."
        # @logger.debug "#{meth} - Elements: #{JSON.stringify res.body.items}"
        if res.body?.items?
          return res.body.items
        else
          throw new Error "List API call returned no item list."
      .catch (err) =>
        errMsg = "Getting '#{plural}' objects with selector
                  #{JSON.stringify selector}: #{JSON.stringify err}"
        @logger.error "#{meth} - #{errMsg}"
        q.reject new Error errMsg
    catch e
      throw e


  createKumoriElement: (kukuElement) ->
    meth = 'k8sApiStub.createKumoriElement'

    kind      = kukuElement.getKind()
    group     = kukuElement.getGroup()
    version   = kukuElement.getVersion()
    namespace = kukuElement.getNamespace()
    plural    = kukuElement.getPlural()
    name      = kukuElement.metadata.name
    body      = kukuElement
    # New object is expected to be in JSON
    # body      = kukuElement.serialize('json')

    @logger.info "#{meth} - Creating: group=#{group} version=#{version}
                  namespace=#{namespace} plural=#{plural} name=#{name}"

    @k8sCustomApi.createNamespacedCustomObject group, version, namespace, plural
    , body, undefined, undefined, ADMISSION_FIELD_MANAGER_NAME
    .then (res) =>
      @logger.info "#{meth} - Successfully created object: #{name} (#{kind})"
      # console.log "-----------------------------------------------------"
      # console.log "RESULT:"
      # console.log "#{JSON.stringify res, null, 2}"
      # console.log "-----------------------------------------------------"
      return true
    .catch (err) =>
      # console.log "********************"
      # console.log JSON.stringify err
      # console.log "********************"
      errMsg = err.body?.message || err.message || JSON.stringify(err)
      @logger.error "#{meth} - Creating #{name} (#{kind}): #{errMsg}"
      q.reject new Error "#{meth} - Creating #{name} (#{kind}): #{errMsg}"


  replaceKumoriElement: (kukuElement) ->
    meth = 'k8sApiStub.replaceKumoriElement'

    kind            = kukuElement.getKind()
    group           = kukuElement.getGroup()
    version         = kukuElement.getVersion()
    namespace       = kukuElement.getNamespace()
    plural          = kukuElement.getPlural()
    name            = kukuElement.metadata.name
    resourceVersion = kukuElement.metadata.resourceVersion
    body            = kukuElement
    # New object is expected to be in JSON
    # body      = kukuElement.serialize('json')

    @logger.info "#{meth} - Replacing: group=#{group} version=#{version}
                  namespace=#{namespace} plural=#{plural} name=#{name}
                  resourceVersion=#{resourceVersion}"

    @k8sCustomApi.replaceNamespacedCustomObject group, version, namespace
    , plural, name, body, undefined, ADMISSION_FIELD_MANAGER_NAME
    .then (res) =>
      @logger.info "#{meth} - Successfully replaced object: #{name} (#{kind})"
      # console.log "-----------------------------------------------------"
      # console.log "RESULT:"
      # console.log "#{JSON.stringify res, null, 2}"
      # console.log "-----------------------------------------------------"
      return true
    .catch (err) =>
      errMsg = err.body?.message || err.message || JSON.stringify(err)
      @logger.error "#{meth} - Replacing #{name} (#{kind}): #{errMsg}"
      q.reject new Error "#{meth} - Replacing #{name} (#{kind}): #{errMsg}"


  saveKumoriElement: (kukuElement) ->
    meth = 'k8sApiStub.saveKumoriElement'
    @logger.info "#{meth} - Saving name=#{kukuElement.metadata.name}"

    # If element already exists, then it is replaced by the new one.
    # This requires to get the existing element current 'resourceVersion' and
    # send it along in the 'replace' request.

    # Try to read the element
    @getKumoriElement kukuElement.getPlural(), kukuElement.metadata.name
    .then (existingElement) =>
      if existingElement?
        @logger.info "#{meth} - Element #{kukuElement.metadata.name} exists."
        # @logger.info "#{JSON.stringify existingElement}"

        if existingElement.metadata.deletionTimestamp?
          errMsg = 'Element is currently being deleted.'
          @logger.error "#{meth} - Updating #{kukuElement.metadata.name}: #{errMsg}"
          q.reject new Error "#{meth} - #{errMsg}"
        else
          kukuElement.setResourceVersion existingElement.metadata.resourceVersion
          return @replaceKumoriElement kukuElement
      else
        @logger.info "#{meth} - Element #{kukuElement.metadata.name} is new."
        return @createKumoriElement kukuElement


  deleteKumoriElement: (plural, name = '') ->
    meth = 'k8sApiStub.deleteKumoriElement'
    @logger.debug "#{meth} - plural=#{plural} name=#{name}"

    if name is ''
      return q.reject new Error "#{meth} - Missing mandatory 'name' parameter."

    group = 'kumori.systems'
    version = 'v1'
    namespace = 'kumori'


    @k8sCustomApi.deleteNamespacedCustomObject group
    , version
    , namespace
    , plural
    , name
    , undefined  # gracePeriodSeconds
    , undefined  # orphanDependents
    , DEFAULT_PROPAGATION_POLICY
    , undefined  # dryRun
    , undefined  # body
    , undefined  # options
    .then (res) =>
      @logger.info "#{meth} - Deleted object #{name} (#{plural})"
      # @logger.info "#{meth} - Deleted object #{name} (#{plural}):
      #               #{JSON.stringify res.body, null, 2}"
      # console.log "-----------------------------------------------------"
      # console.log "KUMORI ELEMENT:"
      # console.log "#{JSON.stringify res.body}"
      # console.log "-----------------------------------------------------"
      return res.body
    .catch (err) =>
      if err.response?.body?.reason is 'NotFound'
        @logger.info "#{meth} - Object #{name} (#{plural}) not found."
        return q null
      else
        errMsg = "Deleting object #{name} (#{plural}): #{JSON.stringify err}"
        @logger.error "#{meth} - #{errMsg}"
        q.reject new Error errMsg


  getService: (namespace, serviceName) ->
    meth = "k8sApiStub.getService #{namespace}/#{serviceName}"
    @logger.info meth

    @k8sApi.readNamespacedService serviceName, namespace
    .then (res) =>
      # @logger.info "#{meth} - Got Service: #{JSON.stringify res.body}"
      return res.body
    .catch (err) =>
      if err.response?.body?.reason is 'NotFound'
        errMsg = "Couldn't get Service #{namespace}/#{serviceName}: NotFound"
        @logger.warn "#{meth} - #{errMsg}"
        q.reject new Error errMsg
      else
        errMsg = "Couldn't get Service #{namespace}/#{serviceName}:
                  #{extractErrorMsg err}"
        @logger.warn "#{meth} - #{errMsg}"
        q.reject new Error errMsg


  deleteService: (namespace, serviceName) ->
    meth = "k8sApiStub.deleteService #{namespace}/#{serviceName}"
    @logger.info meth

    @k8sApi.deleteNamespacedService serviceName
    , namespace
    , undefined  # pretty
    , undefined  # dryRun
    , undefined  # gracePeriodSeconds
    , undefined  # orphanDependents
    , DEFAULT_PROPAGATION_POLICY
    , undefined  # body
    , undefined  # options
    .then (res) =>
      # @logger.info "#{meth} - Deleted Service: #{JSON.stringify res.body}"
      @logger.info "#{meth} - Deleted Service: #{namespace}/#{serviceName}"
      return res.body
    .catch (err) =>
      if err.response?.body?.reason is 'NotFound'
        @logger.info "#{meth} - Service #{namespace}/#{serviceName} not found."
        return q null
      else
        errMsg = "Deleting service #{namespace}/#{serviceName}:
                  #{JSON.stringify err}"
        @logger.error "#{meth} - #{errMsg}"
        q.reject new Error errMsg


  getConfigMap: (namespace, configMapName) ->
    meth = "k8sApiStub.getConfigMap #{namespace}/#{configMapName}"
    @logger.info meth

    @k8sApi.readNamespacedConfigMap configMapName, namespace
    .then (res) =>
      @logger.info "#{meth} - Got ConfigMap: #{JSON.stringify res.body}"
      return res.body
    .catch (err) =>
      errMsg = "Couldn't get ConfigMap #{namespace}/#{configMapName}:
                #{extractErrorMsg err}"
      @logger.warn "#{meth} - #{errMsg}"
      q.reject new Error errMsg


  createConfigMap: (namespace, configMapManifest) ->
    meth = "k8sApiStub.createConfigMap
            #{namespace}/#{configMapManifest.metadata.name}"
    @logger.info meth

    @k8sApi.createNamespacedConfigMap namespace
    , configMapManifest
    , undefined                      # pretty
    , undefined                      # dryRun
    , ADMISSION_FIELD_MANAGER_NAME   # fieldManager
    , undefined                      # fieldValidation
    , undefined                      # options
    .then (res) =>
      @logger.info "#{meth} - Successfully created ConfigMap:
                    #{configMapManifest.metadata.name}"
      return res.body
    .catch (err) =>
      errMsg = err.body?.message || err.message || JSON.stringify(err)
      @logger.error "#{meth} - Creating #{configMapManifest.metadata.name}:
                     #{errMsg}"
      q.reject new Error "#{meth} - Creating #{configMapManifest.metadata.name}:
                          #{errMsg}"


  createJob: (namespace, jobManifest) ->
    meth = "k8sApiStub.createJob
            #{namespace}/#{jobManifest.metadata.name}"
    @logger.info meth

    @k8sBatchApi.createNamespacedJob namespace
    , jobManifest
    , undefined                      # pretty
    , undefined                      # dryRun
    , ADMISSION_FIELD_MANAGER_NAME   # fieldManager
    , undefined                      # fieldValidation
    , undefined                      # options
    .then (res) =>
      @logger.info "#{meth} - Successfully created Job:
                    #{jobManifest.metadata.name}"
      return res.body
    .catch (err) =>
      errMsg = err.body?.message || err.message || JSON.stringify(err)
      @logger.error "#{meth} - Creating #{jobManifest.metadata.name}:
                     #{errMsg}"
      q.reject new Error "#{meth} - Creating #{jobManifest.metadata.name}:
                          #{errMsg}"


  getJobs: (namespace, selector) ->
    meth = "k8sApiStub.getJobs"
    @logger.info "#{meth} - Selector: #{JSON.stringify selector}"

    @k8sBatchApi.listNamespacedJob namespace
    , undefined    # pretty
    , undefined    # allowWatchBookmarks
    , undefined    # _continue
    , undefined    # fieldSelector
    , selector     # labelSelector
    , undefined    # limit
    , undefined    # resourceVersion
    , undefined    # resourceVersionMatch
    , undefined    # timeoutSeconds
    , undefined    # watch
    , undefined    # options
    .then (res) =>
      @logger.info "#{meth} - Got #{res.body.items.length} objects."
      return res.body.items
    .catch (err) =>
      errMsg = "Listing Jobs for namespace #{namespace} : " +
                ( err.body?.message || err.message || JSON.stringify err )
      @logger.error "#{meth} - #{errMsg}"
      q.reject new Error "#{meth} - #{errMsg}"


  getSecret: (namespace, secretName) ->
    meth = "k8sApiStub.getSecret #{namespace}/#{secretName}"
    @logger.info meth

    @k8sApi.readNamespacedSecret secretName, namespace
    .then (res) =>
      @logger.info "#{meth} - Got Secret: #{JSON.stringify res.body}"
      return res.body
    .catch (err) =>
      errMsg = "Couldn't get secret #{namespace}/#{secretName}:
                #{extractErrorMsg err}"
      @logger.warn "#{meth} - #{errMsg}"
      q.reject new Error errMsg


  getEvents: (namespace) ->
    meth = "k8sApiStub.getEvents #{namespace}"
    @logger.info meth

    @k8sApi.listNamespacedEvent namespace
    .then (res) =>
      @logger.info "#{meth} - Got #{res.body.items.length} objects."
      # console.log "-----------------------------------------------------"
      # console.log "LISTED EVENTS FOR NAMESPACE #{namespace}:"
      # console.log "#{JSON.stringify res.body}"
      # console.log "-----------------------------------------------------"
      return res.body.items
    .catch (err) =>
      errMsg = "Listing events for namespace #{namespace} : " +
                ( err.body?.message || err.message || JSON.stringify err )
      @logger.error "#{meth} - #{errMsg}"
      q.reject new Error "#{meth} - #{errMsg}"


  getPods: (namespace = null, labelSelector = null) ->
    ns = namespace || 'ALL NAMESPACES'
    meth = "k8sApiStub.getPods #{ns} Selector: #{JSON.stringify labelSelector}"
    @logger.info meth
    (if namespace?
      @k8sApi.listNamespacedPod namespace, null, null, null, null, labelSelector
    else
      @k8sApi.listPodForAllNamespaces null, null, null, labelSelector
    )
    .then (res) =>
      @logger.info "#{meth} - Found #{res.body.items.length} Pods."
      # console.log "PODS FOR NAMESPACE: #{ns}"
      # @printPodList res.body
      return res.body.items
    .catch (err) =>
      errMsg = "Couldn't get Pod list: #{JSON.stringify err}"
      @logger.error "#{meth} - #{errMsg}"
      q.reject new Error errMsg


  getPod: (namespace, podName) ->
    meth = "k8sApiStub.getPod #{namespace}/#{podName}"
    @logger.info meth

    @k8sApi.readNamespacedPod podName, namespace
    .then (res) =>
      @logger.info "#{meth} - Got Pod: #{JSON.stringify res.body}"
      return res.body
    .catch (err) =>
      errMsg = "Couldn't get pod #{namespace}/#{podName}:
                #{extractErrorMsg err}"
      @logger.warn "#{meth} - #{errMsg}"
      q.reject new Error errMsg


  listPods: (namespace = null) ->
    ns = namespace || 'ALL NAMESPACES'
    (if namespace?
      @k8sApi.listNamespacedPod namespace
    else
      @k8sApi.listPodForAllNamespaces()
    )
    .then (res) =>
      # console.log "PODS FOR NAMESPACE: #{ns}"
      # @printPodList res.body
      return res.body.items
    .catch (err) =>
      # console.log "ERROR GETTING PODS FOR NAMESPACE '#{ns}': #{err.message || err}"
      # console.log "ERROR: #{JSON.stringify err}"
      @logger.error "#{meth} ERROR: #{err.message || err}"
      q.reject err


  listIngresses: (namespace = null) ->
    ns = namespace || 'ALL NAMESPACES'
    meth = "k8sApiStub.listIngresses #{ns}"
    @logger.info meth
    (if namespace?
      @k8sNetApi.listNamespacedIngress namespace, null, null, null, null, labelSelector
    else
      @k8sNetApi.listIngressForAllNamespaces null, null, null, null
    )
    .then (res) =>
      @logger.info "#{meth} - Found #{res.body.items.length} Ingresses."
      # console.log "INGRESSES FOR NAMESPACE: #{ns}"
      # @printPodList res.body
      return res.body.items
    .catch (err) =>
      errMsg = "Couldn't get Ingress list: #{JSON.stringify err}"
      @logger.error "#{meth} - #{errMsg}"
      q.reject new Error errMsg



  deleteIngress: (namespace, ingressName) ->
    meth = "k8sApiStub.deleteIngress #{namespace}/#{ingressName}"
    @logger.info meth

    @k8sNetApi.deleteNamespacedIngress ingressName
    , namespace
    , undefined  # pretty
    , undefined  # dryRun
    , undefined  # gracePeriodSeconds
    , undefined  # orphanDependents
    , DEFAULT_PROPAGATION_POLICY
    , undefined  # body
    , undefined  # options
    .then (res) =>
      @logger.info "#{meth} - Deleted ingress: #{namespace}/#{ingressName}"
      return res.body
    .catch (err) =>
      if err.response?.body?.reason is 'NotFound'
        @logger.info "#{meth} - Ingress #{namespace}/#{ingressName} not found."
        return q null
      else
        errMsg = "Deleting Ingress #{namespace}/#{ingressName}:
                  #{JSON.stringify err}"
        @logger.error "#{meth} - #{errMsg}"
        q.reject new Error errMsg


  listNodes: () ->
    @k8sApi.listNode()
    .then (res) =>
      # console.log "GOT NODES:"
      # console.log JSON.stringify res.body
      return res.body.items
    .catch (err) =>
      # console.log "ERROR GETTING NODES: #{err.message || err}"
      # console.log "ERROR: #{JSON.stringify err}"
      @logger.error "#{meth} ERROR: #{err.message || err}"
      q.reject err


  getNodesMetrics: () ->
    meth = 'k8sApiStub.getNodesMetrics'
    q.promise (resolve, reject) =>
      opts = {}
      @kConfig.applyToRequest opts

      clusterURL = @kConfig.getCurrentCluster().server
      nodeMetricsURL = "#{clusterURL}/apis/metrics.k8s.io/v1beta1/nodes"

      request.get nodeMetricsURL, opts, (error, response, body) =>
        if error
          errMsg = "Unable to node metrics data: #{error.message}"
          @logger.error "#{meth} ERROR: #{errMsg}"
          resolve null
        else if response.statusCode isnt 200
          errMsg = "Unable to node metrics data: unexpected HTTP status
                    #{response.statusCode}"
          @logger.error "#{meth} ERROR: #{errMsg}"
          resolve null
        else
          # Body is a string representation of a JSON object:
          #   {
          #     "kind": "NodeMetricsList",
          #     "apiVersion": "metrics.k8s.io/v1beta1"
          #     },
          #     "items": [
          #       <THE ACTUAL DATA WE'RE INTERESTED IN>
          #     ],
          #     [...]
          #   }
          parsedBody = JSON.parse body
          # Create a dictionnary where the key is the node name and the value an
          # object with the relevant metric data
          result = {}
          for it in (parsedBody.items || [])
            nodeName = it.metadata.name
            nodeData =
              usage:
                cpu: quantityToScalar(it.usage?.cpu || 0)
                memory: quantityToScalar(it.usage?.memory || 0)
            result[nodeName] = nodeData
          resolve result


  getPodMetrics: (namespace, podName) ->
    meth = "k8sApiStub.getPodMetrics(#{namespace}, #{podName})"
    @logger.debug meth

    q.promise (resolve, reject) =>
      opts = {}
      @kConfig.applyToRequest opts

      clusterURL = @kConfig.getCurrentCluster().server
      podMetricsURL = "#{clusterURL}/apis/metrics.k8s.io/v1beta1/namespaces/" +
                      "#{namespace}/pods/#{podName}"

      request.get podMetricsURL, opts, (error, response, body) =>
        if error
          errMsg = "Unable to pod metrics data: #{error.message}"
          @logger.error "#{meth} ERROR: #{errMsg}"
          resolve null
        else if response.statusCode is 404
          errMsg = "Unable to get pod metrics data: pod not found
                    (Status Code: #{response.statusCode})"
          @logger.warn "#{meth} ERROR: #{errMsg}"
          resolve null
        else if response.statusCode isnt 200
          errMsg = "Unable to get pod metrics data: unexpected HTTP status
                    #{response.statusCode}"
          @logger.error "#{meth} ERROR: #{errMsg}"
          resolve null
        else
          # Body is a string representation of a JSON object:
          #   {
          #     "kind": "PodMetrics",
          #     "apiVersion": "metrics.k8s.io/v1beta1",
          #     "metadata": {
          #       "name": "kd-114635-98173648-frontend-deployment-d5c8b44c4-x65hr",
          #       "namespace": "kumori"
          #     },
          #     "timestamp": "2020-04-27T16:37:37Z",
          #     "window": "5m0s",
          #     "containers": [
          #       {
          #         "name": "frontend",
          #         "usage": {
          #           "cpu": "0",
          #           "memory": "2588Ki"
          #         }
          #       }
          #     ]
          #   }
          parsedBody = JSON.parse body
          # Create a dictionnary where the key is the pod name and the value an
          # object with the relevant metric data
          result = {}
          podData = @aggregatePodMetric parsedBody
          resolve podData


  getPodsMetrics: () ->
    meth = 'k8sApiStub.getPodsMetrics'

    q.promise (resolve, reject) =>
      opts = {}
      @kConfig.applyToRequest opts

      clusterURL = @kConfig.getCurrentCluster().server
      podMetricsURL = "#{clusterURL}/apis/metrics.k8s.io/v1beta1/pods"

      request.get podMetricsURL, opts, (error, response, body) =>
        if error
          errMsg = "Unable to pod metrics data: #{error.message}"
          @logger.error "#{meth} ERROR: #{errMsg}"
          resolve null
        else if response.statusCode isnt 200
          errMsg = "Unable to pod metrics data: unexpected HTTP status
                    #{response.statusCode}"
          @logger.error "#{meth} ERROR: #{errMsg}"
          resolve null
        else
          # Body is a string representation of a JSON object:
          #   {
          #     "kind": "PodMetricsList",
          #     "apiVersion": "metrics.k8s.io/v1beta1"
          #     },
          #     "items": [
          #       <THE ACTUAL DATA WE'RE INTERESTED IN>
          #     ],
          #     [...]
          #   }
          parsedBody = JSON.parse body
          # Create a dictionnary where the key is the pod name and the value an
          # object with the relevant metric data
          result = {}
          for it in (parsedBody.items || [])
            podName = it.metadata.name
            podData = @aggregatePodMetric it
            result[podName] = podData

          resolve result


  getPvcMetric: (namespace, pvcName, metricName) ->
    meth = "k8sApiStub.getPvcMetric(#{namespace}, #{pvcName}, #{metricName})"
    @logger.debug meth

    q.promise (resolve, reject) =>
      opts = {}
      @kConfig.applyToRequest opts

      clusterURL = @kConfig.getCurrentCluster().server
      pvcMetricURL = "#{clusterURL}/apis/custom.metrics.k8s.io/v1beta1/" +
                      "namespaces/#{namespace}/" +
                      "persistentvolumeclaims/#{pvcName}/#{metricName}"

      request.get pvcMetricURL, opts, (error, response, body) =>
        if error
          errMsg = "Unable to PVC metrics data: #{error.message || error}"
          @logger.error "#{meth} ERROR: #{errMsg}"
          resolve null
        else if response.statusCode is 404
          errMsg = "Unable to get PVC metrics data: PVC not found
                    (Status Code: #{response.statusCode})"
          @logger.warn "#{meth} ERROR: #{errMsg}"
          resolve null
        else if response.statusCode isnt 200
          errMsg = "Unable to get PVC metrics data: unexpected HTTP status
                    #{response.statusCode}"
          @logger.error "#{meth} ERROR: #{errMsg}"
          resolve null
        else
          # Body is a string representation of a JSON object:
          #   {
          #       "kind": "MetricValueList",
          #       "apiVersion": "custom.metrics.k8s.io/v1beta1",
          #       "metadata":
          #       {
          #           "selfLink": "..."
          #       },
          #       "items":
          #       [
          #           {
          #               "describedObject":
          #               {
          #                   "kind": "PersistentVolumeClaim",
          #                   "namespace": "kumori",
          #                   "name": "c0volume0",
          #                   "apiVersion": "/v1"
          #               },
          #               "metricName": "kubelet_volume_stats_capacity_bytes",
          #               "timestamp": "2021-10-27T18:12:59Z",
          #               "value": "1023303680"
          #           }
          #       ]
          #   }
          parsedBody = JSON.parse body

          if (not parsedBody.items?) or (parsedBody.items.length < 1)
            # No metrics returned
            errMsg = "Unable to get PVC metrics data: no metrics returned."
            @logger.error "#{meth} ERROR: #{errMsg}"
            resolve null
          else if parsedBody.items.length > 1
            # More than one metric returned
            @logger.warn "#{meth} More than one metric returned. Using the first
                          item in the list."
            resolve parsedBody.items[0]
          else
            # Exactly one metric returned
            resolve parsedBody.items[0]


  getPvcMetrics: (namespace, pvcName) ->
    meth = "k8sApiStub.getPvcMetrics(#{namespace}, #{pvcName})"
    @logger.debug meth

    VOLUME_METRICS_NAMES =
      availableBytes: 'kubelet_volume_stats_available_bytes'
      capacityBytes: 'kubelet_volume_stats_capacity_bytes'

    pvcMetrics =
      availableBytes: null
      capacityBytes: null
      usagePercent: null

    promiseChain = q()
    for metricName, metricLabel of VOLUME_METRICS_NAMES
      do (metricName, metricLabel) =>
        promiseChain = promiseChain.then () =>
          @getPvcMetric namespace, pvcName, metricLabel
          .then (metricsResult) =>
            pvcMetrics[metricName] = metricsResult.value
            return true

    promiseChain.then () =>
      @logger.info "#{meth} - Retrieved metrics: #{JSON.stringify pvcMetrics}"
      # If values exist, calculate usage percent
      if pvcMetrics.availableBytes? and pvcMetrics.capacityBytes?
        cap = pvcMetrics.capacityBytes
        avail = pvcMetrics.availableBytes
        rawUsagePercent = ( (cap - avail) / cap ) * 100
        # Round percent to 2 decimals and convert to string to be consistent
        # with the metrics format (also strings)
        pvcMetrics.usagePercent = "#{Math.round(rawUsagePercent * 100) / 100}"


      return pvcMetrics


  aggregatePodMetric: (rawPodMetric) ->
    podData =
      namespace: rawPodMetric.metadata.namespace
      containers: {}
      usage:
        cpu: 0
        memory: 0

    for cont in rawPodMetric.containers
      contData =
        usage:
          cpu: 0
          memory: 0
      # Add container resources to pod totals
      # PROBLEM: they are K8s Quantities --> function to convert
      contData.usage.cpu    = quantityToScalar(cont.usage?.cpu || 0)
      contData.usage.memory = quantityToScalar(cont.usage?.memory || 0)
      podData.usage.cpu    += contData.usage.cpu
      podData.usage.memory += contData.usage.memory

      podData.containers[cont.name] = contData

    # Round to fix JS strange behaviour when adding decimal numbers
    podData.usage.cpu    = Math.round(podData.usage.cpu * 100) / 100
    podData.usage.memory = Math.round(podData.usage.memory * 100) / 100
    podData


  topNodes: () ->
    meth = 'k8sApiStub.topNodes'
    @logger.debug "#{meth}"

    MASTER_NODE_MARK_LABEL = 'node-role.kubernetes.io/control-plane'
    MAINTENANCE_MARK_LABEL = 'kumori/maintenance'

    result = {}
    allPods = []
    podsMetrics = {}
    nodesMetrics = {}

    # List all pods
    @listPods()
    .then (podList) =>
      allPods = podList
      @logger.debug "#{meth} All pods: #{JSON.stringify allPods}"

      # List real-time metrics from all pods
      @getPodsMetrics()
    .then (metrics) =>
      podsMetrics = metrics || {}
      # List real-time metrics from all nodes
      @getNodesMetrics()
    .then (metrics) =>
      nodesMetrics = metrics || {}
      # List all nodes
      @listNodes()
    .then (nodeList) =>
      @logger.debug "#{meth} All nodes: #{JSON.stringify nodeList}"
      for node in nodeList
        nodeName = node.metadata?.name
        # console.log "#{meth} - NODE: #{nodeName}"

        nodeData =
          role: null
          maintenance: false
          usage:
            cpu: null
            memory: null
          pods: {}
          conditions: {}
          taints: []

        # Mark it as Master or Worker
        if MASTER_NODE_MARK_LABEL of node.metadata.labels
          nodeData.role = 'master'
        else
          nodeData.role = 'worker'

        # Determine if node is in maintenance mode
        if (MAINTENANCE_MARK_LABEL of node.metadata.labels) \
        and (node.metadata.labels[MAINTENANCE_MARK_LABEL] is 'true')
          nodeData.maintenance = true

        # Take the actual metrics
        measuredCPU = nodesMetrics[nodeName]?.usage?.cpu || 0
        measuredMem = nodesMetrics[nodeName]?.usage?.memory || 0
        # Take the node total
        totalCPU = quantityToScalar node.status?.capacity?.cpu
        totalMem = quantityToScalar node.status?.capacity?.memory
        # Take the node available/allocatable
        availCPU = quantityToScalar node.status?.allocatable?.cpu
        availMem = quantityToScalar node.status?.allocatable?.memory
        # Calculate allocated from current scheduled pods
        totalPodCPURequest = 0.0
        totalPodCPULimit = 0.0
        totalPodMemRequest = 0
        totalPodMemLimit = 0
        nodePods = allPods.filter (pod) ->
          (pod.spec?.nodeName is nodeName) and
          (pod.status?.phase is 'Running')
        # console.log "#{meth} - NODE PODS: #{JSON.stringify nodePods}"

        for pod in nodePods
          podName = pod.metadata.name
          podData =
            namespace: pod.metadata.namespace
            labels: pod.metadata.labels
            phase: pod.status.phase
            conditions: cleanConditions pod.status.conditions
            containers: {}
            # usage: podsMetrics[podName].usage
            usage: {}

          # console.log "#{meth} - POD: #{pod.metadata.name}"

          for cont in (pod.spec?.containers || [])
            contName = cont.name
            contUsage =
              cpu: {}
              memory: {}

            if podsMetrics[podName]?.containers[contName]?.usage?
              contUsage.cpu.measured =
                podsMetrics[podName].containers[contName].usage.cpu
              contUsage.memory.measured =
                podsMetrics[podName].containers[contName].usage.memory

            if cont.resources?
              if cont.resources.requests?
                if cont.resources.requests?.cpu
                  contUsage.cpu.request =
                    quantityToScalar cont.resources.requests?.cpu
                if cont.resources.requests?.memory
                  contUsage.memory.request =
                    quantityToScalar cont.resources.requests?.memory
              if cont.resources.limits?
                if cont.resources.limits?.cpu
                  contUsage.cpu.limit =
                    quantityToScalar cont.resources.limits?.cpu
                if cont.resources.limits?.memory
                  contUsage.memory.limit =
                    quantityToScalar cont.resources.limits?.memory

            podData.containers[contName] =
              usage: contUsage

          podCPUTotals = totalForResource pod, 'cpu'
          totalPodCPURequest += podCPUTotals.request
          totalPodCPULimit += podCPUTotals.limit

          podMemTotals = totalForResource pod, 'memory'
          totalPodMemRequest += podMemTotals.request
          totalPodMemLimit += podMemTotals.limit

          podData.usage =
            cpu:
              measured: podsMetrics[podName]?.usage?.cpu || 0
              request: podCPUTotals.request
              limit: podCPUTotals.limit
            memory:
              measured: podsMetrics[podName]?.usage?.memory || 0
              request: podMemTotals.request
              limit: podMemTotals.limit

          # Add pod info to Node pod list
          nodeData.pods[podName] = podData

        nodeData.usage.cpu =
          measured: nodesMetrics[nodeName]?.usage?.cpu || 0
          capacity: totalCPU
          alloctable: availCPU
          requestTotal: Math.round(totalPodCPURequest * 100) / 100
          limitTotal: Math.round(totalPodCPULimit * 100) / 100

        nodeData.usage.memory =
          measured: nodesMetrics[nodeName]?.usage?.memory || 0
          capacity: totalMem
          allocatable: availMem
          requestTotal: totalPodMemRequest
          limitTotal: totalPodMemLimit

        nodeData.conditions = cleanConditions node.status.conditions
        nodeData.taints = node.spec.taints || []
        result[nodeName] = nodeData
      result
    .catch (err) =>
      @logger.error "#{meth} ERROR: #{err.message || err}"
      q.reject err


  # LogOptions possible properties:
  #  - follow: <boolean>  (requires wStream to be set)
  #  - tailLines: <number>
  #  - sinceSeconds: <number>  (seconds)
  #
  # It is mandatory to provide a WritableStream to avoid storing logs in memory.
  #
  getPodLogs: (namespace, podName, container = null, opts = {}, wStream) ->
    meth = "k8sApiStub.getPodLogs pod=#{podName} - container=#{container}"
    @logger.info "#{meth} - Options: #{JSON.stringify opts}"

    # Check a stream is provided if follow is true
    if not wStream?
      errMsg = 'Missing mandatory stream.'
      @logger.error "#{meth} - #{errMsg}"
      return q.reject new Error errMsg


    # Temporarily hard coded since AdmissionRestApi is not aware if namespaces
    namespace ?= 'kumori'

    q.promise (resolve, reject) =>

      # Callback handler for resolving the promise if the K8sLog request ends.
      done = (err) =>
        if err
          @logger.info "K8sLog request Done with error: #{err}"
          reject new Error "#{err.message || err}"
        else
          @logger.info "K8sLog request DONE."
          resolve("K8sLog request Done.")


      if not opts.follow
        # This scenario is simpler since K8s library handles the disconnection
        @logger.debug "#{meth} - Starting K8sLog request (no follow)"
        k8sLogRequest =
          @k8sLog.log namespace, podName, container, wStream, done, opts
      else
        # Reference to the ApiServer request object (for aborting it if needed)
        k8sLogRequest = null

        # Handlers and utility method to detect if the stream is closed and
        # aborting the current ApiServer request, that otherwise would still be
        # connected.
        abortLogRequest = (reason) =>
          @logger.info "#{meth} - Writable stream event '#{reason}' received.
                        Closing ApiServer Log request"
          try
            if k8sLogRequest?.abort?
              k8sLogRequest.abort()
          catch err
            @logger.info "#{meth} - Error aborting: #{err.message} #{err.stack}"
          @logger.info "#{meth} - ApiServer Log request closed."
          resolve("Writable stream #{reason}")

        # Logs must be followed (streamed) until the stream is closed
        wStream.on 'end', () => abortLogRequest 'end'
        wStream.on 'finish', () => abortLogRequest 'finish'
        wStream.on 'close', () => abortLogRequest 'close'

        # Temporary intermediary Stream for testing
        ## st = new stream.Writable
        ##   write: (chunk, encoding, callback) ->
        ##     # console.log "--> k8sLog.log receiving data..."
        ##     if not wStream?
        ##       # Print without additional newline (logs already have newlines)
        ##       console.log "CHUNK: #{chunk.toString()}"
        ##       process.stdout.write chunk.toString()
        ##     else
        ##       console.log "CHUNK: #{chunk.toString()}"
        ##       wStream.write chunk.toString()
        ##     callback()

        # Only for non streamed responses (errors, etc)
        done = (err) =>
          if err
            @logger.info "K8sLog request Done with error: #{err}"
            reject new Error "#{err.message || err}"
          else
            @logger.info "K8sLog request DONE."
            resolve("K8sLog request Done.")

        @logger.debug "#{meth} - Starting K8sLog request (with follow)"
        k8sLogRequest =
          @k8sLog.log namespace, podName, container, wStream, done, opts
        # console.log "#{meth} - AFTER K8sLog request"


  execPod: (namespace, podName, container, command, tty, streams = {}) ->
    meth = "k8sApiStub.execPod pod=#{podName} - container=#{container} -
            command='#{JSON.stringify command}'"
    @logger.info meth

    # Temporarily hard coded since AdmissionRestApi is not aware if namespaces
    namespace ?= 'kumori'

    resultPromise = q.promise (resolve, reject) =>

      # Create a new K8s command executor (K8s library)
      @k8sExec = new k8s.Exec @kConfig

      # Callback called by K8s library to indicate the exec process is finished.
      # Sample status values:
      # - On success, status is: { "metadata": {}, "status": "Success" }
      # - On failure:
      #   {
      #     "metadata": {},
      #     "status": "Failure",
      #     "message": "command terminated with non-zero exit code: Error executing in Docker Container: 126",
      #     "reason": "NonZeroExitCode",
      #     "details": {
      #       "causes": [ { "reason": "ExitCode", "message": "126" } ]
      #     }
      #   }
      done = (status) =>
        @logger.info "#{meth} - Command finished, done called:
                      #{JSON.stringify status}"
        resolve status


      @logger.info "#{meth} - Calling K8s ApiServer exec..."
      @k8sExec.exec namespace, podName, container, command
        , streams.out ? null
        , streams.err ? null
        , streams.in ? null
        , tty
        , done
      .then (k8sWS) =>
        # Promise is resolved once the connection with K8s is established
        # The promise is resolved with a Websocket
        @logger.info "#{meth} - K8s ApiServer exec session started (got ws)"

        # Set a handler to detect client disconnection (end of 'in' stream)
        streams.in.on 'end', () =>
          @logger.info "#{meth} - Detected 'in' stream end (disconnection).
                        Resolving exec promise..."
          resolve { status: "Client disconnected" }

        ##############################
        ##      FOR DEBUGGING       ##
        ##############################
        # # K8s Websocket event hadlers
        # k8sWS.onopen = () ->
        #   console.log "K8S WS onOpen"
        # k8sWS.onclose = (closeEvt) ->
        #   # A 'CloseEvent' contains a code, a reason and a target object
        #   # Websocket status codes info:
        #   #   https://tools.ietf.org/html/rfc6455#section-7.4.1
        #   if closeEvt.code is 1000
        #     console.log "K8S WS onClose - Normal exit (code: 1000)"
        #   else
        #     console.log "K8S WS onClose (closeEvt.code} -
        #                  Reason: #{closeEvt.reason})"
        # k8sWS.onerror = (err) ->
        #   console.log "K8S WS onError: #{err.message || err}"
        #   # ws.close()
        # # WARNING: implementing 'onmessage' will overwrite the library default
        # # implementation! Alternatively, set a listener to 'message' events.
        # # k8sWS.onmessage = (data) ->
        # #   streamNumber = data.data.readInt8(0)
        # #   cleanData = data.data.slice 1
        # #   console.log "K8S WS onMessage (stream #{streamNumber})"
        # #   process.stdout.write cleanData.toString()
        # #   ws.send data.data
        ##############################
        ##   END OF FOR DEBUGGING   ##
        ##############################

      .catch (err) =>
        errMsg = "Couldn't execute command '#{command}' in pod #{podName} and
                  container '#{if container? then container else 'default'}'.
                  Error: #{err.message}"
        @logger.info "#{meth} - #{errMsg}"
        reject new Error errMsg


  execValidationCheck: (podName, container) ->
    meth = "k8sApiStub.execValidationCheck instance=#{podName} -
            container=#{container}"
    @logger.info meth

    namespace = 'kumori'

    @getPod namespace, podName
    .then (pod) =>
      if pod.status?.phase isnt 'Running'
        throw new Error "Instance #{podName} is not running."

      if not container?
        # Not specifying a container is only OK if Pod only has one container
        if pod.spec.containers.length > 1
          throw new Error "Instance #{podName} has several containers. A
                           container must be specified."
      else
        # If container was specified, it must exist in the Pod
        found = false
        for cont in pod.spec.containers
          if cont.name is container
            found = true
            break
        if not found
          throw new Error "Container #{container} not found in instance
                           #{podName}"
      return true
    .catch (err) =>
      @logger.warn "#{meth} - Failed validation: #{err.message}"
      q.reject new Error err.message


  getControllerRevisions: (namespace = null, labelSelector = null) ->
    ns = namespace || 'kumori'
    meth = "k8sApiStub.getControllerRevisions Namespace: #{ns} -
            Selector: #{JSON.stringify labelSelector}"
    @logger.info meth
    (if namespace?
      @k8sAppsApi.listNamespacedControllerRevision namespace, null, null, null
      , null, labelSelector
    else
      @k8sAppsApi.listControllerRevisionForAllNamespaces null, null, null
      , labelSelector
    )
    .then (res) =>
      @logger.info "#{meth} - Found #{res.body.items.length}
                    ControllerRevisions."
      return res.body.items
    .catch (err) =>
      errMsg = "Couldn't get ControllerRevision list: #{JSON.stringify err}"
      @logger.error "#{meth} - #{errMsg}"
      q.reject new Error errMsg


  getControllerRevision: (namespace, name) ->
    ns = namespace || 'kumori'
    meth = "k8sApiStub.getControllerRevision #{ns}/#{name}"
    @logger.info meth

    @k8sAppsApi.readNamespacedControllerRevision name, ns
    .then (res) =>
      @logger.info "#{meth} - Got ControllerRevision:
                    #{JSON.stringify res.body}"
      return res.body
    .catch (err) =>
      errMsg = "Couldn't get ControllerRevision #{ns}/#{name}:
                #{extractErrorMsg err}"
      @logger.warn "#{meth} - #{errMsg}"
      q.reject new Error errMsg


  getDeployments: (namespace = null, labelSelector = null) ->
    ns = namespace || 'kumori'
    meth = "k8sApiStub.getDeployments Namespace: #{ns} -
            Selector: #{JSON.stringify labelSelector}"
    @logger.info meth
    (if namespace?
      @k8sAppsApi.listNamespacedDeployment namespace, null, null, null
      , null, labelSelector
    else
      @k8sAppsApi.listDeploymentForAllNamespaces null, null, null
      , labelSelector
    )
    .then (res) =>
      @logger.info "#{meth} - Found #{res.body.items.length}
                    Deployments."
      return res.body.items
    .catch (err) =>
      errMsg = "Couldn't get Deployment list: #{JSON.stringify err}"
      @logger.error "#{meth} - #{errMsg}"
      q.reject new Error errMsg


  getStatefulSets: (namespace = null, labelSelector = null) ->
    ns = namespace || 'kumori'
    meth = "k8sApiStub.getStatefulSets Namespace: #{ns} -
            Selector: #{JSON.stringify labelSelector}"
    @logger.info meth
    (if namespace?
      @k8sAppsApi.listNamespacedStatefulSet namespace, null, null, null
      , null, labelSelector
    else
      @k8sAppsApi.listStatefulSetForAllNamespaces null, null, null
      , labelSelector
    )
    .then (res) =>
      @logger.info "#{meth} - Found #{res.body.items.length}
                    StatefulSets."
      return res.body.items
    .catch (err) =>
      errMsg = "Couldn't get StatefulSet list: #{JSON.stringify err}"
      @logger.error "#{meth} - #{errMsg}"
      q.reject new Error errMsg


  patchDeployment: (namespace, name, patch) ->
    meth = 'k8sApiStub.patchDeployment'
    elementId = "#{namespace}/deployments/#{name}"

    @logger.info "#{meth} - Applying patch to: #{elementId}"

    options =
      headers:
        'Content-type': 'application/json-patch+json'
        # 'Content-type': k8s.PatchUtils.PATCH_FORMAT_JSON_PATCH}};

    @k8sAppsApi.patchNamespacedDeployment name
    , namespace
    , patch
    , undefined                       # pretty
    , undefined                       # dryRun
    , ADMISSION_FIELD_MANAGER_NAME
    , undefined                       # fieldValidation
    , undefined                       # force
    , options
    .then (res) =>
      @logger.info "#{meth} - Successfully patched object: #{elementId}"
      # console.log "-----------------------------------------------------"
      # console.log "RESULT:"
      # console.log "#{JSON.stringify res, null, 2}"
      # console.log "-----------------------------------------------------"
      return true
    .catch (err) =>
      errMsg = err.body?.message || err.message || JSON.stringify(err)
      @logger.error "#{meth} - Patching #{elementId}: #{errMsg}"
      q.reject new Error "#{meth} - Replacing #{elementId}: #{errMsg}"


  patchStatefulSet: (namespace, name, patch) ->
    meth = 'k8sApiStub.patchStatefulSet'
    elementId = "#{namespace}/statefulsets/#{name}"

    @logger.info "#{meth} - Applying patch to: #{elementId}"

    options =
      headers:
        'Content-type': 'application/json-patch+json'
        # 'Content-type': k8s.PatchUtils.PATCH_FORMAT_JSON_PATCH}};

    @k8sAppsApi.patchNamespacedStatefulSet name
    , namespace
    , patch
    , undefined                       # pretty
    , undefined                       # dryRun
    , ADMISSION_FIELD_MANAGER_NAME
    , undefined                       # fieldValidation
    , undefined                       # force
    , options
    .then (res) =>
      @logger.info "#{meth} - Successfully patched object: #{elementId}"
      # console.log "-----------------------------------------------------"
      # console.log "RESULT:"
      # console.log "#{JSON.stringify res, null, 2}"
      # console.log "-----------------------------------------------------"
      return true
    .catch (err) =>
      errMsg = err.body?.message || err.message || JSON.stringify(err)
      @logger.error "#{meth} - Patching #{elementId}: #{errMsg}"
      q.reject new Error "#{meth} - Replacing #{elementId}: #{errMsg}"


  evictPod: (namespace, podName) ->
    meth = 'k8sApiStub.evictPod'
    elementId = "#{namespace}/pods/#{podName}"

    @logger.info "#{meth} - Creating eviction for: #{elementId}"

    body =
      apiVersion: 'policy/v1beta1'
      kind: 'Eviction'
      metadata:
        name: podName
        namespace: namespace

    @k8sApi.createNamespacedPodEviction podName
    , namespace
    , body
    , undefined
    , ADMISSION_FIELD_MANAGER_NAME
    , undefined
    , undefined
    , undefined
    .then (res) =>
      @logger.info "#{meth} - Successfully created eviction for: #{elementId}"
      # console.log "-----------------------------------------------------"
      # console.log "RESULT:"
      # console.log "#{JSON.stringify res, null, 2}"
      # console.log "-----------------------------------------------------"
      return true
    .catch (err) =>
      # console.log "ERROR: #{err}"
      # console.log "ERROR: #{err.body?.message}"
      # console.log "ERROR: #{err.message}"
      # console.log "ERROR: #{JSON.stringify err}"

      if err.response?.body?.message?
        msg = err.response?.body?.message
        if msg.toLowerCase().indexOf('disruption budget') > 0
          errMsg = 'Unable to evict pod due to resilience constraints.'
        else
          errMsg = msg
      else
        errMsg = err.body?.message || err.message || JSON.stringify(err)


      @logger.error "#{meth} - Creating eviction for #{elementId}: #{errMsg}"
      q.reject new Error "Evicting #{podName}: #{errMsg}"



  ##############################################################################
  ##              PUBLIC METHODS FOR MANAGING NON-KUMORI CRDs                 ##
  ##                                                                          ##
  ## - Initially used for managing Keycloak Operator CRDs                     ##
  ##############################################################################

  getCustomResource: (group, version, namespace, plural, name) ->
    meth = 'k8sApiStub.getCustomResource'
    elementId = "#{group}/#{version}/#{namespace}/#{plural}/#{name}"
    @logger.debug "#{meth} - #{elementId}"

    missing = []
    if group is ''
      missing.push 'group'
    if version is ''
      missing.push 'version'
    if namespace is ''
      missing.push 'namespace'
    if plural is ''
      missing.push 'plural'
    if name is ''
      missing.push 'name'

    if missing.length isnt 0
      return q.reject new Error "#{meth} - Missing mandatory parameters:
                                 #{missing}"

    @k8sCustomApi.getNamespacedCustomObject group, version, namespace, plural
    , name
    .then (res) =>
      @logger.info "#{meth} - Got object #{elementId}"
      # console.log "-----------------------------------------------------"
      # console.log "ELEMENT:"
      # console.log "#{JSON.stringify res.body}"
      # console.log "-----------------------------------------------------"
      return res.body
    .catch (err) =>
      if err.response?.body?.reason is 'NotFound'
        @logger.info "#{meth} - Object #{elementId} not found."
        return q null
      else
        errMsg = "Getting object #{elementId}: #{JSON.stringify err}"
        @logger.error "#{meth} - #{errMsg}"
        q.reject new Error errMsg


  listCustomResource: (group, version, namespace, plural, selector) ->
    meth = 'k8sApiStub.listCustomResource'
    type = "#{group}/#{version}/#{namespace}/#{plural}"
    @logger.debug "#{meth} - #{type} selector: #{JSON.stringify selector}"

    watchForChanges = false

    try
      @k8sCustomApi.listNamespacedCustomObject group
      , version
      , namespace
      , plural
      , undefined           # pretty
      , undefined           # allowWatchBookmarks
      , undefined           # _continue
      , undefined           # fieldSelector
      , selector            # labelSelector
      , 0                   # limit
      , undefined           # resourceVersion
      , undefined           # resourceVersionMatch
      , 0                   # timeoutSeconds
      , watchForChanges     # watch

      .then (res) =>
        @logger.info "#{meth} - Got #{res.body.items.length} objects."
        # @logger.debug "#{meth} - Elements: #{JSON.stringify res.body.items}"
        if res.body?.items?
          return res.body.items
        else
          throw new Error "List API call returned no item list."
      .catch (err) =>
        errMsg = "Getting '#{type}' objects with selector
                  #{JSON.stringify selector}: #{JSON.stringify err}"
        @logger.error "#{meth} - #{errMsg}"
        q.reject new Error errMsg
    catch e
      throw e


  createCustomResource: (group, version, namespace, plural, element) ->
    meth = 'k8sApiStub.createCustomResource'
    elementName = element.metadata.name
    elementId = "#{group}/#{version}/#{namespace}/#{plural}/#{elementName}"

    @logger.info "#{meth} - Creating: #{elementId}"

    @k8sCustomApi.createNamespacedCustomObject group, version, namespace, plural
    , element, undefined, undefined, ADMISSION_FIELD_MANAGER_NAME
    .then (res) =>
      @logger.info "#{meth} - Successfully created object: #{elementId}"
      # console.log "-----------------------------------------------------"
      # console.log "RESULT:"
      # console.log "#{JSON.stringify res, null, 2}"
      # console.log "-----------------------------------------------------"
      return true
    .catch (err) =>
      errMsg = err.body?.message || err.message || JSON.stringify(err)
      @logger.error "#{meth} - Creating #{elementId}: #{errMsg}"
      q.reject new Error "#{meth} - Creating #{elementId}: #{errMsg}"


  replaceCustomResource: (group, version, namespace, plural, element) ->
    meth = 'k8sApiStub.replaceCustomResource'
    elementName = element.metadata.name
    elementId = "#{group}/#{version}/#{namespace}/#{plural}/#{elementName}"

    @logger.info "#{meth} - Replacing: #{elementId}"

    @k8sCustomApi.replaceNamespacedCustomObject group, version, namespace
    , plural, elementName, element, undefined, ADMISSION_FIELD_MANAGER_NAME
    .then (res) =>
      @logger.info "#{meth} - Successfully replaced object: #{elementId}"
      # console.log "-----------------------------------------------------"
      # console.log "RESULT:"
      # console.log "#{JSON.stringify res, null, 2}"
      # console.log "-----------------------------------------------------"
      return true
    .catch (err) =>
      errMsg = err.body?.message || err.message || JSON.stringify(err)
      @logger.error "#{meth} - Replacing #{elementId}: #{errMsg}"
      q.reject new Error "#{meth} - Replacing #{elementId}: #{errMsg}"


  saveCustomResource: (group, version, namespace, plural, element) ->
    meth = 'k8sApiStub.saveCustomResource'
    elementName = element.metadata.name
    elementId = "#{group}/#{version}/#{namespace}/#{plural}/#{elementName}"

    @logger.info "#{meth} - Saving #{elementId}"

    # If element already exists, then it is replaced by the new one.
    # This requires to get the existing element current 'resourceVersion' and
    # send it along in the 'replace' request.

    # Try to read the element
    @getCustomResource group, version, namespace, plural, elementName
    .then (existingElement) =>
      if existingElement?
        @logger.info "#{meth} - Element #{elementId} exists."
        # @logger.info "#{JSON.stringify existingElement}"
        element.setResourceVersion existingElement.metadata.resourceVersion
        return @replaceCustomResource group, version, namespace, plural, element
      else
        @logger.info "#{meth} - Element #{elementId} is new."
        return @createCustomResource group, version, namespace, plural, element


  deleteCustomResource: (group, version, namespace, plural, name) ->
    meth = 'k8sApiStub.deleteCustomResource'
    elementId = "#{group}/#{version}/#{namespace}/#{plural}/#{name}"
    @logger.debug "#{meth} - #{elementId}"

    missing = []
    if group is ''
      missing.push 'group'
    if version is ''
      missing.push 'version'
    if namespace is ''
      missing.push 'namespace'
    if plural is ''
      missing.push 'plural'
    if name is ''
      missing.push 'name'

    if missing.length isnt 0
      return q.reject new Error "#{meth} - Missing mandatory parameters:
                                 #{missing}"

    @k8sCustomApi.deleteNamespacedCustomObject group
    , version
    , namespace
    , plural
    , name
    , undefined  # gracePeriodSeconds
    , undefined  # orphanDependents
    , undefined  # propagationPolicy
    , undefined  # dryRun
    , undefined  # body
    , undefined   # options
    .then (res) =>
      @logger.info "#{meth} - Deleted object #{elementId}:
                    #{JSON.stringify res.body, null, 2}"
      # console.log "-----------------------------------------------------"
      # console.log "ELEMENT:"
      # console.log "#{JSON.stringify res.body}"
      # console.log "-----------------------------------------------------"
      return true
    .catch (err) =>
      if err.response?.body?.reason is 'NotFound'
        @logger.info "#{meth} - Object #{elementId} not found."
        return q null
      else
        errMsg = "Deleting object #{elementId}: #{JSON.stringify err}"
        @logger.error "#{meth} - #{errMsg}"
        q.reject new Error errMsg


  patchCustomResource: (group, version, namespace, plural, elementName, patch) ->
    meth = 'k8sApiStub.patchCustomResource'
    elementId = "#{group}/#{version}/#{namespace}/#{plural}/#{elementName}"

    @logger.info "#{meth} - Applying patch to: #{elementId}"

    options =
      headers:
        'Content-type': 'application/json-patch+json'
        # 'Content-type': k8s.PatchUtils.PATCH_FORMAT_JSON_PATCH}};

    @k8sCustomApi.patchNamespacedCustomObject group
    , version
    , namespace
    , plural
    , elementName
    , patch
    , undefined
    , ADMISSION_FIELD_MANAGER_NAME
    , undefined
    , options
    .then (res) =>
      @logger.info "#{meth} - Successfully patched object: #{elementId}"
      # console.log "-----------------------------------------------------"
      # console.log "RESULT:"
      # console.log "#{JSON.stringify res, null, 2}"
      # console.log "-----------------------------------------------------"
      return true
    .catch (err) =>
      errMsg = err.body?.message || err.message || JSON.stringify(err)
      @logger.error "#{meth} - Patching #{elementId}: #{errMsg}"
      q.reject new Error "#{meth} - Replacing #{elementId}: #{errMsg}"


  ##############################################################################
  ##                           TESTING METHODS                                ##
  ##############################################################################

  altGetPodLogs: (namespace, podName) ->
    meth = "k8sApiStub.altGetPodLogs pod=#{podName} - container=#{container}"

    name = "kucontroller-b8bd46bff-j47sw"
    namespace = 'kumori'
    container = null
    follow = false

    @k8sApi.readNamespacedPodLog name, namespace, container, follow
    .then (res) =>
      # Logs are returned as body text
      # console.log "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
      # console.log "BODY:"
      # console.log res.body
      # console.log "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
      return []
    .catch (err) =>
      errMsg = "Getting logs for Pod #{name}: #{JSON.stringify err}"
      @logger.error "#{meth} - #{errMsg}"
      q.reject new Error errMsg


  getKumoriElements: (plural) ->
    meth = 'k8sApiStub.getKumoriElement'
    @logger.debug "#{meth} - plural=#{plural}"

    group = 'kumori.systems'
    version = 'v1'
    namespace = 'kumori'
    name = ''

    @k8sCustomApi.getNamespacedCustomObject group, version, namespace, plural
    , name
    .then (res) =>
      @logger.info "#{meth} - Got #{res.body.items.length} objects:
                    #{JSON.stringify res.body.items, null, 2}"
      # console.log "-----------------------------------------------------"
      # console.log "KUMORI ELEMENT:"
      # console.log "#{JSON.stringify res.body.items}"
      # console.log "-----------------------------------------------------"
      return res.body.items
    .catch (err) =>
      errMsg = "Getting '#{plural}' objects: #{JSON.stringify err}"
      @logger.error "#{meth} - #{errMsg}"
      q.reject new Error errMsg


  listEvents: () ->
    meth = "k8sApiStub.listEvents"
    @logger.info meth

    @k8sApi.listEventForAllNamespaces()
    .then (res) =>
      @logger.info "#{meth} - Got #{res.body.items.length} objects."
      # console.log "-----------------------------------------------------"
      # console.log "LISTED EVENTS:"
      # console.log "#{JSON.stringify res.body}"
      # console.log "-----------------------------------------------------"
      return res.body.items
    .catch (err) =>
      errMsg = "Listing events: " +
                ( err.body?.message || err.message || JSON.stringify err )
      @logger.error "#{meth} - #{errMsg}"
      q.reject new Error "#{meth} - #{errMsg}"


  getCustomResourceObject: () ->
    group = 'kumori.systems'
    version = 'v1'
    plural = 'kumoricomponents'
    name = ''

    @k8sCustomApi.getClusterCustomObject group, version, plural, name
    .then (res) =>
      # console.log "-----------------------------------------------------"
      # console.log "CUSTOM RESOURCES:"
      # console.log "#{JSON.stringify res.body}"
      # console.log "-----------------------------------------------------"
      return true
    .catch (err) =>
      # console.log "ERROR GETTING CUSTOM RESOURCES: #{err.message || err}"
      # console.log "ERROR: #{JSON.stringify err}"
      errMsg = "Listing custom resources: #{err.message || err}"
      @logger.error "#{meth} - #{errMsg}"
      q.reject new Error "#{meth} - #{errMsg}"


  # test: (multiYaml) ->
  #   data = k8s.loadAllYaml multiYaml
  #   console.log "-----------------------------------------------------"
  #   console.log "LOADED FROM MULTI_YAML_STRING: #{JSON.stringify data}"
  #   console.log "-----------------------------------------------------"
  #   i = 0
  #   for item in data
  #     i++
  #     console.log "ITEM #{i}"
  #     console.log "API VERSION : #{item.apiVersion}"
  #     console.log "KIND        : #{item.kind}"
  #     console.log "METADATA    :"
  #     for metaKey, metaValue of item.metadata
  #       if metaKey isnt 'labels'
  #         console.log "  - #{metaKey}: #{metaValue}"
  #       else
  #         console.log "  - labels:"
  #         for labelName, labelValue of item.metadata.labels
  #           console.log "    - #{labelName}: #{labelValue}"
  #     console.log "-----------------------------------------------------"


  # printPodList: (podList) ->
  #   for item in (podList.items || [])
  #     console.log "-----------------------------------------------------"
  #     console.log "POD:"
  #     console.log "  Metadata: #{JSON.stringify item.metadata}"
  #     console.log "  Spec    : #{JSON.stringify item.spec}"
  #     console.log "  Status  : #{JSON.stringify item.status}"
  #     console.log "-----------------------------------------------------"


  ##############################################################################
  ##                              HELPER FUNCTIONS                            ##
  ##############################################################################
  quantityToScalar = (quantity) ->
    return 0 if not quantity?

    return 0 if quantity is 0

    if quantity.endsWith 'm'
      # Rounded to two decimals
      cleanNumberStr = quantity.substr 0, quantity.length - 1
      number = parseInt(cleanNumberStr, 10) / 1000.0
      return Math.round(number * 100) / 100

    if quantity.endsWith 'Ki'
      cleanNumberStr = quantity.substr 0, quantity.length - 2
      return parseInt(cleanNumberStr, 10) * 1024

    if quantity.endsWith 'K'
      cleanNumberStr = quantity.substr 0, quantity.length - 1
      return parseInt(cleanNumberStr, 10) * 1000

    if quantity.endsWith 'Mi'
      cleanNumberStr = quantity.substr 0, quantity.length - 2
      return parseInt(cleanNumberStr, 10) * 1024 * 1024

    if quantity.endsWith 'M'
      cleanNumberStr = quantity.substr 0, quantity.length - 1
      return parseInt(cleanNumberStr, 10) * 1000 * 1000

    if quantity.endsWith 'Gi'
      cleanNumberStr = quantity.substr 0, quantity.length - 2
      return parseInt(cleanNumberStr, 10) * 1024 * 1024 * 1024

    if quantity.endsWith 'G'
      cleanNumberStr = quantity.substr 0, quantity.length - 1
      return parseInt(cleanNumberStr, 10) * 1024 * 1000 * 1000

    if quantity.endsWith 'Ti'
      cleanNumberStr = quantity.substr 0, quantity.length - 2
      return parseInt(cleanNumberStr, 10) * 1024 * 1024 * 1024 * 1024

    if quantity.endsWith 'T'
      cleanNumberStr = quantity.substr 0, quantity.length - 1
      return parseInt(cleanNumberStr, 10) * 1024 * 1000 * 1000 * 1000

    if quantity.endsWith 'Pi'
      cleanNumberStr = quantity.substr 0, quantity.length - 2
      return parseInt(cleanNumberStr, 10) * 1024 * 1024 * 1024 * 1024 * 1024

    if quantity.endsWith 'P'
      cleanNumberStr = quantity.substr 0, quantity.length - 1
      return parseInt(cleanNumberStr, 10) * 1024 * 1000 * 1000 * 1000 * 1000

    num = parseInt quantity, 10
    if isNaN num
      throw new Error "Unknown quantity #{quantity}"
    else
      return num


  totalForResource = (pod, resource) ->
    total =
      request: 0
      limit: 0
      resourceType: resource

    for cont in (pod.spec?.containers || [])
      if cont.resources?
        if cont.resources.requests?
          total.request += quantityToScalar cont.resources.requests[resource]
        if cont.resources.limits?
          total.limit += quantityToScalar cont.resources.limits[resource]
    return total


  cleanConditions = (originalConditions) ->
    conditions = {}
    for oc in (originalConditions || [])
      conditions[oc.type] = oc.status
    conditions


  extractErrorMsg = (k8sApiError) ->
    errMsg = 'Unknown error (unable to determine error message)'
    if not k8sApiError?
      errMsg = 'Unknown error (API returned no info)'
    else if 'string' is typeof k8sApiError
      errMsg = k8sApiError
    else
      if k8sApiError.statusCode? and k8sApiError.statusCode isnt ''
        statusCodeMsg = " (StatusCode #{k8sApiError.statusCode})"
      else
        statusCodeMsg = ''

      if k8sApiError.message?
        errMsg = k8sApiError.message
      else if k8sApiError.response?.body?
        body = k8sApiError.response.body
        if body.message? and body.reason?
          errMsg = "#{body.reason}: #{body.message}"
        else
          try
            errMsg = "Original error body: #{JSON.stringify body}"
          catch e
            errMsg = "Original error body could not be parsed"
      else
        try
          errMsg = "Original error object: #{JSON.stringify k8sApiError}"
        catch e
          errMsg = "Original error object could not be parsed"

      errMsg = errMsg + statusCodeMsg

    return errMsg


module.exports = K8sApiStub


