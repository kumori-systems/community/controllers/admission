###
* Copyright 2022 Kumori Systems S.L.

* Licensed under the EUPL, Version 1.2 or – as soon they
  will be approved by the European Commission - subsequent
  versions of the EUPL (the "Licence");

* You may not use this work except in compliance with the
  Licence.

* You may obtain a copy of the Licence at:

  https://joinup.ec.europa.eu/software/page/eupl

* Unless required by applicable law or agreed to in
  writing, software distributed under the Licence is
  distributed on an "AS IS" basis,

* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
  express or implied.

* See the Licence for the specific language governing
  permissions and limitations under the Licence.
###

q            = require 'q'
EventEmitter = require('events').EventEmitter

ManifestHelper       = require './manifest-helper'
Authorization        = require './authorization'
utils                = require './utils'

DockerRegistryProxy = require './docker-registry-proxy'


################################################################################
##  Temporary mock classes for adapting Admission to KM3 model.               ##
################################################################################
class MockLimitChecker
  constructor: (@config) ->
  getLimits: () ->
    return @config.limits
  check: () ->
    return []
################################################################################
##  End of temporary mock classes for adapting Admission to KM3 model.        ##
################################################################################


class Admission extends EventEmitter


  constructor: (@config, @manifestRepository, @planner) ->
    super()
    @logger.info 'Admission.constructor'
    @manifestHelper = null
    @dockerRegistryProxy = null

    @config ?= {}

    # TODO: When planner API publishes information on its limits, this should
    # combined somehow with Planner limits.
    @config.limitChecker ?= { limits: {} }

    # By default, random domains will created as subdomains of RefDomain
    if @config.domains?.random? and @config.domains?.refDomain?
      if not @config.domains?.random?.baseDomain?
        @config.domains.random.baseDomain = @config.domains.refDomain


  init: () ->
    meth = 'Admission.init()'
    @logger.info meth

    # Build port dictionnary:  <externalPort> - <internalPort>
    @config.portDict = {}
    if @config.tcpports?
      if (not @config.tcpInternalPorts?) \
      or (@config.tcpports.length isnt @config.tcpInternalPorts.length)
        errMsg = "Invalid lists of ports: TcpPorts: #{@config.tcpports} -
                  TcpInternalPorts: #{@config.tcpInternalPorts}"
        @logger.error "#{meth} - ERROR: #{errMsg}"
        q.reject new Error errMsg
      else
        for port, index in @config.tcpports
          @config.portDict[port] = @config.tcpInternalPorts[index]

    # If Admission config does not include a ClusterConfiguration section (it
    # shouldn't at present). add one empty
    if not @config.clusterConfiguration?
      @config.clusterConfiguration = {}

    # If Planner is an event emitter, register a handler
    if @planner.on?
      @logger.info 'Admission.init() - Registering Planner event handler.'
      @planner.on 'planner', (evt) =>
        @logger.debug 'Admission.onPlannerEvent - forwarding event upwards'
        @emit 'planner', evt

    @authorization = new Authorization @manifestRepository, @planner

    # Read configuration file and initialize Manifest Storage
    @manifestHelper = new ManifestHelper()

    @limitChecker = new MockLimitChecker @config.limitChecker, @manifestHelper
    @logger.debug '- Limit Checker initialized.'

    dockerRegistryConfig = @config.dockerRegistry || { username: null, password: null, mirror: null }
    # Create a new Docker registry proxy for image validation
    @dockerRegistryProxy = new DockerRegistryProxy(dockerRegistryConfig)

    # Initialize element locks
    @elementLocks = {}

    return q true


  terminate: () ->
    @logger.info 'Admission.terminate()'
    @planner.terminate()
    .then =>
      @logger.debug 'Planner Terminated.'
      return true


  ##############################################################################
  ##                              PUBLIC METHODS                              ##
  ##############################################################################

  ############################
  ##   ELEMENT LOCK CACHE   ##
  ############################
  acquireElementLockFromRef: (elementRef, opID) ->
    meth = 'Admission.acquireElementLockFromRef'
    @logger.info "#{meth} - ElementRef: #{JSON.stringify elementRef} -
                  OperationID: #{opID}"
    elementURN = @manifestHelper.refToUrn elementRef
    @acquireElementLockFromURN elementURN, opID


  acquireElementLockFromURN: (elementURN, opID) ->
    meth = 'Admission.acquireElementLockFromURN'
    @logger.info "#{meth} - Element: #{elementURN} - OperationID: #{opID}"

    return q.Promise (resolve, reject) =>
      if @elementLocks[elementURN]?
        # Element has an active lock on it, deny operation
        msg = "element #{elementURN} has an active lock (opID: #{opID})"
        @logger.warn "#{meth} - Unable to get lock: #{msg}"
        reject new Error "Unable to get lock: #{msg}"
      else
        # Element is not locked, lock it
        @elementLocks[elementURN] = opID
        @logger.debug "#{meth} - Added lock to #{elementURN} (opID: #{opID})"
        @printElementLocks()
        resolve()


  releaseElementLockFromRef: (elementRef, opID) ->
    meth = 'Admission.releaseElementLockFromRef'
    @logger.info "#{meth} - ElementRef: #{JSON.stringify elementRef} -
                  OperationID: #{opID}"

    if elementRef?
      elementURN = @manifestHelper.refToUrn elementRef
    else
      elementURN = "unknownElement"

    @releaseElementLockFromURN elementURN, opID


  releaseElementLockFromURN: (elementURN, opID) ->
    meth = 'Admission.releaseElementLockFromURN'

    if not elementURN? or elementURN is ''
      elementURN = "unknownElement"
    @logger.info "#{meth} - Element: #{elementURN} - OperationID: #{opID}"

    return q.Promise (resolve, reject) =>
      if @elementLocks[elementURN]?

        lockOpID = @elementLocks[elementURN]
        if opID is lockOpID
          @logger.debug "#{meth} - Releasing lock #{elementURN} (opID: #{opID})"
          delete @elementLocks[elementURN]
          @printElementLocks()
        else
          # Element is locked by another operation, do nothing
          @logger.debug "#{meth} - Element #{elementURN} is locked by a another
                         operation (lockOpID: #{lockOpID} / opID: #{opID})"
      else
        @logger.debug "#{meth} - Element #{elementURN} had no active lock."

      # In all case, return a resolved promise
      resolve()


  clearElementLocks: () ->
    meth = 'Admission.clearElementLocks'
    @logger.warn "#{meth} - Removing all active element locks."
    @elementLocks = {}
    true


  printElementLocks: () ->
    meth = 'Admission.printElementLocks'
    @logger.info "#{meth} - CURRENT OPERATION LOCKS:
                  #{JSON.stringify @elementLocks}"
    # console.log '*****************************************************'
    # console.log 'CURRENT OPERATION LOCKS:'
    # console.log ''
    # console.log(JSON.stringify @elementLocks, null, 2)
    # console.log ''
    # console.log '*****************************************************'
    true


  getOperationID: () ->
    timestamp = @getDateSuffixLong()
    randomStr = utils.randomHex(8)
    return "#{timestamp}_#{randomStr}"





  ############################
  ##  SOLUTIONS MANAGEMENT  ##
  ############################
  registerSolution: (context, options, manifest = null) ->
    meth = "Admission.registerSolution() #{options.solutionURN}"
    @logger.info meth

    solutionManifest = null

    # Calculate a unique ID for this operation
    opID = @getOperationID()

    # If necessary, read manifest from local file
    (if manifest?
      q manifest
    else
      @loadManifest options
    )
    .then (loadedManifest) =>
      solutionManifest = loadedManifest

      # Check that the solution has a ref and a valid ref.domain, or add it
      if solutionManifest.ref?
        if solutionManifest.ref.domain?

          # ref.domain must match the id of the user performing the action,
          # except for Admin users
          if solutionManifest.ref.domain isnt context.user.id
            if @authorization.isAdmin context
              @logger.info "#{meth} - Admin user allowed to use a custom domain."
            else
              errMsg = "Solution domain doesn't match username:
                        (#{solutionManifest.ref.domain} != #{context.user.id}"
              @logger.error "#{meth} - ERROR: #{errMsg}"
              return q.reject new Error errMsg
        else
          @logger.warn "#{meth} - Solution has no domain, adding it."
          solutionManifest.ref.domain = context.user.id
      else
        errMsg = "Solution is missing mandatory property 'ref'."
        @logger.error "#{meth} - ERROR: #{errMsg}"
        throw new Error errMsg
    .then () =>
      # Aquire lock for the element
      @acquireElementLockFromRef solutionManifest.ref, opID
    .then () =>
      @setupSolution context, options, solutionManifest
    .then () =>
      @planner.execSolution solutionManifest
    .then (solutionInfo) =>
      @logger.debug "#{meth} - Solution created:
                     #{JSON.stringify solutionInfo}"
      solutionInfo
    .finally () =>
      @releaseElementLockFromRef (solutionManifest?.ref || null), opID


  deleteSolution: (context, options) ->
    meth = "Admission.deleteSolution #{options.solutionURN}"

    @logger.info meth
    @logger.debug "#{meth} - OPTIONS: #{JSON.stringify options}"
    solutionURN = options.solutionURN

    solutionManifest = null
    topDeploymentRef = null
    topDeploymentManifest = null

    # Calculate a unique ID for this operation
    opID = @getOperationID()

    # Get lock for the element being deleted
    @acquireElementLockFromURN solutionURN, opID
    .then () =>
      @getSolutionFromURN solutionURN, context
    .then (manifest) =>
      if not manifest?
        errMsg = "Solution #{solutionURN} not found."
        @logger.warn "#{meth} - #{errMsg}"
        return q.reject new Error errMsg

      # Solution exists, check its ownership
      solutionManifest = manifest
      @logger.debug "#{meth} - Solution #{solutionURN} exists"
      if not @authorization.isAllowedForManifest(manifest, context, true)
        errMsg = "User is not allowed to delete solution."
        @logger.warn "#{meth} - #{errMsg}"
        return q.reject new Error errMsg
      else
        # Ownership check is ok, check existing links of the top deployment
        topDeploymentRef =
          kind: 'deployment'
          domain: solutionManifest.ref.domain
          name: solutionManifest.top

        @getDeploymentFromRef topDeploymentRef, context
    .then (manifest) =>
      if not manifest?
        @logger.info "#{meth} - Top deployment
                      #{@manifestHelper.refToUrn topDeploymentRef} not found.
                      Skipping link verification."
        q {}
      else
        @logger.debug "#{meth} - Checking active links of the top deployment..."
        topDeploymentManifest = manifest
        @planner.getSolutionLinks solutionManifest.name, solutionURN
        , topDeploymentManifest.name, topDeploymentManifest.urn
    .then (links) =>
      linkNames = Object.keys links
      if linkNames.length > 0
        # Solution has active links
        if options.force
          @logger.info "#{meth} Automatically removing existing links..."
          promises = []
          for linkName, linkManifest of links
            promises.push @unlinkServices context, linkManifest
          q.all promises
          .then () =>
            @logger.info "#{meth} All solution links removed."
            return true
        else
          errMsg = "Solution #{solutionURN} has active links: #{linkNames}"
          @logger.error "#{meth} - #{errMsg}"
          return q.reject new Error  errMsg
      else
        # No links
        return true
    .then () =>
      @planner.deleteSolution solutionManifest.name, 'solution'
    .then (deleteSolutionInfo) =>
      @logger.debug "#{meth} - Solution deleted."
      deleteSolutionInfo
    .finally () =>
      @releaseElementLockFromURN solutionURN, opID


  setupSolution: (context, options, solutionManifest) ->
    meth = 'Admission.setupSolution()'
    @logger.info meth
    @logger.debug "#{meth} - Options: #{JSON.stringify options}"

    solutionName = null
    solutionURN = null

    currentTimestamp = utils.getTimestamp()


    # Check if the manifest includes an 'owner' property and if it matches the
    # ref.domain
    if solutionManifest.owner?
      if solutionManifest.owner is ''
        # Invalid owner received, delete it
        delete solutionManifest.owner
      else if solutionManifest.owner isnt solutionManifest.ref.domain
        # 'owner' property must match the ref.domain, except for Admin users
        if @authorization.isAdmin context
          @logger.info "#{meth} - Admin user allowed to provide an owner
                        different from the ref.domain."
        else
          errMsg = "Solution 'owner' property doesn't match the resource
                    domain: #{solutionManifest.owner} !=
                    #{solutionManifest.ref.domain}"
          @logger.error "#{meth} - ERROR: #{errMsg}"
          return q.reject new Error errMsg


    # Variable just used for log traces
    solutionName =
      solutionManifest.ref.domain + '/' + solutionManifest.ref.name

    # Validate manifest
    @validateSolution solutionManifest, context
    .then () =>
      @logger.debug "#{meth} - Checking existence of solution:
                     #{solutionName} ..."

      # Check if solution already exists
      solutionURN = @manifestHelper.refToUrn solutionManifest.ref
      @getSolutionFromURN solutionURN, context
    .then (previousManifest) =>
      if previousManifest?
        # Deployment already exists, check its ownership
        @logger.debug "#{meth} - Solution #{solutionName} already exists"
        if not @authorization.isAllowedForManifest(previousManifest, context, true)
          errMsg = "User is not allowed to modify solution."
          @logger.warn "#{meth} - #{errMsg}"
          return q.reject new Error errMsg
        else
          # Complete deployment information
          solutionManifest.owner ?= previousManifest.owner || context.user?.id
          solutionManifest.name  = previousManifest.name
          solutionManifest.urn   =
            previousManifest.urn || @manifestHelper.refToUrn solutionManifest.ref
          # Set timestamps
          solutionManifest.creationTimestamp =
            previousManifest.creationTimestamp || currentTimestamp
          solutionManifest.lastModification = currentTimestamp
          return solutionManifest
      else
        # Deployment is new, handle as a creation
        @logger.debug "#{meth} - Solution #{solutionName} is new"

        # Add a URN representing the solution for easier searches
        solutionManifest.urn = solutionURN

        # Add owner property from user information, if not present
        solutionManifest.owner ?= context.user?.id

        # Add timestamps
        solutionManifest.creationTimestamp = currentTimestamp
        solutionManifest.lastModification  = currentTimestamp

        # Add an internal name
        solutionManifest.name = @getSolutionID()
        return solutionManifest
    .catch (err) =>
      @logger.warn "#{meth} - #{err.message}"
      q.reject err


  validateSolution: (solutionManifest, context) ->
    @validateSolutionDeployments solutionManifest, context
    .then () =>
      @validateSolutionResources solutionManifest, context
    .then () =>
      @validateSolutionLinks solutionManifest, context
    .then () =>
      return q solutionManifest


  solutionQuery:  (context, params) ->
    @logger.info 'Admission.solutionQuery()'
    q @planner.solutionQuery(params)


  getSolution: (filter, context) ->
    meth = "Admission.getSolution(#{JSON.stringify filter})"
    @logger.info "#{meth} - User: #{context.user?.id}"

    name = null

    params =
      show: 'manifest'

    if filter.urn?
      name = filter.urn
      params.urn = filter.urn
    else
      errMsg = "Wrong filter for solution search: #{JSON.stringify filter}"
      @logger.warn "#{meth} - #{errMsg}"
      return q.reject new Error errMsg

    @logger.info "#{meth} - Searching for solution: #{name}"

    @planner.solutionQuery params
    .then (solutionList) =>
      if Object.keys(solutionList).length is 0
        @logger.debug "#{meth} - No solution found for #{name}"
        return null
      else if Object.keys(solutionList).length > 1
        errMsg = "Several matches found for solution #{name}"
        @logger.warn "#{meth} - #{errMsg}: #{JSON.stringify solutionList}"
        return q.reject new Error errMsg
      else
        deploymentURN = Object.keys(solutionList)[0]
        @logger.info "#{meth} - Found solution for #{name} : #{deploymentURN}"
        return solutionList[deploymentURN]



  getSolutionFromURN: (urn, context) ->
    meth = "Admission.getSolutionFromURN(#{urn})"
    @logger.info "#{meth} - User: #{context.user?.id}"

    filter =
      urn: urn

    return @getSolution filter, context


  getSolutionFromRef: (ref, context) ->
    meth = "Admission.getSolutionFromRef(#{JSON.stringify ref})"
    @logger.info "#{meth} - User: #{context.user?.id}"

    filter =
      urn: @manifestHelper.refToUrn ref

    return @getSolution filter, context





  ##############################
  ##  DEPLOYMENTS MANAGEMENT  ##
  ##############################
  deploy: (context, deploymentOptions, manifest = null) ->
    @logger.info 'Admission.deploy()'
    @setupDeploy context, deploymentOptions, manifest
    .then (fullDeployment) =>
      @logger.debug "after setupDeploy: #{JSON.stringify fullDeployment}"
      @planner.execDeployment fullDeployment
      .then (deploymentInfo) =>
        @logger.debug ' --> Deployment executed by Planner. ' +
          "#{JSON.stringify deploymentInfo}"
        deploymentInfo


  undeploy: (context, options) ->
    meth = "Admission.undeploy #{options.deploymentURN}"
    if options.force
      meth += ' (force)'

    @logger.info meth
    @logger.debug "UNDEPLOY: #{JSON.stringify options}"
    deploymentURN = options.deploymentURN

    deploymentManifest = null

    @getDeploymentFromURN deploymentURN, context
    .then (manifest) =>
      if not manifest?
        errMsg = "Deployment #{deploymentURN} not found."
        @logger.warn "#{meth} - #{errMsg}"
        return q.reject new Error errMsg

      # Deployment exists, check its ownership
      deploymentManifest = manifest
      @logger.debug "#{meth} - Deployment #{deploymentURN} exists"
      if not @authorization.isAllowedForManifest(manifest, context, true)
        errMsg = "User is not allowed to delete deployment."
        @logger.warn "#{meth} - #{errMsg}"
        return q.reject new Error errMsg
      else
        # Ownership check is ok, check existing links
        @planner.getDeploymentLinks manifest
    .then (links) =>
      linkNames = Object.keys links
      if linkNames.length > 0
        # Deployment has active links
        if options.force
          @logger.info "#{meth} Automatically removing existing links..."
          promises = []
          for linkName, linkManifest of links
            promises.push @unlinkServices context, linkManifest
          q.all promises
          .then () =>
            @logger.info "#{meth} All deployment links removed."
            return true
        else
          errMsg = "Deployment #{deploymentURN} has active links: #{linkNames}"
          @logger.error "#{meth} - #{errMsg}"
          return q.reject new Error  errMsg
      else
        # No links
        return true
    .then () =>
      @planner.execUndeployment deploymentManifest.name, deploymentManifest.ref.kind
    .then (undeploymentInfo) =>
      @logger.debug ' --> Undeployment executed by Planner.'
      undeploymentInfo


  setupDeploy: (context, deploymentOptions, manifest = null) ->
    meth = 'Admission.setupDeploy()'
    @logger.info meth
    @logger.debug "#{meth} - Options: #{JSON.stringify deploymentOptions}"

    refStr = null
    deploymentManifest = null

    # If necessary, read manifest from local file
    (if manifest?
      q manifest
    else
      @loadManifest deploymentOptions
    )
    .then (deplManifest) =>
      deploymentManifest = deplManifest

      # Validate manifest
      @validateV3Deployment deploymentManifest, context
    .then () =>
      refStr = JSON.stringify deploymentManifest.ref
      @logger.debug "#{meth} - Checking existence of deployment: #{refStr} ..."

      # Check if deployment already exists
      @getDeploymentFromRef deploymentManifest.ref, context
    .then (previousManifest) =>
      if previousManifest?
        # Deployment already exists, check its ownership
        @logger.debug "#{meth} - Deployment #{refStr} already exists"
        if not @authorization.isAllowedForManifest(previousManifest, context, true)
          errMsg = "User is not allowed to modify deployment."
          @logger.warn "#{meth} - #{errMsg}"
          return q.reject new Error errMsg
        else
          # Complete deployment information
          deploymentManifest.owner = previousManifest.owner || context.user?.id
          deploymentManifest.name  = previousManifest.name
          deploymentManifest.urn   =
            previousManifest.urn || @manifestHelper.refToUrn deploymentManifest.ref
          return deploymentManifest
      else
        # Deployment is new, handle as a creation
        @logger.debug "#{meth} - Deployment #{refStr} is new"

        # Add owner property from user information
        deploymentManifest.owner = context.user?.id

        # Temporarily add a URN representing the ref for easier searches
        # during Kumori Model transition.
        deploymentManifest.urn = @manifestHelper.refToUrn deploymentManifest.ref

        # Add a globally unique name to the deployment manifest (internal ID)
        deploymentManifest.name = @getDeploymentNameKmv3()
        return deploymentManifest
    .catch (err) =>
      @logger.warn "#{meth} - #{err.message}"
      q.reject err


  deploymentQuery:  (context, params) ->
    @logger.info 'Admission.deploymentQuery()'
    q @planner.deploymentQuery(params)


  getDeployment: (filter, context) ->
    meth = "Admission.getDeployment(#{JSON.stringify filter})"
    @logger.info "#{meth} - User: #{context.user?.id}"

    name = null

    params =
      show: 'manifest'

    if filter.ref?
      name = @manifestHelper.refToUrn filter.ref
      params.ref = filter.ref
    else if filter.urn?
      name = filter.urn
      params.urn = filter.urn
    else
      errMsg = "Wrong filter for deployment search: #{JSON.stringify filter}"
      @logger.warn "#{meth} - #{errMsg}"
      return q.reject new Error errMsg

    @logger.info "#{meth} - Searching for deployment: #{name}"

    @planner.getDeployment params



  getDeploymentFromRef: (ref, context) ->
    meth = "Admission.getDeploymentFromRef(#{JSON.stringify ref})"
    @logger.info "#{meth} - User: #{context.user?.id}"

    filter =
      ref: ref

    return @getDeployment filter, context



  getDeploymentFromURN: (urn, context) ->
    meth = "Admission.getDeploymentFromURN(#{urn})"
    @logger.info "#{meth} - User: #{context.user?.id}"

    filter =
      urn: urn

    return @getDeployment filter, context


  # Expects options object to contain the following properties:
  # - deploymentURN
  # - deploymentName (internal ID)
  # - deploymentRole
  # - instanceName
  restartInstances: (context, options) ->
    meth = "Admission.restartInstances #{JSON.stringify options}"
    @logger.info meth

    (if options.instanceName?
      # Restart the instance
      @logger.debug "#{meth} - Instance name: #{options.instanceName}"
      @planner.restartInstance options.instanceName
    else
      # Restart the role
      @logger.debug "#{meth} - Deployment: #{options.deploymentName}
                     Role: #{options.deploymentRole}"
      @planner.restartRole options.deploymentName, options.deploymentRole
    )
    .then () =>
      @logger.info "#{meth} - Successfully restarted instances."
      return true



  solutionElementToDeploymentElement: (solURN, solRole, solInstance, context) ->
    meth = 'Admission.solutionElementToDeploymentElement'

    @logger.debug "#{meth} - Converting element: Solution URN: #{solURN} -
                   Role: #{solRole} - Instance: solInstance"


    # The info we need to determine
    deplURN      = null
    deplName     = null   # Deployment internal ID
    deplRole     = null
    instanceName = null   # The name of the Pod representing the instance

    # Determine the solution top deployment URN (same as the solution except for
    # the type)
    solRef = @manifestHelper.urnToRef solURN
    topDeplRef =
      domain: solRef.domain
      kind: 'deployment'
      name: solRef.name
    topDeplURN = @manifestHelper.refToUrn topDeplRef

    # Determine the URN of the deployment the instance belongs to

    # Find out the subdeployment corresponding to the role
    # - if role name is compound (for example 'first.second.third'), the last
    #   portion is the actual role, and the leading portions are part of the
    #   sub-deployment name.
    # - if role name is simple (for example 'first'), it is the role 'as is'
    #   and the deployment name is the same as the solution
    segments = solRole.split '.'

    if segments.length is 1
      deplRole = solRole
      deplURN = topDeplURN
    else if segments.length > 1
      deplRole = segments.pop()    # Get the last element and remove it
      console.log "deplRole: #{deplRole}"
      console.log "segments: #{segments}"
      deplURN = "#{topDeplURN}.#{segments.join '.'}"
    else
      errMsg = "Invalid role name (dots): #{solRole}"
      @logger. error "#{meth} - #{errMsg}"
      return q.reject new Error errMsg


    # Get the affected deployment manifest  (to get its internal ID/name)
    @getDeploymentFromURN deplURN, context
    .then (manifest) =>
      if not manifest?
        errMsg = "Deployment #{deplURN} not found."
        @logger.warn "#{meth} - #{errMsg}"
        return q.reject new Error errMsg

      @logger.debug "#{meth} - Deployment #{deplURN} exists"

      deplName = manifest.name

      if solInstance?
        # Determine the instance Pod name
        hashedRoleName = @manifestHelper.hashLabel deplRole
        instanceName = "#{manifest.name}-#{hashedRoleName}-deployment-"+
          "#{solInstance}"
        @logger.debug "#{meth} - Instance name: #{instanceName}"
      else
        instanceName = null

      true
    .then () =>
      result =
        deploymentURN:  deplURN
        deploymentName: deplName
        deploymentRole: deplRole
        instanceName:   instanceName
      @logger.debug "#{meth} - Result: #{JSON.stringify result}"

      return result




  ############################
  ##  RESOURCES MANAGEMENT  ##
  ############################
  registerResource: (context, resourceManifest) ->
    meth = 'Admission.registerResource()'
    @logger.info "#{meth} Resource Ref: #{JSON.stringify resourceManifest.ref}"

    currentTimestamp = utils.getTimestamp()

    # Check that the resource has a ref and a valid ref.domain, or add it
    if resourceManifest.ref?
      if resourceManifest.ref.domain?
        # ref.domain must match the id of the user performing the action,
        # except for Admin users
        if resourceManifest.ref.domain isnt context.user.id
          if @authorization.isAdmin context
            @logger.info "#{meth} - Admin user allowed to use a custom domain."
          else
            errMsg = "Resource domain doesn't match username:
                      (#{resourceManifest.ref.domain} != #{context.user.id}"
            @logger.error "#{meth} - ERROR: #{errMsg}"
            return q.reject new Error errMsg
      else
        @logger.warn "#{meth} - Resource has no domain, adding it."
        resourceManifest.ref.domain = context.user.id
    else
      errMsg = "Reseource is missing mandatory property 'ref'."
      @logger.error "#{meth} - ERROR: #{errMsg}"
      return q.reject new Error errMsg

    # Check if the manifest includes an 'owner' property and if it matches the
    # ref.domain
    if resourceManifest.owner?
      if resourceManifest.owner is ''
        # Invalid owner received, delete it
        delete resourceManifest.owner
      else if resourceManifest.owner isnt resourceManifest.ref.domain
        # 'owner' property must match the ref.domain, except for Admin users
        if @authorization.isAdmin context
          @logger.info "#{meth} - Admin user allowed to provide an owner
                        different from the ref.domain."
        else
          errMsg = "Resource 'owner' property doesn't match the resource domain:
                    #{resourceManifest.owner} != #{resourceManifest.ref.domain}"
          @logger.error "#{meth} - ERROR: #{errMsg}"
          return q.reject new Error errMsg


    refStr = JSON.stringify resourceManifest.ref
    resourceURN = @manifestHelper.refToUrn resourceManifest.ref
    @logger.info "#{meth} - Checking existence of resource: #{resourceURN} ..."


    # Try to acquire lock for the operation
    # Calculate a unique ID for this operation
    opID = @getOperationID()

    # Acquire lock on the resource
    @acquireElementLockFromURN resourceURN, opID
    .then () =>
      # Lock acquired, proceed
      # Check if resource already exists
      @manifestRepository.getManifestByURN resourceURN
    .catch (err) =>
      if err.message?.includes 'Element not found'
        return null
      else
        q.reject err
    .then (previousManifest) =>
      if previousManifest?
        # Resource already exists, check its ownership
        @logger.info "#{meth} - Resource #{refStr} already exists"
        if not @authorization.isAllowedForManifest(previousManifest, context, true)
          errMsg = "User is not allowed to modify resource."
          @logger.warn "#{meth} - #{errMsg}"
          return q.reject new Error errMsg
        else if previousManifest.ref.kind not in @manifestHelper.getUpdatableResourceKinds()
          errMsg = "Update operation is not supported for type #{previousManifest.ref.kind}."
          @logger.warn "#{meth} - #{errMsg}"
          return q.reject new Error errMsg
        else
          # Complete deployment information
          resourceManifest.owner ?= previousManifest.owner || context.user?.id
          resourceManifest.name  = previousManifest.name
          resourceManifest.urn   = previousManifest.urn
          resourceManifest.creationTimestamp =
            previousManifest.creationTimestamp || currentTimestamp
          resourceManifest.lastModification = currentTimestamp

          return resourceManifest
      else
        # Resource is new, handle as a creation
        @logger.info "#{meth} - Resource #{refStr} is new"

        resourceManifest.owner ?= context.user?.id
        resourceManifest.name  = @generateResourceName()
        resourceManifest.urn   = resourceURN
        resourceManifest.creationTimestamp = currentTimestamp
        resourceManifest.lastModification = currentTimestamp
        return resourceManifest
    .then (resManifest) =>
      @validateResource context, resManifest
    .then (resManifest) =>
      @manifestRepository.storeManifest resManifest
    .then (resName) =>
      @logger.info "#{meth} - Resource #{refStr} stored with name
                    #{resourceManifest.name} and URN #{resourceManifest.urn}"
      resourceManifest.urn
    .finally () =>
      @releaseElementLockFromURN resourceURN, opID


  listResources: (filter) ->
    @logger.info "Admission.listResources() filter = #{JSON.stringify filter}"
    @planner.listResources filter


  listResourcesInUse: (filter, context) ->
    @logger.info "Admission.listResourcesInUse() filter = \
      #{JSON.stringify filter}"
    @planner.listResourcesInUse filter, context


  describeResource: (resURN) ->
    @logger.info "Admission.describeResource() Resource URN: #{resURN}"
    @planner.describeResource resURN



  ######################
  #  LINKS MANAGEMENT  #
  ######################
  linkServices: (context, linkManifest) ->
    meth = "Admission.linkServices #{JSON.stringify linkManifest.endpoints}"
    @logger.info "#{meth}"

    linkID = null

    # Calculate a unique ID for this operation
    opID = @getOperationID()

    # Validate that if the link manifest contains an owner property it matches
    # the current user ID (unless current user has ADMIN role)
    if linkManifest.owner?
      if linkManifest.owner is ''
        # Invalid owner received, delete it
        delete linkManifest.owner
      else if linkManifest.owner isnt context.user.id
        # The 'owner' property must match the user ID, except for Admin users
        if @authorization.isAdmin context
          @logger.info "#{meth} - Admin user allowed to provide an owner other
                        than himself."
        else
          errMsg = "Link manifest 'owner' property doesn't match the current
                    user id: #{linkManifest.owner} != #{context.user.id}"
          @logger.warn "#{meth} - #{errMsg}"
          return q.reject new Error errMsg


    # Links manifests contain references to Solutions, but links are performed
    # internally on Deployments.

    # Determine top deployments of each linked solution
    sol1Ref =
      kind:   linkManifest.endpoints[0].kind
      domain: linkManifest.endpoints[0].domain
      name:   linkManifest.endpoints[0].name
    sol1URN = @manifestHelper.refToUrn sol1Ref

    sol2Ref =
      kind:   linkManifest.endpoints[1].kind
      domain: linkManifest.endpoints[1].domain
      name:   linkManifest.endpoints[1].name
    sol2URN = @manifestHelper.refToUrn sol2Ref


    # Validate that the provided endpoints are solutions
    if not @manifestHelper.isSolutionURN sol1URN
      errMsg = "Invalid endpoint #{sol1URN}."
      @logger.warn "#{meth} - #{errMsg}"
      return q.reject new Error errMsg

    if not @manifestHelper.isSolutionURN sol2URN
      errMsg = "Invalid endpoint #{sol2URN}."
      @logger.warn "#{meth} - #{errMsg}"
      return q.reject new Error errMsg


    # Top deployment of solution 1
    depl1URN = null
    depl1Manifest = null
    depl1ChannelType = null
    depl1Channel = linkManifest.endpoints[0].channel

    # Top deployment of solution 2
    depl2URN = null
    depl2Manifest = null
    depl2ChannelType = null
    depl2Channel = linkManifest.endpoints[1].channel


    # Perform validations on the first endpoint

    @getSolutionFromURN sol1URN, context
    .then (manifest) =>
      if not manifest?
        errMsg = "Solution #{sol1URN} not found."
        @logger.warn "#{meth} - #{errMsg}"
        return q.reject new Error errMsg

      # Solution exists, check its ownership
      sol1Manifest = manifest
      @logger.debug "#{meth} - Solution #{sol1URN} exists"
      if not @authorization.isAllowedForManifest(manifest, context, true)
        errMsg = "User is not allowed to link solution #{sol1URN}."
        @logger.warn "#{meth} - #{errMsg}"
        return q.reject new Error errMsg

      # Check if the solution is being deleted
      if sol1Manifest.deletionTimestamp?
        errMsg = "Solution is being deleted (#{sol1URN})."
        @logger.warn "#{meth} - #{errMsg}"
        return q.reject new Error errMsg

      # Determine and get the Solution top deployment
      depl1Ref =
        kind: 'deployment'
        domain: sol1Manifest.ref.domain
        name: sol1Manifest.top
      depl1URN = @manifestHelper.refToUrn depl1Ref

      @getDeploymentFromURN depl1URN, context
    .then (manifest) =>
      if not manifest?
        errMsg = "Deployment #{depl1URN} not found."
        @logger.warn "#{meth} - #{errMsg}"
        return q.reject new Error errMsg

      @logger.debug "#{meth} - Deployment #{depl1URN} found."
      depl1Manifest = manifest

      # Deployment channel is mandatory and must be one of the deployment
      # channels
      serverChannels = depl1Manifest.artifact?.description?.srv?.server || {}
      clientChannels = depl1Manifest.artifact?.description?.srv?.client || {}
      duplexChannels = depl1Manifest.artifact?.description?.srv?.duplex || {}

      if (not depl1Channel?) \
      or ((depl1Channel not of serverChannels) \
      and (depl1Channel not of clientChannels) \
      and (depl1Channel not of duplexChannels))
        errMsg = "Invalid channel '#{depl1Channel}' for solution #{sol1URN}."
        @logger.warn "#{meth} - #{errMsg}"
        return q.reject new Error errMsg

      # Determine the type of the first endpoint channel (client/server/duplex)
      if depl1Channel of serverChannels
        depl1ChannelType = "server"
      else if depl1Channel of clientChannels
        depl1ChannelType = "client"
      else
        depl1ChannelType = "duplex"

      return true
    .then () =>

      # Perform validations on the second endpoint

      @getSolutionFromURN sol2URN, context
    .then (manifest) =>
      if not manifest?
        errMsg = "Solution #{sol2URN} not found."
        @logger.warn "#{meth} - #{errMsg}"
        return q.reject new Error errMsg

      # Solution exists, check its ownership
      sol2Manifest = manifest
      @logger.debug "#{meth} - Solution #{sol2URN} exists"
      if not @authorization.isAllowedForManifest(manifest, context, true)
        errMsg = "User is not allowed to link solution #{sol2URN}."
        @logger.warn "#{meth} - #{errMsg}"
        return q.reject new Error errMsg

      # Check if the solution is being deleted
      if sol2Manifest.deletionTimestamp?
        errMsg = "Solution is being deleted (#{sol2URN})."
        @logger.warn "#{meth} - #{errMsg}"
        return q.reject new Error errMsg

      # Determine and get the Solution top deployment
      depl2Ref =
        kind: 'deployment'
        domain: sol2Manifest.ref.domain
        name: sol2Manifest.top
      depl2URN = @manifestHelper.refToUrn depl2Ref

      @getDeploymentFromURN depl2URN, context
    .then (manifest) =>
      if not manifest?
        errMsg = "Deployment #{depl2URN} not found."
        @logger.warn "#{meth} - #{errMsg}"
        return q.reject new Error errMsg

      @logger.debug "#{meth} - Deployment #{depl2URN} found."
      depl2Manifest = manifest

      # Deployment channel is mandatory and must be one of the deployment
      # channels
      serverChannels = depl2Manifest.artifact?.description?.srv?.server || {}
      clientChannels = depl2Manifest.artifact?.description?.srv?.client || {}
      duplexChannels = depl2Manifest.artifact?.description?.srv?.duplex || {}

      if (not depl2Channel?) \
      or ((depl2Channel not of serverChannels) \
      and (depl2Channel not of clientChannels) \
      and (depl2Channel not of duplexChannels))
        errMsg = "Invalid channel '#{depl2Channel}' for solution #{sol2URN}."
        @logger.warn "#{meth} - #{errMsg}"
        return q.reject new Error errMsg

      # Determine the type of the first endpoint channel (client/server/duplex)
      if depl2Channel of serverChannels
        depl2ChannelType = "server"
      else if depl2Channel of clientChannels
        depl2ChannelType = "client"
      else
        depl2ChannelType = "duplex"

      return true
    .then () =>

      # Perform validation on the channel type to be linked

      # Two server channels or two client channels cannot be linked.
      # Also, links between two duplex are not currently allowed.
      if (depl1ChannelType is depl2ChannelType)
        errMsg = "Two #{depl2ChannelType} channels cannot be linked."
        @logger.warn "#{meth} - #{errMsg}"
        return q.reject new Error errMsg

      return true
    .then () =>
      # All checks passed, try to acquire lock for the link operation

      # Calculate a link ID
      linkID = @generateLinkName linkManifest

      # Acquire lock on the link
      # ID is not a URN, but link don't have URNs
      @acquireElementLockFromURN linkID, opID
    .then () =>
      # Lock acquired, proceed with the link operation

      # Add 'owner' property to the link manifest, if it doesn't have one
      linkManifest.owner ?= context.user?.id

      linkManifest.endpoints[0].kumoriName = depl1Manifest.name
      linkManifest.endpoints[1].kumoriName = depl2Manifest.name

      # Add a new 'name' property to the link manifest to be able to delete it
      # later.
      if not linkManifest.name?
        linkManifest.name = linkID
        @logger.debug "#{meth} - Assigned new link name: #{linkManifest.name}"

      @planner.linkServices linkManifest
    .finally () =>
      # ID is not a URN, but link don't have URNs
      @releaseElementLockFromURN linkID, opID



  unlinkServices: (context, linkManifest) ->
    meth = "Admission.unlinkServices #{JSON.stringify linkManifest.endpoints}"
    @logger.info "#{meth}"

    # Calculate a link ID
    linkID = @generateLinkName linkManifest

    # Calculate a unique ID for this operation
    opID = @getOperationID()

    if not linkManifest.name?
      linkManifest.name = linkID
      @logger.debug "#{meth} - Calculated link name: #{linkManifest.name}"

    prevManifest = null

    # Search link manifest
    @logger.debug "#{meth} - Getting existing link manifest"
    @manifestRepository.getManifestByNameAndKind linkManifest.name, 'link'
    .then (previousManifest) =>
      prevManifest = previousManifest
      @logger.debug "#{meth} - Link manifest found."
      # Check if user is allowed to delete link (requires write permission)
      if not @authorization.isAllowedForManifest(prevManifest, context, true)
        errMsg = "User not authorized (user:#{context.user.id} -
                  owner: #{prevManifest.owner})"
        @logger.error "#{meth} - ERROR: #{errMsg}"
        return q.reject new Error errMsg
      else
        @logger.debug "#{meth} - User is allowed to delete manifest."
    .then () =>
      # Try to acquire lock for the unlink operation

      # Acquire lock on the link
      # ID is not a URN, but link don't have URNs
      @acquireElementLockFromURN linkID, opID

    .then () =>
      # Lock acquired, proceed with the link operation

      # Check element is a Link (a little tricky since there is no 'kind')
      if (prevManifest.name.startsWith 'kl-') \
      and ('endpoints' of prevManifest)
        return @planner.unlinkServices linkManifest.name
      else
        errMsg = 'Invalid element type (not a Link)'
        @logger.error "#{meth} - ERROR: #{errMsg}"
        return q.reject new Error errMsg
    .catch (err) =>
      errMsg = "Unable to find the provided link."
      @logger.warn "#{meth} - ERROR: #{errMsg}"
      throw new Error errMsg
    .finally () =>
      # ID is not a URN, but link don't have URNs
      @releaseElementLockFromURN linkID, opID



  unlinkServicesFromURN: (context, linkURN) ->
    meth = "Admission.unlinkServicesFromURN #{linkURN}"
    @logger.info "#{meth}"

    # Currently (after ticket 215) link are not Manifests anymore but operations
    # and as such don't have `ref` identifiers or internal URNs. The only
    # possible way to refer to an existing link is by the whole manifest or by
    # the Kumori internal ID, which is not available to users.

    # For helping Admin users during development, we will temporarily allow
    # passing the internal ID as a URN. For normal users, this method won't be
    # allowed.
    if @authorization.isAdmin context
      return @unlinkServicesFromName context, linkURN
    else
      # Reject the operation
      errMsg = "Operation not allowed (links don't have URNs)."
      @logger.error "#{meth} - ERROR: #{errMsg}"
      return q.reject new Error errMsg


  unlinkServicesFromName: (context, linkName) ->
    meth = "Admission.unlinkServicesFromName #{linkName}"
    @logger.info "#{meth}"

    # For helping Admin users during development, we will temporarily allow
    # passing the internal ID as a URN. For normal users, this method won't be
    # allowed.
    if not @authorization.isAdmin context
      errMsg = "Operation not allowed for user #{context.user.id}"
      @logger.error "#{meth} - ERROR: #{errMsg}"
      return q.reject new Error errMsg

    # Try to acquire lock for the unlink operation

    # Calculate a unique ID for this operation
    opID = @getOperationID()

    # Acquire lock on the link
    # ID is not a URN, but link don't have URNs
    @acquireElementLockFromURN linkName, opID
    .then () =>
      # Lock acquired, proceed with the link operation
      @manifestRepository.getManifestByNameAndKind linkName, 'link'
    .then (linkManifest) =>
      # Check element is a Link (a little tricky since there is no 'kind')
      if (linkManifest.name.startsWith 'kl-') \
      and ('endpoints' of linkManifest)
        @planner.unlinkServices linkManifest.name
      else
        errMsg = 'Invalid element type (not a Link)'
        @logger.error "#{meth} - ERROR: #{errMsg}"
        return q.reject new Error errMsg
    .finally () =>
      # ID is not a URN, but link don't have URNs
      @releaseElementLockFromURN linkName, opID



  #####################
  #   HELPER METHODS  #
  #####################
  setClusterConfiguration: (newClusterConfiguration) ->
    @logger.info 'Admission.setClusterConfiguration()'
    @config.clusterConfiguration = newClusterConfiguration
    # console.log "CLUSTER CONFIGURATION:"
    # console.log "#{JSON.stringify @config.clusterConfiguration, null, 2}"


  validateV3Deployment: (manifest, context) ->
    meth = 'Admission.validateV3Deployment()'
    @logger.info meth

    # Validate that all requested resources are OK
    @validateV3DeploymentResources manifest, context
    .then (deploymentManifest) =>
      @validateV3DeploymentImages manifest, context
    .then (deploymentManifest) =>
      @logger.info "#{meth} All validations finished."
      return deploymentManifest


  validateV3DeploymentImages: (manifest) ->
    meth = 'Admission.validateV3DeploymentImages()'
    @logger.info meth

    @logger.info "#{meth} Validating used component docker images"

    serviceRoles = manifest.description?.service?.description?.role || {}

    promiseChain = q()
    errors = []

    for roleName, roleData of serviceRoles
      roleContainers = roleData.component?.description?.code || {}
      roleResources = roleData.artifact?.description?.config?.resource || {}
      for contName, contData of roleContainers
        imageData = contData.image
        do (roleName, contName, imageData, roleResources) =>
          @logger.info "#{meth} Validating role #{roleName} - container #{contName}"
          @logger.info "#{meth} Docker image: #{JSON.stringify imageData}"

          try
            dockerImage = @dockerRegistryProxy.normalizeDockerImage imageData
            @logger.info "#{meth} Normalized image: #{JSON.stringify dockerImage}"
            promiseChain = promiseChain.then () =>
              @validateComponentImage dockerImage, roleResources, context
              .then () =>
                @logger.info "#{meth} - Validated role #{roleName} - container #{contName}"
                true
              .catch (error) =>
                errMsg = "Failed docker image validation for role '#{roleName}'
                          container '#{contName}' image
                          '#{dockerImage.hub}/#{dockerImage.name}:#{dockerImage.ref}'
                          ERROR: #{error.message}"
                errors.push errMsg
                true
          catch normErr
            errMsg = "Failed docker image validation for role '#{roleName}'
                      container '#{contName}' tag '#{imageData.tag}'
                      ERROR: #{normErr.message}"
            errors.push errMsg



    promiseChain.then () =>
      @logger.info "#{meth} All component images validations finished."
      if errors.length > 0
        errMsg = "The following component docker images could not be validated:
                  #{JSON.stringify errors}"
        @logger.error "#{meth} #{errMsg}"
        q.reject new Error errMsg
      else
        @logger.info "#{meth} All component images validated!"
        return manifest


  # Resolves an internal resource name (a key in config.resources) to the actual
  # resource object
  resolveInternalResource: (internalResourceName, roleResources) ->
    meth = "Admission.resolveInternalResource(#{internalResourceName})"
    @logger.info "#{meth}"

    if internalResourceName of roleResources
      @logger.info "#{meth} Resolved to: #{JSON.stringify roleResources[internalResourceName]}"
      return roleResources[internalResourceName]
    else
      return null


  # Expects imageData to contain the following keys:
  #  - hub
  #  - secret
  #  - name
  #  - ref
  validateComponentImage: (imageData, roleResources, context) ->
    meth = 'Admission.validateComponentImage()'
    @logger.info "#{meth} Image: #{JSON.stringify imageData}"

    if imageData.secret? and imageData.secret isnt ''

      # Resolve the internal resource name
      secretResourceObj =
        @resolveInternalResource imageData.secret, roleResources

      if not secretResourceObj?
        errMsg = "Internal resource reference '#{imageData.secret}' can not be
                  resolved against role configuration."
        @logger.info "#{meth} - #{errMsg}"
        return q.reject new Error errMsg

      # Normalize the secret resource reference
      secretResource =
        @normalizeResourceReference secretResourceObj.secret, context

      # Fetch credential from ApiServer secret object
      @logger.info "#{meth} Fetching Secret: #{secretResource}"
      @planner.getDockerCredentialsFromSecret secretResource
      .then (credentials) =>
        @logger.info "#{meth} Got credentials!"

        @logger.info "#{meth} Checking image existence..."
        @dockerRegistryProxy.checkImage imageData.hub
        , imageData.name
        , imageData.ref
        , credentials.username
        , credentials.password
      .then (exists) =>
        if exists
          @logger.info "#{meth} - Image validated!"
          return true
        else
          @logger.info "#{meth} - Image could not be found."
          q.reject new Error "Image could not be validated."
    else
      # No need to retrieve credentials, image is public
      @logger.info "#{meth} Checking image existence..."
      @dockerRegistryProxy.checkImage imageData.hub
      , imageData.name
      , imageData.ref
      .then (exists) =>
        if exists
          @logger.info "#{meth} - Image validated!"
          return true
        else
          @logger.info "#{meth} - Image could not be found."
          q.reject new Error "Image could not be validated."



  # Validates that all the resource used in the solution exist, user has
  # permissions and are available.
  # NOTE: for exclusive use resources, it also checks if the resource is used
  #       multiple times inside the solution.
  validateSolutionResources: (solutionManifest, context) ->
    meth = 'Admission.validateSolutionResources()'
    @logger.info "#{meth} MANIFEST: #{JSON.stringify solutionManifest.ref}"

    # Get list of exclusive resource types
    exclusiveResourceTypes = @manifestHelper.getExclusiveResourceKinds()

    # Calculate a list of referenced resource URNs by deployment
    solResourcesByDeployment =
      @getSolutionResourceList solutionManifest, context

    promiseQueue = q()
    errors = []

    usedResources = {}

    for deplName, deplResourceList of solResourcesByDeployment
      for resURN in deplResourceList
        do (deplName, resURN) =>

          resID   = null
          resType = null

          # Basic resource type check; only accept known resource types
          resRef = @manifestHelper.urnToRef resURN
          if not @manifestHelper.isResourceKind resRef.kind
            errMsg = "Resource #{resURN} has an invalid type: #{resRef.kind}."
            @logger.error "#{meth} ERROR: #{errMsg}"
            errors.push errMsg
            return


          promiseQueue = promiseQueue.then () =>
            # First, check access to the resource.
            @checkResourceAccess resURN, context
            .then (checkResult) =>
              if checkResult.error?
                # Resource doesn't exists and user can't access it
                errors.push checkResult.error
              else
                # Resource exists and user can access it
                resID   = checkResult.resID
                resType = checkResult.resType

                # Check If resource is of exclusive use and is used more than
                # once in the solution
                if (resID of usedResources) \
                and (resType in exclusiveResourceTypes)
                  errMsg = "Resource #{resURN} is used more than once."
                  @logger.error "#{meth} ERROR: #{errMsg}"
                  errors.push errMsg
                else
                  # Add resource to resource list for checking double use in the
                  # solution.
                  # Add resource to the final resource list
                  usedResources[resID] =
                    urn: resURN
                    type: resType
            .catch (err) =>
              errMsg = "Unexpected error while validating resource #{resURN}:
                        #{err.message}"
              @logger.error "#{meth} ERROR: #{errMsg}"
              errors.push errMsg
              true

          promiseQueue = promiseQueue.then () =>
            # Next, if exclusive resources are currently in use.
            if resType in exclusiveResourceTypes
              @logger.info "#{meth} Validate availability of URN: #{resURN}"

              @planner.isResourceInUseBySolution resID
              .then (inUseInfo) =>
                if inUseInfo.inUse
                  if inUseInfo.usedByURN is @manifestHelper.refToUrn solutionManifest.ref
                    @logger.info "#{meth} Resource in use by the same solution."
                  else
                    errMsg = "Resource #{resURN} is already in use."
                    @logger.error "#{meth} ERROR: #{errMsg}"
                    errors.push errMsg
                true

    promiseQueue.then () =>
      if errors.length is 0
        @logger.info "#{meth} All resources OK."
        # Add usedResources property to manifest
        solutionManifest.usedResources = usedResources
        return solutionManifest
      else
        errMsg = "Resources validation found some problems:
                  #{JSON.stringify errors}"
        @logger.error "#{meth} #{errMsg}"
        q.reject new Error errMsg


  # Validates several aspects of the solution deployments
  # - names are unique (no other deployments exist with the same domain/name)
  # - docker images are accessible
  validateSolutionDeployments: (solutionManifest, context) ->
    meth = "Admission.validateSolutionDeployments() - Solution:
            #{JSON.stringify solutionManifest.ref}"
    @logger.info meth

    promiseChain = q()
    nameErrors  = []
    imageErrors = []

    # Loop over the solution deployments and:
    # - check their names
    # - check the docker images they use
    for deplName, deplData of ( solutionManifest.deployments || {} )
      do (deplName, deplData) =>
        promiseChain = promiseChain.then () =>
          # Compose a Ref for the deployment
          deplRef =
            kind: 'v3deployment'
            domain: solutionManifest.ref.domain
            name: deplName
          # Check if the deployment exists and user has permissions on it
          @logger.debug "#{meth} - Checking deployment name: #{deplName}"
          @checkDeploymentNameAvailability deplRef, context
          .then () =>
            @logger.debug "#{meth} - Deployment name OK: #{deplName}"
            return true
          .catch (err) =>
            errMsg = "Deployment name #{deplName} is already in use."
            @logger.warn "#{meth} - #{errMsg} - ERROR: ERROR: #{err.message}"
            nameErrors.push errMsg
            return true

        deplRoles = deplData.artifact?.description?.role || {}

        for roleName, roleData of deplRoles
          roleContainers = roleData.artifact?.description?.code || {}
          roleResources = roleData.artifact?.description?.config?.resource || {}
          for contName, contData of roleContainers
            imageData = contData.image
            do (roleName, contName, imageData, roleResources) =>
              @logger.info "#{meth} Check #{deplName}/#{roleName}/#{contName}"
              @logger.info "#{meth} Docker image: #{JSON.stringify imageData}"

              try
                dockerImage = @dockerRegistryProxy.normalizeDockerImage imageData
                @logger.info "#{meth} Normalized: #{JSON.stringify dockerImage}"
                promiseChain = promiseChain.then () =>
                  @validateComponentImage dockerImage, roleResources, context
                  .then () =>
                    @logger.info "#{meth} Validation OK for
                                  #{deplName}/#{roleName}/#{contName}"
                    true
                  .catch (err) =>
                    errMsg = "Failed docker image validation for deployment/role
                              '#{deplName}/#{roleName}' container '#{contName}'
                              image '#{dockerImage.hub}/#{dockerImage.name}:#{dockerImage.ref}'
                              ERROR: #{err.message}"
                    imageErrors.push errMsg
                    true

              catch normErr
                errMsg = "Failed docker image validation for role '#{roleName}'
                          container '#{contName}' tag '#{imageData.tag}'
                          ERROR: #{normErr.message}"
                imageErrors.push errMsg


    promiseChain.then () =>
      @logger.info "#{meth} All names and docker image validations finished."
      if (nameErrors.length > 0) or (imageErrors.length > 0)
        errMsg = "The following errors were found:"
        if nameErrors.length > 0
          errMsg += "#{JSON.stringify nameErrors}"
        if imageErrors.length > 0
          errMsg += "#{JSON.stringify imageErrors}"
        @logger.error "#{meth} #{errMsg}"
        q.reject new Error errMsg
      else
        @logger.info "#{meth} All deployment names and images validated OK!"
        return solutionManifest



  # Validates that the links in the solution are correct.
  # - deployment names and channels exist in the solution
  # - channel types match
  #   - s_c must be a client or duplex
  #   - t_c must be a server or duplex
  #   - both s_c and t_c can't be duplex
  validateSolutionLinks: (solutionManifest, context) ->
    meth = "Admission.validateSolutionLinks() - Solution:
            #{JSON.stringify solutionManifest.ref}"
    @logger.info meth

    linkErrors = []

    # Loop over the solution links and:
    # - check the deployment names exist in the solution
    # - check the channel names exist in the deployment
    # - check the channel names are of the correct type
    linkCounter = 0
    for linkData in ( solutionManifest.links || [] )

      # Validate link deployments exist in the solution
      if linkData['s_d'] not of solutionManifest.deployments
        errMsg = "Link #{linkCounter} source deployment not found in solution:
                  #{linkData['s_d']}"
        linkErrors.push errMsg

      if linkData['t_d'] not of solutionManifest.deployments
        errMsg = "Link #{linkCounter} target deployment not found in solution:
                  #{linkData['t_d']}"
        linkErrors.push errMsg


      # If both deployments exist, validate channel types
      if linkErrors.length is 0

        depl1Data = solutionManifest.deployments[linkData['s_d']]
        depl1ClientChannels = depl1Data.artifact?.description?.srv?.client || {}
        depl1DuplexChannels = depl1Data.artifact?.description?.srv?.duplex || {}

        # Source channel must be client or duplex
        if (linkData['s_c'] not of depl1ClientChannels) \
        and (linkData['s_c'] not of depl1DuplexChannels)
          errMsg = "Link #{linkCounter} source channel not found in deployment
                    #{linkData['s_d']} client or duplex channels:
                    #{linkData['s_c']}"
          linkErrors.push errMsg


        depl2Data = solutionManifest.deployments[linkData['t_d']]
        depl2ServerChannels = depl2Data.artifact?.description?.srv?.server || {}
        depl2DuplexChannels = depl2Data.artifact?.description?.srv?.duplex || {}

        # Target channel must be server or duplex
        if (linkData['t_c'] not of depl2ServerChannels) \
        and (linkData['s_c'] not of depl2DuplexChannels)
          errMsg = "Link #{linkCounter} target channel not found in deployment
                    #{linkData['t_d']} server or duplex channels:
                    #{linkData['t_c']}"
          linkErrors.push errMsg

        # Both channels can't be duplex
        if (linkData['s_c'] of depl1DuplexChannels) \
        and (linkData['t_c'] of depl2DuplexChannels)
          errMsg = "Link #{linkCounter} has two duplex channels."
          linkErrors.push errMsg

      linkCounter++


    if linkErrors.length is 0
      @logger.info "#{meth} All solution links validated OK."
      return true
    else
      errMsg = "The following errors were found in the solution links section:
                #{JSON.stringify linkErrors}"
      throw new Error errMsg


  validateV3DeploymentResources: (deploymentManifest, context) ->
    meth = 'Admission.validateV3DeploymentResources()'
    @logger.info "#{meth} MANIFEST: #{JSON.stringify deploymentManifest}"

    # Get list of exclusive resource types
    exclusiveResourceTypes = @manifestHelper.getExclusiveResourceKinds()

    resourceList = @getDeploymentResourceList deploymentManifest, context

    promiseQueue = q()
    errors = []

    deploymentManifest.usedResources = {}

    for resURN in resourceList
      do (resURN) =>
        @logger.info "#{meth} Checking resource: #{resURN}"

        resID   = null
        resType = null

        promiseQueue = promiseQueue.then () =>
          @logger.info "#{meth} Getting manifest for #{resURN}"
          @manifestRepository.getManifest resURN
          .then (manif) =>
            @logger.info "#{meth} Got manifest for #{resURN}"
            # Keep resource internal name for later
            resID = manif.name
            resType = manif.ref.kind
            @logger.info "#{meth} Resource ID: #{resID} - TYPE: #{resType}"

            if not @authorization.isAllowedForManifest(manif, context)
              errMsg = "Resource #{resURN} not found for user."
              @logger.error "#{meth} ERROR: #{errMsg}"
              errors.push errMsg
            else
              deploymentManifest.usedResources[resID] =
                urn: resURN
                type: resType
            true
          .catch (err) =>
            errMsg = "Resource #{resURN} can't be retrieved. Reason:
                      #{err.message}"
            @logger.error "#{meth} ERROR: #{errMsg}"
            errors.push errMsg
            true

        promiseQueue = promiseQueue.then () =>
          if resType in exclusiveResourceTypes
            @logger.info "#{meth} Validate availability of URN: #{resURN}"

            @planner.isResourceInUse resID
            .then (inUseInfo) =>
              if inUseInfo.inUse
                if inUseInfo.usedByURN is @manifestHelper.refToUrn deploymentManifest.ref
                  @logger.info "#{meth} Resource in use by the same deployment."
                else
                  errMsg = "Resource #{resURN} is already in use."
                  @logger.error "#{meth} ERROR: #{errMsg}"
                  errors.push errMsg
              true

    promiseQueue.then () =>
      if errors.length is 0
        @logger.info "#{meth} All resources OK."
        return deploymentManifest
      else
        errMsg = "Resources validation found some problems:
                  #{JSON.stringify errors}"
        @logger.error "#{meth} #{errMsg}"
        q.reject new Error errMsg



  # Checks if a deployment exists with the provided Ref and if the user (part
  # of the context info) is allowed to modify it.
  # Returns a promise:
  # - resolved (with 'true') if the deployment exists and user is allowed
  # - rejected if the deployments does not exist or user is not allowed
  checkDeploymentNameAvailability: (deploymentRef, context) ->
    meth = "Admission.checkDeploymentNameAvailability() - Deployment Ref:
            #{JSON.stringify deploymentRef} - User: #{context.user?.id}"
    @logger.info meth

    @logger.debug "#{meth} - Checking existence of deployment..."

    # Check if deployment already exists
    @getDeploymentFromRef deploymentRef, context
    .then (previousManifest) =>
      if not previousManifest?
        @logger.debug "#{meth} - Deployment does not exist."
      else
        # Deployment already exists, check its ownership
        @logger.debug "#{meth} - Deployment #{JSON.stringify deploymentRef}
                       already exists"
        if not @authorization.isAllowedForManifest(previousManifest, context, true)
          errMsg = "User is not allowed to modify deployment."
          @logger.warn "#{meth} - #{errMsg}"
          throw new Error errMsg
        else
          @logger.debug "#{meth} - User is allowed to modify deployment."

      return true
    .catch (err) =>
      @logger.warn "#{meth} - #{err.message}"
      q.reject err


  # Checks if a resource exists with the provided URN and if the user (part
  # of the context info) is allowed to use it.
  # Returns a promise, always resolved with a 'result' structure with the
  # following properties:
  # - resID: the resource internal ID
  # - resType: the resource type
  # - error: if resource does not exist or user is not allowed
  checkResourceAccess: (resURN, context) ->
    meth = "Admission.checkResourceAccess() - Resource: #{resURN} -
            User: #{context.user?.id}"
    @logger.info meth

    resID = null
    resType = null

    @logger.info "#{meth} - Getting resource manifest..."
    @manifestRepository.getManifest resURN
    .then (manif) =>
      resID = manif.name
      resType = manif.ref.kind
      @logger.info "#{meth} Got resource manifest. Resource ID: #{resID} -
                    TYPE: #{resType}"
      @logger.info "#{meth} Checking access..."

      if not @authorization.isAllowedForManifest(manif, context)
        errMsg = "Resource #{resURN} not found for user."
        @logger.error "#{meth} ERROR: #{errMsg}"
        result =
          error: errMsg
          resID: resID
          resType: resType
      else
        @logger.info "#{meth} User is allowed to access resource"
        result =
          error: null
          resID: resID
          resType: resType
      return result
    .catch (err) =>
      errMsg = "Resource #{resURN} can't be retrieved. Reason:
                #{err.message}"
      @logger.error "#{meth} ERROR: #{errMsg}"
      result =
        error: errMsg
        resID: resID
        resType: resType
      return result


  getDeploymentResourceList: (deploymentManifest) ->
    meth = 'Admission.getDeploymentResourceList()'
    @logger.info "#{meth}"

    resourceList = []
    deplResources = {}

    # Extract a list of resources used in this deployment
    deplResources = deploymentManifest.description?.config?.resource || {}
    @logger.info "#{meth} config.resource: #{JSON.stringify deplResources}"
    resourceList = @extractResources deplResources, context

    @logger.info "#{meth} ResourceList: #{JSON.stringify resourceList}"

    return resourceList


  # Get a list of URNs of the resources used in the solution.
  #
  # For each deployment, resources are only extracted from "leaf" elements:
  # - the deployment 'config.resource' section is ignored
  # - if the service is builtin:
  #   - the 'config.resource' section of the service is processed
  # - if the service is not builtin:
  #   - if the service has roles:
  #     - the service 'config.resource' section is ignored
  #     - the 'config.resource' section of each of the roles is processed
  #   - if the service has no roles:
  #     - the 'config.resource' section of the service is ignored
  getSolutionResourceList: (solutionManifest, context) ->
    meth = 'Admission.getSolutionResourceList()'

    solResourcesByDeployment = {}

    for deplName, deplData of solutionManifest.deployments

      deplResourceList = []

      # Get deployment artifact descritpion (service-level)
      deplArtDesc = deplData.artifact?.description || {}

      # If service is a builtin, use the service config section
      if deplArtDesc.builtin? and deplArtDesc.builtin
        serviceResources = deplArtDesc.config?.resource || {}
        # Extract role resources and add them to the deployment list
        @extractResources serviceResources, context, deplResourceList
      else
        # If service has roles, use their config sections to extract resources
        if deplArtDesc.role? and (Object.keys(deplArtDesc.role).length isnt 0)
          # Service has roles, use their config sections to extract resources
          for roleName, roleData of deplArtDesc.role
            roleResources = roleData.config?.resource || {}
            # Extract role resources and add them to the deployment list
            @extractResources roleResources, context, deplResourceList

      @logger.info "#{meth} - Extracted resources of deployment #{deplName}:
                    #{JSON.stringify deplResourceList}"

      # Add extracted deployment resources to the solution resources list
      solResourcesByDeployment[deplName] = deplResourceList

    return solResourcesByDeployment


  # Resource validation:
  # - port resources must use a valid (exists in configured ports list) and
  #   not-used port number
  # - domain resources use a valid and not-used domain name
  # - secret resources don't need to be validated
  # - Volume resources don't need to be validated
  # - CA resources don't need to be validated
  validateResource: (context, manifest) ->
    meth = 'Admission.validateResource'
    @logger.info "#{meth} manifest.name: #{manifest?.name}"
    q.promise (resolve, reject) =>
      try
        # port validation
        if @manifestHelper.isPort manifest
          port = manifest.description.port
          owner = manifest.owner || context.user?.id
          uniqueID = owner + '/' + manifest.ref.domain + '/' + manifest.ref.name

          # Port number must be on of the allowed ports. If it is, also store
          # the corresponding internal port in the Port manifest
          if port not of @config.portDict
            errMsg = "Validating port resource: #{port} is not allowed"
            @logger.info "#{meth} - #{errMsg}"
            reject new Error errMsg
          else
            manifest.description.internalPort = @config.portDict[port]

          # Port number is not used in any port resource
          resUsingPort = ""
          @manifestRepository.listElementType 'ports'
          .then (manifests) =>
            for k, m of manifests
              uniqueID2 = m.owner + '/' + m.ref.domain + '/' + m.ref.name
              # Note that the port is stored as externalPort
              if (uniqueID2 isnt uniqueID) \
              and (m.description.externalPort is port)
                resUsingPort = k
                break
            if resUsingPort isnt ""
              errMsg = "Validating port resource: #{port} is in use"
              @logger.info "#{meth} - #{errMsg} by #{resUsingPort}"
              reject new Error errMsg
            # Port number is OK
            resolve manifest
          .catch (err) =>
            @logger.error "#{meth} - #{errMsg}"
            reject err

        # secret resource validation
        else if @manifestHelper.isSecret manifest
          resolve manifest

        # domain resource validation
        else if @manifestHelper.isDomain manifest
          # Convert domain name to lowercase
          manifest.description.domain =
            manifest.description.domain.toLowerCase()

          # Domain name is valid
          domain = manifest.description.domain
          if not (utils.validateDomainName domain)
            errMsg = "Validating domain resource: domain #{domain} is invalid"
            @logger.info "#{meth} - #{errMsg}"
            reject new Error errMsg

          # Domain is not used in any other domain resource
          owner = manifest.owner || context.user?.id
          uniqueID = owner + '/' + manifest.ref.domain + '/' + manifest.ref.name
          resUsingDomain = ""
          @manifestRepository.listElementType 'domains'
          .then (manifests) =>
            for k, m of manifests
              uniqueID2 = m.owner + '/' + m.ref.domain + '/' + m.ref.name
              if (uniqueID2 isnt uniqueID) \
              and (m.description.domain.toLowerCase() is domain)
                resUsingDomain = k
                break
            if resUsingDomain isnt ""
              errMsg = "Validating domain resource: #{domain} in use by
                        #{resUsingDomain}"
              @logger.info "#{meth} - #{errMsg}"
              reject new Error errMsg
            # Domain is OK
            resolve manifest
          .catch (err) =>
            @logger.error "#{meth} - #{errMsg}"
            reject err

        # volume resource validation
        else if @manifestHelper.isVolume manifest
          # If persistent storage is disabled, do not allow KukuVolumes
          if @config.disablePersistentStorage? \
          and @config.disablePersistentStorage is true
            errMsg = "Validating volume resource: persistence is disabled."
            @logger.warn "#{meth} - #{errMsg}"
            reject new Error errMsg
          # Validate volume type exists and is OK for persistent volumes
          else
            validationError = @validateVolumeType manifest
            if validationError?
              errMsg = "Validating volume resource: #{validationError}."
              @logger.warn "#{meth} - #{errMsg}"
              reject new Error errMsg
            else
              resolve manifest

        # CA resource validation
        else if @manifestHelper.isCA manifest
          resolve manifest

        # Certificate resource validation
        else if @manifestHelper.isCertificate manifest
          resolve manifest

        else
          resolve manifest
      catch err
        @logger.error "#{meth} - #{errMsg}"
        reject err


  validateVolumeType: (volumeManifest) ->
    meth = 'Admission.validateVolumeType()'

    if not volumeManifest.description?
      return "missing description in volume manifest"
    if not volumeManifest.description.type?
      return "missing 'type' property in volume manifest"
    # if not volumeManifest.description.items?
    #   return "missing 'items' property in volume manifest"

    volumeType = volumeManifest.description.type

    if (not @config.clusterConfiguration.volumeTypes?) \
    or (Object.keys @config.clusterConfiguration.volumeTypes).length is 0
      @logger.warn "#{meth} No volume type configuration available in cluster.
                    Skipping volume type validation."
      return null

    if volumeType not of @config.clusterConfiguration.volumeTypes
      return "volume type '#{volumeType}' not supported in cluster"

    typeProperties =
      @config.clusterConfiguration.volumeTypes[volumeType].properties || ""
    if typeProperties is ""
      return "volume type '#{volumeType}' is not valid for persistent volumes
              (no properties)"

    typePropertiesArray = typeProperties.split ','
    if 'persistent' not in typePropertiesArray
      return "volume type '#{volumeType}' is not valid for persistent volumes
              (no 'persistent' property)"

    return null


  canBeDeleted: (urn) ->
    @logger.info 'Admission.canBeDeleted()'
    @logger.debug 'Checking if URN is in use: ', urn

    # Ask planner if URN is currently in use
    @isElementInUse urn
    .then (inUse) ->
      q not inUse


  isElementInUse: (urn) ->
    @planner.isElementInUse urn
    .then (info) ->
      q info.inUse
    .fail (error) =>
      @logger.error "Error checking if element in use: #{error.message}"
      @logger.error 'ERROR: ', error.stack
      q.reject error


  isResourceInUse: (resourceName) ->
    @planner.isResourceInUse resourceName
    .then (info) ->
      q info.inUse
    .fail (error) =>
      @logger.error "Error checking if resource in use: #{error.message}"
      @logger.error 'ERROR: ', error.stack
      q.reject error


  loadManifest: (manifestOptions) ->
    @logger.info 'Admission.loadManifest()'
    # if manifestOptions.url?
    #   u.jsonFromUrl manifestOptions.url
    # else if manifestOptions.filename?
    if manifestOptions.filename?
      utils.jsonFromFile manifestOptions.filename
    else if manifestOptions.inline?
      q(manifestOptions.inline)
    else
      q.reject new Error 'Deployment manifest parameter is missing.'


  # VERSION WITH FULL DATE (was too long for some Kubernetes IDs)
  getDateSuffixLong: () ->
    d = new Date()
    s = d.getFullYear()
    s += ('0' + (d.getMonth() + 1)).slice(-2)
    s += ('0' + d.getDate()).slice(-2)
    s += '_' + ('0' + d.getHours()).slice(-2)
    s += ('0' + d.getMinutes()).slice(-2)
    s += ('0' + d.getSeconds()).slice(-2)


  # VERSION WITH FULL DATE (was too long for some Kubernetes IDs)
  getDateSuffix: () ->
    d = new Date()
    s = ('0' + d.getHours()).slice(-2)
    s += ('0' + d.getMinutes()).slice(-2)
    s += ('0' + d.getSeconds()).slice(-2)


  getSolutionID: () ->
    return "ks-#{@getDateSuffix()}-#{utils.randomHex(8)}"


  getDeploymentNameKmv3: () ->
    return "kd-#{@getDateSuffix()}-#{utils.randomHex(8)}"


  generateResourceName: () ->
    return "kres-#{@getDateSuffix()}-#{utils.randomHex(8)}"


  # Look for Resources objects in an object at any depth
  extractResources: (obj, context, resourceList = []) ->

    validKinds = @manifestHelper.getValidResourceKinds()

    if not obj?
      return resourceList

    if 'object' isnt typeof obj
      return resourceList

    keys = Object.keys obj
    if (keys.length is 1) and (keys[0] in validKinds)
      # It's a resource object, only keep resource references (strings) not
      # inline resource definitions (objects)
      if ('string' is typeof obj[keys[0]]) and (obj[keys[0]] isnt '')
        # First and only key (0) is the resource type
        # The object of that key is the resource reference

        # Normalize reference (add a domain if it doesn't include one)
        normResRef = @normalizeResourceReference obj[keys[0]], context

        resURN = 'eslap://' + normResRef.replace '/', "/#{keys[0]}/"
        if resURN not in resourceList
          resourceList.push resURN
      else
        for k, v of obj
          @extractResources v, context, resourceList
    else
      for k, v of obj
        @extractResources v, context, resourceList

    return resourceList


  # Add a domain to a resource reference if it doesn't have one.
  # Currently, the default domain is the username of who performs the operation
  normalizeResourceReference: (resourceReference, context) ->

    if resourceReference.indexOf('/') > 0
      newResourceReference = resourceReference
    else
      newResourceReference = "#{context.user.id}/#{resourceReference}"

    return newResourceReference



  # Generate a deterministic link name, based on the link data
  #
  # The link name is generated by:
  # - creating a string representation of each endpoint (all endpoint elements
  #   concatenated separated by a '/')
  # - ordering the endpoint strings in Lexicographic order and concatenating
  #   them into a new string ('name')
  # - calculating a hash of 'name'
  # - KukuLink name will be 'kl-<calculated_hash>''

  # Sample link manifest endpoints section:
  #   "endpoints": [
  #     {
  #       "name": "helloworlddep",
  #       "kind": "solution",
  #       "domain": "my.examples",
  #       "channel": "hello",
  #       "kumoriName": "kd-065441-53ec4c1b"
  #     },
  #     {
  #       "name": "helloworldinb",
  #       "kind": "solution",
  #       "domain": "my.examples",
  #       "channel": "frontend",
  #       "kumoriName": "kh-083835-557149b5"
  #     }
  #   ]
  generateLinkName: (linkManif) ->

    ep0channel = linkManif.endpoints[0].channel

    ep0Str = linkManif.endpoints[0].domain +
             '/' + linkManif.endpoints[0].kind +
             '/' + linkManif.endpoints[0].name +
             '/' + ep0channel

    ep1channel = linkManif.endpoints[1].channel

    ep1Str = linkManif.endpoints[1].domain +
             '/' + linkManif.endpoints[1].kind +
             '/' + linkManif.endpoints[1].name +
             '/' + ep1channel

    if ep0Str <= ep1Str
      name = ep0Str + '-' + ep1Str
    else
      name = ep1Str + '-' + ep0Str

    hash = utils.getHash name
    name = "kl-#{hash}"

    return name



module.exports = Admission
