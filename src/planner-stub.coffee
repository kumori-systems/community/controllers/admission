###
* Copyright 2022 Kumori Systems S.L.

* Licensed under the EUPL, Version 1.2 or – as soon they
  will be approved by the European Commission - subsequent
  versions of the EUPL (the "Licence");

* You may not use this work except in compliance with the
  Licence.

* You may obtain a copy of the Licence at:

  https://joinup.ec.europa.eu/software/page/eupl

* Unless required by applicable law or agreed to in
  writing, software distributed under the Licence is
  distributed on an "AS IS" basis,

* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
  express or implied.

* See the Licence for the specific language governing
  permissions and limitations under the Licence.
###

q                 = require 'q'
EventEmitter      = require('events').EventEmitter
ManifestHelper    = require './manifest-helper'
EventStoreFactory = require './event-stores/event-store-factory'

utils             = require './utils'

class MockRequester
  constructor: () ->
  do: () -> return q()


KUMORI_NAMESPACE = 'kumori'


class PlannerStub extends EventEmitter

  constructor: (@k8sApiStub, @manifestRepository, eventStoreConfig = null)->
    super()
    meth = 'PlannerStub.constructor'
    @logger.info "#{meth}"
    @requester = new MockRequester()
    @manifestHelper = new ManifestHelper()

    @eventStore = null
    if eventStoreConfig?.type? and (eventStoreConfig.type isnt '')
      # Initialize an Events Store from configuration
      @logger.info "#{meth} Initializing EventStore of type:
                    #{eventStoreConfig.type}"

      # If store is of type 'k8s' pass it the K8sApiStub object
      if eventStoreConfig.type is 'k8s'
        eventStoreConfig.config.k8sApiStub = @k8sApiStub

      @eventStore = EventStoreFactory.create eventStoreConfig.type
        , eventStoreConfig.config
    else
      # If no configuration is supplied, EventStore is Kubernetes itself
      @logger.info "#{meth} Initializing default EventStore (K8s)."
      esConfig =
        k8sApiStub: @k8sApiStub
      @eventStore = EventStoreFactory.create 'k8s', esConfig



  init: ->
    @eventStore.init()
    .then () =>
      true


  terminate: ->
    @eventStore.terminate()
    .then () =>
      true



  ##############################################################################
  ##                   PUBLIC METHODS TO BE USED FROM OUTSIDE                 ##
  ##############################################################################
  execSolution: (solutionManifest) ->
    @logger.info 'PlannerStub.execSolution()'
    @manifestRepository.storeManifest solutionManifest
    .then (solutionURN) ->
      { solutionURN: solutionURN }


  deleteSolution: (name, kind) ->
    @logger.info "PlannerStub.deleteSolution() Name: #{name} Kind: #{kind}"
    @manifestRepository.deleteManifestByNameAndKind name, kind



  execDeployment: (fullDeployment) ->
    @logger.info 'PlannerStub.execDeployment()'
    @manifestRepository.storeManifest fullDeployment
    .then (deploymentURN) ->
      { deploymentURN: deploymentURN }


  execUndeployment: (name, kind) ->
    @logger.info "PlannerStub.execUndeployment() Name: #{name} Kind: #{kind}"
    @manifestRepository.deleteManifestByNameAndKind name, kind


  linkServices: (linkManifest)->
    @logger.info 'PlannerStub.linkServices()'
    # @requester.do
    #   url: "#{@url}/linkServices"
    #   message: 'linkManifest':linkManifest
    @manifestRepository.storeManifest linkManifest


  unlinkServices: (name)->
    @logger.info "PlannerStub.unlinkServices() Name: #{name}"
    @manifestRepository.deleteManifestByNameAndKind name, 'link'


  isElementInUse: (urn)->
    meth = "PlannerStub.isElementInUse() - urn: #{urn}"
    @logger.info meth

    elementRef = @manifestHelper.urnToRef urn
    elementK8sId = elementRef.name

    # Filter on the existence of a label named as the element
    filter = { "#{elementK8sId}": '*' }

    used = false
    @manifestRepository.listElementType 'v3deployments', null, filter
    .then (elements) =>
      if Object.keys(elements).length > 0
        @logger.info "#{meth} - Found v3deployments using the element."
        used = true
      used
    .then (used) =>
      if not used
        @manifestRepository.listElementType 'solutions', null, filter
        .then (elements) =>
          if Object.keys(elements).length > 0
            @logger.info "#{meth} - Found Solutions using the element."
            used = true
            usedByURN = Object.values(elements)[0].urn || 'unknown'
          used
      else
        used
    .then (used) =>
      return { inUse: used }
    .catch (err) =>
      errMsg = "Unable to determine if element is in use: #{err.message}
                #{err.stack}"
      @logger.error "#{meth} - ERROR: #{errMsg}"
      q.reject new Error errMsg


  isResourceInUse: (resourceName)->
    meth = "PlannerStub.isResourceInUse() - Resource name: #{resourceName}"
    @logger.info meth

    # Filter on the existence of a label named as the element
    filter = { "#{resourceName}": '*' }

    used = false
    usedByURN = null
    @manifestRepository.listElementType 'v3deployments', null, filter
    .then (elements) =>
      if Object.keys(elements).length > 0
        @logger.info "#{meth} - Found v3deployments using the element."
        used = true
        usedByURN = Object.values(elements)[0].urn || 'unknown'
      used
    .then (used) =>
      if not used
        @manifestRepository.listElementType 'solutions', null, filter
        .then (elements) =>
          if Object.keys(elements).length > 0
            @logger.info "#{meth} - Found Solutions using the element."
            used = true
            usedByURN = Object.values(elements)[0].urn || 'unknown'
          used
      else
        used
    .then (used) =>
      return {
        inUse: used
        usedByURN: usedByURN
      }
    .catch (err) =>
      errMsg = "Unable to determine if resource is in use: #{err.message}
                #{err.stack}"
      @logger.error "#{meth} - ERROR: #{errMsg}"
      q.reject new Error errMsg


  isResourceInUseBySolution: (resourceID)->
    meth = "PlannerStub.isResourceInUseBySolution() - Name: #{resourceID}"
    @logger.info meth

    # Filter on the existence of a label named as the element
    filter = { "#{resourceID}": '*' }

    used = false
    usedByURN = null
    @manifestRepository.listElementType 'solutions', null, filter
    .then (elements) =>
      if Object.keys(elements).length > 0
        @logger.info "#{meth} - Found solutions using the element."
        used = true
        usedByURN = Object.values(elements)[0].urn || 'unknown'
      used
    .then (used) =>
      return {
        inUse: used
        usedByURN: usedByURN
      }
    .catch (err) =>
      errMsg = "Unable to determine if resource is in use: #{err.message}
                #{err.stack}"
      @logger.error "#{meth} - ERROR: #{errMsg}"
      q.reject new Error errMsg


  listResources: (filter, includePublic = false)->
    meth = 'PlannerStub.listResources'
    if includePublic
      @logger.info "#{meth} FILTER: #{JSON.stringify filter} (include public)"
    else
      @logger.info "#{meth} FILTER: #{JSON.stringify filter}"

    owner = filter?.owner || null


    validResourceKinds = @manifestHelper.getValidResourceKinds()

    allResources = {}

    @manifestRepository.listElementTypes validResourceKinds, owner, null
    .then (resourcesDict) =>
      allResources = resourcesDict
      includePublic = true   # TODO: for testing
      if includePublic
        publicFilter = { 'kumori/public': "true" }
        @manifestRepository.listElementTypes validResourceKinds, null, publicFilter
        .then (publicResourcesDict) =>
          for k,v of publicResourcesDict
            # We don't care if there are duplicates since they would be the same
            allResources[k] = v
          true
    .then () =>
      return allResources
    .catch (err) =>
      errMsg = "Unable to list resources: #{err.message}"
      @logger.error "#{meth} - ERROR: #{errMsg} - #{err.stack}"
      q.reject new Error errMsg


  listResourcesInUse: (filter, context)->
    meth = 'PlannerStub.listResourcesInUse'
    @logger.info "#{meth} FILTER: #{JSON.stringify filter}"

    # If the URN provided is a Solution, modify the filter
    if filter.urn? and (@manifestHelper.isSolutionURN filter.urn)
      filter.solutionURN = filter.urn
      delete filter.urn

    counter = 0
    resInUse = {}
    filter.show = 'manifest'
    @deploymentQuery filter
    .then (deployments) =>
      @logger.info "#{meth} - Got #{Object.keys(deployments).length} deployments."
      if Object.keys(deployments).length > 0
        for deplURN, deplData of deployments

          deplResources = deplData.config?.resource || {}
          resourceList = @extractResources deplResources, context

          for resURN in resourceList
            if resURN not of resInUse
              resRef = @manifestHelper.urnToRef resURN
              resDetails =
                type: resRef.kind
                deployment: deplURN
                name: resURN
              resInUse[resURN] = resDetails
              counter++
      @logger.info "#{meth} - Found #{counter} resources in use."
      return resInUse
    .catch (err) =>
      errMsg = "Unable to perform DeploymentQuery: #{err.message} #{err.stack}"
      @logger.error "#{meth} - ERROR: #{errMsg}"
      q.reject new Error errMsg


  describeResource: (resourceURN) ->
    meth = "PlannerStub.describeResource(#{resourceURN})"
    @logger.info "#{meth}"

    resourceManifest = null
    result = null

    # Get the resource manifest to determine the resource type
    @manifestRepository.getManifest resourceURN
    .then (manifest) =>
      resourceManifest = manifest
      @logger.info "#{meth} - Got manifest for #{resourceURN}"

      elementType = @manifestHelper.getManifestType resourceManifest
        # Check it's a resource
      if elementType isnt ManifestHelper.RESOURCE
        errMsg = "Wrong element type (#{elementType})."
        @logger.error "#{meth} - #{errMsg}"
        throw new Error errMsg

      @logger.info "#{meth} - Element type checked OK - #{resourceURN}"

      # complete the generic part for all resource types
      result =
        name: resourceManifest.name
        urn: resourceManifest.urn
        description: {}
        inUseBy: []
        items: {}
        public: false
        creationTimestamp: resourceManifest.creationTimestamp
        lastModification: resourceManifest.lastModification


      # Determine if the element is public or not
      if resourceManifest.public?
        result.public = resourceManifest.public
      else
        result.public = false

      # Look for deployments that used this resource
      @logger.info "#{meth} - Looking for v3deployments using the resource."

      # Filter on the existence of a label named as the element
      filter = { "#{resourceManifest.name}": '*' }

      @manifestRepository.listElementType 'solutions', null, filter
      .then (elements) =>
        if Object.keys(elements).length > 0
          @logger.info "#{meth} - Found solutions using the element."
          for urn, manifest of elements
            result.inUseBy.push manifest.urn
        else
          @logger.info "#{meth} - No solutions using the element."
        return true
      .catch (err) =>
        errMsg = "Unable to determine if element is in use: #{err.message}
                  #{err.stack}"
        @logger.error "#{meth} - ERROR: #{errMsg}"
        return true
    .then () =>
      # Complete information for each specific resource type
      if @manifestHelper.isCA resourceManifest
        @describeResourceCA resourceManifest, result
      else if @manifestHelper.isCertificate resourceManifest
        @describeResourceCertificate resourceManifest, result
      else if @manifestHelper.isDomain resourceManifest
        @describeResourceDomain resourceManifest, result
      else if @manifestHelper.isPort resourceManifest
        @describeResourcePort resourceManifest, result
      else if @manifestHelper.isSecret resourceManifest
        @describeResourceSecret resourceManifest, result
      else if @manifestHelper.isVolume resourceManifest
        @describeResourceVolume resourceManifest, result
      else
        errMsg = "Describe not supported for type
                  '#{resourceManifest.ref.kind}'."
        @logger.warn "#{meth} - #{errMsg}"
        throw new Error errMsg
    .then () =>
      return result


  describeResourceCA: (resourceManifest, result) ->
    meth = "PlannerStub.describeResourceCA(#{resourceManifest.urn})"
    @logger.info "#{meth}"

    result.description = resourceManifest.description


  describeResourceCertificate: (resourceManifest, result) ->
    meth = "PlannerStub.describeResourceCertificate(#{resourceManifest.urn})"
    @logger.info "#{meth}"

    result.description = resourceManifest.description


  describeResourceDomain: (resourceManifest, result) ->
    meth = "PlannerStub.describeResourceDomain(#{resourceManifest.urn})"
    @logger.info "#{meth}"

    result.description = resourceManifest.description


  describeResourcePort: (resourceManifest, result) ->
    meth = "PlannerStub.describeResourcePort(#{resourceManifest.urn})"
    @logger.info "#{meth}"

    result.description =
      port: resourceManifest.description.externalPort


  describeResourceSecret: (resourceManifest, result) ->
    meth = "PlannerStub.describeResourceSecret(#{resourceManifest.urn})"
    @logger.info "#{meth}"

    # For secrets, there is currently no specific info to include
    result.description = {}


  describeResourceVolume: (resourceManifest, result) ->
    meth = "PlannerStub.describeResourceVolume(#{resourceManifest.urn})"
    @logger.info "#{meth}"

    KV_PLURAL = 'kukuvolumes'
    KVI_PLURAL = 'kukuvolumeitems'

    # Get KukuVolume Kumori internal ID
    kvID = resourceManifest.name
    @logger.info "#{meth} Resource ID: #{kvID}"

    kukuVolumeItemList = []
    kviMetrics = {}

    result.description =
      maxItems: resourceManifest.description.items
      size: resourceManifest.description.size
      type: resourceManifest.description.type

    # Get the KukuVolume Kubernetes object (in case we need something from its
    # status. (currently not necessary)
    @logger.info "#{meth} - Getting KukuVolume object..."
    @k8sApiStub.getKumoriElement KV_PLURAL, kvID
    .then (kvObject) =>
      if not kvObject?
        errMsg = "Unable to get KukuVolume (#{kvID})."
        @logger.error "#{meth} - #{errMsg}"
        return null

      @logger.info "#{meth} - Getting related KukuVolumeItem objects..."

      # Prepare label selector for finding Items
      labelSelector = "kumori/kukuvolume=#{kvID}"

      @k8sApiStub.listKumoriElements KVI_PLURAL, labelSelector
      .then (items) =>
        @logger.info "#{meth} - Got list of related KukuVolumeItems
                      (#{items.length} elements)."
        kukuVolumeItemList = items
        return true
      .catch (err) =>
        @logger.error "#{meth} - Unable to get KukuVolumeItems: #{err.msg}"
        return true
    .then () =>
      promiseChain = q()
      for kvi in kukuVolumeItemList
        do (kvi) =>
          kviName = kvi.metadata.name
          kviPvc = kvi.metadata.labels['kumori/pvc'] || ""
          if kviPvc? and kvi.status.phase is 'Bound'
            promiseChain = promiseChain.then () =>
              @logger.info "#{meth} - Getting metrics for KVI #{kviName}
                            (PVC: #{kviPvc})..."
              @k8sApiStub.getPvcMetrics KUMORI_NAMESPACE, kviPvc
              .then (pvcMetrics) =>
                @logger.info "#{meth} - Got metrics for KVI #{kviName}."
                kviMetrics[kviName] = pvcMetrics
              .catch (err) =>
                @logger.error "#{meth} - Unable to get KVI metrics: #{err.msg}"
                return true

      promiseChain.then () =>
        @logger.info "#{meth} - Got metrics for all relevant KVIs."
        return true
    .then () =>
      # All info gathered. Complete the result object
      for kvi in kukuVolumeItemList
        kviName = kvi.metadata.name
        # If KVI is bound deduce the Pod name from the PVC name (deterministic).
        #   PVC name = <internal_volume_name>-<pod_name>
        #   (where Pod name starts with kd-)
        inUseBy = null
        kviPvc = kvi.metadata.labels['kumori/pvc'] || ""
        if kviPvc? and kvi.status.phase is 'Bound'
          if kviPvc.includes '-kd-'
            start = 1 + kviPvc.indexOf '-kd-'
            inUseBy = kviPvc.substring start
          else
            inUseBy = 'unknown'

        kviItem =
          phase: kvi.status.phase
          creationTimestamp: kvi.metadata.creationTimestamp
          inUseBy: inUseBy
          metrics: kviMetrics[kviName] || null

        result.items[kviName] = kviItem

      # This method modifies the original 'result' parameter provided
      return true


  solutionQuery: (params)->

    VALID_SHOW_VALUES = [ 'urn', 'topology', 'extended', 'manifest' ]

    meth = 'PlannerStub.solutionQuery()'
    @logger.info "#{meth} - Params: #{JSON.stringify params}"

    filter = {}
    type = ""

    if params.urn?
      try
        ref = @manifestHelper.urnToRef params.urn
        @logger.info "#{meth} - URN Data = #{JSON.stringify ref}"
        filter['kumori/name'] = @manifestHelper.hashLabel ref.name
        filter['kumori/domain'] = @manifestHelper.hashLabel ref.domain
        type = ref.kind
      catch e
        errMsg = "Unable to perform SolutionQuery: Invalid URN #{params.urn}
                  (#{e.message})."
        @logger.error "#{meth} - ERROR: #{errMsg}"
        return q.reject new Error errMsg


    # Filter that determines how much information on each deployment will be
    # returned. Possible values are:
    # - urn: return no data
    # - topology: return basic data + instances information (DEFAULT)
    # - extended: return all data available
    if not params.show?
      @logger.info "#{meth} - Empty 'show' property. Defaulting to 'topology'."
      params.show = 'topology'
    else if params.show not in VALID_SHOW_VALUES
      @logger.info "#{meth} - Invalid 'show' value: #{params.show}. Defaulting
                    to 'topology'."
      params.show = 'topology'

    # Look for solutions
    @manifestRepository.listElementType 'solutions', params.owner, filter
    .then (elements) =>
      if params.show is 'manifest'
        q elements
      else if params.show is 'urn'
        elements = @removeSolutionData elements
        q elements
      else if params.show is 'extended'
        @completeSolutions elements, true
      else
        # Default is 'topology'
        @completeSolutions elements, false
        q elements
    .then (elements) =>
      @logger.info "#{meth} - Found #{Object.keys(elements).length} solutions"
      return elements
    .catch (err) =>
      errMsg = "Unable to perform SolutionQuery: #{err.message} #{err.stack}"
      @logger.error "#{meth} - ERROR: #{errMsg}"
      q.reject new Error errMsg


  getDeployment: (params) ->
    meth = "PlannerStub.getDeployment(#{JSON.stringify params})"
    @logger.info meth

    # Just for log traces
    filterStr = JSON.stringify params

    @deploymentQuery params
    .then (deploymentList) =>
      if Object.keys(deploymentList).length is 0
        @logger.debug "#{meth} - No deployment found for #{filterStr}"
        return null
      else if Object.keys(deploymentList).length > 1
        errMsg = "Several matches found for deployment #{filterStr}"
        @logger.warn "#{meth} - #{errMsg}: #{JSON.stringify deploymentList}"
        return q.reject new Error errMsg
      else
        deploymentURN = Object.keys(deploymentList)[0]
        @logger.info "#{meth} - Found deployment for #{filterStr} :
                      #{deploymentURN}"
        return deploymentList[deploymentURN]
    .catch (err) =>
      @logger.error "#{meth} - ERROR: #{err.message}"
      null


  deploymentQuery: (params)->

    VALID_SHOW_VALUES = [ 'urn', 'topology', 'extended', 'manifest' ]

    meth = 'PlannerStub.deploymentQuery()'
    @logger.info "#{meth} - Params: #{JSON.stringify params}"

    filter = {}
    type = ""

    if params.solutionURN?
      try
        ref = @manifestHelper.urnToRef params.solutionURN
        @logger.info "#{meth} - URN Data = #{JSON.stringify ref}"
        filter['kumori/solution.domain'] = @manifestHelper.hashLabel ref.domain
        filter['kumori/solution.name'] = @manifestHelper.hashLabel ref.name
        # For solutions, look for any deployment type
        type = '*'
      catch e
        errMsg = "Unable to perform DeploymentQuery: Invalid URN #{params.urn}
                  (#{e.message})."
        @logger.error "#{meth} - ERROR: #{errMsg}"
        return q.reject new Error errMsg
    else if params.urn?
      try
        ref = @manifestHelper.urnToRef params.urn
        @logger.info "#{meth} - URN Data = #{JSON.stringify ref}"
        filter['kumori/domain'] = @manifestHelper.hashLabel ref.domain
        filter['kumori/name'] = @manifestHelper.hashLabel ref.name
        type = ref.kind
      catch e
        errMsg = "Unable to perform DeploymentQuery: Invalid URN #{params.urn}
                  (#{e.message})."
        @logger.error "#{meth} - ERROR: #{errMsg}"
        return q.reject new Error errMsg

    if params.ref?
      filter['kumori/domain'] = @manifestHelper.hashLabel params.ref.domain
      filter['kumori/name'] = @manifestHelper.hashLabel params.ref.name
      type = params.ref.kind

    # Filter that determines how much information on each deployment will be
    # returned. Possible values are:
    # - urn: return no data
    # - topology: return basic data + instances information (DEFAULT)
    # - extended: return all data available
    if not params.show?
      @logger.info "#{meth} - Empty 'show' property. Defaulting to 'topology'."
      params.show = 'topology'
    else if params.show not in VALID_SHOW_VALUES
      @logger.info "#{meth} - Invalid 'show' value: #{params.show}. Defaulting
                    to 'topology'."
      params.show = 'topology'

    deplList = {}

    # Add v3deployments
    q()
    .then () =>
      if (type is 'deployment') or (type is '*') or (type is '')
        @manifestRepository.listElementType 'v3deployments', params.owner, filter
      else
        q {}
    .then (elements) =>
      if params.show is 'manifest'
        q elements
      else if params.show is 'urn'
        elements = @removeDeploymentData elements
        q elements
      else if params.show is 'extended'
        @completeV3Deployments elements, true
      else
        # Default is 'topology'
        @completeV3Deployments elements, false
    .then (elements) =>
      deplList = utils.extendObject deplList, elements
      @logger.info "#{meth} - Found #{Object.keys(deplList).length} deployments"
      # Make some adjustments in the returned information, since AdmissionClient
      # and/or Dashboard rely on specific fields not presents in the current
      # version
      return deplList
    .catch (err) =>
      errMsg = "Unable to perform DeploymentQuery: #{err.message} #{err.stack}"
      @logger.error "#{meth} - ERROR: #{errMsg}"
      q.reject new Error errMsg


  getDeploymentOwner: (deploymentURN) ->
    meth = "PlannerStub.getDeploymentOwner() - URN: #{deploymentURN}"
    @logger.info meth

    params =
      urn: deploymentURN
      show: 'manifest'

    @deploymentQuery params
    .then (deploymentList) =>
      if Object.keys(deploymentList).length is 0
        errMsg = "No deployment found for #{deploymentURN}"
        @logger.info "#{meth} - #{errMsg}"
        return q.reject new Error errMsg
      else if Object.keys(deploymentList).length > 1
        errMsg = "Several matches found for deployment #{deploymentURN}"
        @logger.warn "#{meth} - #{errMsg}: #{JSON.stringify deploymentList}"
        return q.reject new Error errMsg
      else
        deploymentURN = Object.keys(deploymentList)[0]
        @logger.info "#{meth} - Found deployment for #{deploymentURN} :
                      #{deploymentURN}"
        @logger.info "#{meth} - Owner: #{deploymentList[deploymentURN].owner}"
        return deploymentList[deploymentURN].owner
    .catch (err) =>
      errMsg = "Unable to get owner of deployment #{deploymentURN}:
                #{err.message}"
      @logger.info "#{meth} - ERROR: #{errMsg}"
      return q.reject new Error errMsg


  getInstanceOwner: (instanceId) ->
    meth = "PlannerStub.getInstanceOwner() - instance: #{instanceId}"
    @logger.info meth

    deploymentK8sId = null
    deplList = {}
    filter = {}

    @k8sApiStub.getPod KUMORI_NAMESPACE, instanceId
    .then (podInfo) =>

      if not podInfo?.metadata?.labels?['kumori/deployment.id']?
        errMsg = 'Invalid Instance (no kumori/deployment.id label found)'
        throw new Error errMsg

      deploymentName = podInfo.metadata.labels['kumori/deployment.id']
      @logger.info "#{meth} - Parent KukuDeployment: #{deploymentName}"
      @manifestRepository.getManifestByNameAndKind deploymentName, 'v3deployments'
    .then (manifest) =>
      @logger.debug "#{meth} - Parent KukuDeployment manifest:
                     #{JSON.stringify manifest}"
      @logger.debug "#{meth} - Parent KukuDeployment owner: #{manifest.owner}"
      return manifest.owner
    .catch (err) =>
      errMsg = "Unable to get owner of instance #{instanceId}: #{err.message}"
      @logger.error "#{meth} - ERROR: #{errMsg}"
      return q.reject new Error errMsg


  getDockerCredentialsFromSecret: (secretReference) ->
    meth = "PlannerStub.getDockerCredentialsFromSecret() - Secret reference:
            #{secretReference}"
    @logger.info meth

    # Alternative of searching by labels (requires passing 'owner' to method)
    #
    # refParts = secretReference.split '/'
    # filter = {}
    # filter.domain = refParts[0]
    # filter.name = refParts[1]
    # @manifestRepository.listElementType 'secrets', owner, filter

    # Convert secret reference to a URN
    # Secret reference is in format: <domain>/<name>
    secretURN = 'eslap://' + secretReference.replace '/', '/secret/'

    @logger.info "#{meth} - Getting manifest for URN #{secretURN}"
    @manifestRepository.getManifest secretURN
    .then (manifest) =>
      @logger.info "#{meth} - Got KukuSecret!"

      # 'data' property should contain a base64 string representation of a
      # stringified docker config in JSON format
      if manifest?.description?.data?
        rawDockerConfigJson = manifest.description.data
        @logger.info "#{meth} - RAW    : #{rawDockerConfigJson}"
        decodedDockerConfigJson =
          Buffer.from(rawDockerConfigJson, 'base64').toString()
        # @logger.info "#{meth} - DECODED: #{decodedDockerConfigJson}"
        dockerConfig = JSON.parse decodedDockerConfigJson
        # @logger.info "#{meth} - PARSED : #{dockerConfig}"
        authsKeys = Object.keys dockerConfig.auths
        rawAuthData = dockerConfig.auths[authsKeys[0]].auth
        # @logger.info "#{meth} - AUTH DATA RAW    : #{rawAuthData}"
        decodedAuthData = Buffer.from(rawAuthData, 'base64').toString()
        # @logger.info "#{meth} - AUTH DATA DECODED: #{decodedAuthData}"
        parts = decodedAuthData.split ':'

        if parts.length < 2
          errMsg = "Secret decoded credentials are malformed."
          throw new Error errMsg
        else
          credentials =
            username: parts.shift()
            password: parts.join ':'

          # @logger.info "#{meth} - CREDENTIALS: #{JSON.stringify credentials}"
          @logger.info "#{meth} - CREDENTIALS USERNAME: #{credentials.username}"
          return credentials
      else
        errMsg = "Secret does not contain expected 'description/data' property."
        throw new Error errMsg
    .catch (err) =>
      errMsg = "Unable to get Secret #{secretReference}: #{err.message}"
      @logger.error "#{meth} - ERROR: #{errMsg}"
      return q.reject new Error errMsg


  restartInstance: (podName) ->
    meth = "PlannerStub.restartInstance(#{podName})"
    @logger.info meth

    @logger.info "#{meth} - Evicting Pod #{podName}..."
    @k8sApiStub.evictPod KUMORI_NAMESPACE, podName


  restartRole: (deploymentName, role) ->
    meth = "PlannerStub.restartRole Deployment: #{deploymentName},
            Role: #{role}"
    @logger.info meth

    # Find deployments and statefulSets with the appropriate labels
    labelSelector = ''
    labelSelector += "kumori/deployment.id=#{deploymentName}"
    labelSelector += ",kumori/role=" + @manifestHelper.hashLabel role

    updated = false
    deploymentList = null
    statefulSetList = null
    @k8sApiStub.getDeployments KUMORI_NAMESPACE, labelSelector
    .then (deployments) =>
      deploymentList = deployments
      @k8sApiStub.getStatefulSets KUMORI_NAMESPACE, labelSelector
    .then (statefulSets) =>
      statefulSetList = statefulSets
    .then () =>
      @logger.info "#{meth} - Found #{deploymentList.length} Deployments and
                    #{statefulSetList.length} StatefulSets."
      if deploymentList.length + statefulSetList.length is 0
        errMsg = "No Deployment or StatefulSet found for ID #{deploymentName}
                  and role #{role}"
        @logger.error "#{meth} ERROR: #{errMsg}"
        throw new Error errMsg
      else if deploymentList.length + statefulSetList.length > 1
        errMsg = "More than one Deployment or StatefulSet found for
                  ID #{deploymentName} and role #{role}"
        @logger.error "#{meth} ERROR: #{errMsg}"
        throw new Error errMsg
      else
        # There is only one, so let's patch it
        patch = [{
          op: 'add'
          # The slash in the annotation name (kumori~1restartedAt) is escaped in
          # JSONPatch by using '~1'.
          # Source: http://jsonpatch.com/#json-pointer
          path: '/spec/template/metadata/annotations/kumori~1restartedAt'
          value: utils.getTimestamp()
        }]

        if deploymentList.length is 1
          # Patch the deployment
          name = deploymentList[0].metadata.name
          @logger.info "#{meth} - Patching deployment #{name}..."
          @k8sApiStub.patchDeployment KUMORI_NAMESPACE, name, patch
        else if statefulSetList.length is 1
          # Patch the statefulSet
          name = statefulSetList[0].metadata.name
          @logger.info "#{meth} - Patching StatefulSet #{name}..."
          @k8sApiStub.patchStatefulSet KUMORI_NAMESPACE, name, patch


  ##############################################################################
  ##                           HELPER FUNCTIONS                               ##
  ##############################################################################

  removeSolutionData: (solutionManifests) ->
    cleanManifests = {}
    for solURN, solData of solutionManifests

      cleanItem =
        deployments: []

      # If there is a top, place it in the first position
      if solData.top?
        cleanItem.deployments.push solData.top

      # Add the remaining deployments
      for deplName, deplData of solData.deployments
        if deplName not in cleanItem.deployments
          cleanItem.deployments.push deplName

      # If object is being deleted add a deletionTimestamp property
      if solData.deletionTimestamp?
        cleanItem.deletionTimestamp = solData.deletionTimestamp

      cleanManifests[solURN] = cleanItem
    return cleanManifests


  removeDeploymentData: (deploymentManifests) ->
    for deplURN, deplData of deploymentManifests
      if deplData.urn?
        deploymentManifests[deplData.urn] = {}
      else
        @logger.error "PlannerStub.removeDeploymentData - Manifest #{deplURN}
                       has no 'urn' property."
        deploymentManifests[deplURN] = {}

    return deploymentManifests


  completeSolutions: (solutionManifests, extended = false) ->
    promiseList = []
    for solURN, solData of solutionManifests
      promiseList.push @completeSolution solData, extended
    q.allSettled promiseList
    .then () ->
      solutionManifests


  completeV3Deployments: (deploymentManifests, extended = false) ->
    promiseList = []
    for deplURN, deplData of deploymentManifests
      promiseList.push @completeV3Deployment deplData, extended
    q.allSettled promiseList
    .then () ->
      deploymentManifests


  completeSolution: (solutionManifest, extended = false) ->
    meth = "PlannerStub.completeSolution #{solutionManifest.name} (#{extended})"
    @logger.info meth

    promiseList = []


    # We will need the top deployment ID to differentiate between solution and
    # top deployment links (they have the same domain and name)
    topDeploymentManifest = null

    topDeploymentRef =
      kind: 'deployment'
      domain: solutionManifest.ref.domain
      name: solutionManifest.top

    params =
      ref: topDeploymentRef
      show: 'manifest'

    @getDeployment params
    .then (deploymentManifest) =>
      if not deploymentManifest?
        errMsg = "Top deployment #{@manifestHelper.refToUrn topDeploymentRef}
                  not found."
        @logger.error "#{meth} - #{errMsg}"
        throw new Error errMsg

      topDeploymentManifest = deploymentManifest
      @logger.debug "#{meth} - Found top deployment
                    #{@manifestHelper.refToUrn topDeploymentRef}"


      @logger.debug "#{meth} - Getting solution events..."

      # Get solution events
      @eventStore.getSolutionEvents KUMORI_NAMESPACE, solutionManifest.name
    .then (nsEvents) =>
      @logger.debug "#{meth} - Got #{nsEvents.length} events."

      # Add events info to the original manifest
      solutionManifest.events =
        @filterSolutionEvents solutionManifest.name, nsEvents

      @logger.debug "#{meth} - Getting active links of the solution..."

      # Get solution links (only external links)
      @getSolutionLinks solutionManifest.name, solutionManifest.urn
      , topDeploymentManifest.name , topDeploymentManifest.urn, true
    .then (links) =>
        @logger.debug "#{meth} - Got links."

        # Add links info to the original manifest
        solutionManifest['externalLinks'] = links

        @logger.debug "#{meth} - Getting solution deployments info..."

        # Get detailed info of all deployments of the solution (including  their
        # events and links)
        solutionDeploymentsParams =
          solutionURN: solutionManifest.urn
          show: 'extended'

        @deploymentQuery solutionDeploymentsParams
    .then (deploymentList) =>
      @logger.debug "#{meth} - Got #{Object.keys(deploymentList).length}
                     deployments of the solution."

      # For each deployment, add its events, links and instances to the solution
      # manifest
      for deplURN, deplData of deploymentList
        deplRef = @manifestHelper.urnToRef deplURN

        if deplRef.name of solutionManifest.deployments

          # Add the internalID as 'id' (since name is already used)
          solutionManifest.deployments[deplRef.name].id = deplData.name
          # Add links and events
          solutionManifest.deployments[deplRef.name].events = deplData.events
          solutionManifest.deployments[deplRef.name].links = deplData.links

          # Add deployment events to the solution-level events
          solutionManifest.events =
            utils.arrayMerge solutionManifest.events, deplData.events, true

          # For each role, add its instances
          roles =
            solutionManifest.deployments[deplRef.name].artifact.description.role
          for roleName, roleData of roles
            roleData.instances =
              deplData.artifact.description.role[roleName].instances
        else
          @logger.error = "#{meth} - Deployment not part of the solution:
                           #{deplURN}"

      solutionManifest
    .catch (err) =>
      @logger.error "#{meth} - ERROR: #{err.message} #{err.stack}"
      solutionManifest



  completeV3Deployment: (v3Manifest, extended = false) ->
    meth = "PlannerStub.completeV3Deployment #{v3Manifest.name} (#{extended})"
    @logger.info meth

    promiseList = []

    @eventStore.getDeploymentEvents KUMORI_NAMESPACE, v3Manifest.name
    .then (nsEvents) =>

      if 'events' not of v3Manifest
        v3Manifest.events = @filterDeploymentEvents v3Manifest.name, nsEvents


      if 'links' not of v3Manifest
        promiseList.push (
          @getDeploymentLinks v3Manifest, true
          .then (links) =>
            v3Manifest['links'] = links
        )

      # Differentiate between normal deployment and builtins.
      #
      # Currently, the only builtin is Inbound Service, which doesn't have
      # instances, and no extra info to include.

      if v3Manifest.artifact?.description?.builtin
        # Deployment is a builtin
        if isInboundBuiltin v3Manifest
          # Deployment is an Inbound builtin
          # Currently, we dont add any extra information to an Inbound builtin
          logger.debug "#{meth} - Detected an Inbound builtin."
        else
          logger.warn "#{meth} - Unknown builtin:
                       #{JSON.stringify v3Manifest.ref}"
      else
        # Deployment is not a builtin
        serviceRoles = v3Manifest.artifact?.description?.role || {}

        for roleName, roleData of serviceRoles
          do (roleName, roleData) =>
            if extended and ('instances' not of roleData)
              contNames = Object.keys(roleData.artifact.description.code)
              promiseList.push (
                @getRoleInstances v3Manifest.name, roleName, contNames, nsEvents
                .then (roleInstances) =>
                  roleData['instances'] = roleInstances
              )

      q.allSettled promiseList
    .then () ->
      v3Manifest
    .catch (err) ->
      logger.error "#{meth} - ERROR: #{err.message} #{err.stack}"
      v3Manifest



  getRoleInstances: (deplName, roleName, contNames, nsEvents) ->
    meth = "PlannerStub.getRoleInstances #{deplName} - #{roleName}"
    @logger.info meth

    # Prepare a reverse dictionnary of container names and their hashes
    contHashToName = {}
    for contName in contNames
      hash = 'k-' + @manifestHelper.hashLabel contName
      contHashToName[hash] = contName

    # Prepare the label selector for the search
    labelSelector = ''
    labelSelector += "kumori/deployment.id=#{deplName}"
    labelSelector += ",kumori/role=" + @manifestHelper.hashLabel roleName

    roleInstances = {}
    promiseList = []
    @k8sApiStub.getPods KUMORI_NAMESPACE, labelSelector
    .then (podList) =>
      @logger.info "#{meth} - Found #{podList.length} pods."
      for pod in podList
        do (pod) =>
          promiseList.push (
            @k8sApiStub.getPodMetrics KUMORI_NAMESPACE, pod.metadata.name
            .then (podMetrics) =>
              instanceData =
                name: pod.metadata.name
                creationDate: pod.metadata.creationTimestamp
                node: pod.spec.nodeName || null
                status: pod.status.phase
                statusReason: pod.status.reason || null
                statusMessage: pod.status.message || null
                ready: false
                containers: {}
                metrics: {}

              # Determine if Pod is marked for deletion
              if pod.metadata.deletionTimestamp?
                instanceData.deletionTimestamp = pod.metadata.deletionTimestamp
                if pod.metadata.deletionGracePeriodSeconds?
                  instanceData.deletionGracePeriod = pod.metadata.deletionGracePeriodSeconds

              # Extract Pod ready value from Pod conditions
              for cond in (pod.status.conditions || [])
                if cond.type is 'Ready'
                  if cond.status?.toLowerCase() is 'true'
                    instanceData.ready = true

              # Process containers info
              for contStatus in (pod.status.containerStatuses || [])
                # if 'running' of contStatus.state
                #   instanceData.
                contData =
                  restarts: contStatus.restartCount
                  state: contStatus.state
                  lastState: contStatus.lastState
                  ready: contStatus.ready
                contRealName = contHashToName[contStatus.name]
                instanceData.containers[contRealName] = contData

              # Process init containers info
              for contStatus in (pod.status.initContainerStatuses || [])
                # if 'running' of contStatus.state
                #   instanceData.
                contData =
                  restarts: contStatus.restartCount
                  state: contStatus.state
                  lastState: contStatus.lastState
                  ready: contStatus.ready
                contRealName = contHashToName[contStatus.name]
                instanceData.containers[contRealName] = contData

              @addMetricData instanceData, pod.spec.containers, podMetrics
                , contHashToName
              @addPodEvents instanceData, nsEvents
              roleInstances[instanceData.name] = instanceData
          )
      q.allSettled promiseList
    .then () =>
      # console.log "RETURNING INSTANCE LIST:"
      # console.log JSON.stringify roleInstances
      # @logger.debug "#{meth} RoleInstances: #{JSON.stringify roleInstances}"
      roleInstances


  # podMetrics is an already processed object:
  #  {
  #    "usage": {
  #      "memory": 2650112,
  #      "cpu": 0
  #    },
  #    "containers": {
  #      "frontend": {
  #        "usage": {
  #          "memory": 2650112,
  #          "cpu": 0
  #        }
  #      }
  #    }
  #  }
  addMetricData: (instanceData, containersInfo, podMetrics, contHashToName) ->
    meth = "PlannerStub.addMetricData #{instanceData.name}"
    @logger.info meth

    try
      podRequests =
        cpu: 0
        memory: 0

      podLimits =
        cpu: 0
        memory: 0

      podUsage = podMetrics?.usage || { cpu: 0, memory: 0 }

      for contData in containersInfo

        # Convert hashes name to real container name
        contRealName = contHashToName[contData.name]

        # If container is not in the instance object container list, skip it
        if contRealName not of (instanceData.containers || {})
          continue
        contRes =
          requests: {}
          limits: {}
          usage: podMetrics?.containers?[contData.name]?.usage || { cpu: 0, memory: 0 }

        if contData.resources?.requests?.cpu?
          v = quantityToScalar contData.resources.requests.cpu
          contRes.requests.cpu = v
          podRequests.cpu += v
        if contData.resources?.requests?.memory?
          v = quantityToScalar contData.resources.requests.memory
          contRes.requests.memory = v
          podRequests.memory += v
        if contData.resources?.limits?.cpu?
          v = quantityToScalar contData.resources.limits.cpu
          contRes.limits.cpu = v
          podLimits.cpu += v
        if contData.resources?.limits?.memory?
          v = quantityToScalar contData.resources.limits.memory
          contRes.limits.memory = v
          podLimits.memory += v

        instanceData.containers[contRealName].metrics = contRes

      instanceData.metrics =
        requests: podRequests
        limits: podLimits
        usage: podUsage
      true
    catch err
      @logger.error "#{meth} Unexpected error: #{err.message} #{err.stack}"
      true



  addPodEvents: (instanceData, nsEvents) ->
    meth = "PlannerStub.addPodEvents #{instanceData.name}"
    @logger.info meth
    evts = []
    for nsEvt in nsEvents
      # console.log "EVENT:"
      # console.log " - kind: #{nsEvt.involvedObject.kind}"
      # console.log " - name: #{nsEvt.involvedObject.name}"
      # console.log " - Pod:  #{instanceData.name}"

      if nsEvt.involvedObject?.kind is 'Pod' \
      and nsEvt.involvedObject?.name is instanceData.name
        evt =
          time:            nsEvt.eventTime
          firstTimestamp:  nsEvt.firstTimestamp
          lastTimestamp:   nsEvt.lastTimestamp
          counter:         nsEvt.count
          message:         nsEvt.message
          type:            nsEvt.type
          reason:          nsEvt.reason
        evts.push evt
      # console.log "POD EVENT LIST: #{evts}"
    instanceData.events = evts



  filterSolutionEvents: (solName, nsEvents) ->

    # RELEVANT_TYPES = [
    #   'Pod'
    #   'ReplicaSet'
    #   'StatefulSet'
    # ]

    evts = []
    for nsEvt in nsEvents
      # console.log "EVENT:"
      # console.log JSON.stringify nsEvt, null, 2
      # console.log ""
      # console.log "EVENT:"
      # console.log " - Event involvedObject kind: #{nsEvt.involvedObject.kind}"
      # console.log " - Event involvedObject name: #{nsEvt.involvedObject.name}"
      # console.log " - Deployment name          : #{deplName}"

      # if nsEvt.involvedObject?.kind in RELEVANT_TYPES
      # console.log "Involved object name: #{nsEvt.involvedObject.name}"
      # console.log "Involved object labels: #{JSON.stringify nsEvt.involvedObject}"
      # console.log "Involved object labels: #{JSON.stringify nsEvt.involvedObject.labels}"
      # console.log "Involved object annotations: #{JSON.stringify nsEvt.involvedObject.annotations}"
      if nsEvt.involvedObject?.name? \
      and nsEvt.involvedObject.name.includes solName
        evt =
          time:            nsEvt.eventTime
          firstTimestamp:  nsEvt.firstTimestamp
          lastTimestamp:   nsEvt.lastTimestamp
          counter:         nsEvt.count
          objectKind:      nsEvt.involvedObject.kind
          objectName:      nsEvt.involvedObject.name
          message:         nsEvt.message
          type:            nsEvt.type
          reason:          nsEvt.reason
        evts.push evt
    evts


  filterDeploymentEvents: (deplName, nsEvents) ->

    RELEVANT_TYPES = [
      'Pod'
      'ReplicaSet'
      'StatefulSet'
    ]

    evts = []
    for nsEvt in nsEvents
      # console.log "EVENT:"
      # console.log JSON.stringify nsEvt, null, 2
      # console.log ""
      # console.log "EVENT:"
      # console.log " - Event involvedObject kind: #{nsEvt.involvedObject.kind}"
      # console.log " - Event involvedObject name: #{nsEvt.involvedObject.name}"
      # console.log " - Deployment name          : #{deplName}"

      # if nsEvt.involvedObject?.kind in RELEVANT_TYPES
      # console.log "Involved object name: #{nsEvt.involvedObject.name}"
      # console.log "Involved object labels: #{JSON.stringify nsEvt.involvedObject}"
      # console.log "Involved object labels: #{JSON.stringify nsEvt.involvedObject.labels}"
      # console.log "Involved object annotations: #{JSON.stringify nsEvt.involvedObject.annotations}"
      if nsEvt.involvedObject?.name? \
      and nsEvt.involvedObject.name.includes deplName
        evt =
          time:            nsEvt.eventTime
          firstTimestamp:  nsEvt.firstTimestamp
          lastTimestamp:   nsEvt.lastTimestamp
          counter:         nsEvt.count
          objectKind:      nsEvt.involvedObject.kind
          objectName:      nsEvt.involvedObject.name
          message:         nsEvt.message
          type:            nsEvt.type
          reason:          nsEvt.reason
        evts.push evt
    evts


  getSolutionLinks: (solutionName, solutionURN, topDeploymentName, topDeploymentURN, convertLinks = false) ->

    meth = "PlannerStub.getSolutionLinks() Solution Name: #{solutionName} -
            Top Deployment Name: #{topDeploymentName} -
            Top Deployment URN: #{topDeploymentURN}"
    @logger.info meth

    # Filter on the existence of a label named as the deployment
    filter = {
      "#{topDeploymentName}": '*'
      'kumori/solution.id': "!!#{solutionName}"
    }

    @manifestRepository.listElementType 'links', null, filter
    .then (elements) =>
      if Object.keys(elements).length > 0
        @logger.info "#{meth} - Found links for deployment #{topDeploymentName}."

      if convertLinks
        # console.log "\n\n\n******************************************"
        # console.log "LINKS:"
        # console.log JSON.stringify elements, null, 2

        # Solution level links (external links) are old style
        links = @convertLinks elements, solutionURN

        # console.log "CONVERTED LINKS:"
        # console.log JSON.stringify links, null, 2
        # console.log "******************************************\n\n\n"
        links
      else
        elements
    .catch (err) =>
      errMsg = "Unable to get links for deployment #{topDeploymentName}: #{err.message}
                #{err.stack}"
      @logger.error "#{meth} - ERROR: #{errMsg}"
      q.reject new Error errMsg



  getDeploymentLinks: (deplManifest, convertLinks = false) ->

    deplName = deplManifest.name
    deplURN  = deplManifest.urn

    meth = "PlannerStub.getDeploymentLinks() Name: #{deplName} URN: #{deplURN}"
    @logger.info meth

    # Filter on the existence of a label named as the deployment
    filter = { "#{deplName}": '*' }

    @manifestRepository.listElementType 'links', null, filter
    .then (elements) =>
      if Object.keys(elements).length > 0
        @logger.info "#{meth} - Found links for deployment #{deplName}."

      if convertLinks
        # console.log "\n\n\n******************************************"
        # console.log "LINKS:"
        # console.log JSON.stringify elements, null, 2
        links = @convertLinks elements, deplURN
        # console.log "CONVERTED LINKS:"
        # console.log JSON.stringify links, null, 2
        # console.log "******************************************\n\n\n"
        links
      else
        elements
    .catch (err) =>
      errMsg = "Unable to get links for deployment #{deplName}: #{err.message}
                #{err.stack}"
      @logger.error "#{meth} - ERROR: #{errMsg}"
      q.reject new Error errMsg




  convertLinks: (links, referenceDeploymentURN) ->
    meth = 'PlannerStub.convertLinks'
    @logger.debug "#{meth} ReferenceURN: #{referenceDeploymentURN} -
                   Links: #{JSON.stringify links}"

    # We currently support two link formats, for backwards compatibility.

    newLinks = {}

    for linkName, linkData of links

      if 'endpoints' of linkData
        @convertLinkOld linkData, referenceDeploymentURN, newLinks

      else if 's_d' of linkData
        @convertLinkNew linkData, referenceDeploymentURN, newLinks

      else
        @logger.error "#{meth} - ERROR: invalid link format."

    @logger.debug "#{meth} Converted links: #{JSON.stringify newLinks}"
    return newLinks



  convertLinkNew: (linkData, referenceDeploymentURN, newLinks) ->
    meth = 'PlannerStub.convertLinkNew'
    @logger.debug "#{meth} ReferenceURN: #{referenceDeploymentURN} -
                   Links: #{JSON.stringify linkData}"

    # SOURCE FORMAT (ECLOUD):
    #
    #  {
    #      "kl-c0962e640495cf45d78d073f03ae84ea6d50d169": {
    #          "s_d": "calchazel_deployment",
    #          "s_c": "hazel.management",
    #          "t_d": "calchazel_deployment.hazel",
    #          "t_c": "management",
    #          "meta": {},
    #          "name": "kl-c0962e640495cf45d78d073f03ae84ea6d50d169",
    #          "owner": "devel__arroba__kumori.cloud"
    #      },
    #      "kl-de0683b867a4df1851219838a649fc17b0325aa7": {
    #          "s_d": "calchazel_deployment",
    #          "s_c": "hazel.data",
    #          "t_d": "calchazel_deployment.hazel",
    #          "t_c": "data",
    #          "meta": {},
    #          "name": "kl-de0683b867a4df1851219838a649fc17b0325aa7",
    #          "owner": "devel__arroba__kumori.cloud"
    #      }
    #  }
    #
    # EXPECTED FORMAT:
    #
    # "http-acs": {
    #   "slap://eslap.cloud/deployments/20190911_133549/9b914f9c": {
    #     "service-acs": {}
    #   },
    #   "slap://eslap.cloud/deployments/20190911_133552/ae241834": {
    #     "frontend": {}
    #   }
    # }


    # Extract short deployment name from the URN
    deploymentRef = @manifestHelper.urnToRef referenceDeploymentURN
    deploymentName = deploymentRef.name


    try
      sourceDeploymentURN = @manifestHelper.refToUrn {
        kind:   deploymentRef.kind
        domain: deploymentRef.domain
        name:   linkData['s_d']
      }

      targetDeploymentURN = @manifestHelper.refToUrn {
        kind:   deploymentRef.kind
        domain: deploymentRef.domain
        name:   linkData['t_d']
      }

      if sourceDeploymentURN is referenceDeploymentURN
        if linkData['s_c'] not of newLinks
          newLinks[linkData['s_c']] = {}
        if targetDeploymentURN not of newLinks[linkData['s_c']]
          newLinks[linkData['s_c']][targetDeploymentURN] = {}
        newLinks[linkData['s_c']][targetDeploymentURN][linkData['t_c']] = {}

      if targetDeploymentURN is referenceDeploymentURN
        if linkData['t_c'] not of newLinks
          newLinks[linkData['t_c']] = {}
        if sourceDeploymentURN not of newLinks[linkData['t_c']]
          newLinks[linkData['t_c']][sourceDeploymentURN] = {}
        newLinks[linkData['t_c']][sourceDeploymentURN][linkData['s_c']] = {}

    catch err
      @logger.error "#{meth} ERROR: #{err.message} - #{err.stack}"

    # This method modifies the provided object newLinks
    return true



  convertLinkOld: (linkData, referenceDeploymentURN, newLinks) ->
    meth = 'PlannerStub.convertLinkOld'
    @logger.debug "#{meth} ReferenceURN: #{referenceDeploymentURN} -
                   Links: #{JSON.stringify linkData}"

    # SOURCE FORMAT (ECLOUD):
    #
    # {
    #   "name": "eslap://eslap.cloud/links/kl-e913a2a4e9b5268cda2e3cbd870fa144e5cce769",
    #   "owner": "alice@keycloak.org",
    #   "endpoints": [
    #     {
    #       "channel": "",
    #       "deployment": "eslap://eslap.cloud/httpinbounds/20191114_162738-cb1bf5f9-kh"
    #     },
    #     {
    #       "channel": "http-admission",
    #       "deployment": "eslap://jferrer.dev.testing/deployments/20191114_162738-d5f9c1b8-kd"
    #     }
    #   ],
    #   "spec": "http://eslap.cloud/manifest/link/1_0_0"
    # }
    #
    # EXPECTED FORMAT:
    #
    # "http-acs": {
    #   "slap://eslap.cloud/deployments/20190911_133549/9b914f9c": {
    #     "service-acs": {}
    #   },
    #   "slap://eslap.cloud/deployments/20190911_133552/ae241834": {
    #     "frontend": {}
    #   }
    # }

    try
      # for code legibility
      eps = linkData.endpoints
      @logger.debug "#{meth} Link endpoints: #{JSON.stringify eps}"

      depl0URN = @manifestHelper.refToUrn {
        kind:   eps[0].kind
        domain: eps[0].domain
        name:   eps[0].name
      }

      depl1URN = @manifestHelper.refToUrn {
        kind:   eps[1].kind
        domain: eps[1].domain
        name:   eps[1].name
      }
      @logger.debug "#{meth} Deployment1 URN: #{depl0URN}"
      @logger.debug "#{meth} Deployment2 URN: #{depl1URN}"

      if depl0URN is referenceDeploymentURN
        if eps[0].channel not of newLinks
          newLinks[eps[0].channel] = {}
        if depl1URN not of newLinks[eps[0].channel]
          newLinks[eps[0].channel][depl1URN] = {}
        newLinks[eps[0].channel][depl1URN][eps[1].channel] =
          meta: linkData.meta || {}

      if depl1URN is referenceDeploymentURN
        if eps[1].channel not of newLinks
          newLinks[eps[1].channel] = {}
        if depl0URN not of newLinks[eps[1].channel]
          newLinks[eps[1].channel][depl0URN] = {}
        newLinks[eps[1].channel][depl0URN][eps[0].channel] =
          meta: linkData.meta || {}
    catch err
      @logger.error "#{meth} ERROR: #{err.message} - #{err.stack}"

    # This method modifies the provided object newLinks
    return true


  # Look for Resources objects in an object at any depth
  extractResources: (obj, context, resourceList = []) ->

    validKinds = @manifestHelper.getValidResourceKinds()

    if not obj?
      return resourceList

    if 'object' isnt typeof obj
      return resourceList

    keys = Object.keys obj
    if (keys.length is 1) and (keys[0] in validKinds)
      # It's a resource object, only keep resource references (strings) not
      # inline resrouce definitions (objects)
      if ('string' is typeof obj[keys[0]]) and (obj[keys[0]] isnt '')
        # First and only key (0) is the resource type
        # The object of that key is the resource reference

        # Normalize reference (add a domain if it doesn't include one)
        normResRef = @normalizeResourceReference obj[keys[0]], context

        resURN = 'eslap://' + normResRef.replace '/', "/#{keys[0]}/"
        if resURN not in resourceList
          resourceList.push resURN
    else
      for k, v of obj
        @extractResources v, context, resourceList

    return resourceList


  # Add a domain to a reference if it doesn't have one. The added domain is the
  # current user ID.
  normalizeResourceReference: (resourceReference, context) ->

    if resourceReference.indexOf('/') > 0
      newResourceReference = resourceReference
    else
      newResourceReference = "#{context.user.id}/#{resourceReference}"

    return newResourceReference


  isInboundBuiltin: (deploymentManifest) ->
    if deploymentManifest.artifact?.description?.builtin \
    and deploymentManifest.artifact?.ref? \
    and (deploymentManifest.artifact.ref.kind is 'service') \
    and (deploymentManifest.artifact.ref.domain is 'kumori.systems') \
    and (deploymentManifest.artifact.ref.name is 'inbound')
      return true

    return false


  quantityToScalar = (quantity) ->
    return 0 if not quantity?

    if quantity.endsWith 'm'
      # Rounded to two decimals
      cleanNumberStr = quantity.substr 0, quantity.length - 1
      number = parseInt(cleanNumberStr, 10) / 1000.0
      return Math.round(number * 100) / 100

    if quantity.endsWith 'Ki'
      cleanNumberStr = quantity.substr 0, quantity.length - 2
      return parseInt(cleanNumberStr, 10) * 1024

    if quantity.endsWith 'K'
      cleanNumberStr = quantity.substr 0, quantity.length - 1
      return parseInt(cleanNumberStr, 10) * 1000

    if quantity.endsWith 'Mi'
      cleanNumberStr = quantity.substr 0, quantity.length - 2
      return parseInt(cleanNumberStr, 10) * 1024 * 1024

    if quantity.endsWith 'M'
      cleanNumberStr = quantity.substr 0, quantity.length - 1
      return parseInt(cleanNumberStr, 10) * 1000 * 1000

    if quantity.endsWith 'Gi'
      cleanNumberStr = quantity.substr 0, quantity.length - 2
      return parseInt(cleanNumberStr, 10) * 1024 * 1024 * 1024

    if quantity.endsWith 'G'
      cleanNumberStr = quantity.substr 0, quantity.length - 1
      return parseInt(cleanNumberStr, 10) * 1024 * 1000 * 1000

    if quantity.endsWith 'Ti'
      cleanNumberStr = quantity.substr 0, quantity.length - 2
      return parseInt(cleanNumberStr, 10) * 1024 * 1024 * 1024 * 1024

    if quantity.endsWith 'T'
      cleanNumberStr = quantity.substr 0, quantity.length - 1
      return parseInt(cleanNumberStr, 10) * 1024 * 1000 * 1000 * 1000

    if quantity.endsWith 'Pi'
      cleanNumberStr = quantity.substr 0, quantity.length - 2
      return parseInt(cleanNumberStr, 10) * 1024 * 1024 * 1024 * 1024 * 1024

    if quantity.endsWith 'P'
      cleanNumberStr = quantity.substr 0, quantity.length - 1
      return parseInt(cleanNumberStr, 10) * 1024 * 1000 * 1000 * 1000 * 1000


    num = parseInt quantity, 10
    if isNaN num
      throw new Error "Unknown quantity #{quantity}"
    else
      return num



module.exports = PlannerStub
