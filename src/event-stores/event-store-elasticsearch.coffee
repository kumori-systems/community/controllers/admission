###
* Copyright 2022 Kumori Systems S.L.

* Licensed under the EUPL, Version 1.2 or – as soon they
  will be approved by the European Commission - subsequent
  versions of the EUPL (the "Licence");

* You may not use this work except in compliance with the
  Licence.

* You may obtain a copy of the Licence at:

  https://joinup.ec.europa.eu/software/page/eupl

* Unless required by applicable law or agreed to in
  writing, software distributed under the Licence is
  distributed on an "AS IS" basis,

* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
  express or implied.

* See the Licence for the specific language governing
  permissions and limitations under the Licence.
###

q      = require 'q'
Client = require('@elastic/elasticsearch').Client

EventStore = require './event-store'

# Expected configuration options:
#   options:
#     serverURL: 'http://elasticsearch-jferrer.test.kumori.cloud:9200/'
#     username: ''
#     password: ''
#     index: 'kube-events-*'
#     maxResults: 200
class EventStoreElasticsearch extends EventStore

  constructor: (options) ->
    super 'elasticsearch'
    @index      = options.index
    @maxResults = options.maxResults

    @serverURL  = options.serverURL
    @username   = options.username || ''
    @password   = options.password || ''

    @clientOptions =
      node: @serverURL

    if @username isnt '' and @password isnt ''
      @clientOptions.auth =
        username: @username
        password: @password

    @client = null


  ##############################################################################
  ##                            PUBLIC METHODS                                ##
  ##############################################################################

  # Returns a promise
  init: () ->
    meth = 'EventStoreElasticsearch.init'
    q.promise (resolve, reject) =>
      try
        # Initialize the Elasticsearch client
        @client = new Client @clientOptions

        # Try to refresh the indices
        await @client.indices.refresh { index: @index }

        resolve()
      catch err
        errMsg = "Couldn't initialize Elasticsearch Event Store:
                  #{err.message || err}"
        @logger.error "#{meth} - ERROR: #{errMsg}"
        # Promise was rejected but was changed as part of ticket 1323, to allow
        # users to continue managing deployments even if Elasticsearch is down.
        resolve()


  # Returns a promise
  terminate: () ->
    q.promise (resolve, reject) =>
      try
        # Close Elasticsearch client
        await @client.close()


        resolve()
      catch err
        errMsg = "Couldn't close Elasticsearch Event Store:
                  #{err.message || err}"
        reject new Error err


  # Get events from the store based on the provided filters and the client
  # configuration.
  # Filter parameters:
  # - namespace
  # - deployment (internal ID)
  # - fromDate  (formatted as "2021-05-17T09:00:00Z")
  # - toDate  (formatted as "2021-05-17T09:00:00Z")
  getDeploymentEvents: (namespace, deploymentID, fromDate, toDate) ->
    meth = "EventStoreElasticsearch.getDeploymentEvents
            deploymentID: #{deploymentID} from: #{fromDate} to: #{toDate}"
    @logger.info "#{meth}"


    # Date must follow ISO 8601 format. In Nodejs: Date().toISOString()
    #
    # Example:
    # - fromDate = "2021-05-17T09:05:00.136Z"
    # - toDate   = "2021-05-17T09:05:15.136Z"
    #
    # By default, search events of the last two days (today and yesterday):
    # - greater or equal than yesterday
    # - lower or equal than today
    fromDate ?= "now-1d/d"
    toDate   ?= "now/d"

    query =
      bool:
        # All clauses inside the 'must' array must match
        must: [{

          bool:
            # Any clause inside the 'should' array can match
            should: [{
              # Elasticsearch tokenizes by word separators including '-'
              # As a workaround, we will just use 'AND' operator by default, so
              # all tokens are mandatory.
              match:
                "involvedObject.labels.owner":
                  query: deploymentID
                  operator: "AND"
            },
            {
              match:
                "involvedObject.labels.kumori/deployment.id":
                  query: deploymentID
                  operator: "AND"
            },
            {
              match:
                "involvedObject.name":
                  query: deploymentID
                  operator: "AND"
            }]
        },
        {
          # Filter by events lastTimestamp (for now, this might be reconsidered)
          range:
            lastTimestamp:
              gte: fromDate
              lte: toDate
        }]


    @runQuery query
    .then (eventList) =>
      @cleanEvents eventList


  # Get events from the store based on the provided filters and the client
  # configuration.
  # Filter parameters:
  # - namespace
  # - solution (internal ID)
  # - fromDate  (formatted as "2021-05-17T09:00:00Z")
  # - toDate  (formatted as "2021-05-17T09:00:00Z")
  getSolutionEvents: (namespace, solutionID, fromDate, toDate) ->
    meth = "EventStoreElasticsearch.getDeploymentEvents
            solutionID: #{solutionID} from: #{fromDate} to: #{toDate}"
    @logger.info "#{meth}"


    # Date must follow ISO 8601 format. In Nodejs: Date().toISOString()
    #
    # Example:
    # - fromDate = "2021-05-17T09:05:00.136Z"
    # - toDate   = "2021-05-17T09:05:15.136Z"
    #
    # By default, search events of the last two days (today and yesterday):
    # - greater or equal than yesterday
    # - lower or equal than today
    fromDate ?= "now-1d/d"
    toDate   ?= "now/d"

    query =
      bool:
        # All clauses inside the 'must' array must match
        must: [{

          bool:
            # Any clause inside the 'should' array can match
            should: [{
              # Elasticsearch tokenizes by word separators including '-'
              # As a workaround, we will just use 'AND' operator by default, so
              # all tokens are mandatory.
              match:
                "involvedObject.labels.owner":
                  query: solutionID
                  operator: "AND"
            },
            {
              match:
                "involvedObject.labels.kumori/solution.id":
                  query: solutionID
                  operator: "AND"
            },
            {
              match:
                "involvedObject.name":
                  query: solutionID
                  operator: "AND"
            }]
        },
        {
          # Filter by events lastTimestamp (for now, this might be reconsidered)
          range:
            lastTimestamp:
              gte: fromDate
              lte: toDate
        }]


    @runQuery query
    .then (eventList) =>
      @cleanEvents eventList



  ##############################################################################
  ##                           PRIVATE METHODS                                ##
  ##############################################################################
  runQuery: (query) ->
    meth = "EventStoreElasticsearch.runQuery"
    @logger.debug "#{meth} query: #{JSON.stringify query}"

    q.promise (resolve, reject) =>

      try
        # Refresh indices
        @logger.debug "#{meth} Refreshing indices..."
        await @client.indices.refresh { index: 'kube-events-*' }

        # Sort value: we will sort by 'creationTimestamp' in descending order,
        # to make sure that if there are more than 'size' results, the most
        # recent ones are returned.
        #
        # NOTE: In Kubernetes when an Event is updated ('count' is incremented
        #       and 'lastTimestamp' is updated) the 'creationTimestamp' property
        #       is also updated.
        searchOptions =
          index: @index
          size: @maxResults
          sort: [ "metadata.creationTimestamp:desc" ]
          body:
            query: query

        @logger.debug "#{meth} Performing search with options:
                       #{JSON.stringify searchOptions}"
        response = await @client.search searchOptions
        @logger.debug "#{meth} Search request returned."

        if response.statusCode? and response.statusCode isnt 200
          errMsg = "Query returned HTTP Status code: #{response.statusCode}"
          @logger.error "#{meth} ERROR: #{errMsg}
                        #{JSON.stringify response || {}}"
          return reject new Error Error errMsg

        if not response.body?.hits?.hits?
          errMsg = "Query response contains no 'hits' sections."
          @logger.error "#{meth} ERROR: #{errMsg}
                        #{JSON.stringify response || {}}"
          return reject new Error Error errMsg

        if response?.body?.hits?.total?.value?
          totalValues = response.body.hits.total.value
        else if response?.body?.hits?.total?
          totalValues = response.body.hits.total
        else
          totalValues = '[notAvailable]'

        @logger.debug "#{meth} Query found #{totalValues} results
                       (returned #{response.body.hits.hits.length})."

        return resolve response.body.hits.hits

      catch err
        @logger.error "#{meth} ERROR: Unexpected error searching events:
                       #{JSON.stringify err || {}}"
        # Promise was rejected but was changed as part of ticket 1323, to allow
        # users to continue managing deployments even if Elasticsearch is down.
        return resolve []


  cleanEvents: (rawList) ->
    cleanEvents = []
    for rawEvt in rawList
      cleanEvents.push rawEvt['_source']
    cleanEvents


module.exports = EventStoreElasticsearch
