###
* Copyright 2022 Kumori Systems S.L.

* Licensed under the EUPL, Version 1.2 or – as soon they
  will be approved by the European Commission - subsequent
  versions of the EUPL (the "Licence");

* You may not use this work except in compliance with the
  Licence.

* You may obtain a copy of the Licence at:

  https://joinup.ec.europa.eu/software/page/eupl

* Unless required by applicable law or agreed to in
  writing, software distributed under the Licence is
  distributed on an "AS IS" basis,

* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
  express or implied.

* See the Licence for the specific language governing
  permissions and limitations under the Licence.
###

q      = require 'q'

EventStore = require './event-store'

# Expected configuration options:
#   options:
#     serverURL: 'http://elasticsearch-jferrer.test.kumori.cloud:9200/'
#     username: ''
#     password: ''
#     index: 'kube-events-*'
#     maxResults: 200
class EventStoreK8s extends EventStore

  constructor: (options) ->
    super 'k8s'
    @k8sApiStub = options.k8sApiStub


  ##############################################################################
  ##                            PUBLIC METHODS                                ##
  ##############################################################################

  # Returns a promise
  init: () ->
    q true


  # Returns a promise
  terminate: () ->
    q true


  # Get events from Kubernetes cluster.
  #
  # IMPORTANT: current implementation ignores all filters except 'namespace'
  #
  # Filter parameters:
  # - namespace
  # - deployment (internal ID)
  # - fromDate  (formatted as "2021-05-17T09:00:00Z")
  # - toDate  (formatted as "2021-05-17T09:00:00Z")
  getDeploymentEvents: (namespace, deploymentID, fromDate, toDate) ->
    meth = "EventStoreK8s.getDeploymentEvents
            deploymentID: #{deploymentID} from: #{fromDate} to: #{toDate}"
    @logger.info "#{meth}"

    @k8sApiStub.getEvents namespace
    .then (nsEvents) =>
      @logger.debug "#{meth} Query returned #{nsEvents} results."
      return nsEvents
    .catch (err) =>
      @logger.error "#{meth} ERROR: Unexpected error getting events:
                     #{JSON.stringify err || {}}"
      # Promise was rejected but was changed as part of ticket 1323, to allow
      # users to continue managing deployments even if Elasticsearch is down.
      return []


  # Get events from Kubernetes cluster.
  #
  # IMPORTANT: current implementation ignores all filters except 'namespace'
  #
  # Filter parameters:
  # - namespace
  # - solution (internal ID)
  # - fromDate  (formatted as "2021-05-17T09:00:00Z")
  # - toDate  (formatted as "2021-05-17T09:00:00Z")
  getSolutionEvents: (namespace, solutionID, fromDate, toDate) ->
    meth = "EventStoreK8s.getSolutionEvents
            solutionID: #{solutionID} from: #{fromDate} to: #{toDate}"
    @logger.info "#{meth}"

    @k8sApiStub.getEvents namespace
    .then (nsEvents) =>
      @logger.debug "#{meth} Query returned #{nsEvents.length} results."
      return nsEvents
    .catch (err) =>
      @logger.error "#{meth} ERROR: Unexpected error getting events:
                     #{JSON.stringify err || {}}"
      # Promise was rejected but was changed as part of ticket 1323, to allow
      # users to continue managing deployments even if Elasticsearch is down.
      return []


module.exports = EventStoreK8s
