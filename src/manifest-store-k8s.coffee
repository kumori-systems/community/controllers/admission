###
* Copyright 2020 Kumori Systems S.L.

* Licensed under the EUPL, Version 1.2 or – as soon they
  will be approved by the European Commission - subsequent
  versions of the EUPL (the "Licence");

* You may not use this work except in compliance with the
  Licence.

* You may obtain a copy of the Licence at:

  https://joinup.ec.europa.eu/software/page/eupl

* Unless required by applicable law or agreed to in
  writing, software distributed under the Licence is
  distributed on an "AS IS" basis,

* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
  express or implied.

* See the Licence for the specific language governing
  permissions and limitations under the Licence.
###

q      = require 'q'

ManifestHelper = require './manifest-helper'
ManifestStore  = require './manifest-store'
KukuModel      = require './k8s-kuku-model/kuku-model'
K8sApiStub     = require './k8s-api-stub'
utils          = require './utils'


DEFAULT_CONFIG =
  k8sApi:
    kubeConfig:
      clusters: [{
        name: 'minikube-cluster'
        server: 'https://192.168.99.100:8443'
        caFile: '/home/jferrer/.minikube/ca.crt'
        skipTLSVerify: false
      }]
      users: [{
        name: 'minikube',
        certFile: '/home/jferrer/.minikube/client.crt'
        keyFile: '/home/jferrer/.minikube/client.key'
      },{
          name: 'my-user'
          password: 'my-password'
      }]
      contexts: [
        name: 'minikube'
        user: 'my-user'
        cluster: 'minikube-cluster'
      ]
      currentContext: 'minikube'


MOCK_NULL_MANIFEST =
  spec: "SPEC NULL MANIFEST"
  data: "MOCK NULL MANIFEST"


class ManifestStoreK8S extends ManifestStore

  constructor: (options) ->
    super 'K8S'
    meth = 'ManifestStoreK8S.constructor'
    @logger.info meth

    @config = options || DEFAULT_CONFIG

    # Will need:
    # - a stub for K8S API Server (or configuration for instantiating one)
    # - a KumoriManifest to/from K-Model converter (if done at this level)
    # - algorith to convert URN to K8S-K-Model "selector"
    @kukuModel = new KukuModel()

    if @config.k8sApiStub?
      @apiServer = @config.k8sApiStub
    else
      @apiServer = new K8sApiStub @config.k8sApi

    @manifestHelper = new ManifestHelper()


  init: ->
    meth = 'ManifestStoreK8S.init'
    @logger.info meth
    q.promise (resolve, reject) =>
      # Inititalize/test K8S API Server stub
      @apiServer.init()
      .then () =>
        @logger.info "#{meth} - K8s API Server Stub initialized."
        @logger.info "#{meth} - K8S Manifest Store initialized."
        resolve()
      .catch (err) =>
        errMsg = "Initializing K8S Manifest Store: #{err.message}"
        @logger.error "#{meth} - #{errMsg}"
        reject new Error errMsg


  terminate: (deleteStorage = false) ->
    # Nothing to do in Filew System storage
    q true


  # Expects an Object representing the manifest, *NOT* the JSON
  storeManifest: (manifest) ->
    meth = "ManifestStoreK8S.storeManifest
            #{manifest.name || manifest.ref?.name || '(no name)'}"
    @logger.info meth

    q.promise (resolve, reject) =>
      if not manifest
        reject new Error('Storing manifest: manifest is empty.')

      try
        kukuElement = @kukuModel.fromManifest manifest

        # console.log "**** KUKUELEMENT FROM MANIFEST: ****"
        # console.log JSON.stringify kukuElement, null, 2

        # Validate the KukuElement (throws error if validation fails)
        kukuElement.validate()

        # Make sure logger, helper, etc. are not included
        delete kukuElement.logger
        delete kukuElement.helper

        # @logger.debug "#{meth} - KukuElement from Manifest:
        #                #{JSON.stringify kukuElement}"


        @logger.info "#{meth} - Created KukuElement from manifest.Storing it..."

        @apiServer.saveKumoriElement kukuElement
        .then () =>
          @logger.info "#{meth} - Element successfully saved."
          resolve manifest.name
        .catch (err) =>
          errMsg = "Couldn't save manifest: #{err.message}"
          @logger.error "#{meth} - #{errMsg}"
          reject new Error errMsg
      catch err
        errMsg = "Couldn't save manifest: #{err.message}"
        @logger.error "#{meth} - #{errMsg}"
        reject new Error errMsg


  getManifest: (manifestURN) ->
    meth = "ManifestStoreK8S.getManifest #{manifestURN}"
    @logger.info meth
    return @getManifestByURN manifestURN


  getManifestByNameAndKind: (manifestName, kind) ->
    meth = "ManifestStoreK8S.getManifestByNameAndKind #{kind} #{manifestName}"
    @logger.info meth

    q.promise (resolve, reject) =>
      if not manifestName
        reject new Error('Getting manifest: manifestName parameter is empty.')
        return

      if not kind
        reject new Error('Getting manifest: kind parameter is empty.')
        return

      try
        plural = @kukuModel.helper.getPluralFromType kind
        if plural is null
          errMsg = "Unable to determine KukuElement plural for type #{kind}"
          @logger.error "#{meth} - ERROR: #{errMsg}"
          return reject new Error errMsg

        @apiServer.getKumoriElement plural, manifestName
        .then (element) =>
          if not element?
            throw new Error "Element not found."

          # Extract original manifest from KukuElement.
          # For backwards compatibility, the manifest is searched in two
          # different places:
          # - the 'kumori/manifest' annotation (deprecated)
          # - the 'spec.kumoriManifest' property
          if element.spec?.kumoriManifest?
            compressedManifest = element.spec.kumoriManifest
          else if element.metadata?.annotations?['kumori/manifest']?
            compressedManifest = element.metadata.annotations['kumori/manifest']
          else
            throw new Error "Original manifest not found in KukuElement."

          manifest = JSON.parse (@kukuModel.helper.decompressManifest \
            compressedManifest)
          resolve manifest
        .catch (err) =>
          errMsg = "Couldn't get manifest for #{plural} #{manifestName}:
                    #{err.message}"
          @logger.error "#{meth} - #{errMsg}"
          reject new Error errMsg
      catch err
        errMsg = "Couldn't get manifest for #{plural} #{manifestName}:
                  #{err.message}"
        @logger.error "#{meth} - #{errMsg}"
        reject new Error errMsg


  getManifestByURN: (manifestURN) ->
    meth = "ManifestStoreK8S.getManifestByURN #{manifestURN}"
    @logger.info meth
    if (manifestURN is null) or (manifestURN is 'null')
      @logger.info "#{meth} - Returning mock null manifest"
      return q MOCK_NULL_MANIFEST
    q.promise (resolve, reject) =>
      if not manifestURN
        reject new Error('Getting manifest: manifestURN parameter is empty.')
      try
        ref = @manifestHelper.urnToRef manifestURN
        name = ref.name
        domain = ref.domain
        plural = @kukuModel.helper.getPluralFromType ref.kind
        if plural is null
          errMsg = "Unable to determine KukuElement plural for type #{ref.kind}"
          @logger.error "#{meth} - ERROR: #{errMsg}"
          return reject new Error errMsg


        @getManifestByPluralNameDomain plural, name, domain, null
        .then (manifest) =>
          resolve manifest
        .catch (err) =>
          errMsg = "Couldn't get manifest for #{manifestURN}: #{err.message}"
          if err.message?.includes 'Element not found'
            @logger.info "#{meth} - #{errMsg}"
          else
            @logger.error "#{meth} - #{errMsg}"
          reject new Error errMsg
      catch err
        errMsg = "Couldn't get manifest for #{manifestURN}: #{err.message}"
        @logger.error "#{meth} - #{errMsg}"
        reject new Error errMsg


  getManifestByPluralNameDomain: (plural, name, domain, owner = null) ->
    meth = "ManifestStoreK8S.getManifestByPluralNameDomain
            #{plural}/#{name}/#{domain}"
    @logger.info meth
    q.promise (resolve, reject) =>
      if (not plural) or (not name) or (not domain)
        reject new Error('Getting manifest: any parameter is empty.')
      # Search a KukuElement of the proper type that have labels that match the
      # domain and name of the URN.
      try
        filter = {}
        filter['kumori/name']   = @manifestHelper.hashLabel name
        filter['kumori/domain'] = @manifestHelper.hashLabel domain
        if plural is 'kukudeployments' then plural = 'v3deployments'
        @listElementType plural, owner, filter
        .then (elements) =>
          if Object.keys(elements).length is 0
            throw new Error "Element not found."
          else if Object.keys(elements).length > 1
            throw new Error "Several elements found matching criteria."
          else
            # The map has only one element
            firstKey = Object.keys(elements)[0]
            manifest = elements[firstKey]
            resolve manifest
        .catch (err) =>
          errMsg = "Couldn't get manifest for #{plural}/#{name}/#{domain}:
                    #{err.message}"
          @logger.info "#{meth} - #{errMsg}"
          reject new Error errMsg
      catch err
        errMsg = "Couldn't get manifest for #{plural}/#{name}/#{domain}:
                  #{err.message}"
        @logger.error "#{meth} - #{errMsg}"
        reject new Error errMsg


  deleteManifest: (manifestURN) ->
    meth = "ManifestStoreK8S.deleteManifest #{manifestURN}"
    @logger.info meth
    return @deleteManifestByURN manifestURN


  deleteManifestByURN: (manifestURN) ->
    meth = "ManifestStoreK8S.deleteManifestByURN #{manifestURN}"
    @logger.info meth
    q.promise (resolve, reject) =>
      if not manifestURN
        reject new Error('Deleting manifest: manifest URN is empty.')

      try
        ref = @manifestHelper.urnToRef manifestURN
        name = ref.name
        plural = @kukuModel.helper.getPluralFromType ref.kind
        if plural is null
          errMsg = "Unable to determine KukuElement plural for type #{ref.kind}"
          @logger.error "#{meth} - ERROR: #{errMsg}"
          return reject new Error errMsg

        @logger.info "#{meth} - Deleting KukuElement #{name} (#{plural})..."
        @apiServer.deleteKumoriElement plural, name
        .then (element) =>
          @logger.info "#{meth} - Deleted KukuElement #{name} (#{plural})..."
          resolve manifestURN
        .catch (err) =>
          errMsg = "Couldn't delete manifest: #{err.message}"
          @logger.error "#{meth} - #{errMsg}"
          reject new Error errMsg
      catch err
        errMsg = "Couldn't delete manifest: #{err.message}"
        @logger.error "#{meth} - #{errMsg}"
        reject new Error errMsg


  deleteManifestByNameAndKind: (name, kind) ->
    meth = "ManifestStoreK8S.deleteManifestByNameAndKind #{kind} #{name}"
    @logger.info meth

    q.promise (resolve, reject) =>
      if not name
        reject new Error('Getting manifest: name parameter is empty.')
        return

      if not kind
        reject new Error('Getting manifest: kind parameter is empty.')
        return

      try
        plural = @kukuModel.helper.getPluralFromType kind
        if plural is null
          errMsg = "Unable to determine KukuElement plural for type #{kind}"
          @logger.error "#{meth} - ERROR: #{errMsg}"
          reject new Error errMsg
          return

        if plural is 'kukudeployments'
          plural = 'v3deployments'

        @apiServer.deleteKumoriElement plural, name
        .then (element) =>
          @logger.info "#{meth} - Deleted KukuElement"
          resolve name
        .catch (err) =>
          errMsg = "Couldn't delete manifest #{kind} #{name}: #{err.message}"
          @logger.error "#{meth} - #{errMsg}"
          reject new Error errMsg
      catch err
        errMsg = "Couldn't delete manifest #{kind} #{name}: #{err.message}"
        @logger.error "#{meth} - #{errMsg}"
        reject new Error errMsg
        return


  updateManifest: (manifest) ->
    meth = "ManifestStoreK8S.updateManifest #{manifest.name}"
    @logger.info meth
    # Right now, store overwrites any previously stored manifest
    @storeManifest manifest


  list: (owner = null, filter = {}, includePublic = false) ->
    meth = "ManifestStoreK8S.list Owner: #{owner}"
    @logger.info meth

    if owner? and ('kumori/owner' not of filter)
      filter['kumori/owner'] = @manifestHelper.hashLabel owner

    # TODO: This assumes this naming for element types is know at this level
    TYPES = [
      'cas'
      'certificates'
      'domains'
      'links'
      'ports'
      'secrets'
      'solutions'
      'v3deployments'
      'volumes'
    ]

    allElements = null

    @listElementTypes TYPES, owner, filter
    .then (elementDict) =>
      allElements = elementDict
      includePublic = true   # TODO: for testing
      if includePublic
        publicFilter = { 'kumori/public': "true" }
        @listElementTypes TYPES, null, publicFilter
        .then (publicElementsDict) =>
          for k,v of publicElementsDict
            # We don't care if there are duplicates since they would be the same
            allElements[k] = v
    .then () =>
      return allElements


  listElementTypes: (types = [], owner = null, filter = {}) ->
    meth = "ManifestStoreK8S.listElementTypes Types: #{types} Owner: #{owner}"
    @logger.info meth

    if not filter?
      filter = {}

    if owner? and ('kumori/owner' not of filter)
      filter['kumori/owner'] = @manifestHelper.hashLabel owner

    # List all basic KukuElements of the user or public
    allElements = {}
    promises = []

    for t in types
      do (t) =>
        promises.push ( @listElementType t, owner, filter
          .then (elements) =>
            # console.log "LISTED ELEMENTS:"
            for k,v of elements
              # console.log "   - #{k}"
              allElements[k] = v
            true
          .catch (err) =>
            @logger.error "#{meth} ERROR: Couldn't get elements of type #{t}:
                           #{err.message}"
            true
        )
    q.all promises
    .then () =>
      # Return a dictionary where the keys are the elements URNs and the values
      # are the elements metadata (owner and public)
      result = {}
      for k,v of allElements
        isPublic = (if v.public? then v.public else false)
        result[k] = { owner: v.owner, public: isPublic }
      @logger.info "#{meth} - Finished listing all types. Total elements:
                    #{Object.keys(result).length}"
      return result


  listElementType: (type, owner = null, filter = {}) ->
    meth = "ManifestStoreK8S.listElementType #{type} -
            OWNER: #{owner} - FILTER: #{JSON.stringify filter}"
    @logger.info meth

    if owner? and ('kumori/owner' not of filter)
      filter['kumori/owner'] = @manifestHelper.hashLabel owner

    labelSelector = @filterToLabelSelector filter

    q.promise (resolve, reject) =>
      plural = @kukuModel.helper.getPluralFromType type
      if plural is null
        errMsg = "Unable to determine KukuType plural for type #{type}"
        @logger.error "#{meth} - ERROR: #{errMsg}"
        reject new Error errMsg

      @apiServer.listKumoriElements plural, labelSelector
      .then (items) =>
        @logger.info "#{meth} - Found #{items.length} results."
        elements = {}
        for it in items

          # For backwards compatibility, the manifest is searched in two
          # different places:
          # - the 'kumori/manifest' annotation (deprecated)
          # - the 'spec.kumoriManifest' property
          if it.spec?.kumoriManifest?
            compressedManifest = it.spec.kumoriManifest
          else if it.metadata?.annotations?['kumori/manifest']?
            compressedManifest = it.metadata.annotations['kumori/manifest']
          else
            @logger.warn "#{meth} - Found item with no ECloud manifest:
                          #{JSON.stringify it.metadata}"
            # Skip it
            continue

          manifest = JSON.parse (@kukuModel.helper.decompressManifest \
            compressedManifest)

          # If object is marked for deletion, add that info to Kumori manifest
          if it.metadata?.deletionTimestamp?
            manifest.deletionTimestamp = it.metadata.deletionTimestamp

          if manifest.urn?
            elements[manifest.urn] = manifest
          else
            elements[manifest.name] = manifest

        resolve elements
      .catch (err) =>
        errMsg = "Failed to list #{plural} with selector #{labelSelector}:
                  #{err.message}"
        @logger.error "#{meth} - ERROR: #{errMsg}"
        reject new Error errMsg


  checkElement: (elementURN) ->
    meth = "ManifestStoreK8S.checkElement #{elementURN}"
    @logger.info meth
    return q.reject()
    # q.promise (resolve, reject) =>
    #   if not elementURN?.length
    #     reject new Error('Getting manifest: elementURN is empty.')
    #   else
    #     manifestLocator = path.join elementURN, filename
    #     @fetcher.check manifestLocator, 'skip'
    #     .then () ->
    #       resolve()
    #     .fail (err) ->
    #       reject err


  info: ->
    "#{@type} Manifest Store: K8s config=#{JSON.stringify @config.k8sApi}"


  escapeLabelValue: (str) ->
    LABEL_ESCAPED_CHARS =
      '@': '__arroba__'

    for k, v of LABEL_ESCAPED_CHARS
      str = str.split(k).join(v)
    return str


  # Takes a dictionnary and converts it to a Kubernetes LabelSelector.
  # Its support some special syntax in the filter:
  # - label exists (with any value):  <labelName>: '*'
  # - label not equals to value:      <labelName>: '!!<value>'
  filterToLabelSelector: (filter = {}) ->

    labelSelector = ''

    if filter?
      for k,v of filter
        if labelSelector isnt '' then labelSelector += ','

        # Add filter properties to selector
        if v is '*'
          # Only check if label exists
          labelSelector += k
        else if v.startsWith '!!'
          # It's a negative equality. Remove the '!!' and use '!=' operator
          cleanValue = v.substring 2
          labelSelector += k + '!=' + @escapeLabelValue(cleanValue)
        else
          labelSelector += k + '=' + @escapeLabelValue(v)

    return labelSelector


  # Temporary solution for dealing with the label renaming
  labelNameConverter: (labelName) ->
    LABEL_DICT = {
      "kumoriOwner"   : "kumori/owner"
      "name"          : "kumori/name"
      "domain"        : "kumori/domain"
    }

    if labelName of LABEL_DICT
      console.log ""
      console.log "*********************************************************"
      console.log "*********************************************************"
      console.log "FILTER WITH OLD LABEL: #{labelName}"
      console.log "*********************************************************"
      console.log "*********************************************************"
      console.log ""
      return LABEL_DICT[labelName]
    else
      return null


module.exports = ManifestStoreK8S
