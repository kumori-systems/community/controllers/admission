###
* Copyright 2022 Kumori Systems S.L.

* Licensed under the EUPL, Version 1.2 or – as soon they
  will be approved by the European Commission - subsequent
  versions of the EUPL (the "Licence");

* You may not use this work except in compliance with the
  Licence.

* You may obtain a copy of the Licence at:

  https://joinup.ec.europa.eu/software/page/eupl

* Unless required by applicable law or agreed to in
  writing, software distributed under the Licence is
  distributed on an "AS IS" basis,

* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
  express or implied.

* See the Licence for the specific language governing
  permissions and limitations under the Licence.
###

os      = require 'os'
fs      = require 'fs'
net     = require 'net'
tls     = require 'tls'
path    = require 'path'
util    = require 'util'
winston = require 'winston'
common  = require 'winston/lib/winston/common'

ECONNREFUSED_REGEXP = /ECONNREFUSED/
KEEP_ALIVE_INITIAL_DELAY = 60*1000
MAX_LOG_QUEUE = 10000


class WinstonLogstash extends winston.Transport

  constructor: (options) ->
    super options
    @options = options

    @options ?= {}

    @name       = 'logstash'
    @localhost  = @options.localhost ? os.hostname()
    @host       = @options.host ? '127.0.0.1'
    @port       = @options.port ? 28777
    @node_name  = @options.node_name ? process.title
    @pid        = @options.pid ? process.pid
    @retries    = -1

    # Default is infinite retries (value -1)
    @max_connect_retries =
      if ('number' is typeof @options.max_connect_retries)
        @options.max_connect_retries
      else
        -1
    @timeout_connect_retries =
      if ('number' is typeof @options.timeout_connect_retries)
        @options.timeout_connect_retries
      else
        1000

    # Support for winston build in logstash format
    # https:#github.com/flatiron/winston/blob/master/lib/winston/common.js#L149
    @logstash = @options.logstash ? false

    # SSL Settings
    @ssl_enable         = @options.ssl_enable ? false
    @ssl_key            = @options.ssl_key ? ''
    @ssl_cert           = @options.ssl_cert ? ''
    @ca                 = @options.ca ? ''
    @ssl_passphrase     = @options.ssl_passphrase ? ''
    @rejectUnauthorized = @options.rejectUnauthorized is true

    # Connection state
    @log_queue = []
    @connected = false
    @socket    = null

    # Miscellaneous @options
    @strip_colors  = @options.strip_colors ? false
    @label         = @options.label ? @node_name
    @meta_defaults = @options.meta ? {}

    # We want to avoid copy-by-reference for meta defaults, so make sure it's a
    # flat object.
    for p of @meta_defaults
      delete @meta_defaults[p] if (typeof @meta_defaults[p] is 'object')

    @connect()


  log: (level, msg, meta, callback) ->
    meta = winston.clone(meta ? {})
    log_entry = null
    callback ?= () ->

    for property of @meta_defaults
      meta[property] = @meta_defaults[property]

    if @silent
      return callback null, true

    if  @strip_colors
      msg = msg.stripColors

      # Get rid of colors on meta properties
      if typeof meta is 'object'
        for property in meta
          meta[property] = meta[property].stripColors

    log_entry = common.log {
      level: level
      message: msg
      node_name: @node_name
      meta: meta
      timestamp: @timestamp
      json: true
      label: @label
    }

    if @connected
      @sendLog log_entry, () =>
        @emit 'logged'
        callback null, true
    else if @log_queue.length < MAX_LOG_QUEUE
      @log_queue.push {
        message: log_entry
        callback: (err) =>
          @emit 'logged'
          callback null, true
      }
    else
      # This traces will be lost
      console.log 'WinstonLogstash - Loosing traces!!'


  connect: () ->
    return if @connected

    tryReconnect = true
    options = {}
    @retries++
    @connecting = true
    @terminating = false

    if @ssl_enable

      options =
        key: if @ssl_key? then fs.readFileSync(@ssl_key).toString() else null
        cert: if @ssl_cert? then fs.readFileSync(@ssl_cert).toString() else null
        passphrase: if @ssl_passphrase? then @ssl_passphrase else null
        rejectUnauthorized: @rejectUnauthorized is true

      # TODO: this was supposed to be a list of files.
      if @ca?
        caList = []
        if 'string' is typeof @ca and @ca isnt ''
          caList = [ fs.readFileSync(@ca).toString() ]
        else if Array.isArray @ca
          for item in @ca
            caList.push fs.readFileSync(item).toString()

        options.ca = caList
      else
        options.ca = null

      console.log 'WinstonLogstash - CONNECTING TLS SOCKET....'
      # console.log "WinstonLogstash - TLS OPTIONS: #{JSON.stringify options}"

      try
        @socket = new tls.connect @port, @host, options, () =>
          console.log 'WinstonLogstash - CONNECTED TLS SOCKET!'
          @socket.setEncoding 'UTF-8'
          @announce()
          @connecting = false
      catch e
        console.log "WinstonLogstash - ERROR!!!!! #{e} #{e.message}"

    else
      @socket = new net.Socket()

    @socket.on 'error'  , @onSocketError
    @socket.on 'timeout', @onSocketTimeout
    @socket.on 'connect', @onSocketConnect
    @socket.on 'close'  , @onSocketClose

    if not @ssl_enable
      @socket.connect @port, @host, () =>
        @announce()
        @connecting = false
        # Updating from:
        # https://github.com/jaakkos/winston-logstash/pull/48
        @socket.setKeepAlive true, KEEP_ALIVE_INITIAL_DELAY


  close: () ->
    @terminating = true
    if @connected and @socket
      @connected = false
      @socket.end()
      @socket.destroy()
      @socket = null


  announce: () ->
    @connected = true
    @flush()
    @close() if @terminating


  flush: () ->
    for queueItem in @log_queue
      @sendLog queueItem.message, queueItem.callback
      @emit 'logged'
    @log_queue.length = 0


  sendLog: (message, callback) ->
    callback = callback || () -> # Do nothing
    if @socket?
      try
        @socket.write message + '\n'
      catch err
        console.log "WinstonLogstash.sendLog - #{err.message}"
    else
      console.log 'WinstonLogstash.sendLog - @socket is null. Skipping send.'
      # Do not emit the error upwards since it might end up making a loop
    callback()


  onSocketConnect: () =>
    console.log 'WinstonLogstash - SOCKET CONNECTED!'
    @retries = 0


  onSocketTimeout: () =>
    if (@socket.readyState isnt 'open')
      @socket.destroy()


  onSocketError: (err) =>
    console.log "WinstonLogstash - SOCKET ERROR: #{err}", err
    @connecting = false
    @connected = false

    closedSocket = @socket
    @socket = null
    try
      if typeof closedSocket isnt 'undefined' and closedSocket isnt null
        closedSocket.destroy()
    catch err
      console.log 'WinstonLogstash.onSocketError - socket.destroy failed.', err

    @emit 'error', err


  onSocketClose: (had_error) =>

    return if @terminating

    @connected = false

    closedSocket = @socket
    @socket = null
    try
      if typeof closedSocket isnt 'undefined' and closedSocket isnt null
        closedSocket.destroy()
    catch err
      console.log 'WinstonLogstash.onSocketClose - socket.destroy failed.', err

    if (@max_connect_retries < 0 or
    @retries < @max_connect_retries)
      if not @connecting
        @emit 'error'
        , new Error('Transport disconnected. Trying to reconnect...')
        setTimeout () =>
          @connect()
        , @timeout_connect_retries
    else
      # Unable to connect to server, drop all queued messages, go into silent
      # mode and emit a warning upwards.
      @log_queue = []
      @silent = true
      @emit 'error'
      , new Error('Max retries reached, transport in silent mode, OFFLINE')


# Define a getter so that `winston.transports.Syslog`
# is available and thus backwards compatible.
winston.transports.Logstash = WinstonLogstash

module.exports = WinstonLogstash
