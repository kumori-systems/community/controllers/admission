###
* Copyright 2022 Kumori Systems S.L.

* Licensed under the EUPL, Version 1.2 or – as soon they
  will be approved by the European Commission - subsequent
  versions of the EUPL (the "Licence");

* You may not use this work except in compliance with the
  Licence.

* You may obtain a copy of the Licence at:

  https://joinup.ec.europa.eu/software/page/eupl

* Unless required by applicable law or agreed to in
  writing, software distributed under the Licence is
  distributed on an "AS IS" basis,

* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
  express or implied.

* See the Licence for the specific language governing
  permissions and limitations under the Licence.
###

CircularJSON = require 'circular-json'

# List of properties that should never be included in the stringification
GLOBAL_DO_NOT_ENCODE = [
  'doNotEncode'
  '_logger'
  '_parser'
]

# This filter function looks for a 'doNotStringify' property in the object being
# stringified (or its class). If that property exists, it is supposed to contain
# list of properties that should not be included when stringification ocurs.
# The filter also takes into account a global list of properties that should
# never be included in the stringification.
encodeFilter = (key, value) ->
  if GLOBAL_DO_NOT_ENCODE? and key in GLOBAL_DO_NOT_ENCODE
    return undefined
  if this.doNotEncode? and key in this.doNotEncode
    return undefined
  if this.constructor?.doNotEncode? \
  and key in this.constructor.doNotEncode
    return undefined
  return value


class JsonParser

  # Returns a JSON-stringified version of an object, taking into account a list
  # of properties to exclude defined in the object itself.
  #
  # obj: the object to be encoded
  # filter: if false, all properties will be included, no filtering applied
  encode: (obj, filter = true) ->
    if filter
      return CircularJSON.stringify obj, encodeFilter
    else
      return CircularJSON.stringify obj

  decode: (text) ->
    return CircularJSON.parse text
    # return JSON.parse text


module.exports = JsonParser
