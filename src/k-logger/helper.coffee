###
* Copyright 2022 Kumori Systems S.L.

* Licensed under the EUPL, Version 1.2 or – as soon they
  will be approved by the European Commission - subsequent
  versions of the EUPL (the "Licence");

* You may not use this work except in compliance with the
  Licence.

* You may obtain a copy of the Licence at:

  https://joinup.ec.europa.eu/software/page/eupl

* Unless required by applicable law or agreed to in
  writing, software distributed under the Licence is
  distributed on an "AS IS" basis,

* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
  express or implied.

* See the Licence for the specific language governing
  permissions and limitations under the Licence.
###

errors        = require './errors'
WinstonLogger = require './winston-logger'
ProxyLogger   = require './proxy-logger'


# Inject the `logger` property to the given classes or objects.
#
# When the logger is injected into a class, it can declare (through
# @_loggerDependencies function )other classes to  which inject the logger too
# in a recursive way (addDependencies function).
#
# * `items`: array of classes/objects where the default logger will be injected.
# * `singletonLogger`: (optional) winston-logger instance to use (otherwise,
#                      a factory will be used)
#
inject = (items, singletonLogger) ->
  addDependencies = (currentItems, item) ->
    if item? and item not in currentItems
      currentItems.push item
      if item._loggerDependencies?
        dependentItems = item._loggerDependencies()
        if dependentItems? and (dependentItems instanceof Array)
          for dependentItem in dependentItems
            addDependencies currentItems, dependentItem
  if not (items instanceof Array) then throw new Error errors.ARRAY_EXPECTED
  if (not singletonLogger?) or (not singletonLogger.constructor?) or \
     (singletonLogger.constructor.name isnt 'WinstonLogger')
    singletonLogger = WinstonLogger.getLogger()
  completedItems = []
  addDependencies completedItems, item for item in items
  for item in completedItems
    if typeof item is 'function'
      clazz = item
      do (clazz) ->
        # TO BE REVIEWED: This has been added to avoid errors when calling
        # inject twice.
        if not clazz.prototype.hasOwnProperty 'logger'
          setProperty [clazz], 'logger', () ->
            logger = new ProxyLogger(singletonLogger, clazz)
            logger
    else if typeof item is 'object'
      object = item
      do (object) ->
        if not object.logger?
          setProperty [object], 'logger', () ->
            logger = new ProxyLogger(singletonLogger, object.constructor.name)
            logger


# Defines a new property directly on an object
#
setProperty = (items, name, getDefault) ->
  if not (items instanceof Array)
    throw new Error errors.ARRAY_EXPECTED
  for item in items
    _name = '_' + name
    if typeof item is 'function'
      clazz = item
      Object.defineProperty clazz.prototype, name,
        get: ->
          if not this[_name]
            this[_name] = getDefault()
          return this[_name]
        set: (value) ->
          #if not parser instanceof Parser
          #    throw errors.PARSER_INVALID
          this[_name] = value
    else if typeof item is 'object'
      object = item
      Object.defineProperty object, name,
        get: ->
          if not this[_name]
            this[_name] = getDefault()
          return this[_name]
        set: (value) ->
          #if not parser instanceof Parser
          #    throw errors.PARSER_INVALID
          this[_name] = value


exports.inject            = inject
exports.setProperty       = setProperty
