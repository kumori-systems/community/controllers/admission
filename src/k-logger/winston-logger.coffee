###
* Copyright 2022 Kumori Systems S.L.

* Licensed under the EUPL, Version 1.2 or – as soon they
  will be approved by the European Commission - subsequent
  versions of the EUPL (the "Licence");

* You may not use this work except in compliance with the
  Licence.

* You may obtain a copy of the Licence at:

  https://joinup.ec.europa.eu/software/page/eupl

* Unless required by applicable law or agreed to in
  writing, software distributed under the Licence is
  distributed on an "AS IS" basis,

* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
  express or implied.

* See the Licence for the specific language governing
  permissions and limitations under the Licence.
###

winston = require 'winston'
uuid = require 'uuid'
path = require 'path'
fs = require 'fs'
q = require 'q'
_ = require 'lodash'

# This require makes 'winston.transports.Logstash' available
require './winston-logstash'

###
Configuration sample:
{
  'handleExceptions': false
  'auto-method': false
  'vm': 'aaaa'
  'context': 'bbbb'
  'transports': {
    'console': {
      'level': 'info'
      'colorize': true
      'prettyPrint': false
    }
    'file': {
      'level': 'info'
      'filename': 'slap.log'
      'maxSize': 50*1024*1024
      'maxFiles': 100
      'tailable': true
      'zippedArchive': false
    }
    'http': {
      'level': 'info'
      'host': null
      'port': null
    }
    'logstash': {
      'level': 'info'
      'max_connect_retries': 10
      'timeout_connect_retries': 1000
      'host': null
      'port': null
      'ssl_enable': false
      'ssl_cert': ''  # Path to client certificate
      'ssl_key': ''   # Path to client key
      'ca': [ '' ]        # Paths to server certificate CAs (to validate it)
      'ssl_passphrase': ''
      'rejectUnauthorized': false  # Reject unauthorized connections
    }
  }
}
###

LOGGER_CONFIG_PATH = 'config/logger-config.json'

DEFAULT_CONFIG = {
  'handleExceptions': false
  'vm' : 'not defined'
  'auto-method': false
  'transports': {
    'file': {
      'level' : 'info'
      'filename' : 'slap.log'
      'maxSize': 50*1024*1024
      'maxFiles': 100
      'tailable': true
      'zippedArchive': false
    }
  }
}


# WinstonLogger class extends winston.logger, simply configuring it
# using a config json file.
# This module exports a singleton instance.
#
class WinstonLogger extends winston.Logger


  constructor: (@loggerId) ->
    super { transports:[] }
    @_meta = {}
    @reconfigure(@_getDefaultConfig())
    @log 'info', "Winston Logger created (id=#{@loggerId})", {}
    @maxLevel = null
    @config = null

  getId: () ->
    return @loggerId


  log: (level, message, meta) ->
    try
      if (not @maxLevel?) or (@levels[level] <= @maxLevel)
        meta[key] = value for key, value of @_meta
        meta['slap_timestamp'] = new Date()
        super level, message, meta
    catch e
      console.log "Winston logger error: #{e.message}"


  addFixedMetaData: (dataName, dataValue) ->
    @_meta[dataName] = dataValue


  getConfig: () ->
    return @config


  reconfigure: (config)->
    @log 'info', "Winston Logger reconfiguring (id=#{@loggerId}) \
                  #{JSON.stringify config})", {}
    try
      if config is null then config = DEFAULT_CONFIG
      transports = []

      # Backward compatibility. Check if its an old-version configuration
      if not config.transports? then config = @_updateVersion(config)

      @config = config

      if config.transports['console']?
        options = _.cloneDeep config.transports['console']
        options['handleExceptions'] = config['handleExceptions'] ? false
        if not options['level']? then options['level'] = 'info'
        transports.push new winston.transports.Console(options)

      if config.transports['file']?
        options = _.cloneDeep config.transports['file']
        options['handleExceptions'] = config['handleExceptions'] ? false
        if not options['level']? then options['level'] = 'info'
        if not options['maxsize']? then options['maxsize'] = 50*1024*1024 #50MB
        if not options['maxFiles']? then options['maxFiles'] = '100'
        if not options['tailable']? then options['tailable'] = true
        if not options['zippedArchive']? then options['zippedArchive'] = false
        transports.push new winston.transports.File(options)

      if config.transports['http']?
        options = _.cloneDeep config.transports['http']
        options['handleExceptions'] = config['handleExceptions'] ? false
        if not options['level']? then options['level'] = 'info'
        transports.push new winston.transports.Http(options)

      if config.transports['logstash']?
        options = _.cloneDeep config.transports['logstash']
        options['handleExceptions'] = config['handleExceptions'] ? false
        if not options['level']? then options['level'] = 'info'
        if not options['max_connect_retries']?
          options['max_connect_retries'] = -1 # Infinite retries
        if not options['timeout_connect_retries']?
          options['timeout_connect_retries'] = 1000
        if not options['ssl_enable']?
          options['ssl_enable'] = false
        if not options['ssl_cert']?
          options['ssl_cert'] = ''
        if not options['ssl_key']?
          options['ssl_key'] = ''
        if not options['ca']?
          options['ca'] = [ '' ]
        if not options['ssl_passphrase']?
          options['ssl_passphrase'] = ''
        if not options['rejectUnauthorized']?
          options['rejectUnauthorized'] = false
        logstashTransport = new winston.transports.Logstash options

        logstashTransport.on 'error', (err) =>
          @log 'error', "Logstash Winston transport warning: #{err.message}", {}
        transports.push logstashTransport

      # Because Winston bug ...
      for key, transport of @transports
        if transport.name is 'file'
          transport._buffer.length = 0

      @configure { transports: transports }
      @autoMethod = config['auto-method'] ? false
      @addFixedMetaData 'vm', config['vm']
      if config['context']? then @addFixedMetaData 'context', config['context']
      else if not @_meta['context']? then @addFixedMetaData 'context', ''

      # Improve winston-level management
      for key, transport of @transports
        transportLevel = @levels[transport.level]
        if not @maxLevel? then @maxLevel = transportLevel
        else if transportLevel > @maxLevel then @maxLevel = transportLevel

      @log 'info', "Winston Logger reconfigured (id=#{@loggerId}, \
                    level=#{@maxLevel})", {}

    catch e
      @log 'error', "Winston Logger reconfiguring error: #{e.message}", {}


  _getDefaultConfig: () ->
    base = __dirname.split('/node_modules/')[0]
    tokens = base.split '/src'
    base = tokens[0] if tokens.length is 1
    path = base + '/' + LOGGER_CONFIG_PATH
    config = null
    try
      config = fs.readFileSync path, 'utf8'
      return JSON.parse(config)
    catch err
      console.log('Using default config')
      return DEFAULT_CONFIG


  _updateVersion: (config) ->
    updatedConfig = {
      transports: {}
    }
    updatedConfig['handleExceptions'] = config['handleExceptions'] ? false
    updatedConfig['vm'] = config['vm'] ? 'not defined'
    updatedConfig['auto-method'] = config['auto-method'] ? false
    updatedConfig['context'] = config['context']
    if config['console-log'] is true
      options = {}
      options['level'] = config['console-level'] ? 'info'
      options['colorize'] = config['colorize'] ? false
      options['prettyPrint'] = config['prettyPrint'] ? false
      updatedConfig.transports['console'] = options
    if config['file-log'] is true
      options = {}
      options['level'] = config['file-level'] ? 'info'
      options['filename'] = config['file-filename'] ? false
      options['maxSize'] = config['file-maxSize'] ? 50*1024*1024
      options['maxFiles'] = config['file-maxFiles'] ? 100
      options['zippedArchive'] = config['file-zipOldFiles'] ? false
      updatedConfig.transports['file'] = options
    if config['http-log'] is true
      options = {}
      options['level'] = config['http-level'] ? 'info'
      options['host'] = config['http-host']
      options['port'] = config['http-port']
      updatedConfig.transports['http'] = options
    if config['logstash-log'] is true
      options = {}
      options['level'] = config['logstash-level'] ? 'info'
      options['host'] = config['logstash-host']
      options['port'] = config['logstash-port']
      options['max_connect_retries'] =
        config['max_connect_retries'] ? -1 # Infinite retries
      options['timeout_connect_retries'] =
        config['timeout_connect_retries'] ? 1000
      options['ssl_enable'] = config['logstash-ssl_enable'] = false
      options['ssl_cert'] = options['logstash-ssl_cert'] ? ''
      options['ssl_key'] = options['logstash-ssl_key'] ? ''
      options['ca'] = options['logstash-ca'] ? [ '' ]
      options['ssl_passphrase'] = options['logstash-ssl_passphrase'] ? ''
      options['rejectUnauthorized'] =
        options['logstash-rejectUnauthorized'] ? false

      updatedConfig.transports['logstash'] = options
    return updatedConfig


instances = {}
singletonInstance = null
module.exports.getLogger = (loggerId) ->
  if loggerId?
    instance = instances[loggerId]
    if not instance?
      instance = new WinstonLogger(loggerId)
      instances[loggerId] = instance
      if not singletonInstance? then singletonInstance = instance
    return instance
  else
    if not singletonInstance?
      loggerId = uuid.v4()
      singletonInstance = new WinstonLogger(loggerId)
    return singletonInstance
