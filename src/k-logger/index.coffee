###
* Copyright 2022 Kumori Systems S.L.

* Licensed under the EUPL, Version 1.2 or – as soon they
  will be approved by the European Commission - subsequent
  versions of the EUPL (the "Licence");

* You may not use this work except in compliance with the
  Licence.

* You may obtain a copy of the Licence at:

  https://joinup.ec.europa.eu/software/page/eupl

* Unless required by applicable law or agreed to in
  writing, software distributed under the Licence is
  distributed on an "AS IS" basis,

* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
  express or implied.

* See the Licence for the specific language governing
  permissions and limitations under the Licence.
###

q    = require 'q'
uuid = require 'uuid'

helper          = require './helper'
errors          = require './errors'
JsonParser      = require './json-parser'
NullParser      = require './null-parser'
WinstonLogger   = require './winston-logger'
ProxyLogger     = require './proxy-logger'
WinstonLogstash = require './winston-logstash'

# Initial default parser.
defaultparser = new JsonParser()


# Get (and create if necessary) the singleton-instance of logger
#
getLogger = (clazzOrModuleName, loggerId) ->
  return new ProxyLogger(WinstonLogger.getLogger(loggerId), clazzOrModuleName)


# Set the owner of the singleton logger, a metadata fixed a singleton-logger
# level.
# Owner is a meta-data fixed a singleton-logger level. Represents the PROCESS
# (slap-agent, router-agent, a component instance...) that create
#
setLoggerOwner = (owner) ->
  if typeof owner is 'string'
    WinstonLogger.getLogger().addFixedMetaData 'owner', owner


# Inject the `logger` property to the given classes or objects.
#
# Parameters:
# * `items`: array of classes/objects where the default logger will be injected.
# * `singletonLogger`: (optional) winston-logger instance to use (otherwise,
#                      a factory will be used)
inject = (items, singletonLogger) ->
  helper.inject(items, singletonLogger)


# Inject method - old name
#
setLogger = (items, singletonLogger) ->
  helper.inject(items, singletonLogger)


# Sets/changes a configuration for logger
#
configureLogger = (config)->
  singletonLogger = WinstonLogger.getLogger()
  singletonLogger.reconfigure config


# Gets the parser injected to classes.
#
# Returns: the parser injected to classes.
getDefaultParser = ->
  return defaultparser


# Sets the parser injected to classes.
#
# Parameters:
# * `parser`: the new parser injected to classes.
setDefaultParser = (parser) ->
  defaultparser = parser


# Inject the `parser` property to the given classes or objects. The initial
# value will be the default parser.
#
# Parameters:
# * `items`: array of classes/objects where the default parser will be injected.
setParser = (items) ->
  if not (items instanceof Array) then throw new Error errors.ARRAY_EXPECTED
  # Avoid errors when calling setParser twice
  items2 = []
  for item in items
    if ( (typeof item is 'function') and
         (not item.prototype.hasOwnProperty 'parser') ) or
       ( (typeof item is 'object') and
         (not item.parser?) )
      items2.push item
  setProperty items2, 'parser', getDefaultParser


# Defines a new property directly on an object
#
setProperty = (items, name, getDefault) ->
  helper.setProperty(items, name, getDefault)


# Generates a random ID
#
# Returns: the new ID
generateId = ->
  uuid.v4()


exports.setLoggerOwner    = setLoggerOwner
exports.getLogger         = getLogger
exports.setLogger         = setLogger
exports.inject            = inject
exports.configureLogger   = configureLogger
exports.generateId        = generateId
exports.setProperty       = setProperty

# Exports our own transports
exports.WinstonLogstash   = WinstonLogstash

# For now, we include the parser in k-logger
exports.setParser         = setParser
exports.setDefaultParser  = setDefaultParser
exports.getDefaultParser  = getDefaultParser
exports.JsonParser        = JsonParser
exports.NullParser        = NullParser
