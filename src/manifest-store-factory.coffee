###
* Copyright 2022 Kumori Systems S.L.

* Licensed under the EUPL, Version 1.2 or – as soon they
  will be approved by the European Commission - subsequent
  versions of the EUPL (the "Licence");

* You may not use this work except in compliance with the
  Licence.

* You may obtain a copy of the Licence at:

  https://joinup.ec.europa.eu/software/page/eupl

* Unless required by applicable law or agreed to in
  writing, software distributed under the Licence is
  distributed on an "AS IS" basis,

* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
  express or implied.

* See the Licence for the specific language governing
  permissions and limitations under the Licence.
###

ManifestStoreFS     = require './manifest-store-fs'
ManifestStoreK8S    = require './manifest-store-k8s'

class ManifestStoreFactory

  # Static method to create new Manifest Stores
  @create: (type, options) ->
    switch type
      when 'k8s'
        new ManifestStoreK8S options
      when 'fs'
        new ManifestStoreFS options
      else
        throw new Error "Invalid ManifestStore type: '#{type}'"


module.exports = ManifestStoreFactory
