###
* Copyright 2022 Kumori Systems S.L.

* Licensed under the EUPL, Version 1.2 or – as soon they
  will be approved by the European Commission - subsequent
  versions of the EUPL (the "Licence");

* You may not use this work except in compliance with the
  Licence.

* You may obtain a copy of the Licence at:

  https://joinup.ec.europa.eu/software/page/eupl

* Unless required by applicable law or agreed to in
  writing, software distributed under the Licence is
  distributed on an "AS IS" basis,

* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
  express or implied.

* See the Licence for the specific language governing
  permissions and limitations under the Licence.
###

q        = require 'q'
fs       = require 'fs'
Git      = require 'simple-git'
url      = require 'url'
path     = require 'path'
rimraf   = require 'rimraf'
mkdirp   = require 'mkdirp'
needle   = require 'needle'
archiver = require 'archiver'
moment   = require 'moment'
EventEmitter = require('events').EventEmitter


ManifestHelper       = require './manifest-helper'
ManifestValidator    = require './manifest-validator-mock'
ManifestStoreFactory = require './manifest-store-factory'
utils                = require './utils'

MANIFEST_FILENAME      = 'Manifest.json'
METADATA_FILENAME      = 'metadata.json'
BUNDLES_FILENAME       = 'Bundles.json'
# DEFAULT_CODE_BLOB_NAME = 'code.zip'
DEFAULT_CODE_BLOB_NAME = 'image.tgz'

DOT = '.'
ZIP_EXT = '.zip'
TEMPORARY_CONTEXT_PREFIX = 'temporary'

NEEDLE_HTTP_OPTIONS =
  decode_response: false

# MUST BE OF LENGTH 4
RESOURCE_PREFIX   = 'reso'
DEPLOYMENT_PREFIX = 'depl'
BUNDLE_PREFIX     = 'bund'
BLOB_PREFIX       = 'blob'
TEST_PREFIX       = 'test'
LINK_PREFIX       = 'link'
UNKNOWN_PREFIX    = 'unkn'
KMV3_DEPL_PREFIX  = "kmv3"
SOLUTION_PREFIX   = "solu"

# Prefixes in `code` key in manifests which not require a blob registration.
CODE_NOT_REQUIRES_BLOB = [ 'docker:' ]

REGISTRATION_PRECEDENCE = [
  RESOURCE_PREFIX
  SOLUTION_PREFIX
  DEPLOYMENT_PREFIX
  KMV3_DEPL_PREFIX
  LINK_PREFIX
  TEST_PREFIX
  BUNDLE_PREFIX
  BLOB_PREFIX
  UNKNOWN_PREFIX
]



# FOR KUMORI V2, ONLY MANIFESTS WILL BE TAKEN INTO ACCOUNT, SINCE THERE WON'T BE
# CODE COMPONENT OR RUNTIME BLOBS ANYMORE.
# SERVICE BLOBS (CONTAINING SPREAD FUNCTIONS) WON'T BE SUPPORTED FOR V2.0, AND
# LATER SUPPORT WILL BE EVALUATED.
#
# - ManifestStore: now it's K8S API Server and etcd
# - The bundle zip will be recursively extracted to come up with a list of JSON
#   manifests.


class MockStorage
  constructor: () ->

  put: () ->
    console.log "MockStorage.put - THIS SHOULD NEVER BE CALLED"
    return q()

  deleteDir: () ->
    console.log "MockStorage.deleteDir - THIS SHOULD NEVER BE CALLED"
    return q()



class BundleRegistry extends EventEmitter

  constructor: (@storage, @manifestRepository, @maxBundleSize, @tmpPath)->
    super()
    meth = 'BundleRegistry.constructor'

    @validator = new ManifestValidator()
    @manifestHelper = new ManifestHelper()
    @storage ?= new MockStorage()

    @pstat = q.denodeify fs.stat
    @prename = q.denodeify fs.rename
    @preaddir = q.denodeify fs.readdir
    @primraf = q.denodeify rimraf
    @pmkdirp = q.denodeify mkdirp
    @pchmod = q.denodeify fs.chmod
    @pwritefile = q.denodeify fs.writeFile

    @folderCounter = 0
    @regTokenCounter = 0
    @regLog = {}
    @globalContexts = {}


  init: () ->
    @validator.init()


  close: ->
    if @storage?.close
      @storage.close()


  registerBundleJson: (context, jsonPath) ->
    @logger.info "Registering Bundle.json: #{jsonPath}..."
    regToken = @_initRegistration(context)
    workDirs = null
    @_initTemporaryDirs()
    .then (dirs) =>
      workDirs = dirs
      @_addWorkDirs regToken, workDirs
      @_processBundlesFile jsonPath, workDirs.raw, regToken
    .then () =>
      @processDirectory workDirs, regToken
    .then () =>
      @logger.debug "Bundle.json registration for #{jsonPath} finished."
      @_removeWorkDirs regToken
      return @regLog[regToken]
    .fail (err) =>
      @logger.warn "Registering Bundle.json: #{err.message}"
      @_addErrorMessage regToken, "Registering Bundles.json: #{err.message}"
      @_removeWorkDirs regToken
      q @regLog[regToken]
    .finally () =>
      @logger.debug 'Registration process finished, cleaning up temp files.'
      @_endRegistration regToken
      @_cleanup workDirs.top if workDirs?.top?



  registerBundleZip: (context, zipPath) ->
    @logger.info "Registering Bundle ZIP file: #{zipPath}..."
    @logger.info "Registering Bundle ZIP: CONTEXT: #{JSON.stringify context}..."
    regToken = @_initRegistration(context)
    workDirs = null
    @_initTemporaryDirs()
    .then (dirs) =>
      workDirs = dirs
      @_addWorkDirs regToken, workDirs
      @logger.info "Extracting zip file : #{zipPath} to #{workDirs.raw}..."
      utils.unzip zipPath, workDirs.raw, @maxBundleSize
    .then () =>
      @processDirectory workDirs, regToken
    .then () =>
      @logger.debug "ZIP Bundle registration for #{zipPath} finished."
      @_removeWorkDirs regToken
      return @regLog[regToken]
    .fail (err) =>
      @logger.warn "Registering zip bundle: #{err.message} #{err.stack}"
      @_addErrorMessage regToken, "Registering zip bundle: #{err.message}"
      @_removeWorkDirs regToken
      q @regLog[regToken]
    .finally () =>
      @logger.debug 'Registration process finished, cleaning up temp files.'
      @_endRegistration regToken
      @_cleanup workDirs.top if workDirs?.top?


  processDirectory: (workDirs, regToken) ->
    @preprocessDirectory workDirs, regToken
    .then () =>
      @processCleanDirectory workDirs, regToken



  ##############################################################################
  ##       PRIVATE METHODS - SHOULD NOT BE CALLED FROM OUTSIDE THE CLASS      ##
  ##############################################################################


  preprocessDirectory: (workDirs, regToken) ->
    @logger.debug "Pre-processing raw directory #{workDirs.raw} ..."
    currentDir = workDirs.raw
    topWorkDirs = @_getWorkDirs regToken

    # STEP 1: If Bundles.json, bring bundles to current dir
    bundlesFile = path.join currentDir, BUNDLES_FILENAME
    utils.checkFileExists bundlesFile
    .then (exists) =>
      if exists
        @logger.debug "Bundles.json file found, let's process it"
        @_processBundlesFile bundlesFile, currentDir
        .then () ->
          utils.deleteFile bundlesFile
        .then () =>
          @_checkDirSize topWorkDirs.raw, @maxBundleSize # Now, is too big?
      else
        q()
    .then () =>

      # STEP 2: If any ZIP files, extract them into temporary directories
      @_getDirContents currentDir
    .then (dirContents) =>
      promiseChain = q()
      for zipFile in dirContents.files when utils.endsWith zipFile, ZIP_EXT
        do (zipFile) =>
          promiseChain = promiseChain.finally () =>
            zipFilePath = path.join currentDir, zipFile
            tmpDir = null
            @_createTemporarySubdir currentDir, @_getFilenameNoExtension zipFile
            .then (_tmpDir) ->
              tmpDir = _tmpDir
              utils.getDirSize topWorkDirs.raw
            .then (topWorkDirRawSize) =>
              @logger.info "Extracting zip file: #{zipFilePath} to #{tmpDir}..."
              unzipMaxSize = @maxBundleSize - topWorkDirRawSize
              utils.unzip zipFilePath, tmpDir, unzipMaxSize
            .fail (err) =>
              message = "Extracting zip file: #{zipFilePath}: #{err.message}"
              @logger.warn message
              # Error is stored, and continues processing files
              @_addErrorMessage regToken, message
              if tmpDir? then @primraf(tmpDir) else q()
            .finally () ->
              utils.deleteFile zipFilePath
      promiseChain
    .then () =>

      # STEP 3: If there is a bundle, move it to the "clean" directory
      manifestPath = path.join currentDir, MANIFEST_FILENAME
      utils.checkFileExists manifestPath
      .then (exists) =>
        if exists
          filesToMove = [ MANIFEST_FILENAME ]
          manifest = null
          utils.jsonFromFile manifestPath
          .then (pmanifest) =>
            manifest = pmanifest
            if @_isSolution manifest
              @logger.debug "Detected solution manifest. Dont touch it!"
              @logger.debug "MANIFEST: #{JSON.stringify manifest}"
              q(null)
            else if @_isKmv3Deployment manifest
              @logger.debug "Detected kmv3 deployment manifest. Dont touch it!"
              @logger.debug "MANIFEST: #{JSON.stringify manifest}"
              q(null)
            else if not manifest.spec?
              q.reject new Error 'Manifest file has no required spec attribute.'
            else if @_requiresBlob manifest
              @_findCode manifest.code, currentDir
            else
              q(null)
          .then (codeBundleDir) =>
            filesToMove.push codeBundleDir if codeBundleDir?
            @_createTemporarySubdir workDirs.clean
            , (@_getPrefixFromType(manifest) + '_' + @folderCounter++)
          .then (cleanBundleDir) =>
            @_moveFiles filesToMove, currentDir, cleanBundleDir
          .fail (err) =>
            errMsg =
              if manifest?.name?
                "Moving bundle #{manifest.name}: #{err.message}"
              else
                "Moving bundle from #{manifestPath}: #{err.message}"
            @logger.warn errMsg, err.stack
            @_addErrorMessage regToken, errMsg
            q()
        else
          q()
    .then () =>

      # STEP 4: Walk all sub-directories and pre-pocess them recursively
      @_getDirContents currentDir
      .then (dirContents) =>
        promiseChain = q()
        for dir in dirContents.dirs when not utils.startsWith dir, DOT
          do (dir) =>
            promiseChain = promiseChain.finally () =>
              dirPath = path.join currentDir, dir
              subdirWorkDirs =
                raw: dirPath
                clean: workDirs.clean
              @preprocessDirectory subdirWorkDirs, regToken
        promiseChain.then () =>
          @logger.info "All subdirs explored. Deleting dir #{currentDir}."
          @_cleanup currentDir


  processCleanDirectory: (workDirs, regToken) ->
    @logger.info "Registering detected bundles in #{workDirs.clean}..."
    context =
      id: null
      regToken: regToken
    cleanDir = workDirs.clean
    @_getDirContents cleanDir
    .then (dirContents) =>
      if (dirContents.dirs.length is 0)
        @logger.info 'Processing clean directories: nothing to analyze.'
        return q()
      else
        # Sort the bundles according to a predefined order, to avoid
        # inconsistencies in complex bundles.
        dirContents.dirs.sort @_getPrecedenceSorter()

        for dir in dirContents.dirs when TEST_PREFIX is @_getCleanDirPrefix(dir)
          if not context.id?
            context = @_generateContext regToken
            @logger.debug "Created new temporary context: #{context.id}"

        promiseChain = q()
        for dir in dirContents.dirs
          do (dir) =>
            @logger.debug "Registering bundle in: #{dir}"
            promiseChain = promiseChain.finally () =>
              elementType = @_getCleanDirPrefix dir
              @logger.debug "ElementType: #{elementType}"
              fullDir = path.join cleanDir, dir
              @processElementFromDir elementType, fullDir, context
              .then () =>
                @_cleanup fullDir
              .fail (err) =>
                @logger.warn "Processing bundle in #{dir}: #{err.message}"
                @_addErrorMessage regToken
                , "Processing bundle in #{dir}: #{err.message}"
                q()
        promiseChain.then () =>
          @_setTemporaryContext regToken, context
          @logger.debug 'Bundle registration finished.'
          q()


  processElementFromDir: (type, dir, context) ->
    switch type
      when RESOURCE_PREFIX   then @registerElement dir, context
      when DEPLOYMENT_PREFIX then @deferElement dir, context, type
      when KMV3_DEPL_PREFIX  then @deferElement dir, context, type
      when SOLUTION_PREFIX   then @deferElement dir, context, type
      when LINK_PREFIX       then @deferElement dir, context, type
      when TEST_PREFIX       then @deferElement dir, context, type
      else
        @logger.error "Tried to register element of type: #{type}."
        q.reject new Error "Tried to register element of type: #{type}."


  _validateManifest: (manifest) ->
    data = manifest.name ? manifest.spec
    @logger.info "Validating manifest: #{data}"
    q.Promise (resolve, reject) =>
      try
        if @_isSolution manifest
          @logger.info 'Skipping validation of solution manifest'
        else if @_isKmv3Deployment manifest
          @logger.info 'Skipping validation of kmv3 deployment manifest'
        else
          @validator.validateManifest manifest
        resolve manifest
      catch e
        errMsg = 'Manifest failed validation: '
        if e.validationErrors?
          errMsg += e.validationErrors.toString()
        else
          errMsg += e.message
        @logger.warn "Manifest #{data} failed validation: #{errMsg}"
        reject new Error errMsg


  deferElement: (dir, context, type) ->
    @logger.debug "DeferElement: #{dir}"
    elementManifest = null
    elementManifestPath = path.join dir, MANIFEST_FILENAME
    utils.jsonFromFile elementManifestPath
    .then (manifest) =>
      elementManifest = manifest
      @_validateManifest elementManifest
    .then () =>
      manifest = @contextualizeManifest context, elementManifest
      if type is DEPLOYMENT_PREFIX
        @_addPendingDeployment context.regToken, manifest
        @logger.debug 'Added deployment to pending actions list'
      else if type is LINK_PREFIX
        @_addPendingLink context.regToken, manifest
        @logger.debug 'Added link services to pending actions list'
      else if type is TEST_PREFIX
        @_addPendingTest context.regToken, manifest
        @logger.debug 'Added test to pending actions list'
      else if type is SOLUTION_PREFIX
        @_addPendingSolution context.regToken, manifest
        @logger.debug 'Added solution to pending actions list'
      else if type is KMV3_DEPL_PREFIX
        @_addPendingDeployment context.regToken, manifest
        @logger.debug 'Added kmv3 deployment to pending actions list'
      else
        @logger.warn "Could not add pending action of type: #{type}"
      q()
    .fail (err) =>
      @logger.warn "Defering element in #{dir}: #{err.message}"
      @_addErrorMessage context.regToken
      , "Defering element in #{dir}: #{err.message}"
      q()


  registerElement: (dir, context, requiresCode = false) ->
    @logger.debug "RegisterElement: #{dir}"
    elementManifest = null

    elementManifestPath = path.join dir, MANIFEST_FILENAME
    utils.jsonFromFile elementManifestPath
    .then (manifest) =>
      elementManifest = manifest
      @_validateManifest elementManifest
    .then () =>
      @logger.debug "Registering element: #{elementManifest.name}"
      @elementAlreadyInStorage context, elementManifest
    .then (alreadyRegistered) =>
      if alreadyRegistered
        q.reject new Error "Element #{elementManifest.name} already in storage."
      else
        (if not elementManifest.code?.length
          if requiresCode
            q.reject new Error \
              "Element #{elementManifest.name} is missing required code value."
          else
            q(null)
        else
          # @getBlobZipFile elementManifest, dir
          @getBlobTgzFile elementManifest, dir, context
          .then (blobZipFile) =>
            if blobZipFile
              # Add a code locator to element manifest to facilitate
              # storing/retrieving the blob code from a storage.
              elementManifest.codelocator =
                elementManifest.name + '/' + blobZipFile.name
              @logger.debug \
                "Added codelocator to manifest: #{elementManifest.codelocator}"
              return blobZipFile.path
            else
              return q(null)
        )
    .then (blobZipFilePath) =>
      @storeElement context, elementManifest, blobZipFilePath
    .then (elementName) =>
      @logger.info "Registered element: #{elementName}"
      @_addRegisteredMessage context.regToken
      , "Registered element: #{elementName}"
      q()
    .fail (err) =>
      @logger.warn "Registering bundle in #{dir}: #{err.stack}"
      @_addErrorMessage context.regToken
      , "Registering bundle in #{dir}: #{err.message}"
      q()


  prefixURN: (prefix, urn) ->
    if prefix is null or prefix is ''
      return urn
    else
      return urn.replace '://', ('://' + prefix + '/')


  replaceAll: (str, find, replacement) ->
    return str.split(find).join(replacement)


  contextualizeManifest: (context, manifest) ->
    return manifest if not context.id?
    @logger.info "Contextualizing to prefix: #{context.storagePrefix}", context
    prefix = context.storagePrefix

    # For deployments, do not change their names, since they might be referenced
    # by test manifests, and it's a user generated name.
    if (not @_isDeploymentManifest manifest) \
    and (not @_isSolution manifest) \
    and (not @_isKmv3Deployment manifest)
      if manifest.name?
        context.substitutions[manifest.name] = @prefixURN prefix, manifest.name

      if manifest.codelocator?
        context.substitutions[manifest.codelocator] =
          @prefixURN prefix, manifest.codelocator

    # Update reference to previously contextualized elements
    manifestString = JSON.stringify manifest
    for subKey, subValue of context.substitutions
      manifestString = @replaceAll manifestString, subKey, subValue
    manifest = JSON.parse manifestString

    return manifest


  storeElement: (context, manifest, elementBlobPath = null) ->
    manifest = @contextualizeManifest context, manifest
    metadata = metadataFromContext @globalContexts[context.regToken]

    # Set element owner
    manifest.owner = metadata.owner

    if elementBlobPath?
      storageKey = manifest.codelocator ? manifest.name
      @logger.debug "Storing #{storageKey} with blob #{elementBlobPath}"
      utils.calculateChecksum elementBlobPath
      .then (hash) =>
        manifest.code = hash
        @manifestRepository.storeManifest manifest
      .then () =>
        @storage.put storageKey, elementBlobPath, manifest.code, false
      .then (info) ->
        q manifest.name
    else
      @logger.debug "Storing #{manifest.name}"
      @manifestRepository.storeManifest manifest
      .then () =>
        if @_isResource manifest
          @emit 'resource', {
            timestamp: utils.getTimestamp()
            owner: manifest.owner
            source: 'admission'
            entity: { resource: manifest.name }
            type: 'resource'
            name: 'resourcecreated'
            data: manifest
          }
        q manifest.name


  elementAlreadyInStorage: (context, manifest) ->
    manifest = @contextualizeManifest context, manifest
    @logger.debug "Checking if element #{manifest.name} is already in storage"
    # @manifestRepository.getManifest manifest.name
    @manifestRepository.checkElement manifest.name
    .then () ->
      q true
    .fail (err) ->
      q false


  getBlobTgzFile: (elementManifest, dir, context) ->
    blobZipFileName   = null
    blobZipFilePath   = null
    blobBundleDir     = null

    if not @_requiresBlob elementManifest
      return q(null)

    @_getDirContents dir
    .then (dirContents) =>
      if dirContents.dirs.length isnt 1
        q.reject new Error \
          "More than one blob bundle directory for #{elementManifest.name}"
      else
        blobBundleDir = path.join dir, dirContents.dirs[0]
        @_getDirContents blobBundleDir
    .then (dirContents) =>
      zipFiles = dirContents.files.filter (x) -> utils.endsWith x, ZIP_EXT

      blobManifest = null
      blobManifestPath = path.join blobBundleDir, MANIFEST_FILENAME
      utils.jsonFromFile blobManifestPath
      .then (manifest) =>
        blobManifest = manifest
        if blobManifest.hash?.trim().length
          # A hash attribute means that the code comes in a zip file
          if zipFiles.length < 1
            q.reject new Error \
              "Expected blob zip file not found for #{elementManifest.name}"
          else if zipFiles.length > 1
            q.reject new Error \
              "More than one blob zip file found for #{elementManifest.name}"
          else
            blobZipFileName = zipFiles[0]
            blobZipFilePath = path.join blobBundleDir, zipFiles[0]
            @_validateHash blobZipFilePath, blobManifest.hash
            .then (matches) =>
              if not matches
                q.reject new Error \
                  "Zip failed hash validation for #{elementManifest.name}"
              else
                @logger.debug 'CONVERTING ZIP BLOB TO TGZ BLOB'
                @logger.debug "ZIP : #{blobZipFilePath}"
                @logger.debug "NAME: #{blobZipFileName}"
                blobTgzFilePath =
                  blobZipFilePath.replace blobZipFileName
                  , DEFAULT_CODE_BLOB_NAME
                @logger.debug "TGZ : #{blobTgzFilePath}"
                @_zipToTgz blobZipFilePath, blobTgzFilePath, context
                .then () ->
                  return {
                    name: DEFAULT_CODE_BLOB_NAME
                    path: blobTgzFilePath
                  }
        else
          # No hash attribute means that the code comes in a folder
          if dirContents.dirs.length < 1
            q.reject new Error \
              "Expected blob folder not found for #{elementManifest.name}"
          else if dirContents.dirs.length > 1
            q.reject new Error \
              "More than one blob folder found for #{elementManifest.name}"
          else
            blobFolderPath = path.join blobBundleDir, dirContents.dirs[0]
            blobZipFileName = DEFAULT_CODE_BLOB_NAME
            blobZipFilePath = path.join blobBundleDir, DEFAULT_CODE_BLOB_NAME
            @_tgzDirectoryContents blobFolderPath, blobZipFilePath
            .then () ->
              return {
                name: blobZipFileName
                path: blobZipFilePath
              }


  getBlobZipFile: (elementManifest, dir) ->
    blobZipFileName   = null
    blobZipFilePath   = null
    blobBundleDir     = null

    @_getDirContents dir
    .then (dirContents) =>
      if dirContents.dirs.length isnt 1
        q.reject new Error \
          "More than one blob bundle directory for #{elementManifest.name}"
      else
        blobBundleDir = path.join dir, dirContents.dirs[0]
        @_getDirContents blobBundleDir
    .then (dirContents) =>
      zipFiles = dirContents.files.filter (x) -> utils.endsWith x, ZIP_EXT

      blobManifest = null
      blobManifestPath = path.join blobBundleDir, MANIFEST_FILENAME
      utils.jsonFromFile blobManifestPath
      .then (manifest) =>
        blobManifest = manifest
        if blobManifest.hash?.trim().length
          # A hash attribute means that the code comes in a zip file
          if zipFiles.length < 1
            q.reject new Error \
              "Expected blob zip file not found for #{elementManifest.name}"
          else if zipFiles.length > 1
            q.reject new Error \
              "More than one blob zip file found for #{elementManifest.name}"
          else
            blobZipFileName = zipFiles[0]
            blobZipFilePath = path.join blobBundleDir, zipFiles[0]
            @_validateHash blobZipFilePath, blobManifest.hash
            .then (matches) ->
              if not matches
                q.reject new Error \
                  "Zip failed hash validation for #{elementManifest.name}"
              else
                return {
                  name: blobZipFileName
                  path: blobZipFilePath
                }
        else
          # No hash attribute means that the code comes in a folder
          if dirContents.dirs.length < 1
            q.reject new Error \
              "Expected blob folder not found for #{elementManifest.name}"
          else if dirContents.dirs.length > 1
            q.reject new Error \
              "More than one blob folder found for #{elementManifest.name}"
          else
            blobFolderPath = path.join blobBundleDir, dirContents.dirs[0]
            blobZipFileName = DEFAULT_CODE_BLOB_NAME
            blobZipFilePath = path.join blobBundleDir, DEFAULT_CODE_BLOB_NAME
            @_zipDirectoryContents blobFolderPath, blobZipFilePath
            .then () ->
              return {
                name: blobZipFileName
                path: blobZipFilePath
              }

  listTestContexts: (context, isAdmin) ->
    tContexts = {}
    @manifestRepository.list()
    .then (manifests)=>
      for m in manifests
        continue if not m.startsWith 'temporary'
        c = m.split('/')[1]
        if not tContexts[c]?
          tContexts[c] = false
        if m.endsWith 'metadata.json'
          tContexts[c] = m
      promise = q true
      if not isAdmin
        promises = []
        for k,v of tContexts
          continue if not v
          pathT = "eslap://#{path.dirname v}"
          do (k)=>
            promises.push \
              ((@manifestRepository.getManifest pathT, 'metadata.json')
              .then (metadata)->
                return if not metadata?
                if not (metadata.owner is context.user.id)
                  delete tContexts[k])
        promise = q.all promises
      promise
    .then ->
      (k for k of tContexts)


  releaseTestContext: (context, contextURN, isAdmin) ->
    @logger.info "Releasing test context #{contextURN}"
    promise = q true
    if not isAdmin
      pathT = contextURN.split('//')[1]
      promise = @manifestRepository.list()
      .then (manifests)=>
        for m in manifests
          continue if not m.startsWith pathT
          continue if not m.endsWith 'metadata.json'
          pathT = "eslap://#{path.dirname m}"
          return @manifestRepository.getManifest pathT, 'metadata.json'
        return false
      .then (metadata)->
        return if not metadata?
        if not (metadata.owner is context.user.id)
          q.reject new Error "Unauthorized access to #{contextURN}"
    promise
    .then =>
      @storage.deleteDir contextURN
    .then =>
      @manifestRepository.fetcher.deleteDir contextURN


  ##############################################################################
  ##  Class helpers methods                                                   ##
  ##############################################################################

  # This private method is used to determine if a code blob must be stored in
  # the internal storage or not. This is supposed to change
  _requiresBlob: (elementManifest) ->
    # The element will not require a code blob if there isn't a code key in its
    # manifest
    if not elementManifest?.code?
      return false
    # The element will not require a code blob if the code is stored in a docker
    # image
    for prefix in CODE_NOT_REQUIRES_BLOB
      return false if 'string' isnt typeof elementManifest.code
      return false if elementManifest.code.startsWith(prefix)
    # Otherwise, the element require a code blob
    return true

  # Create a new log for a registration operation, associated with a token
  _initRegistration: (globalContext) ->
    newToken = @regTokenCounter++
    @regLog[newToken] =
      registered: []
      errors: []
      pendingActions: {
        solutions: []
        deployments: []
        links: []
        tests: []
      }
      temporaryContext: null
    # console.log "ADDING TO GLOBALCONTEXTS[#{newToken}]"
    @globalContexts[newToken] = globalContext
    return newToken


  # Dispose the registration log associated with a token
  _endRegistration: (regToken) ->
    delete @regLog[regToken]
    delete @globalContexts[regToken]

  # Add an error message to a registration log identified by the token
  _addErrorMessage: (regToken, errorMessage) ->
    @regLog[regToken].errors.push errorMessage


  # Add a "registered" message to a registration log identified by the token
  _addRegisteredMessage: (regToken, message) ->
    @regLog[regToken].registered.push message


  # Add a "pending" test manifest to a registration log identified by the token
  _addPendingTest: (regToken, manifest) ->
    @regLog[regToken].pendingActions.tests.push manifest


  # Add a "pending" solution manifest to a registration log
  _addPendingSolution: (regToken, manifest) ->
    @regLog[regToken].pendingActions.solutions.push manifest


  # Add a "pending" deployment manifest to a registration log
  _addPendingDeployment: (regToken, manifest) ->
    @regLog[regToken].pendingActions.deployments.push manifest


  # Add a "pending" link manifest to a registration log
  _addPendingLink: (regToken, manifest) ->
    @regLog[regToken].pendingActions.links.push manifest


  # Set a temporary context for all registry operations
  _setTemporaryContext: (regToken, context) ->
    if context.id?
      @regLog[regToken].temporaryContext = context
    else
      @regLog[regToken].temporaryContext = null


  _addWorkDirs: (regToken, workDirs) ->
    @regLog[regToken]._workDirs = workDirs


  _removeWorkDirs: (regToken) ->
    if @regLog[regToken]?._workDirs?
      delete @regLog[regToken]._workDirs


  _getWorkDirs: (regToken) ->
    return @regLog[regToken]?._workDirs


  # Cleans up a temporary directory
  _cleanup: (tmpWorkDir) ->
    @logger.info "Deleting temporary directory #{tmpWorkDir}"
    @primraf tmpWorkDir


  # Creates a set of temporary working directories
  _initTemporaryDirs: () ->
    tmpDir      = path.join @tmpPath, utils.generateId()
    @logger.info "Creating temporary working directory: #{tmpDir}..."
    tmpDirRaw   = path.join tmpDir, 'raw'
    tmpDirClean = path.join tmpDir, 'clean'
    @pmkdirp tmpDir
    .then () =>
      @pmkdirp tmpDirRaw
    .then () =>
      @pmkdirp tmpDirClean
    .then () ->
      {
        top: tmpDir
        raw: tmpDirRaw
        clean: tmpDirClean
      }

  # Create a temporary sub-directory of the given parent. The name is a
  # random string, which can be prefixed by a supplied string.
  _createTemporarySubdir: (parentDir, prefix = null) ->
    if prefix?
      tmpDir = path.join parentDir, (prefix + '_' + utils.generateId())
    else
      tmpDir = path.join parentDir, utils.generateId()
    @pmkdirp tmpDir
    .then () =>
      @logger.debug "Created temporary subdir: #{tmpDir}..."
      return tmpDir


  # Generates a UUID without dashes (for elasticsearch search compatibility)
  _generateContext: (regToken) ->
    uuid = utils.generateId()
    uuid = @replaceAll uuid, '-', ''

    context =
      id: uuid
      storagePrefix: path.join TEMPORARY_CONTEXT_PREFIX, uuid
      substitutions: {}
      regToken: regToken

    return context


  # Given a path, it returns the filename without the extension.
  _getFilenameNoExtension: (filename) ->
    path.parse(filename).name


  # Returns a prefix corresponding to an element type according to its manifest.
  _getPrefixFromType: (manifest) ->
    return SOLUTION_PREFIX if @_isSolution manifest
    return KMV3_DEPL_PREFIX if @_isKmv3Deployment manifest
    return RESOURCE_PREFIX if @_isResource manifest
    return LINK_PREFIX if @_isLinkManifest manifest
    return '' if not manifest?.spec?
    return DEPLOYMENT_PREFIX if @_isDeploymentManifest manifest
    return BUNDLE_PREFIX if @_isBundleManifest manifest
    return BLOB_PREFIX if @_isBlobManifest manifest
    return TEST_PREFIX if @_isTestManifest manifest
    return UNKNOWN_PREFIX


  # Returns a sorting function for ordering elements according to their
  # precedence in the registration process.
  _getPrecedenceSorter: () ->
    return (x, y) =>
      xPrefix = @_getCleanDirPrefix x
      yPrefix = @_getCleanDirPrefix y
      orderX = REGISTRATION_PRECEDENCE.indexOf(xPrefix)
      orderY = REGISTRATION_PRECEDENCE.indexOf(yPrefix)

      if orderX < orderY
        return -1
      else if orderX > orderY
        return 1
      else if x < y
        return -1
      else if x > y
        return 1
      else
        return 0


  _getCleanDirPrefix: (dirname) ->
    try
      prefix = dirname.slice(0,4)
      if prefix in REGISTRATION_PRECEDENCE
        return prefix
      else
        return UNKNOWN_PREFIX
    catch err
      return UNKNOWN_PREFIX


  # TODO: These validations should be a little more sophisticated in the future,
  # like spec version checks or even JSON validation with corresponding schemas.
  _isDeploymentManifest: (manifest) ->
    manifestType = @manifestHelper.getManifestType manifest
    return (manifestType is ManifestHelper.DEPLOYMENT)

  _isSolution: (manifest) ->
    return @manifestHelper.isSolution manifest

  _isKmv3Deployment: (manifest) ->
    return @manifestHelper.isKmv3Deployment manifest

  _isLinkManifest: (manifest) ->
    manifestType = @manifestHelper.getManifestType manifest
    return (manifestType is ManifestHelper.LINK)

  _isResource: (manifest) ->
    manifestType = @manifestHelper.getManifestType manifest
    return (manifestType is ManifestHelper.RESOURCE)

  _isBundleManifest: (manifest) ->
    manifestType = @manifestHelper.getManifestType manifest
    return (manifestType is ManifestHelper.BUNDLE)

  _isBlobManifest: (manifest) ->
    manifestType = @manifestHelper.getManifestType manifest
    return (manifestType is ManifestHelper.BLOB)

  _isTestManifest: (manifest) ->
    manifestType = @manifestHelper.getManifestType manifest
    return (manifestType is ManifestHelper.TEST)


  # Clones the git repository of the URL to a specific directory.
  _gitCloneRepository: (repositoryURL, dstPath) ->
    q.Promise (resolve, reject) ->
      git = new Git dstPath
      git
      .silent true
      .clone repositoryURL, dstPath, (error) ->
        if error
          reject error
        else
          resolve()


  # Switch to specified branch in a local git repository
  _gitCheckout: (repository, branch) ->
    q.Promise (resolve, reject) ->
      repository.checkout branch, (error) ->
        if error
          reject error
        else
          resolve branch


  # Returns two lists (files and sub-directories) based on the contents of the
  # given directory.
  _getDirContents: (dir) ->
    fileList = []
    directoryList = []
    @preaddir dir
    .then (elements) =>
      promiseList = []
      for el in elements
        do (el) =>
          elementPath = path.join dir, el
          promiseList.push (\
            @pstat elementPath
            .then (fileStats) ->
              if fileStats.isFile()
                fileList.push el
              else if fileStats.isDirectory()
                directoryList.push el
            )
      q.all promiseList
    .then () ->
      return {
        files: fileList
        dirs: directoryList
      }


  # Move the contents of a directory to another directory
  _moveDirContents: (srcDir, dstDir) ->
    @pmkdirp dstDir
    .then () =>
      @preaddir srcDir
    .then (elements) =>
      promiseList = []
      for el in elements
        do (el) =>
          srcElement = path.join srcDir, el
          dstElement = path.join dstDir, el
          promiseList.push(@prename srcElement, dstElement)
      q.all promiseList


  # Move a list of files or sub-directories from a directory to another
  _moveFiles: (fileList, srcDir, dstDir) ->
    @pmkdirp dstDir
    .then () =>
      promiseList = []
      for file in fileList
        do (file) =>
          srcElement = path.join srcDir, file
          dstElement = path.join dstDir, file
          promiseList.push(@prename srcElement, dstElement)
      q.all promiseList


  # Verifies that a file HSA1 hash is the expected one
  _validateHash: (file, hash) ->
    utils.calculateChecksum file
    .then (checksum) ->
      return checksum.toUpperCase() is hash.toUpperCase()


  # Add the contents of a directory to a new zip file
  _zipDirectoryContents: (dir, newZipFile) ->
    @logger.info "Zipping contents of directory #{dir} to #{newZipFile}"
    q.Promise (resolve, reject) =>
      @pstat dir
      .then (stats) ->
        try
          if not stats.isDirectory()
            reject new Error 'Expected a directory, got something else.'
          else
            newZip = archiver.create 'zip', {}
            newZip.directory(dir + '/', '')
            newZip.pipe fs.createWriteStream newZipFile
            .on 'error', (err) ->
              reject err
            .on 'finish', () ->
              resolve()

            newZip.finalize()
        catch error
          reject err


  # Add the contents of a directory to a new zip file
  _tgzDirectoryContents: (dir, newTgzFile) ->
    @logger.info "TGZing contents of directory #{dir} to #{newTgzFile}"
    q.Promise (resolve, reject) =>
      @pstat dir
      .then (stats) ->
        if not stats.isDirectory()
          reject new Error 'Expected a directory, got something else.'
        else
          # files = dir + '/*'
          utils.packDirectory dir, newTgzFile
          .then () ->
            resolve()
      .fail (err) ->
        reject new Error "TGZing directory: #{err.message}"


  # Transform a ZIP file to a TGZ file
  # The operation should not exceed the maximum size of the working directory
  _zipToTgz: (srcZip, dstTgz, context) ->
    @logger.debug "Converting #{srcZip} to #{dstTgz}"
    tmpDir = null
    cleanDir = @_getWorkDirs(context.regToken).clean
    parentDirSize = null
    utils.getDirSize cleanDir
    .then (_parentDirSize) =>
      parentDirSize = _parentDirSize
      @_createTemporarySubdir @tmpPath
    .then (dir) =>
      tmpDir = dir
      @logger.info "Extracting zip file : #{srcZip} to #{tmpDir}..."
      unzipMaxSize = @maxBundleSize - parentDirSize
      utils.unzip srcZip, tmpDir, unzipMaxSize
    .then () =>
      @logger.debug "Extracted #{srcZip}"
      @_tgzDirectoryContents tmpDir, dstTgz
    .then () =>
      @logger.debug "Created #{dstTgz}"
    .finally () =>
      @_cleanup tmpDir


  # Bring to a destination directory the files specified by a git repository,
  # a branch and a sub-folder.
  _getFromGit: (gitConfig, dstPath, regToken) ->
    repo   = gitConfig.repository
    branch = gitConfig.branch ? 'master'
    subdir = gitConfig.subdir ? null
    @logger.info "Cloning #{repo}##{branch}/#{subdir} to #{dstPath}..."

    tmpCloneDir = path.join dstPath, 'fullRepo'
    @pmkdirp tmpCloneDir
    .then () =>
      @_gitCloneRepository repo, tmpCloneDir
    .then () =>
      @logger.debug "Repository #{repo} cloned to #{tmpCloneDir}"
      newRepo = new Git tmpCloneDir
      @_gitCheckout newRepo, branch
    .then (data) =>
      @logger.debug "Repository #{repo} switched to branch #{branch}"
      if subdir?
        fullSubdir = path.join tmpCloneDir, subdir
      else
        fullSubdir = tmpCloneDir
      @_moveDirContents fullSubdir, dstPath
    .then () =>
      @_cleanup tmpCloneDir
    .fail (err) =>
      @logger.warn "Cloning repository #{repo}: #{JSON.stringify err}"
      @_addErrorMessage regToken
      , "Cloning repository #{repo}: #{JSON.stringify err}"
      q()


  # Download to a destination directory the file specified by the URL
  _downloadHttpFile: (fileURL, dstPath, regToken) ->
    @logger.info "Downloading file #{fileURL} to #{dstPath}..."
    q.Promise (resolve, reject) =>
      try
        needleOptions =
          follow_max: 5
        stream = needle.get fileURL, needleOptions
        stream
        .on 'error', (err) ->
          reject err
        .on 'header', (statusCode, headers) =>
          if statusCode not in [ 200, 301, 302, 303 ]
            reject new Error "HTTP returned #{statusCode}."
          else
            parsed = url.parse fileURL
            filename = path.basename parsed.pathname
            dstFile = path.join dstPath, filename
            @logger.debug "File destination: #{dstFile}"
            out = fs.createWriteStream dstFile, NEEDLE_HTTP_OPTIONS
            stream.pipe out
            .on 'error', (err) ->
              reject err
            .on 'finish', resolve
      catch error
        reject error
    .fail (err) =>
      @logger.warn "Downloading file #{fileURL}: #{err.message}"
      @_addErrorMessage regToken
      , "Downloading file #{fileURL}: #{err.message}"
      q()


  # Takes a Bundles.json file and bring all the bundles referenced inside to
  # a local temporary directory.
  _processBundlesFile: (bundlesFile, workDir, regToken) ->
    @logger.info "Processing Bundles.json: #{bundlesFile}..."
    utils.jsonFromFile bundlesFile
    .then (bundleData) =>
      @logger.debug "Bundles data: #{JSON.stringify bundleData}"
      if not bundleData.spec? or not @_isBundleManifest bundleData.spec
        q.reject new Error 'Bundle.json file has no required spec attribute.'

      promises = []
      if bundleData.bundles?.length > 0
        for bun in bundleData.bundles
          if bun.repository?
            tmpBundleDir =
              path.join workDir, "bundle_#{utils.generateId()}"
            promises.push \
              @_getFromGit bun, tmpBundleDir, regToken
          else if bun.site?
            tmpBundleDir =
              path.join workDir, "bundle_#{utils.generateId()}"
            promises.push \
              @_downloadHttpFile bun.site, workDir, regToken
          else
            @logger.warn "Incorrect bundle definition: #{JSON.stringify bun}"
            @_addErrorMessage regToken
            , "Incorrect bundle definition: #{JSON.stringify bun}"
            promises.push q()
      q.all promises
    .fail (err) =>
      @logger.warn "Processing Bundles.json: #{err.message}"
      @_addErrorMessage regToken
      , "Processing Bundles.json: #{err.message}"
      q()


  # Looks in the 'dstPath' directory for a blob bundle with the name 'codeName'
  _findCode: (codeName, dirPath, regToken) ->
    @logger.info "Looking for a blob bundle named '#{codeName}'..."

    return q(null) if not codeName?.trim().length > 0

    # Look for Blob bundle that matches the name
    @_getDirContents dirPath
    .then (dirContents) =>
      codeBundleDir = null
      promiseChain = q()
      # List sub-directories, excluding hidden ones (for example .git)
      for dir in dirContents.dirs when not utils.startsWith dir, DOT
        do (dir) =>
          promiseChain = promiseChain.finally () =>
            # If blob bundles has already be found, skip this sub-dir
            return q() if codeBundleDir?
            manifestFile = path.join dirPath, dir, MANIFEST_FILENAME
            utils.checkFileExists manifestFile
            .then (exists) =>
              if exists
                utils.jsonFromFile manifestFile
                .then (manifest) =>
                  if @_isBlobManifest(manifest) and manifest.name is codeName
                    codeBundleDir = dir
            .fail (err) =>
              @logger.warn "Getting code for '#{codeName}: #{err.message}"
              @_addErrorMessage regToken
              , "Getting code for '#{codeName}: #{err.message}"
              q()
      promiseChain.then () =>
        if not codeBundleDir?
          q.reject new Error \
            "Unable to find blob bundle matching the code value #{codeName}"
        else
          @logger.info "Found blob bundle '#{codeName}' at #{codeBundleDir}"
          return codeBundleDir


  _checkDirSize: (dir, maxSize) ->
    utils.getDirSize dir
    .then (currentSize) ->
      if currentSize > maxSize
        q.reject new Error "Directory #{dir} exceeds max size (#{maxSize})"
      else
        q.resolve currentSize


metadataFromContext = (context)->
  # console.log "???????????????????????????????????????????????'"
  # console.log "???????????????????????????????????????????????'"
  # console.log "BUNDLEREGISTRY.metadataFromContext CONTEXT: #{JSON.stringify context, null, 2}"
  # console.log "???????????????????????????????????????????????'"
  # console.log "???????????????????????????????????????????????'"
  owner: context.user.id
  created: (new Date).toISOString()


  # Function to test the element precedence sorter
  _testDirectorySorter: () ->
    # console.log 'SORTED DIRS TO PROCESS: ', dirContents.dirs
    test = [
      'comp_1_c62c2b62-49ed-4c0f-b27c-6a435c96caf7',
      'comp_3_c62c2b62-49ed-4c0f-b27c-6a435c96caf7',
      'comp_0_c62c2b62-49ed-4c0f-b27c-6a435c96caf7',
      'runt_1_c62c2b62-49ed-4c0f-b27c-6a435c96caf7',
      'runt_2_c62c2b62-49ed-4c0f-b27c-6a435c96caf7',
      'runt_2_c62c2b62-49ed-4c0f-b27c-6a435c96caf7',
      'comp_1_c62c2b62-49ed-4c0f-b27c-6a435c96caf7',
      'reso_2_c62c2b62-49ed-4c0f-b27c-6a435c96caf7',
      'depl_0_c62c2b62-49ed-4c0f-b27c-6a435c96caf7',
      'reso_0_c62c2b62-49ed-4c0f-b27c-6a435c96caf7',
      'serv_0_c62c2b62-49ed-4c0f-b27c-6a435c96caf7',
      'comp_9_c62c2b62-49ed-4c0f-b27c-6a435c96caf7'
    ]
    # console.log 'TEST: ', test
    test.sort @_getPrecedenceSorter()
    # console.log 'TEST: ', test



module.exports = BundleRegistry
