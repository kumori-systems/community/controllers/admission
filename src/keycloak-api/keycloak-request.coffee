###
* Copyright 2022 Kumori Systems S.L.

* Licensed under the EUPL, Version 1.2 or – as soon they
  will be approved by the European Commission - subsequent
  versions of the EUPL (the "Licence");

* You may not use this work except in compliance with the
  Licence.

* You may obtain a copy of the Licence at:

  https://joinup.ec.europa.eu/software/page/eupl

* Unless required by applicable law or agreed to in
  writing, software distributed under the Licence is
  distributed on an "AS IS" basis,

* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
  express or implied.

* See the Licence for the specific language governing
  permissions and limitations under the Licence.
###

q       = require 'q'
path    = require 'path'
request = require 'request'


class KeycloakRequest

  constructor: (@config)->
    meth = 'KeycloakRequest.constructor'
    if not @logger
      @logger =
        error: console.log
        warn: console.log
        info: console.log
        debug: console.log
    @logger.info "#{meth} CONFIG: #{JSON.stringify @config}"


  # Gets an Access Token for the given user credentials (username and password)
  # from Keycloak.
  #
  # Equivalent to the OpenID API call:
  #
  # curl -X POST \
  #    http://{hostname}:8080/auth/realms/{realm}/protocol/openid-connect/token \
  #    -H 'Content-Type: application/x-www-form-urlencoded' \
  #    -d 'username=admin&password=admin&grant_type=password&client_id=admin-cli'
  getAccessToken: (username, password) ->
    meth = 'KeycloakRequest.getAccessToken'
    q.promise (resolve, reject) =>

      # KEYCLOAK GET TOKEN API CALL (OPENID PROTOCOL)
      #
      # Method: POST
      # URL: https://keycloak.example.com/auth/realms/myrealm/protocol/openid-connect/token
      # Body type: x-www-form-urlencoded
      # Form fields:
      # username: username
      # password: password
      # grant_type : password
      # client_id : <my-client-name>

      urlBase = @config['auth-server-url']
      urlRoute = path.join 'realms'
        , @config.realm
        , 'protocol'
        , 'openid-connect'
        , 'token'
      url = "#{urlBase}/#{urlRoute}"

      formData =
        username: username
        password: password
        grant_type: 'password'
        client_id: @config.resource # 'admission'
        client_secret : @config.credentials.secret  # '951addd0-d563-439e-aa74-dfcb0e55ba64',
        scope: 'offline_access'

      postData =
        url: url
        form: formData

      # @logger.debug "#{meth} Request POST data: #{JSON.stringify postData}"

      request.post postData, (error, response, body) =>
        if error
          errMsg = "Unable to get token for user #{username}: #{error.message}"
          @logger.error "#{meth} ERROR: #{errMsg}"
          reject new Error errMsg
        else if response.statusCode isnt 200
          errMsg = "Unable to get token for user #{username}: unexpected HTTP
                    status #{response.statusCode}"
          @logger.error "#{meth} ERROR: #{errMsg}"
          reject new Error errMsg
        else
          parsedBody = JSON.parse body
          # console.log "GOT TOKEN DATA FROM KEYCLOAK:"
          # console.log "#{JSON.stringify parsedBody}"

          # Returned information has the following format:
          #
          # {
          #   "access_token": "eyJhbG...Z88Q",
          #   "expires_in": 36000,
          #   "refresh_expires_in": 600,
          #   "refresh_token": "eyJhbG...AgNY",
          #   "token_type": "bearer",
          #   "not-before-policy": 0,
          #   "session_state": "49713eaa-0836-4efa-b619-30a6fbb323dc",
          #   "scope": "profile email"
          # }
          result = parsedBody
          result.user = {}
          resolve result


  # Gets an Access Token for the given user credentials (username and password)
  # from Keycloak.
  #
  # Equivalent to the OpenID API call:
  #
  # curl -X POST \
  #    http://{hostname}:8080/auth/realms/{realm}/protocol/openid-connect/token \
  #    -H 'Content-Type: application/x-www-form-urlencoded' \
  #    -d 'username=admin&password=admin&grant_type=password&client_id=admin-cli'
  refreshAccessToken: (refreshToken) ->
    meth = 'KeycloakRequest.refreshAccessToken'
    q.promise (resolve, reject) =>

      # KEYCLOAK REFRESH TOKEN API CALL (OPENID PROTOCOL)
      #
      # Method: POST
      # URL: https://keycloak.example.com/auth/realms/myrealm/protocol/openid-connect/token
      # Body type: x-www-form-urlencoded
      # Form fields:
      # client_id : <my-client-name>
      # grant_type : refresh_token
      # refresh_token: <my-refresh-token>

      urlBase = @config['auth-server-url']
      urlRoute = path.join 'realms'
        , @config.realm
        , 'protocol'
        , 'openid-connect'
        , 'token'
      url = "#{urlBase}/#{urlRoute}"

      formData =
        refresh_token: refreshToken
        grant_type: 'refresh_token'
        client_id: @config.resource # 'test-webserver'
        client_secret : @config.credentials.secret  # '951addd0-d563-439e-aa74-dfcb0e55ba64',

      postData =
        url: url
        form: formData

      # @logger.debug "#{meth} Request POST data: #{JSON.stringify postData}"

      request.post postData, (error, response, body) =>
        if error
          errMsg = "Unable to refresh token : #{error.message}"
          @logger.error "#{meth} ERROR: #{errMsg}"
          reject new Error errMsg
        else if response.statusCode isnt 200
          errMsg = "Unable to refresh token :unexpected HTTP status
                    #{response.statusCode}"
          @logger.error "#{meth} ERROR: #{errMsg}"
          reject new Error errMsg
        else
          parsedBody = JSON.parse body
          # console.log "GOT TOKEN DATA FROM KEYCLOAK:"
          # console.log "#{JSON.stringify parsedBody}"

          # Returned information has the following format:
          #
          # {
          #   "access_token": "eyJhbG...Z88Q",
          #   "expires_in": 36000,
          #   "refresh_expires_in": 600,
          #   "refresh_token": "eyJhbG...AgNY",
          #   "token_type": "bearer",
          #   "not-before-policy": 0,
          #   "session_state": "49713eaa-0836-4efa-b619-30a6fbb323dc",
          #   "scope": "profile email"
          # }
          result = parsedBody
          result.user = {}
          resolve result


  # Gets user info associated to a given Access Token from Keycloak.
  #
  # Equivalent to the OpenID API call:
  #
  # curl -X POST \
  #    http://{hostname}:8080/auth/realms/{realm}/protocol/openid-connect/userinfo \
  #    -H 'Content-Type: application/x-www-form-urlencoded' \
  #    -d 'access_token=xxxxxxxxxxxxxxx'
  getUserInfo: (accessToken) ->
    meth = 'KeycloakRequest.getUserInfo'
    @logger.info "#{meth} Getting user info for token: #{accessToken}"
    q.promise (resolve, reject) =>

      urlBase = @config['auth-server-url']
      urlRoute = path.join 'realms'
        , @config.realm
        , 'protocol'
        , 'openid-connect'
        , 'userinfo'
      url = "#{urlBase}/#{urlRoute}"

      formData =
        access_token: accessToken

      postData =
        url: url
        form: formData

      # @logger.debug "#{meth} Request POST data: #{JSON.stringify postData}"

      request.post postData, (error, response, body) =>
        if error
          errMsg = "Unable to userinfo: #{error.message}"
          @logger.error "#{meth} ERROR: #{errMsg}"
          reject new Error errMsg
        else if response.statusCode isnt 200
          errMsg = "Unable to userinfo: unexpected HTTP status
                    #{response.statusCode}"
          @logger.error "#{meth} ERROR: #{errMsg}"
          reject new Error errMsg
        else
          parsedBody = JSON.parse body
          # console.log "GOT USERINFO FROM KEYCLOAK:"
          # console.log "#{JSON.stringify parsedBody}"

          # Returned information has the following format:
          #
          # {
          #   "sub": "2dfbfa1b-25ee-42f0-9bc7-ede4b412a3e9",
          #   "email_verified": false,
          #   "name": "Alice Liddel",
          #   "groups": [
          #     "offline_access",
          #     "user"
          #   ],
          #   "preferred_username": "alice",
          #   "given_name": "Alice",
          #   "client-groups": [
          #     "developer"
          #   ],
          #   "family_name": "Liddel",
          #   "email": "alice@keycloak.org"
          # }

          userData =
            email: parsedBody.email
            id: parsedBody.preferred_username
            username: parsedBody.preferred_username
            limits: null
            name: parsedBody.name
            roles: parsedBody['client-groups'] || []

          @logger.debug "#{meth} Final user data: #{JSON.stringify userData}"
          resolve userData


  authenticate: (accessToken) ->
    @getUserInfo accessToken


module.exports = KeycloakRequest
