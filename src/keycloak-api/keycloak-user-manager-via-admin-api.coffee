###
* Copyright 2022 Kumori Systems S.L.

* Licensed under the EUPL, Version 1.2 or – as soon they
  will be approved by the European Commission - subsequent
  versions of the EUPL (the "Licence");

* You may not use this work except in compliance with the
  Licence.

* You may obtain a copy of the Licence at:

  https://joinup.ec.europa.eu/software/page/eupl

* Unless required by applicable law or agreed to in
  writing, software distributed under the Licence is
  distributed on an "AS IS" basis,

* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
  express or implied.

* See the Licence for the specific language governing
  permissions and limitations under the Licence.
###

q                   = require 'q'
_                   = require 'lodash'
KeycloakAdminClient = require('keycloak-admin').default

httpStatusCodes = require('./http-status-codes').HTTP_STATUS_CODES

MANDATORY_USER_PROPERTIES = [
  'username'
  'email'
  'firstName'
  'lastName'
  'password'
  'enabled'
  'groups'
]

class KeycloakUserManager

  # This constructor expects the 'config' in Admission keycloakConfig format:
  # {
  #   "realm": "KumoriCluster",
  #   "auth-server-url": "http://jferrer-keycloak.test.kumori.cloud:8080/auth",
  #   "ssl-required": "external",
  #   "resource": "admission",
  #   "verify-token-audience": true,
  #   "credentials": {
  #     "secret": "<secret>"
  #   },
  #   "use-resource-role-mappings": true,
  #   "bearerOnly": true
  # }
  constructor: (@config)->
    meth = 'KeycloakUserManager.constructor'

    @logger.info "#{meth} CONFIG: #{JSON.stringify @config}"

    @kcConfig =
      baseUrl: @config['auth-server-url']
      realmName: @config['realm']
      requestConfig: {}
    @logger.info "#{meth} kcConfig: #{JSON.stringify @kcConfig}"

    @kcAdminClient = new KeycloakAdminClient @kcConfig

  ##############################################################################
  ##                           "PUBLIC" METHODS                               ##
  ##############################################################################
  #    ONLY THESE PUBLIC METHOD SHOULD SET THE ACCESS TOKEN OF THE LIBRARY    ##
  ##############################################################################

  # Returns a promise resolved with an array of user objects like the ones
  # returned by getUser method.
  getUsers: (accessToken) ->
    meth = 'KeycloakUserManager.getUsers'
    @logger.info "#{meth}"

    @kcAdminClient.setAccessToken(accessToken)
    @_getUsers()


  # Returns a promise resolved with an object representing the user matching the
  # provided 'username'.
  # The user object has the following structure:
  #
  #   - username:  <string>
  #   - email:     <string>
  #   - firstName: <string>
  #   - lastName:  <string>
  #   - enabled:   bolean
  #   - groups:    [ <string> ]
  getUser: (username, accessToken) ->
    meth = "KeycloakUserManager.getUser(#{username})"
    @logger.info "#{meth}"

    @kcAdminClient.setAccessToken(accessToken)
    @_getUser username


  # Creates a new user. An object with the new user properties must specified.
  # Expected format (all properties are mandatory for now):
  #
  #  - username:   <string>
  #  - email:      <string>
  #  - firstName:  <string>
  #  - lastName:   <string>
  #  - password:   <string>  # TODO: implement a more secure way
  #  - enabled:    <boolean>
  #  - groups:     <list/array of group names>
  #
  # Returns a promise resolved with 'true' if the user is correctly created.
  createUser: (userData, accessToken) ->
    meth = 'KeycloakUserManager.createUser'
    @logger.info "#{meth} User data: #{JSON.stringify userData}"

    @kcAdminClient.setAccessToken(accessToken)
    @_createUser userData


  # Updates an existing user, identified by the username.
  # An object with the new user properties must specified.
  # Expected format (all properties are mandatory for now):
  #
  #  - username:   <string>
  #  - email:      <string>
  #  - firstName:  <string>
  #  - lastName:   <string>
  #  - password:   <string>  # TODO: implement a more secure way
  #  - enabled:    <boolean>
  #  - groups:     <list/array of group names>
  #
  # Returns a promise resolved with 'true' if the user is correctly created.
  updateUser: (userData, accessToken) ->
    meth = 'KeycloakUserManager.updateUser'
    @logger.info "#{meth} User data: #{JSON.stringify userData}"

    @kcAdminClient.setAccessToken(accessToken)
    @_updateUser userData


  # Deletes the user matching the provided 'username'.
  # Returns a promise resolved with 'true' if the user is correctly deleted.
  deleteUser: (username, accessToken) ->
    meth = "KeycloakUserManager.deleteUser(#{username})"
    @logger.info "#{meth}"

    @kcAdminClient.setAccessToken(accessToken)
    @_deleteUser username


  # Returns a promise resolved with an array of user objects like the ones
  # returned by getUser method.
  getGroups: (accessToken) ->
    meth = 'KeycloakUserManager.getGroups'
    @logger.info "#{meth}"

    @kcAdminClient.setAccessToken(accessToken)
    @_getGroups()



  ##############################################################################
  ##                      KEYCLOAK API HELPER METHODS                         ##
  ##############################################################################

  # Returns a promise resolved with the list of groups names/objects the
  # provided user belongs to.
  # If raw is 'true', each group is returned as the original object.
  # If raw is 'false', each group is returned as a single string (its name).
  _getUsers: (raw = false) ->
    meth = 'KeycloakUserManager._getUsers'
    @logger.info "#{meth}"

    q.promise (resolve, reject) =>
      try
        @logger.info "#{meth} Getting all users..."
        userQuery = {}
        userList = await @kcAdminClient.users.find userQuery
        if raw
          resolve userList
        else
          resolve @_completeUserList userList
      catch err
        errMsg = "Failed to get users: #{@beautifyError err}"
        @logger.warn "#{meth} ERROR: #{errMsg}"
        reject new Error errMsg


  _getUser: (username, raw = false) ->
    meth = "KeycloakUserManager._getUser(#{username})"
    @logger.info "#{meth}"

    q.promise (resolve, reject) =>
      try
        @logger.info "#{meth} Getting user..."

        userQuery = { username: username }
        userList = await @kcAdminClient.users.find userQuery

        if userList.length is 0
          throw new Error "user not found."
        else if userList.length > 1
          throw new Error "found #{userList.length} users matching username."

        if raw
          resolve userList[0]
        else
          resolve @_completeUser userList[0]
      catch err
        errMsg = "Failed to get user with username '#{username}':
                  #{@beautifyError err}"
        @logger.warn "#{meth} ERROR: #{errMsg}"
        reject new Error errMsg


  _createUser: (userData) ->
    meth = 'KeycloakUserManager._createUser'
    @logger.info "#{meth} User data: #{JSON.stringify userData}"

    q.promise (resolve, reject) =>
      try
        @_validateNewUserData userData

        # Get groups mappings for easier manipulation
        @logger.info "#{meth} Creating groups id-name mappings..."
        @_getGroupsMaps()
        .then (groupsMaps) =>

          @logger.info "#{meth} Validating user group names..."
          @_validateGroupList(groupsMaps, userData.groups)

          @logger.info "#{meth} Creating new user..."
          userCredentials =
            temporary: false
            type: 'password'
            value: userData.password

          newUser =
            username:  userData.username
            firstName: userData.firstName
            lastName:  userData.lastName
            email:     userData.email
            enabled:   userData.enabled
            credentials: [ userCredentials ]

          # Create user
          userResult = await @kcAdminClient.users.create newUser
          # returns { id: "<user_id>" }
          userId = userResult.id

          @logger.info "#{meth} Creating new user role bindings..."
          # Add user to each of its groups
          for groupName in (userData.groups || [])
            groupId = groupsMaps.name2id[groupName]
            groupResult = await @_addUserToGroup userId, groupId

          resolve true
        .catch (err) =>
          # Special case: we must duplicate error handling since there are
          # await calls inside a Promise.then() function
          errMsg = "Failed to create user '#{userData.username}':
                    #{@beautifyError err}"
          @logger.warn "#{meth} ERROR: #{errMsg}"
          reject new Error errMsg
      catch err
        errMsg = "Failed to create user '#{userData.username}':
                  #{@beautifyError err}"
        @logger.warn "#{meth} ERROR: #{errMsg}"
        reject new Error errMsg


  _updateUser: (userData) ->
    meth = 'KeycloakUserManager._updateUser'
    @logger.info "#{meth} User data: #{JSON.stringify userData}"

    q.promise (resolve, reject) =>
      try
        # @_validateUserData userData
        if not userData?.username?
          throw new Error "User data is missing required property 'username'"

        @logger.info "#{meth} Retrieving current user data..."
        currentUser = await @_getUser userData.username, true
        userId = currentUser.id
        @logger.info "#{meth} Got user data (ID: #{userId})."

        # Convert password data to credential format
        if userData.password?
          userCredentials =
            temporary: false
            type: 'password'
            value: userData.password
          userData.credentials = [ userCredentials ]
        delete userData.password

        # Keep a copy of groups and remove it from user data
        newUserGroups = null
        if userData.groups?
          newUserGroups = userData.groups.slice()
          delete userData.groups

        # Validate user groups
        @logger.info "#{meth} Validating user group names..."
        groupsMaps = await @_getGroupsMaps()
        @_validateGroupList(groupsMaps, newUserGroups)


        @logger.info "#{meth} Updating user data..."
        # Update user (return empty string on success)
        query = { id: userId }
        updateUserResult = await @kcAdminClient.users.update query, userData
        @logger.info "#{meth} User data updated."

        # If a group list was supplied, the new list replaces the new one
        if newUserGroups?
          @logger.info "#{meth} Preparing to update user groups..."
          # Determine which groups association must be added and removed

          currentUserGroups = await @_getUserGroups userId, true

          @logger.info "#{meth} Got user current groups."

          # Holds group names (later converted to ids)
          groupsToAdd = newUserGroups.slice() # Make a copy of original list
          # Holds group ids
          groupIdsToRemove = []

          for currentGroup in currentUserGroups
            index = groupsToAdd.indexOf currentGroup.name
            if index is -1
              # Old group is not in new list. Add it to remove list
              groupIdsToRemove.push currentGroup.id
            else
              # Old group is in new list. Remove it from add list
              groupsToAdd.splice index, 1

          # Convert groupsToAdd name list to ids.
          groupIdsToAdd = groupsToAdd.map (g) -> groupsMaps.name2id[g]

          @logger.info "#{meth} Removing groups: #{groupIdsToRemove}"
          @logger.info "#{meth} Adding groups: #{groupIdsToAdd}"
          # Remove obsolete groups associations
          for groupId in groupIdsToRemove
            groupResult = await @_removeUserFromGroup userId, groupId

          # Add new groups associations
          for groupId in groupIdsToAdd
            groupResult = await @_addUserToGroup userId, groupId

          @logger.info "#{meth} User groups update finished."

        resolve true
      catch err
        errMsg = "Failed to update user with username '#{userData.username}':
                  #{@beautifyError err}"
        @logger.warn "#{meth} ERROR: #{errMsg}"
        reject new Error errMsg


  _deleteUser: (username, accessToken) ->
    meth = "KeycloakUserManager._deleteUser(#{username})"
    @logger.info "#{meth}"

    q.promise (resolve, reject) =>
      try
        @logger.info "#{meth} Deleting user..."

        # Retrieve user ID
        query =
          username: username
        userList = await @kcAdminClient.users.findOne query
        # *** WARNING: This returns an array (despite the docs and its name ***

        if userList.length > 1
          throw new Error "Found #{userList.length} users matching username."

        if userList.length < 1
          throw new Error "User #{username} not found."

        userId = userList[0].id
        @logger.info "#{meth} Deleting user with ID #{userId}..."

        await @kcAdminClient.users.del { id: userId }
        resolve true
      catch err
        errMsg = "Failed to delete user with username '#{username}':
                  #{@beautifyError err}"
        @logger.warn "#{meth} ERROR: #{errMsg}"
        reject new Error errMsg


  _getUserGroups: (userId, raw = false) ->
    meth = "KeycloakUserManager._getUserGroups(#{userId})"
    @logger.info "#{meth}"

    q.promise (resolve, reject) =>
      try
        query = { id: userId }
        groupList = await @kcAdminClient.users.listGroups query

        if raw
          resolve groupList
        else
          resolve @_cleanGroupList groupList
      catch err
        errMsg = "Failed to get groups for user: #{@beautifyError err}"
        @logger.warn "#{meth} ERROR: #{errMsg}"
        reject new Error errMsg


  # Returns a promise resolved with the list of existing groups names/objects.
  # If raw is 'true', each group is returned as the original object.
  # If raw is 'false', each group is returned as a single string (its name).
  _getGroups: (raw = false) ->
    meth = 'KeycloakUserManager._getGroups'
    @logger.info "#{meth}"

    q.promise (resolve, reject) =>
      try
        @logger.info "#{meth} Getting all groups..."

        groupQuery = {}
        groupList = await @kcAdminClient.groups.find groupQuery

        @logger.info "#{meth} Got all groups..."
        if raw
          resolve groupList
        else
          resolve @_cleanGroupList groupList
      catch err
        errMsg = "Failed to get group list: #{@beautifyError err}"
        @logger.warn "#{meth} ERROR: #{errMsg}"
        reject new Error errMsg


  # Returns a promise resolved with an object containing two dictionnaries
  # representing mapping group IDs with group names and viceversa.
  _getGroupsMaps: () ->
    meth = 'KeycloakUserManager._getGroupsMaps'
    @logger.info "#{meth}"

    @_getGroups true
    .then (rawGroupList) =>
      @logger.info "#{meth} Got raw group list..."
      maps =
        id2name: {}
        name2id: {}
      for rawGroup in rawGroupList
        maps.id2name[rawGroup.id] = rawGroup.name
        maps.name2id[rawGroup.name] = rawGroup.id
      maps


  # Returns a promise resolved with group ID of the group matching the provided
  # group name.
  _getGroupId: (groupName) ->
    meth = "KeycloakUserManager._getGroupId(#{groupName})"
    @logger.info "#{meth}"

    q.promise (resolve, reject) =>
      try
        query =
          search: groupName
        groupList = await @kcAdminClient.groups.find query

        if groupList.length > 1
          throw new Error "Found #{groupList.length} groups matching name."

        @logger.debug "GOT GROUP FOR name=#{groupName}: #{JSON.stringify group}"
        resolve groupList[0].id
      catch err
        errMsg = "Failed to get group id: #{@beautifyError err}"
        @logger.warn "#{meth} ERROR: #{errMsg}"
        reject new Error errMsg


  # Add a user to a group, both provided as their IDs.
  # Returns a promise resolved with 'true' if user is added to the group.
  _addUserToGroup: (userId, groupId) ->
    meth = 'KeycloakUserManager._addUserToGroup'
    console.log "#{meth} userId: #{userId} - groupId: #{groupId}"

    q.promise (resolve, reject) =>
      try
        data =
          id: userId
          groupId: groupId

        result = await @kcAdminClient.users.addToGroup data
        resolve true
      catch err
        errMsg = "Failed to add user to group: #{@beautifyError err}"
        @logger.warn "#{meth} ERROR: #{errMsg}"
        reject new Error errMsg


  # Add a user to a group, both provided as their IDs.
  # Returns a promise resolved with 'true' if user is added to the group.
  _removeUserFromGroup: (userId, groupId) ->
    meth = 'KeycloakUserManager._removeUserFromGroup'
    console.log "#{meth} userId: #{userId} - groupId: #{groupId}"

    q.promise (resolve, reject) =>
      try
        data =
          id: userId
          groupId: groupId

        result = await @kcAdminClient.users.delFromGroup data
        resolve true
      catch err
        errMsg = "Failed to remove user from group: #{@beautifyError err}"
        @logger.warn "#{meth} ERROR: #{errMsg}"
        reject new Error errMsg


  ##############################################################################
  ##                             HELPER METHODS                               ##
  ##############################################################################

  # Filter to only keep relevant group properties (its name)
  # Raw group returned by Keycloak library:
  #  {
  #    "id": "924b54df-693d-4db6-97d4-5ae358938872",
  #    "name": "developers",
  #    "path": "/developers"
  #  }
  _cleanGroup: (rawGroup) ->
    meth = 'KeycloakUserManager._cleanGroup'
    @logger.info "#{meth}"
    rawGroup.name


  # Filter to only keep relevant group properties (its name)
  _cleanGroupList: (rawGroupList) ->
    meth = 'KeycloakUserManager._cleanGroupList'
    @logger.info "#{meth}"

    groupList = []
    for rawGroup in rawGroupList
      groupList.push @_cleanGroup rawGroup
    groupList


  # Filter to only keep relevant user properties, plus add user groups.
  # Raw user returned by Keycloak library:
  #  {
  #    "access": {
  #      "manage": true,
  #      "impersonate": false,
  #      "mapRoles": true,
  #      "view": true,
  #      "manageGroupMembership": true
  #    },
  #    "notBefore": 0,
  #    "requiredActions": [],
  #    "disableableCredentialTypes": [
  #      "password"
  #    ],
  #    "email": "javier@kumori.systems",
  #    "id": "7df85f09-940f-446b-9219-9f0a20ece1bb",
  #    "createdTimestamp": 1583495410854,
  #    "username": "javier",
  #    "enabled": true,
  #    "totp": false,
  #    "emailVerified": false,
  #    "firstName": "Javier",
  #    "lastName": "Ferrer"
  #  }
  _completeUser: (rawUser) ->
    meth = 'KeycloakUserManager._completeUser'
    @logger.info "#{meth}"
    @logger.info "#{meth} rawUser: #{JSON.stringify rawUser}"

    try
      userGroups = await @_getUserGroups rawUser.id
    catch err
      @logger.warn "#{meth} Couldn't get groups for user #{rawUser.id}.
                    Reason: #{err.message || err}"
      userGroups = []

    user =
      username:    rawUser.username
      email:       rawUser.email
      firstName:   rawUser.firstName
      lastName:    rawUser.lastName
      enabled:     rawUser.enabled
      credentials: rawUser.credentials
      groups:      userGroups

    @logger.info "#{meth} Completed User: #{JSON.stringify user}"
    user


  # Filter to only keep relevant user properties, plus add user groups.
  _completeUserList: (rawUserList) ->
    meth = 'KeycloakUserManager._completeUserList'
    @logger.info "#{meth}"
    @logger.info "#{meth} rawUserList: #{JSON.stringify rawUserList}"

    userList = []
    for rawUser in rawUserList
      user = await @_completeUser rawUser
      userList.push user
    @logger.info "#{meth} clean UserList: #{JSON.stringify userList}"
    userList


  # Validate all required user properties are present
  _validateNewUserData: (userData) ->
    meth = 'KeycloakUserManager._validateNewUserData'

    missingProps = []
    for prop in MANDATORY_USER_PROPERTIES
      if (prop not of userData) or (userData[prop] is '')
        missingProps.push prop
    if missingProps.length isnt 0
      errMsg = "User data is missing one or more mandatory properties:
                #{missingProps}"
      @logger.warn "#{meth} - #{errMsg}"
      throw new Error errMsg


  # Validate that all groups in the list exist
  _validateGroupList: (groupsMaps, groupList) ->
    meth = 'KeycloakUserManager._validateGroupList'

    return if not groupList?

    errors = []
    for groupName in groupList
      if groupName not of groupsMaps.name2id
        errors.push groupName
    if errors.length isnt 0
      errMsg = "Invalid groups: #{errors}"
      @logger.warn "#{meth} - #{errMsg}"
      throw new Error errMsg
    else
      return


  beautifyError: (err) ->
    if err.response?.status? \
    and err.response?.statusText? \
    and err.response?.data?.errorMessage?
      errMsg = "#{err.response.data.errorMessage}
                (Keycloak API Error: #{err.response.status}
                #{err.response.statusText})"
    else
      errMsg = "#{err.message || err}"
    errMsg


module.exports = KeycloakUserManager


