###
* Copyright 2022 Kumori Systems S.L.

* Licensed under the EUPL, Version 1.2 or – as soon they
  will be approved by the European Commission - subsequent
  versions of the EUPL (the "Licence");

* You may not use this work except in compliance with the
  Licence.

* You may obtain a copy of the Licence at:

  https://joinup.ec.europa.eu/software/page/eupl

* Unless required by applicable law or agreed to in
  writing, software distributed under the Licence is
  distributed on an "AS IS" basis,

* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
  express or implied.

* See the Licence for the specific language governing
  permissions and limitations under the Licence.
###

q             = require 'q'
KeycloakUser  = require './keycloak-user'


MANDATORY_USER_PROPERTIES = [
  'username'
  'email'
  'firstName'
  'lastName'
  'password'
  'enabled'
  'groups'
]

MANDATORY_PASSWORD_CHANGE_PROPERTIES = [
  'oldPassword'
  'newPassword'
]

MANDATORY_PASSWORD_CHANGE_PROPERTIES_PRIVILEGED = [
  'newPassword'
]

KEYCLOAK_K8s_APIGROUP = 'keycloak.org'
KEYCLOAK_K8s_VERSION  = 'v1alpha1'

KEYCLOAK_K8s_PLURAL_USERS   = 'keycloakusers'
KEYCLOAK_K8s_PLURAL_CLIENTS = 'keycloakclients'
KEYCLOAK_K8s_PLURAL_REALMS  = 'keycloakrealms'

KEYCLOAK_K8s_CRDs = [
  KEYCLOAK_K8s_PLURAL_USERS
  KEYCLOAK_K8s_PLURAL_CLIENTS
  KEYCLOAK_K8s_PLURAL_REALMS
]

KEYCLOAK_K8s_DEFAULT_NAMESPACE = 'keycloak'
KEYCLOAK_DEFAULT_REALM_NAME    = 'KumoriCluster'
KEYCLOAK_DEFAULT_REALM_LABEL   = 'keycloak'
KEYCLOAK_DEFAULT_CLIENT_NAME   = 'admission'

FINALIZER_REMOVAL_PERIOD_SECONDS = 30


class KeycloakUserManager

  # This constructor expects the 'config' in Admission keycloakConfig format:
  # {
  #   "namespace": "keycloak",
  #   "realmName": "KumoriCluster",
  #   "realmLabel": "keycloak-sso",
  #   "clientName": "admission"
  # }

  # {
  #   "realm": "KumoriCluster",
  #   "auth-server-url": "http://jferrer-keycloak.test.kumori.cloud:8080/auth",
  #   "ssl-required": "external",
  #   "resource": "admission",
  #   "verify-token-audience": true,
  #   "credentials": {
  #     "secret": "<secret>"
  #   },
  #   "use-resource-role-mappings": true,
  #   "bearerOnly": true



  constructor: (@config = {}, @k8sApiStub) ->
    meth = 'KeycloakUserManager.constructor'

    if not @logger
      @logger =
        error: console.log
        warn: console.log
        info: console.log
        debug: console.log

    @logger.info "#{meth} CONFIG: #{JSON.stringify @config}"

    @k8sNamespace = @config.namespace   || KEYCLOAK_K8s_DEFAULT_NAMESPACE
    @realmName    = @config.realmName   || KEYCLOAK_DEFAULT_REALM_NAME
    @realmLabel   = @config.realmLabel  || KEYCLOAK_DEFAULT_REALM_LABEL
    @clientName   = @config.clientName  || KEYCLOAK_DEFAULT_CLIENT_NAME


    # Interval for periodically removing 'Finalizers' from Keycloak Operator
    # Resources, so they are reprocessed.
    # NOTE: this is necessary to reconfigure Keycloak after a Pod restart
    @finalizerRemovalPeriodSeconds =
      @config.finalizerRemovalPeriodSeconds || FINALIZER_REMOVAL_PERIOD_SECONDS

    @removeFinalizersTimer = null


  init: () ->
    # Kick-off timer for removing Finalizers from Keycloak objects
    @removeFinalizersTimer = setInterval () =>
      @removeFinalizers()
    , @finalizerRemovalPeriodSeconds * 1000


  terminate: () ->
    # Stop timer for removing Finalizers from Keycloak objects
    clearInterval @removeFinalizersTimer


  removeFinalizers: () ->
    meth = 'KeycloakUserManager.removeFinalizers'
    try
      finalizerPatch = [{
        op: 'replace'
        path: '/metadata/finalizers'
        value: []
      }]

      for crd in KEYCLOAK_K8s_CRDs
        do (crd) =>
          @k8sApiStub.listCustomResource KEYCLOAK_K8s_APIGROUP
          , KEYCLOAK_K8s_VERSION
          , @k8sNamespace
          , crd
          , null
          .then (list) =>
            for item in list
              name = item.metadata.name
              do (name, crd) =>
                @k8sApiStub.patchCustomResource KEYCLOAK_K8s_APIGROUP
                , KEYCLOAK_K8s_VERSION
                , @k8sNamespace
                , crd
                , name
                , finalizerPatch
                .then () =>
                  true
                .catch (err) =>
                  @logger.error "#{meth} Patching #{crd} #{name}:
                                 #{@beautifyError err}"
                  true
          .catch (err) =>
            @logger.error "#{meth} Getting list of #{crd}:
                           #{@beautifyError err}"
            true
    catch err
      @logger.error "#{meth} ERROR: #{err.message} #{err.stack}"
      true


  ##############################################################################
  ##                           "PUBLIC" METHODS                               ##
  ##############################################################################

  # Returns a promise resolved with an array of user objects like the ones
  # returned by getUser method.
  getUsers: () ->
    meth = 'KeycloakUserManager.getUsers'
    @logger.info "#{meth}"

    @k8sApiStub.listCustomResource KEYCLOAK_K8s_APIGROUP
    , KEYCLOAK_K8s_VERSION
    , @k8sNamespace
    , KEYCLOAK_K8s_PLURAL_USERS
    , null
    .then (list) =>
      # console.log "RESPONSE: #{JSON.stringify list}"
      @_cleanUserList list
    .catch (err) =>
      errMsg = "Failed to list users: #{@beautifyError err}"
      @logger.warn "#{meth} ERROR: #{errMsg}"
      return q.reject new Error errMsg


  # Returns a promise resolved with an object representing the user matching the
  # provided 'username'.
  # The user object has the following structure:
  #
  #   - username:  <string>
  #   - email:     <string>
  #   - firstName: <string>
  #   - lastName:  <string>
  #   - enabled:   bolean
  #   - groups:    [ <string> ]
  getUser: (username) ->
    meth = "KeycloakUserManager.getUser(#{username})"
    @logger.info "#{meth}"

    @k8sApiStub.getCustomResource KEYCLOAK_K8s_APIGROUP
    , KEYCLOAK_K8s_VERSION
    , @k8sNamespace
    , KEYCLOAK_K8s_PLURAL_USERS
    , username
    .then (user) =>
      # console.log "RESPONSE: #{JSON.stringify user}"
      if not user?
        throw new Error "user not found"
      else
        @_cleanUser user
    .catch (err) =>
      errMsg = "Failed to get user with username '#{username}':
                #{@beautifyError err}"
      @logger.warn "#{meth} ERROR: #{errMsg}"
      return q.reject new Error errMsg


  # Returns a promise resolved with an array of user objects like the ones
  # returned by getUser method.
  getGroups: () ->
    meth = 'KeycloakUserManager.getGroups'
    @logger.info "#{meth}"

    @k8sApiStub.getCustomResource KEYCLOAK_K8s_APIGROUP
    , KEYCLOAK_K8s_VERSION
    , @k8sNamespace
    , KEYCLOAK_K8s_PLURAL_CLIENTS,
    @clientName
    .then (admClient) =>
      # console.log "RESPONSE: #{JSON.stringify admClient}"
      @_getRolesFromClient admClient
    .catch (err) =>
      errMsg = "Failed to list groups: #{@beautifyError err}"
      @logger.warn "#{meth} ERROR: #{errMsg}"
      return q.reject new Error errMsg


  # Deletes the user matching the provided 'username'.
  # Returns a promise resolved with 'true' if the user is correctly deleted.
  deleteUser: (username) ->
    meth = "KeycloakUserManager.deleteUser(#{username})"
    @logger.info "#{meth}"

    @k8sApiStub.deleteCustomResource KEYCLOAK_K8s_APIGROUP
    , KEYCLOAK_K8s_VERSION
    , @k8sNamespace
    , KEYCLOAK_K8s_PLURAL_USERS
    , username
    .catch (err) =>
      errMsg = "Failed to delete user with username '#{username}':
                #{@beautifyError err}"
      @logger.warn "#{meth} ERROR: #{errMsg}"
      return q.reject new Error errMsg


  # Creates a new user. An object with the new user properties must specified.
  # Expected format (all properties are mandatory for now):
  #
  #  - username:   <string>
  #  - email:      <string>
  #  - firstName:  <string>
  #  - lastName:   <string>
  #  - password:   <string>  # TODO: implement a more secure way
  #  - enabled:    <boolean>
  #  - groups:     <list/array of group names>
  #
  # Returns a promise resolved with 'true' if the user is correctly created.
  createUser: (userData) ->
    meth = 'KeycloakUserManager.createUser'
    @logger.info "#{meth} User data: #{JSON.stringify userData}"

    # Start a promise chain so synchronous validation exceptions are handled by
    # the promise catch handler.
    q()
    .then () =>
      # Check that all mandatory properties are present
      @logger.info "#{meth} Validating user data..."
      @_validateUserDataProperties userData

      # Check that all groups are valid
      @logger.info "#{meth} Validating user group names..."
      @_validateUserGroupList(userData.groups)
    .then () =>

      # Check if user already exists
      @logger.info "#{meth} Validating username availability..."
      @k8sApiStub.getCustomResource KEYCLOAK_K8s_APIGROUP
      , KEYCLOAK_K8s_VERSION
      , @k8sNamespace
      , KEYCLOAK_K8s_PLURAL_USERS
      , userData.username
    .then (previousUser) =>
      if previousUser?
        throw new Error "user already exists (#{userData.username})"
      else
        newUserCR = new KeycloakUser @realmLabel
          , @k8sNamespace
          , null
          , userData.username
          , userData.password
          , userData.firstName
          , userData.lastName
          , userData.email
          , userData.enabled
          , userData.groups
          , null

        # console.log "NEW USER CUSTOM RESOURCE:"
        # console.log JSON.stringify newUserCR, null, 2

        # Check if user already exists
        @logger.info "#{meth} Creating new user..."
        @k8sApiStub.createCustomResource KEYCLOAK_K8s_APIGROUP
        , KEYCLOAK_K8s_VERSION
        , @k8sNamespace
        , KEYCLOAK_K8s_PLURAL_USERS
        , newUserCR
    .then () =>
      @logger.info "#{meth} New user #{userData.username} created."
      true
    .catch (err) =>
      errMsg = "Failed to create user '#{userData.username}':
                #{@beautifyError err}"
      @logger.warn "#{meth} ERROR: #{errMsg}"
      return q.reject new Error errMsg


  # Updates an existing user, identified by the username.
  # An object with the new user properties must specified.
  # Expected format (all properties are mandatory for now):
  #
  #  - username:   <string>
  #  - email:      <string>
  #  - firstName:  <string>
  #  - lastName:   <string>
  #  - password:   <string>  # TODO: implement a more secure way
  #  - enabled:    <boolean>
  #  - groups:     <list/array of group names>
  #
  # Returns a promise resolved with 'true' if the user is correctly created.
  updateUser: (userData) ->
    meth = 'KeycloakUserManager.updateUser'
    @logger.info "#{meth} User data: #{JSON.stringify userData}"

    # Start a promise chain so synchronous validation exceptions are handled by
    # the promise catch handler.
    q()
    .then () =>
      # Check that all mandatory properties are present
      @logger.info "#{meth} Validating user data..."
      @_validateUserDataProperties userData

      # Check that all groups are valid
      @logger.info "#{meth} Validating user group names..."
      @_validateUserGroupList(userData.groups)
    .then () =>

      # Check if user already exists
      @logger.info "#{meth} Validating username existence..."
      @k8sApiStub.getCustomResource KEYCLOAK_K8s_APIGROUP
      , KEYCLOAK_K8s_VERSION
      , @k8sNamespace
      , KEYCLOAK_K8s_PLURAL_USERS
      , userData.username
    .then (previousUser) =>
      if not previousUser?
        throw new Error "user not found (#{userData.username})"
      else
        @logger.info "#{meth} Found user, updating it..."

        # Create new CR object
        newUserCR = new KeycloakUser @realmLabel
          , @k8sNamespace
          , null
          , userData.username
          , userData.password
          , userData.firstName
          , userData.lastName
          , userData.email
          , userData.enabled
          , userData.groups
          , null

        # Set the User ID property from previous object version
        newUserCR.spec.user.id = previousUser.spec.user.id

        # Set resource version to the previous object version
        newUserCR.metadata.resourceVersion =
          previousUser.metadata.resourceVersion

        # console.log "NEW USER CUSTOM RESOURCE:"
        # console.log JSON.stringify newUserCR, null, 2

        # Update user
        @logger.info "#{meth} Updating user..."
        @k8sApiStub.replaceCustomResource KEYCLOAK_K8s_APIGROUP
        , KEYCLOAK_K8s_VERSION
        , @k8sNamespace
        , KEYCLOAK_K8s_PLURAL_USERS
        , newUserCR
    .then () =>
      @logger.info "#{meth} User #{userData.username} updated."
      true
    .catch (err) =>
      errMsg = "Failed to update user '#{userData.username}':
                #{@beautifyError err}"
      @logger.warn "#{meth} ERROR: #{errMsg}"
      return q.reject new Error errMsg


  # Updates an existing user password.
  # An object with the old and new passwords must specified.
  # Expected format (all properties are mandatory for now):
  #
  #  - newPassword:   <string>
  #  - oldPassword:   <string>
  #
  # If 'privileged' flag is provided, it is not mandatory to provide the old
  # password.
  #
  # Returns a promise resolved with 'true' if the user is correctly updated
  updateUserPassword: (username, passwordChangeData, privileged=false) ->
    meth = 'KeycloakUserManager.updateUserPassword'
    @logger.info "#{meth} User: #{username} (privileged=#{privileged}"

    # Start a promise chain so synchronous validation exceptions are handled by
    # the promise catch handler.
    q()
    .then () =>
      # Check that all mandatory properties are present
      @logger.info "#{meth} Validating password change data..."
      @_validatePasswordChangeProperties passwordChangeData, privileged
    .then () =>
      # Get user
      @logger.info "#{meth} Validating username existence..."
      @k8sApiStub.getCustomResource KEYCLOAK_K8s_APIGROUP
      , KEYCLOAK_K8s_VERSION
      , @k8sNamespace
      , KEYCLOAK_K8s_PLURAL_USERS
      , username
    .then (previousUser) =>
      if not previousUser?
        throw new Error "user not found (#{username})"

      @logger.info "#{meth} Found user, updating its password..."

      # Verify that 'oldPassword' parameter matches the user previous password,
      # unless request is made by an Admin on a different user (privileged
      # operation)
      previousPassword = previousUser.spec.user?.credentials?[0]?.value || null
      if (passwordChangeData.oldPassword isnt previousPassword) \
      and (not privileged)
        throw new Error "incorrect password ('oldPassword' property)."

      # Create new CR object
      newUserCR = new KeycloakUser @realmLabel
        , @k8sNamespace
        , previousUser.spec.user.id
        , previousUser.spec.user.username
        , passwordChangeData.newPassword
        , previousUser.spec.user.firstName
        , previousUser.spec.user.lastName
        , previousUser.spec.user.email
        , previousUser.spec.user.enabled
        , previousUser.spec.user.clientRoles.admission
        , previousUser.metadata.resourceVersion

        ## # Set the User ID property from previous object version
        ## newUserCR.spec.user.id = previousUser.spec.user.id

        ## # Set resource version to the previous object version
        ## newUserCR.metadata.resourceVersion =
        ##   previousUser.metadata.resourceVersion

        # Check if user already exists
        @logger.info "#{meth} Updating user..."
        @k8sApiStub.replaceCustomResource KEYCLOAK_K8s_APIGROUP
        , KEYCLOAK_K8s_VERSION
        , @k8sNamespace
        , KEYCLOAK_K8s_PLURAL_USERS
        , newUserCR
    .then () =>
      @logger.info "#{meth} User #{username} updated."
      true
    .catch (err) =>
      errMsg = "Failed to update user '#{username}':
                #{@beautifyError err}"
      @logger.warn "#{meth} ERROR: #{errMsg}"
      return q.reject new Error errMsg



  ##############################################################################
  ##                             UTIL METHODS                                 ##
  ##############################################################################
  _cleanUserList: (userList) ->
    meth = 'KeycloakUserManager._cleanUserList'
    cleanUserList = []
    for user in userList
      cleanUserList.push @_cleanUser user

      # NOTE: If we want to filter out users with no Admission permissions,
      #       comment the previous statement and uncomment the following block.
      #
      # if not user.spec.user.clientRoles?[@clientName]?
      #   @logger.info "#{meth} Skipping non-admission user:
      #                 username=#{user.spec.user.username}
      #                 clients=#{Object.keys user.spec.user.clientRoles}"
      # else
      #   cleanUserList.push @_cleanUser user
    cleanUserList


  _cleanUser: (user) ->
    cleanUser =
      username:  user.spec.user.username
      email:     user.spec.user.email
      firstName: user.spec.user.firstName
      lastName:  user.spec.user.lastName
      enabled:   user.spec.user.enabled
      groups:    []
      status:    ""

    for role in user.spec.user.clientRoles[@clientName] || []
      if role is 'administrator'
        cleanUser.groups.push 'administrators'
      if role is 'developer'
        cleanUser.groups.push 'developers'

    if not user.status?
      cleanUser.status = 'pending creation'
    else if user.status.phase is 'reconciled'
      cleanUser.status = 'ok'
    else
      cleanUser.status = "#{user.status.phase} - #{user.status.message}"

    cleanUser


  _getRolesFromClient: (client) ->
    # Groups are Roles, but with a final 's' (for backwards compatibility)
    groups = []
    for roleItem in (client.spec.roles || [])
      # Only take into account client roles (boolean flag)
      if roleItem.clientRole
        if roleItem.name is 'administrator'
          groups.push 'administrators'
        if roleItem.name is 'developer'
          groups.push 'developers'
    groups


  # Validate all required user properties are present
  _validateUserDataProperties: (userData) ->
    meth = 'KeycloakUserManager._validateUserDataProperties'

    missingProps = []
    for prop in MANDATORY_USER_PROPERTIES
      if (prop not of userData) or (userData[prop] is '')
        missingProps.push prop
    if missingProps.length isnt 0
      errMsg = "User data is missing one or more mandatory properties:
                #{missingProps}"
      @logger.warn "#{meth} - #{errMsg}"
      throw new Error errMsg


  # Validate all required password change properties are present
  _validatePasswordChangeProperties: (passwordChangeData, privileged) ->
    meth = 'KeycloakUserManager._validatePasswordChangeProperties'

    mandatoryProperties = MANDATORY_PASSWORD_CHANGE_PROPERTIES
    if privileged
      mandatoryProperties = MANDATORY_PASSWORD_CHANGE_PROPERTIES_PRIVILEGED

    missingProps = []
    for prop in mandatoryProperties
      if (prop not of passwordChangeData) or (passwordChangeData[prop] is '')
        missingProps.push prop
    if missingProps.length isnt 0
      errMsg = "Missing one or more mandatory properties:
                #{missingProps}"
      @logger.warn "#{meth} - #{errMsg}"
      throw new Error errMsg


  # Validate that all groups in the list exist
  _validateUserGroupList: (groupList) ->
    meth = 'KeycloakUserManager._validateUserGroupList'

    return if not groupList?

    @getGroups()
    .then (validGroups) =>
      errors = []
      for groupName in groupList
        if groupName not in validGroups
          errors.push groupName
      if errors.length isnt 0
        errMsg = "Invalid groups: #{errors}"
        @logger.warn "#{meth} - #{errMsg}"
        return q.reject new Error errMsg
      else
        return true


  beautifyError: (err) ->
    if err.response?.status? \
    and err.response?.statusText? \
    and err.response?.data?.errorMessage?
      errMsg = "#{err.response.data.errorMessage}
                (Keycloak API Error: #{err.response.status}
                #{err.response.statusText})"
    else
      errMsg = "#{err.message || err}"
    errMsg




module.exports = KeycloakUserManager


