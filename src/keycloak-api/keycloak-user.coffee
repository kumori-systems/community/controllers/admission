###
* Copyright 2022 Kumori Systems S.L.

* Licensed under the EUPL, Version 1.2 or – as soon they
  will be approved by the European Commission - subsequent
  versions of the EUPL (the "Licence");

* You may not use this work except in compliance with the
  Licence.

* You may obtain a copy of the Licence at:

  https://joinup.ec.europa.eu/software/page/eupl

* Unless required by applicable law or agreed to in
  writing, software distributed under the Licence is
  distributed on an "AS IS" basis,

* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
  express or implied.

* See the Licence for the specific language governing
  permissions and limitations under the Licence.
###


# This class is a representation of a Keycloak Operator User CRD
#
# Sample User resource:
#
#  apiVersion: keycloak.org/v1alpha1
#  kind: KeycloakUser
#  metadata:
#    name: admin
#    namespace: keycloak-namespace
#    labels:
#      app: keycloak-sso
#  spec:
#    user:
#      username: "admin"
#      firstName: "Default"
#      lastName: "Administrator"
#      email: "admin@kumori.cloud"
#      enabled: True
#      emailVerified: False
#      credentials:
#      - type: "password"
#        value: "condemor"
#      realmRoles:
#      - "user"
#      - "offline_access"
#      - "uma_authorization"
#      clientRoles:
#        admission:
#        - administrator
#        - developer
#        account:
#        - "manage-account"
#        - "view-profile"
#        realm-management:
#        - "manage-users"
#    realmSelector:
#      matchLabels:
#        app: keycloak-sso
#

KEYCLOAK_K8s_APIGROUP = "keycloak.org"
KEYCLOAK_K8s_VERSION  = "v1alpha1"
KEYCLOAK_K8s_KIND     = "KeycloakUser"


class KeycloakUser

  constructor: (
    realmLabel,
    namespace
    , id                  # Optional
    , username
    , password
    , firstName
    , lastName
    , email
    , enabled
    , groups
    , resourceVersion     # Optional
  ) ->

    meth = 'KeycloakUser.constructor'

    @apiVersion = "#{KEYCLOAK_K8s_APIGROUP}/#{KEYCLOAK_K8s_VERSION}"
    @kind = KEYCLOAK_K8s_KIND
    @metadata =
      name: username
      namespace: namespace
      labels:
        app: realmLabel
    @spec =
      user:
        username: username
        firstName: firstName
        lastName: lastName
        email: email
        enabled: enabled
        emailVerified: false
        credentials: [{
          type: 'password'
          value: password
        }]
        realmRoles: [
          'user'
          'offline_access'
          'uma_authorization'
        ]
        clientRoles:
          account: [
            'manage-account'
            'view-profile'
          ]
          admission: []
          'realm-management': []
      realmSelector:
        matchLabels:
          app: realmLabel

    if id?
      @spec.user.id = id

    if resourceVersion?
      @metadata.resourceVersion = resourceVersion

    for group in groups
      if group in [ 'administrator', 'administrators' ]
        @spec.user.clientRoles.admission.push 'administrator'
        @spec.user.clientRoles['realm-management'].push 'manage-users'
      else if group in [ 'developer', 'developers' ]
        @spec.user.clientRoles.admission.push 'developer'
      else
        # Ignore unknown group



module.exports = KeycloakUser
