###
* Copyright 2022 Kumori Systems S.L.

* Licensed under the EUPL, Version 1.2 or – as soon they
  will be approved by the European Commission - subsequent
  versions of the EUPL (the "Licence");

* You may not use this work except in compliance with the
  Licence.

* You may obtain a copy of the Licence at:

  https://joinup.ec.europa.eu/software/page/eupl

* Unless required by applicable law or agreed to in
  writing, software distributed under the Licence is
  distributed on an "AS IS" basis,

* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
  express or implied.

* See the Licence for the specific language governing
  permissions and limitations under the Licence.
###

fs     = require 'fs'
url    = require 'url'
path   = require 'path'
mkdirp = require 'mkdirp'
rmdir  = require 'rmdir'
q      = require 'q'

ManifestStore  = require './manifest-store'

DEFAULT_MANIFEST_FILENAME = 'manifest.json'


class ManifestStoreFS extends ManifestStore

  constructor: (options) ->
    super 'FS'
    @storePath = options.storePath


  init: ->
    q.promise (resolve, reject) =>
      # Make sure specified storage directory exists (synchronously)
      mkdirp @storePath, (err) ->
        if err
          reject new Error('Initializing Manifest Store: ' + err.message)
        else
          resolve @storepath


  terminate: (deleteStorage = false) ->
    # Nothing to do in Filew System storage
    q true


  getManifest: (manifestURN) ->
    q.promise (resolve, reject) =>
      if not manifestURN
        reject new Error('Getting manifest: manifestURN is empty.')
      try
        parsed = url.parse manifestURN
        filename = path.join @storePath
          , parsed.host
          , parsed.pathname
          , DEFAULT_MANIFEST_FILENAME
        # TODO: check if manifest exists ?
        fs.readFile filename, (error, rawData) ->
          if (error)
            reject new Error('Reading manifest from file: ' + error.message)
          else
            try
              parsed = JSON.parse rawData
              resolve parsed
            catch err
              reject new Error('Parsing manifest: ' + err)
      catch err
        reject new Error('Parsing manifest URN: ' + err)


  # Expects an Object representing the manifest, *NOT* the JSON
  storeManifest: (manifest) ->
    q.promise (resolve, reject) =>
      if not manifest
        reject new Error('Storing manifest: manifest is empty.')
      try
        manifestURN = manifest.name
        parsed = url.parse manifestURN
        filename = path.join @storePath
          , parsed.host
          , parsed.pathname
          , DEFAULT_MANIFEST_FILENAME
        # Make sure the destination directory exists
        mkdirp (path.dirname filename), (error, made) ->
          if error
            reject new Error('Creating subdirectory: ' + error.message)
          else
            fs.writeFile filename, (JSON.stringify manifest), (err) ->
              if err
                reject new Error('Storing Service Application: ' + err.message)
              else
                resolve manifestURN
      catch e
        reject new Error('Determining storage filename: ' + e)


  deleteManifest: (manifestURN) ->
    q.promise (resolve, reject) =>
      if not manifestURN
        reject new Error('Deleting manifest: manifest URN is empty.')
      try
        parsed = url.parse manifestURN
        filename = path.join @storePath
          , parsed.host
          , parsed.pathname
          , DEFAULT_MANIFEST_FILENAME
        # Make sure the destination directory exists
        rmdir (path.dirname filename), (error, dirs, files) ->
          if error
            reject new Error('Removing subdirectory: ' + error.message)
          else
            resolve manifestURN
      catch e
        reject new Error('Determining storage filename: ' + e)

  updateManifest: (manifest) ->
    # Right now, store overwrites any previously stored manifest
    @storeManifest manifest

  list: ->
    # TODO

  info: ->
    "#{@type} Manifest Store (Path: #{@storePath})"

module.exports = ManifestStoreFS
