###
* Copyright 2022 Kumori Systems S.L.

* Licensed under the EUPL, Version 1.2 or – as soon they
  will be approved by the European Commission - subsequent
  versions of the EUPL (the "Licence");

* You may not use this work except in compliance with the
  Licence.

* You may obtain a copy of the Licence at:

  https://joinup.ec.europa.eu/software/page/eupl

* Unless required by applicable law or agreed to in
  writing, software distributed under the Licence is
  distributed on an "AS IS" basis,

* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
  express or implied.

* See the Licence for the specific language governing
  permissions and limitations under the Licence.
###

q           = require 'q'
path        = require 'path'
request     = require 'request'
querystring = require 'querystring'


DOCKER_DEFAULT_REGISTRY  = "docker.io"
DOCKER_DEFAULT_NAMESPACE = "library"
DOCKER_DEFAULT_TAG       = "latest"
DOCKER_REGISTRY_URL      = "index.docker.io"

DOCKER_HUB_ALT_REGISTRY  = "registry.hub.docker.com"

class DockerRegistryProxy

  # Configuration object is expected to have the following format:
  #   "dockerRegistry": {
  #     "username": "dockerhub-username",
  #     "password": "dockerhub-password",
  #     "mirror": "https://docker-registry.example.com"
  #   }
  constructor: (config)->
    meth = 'DockerRegistryProxy.constructor'

    if config.mirror? and config.mirror isnt ''
      @dockerRegistryMirror = config.mirror
    else
      @dockerRegistryMirror = null

    if config.dockerHubUsername? and config.dockerHubUsername isnt ''
      @dockerHubUsername = config.dockerHubUsername
    else
      @dockerHubUsername = null

    if config.dockerHubPassword? and config.dockerHubPassword isnt ''
      @dockerHubPassword = config.dockerHubPassword
    else
      @dockerHubPassword = null

    if @dockerRegistryMirror? and @dockerHubUsername?
      @logger.warn "#{meth} Docker registry configuration includes both a
                    registry mirror and DockerHub credentials. Credentials will
                    be ignored."

    if not @logger
      @logger =
        error: console.log
        warn:  console.log
        info:  console.log
        debug: console.log
    @logger.info "#{meth} CONFIG: #{JSON.stringify config}"



  normalizeDockerImage: (imageData) ->
    meth = 'DockerRegistryProxy.normalizeDockerImage()'

    @logger.info "#{meth} Docker image data: #{JSON.stringify imageData}"

    # Check image tag string for blank spaces at the beginning or at the end
    if imageData.tag.startsWith(' ') or imageData.tag.endsWith(' ')
      errMsg = "Image tag cannot start or end with spaces: '#{imageData.tag}'"
      @logger.warn "#{meth} - #{errMsg}"
      throw new Error errMsg

    dockerImage =
      hub: imageData.hub.name
      secret: imageData.hub.secret
      name: null
      ref: null

    # If no Docker registry is specified, default to Docker Hub (docker.io)
    if (not dockerImage.hub?) or dockerImage.hub is ''
      dockerImage.hub = DOCKER_DEFAULT_REGISTRY
    else if dockerImage.hub is DOCKER_HUB_ALT_REGISTRY
      # Fix for registry mirror not allowing pull from 'registry.hub.docker.com'
      dockerImage.hub = DOCKER_DEFAULT_REGISTRY
      # NOT NECESSARY ANYMORE (13/01/2021) - LEFT FOR REFERENCE
      # Overwrite the value in the original data
      # imageData.hub.name = DOCKER_DEFAULT_REGISTRY

    # Extract tag or set default to 'latest'
    nameParts = imageData.tag.split ':'
    if nameParts.length is 2
      dockerImage.name = nameParts[0]
      dockerImage.ref = nameParts[1]
    else
      dockerImage.name = nameParts[0]
      dockerImage.ref = DOCKER_DEFAULT_TAG

    # If image is from DockerHub and has no namespace, add default 'library'
    if dockerImage.hub is DOCKER_DEFAULT_REGISTRY
      nameParts = dockerImage.name.split '/'
      if nameParts.length is 1
        dockerImage.name = DOCKER_DEFAULT_NAMESPACE + '/' + nameParts[0]

    @logger.info "#{meth} Normalized: #{JSON.stringify dockerImage}"
    return dockerImage



  # Checks if a docker image exists in a Docker repository.
  #
  # If conditions are met to use a registry mirror (image is hosted on
  # DockerHub, a mirror configured and image credentials have not been
  # provided), a first attempt at validating the image is done via the mirror.
  # In any other case or if the mirror validation fails, the image is validated
  # using its corresponding registry.
  checkImage: (registryURL, imageName, imageRef, username = null
  , password = null, token = null) ->

    if (not registryURL?) or (registryURL is '')
      registryURL = DOCKER_DEFAULT_REGISTRY
      @logger.info "#{meth} - Replaced empty registry URL: #{registryURL}"

    meth = "DockerRegistryProxy.checkImage
            #{registryURL}/#{imageName}:#{imageRef}"
    @logger.info "#{meth} - Token? #{token?} - Username: #{username}"

    useMirror = false
    if (registryURL is DOCKER_DEFAULT_REGISTRY) \
    and @dockerRegistryMirror? \
    and (not username?) and (not password?)
      useMirror = true

    (if useMirror
      @logger.info "#{meth} - Trying to validate image using configured mirror."
      @performImageCheck registryURL
        , imageName
        , imageRef
        , null
        , null
        , null
        , true   # Use mirror
    else
      q false
    )
    .then (validated) =>
      if validated
        return true
      else
        @logger.info "#{meth} - Trying to validate image (no mirror)."
        @performImageCheck registryURL
          , imageName
          , imageRef
          , username
          , password
          , null
          , false   # Do not use mirror


  # Perform the actual image check according to the configuration (useMirror).
  #
  # This method takes care of contacting the registry and authenticated when
  # and where required by the registry response.
  performImageCheck: (registryURL, imageName, imageRef, username = null
  , password = null, token = null, useMirror = false) ->

    meth = "DockerRegistryProxy.performImageCheck
            #{registryURL}/#{imageName}:#{imageRef}"
    @logger.info "#{meth} - Token? #{token?} - Username: #{username} -
                  Use mirror? #{useMirror}"

    # If registry is official docker hub (docker.io), do some configuration
    # adjustments to use the correct values:
    #
    # - if credentials (username/password) are provided
    #   - always use DockerHub registry (index.docker.io)
    # - if credentials are NOT provided
    #   - if a mirror is configured and useMirror is true
    #     - use the mirror URL
    #   - if a mirror is NOT configured
    #     - use DockerHub registry (index.docker.io)
    #     - if DockerHub credentials are configured in Admission use them

    if registryURL is DOCKER_DEFAULT_REGISTRY
      if username? and password?
        # DockerHub credentials provided, use DockerHub registry
        registryURL = DOCKER_REGISTRY_URL
        @logger.info "#{meth} - Replaced DockerHub registry URL: #{registryURL}"
      else
        # No DockerHub credentials provided
        if @dockerRegistryMirror? and useMirror
          # Registry mirror is configured, use it
          registryURL = @dockerRegistryMirror
          @logger.info "#{meth} - Replaced DockerHub registry URL to use mirror:
                        #{registryURL}"
        else
          # Registry mirror not configured, use DockerHub registry
          registryURL = DOCKER_REGISTRY_URL
          @logger.info "#{meth} - Replaced DockerHub registry URL: #{registryURL}"
          # If Admission configuration includes DockerHub credentials, use them
          if @dockerHubUsername? and @dockerHubPassword?
            username = @dockerHubUsername
            password = @dockerHubPassword
            @logger.info "#{meth} - Replaced DockerHub username: #{username}"
            @logger.info "#{meth} - Replaced DockerHub password: <hidden>"


    # Make sure the registry URL contains a protocol section (default to https)
    if (not registryURL.startsWith 'http://') \
    and (not registryURL.startsWith 'https://')
      registryURL = 'https://' + registryURL


    q.promise (resolve, reject) =>

      # 'Accept' header has been added due to some images not working OK with
      # the default type.
      getOpts =
        method: 'GET'
        headers:
          'Accept': 'application/vnd.oci.image.index.v1+json, ' +
                    'application/vnd.oci.image.manifest.v1+json,' +
                    'application/vnd.docker.distribution.manifest.v2+json,' +
                    'application/vnd.docker.distribution.manifest.list.v2+json'
        url: "#{registryURL}/v2/#{imageName}/manifests/#{imageRef}"

      if token? and token isnt ''
        getOpts.auth =
          bearer: token

      @logger.info "#{meth} Trying to get image manifest from URL: #{getOpts.url}"
      request getOpts, (error, response, body) =>
        @logger.info "#{meth} Response status code: #{response?.statusCode}"
        if error
          @logger.error "#{meth} Unable to get image manifest (request error).
                         ERROR: #{error.message}"
          resolve false
        else if response.statusCode is 200
          @logger.info "#{meth} Got image manifest (Status code 200 OK)"
          # @logger.debug "#{meth} HEADERS: #{JSON.stringify response.headers}"
          parsedBody = null
          try
            parsedBody = JSON.parse body
          catch parseError
            # The request returns a non-JSON response, probably a wrong URL
            @logger.error "#{meth} Docker registry returned a non-JSON response.
                          Body: #{JSON.stringify parsedBody}"
            resolve false

          if not parsedBody.schemaVersion?
            # The JSON response does not have the required properties
            @logger.error "#{meth} Docker registry returned a non-compliant
                          response. Body: #{JSON.stringify parsedBody}"
            resolve false

          # @logger.debug "#{meth} BODY: #{JSON.stringify parsedBody}"
          resolve true
        else if response.statusCode is 404
          @logger.info "#{meth} Got Status code: 404 Not Found"

          # If image doesn't exist, a detailed response is received. Check it.
          # Example body:
          #   {
          #     "errors": [
          #       {
          #         "code": "MANIFEST_UNKNOWN",
          #         "message": "manifest unknown",
          #         "detail": {
          #           "Tag": "0.0.144"
          #         }
          #       }
          #     ]
          #   }
          #
          # NOTE (ticket303): Docker Hub behaves differently for multilevel
          # image requests (not supported by their registry) and return an ugly
          # HTTP 404 error page.
          parsedBody = null
          try
            parsedBody = JSON.parse body
          catch parseError
            # For example, multilevel image request to DockerHub
            @logger.info "#{meth} Docker registry returned a non-JSON response."
            parsedBody = "Non-JSON response"

          if parsedBody?.errors?[0]?.code is 'MANIFEST_UNKNOWN'
            @logger.error "#{meth} Unable to get image manifest (404 Not Found)
                           Error: #{parsedBody.errors[0].message}"
            resolve false
          else
            @logger.error "#{meth} Unable to get image manifest (404 Not Found
                           with unexpected response body).
                           Body: #{JSON.stringify parsedBody}"
            resolve false
        else if response.statusCode is 401
          @logger.info "#{meth} Got Status code: 401 Unauthorized."
          # This might mean that it is the first unauthorized attempt, necessary
          # to retrieve authentication challenge details in the response (part
          # of the Registry authentication  protocol).
          if token?
            # If token was present, it was not the initial attempt, but a real
            # failure.
            @logger.error "#{meth} Unable to get image (401 Unauthorized in
                           request with token)"
            resolve false
          else if not response.headers['www-authenticate']?
            @logger.error "#{meth} Unable to get image manifest (No
                           authentication header returned). Make sure the image
                           #{registryURL}/#{imageName}:#{imageRef} exists."
            resolve false
          else
            # It was the initial attempt. Login, get a token then retry
            @logger.info "#{meth} Checking www-authenticate header..."
            # @logger.debug "#{meth} HEADERS: #{JSON.stringify response.headers}"
            wwwAuthHeader = response.headers['www-authenticate']
            @logger.info "#{meth} Found www-authenticate header:
                          #{wwwAuthHeader}"
            # Split www-authenticate header in parts. It looks like this:
            #   Bearer realm="<auth-endpoint",service="<auth-service>",scope="<auth_scope>"
            # Example:
            #   Bearer realm="https://auth.docker.io/token",service="registry.docker.io",scope="repository:kumori/calccachecache:pull"
            parsedWAH = @parseWAH wwwAuthHeader
            @logger.info "#{meth} Parsed www-authenticate header:
                          #{JSON.stringify parsedWAH}"
            @logger.info "#{meth} Performing authentication login..."
            @authenticate parsedWAH, username, password
            .then (token) =>
              @logger.info "#{meth} - Got auth token. Retrying to get manifest."
              # Repeat the check but including the obtained token
              @performImageCheck registryURL
                , imageName
                , imageRef
                , null
                , null
                , token
                , useMirror
            .then (exists) =>
              resolve exists
            .catch (err) =>
              @logger.error "#{meth} Unable to get image manifest.
                             Error: #{err.message}"
              resolve false
        else if response.statusCode is 429
          @logger.error "#{meth} Unable to get image manifest (RateLimit
                          exceeded - status code #{response.statusCode}"
          resolve false
        else
          @logger.error "#{meth} Unable to get image manifest (unexpected HTTP
                         status code #{response.statusCode}"
          resolve false



  authenticate: (authConfig, username, password) ->
    meth = "DockerRegistryProxy.authenticate"
    @logger.info "#{meth} - Auth config: #{JSON.stringify authConfig}"

    q.promise (resolve, reject) =>

      getParams =
        service: authConfig.service
        scope: authConfig.scope

      paramsString = querystring.stringify getParams

      @logger.info "#{meth} - Auth URL: #{authConfig.realm}?#{paramsString}"

      getOpts =
        method: 'GET'
        url: "#{authConfig.realm}?#{paramsString}"

      if username? and password? and (username isnt '') and (password isnt '')
        getOpts.auth =
          user: username
          pass: password
          sendImmediately: true

      @logger.info "#{meth} Sending authentication request..."
      request getOpts, (error, response, body) =>
        @logger.info "#{meth} Response status code: #{response?.statusCode}"
        if error
          errMsg = "Unable to authenticate: #{error.message} #{error.stack}"
          @logger.error "#{meth} ERROR: #{errMsg}"
          reject new Error errMsg
        else if response.statusCode isnt 200
          errMsg = "Unable to authenticate: unexpected HTTP status code
                    #{response.statusCode}"
          @logger.error "#{meth} ERROR: #{errMsg}"
          reject new Error errMsg
        else
          # Login OK, extract token from response body
          @logger.info "#{meth} Got status code: 200 OK."
          # @logger.debug "#{meth} HEADERS: #{JSON.stringify response.headers}"
          parsedBody = JSON.parse body
          # @logger.debug "#{meth} BODY: #{JSON.stringify parsedBody}"
          if parsedBody?.token?
            @logger.info "#{meth} Got token!"
            resolve parsedBody.token
          else if parsedBody?.access_token?
            @logger.info "#{meth} Got token!"
            resolve parsedBody.access_token
          else
            errMsg = "Unable to authenticate: no token or access_token property
                      found in successful response body. BODY:
                      #{JSON.stringify parsedBody}"
            @logger.error "#{meth} ERROR: #{errMsg}"
            reject new Error errMsg



  parseWAH: (wwwAthenticateHeader) ->
    meth = "DockerRegistryProxy.parseWAH"
    @logger.info "#{meth} Header: #{wwwAthenticateHeader}"

    parts = wwwAthenticateHeader.split ' '
    if parts.length isnt 2
      errMsg = "Unable to parse www-authenticate header (wrong number of parts)"
      @logger.error "#{meth} - ERROR: #{errMsg}"
      throw new Error errMsg

    if parts[0].toLowerCase() isnt 'bearer'
      errMsg = "Unable to parse www-authenticate header (unexpected type '#{parts[0]}')"
      @logger.error "#{meth} - ERROR: #{errMsg}"
      throw new Error errMsg

    subparts = parts[1].split ','
    if subparts.length isnt 3
      errMsg = "Unable to parse www-authenticate header (wrong number of subparts)"
      @logger.error "#{meth} - ERROR: #{errMsg}"
      throw new Error errMsg

    data = {}
    for sp in subparts
      # spparts = sp.split /([",=])/
      spparts = sp.split /[",=]/
      spparts = spparts.filter (x) -> x
      if spparts.length isnt 2
        errMsg = "Unable to parse www-authenticate header (wrong number of sub-subparts)"
        @logger.error "#{meth} - ERROR: #{errMsg}"
        throw new Error errMsg
      data[spparts[0].toLowerCase()] = spparts[1].toLowerCase()

    # console.log "WAH DATA: #{JSON.stringify data, null, 2}"
    return data


module.exports = DockerRegistryProxy
