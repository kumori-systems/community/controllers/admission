###
* Copyright 2022 Kumori Systems S.L.

* Licensed under the EUPL, Version 1.2 or – as soon they
  will be approved by the European Commission - subsequent
  versions of the EUPL (the "Licence");

* You may not use this work except in compliance with the
  Licence.

* You may obtain a copy of the Licence at:

  https://joinup.ec.europa.eu/software/page/eupl

* Unless required by applicable law or agreed to in
  writing, software distributed under the Licence is
  distributed on an "AS IS" basis,

* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
  express or implied.

* See the Licence for the specific language governing
  permissions and limitations under the Licence.
###
q                   = require 'q'
stream              = require 'stream'
express             = require 'express'
EventEmitter        = require('events').EventEmitter
httpProxyMiddleware = require 'http-proxy-middleware'

BundleRegistry       = require './bundle-registry'
Admission            = require './admission'
PlannerStub          = require './planner-stub'
ManifestStoreFactory = require './manifest-store-factory'
ManifestHelper       = require './manifest-helper'
K8sApiStub           = require './k8s-api-stub'
KeycloakRequest      = require './keycloak-api/keycloak-request'
ReschedulerFactory   = require './reschedulers/rescheduler-factory'

utils                = require './utils'

DEFAULT_TMP_PATH = '/tmp/slap/admission'

DEFAULT_REST_TIMEOUT = 30 * 60 * 1000

EPEHEMERAL_WS_TOKEN_LIFETIME = 30 * 1000  # Milliseconds

KUMORI_NAMESPACE = 'kumori'
CLUSTER_CONFIG_CONFIGMAP = 'cluster-config'
INGRESS_SERVICE_OBJECT_PREFIX="ingress-lb-service-"

SHUTDOWN_RETRY_WAIT_MILLIS = 5000


class AdmissionRestAPI extends EventEmitter

  constructor: (@config = {}, @planner, @keycloak, @keycloakLogin, @keycloakUserManager) ->
    super()
    meth = 'AdmissionRestAPI.constructor'
    @logger.info "#{meth} Config: #{JSON.stringify @config}"

    @config.tmpPath ?= DEFAULT_TMP_PATH

    # Instantiate a Manifest helper
    @manifestHelper = new ManifestHelper()

    # Instantiate a Kubernetes API Server stub
    k8sApiConfig = @config.manifestRepository.config.k8sApi || null
    @k8sApiStub  = new K8sApiStub k8sApiConfig

    # Pass K8sApiStub to KeycloakUserManager (now uses Operator CRDs)
    @keycloakUserManager.k8sApiStub = @k8sApiStub

    # Instantiate a Manifest repository
    mrConfig = @config.manifestRepository
    mrConfig.config.k8sApiStub = @k8sApiStub
    @manifestRepository =
      ManifestStoreFactory.create mrConfig.type, mrConfig.config

    # Instantiate a "Planner" Stub (will simulate old Planner actions)
    @plannerStub = new PlannerStub @k8sApiStub, @manifestRepository
      , @config.eventStore

    # Instantiate a BundleRegistry
    @bundleRegistry = new BundleRegistry null, @manifestRepository
      , @config.maxBundleSize, @config.tmpPath

    # Instantiate an Admission
    @adm = new Admission @config, @manifestRepository, @plannerStub

    @config.restTimeout ?= DEFAULT_REST_TIMEOUT

    @apiRequestCount = 0
    @anonymous = @config.acs?['anonymous-user']

    # Object for keeping track of valid Websocket tokens
    @ephemeralWebsocketTokens = {}

    # Variable to keep track of maintenance status
    @downForMaintenance = false

    # Last read Cluster Configuration (from Etcd ConfigMap)
    @latestClusterConfiguration = null

    # Represents the status of the shutdown process
    @shutdownStatus =
      initiated: false
      completed: false
      messages: []
      # State transitions flags
      solutionsDeleted: false
      volumesDeleted: false
      ingressesDeleted: false
      ingressLoadBalancerDeleted: false



  init: () ->
    meth = 'AdmissionRestAPI.init()'
    @logger.info meth

    # Reset active websocket token
    @ephemeralWebsocketTokens = {}

    @bundleRegistry.init()
    .then () =>
      @k8sApiStub.init()
    .then () =>
      @manifestRepository.init()
    .then () =>
      @plannerStub.init()
    .then () =>
      @adm.init()
      @authorization = @adm.authorization
    .then () =>
      @readClusterConfig()
    .then () =>
      ##########################################################################
      ##                          Login API Router                            ##
      ##########################################################################
      @loginRouter = express.Router()
      # @loginRouter.get '/login', @keycloakLogin.protect('realm:user'), @login
      @loginRouter.get '/login', @login

      # Temporary method for Dashboard to work
      # @loginRouter.get '/tokens/refresh', @keycloak.protect('realm:user'), @refreshToken
      @loginRouter.post '/tokens/refresh', @refreshToken


      ##########################################################################
      ##                        Admission API Router                          ##
      ##########################################################################
      @router = express.Router()

      # Add a custom middleware for managing API disabling for maintenance
      @router.use (req, res, next) =>
        if @downForMaintenance
          maintenanceMsg = 'Admission Server is down for maintenance.'
          # Retry-After header to 5 minutes
          res.set 'Retry-After', '300'
          return res.status(503).send(maintenanceMsg)
        else
          next()

      # Unprotected API routes (don't require authenticated user)
      @router.get '/clusterconfig', @getClusterConfig

      # Protect all Admission API routes (require authenticated in "user" realm)
      #
      # More fine grained protection can be applied to each route
      # Example:  @router.post '/test', @keycloak.protect('realm:user'), @test
      @router.use @keycloak.protect('realm:user')

      # Admission API routes
      @router.post   '/bundles',           @registerBundle
      @router.get    '/registries',        @listElements
      @router.get    '/registries/:urn?',  @readManifest
      @router.delete '/registries/:urn?',  @unregister

      @router.post   '/solutions',                                       @registerSolution
      @router.get    '/solutions/:urn?',                                 @solutionQuery
      @router.get    '/solutions/:urn/revisions/:rev?',                  @getSolutionRevision
      @router.delete '/solutions/:urn?:force?',                          @deleteSolution
      @router.delete '/solutions/:urn/roles/:role/instances/:instance?', @restartSolutionInstances
      @router.get    '/solutions/:urn/roles/:role/instances/:instance/:container?/logs', @getSolutionInstanceLogs
      @router.get    '/solutions/:urn/roles/:role/instances/:instance/:container?/exec', @execSolutionInstanceCommandGet
      @router.ws     '/solutions/:urn/roles/:role/instances/:instance/exec', @execSolutionInstanceCommand

      @router.get    '/links',      @listLinks
      @router.post   '/links',      @linkServices
      @router.delete '/links:urn?', @unlinkServices

      @router.post   '/resources',                    @registerResource
      @router.get    '/resources/inuse/:owner?:urn?', @listResourcesInUse
      @router.get    '/resources/:owner?',            @listResources
      @router.get    '/resources/:urn/details',       @describeResource
      @router.delete '/resources/:urn',               @deleteResource


      ##########################################################
      ##      CURRENTLY DEPRECATED API METHODS (NOT USED      ##
      ##########################################################
      @router.delete '/deployments/:urn?:force?',                          @deprecated, @undeploy
      @router.delete '/deployments/:urn/roles/:role/instances/:instance?', @deprecated, @restartInstances
      @router.post   '/deployments',                                       @deprecated, @deploy
      @router.get    '/deployments',                                       @deprecated, @deploymentQuery
      @router.get    '/deployments/:urn?',                                 @deprecated, @deploymentQuery
      @router.get    '/deployments/:urn/revisions/:rev?',                  @deprecated, @getDeploymentRevision

      @router.get    '/logs/:instance/:container?', @deprecated, @getInstanceLogs
      @router.get    '/instances/:instance/logs',   @deprecated, @getInstanceLogs
      @router.get    '/instances/:instance/exec',   @deprecated, @execInstanceCommandGet
      # Websocket route (express-ws)
      @router.ws     '/instances/:instance/exec',   @deprecated, @execInstanceCommand
      ##########################################################
      ##  END OF CURRENTLY DEPRECATED API METHODS (NOT USED   ##
      ##########################################################



      # Cluster management API routes
      @router.get '/clusterinfo', @getClusterInfo


      # TODO: The protect method could already check access based on
      # Example: @keycloak.protect('admin-users')
      #          --> where 'admin-users' is a custom Client role

      # User management API routes
      # These routes are disabled when no authentication is configured
      @router.get    '/users',           @keycloak.protect('admission:administrator'), @checkUserMgmt, @listUsers
      @router.get    '/users/:username', @keycloak.protect('admission:administrator'), @checkUserMgmt, @getUser
      @router.post   '/users',           @keycloak.protect('admission:administrator'), @checkUserMgmt, @createUser
      @router.put    '/users',           @keycloak.protect('admission:administrator'), @checkUserMgmt, @updateUser
      @router.delete '/users/:username', @keycloak.protect('admission:administrator'), @checkUserMgmt, @deleteUser
      @router.get    '/groups',          @keycloak.protect('admission:administrator'), @checkUserMgmt, @listGroups
      # Special non-admin method for users to update their own passwords
      @router.patch  '/users/:username/password', @keycloak.protect('realm:user'), @updateUserPassword
      # TODO:
      # - Consider managing groups

      # Alarms API routes. RestAPI is implemented by kualarm-controller, so
      # Admission just proxify requests using the http-proxy-middledware package.
      # Note: the interceptor "fixRequestBody" is used to fix proxied POST
      # requests when bodyParser is applied before this middleware.
      alarmProxy = httpProxyMiddleware.createProxyMiddleware({
        target: @config.alarmsRestAPI.serverURL,
        changeOrigin: true,
        pathRewrite: { ['^/admission/alarms']: '' }
        onProxyReq: httpProxyMiddleware.fixRequestBody
      })

      @router.use '/alarms', @keycloak.protect('admission:administrator'), @checkAlarmsMgmt, alarmProxy


      # # ROUTES FOR TESTING (should be removed)
      #
      # @router.get '/test',                     @test
      # @router.get '/test2',                    @test
      # @router.get '/testRead',                 @testRead
      # @router.get '/list',                     @testList
      # @router.get '/events',                   @testEvents
      # @router.get '/listNodes',                @testListNodes
      # @router.get '/listPods',                 @testListPods
      # @router.get '/topNodes',                 @testTopNodes
      # @router.get '/nodeMetrics',              @testNodeMetrics
      # @router.get '/podMetrics',               @testPodMetrics
      # @router.get '/podLogs',                  @testPodLogs
      # @router.get '/testInstance/:instance?',  @testInstanceOwner
      # @router.get '/listRevisions',            @testListControllerRevisions
      # @router.get '/listOneRevision/:rev',     @testGetControllerRevision
      # @router.get '/testGetPvcMetrics/:pvc',   @testGetVolumeMetrics



      ##########################################################################
      ##                       Management API Router                          ##
      ##########################################################################
      @managementRouter = express.Router()

      # Management API routes
      # @managementRouter.get '/test', keycloak.protect('realm:user'), @test
      @managementRouter.get  '/health', @health
      @managementRouter.get  '/ready', @ready
      @managementRouter.get  '/maintenance/:action?', @keycloak.protect('admission:administrator'), @maintenance
      @managementRouter.get  '/reschedule/:type', @keycloak.protect('admission:administrator'), @checkRescheduling, @getRescheduleJobs
      @managementRouter.post '/reschedule/:type', @keycloak.protect('admission:administrator'), @checkRescheduling, @reschedule
      @managementRouter.post '/clearlocks', @keycloak.protect('admission:administrator'), @clearOperationLocks
      @managementRouter.post '/shutdown', @keycloak.protect('admission:administrator'), @shutdown



  terminate:  ->
    meth = 'AdmissionRestAPI.terminate()'
    @logger.info meth
    @bundleRegistry.close()
    @keycloakUserManager.terminate()
    @adm.terminate()
    .then () =>
      @logger.info "#{meth} - Terminated."
      return true


  setupContext: (req) ->
    meth = 'AdmissionRestAPI.setupContext()'
    @logger.debug {meth}
    @logger.debug "#{meth} - "
    authData = getAuthData req
    if authData isnt null
      context =
        user: authData.user
    else if @config.authentication?.keycloakConfig?
      # If authentication is enabled but no auth data, this must be from a
      # non-protected path. Create a guest user with no permissions.
      context =
        user:
          id: 'anonymous'
          name: 'Anonymous unauthenticated user'
          email: ''
          roles: [ 'GUEST' ]
          limits: null
    else if @config.authentication?.clientCertificateConfig?
      # If authentication is enabled but no auth data, this must be from a
      # non-protected path. Create a guest user with no permissions.
      context =
        user:
          id: 'anonymous'
          name: 'Anonymous unauthenticated user'
          email: ''
          roles: [ 'GUEST' ]
          limits: null
    else
      # If authentication is disabled, return a mock user with full rights
      context =
        user:
          id: 'anonymous'
          name: 'Anonymous user (authentication disabled)'
          email: ''
          roles: [ 'GUEST', 'DEVEL', 'ADMIN' ]
          limits: null
    @logger.debug "#{meth} - Context: #{JSON.stringify context}"
    return context


  getRouter: () ->
    @router


  getManagementRouter: () ->
    @managementRouter


  getLoginRouter: () ->
    @loginRouter


  ##############################################################################
  ##                         DEV TEST METHODS (TEMPORARY)                     ##
  ##############################################################################
  test: (req, res) =>

    userData = getUserData req
    context = @setupContext req
    msg = "Access granted to user #{context.user.id} (Roles: #{context.user.roles})"
    res.json { message: msg, context: context, userData: userData }


  escapeLabelValue: (str) ->
    LABEL_ESCAPED_CHARS =
      '@': '__arroba__'

    for k, v of LABEL_ESCAPED_CHARS
      str = str.split(k).join(v)
    return str


  testList: (req,res) =>
    meth = 'AdmissionRestAPI.testList'
    context = @setupContext req
    @logger.info "#{meth} - User: #{context.user.id || context.user.name || ''}"

    results = []

    owner = context?.user?.id
    filter =
      'kumori/owner': context?.user?.id || 'ANONYMOUS'

    # labelSelector = 'domain=jferrer.dev.testing'
    labelSelector = ''
    for k,v of filter
      if labelSelector isnt '' then labelSelector += ','
      labelSelector += k + '=' + @escapeLabelValue(v)

    # elementPlural = 'kukucomponents'
    # labelSelector = 'domain=jferrer.dev.testing'
    elementPlural = 'kukuvhosts'


    @k8sApiStub.listKumoriElements elementPlural, labelSelector
    .then (items) =>
      console.log "LIST FINISHED OK AND RETURNED #{items.length} RESULTS"
      manifests = []
      for it in items
        if it.spec?.kumoriManifest?
          manifest = JSON.parse (utils.gunzipString  \
            it.spec.kumoriManifest)
          manifests.push manifest
        else if it.metadata?.annotations?['kumori/manifest']?
          manifest = JSON.parse (utils.gunzipString  \
            it.metadata.annotations['kumori/manifest'])
          manifests.push manifest
        else
          console.log "WARNING: ITEM HAS NO ORIGINAL ECLOUD MANIFEST!"
      response =
        success : true
        message: "GOT #{manifests.length} MANIFESTS",
        data: manifests
      res.send response
      return true
    .catch (err) =>
      console.log "LIST FAILED"
      console.log "ERROR: #{err.message} - #{err.stack}"
      response =
        success : false
        message: 'LIST FAILED',
        data: results
      res.send response
      return true


  testListNodes: (req,res) =>
    meth = 'AdmissionRestAPI.testListNodes'
    context = @setupContext req
    @logger.info "#{meth} - User: #{context.user.id || context.user.name || ''}"

    results = []

    owner = context?.user?.id
    filter =
      'kumori/owner': context?.user?.id || 'ANONYMOUS'


    @k8sApiStub.listNodes()
    .then (items) =>
      console.log "LIST FINISHED OK AND RETURNED #{items.length} RESULTS"
      response =
        success : true
        message: "GOT #{items} NODES",
        data: items
      res.send response
      return true
    .catch (err) =>
      console.log "LIST FAILED"
      console.log "ERROR: #{err.message} - #{err.stack}"
      response =
        success : false
        message: 'LIST FAILED',
        data: results
      res.send response
      return true


  testTopNodes: (req,res) =>
    meth = 'AdmissionRestAPI.testTopNodes'
    context = @setupContext req
    @logger.info "#{meth} - User: #{context.user.id || context.user.name || ''}"

    results = []

    owner = context?.user?.id
    filter =
      'kumori/owner': context?.user?.id || 'ANONYMOUS'


    # q.reject new Error 'NOT IMPLEMENTED'
    @k8sApiStub.topNodes()
    .then (items) =>
      console.log "TOP FINISHED AND RETURNED #{items}"
      response =
        success : true
        message: "GOT TOP NODES DATA",
        data: items
      res.send response
      return true
    .catch (err) =>
      console.log "TOP FAILED"
      console.log "ERROR: #{err.message} - #{err.stack}"
      response =
        success : false
        message: 'TOP FAILED',
        data: results
      res.send response
      return true


  testNodeMetrics: (req,res) =>
    meth = 'AdmissionRestAPI.testNodeMetrics'
    context = @setupContext req
    @logger.info "#{meth} - User: #{context.user.id || context.user.name || ''}"

    results = []

    owner = context?.user?.id
    filter =
      'kumori/owner': context?.user?.id || 'ANONYMOUS'


    # q.reject new Error 'NOT IMPLEMENTED'
    @k8sApiStub.getNodesMetrics()
    .then (items) =>
      console.log "GET NODE METRICS FINISHED AND RETURNED #{items}"
      response =
        success : true
        message: "GOT NODE METRICS DATA",
        data: items
      res.send response
      return true
    .catch (err) =>
      console.log "GET NODE METRICS FAILED"
      console.log "ERROR: #{err.message} - #{err.stack}"
      response =
        success : false
        message: 'GET NODE METRICS FAILED',
        data: results
      res.send response
      return true


  testPodMetrics: (req,res) =>
    meth = 'AdmissionRestAPI.testPodMetrics'
    context = @setupContext req
    @logger.info "#{meth} - User: #{context.user.id || context.user.name || ''}"

    results = []

    owner = context?.user?.id
    filter =
      'kumori/owner': context?.user?.id || 'ANONYMOUS'


    # q.reject new Error 'NOT IMPLEMENTED'
    @k8sApiStub.getPodsMetrics()
    .then (items) =>
      console.log "GET POD METRICS FINISHED AND RETURNED #{items}"
      console.log "DATA: #{JSON.stringify items}"
      response =
        success : true
        message: "GOT POD METRICS DATA",
        data: items
      res.send response
      return true
    .catch (err) =>
      console.log "GET POD METRICS FAILED"
      console.log "ERROR: #{err.message} - #{err.stack}"
      response =
        success : false
        message: 'GET POD METRICS FAILED',
        data: results
      res.send response
      return true


  testPodLogs: (req,res) =>
    meth = 'AdmissionRestAPI.testPodLogs'
    context = @setupContext req
    @logger.info "#{meth} - User: #{context.user.id || context.user.name || ''}"

    results = []

    owner = context?.user?.id
    filter =
      'kumori/owner': context?.user?.id || 'ANONYMOUS'


    # q.reject new Error 'NOT IMPLEMENTED'
    @k8sApiStub.altGetPodLogs()
    .then (items) =>
      console.log "GET POD LOGS FINISHED AND RETURNED #{items}"
      console.log "DATA: #{JSON.stringify items}"
      response =
        success : true
        message: "GOT POD LOGS DATA",
        data: items
      res.send response
      return true
    .catch (err) =>
      console.log "GET POD LOGS FAILED"
      console.log "ERROR: #{err.message} - #{err.stack}"
      response =
        success : false
        message: 'GET POD LOGS FAILED',
        data: results
      res.send response
      return true


  testListPods: (req,res) =>
    meth = 'AdmissionRestAPI.testListPods'
    context = @setupContext req
    @logger.info "#{meth} - User: #{context.user.id || context.user.name || ''}"

    results = []

    owner = context?.user?.id
    filter =
      'kumori/owner': context?.user?.id || 'ANONYMOUS'


    @k8sApiStub.listPods()
    .then (items) =>
      console.log "LIST FINISHED OK AND RETURNED #{items.length} RESULTS"
      response =
        success : true
        message: "GOT #{items} PODS",
        data: items
      res.send response
      return true
    .catch (err) =>
      console.log "LIST FAILED"
      console.log "ERROR: #{err.message} - #{err.stack}"
      response =
        success : false
        message: 'LIST FAILED',
        data: results
      res.send response
      return true


  testEvents: (req,res) =>
    meth = 'AdmissionRestAPI.testEvents'
    context = @setupContext req
    @logger.info "#{meth} - User: #{context.user.id || context.user.name || ''}"

    results = []

    owner = context?.user?.id
    filter =
      'kumori/owner': context?.user?.id || 'ANONYMOUS'


    @k8sApiStub.listEvents()
    .then (items) =>
      console.log "LIST EVENTS FINISHED OK AND RETURNED #{items.length} RESULTS"
      response =
        success : true
        message: "GOT EVENTS",
        data: items
      res.send response
      return true
    .catch (err) =>
      console.log "LIST EVENTS FAILED"
      console.log "ERROR: #{err.message} - #{err.stack}"
      response =
        success : false
        message: 'LIST EVENTS FAILED',
        data: results
      res.send response
      return true


  testRead: (req,res) =>
    meth = 'AdmissionRestAPI.testRead'
    context = @setupContext req
    @logger.info "#{meth} - User: #{context.user.id || context.user.name || ''}"

    urns = [
      'eslap://jferrer.dev.testing/components/first/1_0_0'
      'eslap://jferrer.dev.testing/services/myfirst/1_0_0'
    ]

    promises = []
    results = {}
    for urn in urns
      do (urn) =>
        promises.push (
          console.log "READING MANIFEST: #{urn}"
          @manifestRepository.getManifest urn
          .then (manifest) =>
            console.log "#{JSON.stringify manifest, null, 2}"
            results[urn] = manifest
            return true
          .catch (err) =>
            console.log "ERROR: #{err.message}"
          .catch (err) =>
            console.log "ERROR: #{err.message}"
            results[urn] = "ERROR: #{err.message}"
            return true
        )
    q.allSettled promises
    .then () =>
      console.log "RESULTS: #{JSON.stringify results, null, 2}"
      response =
        success : true
        message: 'GOT MANIFESTS',
        data: results
      res.send response
      return true


  testInstanceOwner: (req,res) =>
    meth = 'AdmissionRestAPI.testInstanceOwner'
    context = @setupContext req
    console.log "#{meth} - User: #{context.user.id || context.user.name || ''}"

    # Retrieve request parameters
    instanceId   = getFromReq req, 'instance'

    console.log "#{meth} - Parameters: instanceId=#{instanceId}"

    # Validate mandatory parameter instanceId is present
    if not instanceId?
      errMsg = 'Missing mandatory parameter (instance ID)'
      console.log "#{meth} - ERROR: #{errMsg}"
      response =
        success : false
        message: errMsg
      res.send response
      return true

    # Validate user has permissions to access the requested instance
    console.log "#{meth} - Checking user permissions to access instance #{instanceId}"
    @authorization.isAllowed 'instance', instanceId, context
    .then (allowed) =>
      if not allowed
        errMsg = "User is not allowed to access instance."
        console.log "#{meth} - ERROR: #{errMsg}"
        response =
          success : false
          message: errMsg
        res.send response
        return true
      else
        msg = "User #{context.user.id} can access instance #{instanceId}."
        console.log "#{meth} - #{msg}"
        response =
          success : true
          message: msg
          data: null
        res.send response
        return true
    .catch (err) =>
      console.log "#{meth} - ERROR: #{err.message} - #{err.stack}"
      response =
        success : false
        message: 'Instance permissions check failed',
        data: results
      res.send response
      return true


  testListControllerRevisions: (req,res) =>
    meth = 'AdmissionRestAPI.testListControllerRevisions'
    context = @setupContext req
    @logger.info "#{meth} - User: #{context.user.id || context.user.name || ''}"

    results = []

    owner = context?.user?.id
    filter =
      'kumori/owner': context?.user?.id || 'ANONYMOUS'


    @k8sApiStub.getControllerRevisions KUMORI_NAMESPACE
    .then (items) =>
      console.log "LIST FINISHED OK AND RETURNED #{items.length} RESULTS"
      console.log "CONVERTING LIST TO SIMPLIFIED LIST"

      list = []
      for it in items
        if it.metadata?.annotations? \
        and 'kumori/history' of it.metadata.annotations
          console.log JSON.stringify it, null, 2
          history = JSON.parse it.metadata.annotations['kumori/history']
          for hit in history
            newItem =
              name: it.metadata.name
              revision: hit.revision
              timestamp: hit.timestamp
              comment: hit.comment
            list.push newItem
      console.log "SIMPLIFIED LIST : #{JSON.stringify list, null, 2}"

      response =
        success : true
        message: "GOT #{items.length || 0} CONTROLLERREVISIONS",
        data: list
      res.send response
      return true
    .catch (err) =>
      console.log "LIST FAILED"
      console.log "ERROR: #{err.message} - #{err.stack}"
      response =
        success : false
        message: 'LIST FAILED',
        data: results
      res.send response
      return true


  testGetControllerRevision: (req,res) =>
    meth = 'AdmissionRestAPI.testGetControllerRevision'
    context = @setupContext req
    @logger.info "#{meth} - User: #{context.user.id || context.user.name || ''}"

    revName = getFromReq req, 'rev'
    revName = 'kd-083221-552575c6-6bdb9d7d5'

    owner = context?.user?.id
    filter =
      'kumori/owner': context?.user?.id || 'ANONYMOUS'


    @k8sApiStub.getControllerRevision KUMORI_NAMESPACE, revName
    .then (result) =>
      console.log "RETRIEVED REVISION"
      response =
        success : true
        message: "GOT CONTROLLERREVISIONS",
        data: result
      res.send response
      return true
    .catch (err) =>
      console.log "GET FAILED"
      console.log "ERROR: #{err.message} - #{err.stack}"
      response =
        success : false
        message: 'GET FAILED',
        data: {}
      res.send response
      return true


  testGetVolumeMetrics: (req,res) =>
    meth = 'AdmissionRestAPI.testGetVolumeMetrics'
    context = @setupContext req
    @logger.info "#{meth} - User: #{context.user.id || context.user.name || ''}"

    pvcName = getFromReq req, 'pvc'
    pvcvName = 'c0volume0-kd-164933-0b08f516-worker000-deployment-0'

    owner = context?.user?.id
    filter =
      'kumori/owner': context?.user?.id || 'ANONYMOUS'

    result = {}
    @k8sApiStub.getPvcMetrics KUMORI_NAMESPACE, pvcName
    .then (metrics) =>
      console.log "RETRIEVED VOLUME METRICS"
      response =
        success : true
        message: "GOT VOLUME METRICS",
        data: metrics
      res.send response
      return true
    .catch (err) =>
      console.log "GET VOLUME METRICS FAILED"
      console.log "ERROR: #{err.message} - #{err.stack}"
      response =
        success : false
        message: 'GET VOLUME METRICS FAILED',
        data: {}
      res.send response
      return true


  ##############################################################################
  ##                     END OF DEV TEST METHODS (TEMPORARY)                  ##
  ##############################################################################



  ##############################################################################
  ##                     GENERIC "NOT IMPLMENTED" API METHODS                 ##
  ##############################################################################
  apiNotImplemented: (req,res) ->
    response =
      success : false
      message: 'API METHOD NOT IMPLEMENTED YET',
      data: {}
    res.send response
    return true


  ##############################################################################
  ##                  MIDDLEWARE FOR USER MANAGEMENT DISABLED                 ##
  ##############################################################################
  checkUserMgmt: (req, res, next) =>
    meth = 'AdmissionRestAPI.checkUserMgmt'
    if @config.authentication?.keycloakConfig?
      next()
    else
      msg = "User management is currently disabled in Admission."
      @logger.warn "#{meth} - #{msg}"
      response =
        success : false
        message: msg
      res.json response


  ##############################################################################
  ##                    MIDDLEWARE FOR RESCHEDULER DISABLED                   ##
  ##############################################################################
  checkRescheduling: (req, res, next) =>
    meth = 'AdmissionRestAPI.checkRescheduling'
    if @config.rescheduling?.enabled? and @config.rescheduling?.enabled
      next()
    else
      msg = "Rescheduling is currently disabled in the cluster."
      @logger.warn "#{meth} - #{msg}"
      response =
        success : false
        message: msg
      res.json response


  ##############################################################################
  ##                    MIDDLEWARE FOR RESCHEDULER DISABLED                   ##
  ##############################################################################
  checkAlarmsMgmt: (req, res, next) =>
    meth = 'AdmissionRestAPI.checkAlarmsMgmt'
    if @config.alarmsRestAPI?.enabled? and @config.alarmsRestAPI?.enabled
      next()
    else
      msg = "Alarms are currently disabled in the cluster."
      @logger.warn "#{meth} - #{msg}"
      response =
        success : false
        message: msg
      res.json response


  ##############################################################################
  ##            MIDDLEWARE FOR LOGGING USAGE OF DEPRECATED METHOD             ##
  ##############################################################################
  deprecated: (req, res, next) =>
    @logger.warn "*************************************************************"
    @logger.warn "** DEPRECATED API METHOD USED: #{req.method} #{req.path}"
    @logger.warn "*************************************************************"
    next()


  ##############################################################################
  ##                   MANAGEMENT API IMPLEMENTATION METHODS                  ##
  ##############################################################################
  health: (req,res) ->
    res.status(200).json({status:"healthy"})
    return true


  ready: (req,res) ->
    res.status(200).json({status:"ready"})
    return true


  processShutdown: (context) =>
    meth = 'AdmissionRestAPI.processShutdown'
    @logger.info meth

    # If shutdown has been completed just return
    if @shutdownStatus.completed
      @logger.info "#{meth} - Admission cluster shutdown is complete."
      return q()

    # If shutdown has been triggered, just return the current status
    if @shutdownStatus.initiated
      @logger.info "#{meth} - Admission cluster shutdown is ongoing."
      return q()

    # Initiate the shutdown process, but leave it run in the background and
    # return
    @shutdownOperations context
    .done()

    return q()


  shutdownOperations: (context) =>
    meth = 'AdmissionRestAPI.shutdownOperations'
    @logger.info meth

    # This method will launch the necessary cleanup operations

    @shutdownStatus.initiated = true

    q()
    .then () =>
      # Delete solutions
      @shutdownRemoveSolutions context
    .then () =>
      # If all solutions are deleted, delete all Volumes
      if @shutdownStatus.solutionsDeleted
        @shutdownRemoveVolumes context
      else
        return true
    .then () =>
      # If all solutions and volumes are deleted, delete all Ingress objects
      if @shutdownStatus.solutionsDeleted \
      and @shutdownStatus.volumesDeleted
        @shutdownRemoveIngresses context
      else
        return true
    .then () =>
      # If all solutions, volumes and ingresses are deleted, delete the Ingress
      # LoadBalancer Kubernetes service
      if @shutdownStatus.solutionsDeleted \
      and @shutdownStatus.volumesDeleted \
      and @shutdownStatus.ingressesDeleted
        @shutdownRemoveIngressLBService context
      else
        return true
    .then () =>
      # If all solutions, volumes ingresses and the Ingress LoadBalancer service
      # are deleted, then the cleanup is complete.
      # If not, wait and make a recursive call to this same method.
      if @shutdownStatus.solutionsDeleted \
      and @shutdownStatus.volumesDeleted \
      and @shutdownStatus.ingressesDeleted \
      and @shutdownStatus.ingressLoadBalancerDeleted
        @shutdownStatus.completed = true
        return true
      else
        # Wait for some seconds and make a recursive call to this same method
        q.delay SHUTDOWN_RETRY_WAIT_MILLIS
        .then () =>
          return @shutdownOperations context
    .catch (err) =>
      # Ignore any error (just log it) and retry.
      @logger.warn "#{meth} - ERROR (ignored and retrying): #{err.message}"

      # Wait for some seconds and make a recursive call to this same method
      q.delay SHUTDOWN_RETRY_WAIT_MILLIS
      .then () =>
        return @shutdownOperations context



  shutdownRemoveSolutions: (context) =>
    meth = 'AdmissionRestAPI.shutdownRemoveSolutions'
    @logger.info meth

    if @shutdownStatus.solutionsDeleted
      @logger.info "#{meth} - Solutions already deleted, nothing to do."
      return q true

    errors = []

    # List all Solutions
    @manifestRepository.listElementType 'solutions'
    .then (solutionList) =>
      if Object.keys(solutionList).length is 0
        # No solutions found
        msg = "No solutions found."
        @shutdownStatus.messages.push "#{utils.getTimestamp()} #{msg}"
        @logger.debug "#{meth} - #{msg}"
        @shutdownStatus.solutionsDeleted = true
        return true

      @logger.info "#{meth} - Found #{Object.keys(solutionList).length} solutions."

      # Delete all solutions (chain operations as promises)
      promiseChain = q()
      for solURN, solData of solutionList
        if solData.deletionTimestamp?
          # If solution is already being deleted, skip it
          continue
        else
          do (solURN, solData) =>
            promiseChain = promiseChain.finally () =>
              msg = "Deleting solution: #{solURN}"
              @shutdownStatus.messages.push "#{utils.getTimestamp()} #{msg}"
              @logger.debug "#{meth} - #{msg}"

              @adm.deleteSolution context, {
                solutionURN: solURN,
                force: true
              }
              .then (solutionDeletionInfo) =>
                msg = "Deleted solution: #{solURN}"
                @shutdownStatus.messages.push "#{utils.getTimestamp()} #{msg}"
                @logger.debug "#{meth} - #{msg}"
                return true
              .fail (error) =>
                msg = "Failed to delete solution #{solURN} : #{err.message || err}"
                @shutdownStatus.messages.push "#{utils.getTimestamp()} #{msg}"
                errors.push msg
                @logger.error "ERROR: #{msg} - ", error.stack || error
                return true

      promiseChain.finally () =>
        if errors.length > 0
          # Some errors detected deleting the solutions
          msg = "All solutions deletion processed. Numer of errors: #{errors.length}"
          @shutdownStatus.messages.push "#{utils.getTimestamp()} #{msg}"
          @logger.info "#{meth} #{msg}"
          return false
        else
          msg = "All solutions deletion processed. No errors detected."
          @shutdownStatus.messages.push "#{utils.getTimestamp()} #{msg}"
          @logger.info "#{meth} #{msg}"
          return true



  shutdownRemoveIngresses: (context) =>
    meth = 'AdmissionRestAPI.shutdownRemoveIngresses'
    @logger.info meth

    if @shutdownStatus.ingressesDeleted
      @logger.info "#{meth} - Ingress objects already deleted, nothing to do."
      return q true

    errors = []

    # List all Solutions
    @k8sApiStub.listIngresses()
    .then (items) =>
      if (not items?) or (items.length is 0)
        # No ingresses found
        msg = "No Ingress objects found."
        @shutdownStatus.messages.push "#{utils.getTimestamp()} #{msg}"
        @logger.debug "#{meth} - #{msg}"
        @shutdownStatus.ingressesDeleted = true
        return true

      @logger.info "#{meth} - Found #{items.length} Ingress objects."

      # Delete all ingresses (chain operations as promises)
      promiseChain = q()
      for item in items
        if item.metadata?.deletionTimestamp?
          # If ingress is already being deleted, skip it
          continue
        else
          ns = item.metadata.namespace
          name = item.metadata.name
          do (ns, name) =>
            promiseChain = promiseChain.finally () =>
              msg = "Deleting Ingress object #{ns}/#{name}..."
              @shutdownStatus.messages.push "#{utils.getTimestamp()} #{msg}"
              @logger.debug "#{meth} - #{msg}"

              @k8sApiStub.deleteIngress ns, name
              .then () =>
                msg = "Deleted ingress: #{ns}/#{name}"
                @shutdownStatus.messages.push "#{utils.getTimestamp()} #{msg}"
                @logger.debug "#{meth} - #{msg}"
                return true
              .catch (error) =>
                msg = "Failed to delete Ingress #{ns}/#{name} : #{err.message || err}"
                @shutdownStatus.messages.push "#{utils.getTimestamp()} #{msg}"
                errors.push msg
                @logger.error "ERROR: #{msg} - ", error.stack || error
                return true
      promiseChain.finally () =>
        if errors.length > 0
          # Some errors detected deleting the ingresses
          msg = "All ingresses deletion processed. Numer of errors: #{errors.length}"
          @shutdownStatus.messages.push "#{utils.getTimestamp()} #{msg}"
          @logger.info "#{meth} #{msg}"
          return false
        else
          msg = "All ingresses deletion processed. No errors detected."
          @shutdownStatus.messages.push "#{utils.getTimestamp()} #{msg}"
          @logger.info "#{meth} #{msg}"
          return true



  shutdownRemoveVolumes: (context) =>
    meth = 'AdmissionRestAPI.shutdownRemoveVolumes'
    @logger.info meth

    if @shutdownStatus.volumesDeleted
      @logger.info "#{meth} - Volumes already deleted, nothing to do."
      return q true

    errors = []

    # List all Volumes
    @manifestRepository.listElementType 'volumes'
    .then (volumeList) =>
      if Object.keys(volumeList).length is 0
        # No volumes found
        msg = "No volumes found."
        @shutdownStatus.messages.push "#{utils.getTimestamp()} #{msg}"
        @logger.debug "#{meth} - #{msg}"
        @shutdownStatus.volumesDeleted = true
        return true

      @logger.info "#{meth} - Found #{Object.keys(volumeList).length} volumes."

      # Delete all volumes (chain operations as promises)
      promiseChain = q()
      for volURN, volData of volumeList
        if volData.deletionTimestamp?
          # If volume is already being deleted, skip it
          continue
        else
          do (volURN, volData) =>
            promiseChain = promiseChain.finally () =>
              msg = "Deleting volume: #{volURN} (#{volData.name})"
              @shutdownStatus.messages.push "#{utils.getTimestamp()} #{msg}"
              @logger.debug "#{meth} - #{msg}"

              @k8sApiStub.deleteKumoriElement 'kukuvolumes', volData.name
              .then (volumeDeletionInfo) =>
                msg = "Deleted volume: #{volURN}"
                @shutdownStatus.messages.push "#{utils.getTimestamp()} #{msg}"
                @logger.debug "#{meth} - #{msg}"
                return true
              .fail (error) =>
                msg = "Failed to delete volume #{volURN} : #{err.message || err}"
                @shutdownStatus.messages.push "#{utils.getTimestamp()} #{msg}"
                errors.push msg
                @logger.error "ERROR: #{msg} - ", error.stack || error
                return true

      promiseChain.finally () =>
        if errors.length > 0
          # Some errors detected deleting the volumes
          msg = "All volumes deletion processed. Numer of errors: #{errors.length}"
          @shutdownStatus.messages.push "#{utils.getTimestamp()} #{msg}"
          @logger.info "#{meth} #{msg}"
          return false
        else
          msg = "All volumes deletion processed. No errors detected."
          @shutdownStatus.messages.push "#{utils.getTimestamp()} #{msg}"
          @logger.info "#{meth} #{msg}"
          return true


  shutdownRemoveIngressLBService: (context) =>
    meth = 'AdmissionRestAPI.shutdownRemoveIngressLBService'
    @logger.info meth

    if @shutdownStatus.ingressLoadBalancerDeleted
      @logger.info "#{meth} - Ingress LB Service already deleted, nothing to do."
      return q true

    # Determine the Ingress Service name
    clusterName = @latestClusterConfiguration.clusterName
    ingressServiceName = INGRESS_SERVICE_OBJECT_PREFIX + clusterName

    # Check if the Ingress Service still exists
    @k8sApiStub.getService KUMORI_NAMESPACE, ingressServiceName
    .then (ingressService) =>
      if ingressService.metadata.deletionTimestamp?
        # If Ingress Service is already being deleted, skip it
        @logger.info "#{meth} - Kubernetes Service #{ingressServiceName} exists
                      and is already marked for deletion."
        return true
      else
        # Ingress Service is not marked for deletion, delete it
        @logger.info "#{meth} - Kubernetes Service #{ingressServiceName} exists,
                      delete it..."
        @shutdownStatus.messages.push "#{utils.getTimestamp()} Deleting Ingress
                                       Service object."

        @k8sApiStub.deleteService KUMORI_NAMESPACE, ingressServiceName
        .then () =>
          @logger.info "#{meth} - Kubernetes Service #{ingressServiceName} has
                        been marked for deletion."
          return true
        .catch (error) =>
          errMsg = "Failed to delete Kubernetes Service #{ingressServiceName}.
                    ERROR: #{error.message}"
          @shutdownStatus.messages.push "#{utils.getTimestamp()} #{errMsg}"
          @logger.error "#{meth} - #{errMsg}"
          return true
    .catch (error) =>
      # Check if the error is due to the object not existing (HTTP status 404)
      if (error.message?.includes 'NotFound') or
      (error.message?.includes 'StatusCode 404')

        # Service does not exist (has been deleted), mark it
        @shutdownStatus.ingressLoadBalancerDeleted = true

        msg = "Kubernetes Service #{ingressServiceName} does not exist."
        @logger.info "#{meth} - #{msg}"
        @shutdownStatus.messages.push "#{utils.getTimestamp()} #{msg}"
        return true
      else
        # Some other error while getting the Service object
        errMsg = "Failed to get Kubernetes Service #{ingressServiceName}.
                  ERROR: #{error.message}"
        @shutdownStatus.messages.push "#{utils.getTimestamp()} #{errMsg}"
        @logger.error "#{meth} - #{errMsg}"
        return true



  shutdown: (req, res) =>
    meth = 'AdmissionRestAPI.shutdown'
    @logger.info meth

    context = @setupContext req
    @logger.info "#{meth} - User: #{context.user.id || context.user.name || ''}"

    # The shutdown process will be carried out in the background, so this call
    # will return immediately
    @processShutdown context
    .then () =>
      @logger.info "#{meth} - Processed shutdown request."
      response =
        success : true
        message: "Cluster shutdown request processed."
        data: @shutdownStatus
      res.json response
      return true
    .catch (error) =>
      @logger.error "#{meth} - ERROR: #{error.message}", error.stack
      response =
        success : false
        message: "Failed to process shutdown request."
        error: error.stack
      res.json response
      return false


  maintenance: (req, res) =>
    meth = 'AdmissionRestAPI.maintenance'
    @logger.info meth

    context = @setupContext req
    action = getFromReq req, 'action'
    @logger.info "#{meth} - User: #{context.user.id || context.user.name || ''}
                  - Action: #{action}"

    if not action?
      errMsg = "missing mandatory parameter: action"
      @logger.warn "#{meth} - ERROR: #{errMsg}"
      response =
        success : false
        message: "Unable to process maintenance request: #{errMsg}"
      res.json response
    else if action?.toLowerCase() is 'offline'
      # Set maintenance mode for Admission API
      @downForMaintenance = true
      response =
        success : true
        message: 'Admission API is now down for maintenance.'
        data: null
      res.json response
    else if action?.toLowerCase() is 'online'
      # Unset maintenance mode for Admission API
      @downForMaintenance = false
      response =
        success : true
        message: 'Admission API is now online.'
        data: null
      res.json response
    else
      errMsg = "unknown maintenance action: #{action}"
      @logger.warn "#{meth} - ERROR: #{errMsg}"
      response =
        success : false
        message: "Unable to process action: #{errMsg}"
      res.send response


  reschedule: (req, res) =>
    meth = 'AdmissionRestAPI.reschedule'
    @logger.info meth

    context = @setupContext req
    rescheduleType = getFromReq req, 'type'
    @logger.info "#{meth} - User: #{context.user.id || context.user.name || ''}
                  - Type: #{rescheduleType}"

    # Get reschedule type from request
    if not rescheduleType?
      errMsg = "missing mandatory parameter: type"
      @logger.warn "#{meth} - ERROR: #{errMsg}"
      response =
        success : false
        message: "Unable to process reschedule request: #{errMsg}"
      res.json response
      return

    # Get config provided
    rescheduleConfig = req.body
    if not rescheduleConfig?
      rescheduleConfig = null
      @logger.info "#{meth} - No reschedule configuration provided. Defaults
                    will be used if available."

    # Instantiate a rescheduler object and create a job
    reschedulerOptions =
      k8sApiStub: @k8sApiStub
    rescheduler = ReschedulerFactory.create rescheduleType, reschedulerOptions

    rescheduler.createJob rescheduleConfig
    .then (result) =>
      @logger.info "#{meth} - Reschedule job created."
      response =
        success : true
        message: "Reschedule request successfully processed."
        data: null
      res.json response
      return true
    .catch (error) =>
      @logger.error "#{meth} - ERROR: #{error.message}", error.stack
      response =
        success : false
        message: "Failed to process reschedule request: #{error.message}"
        error: error.stack
      res.json response
      return false


  getRescheduleJobs: (req, res) =>
    meth = 'AdmissionRestAPI.getRescheduleJobs'
    @logger.info meth

    context = @setupContext req
    rescheduleType = getFromReq req, 'type'
    @logger.info "#{meth} - User: #{context.user.id || context.user.name || ''}
                  - Type: #{rescheduleType}"

    # Get reschedule type from request
    if not rescheduleType?
      errMsg = "missing mandatory parameter: type"
      @logger.warn "#{meth} - ERROR: #{errMsg}"
      response =
        success : false
        message: "Unable to process reschedule request: #{errMsg}"
      res.json response
      return

    # Instantiate a rescheduler object and get job list
    reschedulerOptions =
      k8sApiStub: @k8sApiStub
    rescheduler = ReschedulerFactory.create rescheduleType, reschedulerOptions

    rescheduler.listJobs()
    .then (result) =>
      @logger.info "#{meth} - Got job list."
      response =
        success : true
        message: "Retrieved reschedule job list."
        data: result
      res.json response
      return true
    .catch (error) =>
      @logger.error "#{meth} - ERROR: #{error.message}", error.stack
      response =
        success : false
        message: "Failed to process reschedule request: #{error.message}"
        error: error.stack
      res.json response
      return false


  clearOperationLocks: (req, res) =>
    meth = 'AdmissionRestAPI.clearOperationLocks'
    @logger.info meth

    context = @setupContext req
    @logger.info "#{meth} - User: #{context.user.id || context.user.name || ''}
                  - Clear operation locks"
    @adm.clearElementLocks()

    response =
      success : true
      message: 'Operation locks cleared.'
      data: null
    res.json response
    true


  ##############################################################################
  ##                 AUTHENTICATION API IMPLEMENTATION METHODS                ##
  ##############################################################################

  refreshToken: (req, res) =>
    meth = 'AdmissionRestAPI.refreshToken'
    @logger.info meth

    if @config.authentication?.keycloakConfig?

      # Refresh token is received as POST data
      if 'refresh_token' not of req.body
        msg = "Mandatory 'request_token' parameter not found."
        @logger.warn "#{meth} - #{msg}"
        res.json { message: msg }
        return

      if 'grant_type' not of req.body
        msg = "Mandatory 'grant_type' parameter not found."
        @logger.warn "#{meth} - #{msg}"
        res.json { message: msg }
        return

      if req.body['grant_type'] isnt 'refresh_token'
        msg = "Unsupported 'grant_type' for refresh token method
               (#{req.body['grant_type']})"
        @logger.warn "#{meth} - #{msg}"
        res.json { message: msg }
        return

      refreshToken = req.body['refresh_token']

      kcReq = new KeycloakRequest @config.authentication.keycloakConfig
      @logger.info "#{meth} - GETTING FRESH TOKEN FROM KEYCLOAK..."
      result = null
      kcReq.refreshAccessToken refreshToken
      .then (data) =>
        result = data
        @logger.info "#{meth} - GOT KEYCLOAK DATA"
        # @logger.info "#{meth} - GOT KEYCLOAK DATA: #{JSON.stringify data}"
        # res.json { message: data }
        kcReq.getUserInfo data.access_token
      .then (data) =>
        @logger.info "#{meth} - GOT KEYCLOAK USER INFO"
        # @logger.info "#{meth} - GOT KEYCLOAK USER INFO: #{JSON.stringify data}"
        result.user = data
        # @logger.info "#{meth} - RETURNING: #{JSON.stringify result}"
        res.json result
      .catch (err) =>
        # console.log "*** LOGIN *** - KEYCLOAK ERROR: #{err.message} #{err.stack}"
        msg = "Login failed. Unable to login on Keycloak: #{err.message}"
        @logger.debug "#{meth} - #{msg}"
        res.json { message: msg }
        return
    else
      # No authentication configured in Admission

      # Return a warning stating that authentication is disabled
      msg = "Token-based authentication is currently disabled in Admission."
      @logger.warn "#{meth} - #{msg}"
      res.json { message: msg }
      return


  login: (req, res) =>

    meth = 'AdmissionRestAPI.login'

    # console.log "REQUEST HEADERS: #{JSON.stringify req.headers}"
    # console.log "REQUEST BODY: #{JSON.stringify req.body}"

    if @config.authentication?.keycloakConfig?
      # "authorization":"Basic dXN1YXJpbzpwYXNzc3Nzc3Nz"
      if req.headers?.authorization?
        authorization = req.headers.authorization
        token = authorization.split(/\s+/).pop()
        auth = Buffer.from(token, 'base64').toString()
        parts = auth.split(/:/)
        username = parts[0]
        password = parts[1]
        # console.log "LOGIN DATA: user:#{username}  password:#{password}"

        kcReq = new KeycloakRequest @config.authentication.keycloakConfig
        @logger.info "#{meth} - GETTING KEYCLOAK DATA..."
        result = null
        kcReq.getAccessToken username, password
        .then (data) =>
          result = data
          @logger.info "#{meth} - GOT KEYCLOAK DATA"
          # @logger.info "#{meth} - GOT KEYCLOAK DATA: #{JSON.stringify data}"
          # res.json { message: data }
          kcReq.getUserInfo data.access_token
        .then (data) =>
          @logger.info "#{meth} - GOT KEYCLOAK USER INFO"
          # @logger.info "#{meth} - GOT KEYCLOAK USER INFO: #{JSON.stringify data}"
          result.user = data
          # @logger.info "#{meth} - RETURNING: #{JSON.stringify result}"
          res.json result
        .catch (err) =>
          # console.log "*** LOGIN *** - KEYCLOAK ERROR: #{err.message} #{err.stack}"
          msg = "Login failed. Unable to login on Keycloak: #{err.message}"
          @logger.debug "#{meth} - #{msg}"
          res.json { message: msg }
      else
        # console.log "*** LOGIN *** - LOGIN FAILED (no authorization header)"
        msg = "Login failed. Unable to retrieve authorization header."
        @logger.debug "#{meth} - #{msg}"
        res.json { message: msg }
    else
      # No authentication configured in Admission

      # Return a warning stating that authentication is disabled
      msg = "Token-based authentication is currently disabled in Admission."
      @logger.warn "#{meth} - #{msg}"
      res.json { message: msg }



  # Old version of login that relied on keycloak-connect middleware but was
  # failing in browser (Dashboard with AdmissionClient) due to CORS headers not
  # being set by Keycloak server.
  #
  # Kept for future reference.
  oldLogin: (req, res) =>

    # For backwards compatibility with ACS, this should return an object like:
    #     access_token: token
    #     token_type: 'Bearer'
    #     expires_in: @config.accessTokenTTL
    #     refresh_token: refresh
    #     user: user
    #
    # where user object is:
    #   - email: "slapbot@iti.es"
    #   - id: "slapbot@iti.es"
    #   - limits: {max_bundle_size: -1, max_storage_space: -1, ...}
    #   - name: "slapbot"
    #   - roles: ["ADMIN"]


    # Currently, it returns:
    #
    # {
    #   "message": "Access granted to user alice@keycloak.org (Roles: developer)",
    #   "context": {
    #     "user": {
    #       "id": "alice@keycloak.org",
    #       "roles": [
    #         "developer"
    #       ]
    #     }
    #   },
    #   "userData": {
    #     "username": "alice",
    #     "name": "Alice Liddel",
    #     "email": "alice@keycloak.org",
    #     "groups": [
    #       "offline_access",
    #       "user"
    #     ],
    #     "kumoriGroups": [
    #       "developer"
    #     ]
    #   }
    # }

    meth = 'AdmissionRestAPI.login'

    authData = getAuthData req
    context = @setupContext req
    console.log "*** LOGIN *** - AUTH DATA: #{JSON.stringify authData, null, 2}"
    console.log "*** LOGIN *** - CONTEXT: #{JSON.stringify context, null, 2}"
    if authData is null
      console.log "*** LOGIN *** - LOGIN FAILED (authData is null)"
      msg = "Login failed. Unable to retrieve authentication data."
      @logger.debug "#{meth} - #{msg}"
      res.json { message: msg }
    else
      console.log "*** LOGIN *** - RETURNING: #{JSON.stringify authData, null, 2}"
      @logger.debug "#{meth} - Successful login for user: #{authData.user.id}"
      # msg = "Login successfull for user #{context.user.id} (Roles: #{context.user.roles})"
      # msg = "Access granted to user #{context.user.id} (Roles: #{context.user.roles})"
      # res.json { message: msg, context: context, userData: userData }
      res.json authData



  ##############################################################################
  ##                 USER MANAGEMENT API IMPLEMENTATION METHODS               ##
  ##############################################################################
  listUsers: (req,res) =>
    meth = 'AdmissionRestAPI.listUsers'
    context = @setupContext req
    @logger.info "#{meth} - User: #{context.user.id || context.user.name || ''}"

    results = []

    accessToken = req.kauth.grant.access_token.token

    @keycloakUserManager.getUsers accessToken
    .then (data) =>
      # console.log "LIST USERS FINISHED OK AND RETURNED #{data.length} RESULTS"
      response =
        success : true
        message: "GOT #{data.length} USERS.",
        data: data
      res.send response
      return true
    .catch (err) =>
      @logger.error "ERROR: #{err.message || err}"
      response =
        success : false
        message: "ERROR: #{err.message || err}"
      res.send response
      return true


  getUser: (req,res) =>
    meth = 'AdmissionRestAPI.getUser'
    context = @setupContext req
    @logger.info "#{meth} - User: #{context.user.id || context.user.name || ''}"

    username = getFromReq req, 'username'
    if not username
      errMsg = 'Missing mandatory parameter (username)'
      @logger.error "#{meth} ERROR: #{errMsg}"
      response =
        success : false
        message: errMsg
      res.send response
      return q()

    accessToken = req.kauth.grant.access_token.token

    @keycloakUserManager.getUser username, accessToken
    .then (data) =>
      # console.log "GET USER #{username} FINISHED OK AND RETURNED #{data}"
      response =
        success : true
        message: "GOT USER #{username}.",
        data: data
      res.send response
      return true
    .catch (err) =>
      @logger.error "ERROR: #{err.message || err}"
      response =
        success : false
        message: "ERROR: #{err.message || err}"
      res.send response
      return true


  deleteUser: (req,res) =>
    meth = 'AdmissionRestAPI.deleteUser'
    context = @setupContext req
    @logger.info "#{meth} - User: #{context.user.id || context.user.name || ''}"

    username = getFromReq req, 'username'
    if not username
      errMsg = 'Missing mandatory parameter (username)'
      @logger.error "#{meth} ERROR: #{errMsg}"
      response =
        success : false
        message: errMsg
      res.send response
      return q()

    accessToken = req.kauth.grant.access_token.token

    @keycloakUserManager.deleteUser username, accessToken
    .then (data) =>
      # console.log "DELETE USER #{username} FINISHED OK AND RETURNED #{data}"
      response =
        success : true
        message: "DELETED USER #{username}.",
        data: data
      res.send response
      return true
    .catch (err) =>
      @logger.error "ERROR: #{err.message || err}"
      response =
        success : false
        message: "ERROR: #{err.message || err}"
      res.send response
      return true


  createUser: (req,res) =>
    meth = 'AdmissionRestAPI.createUser'
    context = @setupContext req
    @logger.info "#{meth} - User: #{context.user.id || context.user.name || ''}"

    results = []

    user = req.body
    if not user
      errMsg = 'Missing mandatory user details.'
      @logger.error "#{meth} ERROR: #{errMsg}"
      response =
        success : false
        message: errMsg
      res.send response
      return q()

    accessToken = req.kauth.grant.access_token.token

    @keycloakUserManager.createUser user, accessToken
    .then (data) =>
      # console.log "CREATE USER #{user.username} FINISHED OK"
      response =
        success : true
        message: "CREATED USER #{user.username}.",
        data: data
      res.send response
      return true
    .catch (err) =>
      @logger.error "ERROR: #{err.message || err}"
      response =
        success : false
        message: "ERROR: #{err.message || err}"
      res.send response
      return true


  updateUser: (req,res) =>
    meth = 'AdmissionRestAPI.updateUser'
    context = @setupContext req
    @logger.info "#{meth} - User: #{context.user.id || context.user.name || ''}"

    results = []

    user = req.body
    if not user
      errMsg = 'Missing mandatory user details.'
      @logger.error "#{meth} ERROR: #{errMsg}"
      response =
        success : false
        message: errMsg
      res.send response
      return q()

    accessToken = req.kauth.grant.access_token.token

    @keycloakUserManager.updateUser user, accessToken
    .then (data) =>
      # console.log "UPDATE USER #{user.username} FINISHED OK"
      response =
        success : true
        message: "UPDATED USER #{user.username}.",
        data: data
      res.send response
      return true
    .catch (err) =>
      @logger.error "ERROR: #{err.message || err}"
      response =
        success : false
        message: "ERROR: #{err.message || err}"
      res.send response
      return true


  updateUserPassword: (req,res) =>
    meth = 'AdmissionRestAPI.updateUserPassword'
    context = @setupContext req
    @logger.info "#{meth} - User: #{context.user.id || context.user.name || ''}"

    username = getFromReq req, 'username'
    if not username
      errMsg = 'Missing mandatory parameter (username)'
      @logger.error "#{meth} ERROR: #{errMsg}"
      response =
        success : false
        message: errMsg
      res.send response
      return q()

    callerUsername = context?.user?.username

    isAdmin = @authorization.isAdmin context

    # A privileged operation is one requested by an admin user that modifies the
    # password of a user other than himself.
    isPrivileged = isAdmin and (username isnt callerUsername)

    # Only accept operation on the caller's own user (unless admin)
    if (username isnt callerUsername) and (not isAdmin)
      errMsg = "Operation not allowed on user #{username} (current user
                #{callerUsername})."
      @logger.error "#{meth} ERROR: #{errMsg}"
      response =
        success : false
        message: errMsg
      res.send response
      return q()

    passwordChangeData = req.body
    if not passwordChangeData
      errMsg = 'Missing mandatory password change data.'
      @logger.error "#{meth} ERROR: #{errMsg}"
      response =
        success : false
        message: errMsg
      res.send response
      return q()


    @keycloakUserManager.updateUserPassword username
    , passwordChangeData
    , isPrivileged
    .then (data) =>
      # console.log "UPDATE USER #{user.username} FINISHED OK"
      response =
        success : true
        message: "UPDATED PASSWORD FOR USER #{username}.",
        data: data
      res.send response
      return true
    .catch (err) =>
      @logger.error "ERROR: #{err.message || err}"
      response =
        success : false
        message: "ERROR: #{err.message || err}"
      res.send response
      return true


  listGroups: (req,res) =>
    meth = 'AdmissionRestAPI.listGroups'
    context = @setupContext req
    @logger.info "#{meth} - User: #{context.user.id || context.user.name || ''}"

    results = []

    accessToken = req.kauth.grant.access_token.token

    @keycloakUserManager.getGroups accessToken
    .then (data) =>
      # console.log "LIST GROUPS FINISHED OK. RETURNED #{data.length} RESULTS"
      response =
        success : true
        message: "GOT #{data.length} GROUPS.",
        data: data
      res.send response
      return true
    .catch (err) =>
      @logger.error "ERROR: #{err.message || err}"
      response =
        success : false
        message: "ERROR: #{err.message || err}"
      res.send response
      return true


  ##############################################################################
  ##                    ADMISSION API IMPLEMENTATION METHODS                  ##
  ##############################################################################
  deploymentQuery: (req,res) =>
    meth = 'AdmissionRestAPI.deploymentQuery'
    @apiRequestCount++

    context = @setupContext req
    @logger.info "#{meth} - User: #{context.user.id || context.user.name || ''}"

    queryParams = {}

    owner = context?.user?.id
    if not @authorization.isAdmin context
      queryParams.owner = context.user.id

    urn = getFromReq req, 'urn'
    if urn?
      # If getting a specific URN, return all available data
      queryParams.urn = urn
      queryParams.show = 'extended'
    else
      # If listing all deployments, return only a urn list unless a specific
      # format is specified
      if not (req.query.show? and req.query.show isnt '')
        queryParams.show = 'urn'
      else
        queryParams.show = req.query.show

    # @logger.debug "#{meth} REQ: #{JSON.stringify req.headers, null, 2}"
    @logger.info  "#{meth} URN: #{urn}"
    @logger.debug "#{meth} req.query: #{JSON.stringify req.query}"
    @logger.debug "#{meth} queryParams: #{JSON.stringify queryParams}"


    @adm.deploymentQuery(context, queryParams)
    .then (info)->
      res.json
        success : true
        message: 'SUCCESSFULLY PROCESSED DEPLOYMENT INFO.'
        data: info
    .fail (error) =>
      @logger.error 'ERROR:', error.stack || error
      res.send {
        success : false
        , message: "ERROR PROCESSING DEPLOYMENTS QUERY: #{error.message}"
      }


  solutionQuery: (req,res) =>
    meth = 'AdmissionRestAPI.solutionQuery'
    @apiRequestCount++

    context = @setupContext req
    @logger.info "#{meth} - User: #{context.user.id || context.user.name || ''}"

    queryParams = {}

    owner = context?.user?.id
    if not @authorization.isAdmin context
      queryParams.owner = context.user.id

    urn = getFromReq req, 'urn'
    if urn?
      # If getting a specific URN, return all available data
      queryParams.urn = urn
      queryParams.show = 'extended'
    else
      # If listing all deployments, return only a urn list unless a specific
      # format is specified
      if not (req.query.show? and req.query.show isnt '')
        queryParams.show = 'urn'
      else
        queryParams.show = req.query.show

    # @logger.debug "#{meth} REQ: #{JSON.stringify req.headers, null, 2}"
    @logger.info  "#{meth} URN: #{urn}"
    @logger.debug "#{meth} req.query: #{JSON.stringify req.query}"
    @logger.debug "#{meth} queryParams: #{JSON.stringify queryParams}"


    @adm.solutionQuery(context, queryParams)
    .then (info)->
      res.json
        success : true
        message: 'SUCCESSFULLY PROCESSED SOLUTION INFO.'
        data: info
    .fail (error) =>
      @logger.error 'ERROR:', error.stack || error
      res.send {
        success : false
        , message: "ERROR PROCESSING SOLUTIONS QUERY: #{error.message}"
      }


  registerSolution: (req, res) =>
    meth = 'AdmissionRestAPI.registerSolution'
    @apiRequestCount++

    context = @setupContext req
    @logger.info "#{meth} - User: #{context.user.id || context.user.name || ''}"

    filesToCleanup = []

    # @logger.debug "#{meth} REQ: #{JSON.stringify req.headers, null, 2}"
    @logger.debug "#{meth} - DATA: #{JSON.stringify req.body}"

    if @config.restTimeout isnt -1
      res.setTimeout @config.restTimeout, =>
        res.send {
          success: false,
          message: "ADMISSION SOLUTION CREATION TIMEOUT"
        }
        res.send = (m) =>
          @logger.warn "HTTP Response Already Sent #{JSON.stringify m}"

    options = null
    if req.files?[0]?.fieldname is 'inline'
      options= {filename: req.files[0].path}
      filesToCleanup.push req.files[0].path
    else
      @logger.error "#{meth} - ERROR: Missing mandatory parameter (inline file)"
      response =
        success : false
        message: 'ERROR PROCESSING SOLUTION: Missing mandatory parameter \
                  (inline file)'
      res.send response
      # Cleanup uploaded files from temporary directory
      cleanupFiles.call this, filesToCleanup
      return

    # Proceed to registering the solution in Admission
    @adm.registerSolution context, options
    .then (solutionRegistrationInfo) =>
      @logger.debug "#{meth} - Processed Solution:
                     #{JSON.stringify solutionRegistrationInfo}"
      response =
        success: true
        message: 'SUCCESSFULLY PROCESSED SOLUTION: ' +
                 "#{solutionRegistrationInfo.solutionURN}"
        data: solutionRegistrationInfo
      res.send response
    .fail (error) =>
      @logger.error "#{meth} - ERROR: #{error.message}", error.stack
      response =
        success : false
        message: "ERROR PROCESSING SOLUTION: #{error.message}"
        error: error.stack
      res.send response
    .finally () =>
      # Cleanup uploaded files from temporary directory
      cleanupFiles.call this, filesToCleanup



  deleteSolution: (req, res) =>
    meth = 'AdmissionRestAPI.deleteSolution'
    @apiRequestCount++

    context = @setupContext req
    @logger.info "#{meth} - User: #{context.user.id || context.user.name || ''}"

    owner = context?.user?.id
    urn   = getFromReq req, 'urn'
    force = getFromReq req, 'force'

    force = force? and (force is 'yes')

    @logger.info  "#{meth} URN: #{urn} (force: #{force})"
    # @logger.debug "#{meth} REQ: #{JSON.stringify req.headers, null, 2}"
    @logger.debug "#{meth} - QUERY: #{JSON.stringify req.query}"

    if not urn
      errMsg = 'Missing mandatory parameter (solution URN)'
      @logger.error "#{meth} - ERROR: #{errMsg}"
      response =
        success : false
        message: errMsg
      res.send response
      q()
    else
      if @config.restTimeout isnt -1
        res.setTimeout @config.restTimeout, =>
          res.send {
            success: false,
            message: 'ADMISSION DELETE SOLUTION TIMEOUT'
          }
          res.send = (m) =>
            @logger.warn "HTTP Response Already Sent #{JSON.stringify m}"
      @adm.deleteSolution context, { solutionURN:urn, force: force }
      .then (solutionDeletionInfo) =>
        @logger.debug "#{meth} - Deleted solution: #{solutionDeletionInfo}"
        response =
          success : true
          message: "SUCCESSFULLY DELETED SOLUTION: #{urn}"
          data: solutionDeletionInfo
        res.send response
      .fail (error) =>
        @logger.error 'ERROR:', error.stack || error
        response =
          success : false
          message: "ERROR DELETING SOLUTION: #{error.message}"
        res.send response


  deploy: (req, res) =>
    meth = 'AdmissionRestAPI.deploy'
    @apiRequestCount++

    context = @setupContext req
    @logger.info "#{meth} - User: #{context.user.id || context.user.name || ''}"

    filesToCleanup = []

    # @logger.debug "#{meth} REQ: #{JSON.stringify req.headers, null, 2}"
    @logger.debug "#{meth} DATA: #{JSON.stringify req.body}"

    if @config.restTimeout isnt -1
      res.setTimeout @config.restTimeout, =>
        res.send {success: false, message: 'ADMISSION DEPLOY TIMEOUT'}
        res.send = (m) =>
          @logger.warn "HTTP Response Already Sent #{JSON.stringify m}"

    options = null
    if req.files?[0]?.fieldname is 'inline'
      options= {filename: req.files[0].path}
      filesToCleanup.push req.files[0].path
    else if req.body.url
      options = {url: req.body.url}
    else if req.body.filename
      options = {filename: req.body.filename}
    #console.log "options", options

    if options?
      @adm.deploy context, options
      .then (deploymentInfo) =>
        @logger.debug "#{meth} - Processed deploy:
                       #{JSON.stringify deploymentInfo}"
        response =
          success: true
          message: 'SUCCESSFULLY PROCESSED DEPLOYMENT: ' +
                   "#{deploymentInfo.deploymentURN}"
          data: deploymentInfo
        res.send response
      .fail (error) =>
        @logger.error "ERROR: #{error.message}", error.stack
        response =
          success : false
          message: "ERROR PROCESSING DEPLOYMENT: #{error.message}"
          error: error.stack
        res.send response
      .finally () =>
        # Cleanup uploaded files from temporary directory
        cleanupFiles.call this, filesToCleanup
    else
      @logger.error 'ERROR: Missing mandatory parameter (URL or Filename)'
      response =
        success : false
        message: 'ERROR PROCESSING DEPLOYMENT: Missing mandatory parameter \
                  (URL or Filename)'
      res.send response
      # Cleanup uploaded files from temporary directory
      cleanupFiles.call this, filesToCleanup


  undeploy: (req, res) =>
    meth = 'AdmissionRestAPI.undeploy'
    @apiRequestCount++

    context = @setupContext req
    @logger.info "#{meth} - User: #{context.user.id || context.user.name || ''}"

    owner = context?.user?.id
    urn = getFromReq req, 'urn'
    force = getFromReq req, 'force'

    force = force? and (force is 'yes')
    @logger.info  "#{meth} URN: #{urn} (force: #{force})"
    # @logger.debug "#{meth} REQ: #{JSON.stringify req.headers, null, 2}"
    @logger.debug "#{meth} QUERY: #{JSON.stringify req.query}"

    if not urn
      errMsg = 'Missing mandatory parameter (deployment URN)'
      @logger.error "ERROR: #{errMsg}"
      response =
        success : false
        message: errMsg
      res.send response
      q()
    else
      # Check permissions for writing (deleting)
      @authorization.isAllowedForURN urn, context, true
      .then (isAllowed) =>
        if not isAllowed
          return q.reject new Error "Unauthorized request to #{urn}"
        else
          @logger.info "#{meth} - User is allowed to access element #{urn}"
          return true
      .then =>
        if @config.restTimeout isnt -1
          res.setTimeout @config.restTimeout, =>
            res.send {success: false, message: 'ADMISSION UNDEPLOY TIMEOUT'}
            res.send = (m) =>
              @logger.warn "HTTP Response Already Sent #{JSON.stringify m}"
        @adm.undeploy context, { deploymentURN:urn, force: force }
      .then (undeploymentInfo) =>
        @logger.debug "#{meth} - Processed undeploy: #{undeploymentInfo}"
        response =
          success : true
          message: "SUCCESSFULLY PROCESSED UNDEPLOYMENT: #{urn}"
          data: undeploymentInfo
        res.send response
      .fail (error) =>
        @logger.error 'ERROR:', error.stack || error
        response =
          success : false
          message: "ERROR PROCESSING UNDEPLOYMENT: #{error.message}"
        res.send response


  restartSolutionInstances: (req, res) =>
    meth = 'AdmissionRestAPI.restartSolutionInstances'
    @apiRequestCount++

    context = @setupContext req
    @logger.info "#{meth} - User: #{context.user.id || context.user.name || ''}"

    owner = context?.user?.id

    urn      = getFromReq req, 'urn'
    role     = getFromReq req, 'role'
    instance = getFromReq req, 'instance'

    force = force? and (force is 'yes')

    @logger.info  "#{meth} - URN: #{urn}  ROLE: #{role}  INSTANCE: #{instance}"
    # @logger.debug "#{meth} PARAMS: #{JSON.stringify req.params}"
    # @logger.debug "#{meth} QUERY: #{JSON.stringify req.query}"

    if not urn?
      errMsg = 'Missing mandatory parameter (solution URN)'
      @logger.error "ERROR: #{errMsg}"
      response =
        success : false
        message: errMsg
      res.send response
      return true

    if not role?
      errMsg = 'Missing mandatory parameter (solution role)'
      @logger.error "ERROR: #{errMsg}"
      response =
        success : false
        message: errMsg
      res.send response
      return true

    # Validate user has permissions to access the requested solution
    @logger.info "#{meth} - Checking user permissions to access solution #{urn}"

    @authorization.isAllowedForURN urn, context
    .then (isAllowed) =>
      if not isAllowed
        errMsg = 'User is not allowed to access solution.'
        @logger.debug "#{meth} - #{errMsg}"
        throw new Error errMsg

      @logger.info "#{meth} - User is allowed to access element #{urn}"
      return true
    .then () =>

      # Convert solution+role+instance into deployment+role+instance
      @adm.solutionElementToDeploymentElement urn, role, instance, context
    .then (deploymentInstanceData) =>

      # Perform restart in Admission
      @adm.restartInstances context, deploymentInstanceData
    .then (restartInfo) =>
      @logger.debug "#{meth} - Processed restart: #{restartInfo}"
      element = instance ? "#{role}/#{instance}" : role
      response =
        success : true
        message: "SUCCESSFULLY PROCESSED RESTART: #{urn}/#{element}"
        data: null
      res.send response
      return true
    .catch (err) =>
      errMsg = "Error while processing restart: #{err.message}"
      @logger.error "#{meth} - ERROR: #{errMsg}"
      response =
        success : false
        message: errMsg
      res.send response
      return true



  restartInstances: (req, res) =>
    meth = 'AdmissionRestAPI.restartInstances'
    @apiRequestCount++

    context = @setupContext req
    @logger.info "#{meth} - User: #{context.user.id || context.user.name || ''}"

    owner = context?.user?.id

    urn      = getFromReq req, 'urn'
    role     = getFromReq req, 'role'
    instance = getFromReq req, 'instance'

    force = force? and (force is 'yes')
    @logger.info  "#{meth} URN: #{urn}  ROLE: #{role}  INSTANCE: #{instance}"
    # @logger.debug "#{meth} REQ: #{JSON.stringify req.headers, null, 2}"
    @logger.debug "#{meth} PARAMS: #{JSON.stringify req.params}"
    @logger.debug "#{meth} QUERY: #{JSON.stringify req.query}"

    if not urn
      errMsg = 'Missing mandatory parameter (deployment URN)'
      @logger.error "ERROR: #{errMsg}"
      response =
        success : false
        message: errMsg
      res.send response
      q()
    else if not role
      errMsg = 'Missing mandatory parameter (deployment role)'
      @logger.error "ERROR: #{errMsg}"
      response =
        success : false
        message: errMsg
      res.send response
      q()
    else
      @authorization.isAllowedForURN urn, context
      .then (isAllowed) =>
        if not isAllowed
          return q.reject new Error "Unauthorized request to #{urn}"
        else
          @logger.info "#{meth} - User is allowed to access element #{urn}"
          return true
      .then () =>
        # If an instance was provided, check if user has permissions
        if instance?
          @authorization.isAllowed 'instance', instance, context
          .then (allowed) =>
            if not allowed
              throw new Error 'User is not allowed to access instance.'
            else
              return true
      .then () =>
        # Perform restart in Admission
        @adm.restartInstances context, {
          deploymentURN: urn
          role: role
          instance: instance
        }
      .then (restartInfo) =>
        @logger.debug "#{meth} - Processed restart: #{restartInfo}"
        element = instance ? "#{role}/#{instance}" : role
        response =
          success : true
          message: "SUCCESSFULLY PROCESSED RESTART: #{urn}/#{element}"
          data: null
        res.send response
      .fail (error) =>
        @logger.error 'ERROR:', error.stack || error
        response =
          success : false
          message: "ERROR PROCESSING RESTART: #{error.message}"
        res.send response


  unregister: (req,res) =>
    meth = 'AdmissionRestAPI.unregister'
    @apiRequestCount++

    context = @setupContext req
    @logger.info "#{meth} - User: #{context.user.id || context.user.name || ''}"

    owner = context?.user?.id
    urn = getFromReq req, 'urn'

    @logger.info "#{meth} URN: #{urn} - OWNER: #{owner}"
    # @logger.debug "REQ: #{JSON.stringify req.headers, null, 2}"
    @logger.debug "QUERY: #{JSON.stringify req.query}"
    @logger.debug "PARAMS: #{JSON.stringify req.params}"

    manifestName = null
    elementType = null

    # Check user permissions for writing (deleting)
    @authorization.isAllowedForURN urn, context, true
    .then (isAllowed) =>
      if not isAllowed
        q.reject new Error 'Unauthorized'
      else
        @logger.info "#{meth} - User is allowed to access element #{urn}"
    .then () =>
      @logger.info "#{meth} Getting manifest for URN #{urn}"
      console.log "GETTING MANIFEST"
      @manifestRepository.getManifest urn
    .then (manifest) =>
      @logger.info "#{meth} Manifest (URN #{urn}) : #{JSON.stringify manifest}"

      ALLOWED_TYPES = [
        ManifestHelper.COMPONENT
        ManifestHelper.SERVICE
        ManifestHelper.RESOURCE
      ]
      elementName = manifest.name
      elementType = @manifestHelper.getManifestType manifest
      @logger.info "#{meth} ElementType: #{elementType} - URN: #{urn}"
      if elementType not in ALLOWED_TYPES
        errMsg = "Error unregistering #{urn}: Unregister operation not supported
                  for element type."
        @logger.error "#{meth} - #{errMsg}"
        throw new Error errMsg
      else
        @adm.canBeDeleted urn
    .then (canBeDeleted) =>
      @logger.info "#{meth} Checking if element can be deleted - URN: #{urn}"
      if not canBeDeleted
        q.reject new Error "Component #{urn} is in use."
      else
        @manifestRepository.deleteManifest urn
    .then =>
      response =
        success : true
        message: 'Unregistered succesfully'
      res.send response
    .fail (error) =>
      @logger.error 'Error unregistering:', error
      , error.stack
      message = error.message or error
      if message.indexOf('Error code 23') > 0
        message = "Error reading element #{urn} manifest. Check the URN."
      response =
        success : false
        message: message
      res.send response


  readManifest: (req,res) =>
    meth = 'AdmissionRestAPI.readManifest'
    @apiRequestCount++

    context = @setupContext req
    @logger.info "#{meth} - User: #{context.user.id || context.user.name || ''}"
    urn = getFromReq req, 'urn'

    # @logger.debug "REQ: #{JSON.stringify req.headers, null, 2}"
    @logger.debug "QUERY: #{JSON.stringify req.query}"
    @logger.debug "PARAMS: #{JSON.stringify req.params}"
    @logger.debug "#{meth} - URN: #{urn}"

    data = null
    q()
    .then =>
      # Check if user has permissions to access manifest
      @authorization.isAllowedForURN urn, context
    .then (isAllowed) =>
      if not isAllowed
        errMsg = "User is not allowed to access element."
        @logger.warn "#{meth} ERROR: #{errMsg}"
        return q.reject new Error errMsg
      else
        @manifestRepository.getManifest urn
    .then (manifest) ->
      response =
        success : true
        message: 'Manifest obtained'
        data: manifest
      res.send response
    .fail (error) =>
      @logger.error "Error accessing #{urn} - #{error.message}"
      response =
        success : false
        message: error.message
      res.send response


  listLinks: (req, res) =>
    meth = 'AdmissionRestAPI.listLinks'
    @apiRequestCount++

    context = @setupContext req
    @logger.info "#{meth} - User: #{context.user.id || context.user.name || ''}"

    @manifestRepository.listElementType 'kukulinks', context.owner, null
    .then (elements) =>
      response =
        success : true
        message: 'Successfully got list of links.'
        data: elements
      res.send response
    .fail (error) =>
      @logger.error "Error getting list of links: #{error.message}"
      response =
        success : false
        message: error.message
      res.send response


  linkServices: (req, res) =>
    meth = 'AdmissionRestAPI.linkServices'
    @apiRequestCount++

    context = @setupContext req
    @logger.info "#{meth} - User: #{context.user.id || context.user.name || ''}"

    filesToCleanup = []
    endpoints = null
    q()
    .then () =>
      if req.files?[0]?.fieldname is 'linkManifest'
        file = req.files[0]
        filesToCleanup.push file.path
        @logger.info "#{meth} file = #{file.originalname}"
        @logger.debug "#{meth} request = #{JSON.stringify req.headers, null, 2}"
        loadFile file.path
      else
        q.reject new Error 'Link a service requires a file'
    .then (linkManifest) =>
      (if not linkManifest.endpoints?
        @logger.error  "#{meth} Missing endpoints section in manifest."
        q.reject new Error 'Missing endpoints section in manifest.'
      else if linkManifest.endpoints.length isnt 2
        @logger.error \
           "#{meth} Wrong number of endpoints: #{linkManifest.endpoints.length}"
        q.reject new Error \
           "Wrong number of endpoints: #{linkManifest.endpoints.length}"
      else
        endpoints = linkManifest.endpoints
        @adm.linkServices context, linkManifest
      )
    .then (info)->
      response =
        success : true
        message: "SUCCESSFULLY LINKED SERVICES: #{JSON.stringify endpoints}"
        data: info
      res.json response
    .fail (error) =>
      @logger.error 'ERROR:', error.stack || error
      response =
        success : false
        message: "ERROR LINKING SERVICES: #{error.message}"
      res.send response
    .finally () =>
      # Cleanup uploaded files from temporary directory
      cleanupFiles.call this, filesToCleanup


  unlinkServices: (req, res) =>
    meth = 'AdmissionRestAPI.unlinkServices'
    @apiRequestCount++

    context = @setupContext req
    @logger.info "#{meth} - User: #{context.user.id || context.user.name || ''}"

    urn = getFromReq req, 'urn'

    filesToCleanup = []
    endpoints = null
    # If a link name is provided, it takes precedence over file upload
    (if urn?
      @logger.debug "#{meth} - Link URN: #{urn}"
      @adm.unlinkServicesFromURN context, urn
    else
      (if req.files?[0]?.fieldname is 'linkManifest'
        file = req.files[0]
        filesToCleanup.push file.path
        @logger.info "#{meth} file = #{file.originalname}"
        @logger.debug "#{meth} request = #{JSON.stringify req.headers, null, 2}"
        loadFile file.path
      else if req?.query?.linkManifest?
        q JSON.parse req.query.linkManifest
      else
        q.reject new Error 'Missing parameter linkManifest'
      )
      .then (linkManifest) =>
        if not linkManifest.endpoints?
          @logger.error  "#{meth} Missing endpoints section in manifest."
          q.reject new Error 'Missing endpoints section in manifest.'
        else if linkManifest.endpoints.length isnt 2
          @logger.error \
             "#{meth} Wrong number of endpoints: #{linkManifest.endpoints.length}"
          q.reject new Error \
             "Wrong number of endpoints: #{linkManifest.endpoints.length}"
        else
          endpoints = linkManifest.endpoints
          @adm.unlinkServices context, linkManifest
    )
    .then (info)->
      response =
        success : true
        message: "SUCCESSFULLY UNLINKED SERVICES: #{urn || JSON.stringify endpoints}"
        data: info
      res.json response
    .fail (error) =>
      @logger.error  "#{meth} ERROR: #{error.stack || error}"
      response =
        success : false
        message: "ERROR UNLINKING SERVICES: #{error.message}"
      res.send response
    .finally () =>
      # Cleanup uploaded files from temporary directory
      cleanupFiles.call this, filesToCleanup


  registerBundle: (req,res, performDeployments = false) =>
    meth = 'AdmissionRestAPI.registerBundle()'
    @apiRequestCount++

    context = @setupContext req

    filesToCleanup = []
    @logger.info "#{meth} - User: #{context.user.id || context.user.name || ''}"
    if @config.restTimeout isnt -1
      res.setTimeout @config.restTimeout, =>
        res.send {
          success: false
          message: 'ADMISSION REGISTER BUNDLE TIMEOUT'
        }
        res.send = (m) =>
          @logger.warn "#{meth} HTTP Response Already Sent #{JSON.stringify m}"
    # @logger.debug "#{meth} - REQ: #{JSON.stringify req.headers, null, 2}"
    @logger.debug "#{meth} - REQ FILES: #{JSON.stringify req.files, null, 2}"
    bundlesFile = null
    fullResults = null
    deploymentNameToURN = {}
    (if req.files?[0]?.fieldname is 'bundlesJson'
      file = req.files[0]
      @logger.debug "File #{file.originalname} uploaded."
      bundlesFile = file.originalname
      filesToCleanup.push file.path
      @bundleRegistry.registerBundleJson context, file.path
    else if req.files?[0]?.fieldname is 'bundlesZip'
      file = req.files[0]
      @logger.debug "File #{file.originalname} uploaded."
      bundlesFile = file.originalname
      filesToCleanup.push file.path
      @bundleRegistry.registerBundleZip context, file.path
    else
      q.reject new Error 'Bundle registration requires a JSON or zip file.'
    )
    .then (result) =>
      fullResults = result

      # Handle deferred Solution manifests
      solutionChain = q()
      if fullResults.pendingActions?.solutions?.length > 0
        @logger.debug 'Processing pending solutions from bundle...'
        fullResults.solutions =
          errors: []
          successful: []
        for solManifest in fullResults.pendingActions.solutions
          do (solManifest) =>
            solutionChain = solutionChain.then () =>
              # Pass the Solution manifest to Admission
              @adm.registerSolution context, {}, solManifest
              .then (solutionInfo) =>
                @logger.info \
                  "Solution creation finished: #{solutionInfo.solutionURN}"
                fullResults.solutions.successful.push solutionInfo
              .fail (err) =>
                @logger.error "Solution creation error: #{err.stack ? err}"
                fullResults.solutions.errors.push {
                  message: "ERROR IN SOLUTION: #{err.message ? err}"
                  error: err.stack
                }
                q()
      solutionChain
    .then () =>
      # Handle deferred Deployment manifests
      deploymentChain = q()
      if fullResults.pendingActions?.deployments?.length > 0
        @logger.debug 'Processing pending deployments from bundle...'
        fullResults.deployments =
          errors: []
          successful: []
        for depManifest in fullResults.pendingActions.deployments
          do (depManifest) =>
            action = null
            # If deployment had a name (for auto-linking), keep it and remove it
            # from manifest. Use it as default nickname if none provided.
            # Dont do this for kmv3!
            if ! @manifestHelper.isKmv3Deployment depManifest
              name = depManifest.name
              depManifest.nickname ?= name
              delete depManifest.name

            if performDeployments
              deploymentChain = deploymentChain.then () =>
                @adm.deploy context, {simulation: false}, depManifest
                .then (deploymentInfo) =>
                  @logger.info \
                    "Deployment finished: #{deploymentInfo.deploymentURN}"
                  name ?= deploymentInfo.deploymentURN
                  deploymentNameToURN[name] = deploymentInfo.deploymentURN
                  fullResults.deployments.successful.push deploymentInfo
                .fail (err) =>
                  @logger.error "Deployment error: #{err.stack ? err}"
                  fullResults.deployments.errors.push {
                    message: "ERROR IN DEPLOYMENT: #{err.message ? err}"
                    error: err.stack
                  }
                  q()
            else
              action = @adm.setupDeploy
              deploymentChain = deploymentChain.then () =>
                @adm.setupDeploy context, {simulation: false}, depManifest
                .then (deploymentInfo) =>
                  @logger.info "Deployment finished: #{deploymentInfo.name}"
                  name ?= deploymentInfo.deploymentURN
                  deploymentNameToURN[name] = deploymentInfo.name
                  fullResults.deployments.successful.push deploymentInfo
                .fail (err) =>
                  @logger.error "Deployment error: #{err?.stack ? err}"
                  fullResults.deployments.errors.push {
                    message: "ERROR IN DEPLOYMENT: #{err.message ? err}"
                    error: err.stack
                  }
                  q()
      deploymentChain
    .then () =>
      # Process pending LinkService manifests
      linkPromises = []
      linkErrors = 0
      if fullResults.pendingActions?.links?.length > 0
        @logger.debug 'Processing pending links from bundle...'
        fullResults.links =
          errors: []
          successful: []
        for linkManifest in fullResults.pendingActions.links
          do (linkManifest) =>
            # Replace local deployment name with actual deployment URN
            if not linkManifest.endpoints?[0]?.deployment?
              errorMsg = 'Link manifest is missing endpoint[0] deployment.'
              @logger.warn errorMsg
              result =
                error: errorMsg
                manifest: linkManifest
              fullResults.links.errors.push result
              q()
            else if not linkManifest.endpoints?[1]?.deployment?
              errorMsg = 'Link manifest is missing endpoint[1] deployment.'
              @logger.warn errorMsg
              result =
                error: errorMsg
                manifest: linkManifest
              fullResults.links.errors.push result
              q()
            else
              # If endpoint deployments are local names (deployments in the same
              # bundle), then replace local names with deployment URNs.
              depName1 = linkManifest.endpoints[0].deployment
              depName2 = linkManifest.endpoints[1].deployment

              if depName1 of deploymentNameToURN
                linkManifest.endpoints[0].deployment =
                  deploymentNameToURN[depName1]
              if depName2 of deploymentNameToURN
                linkManifest.endpoints[1].deployment =
                  deploymentNameToURN[depName2]

              @logger.debug "Linking: #{JSON.stringify linkManifest}"
              linkPromises.push (\
                @adm.linkServices context, linkManifest
                .then (linkResult) =>
                  @logger.info \
                    "Link succeeded: #{JSON.stringify linkManifest.endpoints}"
                  fullResults.links.successful.push linkManifest
                .fail (err) =>
                  linkErrors++

                  @logger.error "Service link error: #{err.stack ? err}"
                  fullResults.links.errors.push {
                    message: "ERROR IN SERVICE LINK: #{err.message}"
                    error: err.stack
                  }
                  q()
              )
      q.all linkPromises
    .then () =>
      @logger.info "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
      @logger.info "#{meth} - FINISHED"
      @logger.info "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
    .then () =>
      @logger.info "#{meth} - Sending response"
      response =
        success: null
        message: null
        data: {}

      if fullResults.registered.length > 0 or
      fullResults.solutions?.successful?.length > 0 or
      fullResults.deployments?.successful?.length > 0 or
      fullResults.links?.successful?.length > 0
        response.success = true
      else
        response.success = false

      if fullResults.errors.length > 0 or
      fullResults.deployments?.errors?.length > 0 or
      fullResults.solutions?.errors?.length > 0 or
      fullResults.links?.errors?.length > 0
        response.message = 'Bundle registration found some errors.'
      else
        response.message = 'Bundle registration finished with no errors.'

      response.data =
        successful: fullResults.registered
        errors: fullResults.errors

      if fullResults.solutions?
        response.data.solutions = fullResults.solutions

      if fullResults.deployments?
        response.data.deployments = fullResults.deployments

      if fullResults.links?
        response.data.links = fullResults.links

      res.send response
    .fail (error) =>
      @logger.error "Error registering #{bundlesFile}", error, error.stack
      response =
        success : false
        message: error.message
      res.send response
    .fail (error) =>
      @logger.error "Error registering #{bundlesFile}:", error.stack
    .finally () =>
      # Cleanup uploaded files from temporary directory
      cleanupFiles.call this, filesToCleanup


  registerResource: (req, res) =>
    meth = 'AdmissionRestAPI.registerResource'
    @apiRequestCount++

    context = @setupContext req
    @logger.info "#{meth} - User: #{context.user.id || context.user.name || ''}"

    filesToCleanup = []
    endpoints = null
    q()
    .then () =>
      if req.files?[0]?.fieldname is 'resourceManifest'
        file = req.files[0]
        filesToCleanup.push file.path
        @logger.info "#{meth} file = #{file.originalname}"
        @logger.debug "#{meth} request = #{JSON.stringify req.headers, null, 2}"
        loadFile file.path
      else
        q.reject new Error 'Resource registration requires a file.'
    .then (resourceManifest) =>
      (if @manifestHelper.isResource resourceManifest
        @adm.registerResource context, resourceManifest
      else
        errMsg= "File doesn't contain a valid resource definition."
        @logger.error  "#{meth} #{errMsg}."
        q.reject new Error errMsg
      )
    .then (info)->
      response =
        success : true
        message: "SUCCESSFULLY REGISTERED RESOURCE."
        data: info
      res.json response
    .fail (error) =>
      @logger.error 'ERROR:', error.stack || error
      response =
        success : false
        message: "ERROR REGISTERING RESOURCE: #{error.message}"
      res.send response
    .finally () =>
      # Cleanup uploaded files from temporary directory
      cleanupFiles.call this, filesToCleanup


  deleteResource: (req, res) =>
    meth = 'AdmissionRestAPI.deleteResource()'

    context = @setupContext req
    @logger.info "#{meth} - User: #{context.user.id || context.user.name || ''}"

    # Compose the filter from the parameters
    urn = getFromReq req, 'urn'
    @logger.info "#{meth} - URN: #{urn}"


    if not urn?
      errMsg = 'Missing mandatory parameter (resource URN)'
      @logger.error "ERROR: #{errMsg}"
      response =
        success : false
        message: errMsg
      res.send response
      return true

    elementManifest = null

    # Calculate a unique ID for this operation
    opID = @adm.getOperationID()

    # Check user permissions on element for writing (deleting)
    @authorization.isAllowedForURN urn, context, true
    .then (isAllowed) =>
      if not isAllowed
        errMsg = "Resource does not exist or user is not allowed to access it."
        @logger.warn "#{meth} ERROR: #{errMsg}"
        return q.reject new Error errMsg
      else
        @logger.info "#{meth} - User is allowed to access resource #{urn}"
        # Get element manifest
        @manifestRepository.getManifest urn
    .then (manifest) =>
      @logger.info "#{meth} Got manifest for #{urn}"
      elementManifest = manifest

      elementType = @manifestHelper.getManifestType manifest
        # Check it's a resource
      if elementType isnt ManifestHelper.RESOURCE
        errMsg = "Wrong element type (#{elementType})."
        @logger.error "#{meth} - #{errMsg}"
        throw new Error errMsg
      else
        @logger.info "#{meth} - Element type checked OK - #{urn}"

        # Keep resource internal name for later
        resName = elementManifest.name
        resKind = elementManifest.ref.kind
        @logger.info "#{meth} Resource ID: #{resName} - TYPE: #{resKind}"

        # Check resource is not in use
        @adm.isResourceInUse resName
    .then (inUse) =>
      if inUse
        errMsg = "Resource is in use."
        @logger.error "#{meth} ERROR: #{errMsg}"
        q.reject new Error errMsg
      else
        @logger.info "#{meth} - Resource is not in use - #{urn}"
        # Get lock for the element being deleted
        @adm.acquireElementLockFromURN elementManifest.name, opID
    .then () =>
        # Delete the manifest
        resName = elementManifest.name
        resKind = elementManifest.ref.kind
        @manifestRepository.deleteManifestByNameAndKind resName, resKind
    .then () =>
      @logger.info "#{meth} - Resource deleted - #{urn}"
      response =
        success : true
        message: 'Resource deleted succesfully'
      res.send response
      return true
    .catch (error) =>
      errMsg = "Unable to delete resource #{urn}: #{error.message}"
      @logger.error "#{meth} ERROR: #{errMsg} - #{error.stack}"
      response =
        success : false
        message: errMsg
      res.send response
      return true
    .finally () =>
      @adm.releaseElementLockFromURN elementManifest.name, opID


  listResources: (req, res) =>
    meth = 'AdmissionRestAPI.listResources()'
    @apiRequestCount++

    context = @setupContext req
    @logger.info "#{meth} - User: #{context.user.id || context.user.name || ''}"

    owner = getFromReq req, 'owner'

    filter = {}
    if owner?
      filter.owner = owner
    else if not @authorization.isAdmin context
      filter.owner = context.user.id

    @adm.listResources filter
    .then (resources) ->
      res.json
        success : true
        message: 'SUCCESSFULLY PROCESSED LIST RESOURCES.'
        data: resources
    .fail (error) =>
      @logger.error 'ERROR:', error.stack || error
      res.json
        success : false
        message: "ERROR LISTING RESOURCES: #{error.message}"


  listResourcesInUse: (req, res) =>
    meth = 'AdmissionRestAPI.listResourcesInUse()'
    @apiRequestCount++

    context = @setupContext req
    @logger.info "#{meth} - User: #{context.user.id || context.user.name || ''}"

    # Compose the filter from the parameters
    owner = getFromReq req, 'owner'
    urn   = getFromReq req, 'urn'
    filter = {}

    if owner?
      filter.owner = owner
    else if not @authorization.isAdmin context
      filter.owner = context.user.id
    if urn? then filter.urn = urn
    @logger.info "#{meth} filter = #{JSON.stringify filter}"
    # Check if current user has permission to process the filter
    @authorization.canListResources context, filter
    .then () =>
      @adm.listResourcesInUse filter, context
    .then (resources) ->
      res.json
        success : true
        message: 'SUCCESSFULLY PROCESSED LIST RESOURCES.'
        data: resources
    .fail (error) =>
      @logger.error 'ERROR:', error.stack || error
      res.json
        success : false
        message: "ERROR LISTING RESOURCES: #{error.message}"


  describeResource: (req, res) =>
    meth = 'AdmissionRestAPI.describeResource()'
    @apiRequestCount++

    context = @setupContext req
    @logger.info "#{meth} - User: #{context.user.id || context.user.name || ''}"

    resourceURN = getFromReq req, 'urn'

    # Check if current user has permission to view the resource
    @authorization.isAllowedForURN resourceURN, context
    .then (isAllowed) =>
      if not isAllowed
        errMsg = "Resource does not exist or user is not allowed to access it."
        @logger.warn "#{meth} ERROR: #{errMsg}"
        return q.reject new Error errMsg
      else
        @logger.info "#{meth} - User is allowed to access resource #{resourceURN}"

        # Get element description
        @adm.describeResource resourceURN
    .then (resourceDescription) ->
      res.json
        success : true
        message: 'SUCCESSFULLY PROCESSED DESCRIBE RESOURCE.'
        data: resourceDescription
    .fail (error) =>
      @logger.error 'ERROR:', error.stack || error
      res.json
        success : false
        message: "ERROR DESCRIBING RESOURCE: #{error.message}"



  listElements: (req, res) =>
    @apiRequestCount++
    meth = 'AdmissionRestAPI.listElements()'
    @apiRequestCount++

    context = @setupContext req
    @logger.info "#{meth} - User: #{context.user.id || context.user.name || ''}"

    owner = null
    if not @authorization.isAdmin context
      owner = context.user.id

    @manifestRepository.list owner
    .then (info)->
      res.json
        success : true
        message: 'SUCCESSFULLY PROCESSED LIST ELEMENTS.'
        data: info
    .fail (error) =>
      @logger.error 'Error listing bundles: '
      , error, error.stack
      response =
        success : false
        message: error.message
      res.send response


  getClusterInfo: (req,res) =>
    meth = 'AdmissionRestAPI.getClusterInfo'
    context = @setupContext req
    @logger.info "#{meth} - User: #{context.user.id || context.user.name || ''}"

    results = []

    owner = context?.user?.id
    if not @authorization.isAdmin context
      errMsg = "Access denied. Admin role required (user #{owner})"
      @logger.error "#{meth} - Error: #{errMsg}"
      response =
        success : false
        message: errMsg
      res.send response
      return true


    # q.reject new Error 'NOT IMPLEMENTED'
    @k8sApiStub.topNodes()
    .then (clusterInfo) =>
      response =
        success : true
        message: "Cluster information retrieved successfully.",
        data: clusterInfo
      res.send response
      return true
    .catch (err) =>
      errMsg = "Couldn't get cluster information: #{err.message})"
      @logger.error "#{meth} - Error: #{errMsg}"
      response =
        success : false
        message: errMsg
      res.send response
      return true


  getClusterConfig: (req,res) =>
    meth = 'AdmissionRestAPI.getClusterConfig'
    context = @setupContext req
    @logger.info "#{meth} - User: #{context.user.id || context.user.name || ''}"

    @readClusterConfig()
    .then (clusterConfiguration) =>
      response =
        success : true
        message: "Cluster configuration retrieved successfully.",
        data: clusterConfiguration
      res.send response
      return true

    .catch (err) =>
      errMsg = "Couldn't get cluster configuration: #{err.message})"
      @logger.error "#{meth} - Error: #{errMsg}"
      response =
        success : false
        message: errMsg
      res.send response
      return true


  readClusterConfig: () =>
    meth = 'AdmissionRestAPI.readClusterConfig'
    @logger.debug "#{meth}"

    @k8sApiStub.getConfigMap KUMORI_NAMESPACE, CLUSTER_CONFIG_CONFIGMAP
    .then (configMap) =>
      @logger.debug "#{meth} - Found cluster configuration."
      # Extract policy from 'cluster-config.yaml' property
      if configMap?.data?['cluster-config.yaml']?
        clusterConfigStr = configMap.data['cluster-config.yaml']
        @logger.debug "#{meth} - Configuration (string): #{clusterConfigStr}"
        parsedClusterConfiguration = utils.yamlParse clusterConfigStr
        @updateClusterConfiguration parsedClusterConfiguration
        return parsedClusterConfiguration
      else
        throw new Error 'Cluster configuration not found.'
    .catch (err) =>
      throw new Error err.message


  updateClusterConfiguration: (newClusterConfiguration) ->
    meth = 'AdmissionRestAPI.updateClusterConfiguration'
    @logger.debug "#{meth}"
    @latestClusterConfiguration = newClusterConfiguration
    @adm.setClusterConfiguration newClusterConfiguration


  getSolutionInstanceLogs: (req,res) =>
    meth = 'AdmissionRestAPI.getSolutionInstanceLogs'
    context = @setupContext req
    @logger.info "#{meth} - User: #{context.user.id || context.user.name || ''}"

    # Handlers to log client disconnections (just for information)
    req.on "close", () =>
      @logger.info "#{meth} - Http connection closed by the client."
    req.on "end", () =>
      @logger.info "#{meth} - Http connection ended."


    # Retrieve request parameters
    urn          = getFromReq req, 'urn'
    role         = getFromReq req, 'role'
    instanceId   = getFromReq req, 'instance'
    container    = getFromReq req, 'container'
    follow       = getFromReq(req, 'follow') == 'yes'
    previous     = getFromReq(req, 'previous') == 'yes'
    tailLines    = getFromReq req, 'tail'
    sinceSeconds = getFromReq req, 'since'

    @logger.info "#{meth} - Parameters: URN: #{urn} - Role: #{role} -
                  instanceId=#{instanceId} - container: #{container} -
                  follow: #{follow} - tailLines: #{tailLines} -
                  sinceSeconds: #{sinceSeconds} - previous: #{previous}"

    # If follow mode is on, don't let the connection timeout
    if follow
      req.setTimeout 30*24*60*60*1000 # 30 days

    # Prepare options to pass to log retriever
    # As a WritableStream we pass the HTTP response object
    logOptions =
      follow: follow
      previous: previous
      tailLines: tailLines
      sinceSeconds: sinceSeconds


    # Validate mandatory parameters are present
    if not urn?
      errMsg = 'Missing mandatory parameter (solution URN)'
      @logger.error "ERROR: #{errMsg}"
      response =
        success : false
        message: errMsg
      res.send response
      return true

    if not role?
      errMsg = 'Missing mandatory parameter (solution role)'
      @logger.error "ERROR: #{errMsg}"
      response =
        success : false
        message: errMsg
      res.send response
      return true

    if not instanceId?
      errMsg = 'Missing mandatory parameter (instance ID)'
      @logger.error "ERROR: #{errMsg}"
      response =
        success : false
        message: errMsg
      res.send response
      return true

    # Validate user has permissions to access the requested solution
    @logger.info "#{meth} - Checking user permissions to access solution #{urn}"

    @authorization.isAllowedForURN urn, context
    .then (isAllowed) =>
      if not isAllowed
        errMsg = 'User is not allowed to access solution.'
        @logger.debug "#{meth} - #{errMsg}"
        throw new Error errMsg

      @logger.info "#{meth} - User is allowed to access element #{urn}"

      # Convert solution+role+instance into deployment+role+instance
      @adm.solutionElementToDeploymentElement urn, role, instanceId, context
    .then (deploymentInstanceData) =>
      realInstanceId = deploymentInstanceData.instanceName
      realContainerName = @manifestHelper.hashContainerName container

      res.writeHead 200, { 'Content-Type' : 'text/plain; charset=utf-8' }
      @k8sApiStub.getPodLogs null, realInstanceId, realContainerName, logOptions, res
      .catch (err) =>
        if err.message?.includes "previous terminated container"
          errMsg = 'Previous container not found.'
        else if err.message?.includes "is not valid for pod"
          errMsg = 'Container not valid for instance.'
        else
          errMsg = "Unable to get instance logs."
          @logger.error "ERROR: Unexpected error: #{err.message}"
        @logger.error "ERROR: #{errMsg}"
        response =
          success : false
          message: errMsg
        res.write JSON.stringify response
        res.end()
        return true
    .then (status) =>
      @logger.info "#{meth} - Finished log retrieval finished. (#{status})"
      return true
    .catch (err) =>
      errMsg = "Error while tailing logs: #{err}"
      @logger.error "ERROR: #{errMsg}"
      response =
        success : false
        message: errMsg
      res.send response
      return true


  execSolutionInstanceCommandGet: (req, res) =>
    meth = 'AdmissionRestAPI.execSolutionInstanceCommandGet'
    context = @setupContext req
    @logger.info "#{meth} - User: #{context.user.id || context.user.name || ''}"

    # Retrieve request parameters
    urn          = getFromReq req, 'urn'
    role         = getFromReq req, 'role'
    instanceId   = getFromReq req, 'instance'
    container    = getFromReq req, 'container'
    command      = getFromReq req, 'command'
    tty          = getFromReq(req, 'tty') == 'yes'
    outRows      = getFromReq req, 'rows'
    outColumns   = getFromReq req, 'columns'

    @logger.info "#{meth} - Parameters: URN: #{urn} - Role: #{role} -
                  instanceId=#{instanceId} - container: #{container} -
                  command: #{JSON.stringify command} - tty: #{tty} -
                  outRows: #{outRows} - outColumns: #{outColumns}"

    rows    = parseInt outRows
    columns = parseInt outColumns

    # Validate mandatory parameters are present
    if not urn?
      errMsg = 'Missing mandatory parameter (solution URN)'
      @logger.error "ERROR: #{errMsg}"
      response =
        success : false
        message: errMsg
      res.send response
      return true

    if not role?
      errMsg = 'Missing mandatory parameter (solution role)'
      @logger.error "ERROR: #{errMsg}"
      response =
        success : false
        message: errMsg
      res.send response
      return true

    if not instanceId?
      errMsg = 'Missing mandatory parameter (instance ID)'
      @logger.error "ERROR: #{errMsg}"
      response =
        success : false
        message: errMsg
      res.send response
      return true

    if not command?
      errMsg = 'Missing mandatory parameter (command)'
      @logger.error "ERROR: #{errMsg}"
      response =
        success : false
        message: errMsg
      res.send response
      return true

    if isNaN(rows) or isNaN(columns)
      errMsg = "Output stream size contains non numeric values.
                outRows: #{outRows} - outColumns: #{outColumns}"
      @logger.error "ERROR: #{errMsg}"
      response =
        success : false
        message: errMsg
      res.send response
      return true


    # Validate user has permissions to access the requested solution
    @logger.info "#{meth} - Checking user permissions to access solution #{urn}"

    realInstanceId = null

    @authorization.isAllowedForURN urn, context
    .then (isAllowed) =>
      if not isAllowed
        errMsg = 'User is not allowed to access solution.'
        @logger.debug "#{meth} - #{errMsg}"
        throw new Error errMsg

      @logger.info "#{meth} - User is allowed to access element #{urn}"

      # Convert solution+role+instance into deployment+role+instance
      @adm.solutionElementToDeploymentElement urn, role, instanceId, context
    .then (deploymentInstanceData) =>
      realInstanceId = deploymentInstanceData.instanceName
      realContainerName = @manifestHelper.hashContainerName container

      @logger.info "#{meth} - Checking instance and container data..."
      @k8sApiStub.execValidationCheck realInstanceId, realContainerName
    .then () =>
      response =
        success : true
        message: "Websocket access is allowed to instance #{realInstanceId}."
        data:
          wsSessionToken: @generateEphemeralExecSessionToken context.user.id
      res.send response
      return true
    .catch (err) =>
      errMsg = "Error running pre-exec checks: #{err.message}"
      @logger.error "#{meth} - ERROR: #{errMsg}"
      response =
        success : false
        message: errMsg
      res.send response
      return true


  execSolutionInstanceCommand: (ws, req) =>
    meth = 'AdmissionRestAPI.execSolutionInstanceCommand'
    context = @setupContext req
    @logger.info "#{meth} - User: #{context.user.id || context.user.name || ''}"

    # Exec requests are unlimited in time
    req.setTimeout 30*24*60*60*1000 # 30 days

    # Initialize stream holder
    myStreams = {}

    # Validate ephemeral Websocket session token
    wsToken = getFromReq req, 'session_token'
    @logger.info "#{meth} - Validating WS session token: #{wsToken}"

    wsSessionErr = null
    if not wsToken?
      wsSessionErr = 'Missing mandatory parameter (session_token).'
    else if wsToken not of @ephemeralWebsocketTokens
      wsSessionErr = 'Websocket session token is expired or invalid.'
    else if @ephemeralWebsocketTokens[wsToken] isnt context.user.id
      wsSessionErr = "Websocket session token doesn't belong to user."

    if wsSessionErr?
      @logger.error "ERROR: #{wsSessionErr}"
      ws.close 4000, wsSessionErr
      return true

    # Retrieve request parameters
    urn          = getFromReq req, 'urn'
    role         = getFromReq req, 'role'
    instanceId   = getFromReq req, 'instance'
    container    = getFromReq req, 'container'
    command      = getFromReq req, 'command'
    tty          = getFromReq(req, 'tty') == 'yes'
    outRows      = getFromReq req, 'rows'
    outColumns   = getFromReq req, 'columns'

    @logger.info "#{meth} - Parameters: URN: #{urn} - Role: #{role} -
                  instanceId=#{instanceId} - container: #{container} -
                  command: #{JSON.stringify command} - tty: #{tty} -
                  outRows: #{outRows} - outColumns: #{outColumns}"

    rows    = parseInt outRows
    columns = parseInt outColumns

    if not urn?
      errMsg = 'Missing mandatory parameter (solution URN)'
      @logger.error "ERROR: #{errMsg}"
      ws.close 4000, errMsg
      return true

    if not role?
      errMsg = 'Missing mandatory parameter (solution role)'
      @logger.error "ERROR: #{errMsg}"
      ws.close 4000, errMsg
      return true

    if not instanceId?
      errMsg = 'Missing mandatory parameter (instance ID)'
      @logger.error "ERROR: #{errMsg}"
      ws.close 4000, errMsg
      return true

    if not command?
      errMsg = 'Missing mandatory parameter (command)'
      @logger.error "ERROR: #{errMsg}"
      ws.close 4000, errMsg
      return true

    if isNaN(rows) or isNaN(columns)
      errMsg = "Output stream size contains non numeric values.
                outRows: #{outRows} - outColumns: #{outColumns}"
      @logger.error "ERROR: #{errMsg}"
      ws.close 4000, errMsg
      return true


    # Cleanup function to be called when WS session is finished
    cleanup = (code, reason) =>
      @logger.info "#{meth} - Clean-up (Code: #{code}, Reason: #{reason})"
      try
        ws.removeAllListeners()
        ws.close code, reason
        ws.terminate()
        # If streams have been initialized (check any stream), destroy them
        if myStreams.out?
          myStreams.out.removeAllListeners()
          myStreams.err.removeAllListeners()
          myStreams.in.removeAllListeners()
          myStreams.out.destroy()
          myStreams.err.destroy()
          myStreams.in.destroy()
      catch err
        @logger.info "#{meth} - Error during clean-up: #{err.message} -
                      #{err.stack})"
      true


    # Validate user has permissions to access the requested solution
    @logger.info "#{meth} - Checking user permissions to access solution #{urn}"

    @authorization.isAllowedForURN urn, context
    .then (isAllowed) =>
      if not isAllowed
        errMsg = 'User is not allowed to access solution.'
        @logger.debug "#{meth} - #{errMsg}"
        throw new Error errMsg

      @logger.info "#{meth} - User is allowed to access element #{urn}"

      # Convert solution+role+instance into deployment+role+instance
      @adm.solutionElementToDeploymentElement urn, role, instanceId, context
    .then (deploymentInstanceData) =>
      realInstanceId = deploymentInstanceData.instanceName
      realContainerName = @manifestHelper.hashContainerName container

      # Create readable stream for sending commands ('stdin')
      rInStreamOpts =
        encoding: 'utf8'
        read: (size) ->
      rInStream = new stream.Readable rInStreamOpts

      # Create writable stream for handling received output ('stdout')
      wOutStreamOpts =
        defaultEncoding: 'utf8'
        write: (chunk, encoding, callback) ->
          # console.log "wOutStream.write - #{chunk}"
          ws.send "OUT:" + chunk
          process.nextTick callback
        writev: (chunks, callback) ->
          # console.log "wOutStream.writev - #{chunks}"
          process.nextTick callback
      wOutStream = new stream.Writable wOutStreamOpts
      wOutStream.rows = rows
      wOutStream.columns = columns
      wOutStream.on 'error', ->

      # Create writable stream for handling received errors ('stderr')
      wErrStreamOpts =
        defaultEncoding: 'utf8'
        write: (chunk, encoding, callback) ->
          # console.log "wErrStream.write - #{chunk}"
          ws.send "ERR:" + chunk
          process.nextTick callback
        writev: (chunks, callback) ->
          #console.log "wErrStream.writev - #{chunks}"
          process.nextTick callback
      wErrStream = new stream.Writable wErrStreamOpts
      wErrStream.on 'error', ->

      # Prepare streams to use in K8s exec connection
      myStreams =
        out: wOutStream
        err: wErrStream
        in:  rInStream

      # Client Websocket event handlers
      ws.on 'message', (msg) =>
        # A message from the client is considered a comman to execute
        # console.log "ClientWS --> onMessage (stdin): #{msg}"
        if msg?.startsWith "RESIZE:"
          @logger.info "#{meth} - Output resize received: #{msg}"
          msg = msg.slice 7        # Remove 'RESIZE:'
          toks = msg.split ','
          if toks.length is 2
            newRows = parseInt toks[0]
            newColumns = parseInt toks[1]
            if (not isNaN(rows)) and (not isNaN(columns))
              wOutStream.rows = newRows
              wOutStream.columns = newColumns
              wOutStream.emit 'resize'
        else
          rInStream.push msg

      ws.on 'open', () =>
        @logger.info "#{meth} - Websocket with client is open!"

      ws.on 'close', (code, reason) =>
        # The client closed its websocket, cleanup
        @logger.info "#{meth} - Client Websocket closed! Code: #{code} -
                      Reason: #{reason}"
        # Sending 'null' is equivalent as signaling the end of input
        rInStream.push(null)

      ws.on 'error', (err) =>
        # En error ocurred in WS communication
        @logger.info "#{meth} - Client Websocket error: #{err}"
        # Sending 'null' is equivalent as signaling the end of input
        rInStream.push(null)

      # Call K8s API stub for executing the command
      @k8sApiStub.execPod null, realInstanceId, realContainerName, command, tty, myStreams
      .then (status) =>
        @logger.info "#{meth} Exec command finished with status:
                     #{JSON.stringify status}"
        cleanup 1000, "Command finished."
    .catch (err) =>
      errMsg = "Command could not be executed. Error: #{err.message || err}"
      @logger.error "#{meth} #{errMsg}"
      cleanup 4000, errMsg


  getInstanceLogs: (req,res) =>
    meth = 'AdmissionRestAPI.getInstanceLogs'
    context = @setupContext req
    @logger.info "#{meth} - User: #{context.user.id || context.user.name || ''}"

    # Handlers to log client disconnections (just for information)
    req.on "close", () =>
      @logger.info "#{meth} - Http connection closed by the client."
    req.on "end", () =>
      @logger.info "#{meth} - Http connection ended."


    # Retrieve request parameters
    instanceId   = getFromReq req, 'instance'
    container    = getFromReq req, 'container'
    follow       = getFromReq(req, 'follow') == 'yes'
    previous     = getFromReq(req, 'previous') == 'yes'
    tailLines    = getFromReq req, 'tail'
    sinceSeconds = getFromReq req, 'since'

    @logger.info "#{meth} - Parameters: instanceId=#{instanceId} -
                  container: #{container} - follow: #{follow} -
                  tailLines: #{tailLines} - sinceSeconds: #{sinceSeconds}
                  previous: #{previous}"

    # If follow mode is on, don't let the connection timeout
    if follow
      req.setTimeout 30*24*60*60*1000 # 30 days

    # Validate mandatory parameter instanceId is present
    if not instanceId?
      errMsg = 'Missing mandatory parameter (instance ID)'
      @logger.error "ERROR: #{errMsg}"
      response =
        success : false
        message: errMsg
      res.send response
      return true

    # Validate user has permissions to access the requested instance
    @logger.info "#{meth} - Checking user permissions to access instance #{instanceId}"
    @authorization.isAllowed 'instance', instanceId, context
    q true
    .then (allowed) =>
      if not allowed
        errMsg = "User is not allowed to access instance."
        @logger.error "#{meth} - #{errMsg}"
        response =
          success : false
          message: errMsg
        res.send response
        return true

      # Prepare options to pass to log retriever
      # As a WritableStream we pass the HTTP response object
      logOptions =
        follow: follow
        previous: previous
        tailLines: tailLines
        sinceSeconds: sinceSeconds

      res.writeHead 200, { 'Content-Type' : 'text/plain; charset=utf-8' }
      @k8sApiStub.getPodLogs null, instanceId, container, logOptions, res
      .catch (err) =>
        if err.message?.includes "previous terminated container"
          errMsg = 'Previous container not found.'
        else if err.message?.includes "is not valid for pod"
          errMsg = 'Container not valid for instance.'
        else
          errMsg = "Unable to get instance logs."
          @logger.error "ERROR: Unexpected error: #{err.message}"
        @logger.error "ERROR: #{errMsg}"
        response =
          success : false
          message: errMsg
        res.write JSON.stringify response
        res.end()
        return true
    .then (status) =>
      @logger.info "#{meth} - Finished log retrieval finished. (#{status})"
      return true
    .catch (err) =>
      errMsg = "Error while tailing logs: #{err}"
      @logger.error "ERROR: #{errMsg}"
      response =
        success : false
        message: errMsg
      res.send response
      return true


  execInstanceCommandGet: (req, res) =>
    meth = 'AdmissionRestAPI.execInstanceCommandGet'
    context = @setupContext req
    @logger.info "#{meth} - User: #{context.user.id || context.user.name || ''}"

    # Retrieve request parameters
    instanceId   = getFromReq req, 'instance'
    container    = getFromReq req, 'container'
    command      = getFromReq req, 'command'
    tty          = getFromReq(req, 'tty') == 'yes'
    outRows      = getFromReq req, 'rows'
    outColumns   = getFromReq req, 'columns'

    @logger.info "#{meth} - Parameters: instanceId=#{instanceId} -
                  container: #{container} - command: #{command} -
                  tty: #{tty} - outRows: #{outRows} - outColumns: #{outColumns}"

    rows    = parseInt outRows
    columns = parseInt outColumns

    # Validate mandatory parameter instanceId is present
    if not instanceId?
      errMsg = 'Missing mandatory parameter (instance ID)'
      @logger.error "ERROR: #{errMsg}"
      response =
        success : false
        message: errMsg
      res.send response
      return true

    if not command?
      errMsg = 'Missing mandatory parameter (command)'
      @logger.error "ERROR: #{errMsg}"
      response =
        success : false
        message: errMsg
      res.send response
      return true

    if isNaN(rows) or isNaN(columns)
      errMsg = "Output stream size contains non numeric values.
                outRows: #{outRows} - outColumns: #{outColumns}"
      @logger.error "ERROR: #{errMsg}"
      response =
        success : false
        message: errMsg
      res.send response
      return true

    @logger.info "#{meth} - Checking instance and container data..."
    @k8sApiStub.execValidationCheck instanceId, container
    .then () =>
      # Validate user has permissions to access the requested instance
      @logger.info "#{meth} - Checking user permissions to access instance
                    #{instanceId}..."
      @authorization.isAllowed 'instance', instanceId, context
    .then (allowed) =>
      if not allowed
        throw new Error "User is not allowed to access instance."

      response =
        success : true
        message: "Websocket access is allowed to instance #{instanceId}."
        data:
          wsSessionToken: @generateEphemeralExecSessionToken context.user.id
      res.send response
      return true
    .catch (err) =>
      errMsg = "Error running pre-exec checks: #{err.message}"
      @logger.error "#{meth} - ERROR: #{errMsg}"
      response =
        success : false
        message: errMsg
      res.send response
      return true


  execInstanceCommand: (ws, req) =>
    meth = 'AdmissionRestAPI.execInstanceCommand'
    context = @setupContext req
    @logger.info "#{meth} - User: #{context.user.id || context.user.name || ''}"

    # Exec requests are unlimited in time
    req.setTimeout 30*24*60*60*1000 # 30 days

    # Initialize stream holder
    myStreams = {}

    # Validate ephemeral Websocket session token
    wsToken = getFromReq req, 'session_token'
    @logger.info "#{meth} - Validating WS session token: #{wsToken}"

    wsSessionErr = null
    if not wsToken?
      wsSessionErr = 'Missing mandatory parameter (session_token).'
    else if wsToken not of @ephemeralWebsocketTokens
      wsSessionErr = 'Websocket session token is expired or invalid.'
    else if @ephemeralWebsocketTokens[wsToken] isnt context.user.id
      wsSessionErr = "Websocket session token doesn't belong to user."

    if wsSessionErr?
      @logger.error "ERROR: #{wsSessionErr}"
      ws.close 4000, wsSessionErr
      return true

    # Retrieve request parameters
    instanceId   = getFromReq req, 'instance'
    container    = getFromReq req, 'container'
    command      = getFromReq req, 'command'
    tty          = getFromReq(req, 'tty') == 'yes'
    outRows      = getFromReq req, 'rows'
    outColumns   = getFromReq req, 'columns'

    @logger.info "#{meth} - Parameters: instanceId=#{instanceId} -
                  container: #{container} - command: #{command} -
                  tty: #{tty} - outRows: #{outRows} - outColumns: #{outColumns}"

    rows    = parseInt outRows
    columns = parseInt outColumns

    if not instanceId?
      errMsg = 'Missing mandatory parameter (instance ID)'
      @logger.error "ERROR: #{errMsg}"
      ws.close 4000, errMsg
      return true

    if not command?
      errMsg = 'Missing mandatory parameter (command)'
      @logger.error "ERROR: #{errMsg}"
      ws.close 4000, errMsg
      return true

    if isNaN(rows) or isNaN(columns)
      errMsg = "Output stream size contains non numeric values.
                outRows: #{outRows} - outColumns: #{outColumns}"
      @logger.error "ERROR: #{errMsg}"
      ws.close 4000, errMsg
      return true


    # Cleanup function to be called when WS session is finished
    cleanup = (code, reason) =>
      @logger.info "#{meth} - Clean-up (Code: #{code}, Reason: #{reason})"
      try
        ws.removeAllListeners()
        ws.close code, reason
        ws.terminate()
        # If streams have been initialized (check any stream), destroy them
        if myStreams.out?
          myStreams.out.removeAllListeners()
          myStreams.err.removeAllListeners()
          myStreams.in.removeAllListeners()
          myStreams.out.destroy()
          myStreams.err.destroy()
          myStreams.in.destroy()
      catch err
        @logger.info "#{meth} - Error during clean-up: #{err.message} -
                      #{err.stack})"
      true


    # Validate user has permissions to access the requested instance
    @logger.info "#{meth} - Checking user permissions to access instance #{instanceId}"
    @authorization.isAllowed 'instance', instanceId, context
    .then (allowed) =>
      if not allowed
        errMsg = "User is not allowed to access instance."
        @logger.error "#{meth} - #{errMsg}"
        ws.close 4000, errMsg
        return true
      else
        # Create readable stream for sending commands ('stdin')
        rInStreamOpts =
          encoding: 'utf8'
          read: (size) ->
        rInStream = new stream.Readable rInStreamOpts

        # Create writable stream for handling received output ('stdout')
        wOutStreamOpts =
          defaultEncoding: 'utf8'
          write: (chunk, encoding, callback) ->
            # console.log "wOutStream.write - #{chunk}"
            ws.send "OUT:" + chunk
            process.nextTick callback
          writev: (chunks, callback) ->
            # console.log "wOutStream.writev - #{chunks}"
            process.nextTick callback
        wOutStream = new stream.Writable wOutStreamOpts
        wOutStream.rows = rows
        wOutStream.columns = columns
        wOutStream.on 'error', ->

        # Create writable stream for handling received errors ('stderr')
        wErrStreamOpts =
          defaultEncoding: 'utf8'
          write: (chunk, encoding, callback) ->
            # console.log "wErrStream.write - #{chunk}"
            ws.send "ERR:" + chunk
            process.nextTick callback
          writev: (chunks, callback) ->
            #console.log "wErrStream.writev - #{chunks}"
            process.nextTick callback
        wErrStream = new stream.Writable wErrStreamOpts
        wErrStream.on 'error', ->

        # Prepare streams to use in K8s exec connection
        myStreams =
          out: wOutStream
          err: wErrStream
          in:  rInStream

        # Client Websocket event handlers
        ws.on 'message', (msg) =>
          # A message from the client is considered a comman to execute
          # console.log "ClientWS --> onMessage (stdin): #{msg}"
          if msg?.startsWith "RESIZE:"
            @logger.info "#{meth} - Output resize received: #{msg}"
            msg = msg.slice 7        # Remove 'RESIZE:'
            toks = msg.split ','
            if toks.length is 2
              newRows = parseInt toks[0]
              newColumns = parseInt toks[1]
              if (not isNaN(rows)) and (not isNaN(columns))
                wOutStream.rows = newRows
                wOutStream.columns = newColumns
                wOutStream.emit 'resize'
          else
            rInStream.push msg

        ws.on 'open', () =>
          @logger.info "#{meth} - Websocket with client is open!"

        ws.on 'close', (code, reason) =>
          # The client closed its websocket, cleanup
          @logger.info "#{meth} - Client Websocket closed! Code: #{code} -
                        Reason: #{reason}"
          # Sending 'null' is equivalent as signaling the end of input
          rInStream.push(null)

        ws.on 'error', (err) =>
          # En error ocurred in WS communication
          @logger.info "#{meth} - Client Websocket error: #{err}"
          # Sending 'null' is equivalent as signaling the end of input
          rInStream.push(null)

        # Call K8s API stub for executing the command
        @k8sApiStub.execPod null, instanceId, container, command, tty, myStreams
        .then (status) =>
          @logger.info "#{meth} Exec command finished with status:
                       #{JSON.stringify status}"
          cleanup 1000, "Command finished."
    .catch (err) =>
      errMsg = "Command could not be executed. Error: #{err.message || err}"
      @logger.error "#{meth} #{errMsg}"
      cleanup 4000, errMsg


  generateEphemeralExecSessionToken: (user) ->
    meth = "generateEphemeralExecSessionToken user: #{user}"
    @logger.debug meth

    token = utils.randomHex 10
    @ephemeralWebsocketTokens[token] = user
    @logger.debug "#{meth} - Generated new token for user (will expire in
                   #{EPEHEMERAL_WS_TOKEN_LIFETIME/1000} seconds."
    # Self destroy after lifetime
    setTimeout () =>
      delete @ephemeralWebsocketTokens[token]
      @logger.debug "#{meth} - Token expired : #{token}"
    , EPEHEMERAL_WS_TOKEN_LIFETIME

    return token


  getSolutionRevision: (req, res) =>
    meth = 'AdmissionRestAPI.getSolutionRevision'
    context = @setupContext req
    @logger.info "#{meth} - User: #{context.user.id || context.user.name || ''}"

    # Retrieve request parameters
    urn = getFromReq req, 'urn'
    rev = getFromReq req, 'rev'
    @logger.info "#{meth} - Parameters: urn=#{urn} - rev=#{rev}"

    # Validate mandatory parameter urn is present
    if not urn?
      errMsg = 'Missing mandatory parameter (solution URN)'
      @logger.error "ERROR: #{errMsg}"
      response =
        success : false
        message: errMsg
      res.send response
      return true

    solRef = @manifestHelper.urnToRef urn
    @logger.debug "#{meth} - solRef = #{JSON.stringify solRef}"

    # Validate URN is of a solution
    if solRef.kind isnt 'solution'
      errMsg = 'Invalid URN (not a solution)'
      @logger.error "ERROR: #{errMsg}"
      response =
        success : false
        message: errMsg
      res.send response
      return true



    # Validate user has permissions to access the requested solution
    @logger.info "#{meth} - Checking user permissions to access solution
                  #{urn}"

    # Check permissions
    @authorization.isAllowedForURN urn, context
    .then (isAllowed) =>
      if not isAllowed
        errMsg = "User is not allowed to access solution."
        @logger.info "#{meth} - #{errMsg}"
        throw new Error errMsg
    .then () =>
      if not rev?
        # Return a list of all revisions of the solution
        # The last part of the selector is for excluding revisions of objects
        # that are derived from the solution itself.
        labelSelector = ''
        labelSelector += "kumori/domain=" +
                         @manifestHelper.hashLabel solRef.domain
        labelSelector += ",kumori/name=" +
                         @manifestHelper.hashLabel solRef.name
        labelSelector += ",!kumori/solution.id"

        @k8sApiStub.getControllerRevisions KUMORI_NAMESPACE, labelSelector
        .then (items) =>
          @logger.debug "#{meth} - Got list of revisions (#{items.length})"
          list = []
          # From the list of ControllerRevisions that matched the LabelSelector,
          # discard the ones that don't have the Kumori history annotation.
          # With the remaining ones, create a simplified list based on the
          # history.
          for it in items
            if it.metadata?.annotations? \
            and 'kumori/history' of it.metadata.annotations
              history =
                JSON.parse it.metadata.annotations['kumori/history']
              for hit in history
                newItem =
                  name: it.metadata.name
                  revision: hit.revision
                  timestamp: hit.timestamp
                  comment: hit.comment
                list.push newItem
          @logger.debug "#{meth} - Returning list: #{JSON.stringify list}"
          response =
            success : true
            message: "solution revisions retrieved successfully."
            data: list
          res.send response
          return true
      else
        # Return a specific revision
        @k8sApiStub.getControllerRevision KUMORI_NAMESPACE, rev
        .then (revision) =>
          @logger.debug "#{meth} - Got revision: #{JSON.stringify revision}"

          # Verify that the revision contains the original manifest
          if revision.spec?.kumoriManifest?
            compressedManifest = revision.spec.kumoriManifest
          if revision.metadata?.annotations?['kumori/manifest']?
            compressedManifest = revision.metadata.annotations['kumori/manifest']
          else
            errMsg = "No manifest data found in revision."
            @logger.error "#{meth} - #{errMsg}"
            throw new Error errMsg

          # Extract the original manifest from the revision
          manifest = JSON.parse (utils.gunzipString compressedManifest)
          @logger.debug "#{meth} - Manifest Name: #{manifest.urn}"

          # Check that the revision belongs to the requested solution
          if manifest.urn isnt urn
            errMsg = "Revision belongs to a different solution."
            @logger.error "#{meth} - #{errMsg}"
            throw new Error errMsg

          response =
            success : true
            message: "Solution revision retrieved successfully."
            data: manifest
          res.send response
          return true
    .catch (err) =>
      errMsg = "Couldn't get solution revisions: #{err.message}"
      @logger.error "#{meth} - Error: #{errMsg}"
      response =
        success : false
        message: errMsg
      res.send response
      return true


  getDeploymentRevision: (req, res) =>
    meth = 'AdmissionRestAPI.getDeploymentRevision'
    context = @setupContext req
    @logger.info "#{meth} - User: #{context.user.id || context.user.name || ''}"

    # Retrieve request parameters
    urn = getFromReq req, 'urn'
    rev = getFromReq req, 'rev'
    @logger.info "#{meth} - Parameters: urn=#{urn} - rev=#{rev}"

    # Validate mandatory parameter instanceId is present
    if not urn?
      errMsg = 'Missing mandatory parameter (deployment URN)'
      @logger.error "ERROR: #{errMsg}"
      response =
        success : false
        message: errMsg
      res.send response
      return true

    deplRef = @manifestHelper.urnToRef urn
    @logger.debug "#{meth} - deplRef = #{JSON.stringify deplRef}"

    # Validate user has permissions to access the requested deployment
    @logger.info "#{meth} - Checking user permissions to access deployment
                  #{urn}"

    # Check permissions
    @authorization.isAllowed 'deployment', urn, context
    .then (allowed) =>
      if not allowed
        errMsg = "User is not allowed to access deployment."
        @logger.info "#{meth} - #{errMsg}"
        throw new Error errMsg
    .then () =>
      if not rev?
        # Return a list of all revisions of the deployment
        labelSelector = ''
        labelSelector += "kumori/domain=#{deplRef.domain}"
        labelSelector += ",kumori/name=#{deplRef.name}"

        @k8sApiStub.getControllerRevisions KUMORI_NAMESPACE, labelSelector
        .then (items) =>
          @logger.debug "#{meth} - Got list of revisions (#{items.length})"
          list = []
          # From the list of ControllerRevisions that matched the LabelSelector,
          # discard the ones that don't have the Kumori history annotation.
          # With the remaining ones, create a simplified list based on the
          # history.
          for it in items
            if it.metadata?.annotations? \
            and 'kumori/history' of it.metadata.annotations
              history =
                JSON.parse it.metadata.annotations['kumori/history']
              for hit in history
                newItem =
                  name: it.metadata.name
                  revision: hit.revision
                  timestamp: hit.timestamp
                  comment: hit.comment
                list.push newItem
          @logger.debug "#{meth} - Returning list: #{JSON.stringify list}"
          response =
            success : true
            message: "Deployment revisions retrieved successfully."
            data: list
          res.send response
          return true
      else
        # Return a specific revision
        @k8sApiStub.getControllerRevision KUMORI_NAMESPACE, rev
        .then (revision) =>
          @logger.debug "#{meth} - Got revision: #{JSON.stringify revision}"

          # Verify that the revision contains the original manifest
          if not revision.metadata?.annotations?.manifest?
            errMsg = "No manifest data found in revision."
            @logger.error "#{meth} - #{errMsg}"
            throw new Error errMsg

          # Extract the original manifest from the revision
          manifest = JSON.parse (utils.gunzipString revision.metadata.annotations.manifest)
          @logger.debug "#{meth} - MANIFEST NAME: #{manifest.urn}"

          # Check that the revision belongs to the requested deployment
          if manifest.urn isnt urn
            errMsg = "Revision belongs to a different deployment."
            @logger.error "#{meth} - #{errMsg}"
            throw new Error errMsg

          response =
            success : true
            message: "Deployment revision retrieved successfully."
            data: manifest
          res.send response
          return true
    .catch (err) =>
      errMsg = "Couldn't get deployment revisions: #{err.message}"
      @logger.error "#{meth} - Error: #{errMsg}"
      response =
        success : false
        message: errMsg
      res.send response
      return true





################################################################################
##                           HELPER FUNCTIONS                                 ##
################################################################################
startsWith = (str, prefix) ->
  str[..(prefix.length-1)] is prefix


decodeJwt = (token) ->
  segments = token.split '.'

  if segments.length isnt 3
    throw new Error("Token has wrong number of segments (#{segments.length}).")

  # All segment should be base64
  headerSeg = segments[0]
  payloadSeg = segments[1]
  signatureSeg = segments[2]
  # base64 decode and parse JSON
  header  = JSON.parse base64urlDecode headerSeg
  payload = JSON.parse base64urlDecode payloadSeg

  return {
    header: header
    payload: payload
    signature: signatureSeg
  }


base64urlDecode = (str) ->
  return Buffer.from(base64urlUnescape(str), 'base64').toString()


base64urlUnescape = (str) ->
  str += Array(5 - str.length % 4).join('=')
  return str.replace(/\-/g, '+').replace(/_/g, '/')


# Returns an object with the following User data:
#
# - access_token: (example: "njrfo4fnonf2f...")
# - token_type: (example 'bearer')
# - expires_in: (example 60)
# - refresh_token: (example: "njrfo4fnonf2f...")
# - user:
#   - username (example: "alice")
#   - name (example: "Alice Liddel")
#   - email (example: "alice@keycloak.org")
#   - groups (example: ["offline_access","user"])
#   - kumoriGroups (example: ["developer"])
getAuthData = (req) ->
  # console.log "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
  # console.log "getAuthData - REQ KEYS: #{Object.keys req}"
  # console.log "getAuthData - KAUTH: #{JSON.stringify req.kauth}"
  # console.log "getAuthData - SESSION: #{JSON.stringify req.session}"
  # console.log "getAuthData - CCAUTH: #{JSON.stringify req.clientcertauth}"
  # console.log "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
  if req.kauth?.grant?
    # console.log "getAuthData - KAUTH: #{JSON.stringify req.kauth.grant,}"
    accessTokenData = req.kauth.grant.access_token.content
    authData =
      access_token: req.kauth.grant.access_token.token
      token_type: req.kauth.grant.token_type
      expires_in: req.kauth.grant.expires_in
      refresh_token: null
      user:
        id: accessTokenData.preferred_username
        username: accessTokenData.preferred_username
        name: accessTokenData.name
        email: accessTokenData.email
        groups: accessTokenData.groups || []
        kumoriGroups: accessTokenData['client-groups'] || []
        roles: accessTokenData['client-groups'] || []
        limits: null
    if req.kauth.grant.refresh_token?
      authData.refresh_token = req.kauth.grant.refresh_token.token
    # console.log "REQUEST AUTH DATA: #{JSON.stringify authData}"
    return authData
  else if req.clientcertauth?
    authData = req.clientcertauth
    console.log "REQUEST AUTH DATA FROM CLIENT CERT: #{JSON.stringify authData}"
    return authData
  else
    # console.log "getAuthData - NO KAUTH.GRANT --> returning null"
    return null


# Load a file and return its content
loadFile = (path) ->
  content = null
  try
    content = require path
    q.resolve content
  catch error
    q.reject error


cleanupFiles = (fileList) ->
  promises = []
  for file in fileList
    promises.push utils.deleteFile file
  q.all promises



# Generic for getting a named parameter from request URL.
# If the value is URI-encoded, it decodes it
getFromReq = (req, name)->
  value = req.params?[name] ? req.query?[name]
  if (not value?) or (value is '')
    return undefined
  else if 'string' is typeof value
    # If param is a string, URI-decode it after replacing + with space
    # Reference: https://stackoverflow.com/a/20247435
    return decodeURIComponent value.replace(/\+/g, ' ')
  else if Array.isArray value
    # If param is an array, return it but with any strings URI-decoded
    cleanValues = []
    for v in value
      if 'string' is typeof v
        cleanValues.push(decodeURIComponent v.replace(/\+/g, ' '))
      else
        cleanValues.push v
    return cleanValues
  else
    return value


module.exports = AdmissionRestAPI
