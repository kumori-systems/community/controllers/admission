#!/bin/bash

################################################################
#                                                              #
#  * Copyright 2022 Kumori Systems S.L.                        #
#                                                              #
#  * Licensed under the EUPL, Version 1.2 or – as soon they    #
#    will be approved by the European Commission - subsequent  #
#    versions of the EUPL (the "Licence");                     #
#                                                              #
#  * You may not use this work except in compliance with the   #
#    Licence.                                                  #
#                                                              #
#  * You may obtain a copy of the Licence at:                  #
#                                                              #
#    https://joinup.ec.europa.eu/software/page/eupl            #
#                                                              #
#  * Unless required by applicable law or agreed to in         #
#    writing, software distributed under the Licence is        #
#    distributed on an "AS IS" basis,                          #
#                                                              #
#  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either      #
#    express or implied.                                       #
#                                                              #
#  * See the Licence for the specific language governing       #
#    permissions and limitations under the Licence.            #
#                                                              #
################################################################


# Colors for logging
COL_DEFAULT='\e[0;00m'
COL_BLUE='\e[0;96m'
COL_GREEN='\e[0;92m'
COL_RED='\e[0;91m'


IMAGE_NAME="admission-kv3:1.0"


BASE_DIR="$(dirname $(readlink -f $0))"
WORK_DIR="${BASE_DIR}/Temp"

SSH_KEY_PATH="${HOME}/.ssh/<key-with-git-clone-permissions>"

echo
echo -e $COL_GREEN"BUILDING IMAGE ${IMAGE_NAME}"$COL_DEFAULT
echo

echo
echo -e $COL_BLUE"Building source..."$COL_DEFAULT
echo
npm run build
echo
echo -e $COL_BLUE"Done."$COL_DEFAULT
echo

echo
echo -e $COL_BLUE"Preparing working directory..."$COL_DEFAULT
echo
rm -rf ${WORK_DIR}

# Optionally add a private key for accessing private Git repositories (mostly
# for DEV or private dependencies)
mkdir -p ${WORK_DIR}/.ssh
cp ${SSH_KEY_PATH} ${WORK_DIR}/.ssh/deployment_key

cp -r ../package.json ${WORK_DIR}/package.json
cp -r ../lib ${WORK_DIR}
cp -r ../config ${WORK_DIR}
cp -r ../LICENSE.txt ${WORK_DIR}/LICENSE.txt
cp -r Dockerfile ${WORK_DIR}/Dockerfile
ls -l ${WORK_DIR}
echo
echo -e $COL_BLUE"Done."$COL_DEFAULT
echo

echo
echo -e $COL_BLUE"Docker building the image..."$COL_DEFAULT
echo
echo "docker build -t ${IMAGE_NAME} ${WORK_DIR}"
docker build -t ${IMAGE_NAME} ${WORK_DIR}
echo
echo -e $COL_BLUE"Done."$COL_DEFAULT
echo

echo
echo -e $COL_BLUE"Cleaning up working directory..."$COL_DEFAULT
echo
rm -rf ${WORK_DIR}
echo
echo -e $COL_BLUE"Done."$COL_DEFAULT
echo
echo
echo -e $COL_GREEN"DOCKER IMAGE ${IMAGE_NAME} BUILT."$COL_DEFAULT
echo
