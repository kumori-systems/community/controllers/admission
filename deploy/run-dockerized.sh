#!/bin/bash

################################################################
#                                                              #
#  * Copyright 2022 Kumori Systems S.L.                        #
#                                                              #
#  * Licensed under the EUPL, Version 1.2 or – as soon they    #
#    will be approved by the European Commission - subsequent  #
#    versions of the EUPL (the "Licence");                     #
#                                                              #
#  * You may not use this work except in compliance with the   #
#    Licence.                                                  #
#                                                              #
#  * You may obtain a copy of the Licence at:                  #
#                                                              #
#    https://joinup.ec.europa.eu/software/page/eupl            #
#                                                              #
#  * Unless required by applicable law or agreed to in         #
#    writing, software distributed under the Licence is        #
#    distributed on an "AS IS" basis,                          #
#                                                              #
#  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either      #
#    express or implied.                                       #
#                                                              #
#  * See the Licence for the specific language governing       #
#    permissions and limitations under the Licence.            #
#                                                              #
################################################################


# Colors for logging
COL_DEFAULT='\e[0;00m'
COL_BLUE='\e[0;96m'
COL_GREEN='\e[0;92m'
COL_RED='\e[0;91m'


IMAGE_NAME="admission-kv3:1.0"

CONTAINER_NAME="admission"
ADMISSION_PORT=3003
ADMISSION_CONFIG_FILE=""
KUBERNETES_CONFIG_DIR=""


echo
echo -e $COL_GREEN"RUNNING ADMISSION ON PORT ${ADMISSION_PORT} FROM DOCKER FILE ${IMAGE_NAME}"$COL_DEFAULT
echo
echo -e $COL_GREEN"Admission configuration file : ${ADMISSION_CONFIG_FILE}"$COL_DEFAULT
echo -e $COL_GREEN"Kubernetes configuration dir: ${KUBERNETES_CONFIG_DIR}"$COL_DEFAULT
echo

docker run --rm -it \
  --name ${CONTAINER_NAME} \
  -e KUBECONFIG=/kumori/kubernetes-config/kubeconfig \
  -v ${ADMISSION_CONFIG_FILE}:/kumori/admission-server-config.json \
  -v ${KUBERNETES_CONFIG_DIR}:/kumori/kubernetes-config \
  -p ${ADMISSION_PORT}:${ADMISSION_PORT} \
  ${IMAGE_NAME}

